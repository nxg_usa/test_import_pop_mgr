<?php
/*
 *
 *  Copyright (c) 2004, SWITCH - Teleinformatikdienste fuer Lehre und Forschung
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *   * Neither the name of SWITCH nor the names of its contributors may be
 *     used to endorse or promote products derived from this software without
 *     specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 * 
 *  $Author: phaag $
 *
 *  $Id: nfsen.php 22 2007-11-20 12:27:38Z phaag $
 *
 *  $LastChangedRevision: 22 $
 *
 */

// The very first function to call
session_start();

$expected_version = "1.3.6p1";

// Session check
if ( !array_key_exists('backend_version', $_SESSION ) || $_SESSION['backend_version'] !=  $expected_version ) {
	session_destroy();
	session_start();
	$_SESSION['version'] = $expected_version;
//	print "<h1>Frontend - Backend version missmatch!</h1>\n";
}

include ("conf.php");
include ("nfsenutil.php");
//include ("navigator.php");
include ("nfdumpquery_conf.php");

$TabList	= array ( 'Home', 'Graphs', 'Details', 'Alerts', 'Stats', 'Plugins', 'Dashboard' );
$GraphTabs	= array ( 'Flows', 'Packets', 'Traffic');

// these session vars are packed into the bookmark
$BookmarkVars =  array ( 'tab', 'sub_tab', 'profileswitch', 'channellist', 'detail_opts/proto', 'detail_opts/type', 
						 'detail_opts/wsize', 'detail_opts/cursor_mode', 'tend', 'tleft', 'tright', 
						 'detail_opts/logscale', 'detail_opts/ratescale', 'detail_opts/linegraph');

// All available options 


$self = $_SERVER['PHP_SELF'];

//
// Function definitions
//

function SendHeader ($established) {

	global $self;
	global $TabList;

//	header("Content-type: text/html; charset=ISO-8859-1");
//	header("Content-type: application/json; charset=UTF-8");
	header("Content-type: application/json");
//	header("Content-type: text/plain; charset=us-ascii");

	if ( !$established ) 
		return;

	$refresh = $_SESSION['refresh'];

} // End of SendHeader

function OpenLogFile () {
	global $log_handle;
	global $DEBUG;

	if ( $DEBUG ) {
		$log_handle = @fopen("/var/tmp/nfsen-log", "a");
		$_d = date("Y-m-d-H:i:s");
		ReportLog("\n=========================\nRun at $_d\n"); 
	} else 
		$log_handle = null;

} // End of OpenLogFile

function CloseLogFile () {
	global $log_handle;

	if ( $log_handle )
		fclose($log_handle);

} // End of CloseLogFile

function ReportLog($message) {
	global $log_handle;

	if ( $log_handle )
		fwrite($log_handle, "$message\n");

} // End of ReportLog

function InitSession ($num_vars) {

	// force loading profil and plugin list
	unset($_SESSION['ProfileList']);
	unset($_SESSION['PluginList']);
	GetProfiles();
//	GetPlugins();

	$_SESSION['auto_filter'] = array();
//	DefaultFilters();

	// make tab and profileinfo exist in _SESSION
	$_SESSION['tab'] 		   = NULL;
	$_SESSION['profileswitch'] = NULL;

	// simulate a POST of tab and profileswitch
	$_POST['profileswitch'] = './live';
	$_POST['tab'] 			= 0;
	$_POST['sub_tab'] 		= 0;

	// Empty bookmark
	$vars = array();
	for ( $i=0; $i < $num_vars; $i++ ) {
		$vars[] = '-';
	}
	$_SESSION['bookmark'] = urlencode(base64_encode(implode('|', $vars)));


} // End of InitSession


////////////////////
// Main starts here
////////////////////

// Set debugging var
if ( !isset($DEBUG) ) {
    $DEBUG=0;
}

ClearMessages();

OpenLogFile();

// bootstrap

// Force new nfcapd session
unset($_SESSION['nfsend']);
$out_list = nfsend_query('get-globals', array(), 0);
if ( !is_array($out_list) ) {
	SetMessage('error', "Can not initialize globals");
	SendHeader(0);
	ShowMessages();
	exit;
}
foreach ( $out_list['globals'] as $global ) {
	eval($global);
}
$_SESSION['backend_version'] = $version;

// check disk usage
nfsend_query("get-du", array(), 0);

// Parameter parsing: check for tab and profileswitch
//list ($status, $tab_changed, $profile_changed) = ParseInput();
$status = TRUE;
if ( $status == FALSE ) {
	SendHeader(1);
	ShowMessages();
	exit;
}

//SendHeader(1);
//print "(1) Hello world!";
//return;

// tab processing
//$label = $TabList[$_SESSION['tab']];
$label = 'Dashboard';
switch ($label) {
	case "Dashboard":
		require ("dashboard.php");
		SetNfdumpQueryParams();
		break;
	default:
}


SendHeader(1);
//navigator();
ShowMessages();
//print "(2) Hello world!";
//return;

// Debugging
ob_start();
print "_SESSION 1:\n";
print_r($_SESSION);
print "_POST:\n";
print_r($_POST);
print "_GET:\n";
print_r($_GET);
print "_COOKIE:\n";
print_r($_COOKIE);
ReportLog(ob_get_contents());
ob_clean();

//print "(3) Hello world!";
//return;

$TabList 	  = array ( 'Home', 'Graphs', 'Details', 'Alerts', 'Stats', 'Plugins', 'Dashboard' );
// tab display
switch ($label) {
	case "Dashboard":
		RunProcessing();
		break;
	default:
}

//print "(4) Hello world!";
//return;

nfsend_disconnect();
unset($_SESSION['nfsend']);
CloseLogFile();

?>
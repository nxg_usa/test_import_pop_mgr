<?php
/* 
configuration specific to dashboard.php functionality
*/

// see corresponding variables set within /usr/local/nfsen/nfsen.conf
$BASEDIR = "/usr/local/nfsen";
$PROFILEDATADIR="$BASEDIR/profiles-data";


$POP1_PROFILE = 'live';
$POP1_PROFILE_SWITCH = "./$POP1_PROFILE";
//$POP1_ROUTER = "vmx-wave-00";
//$POP1_ROUTER_IFINDEXES = array( 523, 528 );
$POP1_ROUTER = "device_25";
$POP1_ROUTER_IFINDEXES = array( 761, 760, 754 );

?>

<?php


include "dashboard_conf.php";
include "simpleprocess.php";

$DisplayOrder = array ( 'any', 'TCP', 'UDP', 'ICMP', 'other' );

$TypeOrder	  = array ( 'flows', 'packets', 'traffic');


/* 
 * scale factor: Number of 5min slices per pixel
 * graph with 576 pixel
 * 0.25 * 576 * 300 = 43200 => 12 hours
 * 0.5  * 576 * 300 = 86400 => 1 day
 * $scale * 172800 = time range of graph
 */
$WinSizeScale  = array ( 0.25, 0.5, 1, 2, 3.5, 7, 15, 30, 90, 120, 183 );
// labels of the scale selector
$WinSizeLabels = array ( '12 Hours', '1 day', '2 days', '4 days', '1 week', '2 weeks', '1 month', '2 months', '6 months', '8 months', '1 year' );

// Definitions for the netflow processing table
$TopNOption   = array ( 10, 20, 50, 100, 200, 500);

$ListNOption  = array ( 20, 50, 100, 500, 1000, 10000);

$IPStatOption = array ( 'Flow Records', 
						'Any IP Address', 'SRC IP Address', 'DST IP Address', 
						'Any Port', 'SRC Port', 'DST Port',  
						'Any interface', 'IN interface', 'OUT interface',
						'Any AS',  'SRC AS',   'DST AS',
						'Next Hop IP', 'Next Hop BGP IP', 'Router IP',
						'Proto', 'Direction',
						'SRC TOS', 'DST TOS', 'TOS',
						'Any Mask Bits', 'SRC Mask Bits', 'DST Mask Bits',  
						'Any VLAN ID', 'SRC VLAN ID', 'DST VLAN ID',  
						'SRC MAC', 'DST MAC', 'IN MAC', 'OUT MAC',
						'IN SRC MAC', 'OUT DST MAC', 'IN DST MAC', 'OUT SRC MAC',
						'MPLS Label 1', 'MPLS Label 2', 'MPLS Label 3', 'MPLS Label 4', 'MPLS Label 5', 'MPLS Label 6', 'MPLS Label 7', 'MPLS Label 8', 'MPLS Label 9', 'MPLS Label 10'
					);

$IPStatArg	  = array ( '-s record', 
						'-s ip',   '-s srcip',   '-s dstip', 
						'-s port', '-s srcport', '-s dstport', 
						'-s if',   '-s inif',    '-s outif',
						'-s as',   '-s srcas',   '-s dstas',
						'-s nhip', '-s nhbip', 	 '-s router',
						'-s proto', '-s dir',
						'-s srctos', '-s dsttos', '-s tos',	 
						'-s mask',   '-s srcmask','-s dstmask',
						'-s vlan',   '-s srcvlan','-s dstvlan',
						'-s srcmac', '-d dstmac', '-s inmac', '-s outmac',
						'-s insrcmac',   '-s outdstmac','-s indstmac', '-s outsrcmac',
						'-s mapls1', '-s mapls2', '-s mapls3', '-s mapls4', '-s mapls5', '-s mapls6', '-s mapls7', '-s mapls8', '-s mapls9', '-s mapls10',
					);

$IPStatOrder  = array ( 'flows', 'packets', 'bytes', 'pps', 'bps', 'bpp' );
$LimitScale	  = array ( '-', 'K', 'M', 'G', 'T' );

$OutputFormatOption = array ( 'auto', 'line', 'long', 'extended');

$GlobalProfileSwitch = './live';
$GlobalProfile = 'live';
$GlobalProfileGroup = '.';
$GlobalChannelList = array ( "vmx-wave-00" => "vmx-wave-00" );

$GlobalTRight = [];
$GlobalTLeft = [];
$GlobalDetailOpts = [];
$GlobalProcessForm = [];
$GlobalRunQueryOpts = [];
$GlobalFormatList = [];

$GlobalDstIPFieldIndex = 8;
$GlobalBWMetricFieldIndex = 14;

/* */
function findEarliestTimeSlot($tleft,$tright) {
	global $BASEDIR;
	global $PROFILEDATADIR;
	global $POP1_PROFILE;
	global $POP1_PROFILE_SWITCH;
	global $POP1_ROUTER;

	$isoTLeft = UNIX2DISPLAY($tleft);
	$explodedISOTLeft = explode('-',$isoTLeft);
	$isoTRight = UNIX2DISPLAY($tright);
	$explodedISOTRight = explode('-',$isoTRight);

	$leftTimeSlotDir = "$PROFILEDATADIR/$POP1_PROFILE/$POP1_ROUTER/$explodedISOTLeft[0]/$explodedISOTLeft[1]/$explodedISOTLeft[2]/";
	$rightTimeSlotDir = "$PROFILEDATADIR/$POP1_PROFILE/$POP1_ROUTER/$explodedISOTRight[0]/$explodedISOTRight[1]/$explodedISOTRight[2]/";

	$get_first_file_cmd = 'DIR="' . $leftTimeSlotDir . '"; if [ -d $DIR ]; then ls -1 $DIR | head -n 1; fi';
	$cmd_result_string = shell_exec($get_first_file_cmd);
//	echo '$cmd_result_string: ' . $cmd_result_string;

	if(!is_null($cmd_result_string) && $cmd_result_string != "") {
		$tmp = explode('.',trim($cmd_result_string)); // need to strip trailing newline char
//		echo var_dump($tmp);
		$earliestTimeSlot = ISO2UNIX($tmp[1]);
//		echo '$earliestTimeSlot: ' . $earliestTimeSlot;
//		echo '$tleft: ' . $tleft;
		$earliestTimeSlot = ($tleft < $earliestTimeSlot) ? $earliestTimeSlot : $tleft;
		return $earliestTimeSlot;
	}

	$get_first_file_cmd = 'DIR="' . $rightTimeSlotDir . '"; if [ -d $DIR ]; then ls -1 $DIR | head -n 1; fi';
	$cmd_result_string = shell_exec($get_first_file_cmd);

	if(!is_null($cmd_result_string) && $cmd_result_string != "") {
		$tmp = explode('.',$cmd_result_string);
		$earliestTimeSlot = ISO2UNIX($tmp[1]);
		$earliestTimeSlot = ($tright < $earliestTimeSlot) ? $earliestTimeSlot : $tright;
		return $earliestTimeSlot;
	}

	return NULL;
}
/* */
function SetHardCodedTimeSlot ($detail_opts) {
	global $WinSizeScale;
	global $RRDoffset;
	global $GlobalTRight;
	global $GlobalTLeft;

//	$_SESSION['tright']	= mktime(0,25,0,9,21,2015); //nfcapd.201509210025
//	$_SESSION['tleft']  = mktime(9,10,0,9,17,2015); // nfcapd.201509170910

// existing 24hrs of data for vmx-wave-00 only !!
//	$GlobalTRight = mktime(0,25,0,9,21,2015); //nfcapd.201509210025
//	$GlobalTLeft = mktime(9,10,0,9,17,2015); // nfcapd.201509170910

	$tnow		 = time();
	$tnow		-= $tnow % 300;
	$GlobalTRight = $tnow; // set to most recent 300s time slot
	$GlobalTLeft = $GlobalTRight - 86400; // set to 24hr earlier
	$earliestTimeSlot = findEarliestTimeSlot($GlobalTLeft,$GlobalTRight);
//	echo '$earliestTimeSlot: ' . $earliestTimeSlot;
	if(!is_null($earliestTimeSlot)) {
		$GlobalTLeft = ($GlobalTLeft < $earliestTimeSlot) ? $earliestTimeSlot : $GlobalTLeft;
	}

} // End of SetHardCodedTimeSlot


function SetNfdumpQueryParams () {
	 global $GlobalChannelList;
	 global $GlobalDetailOpts;
	 global $GlobalProcessForm;
	 global $GlobalRunQueryOpts;
	global $OutputFormatOption;
	 global $GlobalFormatList;
	global $GlobalDstIPFieldIndex;
	global $GlobalBWMetricFieldIndex;

	 if ( !array_key_exists('query', $_GET) ) {
	    $statorder = 4; // order by bps
	    $aggr_dstip = 'checked';
	    $GlobalDstIPFieldIndex = 8;
	    $GlobalBWMetricFieldIndex = 14; // bps column
	 }
	 else {
	    if ( $_GET["query"] == 'Top10CustomersByPps' ) {
	    	    $statorder = 3; // order by pps
	    	    $aggr_dstip = 'checked';
		    $GlobalDstIPFieldIndex = 8;
		    $GlobalBWMetricFieldIndex = 13; // pps column
	    }
	    else {
	    	    $statorder = 4; // order by bps
	    	    $aggr_dstip = 'checked';
		    $GlobalDstIPFieldIndex = 8;
		    $GlobalBWMetricFieldIndex = 14; // bps column
	    }
	 }
//	 print '$_GET[query]: ' . $_GET['query'] . "\n";
//	 print '$statorder: ' . $statorder . "\n";
//	 print '$aggr_dstip: ' . $aggr_dstip . "\n";
//	 print '$GlobalDstIPFieldIndex: ' . $GlobalDstIPFieldIndex . "\n";
//	 print '$GlobalBWMetricFieldIndex: ' . $GlobalBWMetricFieldIndex . "\n";

	$detail_opts = array( 
		// type
		"type" 		=> 'flows',
		// proto
		"proto" 	=> 'any',
		// wsize
		"wsize" 	=> 1,
		// ratescale - absolute or per sec values
		"ratescale" => 1,
		// logscale - lin/log display
		"logscale" 	=> 0,
		// linegraph - line/stacked graphs
		"linegraph" => 0,
		// linegraph - line/stacked graphs
		"cursor_mode" => 1,
		// list of displayed channels in graphs
//		"channellist" => implode('!', array_keys($_SESSION['profileinfo']['channel'])),
		"channellist" => implode('!', array_keys($GlobalChannelList)),
		// 
		"statpref" => '0:0:0',
		// 
		"statvisible" => 1,
	);

//	$_SESSION['detail_opts'] = $detail_opts;
	$GlobalDetailOpts = $detail_opts;

	SetHardCodedTimeSlot($detail_opts);

	$process_form = array( 
		"modeselect" 	=> 1,
		"srcselector"	=> explode('!', $detail_opts['channellist']),
		"DefaultFilter" => -1,
		"filter"		=> NULL,
		"filter_name" 	=> '',
		"filter_edit" 	=> null,
		"filter_delete" => null,
		"listN" 		=> 0,
		"aggr_bidir" 	=> '',
		"aggr_proto" 	=> '',
		"aggr_srcip" 	=> '',
		"aggr_srcport" 	=> '',
//		"aggr_dstip" 	=> 'checked',
		"aggr_dstip" 	=> $aggr_dstip,
		"aggr_dstport" 	=> '',
		"aggr_srcselect" => 0, // 0=dstip, 1=dstip4/mask, 2=dstip6/mask
		"aggr_dstselect" => 0, // 0=dstip, 1=dstip4/mask, 2=dstip6/mask
		"aggr_srcnetbits" => 24,
		"aggr_dstnetbits" => 24,
		"timesorted" 	=> '',
		"IPv6_long" 	=> '',
		"output" 		=> 'extended',
		"customfmt" 	=> '',
		"fmt_save" 		=> '',
		"fmt_delete" 	=> '',
		// stat type inputs
		"topN" 			=> 0, // 0=10, 1=20, 2=50, 3=100, 4=200, 5=500
		"stattype" 		=> 0, // 0=flow-records
//		"statorder" 	=> 4, // 4=order by bps
		"statorder" 	=> $statorder, // 4=order by bps, 3=order by pps
		"limitoutput" 	=> '',
		"limitwhat" 	=> '',
		"limithow" 		=> 0,
		"limitsize" 	=> 0,
		"limitscale" 	=> 0,
	);

//	$_SESSION['process_form'] = $process_form;
	$GlobalProcessForm = $process_form;

/*
	if ( !array_key_exists('formatlist', $_SESSION) ) {
		foreach ( $OutputFormatOption as $opt ) {
			$_SESSION['formatlist'][$opt] = $opt;
		}
		$cmd_out =  nfsend_query("get-formatlist", array(), 0);
		if ( is_array($cmd_out) ) {
			foreach ( $cmd_out as $key => $value ) 
				$_SESSION['formatlist'][$key] = $value;
		}
		$_SESSION['formatlist']['custom ...'] = 0;
	}
/* */
		foreach ( $OutputFormatOption as $opt ) {
			$GlobalFormatList[$opt] = $opt;
		}
//		print var_dump($GlobalFormatList);
		$cmd_out =  nfsend_query("get-formatlist", array(), 0);
//		print var_dump($cmd_out);
		if ( is_array($cmd_out) ) {
			foreach ( $cmd_out as $key => $value ) 
				$GlobalFormatList[$key] = $value;
		}
		$GlobalFormatList['custom ...'] = 0;
//		print var_dump($GlobalFormatList);

	$run = CompileCommand($process_form['modeselect']);
//	print var_dump($run);

	$_SESSION['anchor'] = '#processing';

	$GlobalRunQueryOpts = $run;

} // End of SetNfdumpQueryParams


function RunProcessing() {

	global $self;
	global $ListNOption;
	global $TopNOption;
	global $OutputFormatOption;
	global $IPStatOption;
	global $IPStatOrder;
	global $LimitScale;
	 global $GlobalDetailOpts;
	 global $GlobalProcessForm;
	 global $GlobalRunQueryOpts;
	global $GlobalDstIPFieldIndex;
	global $GlobalBWMetricFieldIndex;

	global $POP1_PROFILE_SWITCH;
	global $POP1_ROUTER;
	global $POP1_ROUTER_IFINDEXES;

//	$detail_opts = $_SESSION['detail_opts'];
	$detail_opts = $GlobalDetailOpts;
//	$process_form = $_SESSION['process_form'];
	$process_form = $GlobalProcessForm;

/*
		if ( !array_key_exists('run', $_SESSION) )
			return;
/* */

//print var_dump($_SESSION);

//		print "<div class='flowlist'>\n";
//		$run = $_SESSION['run'];
		$run = $GlobalRunQueryOpts;
		if ( $run != null ) {
		     	$filterstring = "";
			$ifcount = count($POP1_ROUTER_IFINDEXES);
			$ifcounter = 0;
			foreach ( $POP1_ROUTER_IFINDEXES as $ifindex ) {
				if ( $ifcounter > 0 && $ifcounter < $ifcount ) {
				   $filterstring .= "or ";
				}
				$filterstring .= "if " . $ifindex . " ";
				$ifcounter += 1;
			}
			$filterstring = "( " . $filterstring . " )";
//			print "filterstring: " . $filterstring . "\n";
			$filter = array( $filterstring );
//			$filter = array("(in if 523 or in if 528)");
//			SetMessage('error', "filter: '$filter'");
//			ReportLog("filter: '$filter'");

			$cmd_opts['type'] = 'real';
//			$cmd_opts['profile'] = './live';
			$cmd_opts['profile'] = "$POP1_PROFILE_SWITCH";
//			print "cmd_opts[profile]: " . $cmd_opts['profile'] . "\n";

//			$cmd_opts['srcselector'] = "vmx-wave-00";
			$cmd_opts['srcselector'] = "$POP1_ROUTER";

//			print "<pre>\n";

			ClearMessages();
			$cmd_opts['args'] = "-T $run";
			$cmd_opts['filter'] = $filter;
//			print var_dump($cmd_opts);

			$cmd_out = nfsend_query("run-nfdump", $cmd_opts);
			$customer_by_bps_data = [];
				
			if ( !is_array($cmd_out) ) {
				ShowMessages();
			} else {

				if ( array_key_exists('arg', $cmd_out) ) {
//					print "** nfdump " . $cmd_out['arg'] . "\n";
				}
				if ( array_key_exists('filter', $cmd_out) ) {
//					print "nfdump filter:\n";
					foreach ( $cmd_out['filter'] as $line ) {
//						print "$line\n";
					}
				}
				$top_customers_data = "";
				$number_of_lines = count($cmd_out['nfdump']);
//				print "Number of lines: " . $number_of_lines . "\n";
				$lineCount = 0;
				foreach ( $cmd_out['nfdump'] as $line ) {
					if( $lineCount < 3 || $lineCount >= ( $number_of_lines - 4 ) ) {
					}
					else {
					     $tmpln1 = preg_replace('{(\s)\1+}', '$1', $line); // replace repeating space chars with a single space char
					     $tmpln2 = strtr($tmpln1,' ',',');
					     $tmpln3 = preg_replace('/,K/', 'K', $tmpln2);
					     $tmpln4 = preg_replace('/,M/', 'M', $tmpln3);
					     $tmpln5 = preg_replace('/,G/', 'G', $tmpln4);
					     $tmpflds = explode(',', $tmpln5);
//				 	     print var_dump($tmpflds);
//					     print '$GlobalDstIPFieldIndex: ' . $GlobalDstIPFieldIndex . "\n";
//					     print '$GlobalBWMetricFieldIndex: ' . $GlobalBWMetricFieldIndex . "\n";
//					     array_push($customer_by_bps_data, array('name' => $tmpflds[8], 'data' => array( (int) $tmpflds[14] ) ));
					     array_push($customer_by_bps_data, array('name' => $tmpflds[(int) $GlobalDstIPFieldIndex], 'data' => array( (int) $tmpflds[(int) $GlobalBWMetricFieldIndex] ) ));

					}
					$lineCount += 1;
				}
				$customer_by_bps_highcharts_series = json_encode(array('series' => $customer_by_bps_data));


			}
//			print "</pre>\n";

//			print "<script language=\"Javascript\" type=\"text/javascript\"> window.global_nxg_dashboard_array[0]=" . $customer_by_bps_highcharts_series . "; </script>";
//			print var_dump($customer_by_bps_highcharts_series);
			echo $customer_by_bps_highcharts_series;
//			print $customer_by_bps_highcharts_series;

		}
//		print "</div>\n";

	return;

} // End of RunProcessing

?>
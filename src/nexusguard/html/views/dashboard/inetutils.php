<?php

$MAX_INET_BITS = 32;
$MAX_32BIT_INT = 0xffffffff;
$MAX_8BIT_INT = 0xff;

function examineConstants(){
  echo 'PHP_INT_SIZE: ' . PHP_INT_SIZE;
}

function genNetMask($cidr_numbits) {
  global $MAX_INET_BITS;
  // echo '$MAX_INET_BITS: ' . $MAX_INET_BITS;
  global $MAX_32BIT_INT;
  // echo '$MAX_32BIT_INT: ' . $MAX_32BIT_INT;
  global $MAX_8BIT_INT;
  // echo '$MAX_8BIT_INT: ' . $MAX_8BIT_INT;

  $cidr_numbits = intval($cidr_numbits);
  if( $cidr_numbits < 0 ) {
    $cidr_numbits = intval(0);
  }
  if( $cidr_numbits > $MAX_INET_BITS ) {
    $cidr_numbits = intval($MAX_INET_BITS);
  }

  $netmask = (int) 0; // all zeros
  $netmask = ($netmask ^ $netmask); // all zeros
  $netmask = (~ $netmask); // all ones
  $netmask = ($netmask << ($MAX_INET_BITS - $cidr_numbits));
  $netmask = ($netmask & $MAX_32BIT_INT); // only LS 32bits

  return $netmask;
}

function my_inet_pton($dotNotationIPV4) {
  global $MAX_INET_BITS;
  global $MAX_32BIT_INT;
  global $MAX_8BIT_INT;

  $ipv4addr = NULL;
  // echo '$dotNotationIPV4: ' . $dotNotationIPV4;
  $octet_segments = explode('.', $dotNotationIPV4);
  // echo var_dump($octet_segments);
  if( count($octet_segments) != 4 ) {
    return $ipv4addr;
  }
  $ipv4addr = (int) 0;
  foreach($octet_segments as $octet_string) {
    $octet = intval($octet_string);
    $octet = ($octet & $MAX_8BIT_INT);
    $ipv4addr = ($ipv4addr << 4); // multiply by 2**4
    $ipv4addr = ($ipv4addr + $octet);
  }
  $ipv4addr = ($ipv4addr & $MAX_32BIT_INT); // only LS 32bits

  return $ipv4addr;
}

function my_inet_cton($cidrNotationIPV4) {
  global $MAX_INET_BITS;
  global $MAX_32BIT_INT;
  global $MAX_8BIT_INT;

  $ipv4addr = NULL;

  // $cidr_segments = explode('/', $cidrNotationIPV4);
  $cidr_segments = extractCIDRAddrAndMask($cidrNotationIPV4);
  // echo var_dump($cidr_segments);
  if( count($cidr_segments) != 2 ) {
    return $ipv4addr;
  }

  $ipv4addr = my_inet_pton($cidr_segments['address']);
  // echo '$ipv4addr: ' . $ipv4addr;
  if( is_null($ipv4addr) ) {
    return $ipv4addr;
  }
  $netmask = genNetMask( intval( $cidr_segments['netmask'] ) );
  // echo '$netmask: ' . $netmask;
  $ipv4addr = ($ipv4addr & $netmask);
  $ipv4addr = ($ipv4addr & $MAX_32BIT_INT); // only LS 32bits

  return $ipv4addr;
}

function inet_cidr_cmp($cidr_string1, $cidr_string2, $exact_match) {

  $tmp_cidr_string1 = $cidr_string1;
  $tmp_cidr_string2 = $cidr_string2;

  if( !$exact_match ) {
    $splitCIDR1 = extractCIDRAddrAndMask($cidr_string1);
    $splitCIDR2 = extractCIDRAddrAndMask($cidr_string2);
    if( array_key_exists('netmask', $splitCIDR1) && array_key_exists('netmask', $splitCIDR2) ) {
    	$min_netmask = min($splitCIDR1['netmask'], $splitCIDR2['netmask']);
	$tmp_cidr_string1 = $splitCIDR1['address'] . "/" . $min_netmask;
	$tmp_cidr_string2 = $splitCIDR2['address'] . "/" . $min_netmask;
    }
  }

  // echo '$tmp_cidr_string1: ' . $tmp_cidr_string1;
  $ipv4addr1 = my_inet_cton($tmp_cidr_string1);
  if( is_null($ipv4addr1) ) {
    return FALSE;
  }

  // echo '$tmp_cidr_string2: ' . $tmp_cidr_string2;
  $ipv4addr2 = my_inet_cton($tmp_cidr_string2);
  if( is_null($ipv4addr2) ) {
    return FALSE;
  }

  // echo '$ipv4addr1: ' . $ipv4addr1;
  // echo '$ipv4addr2: ' . $ipv4addr2;

  return ($ipv4addr1 == $ipv4addr2);
}

function extractHostAddrAndPort($dotNotationIPV4PlusPort) {
  $hostComponents = explode(":", $dotNotationIPV4PlusPort);

  if( count($hostComponents) != 2 ) {
    return $dotNotationIPV4PlusPort;
  }

  return array( 'host' => $hostComponents[0], 'port' => $hostComponents[1] );
}

function extractCIDRAddrAndMask($cidrNotationIPV4) {
  $cidrComponents = explode("/", $cidrNotationIPV4);

  if( count($cidrComponents) != 2 ) {
    return $cidrNotationIPV4;
  }

  return array( 'address' => $cidrComponents[0], 'netmask' => $cidrComponents[1] );
}

?>

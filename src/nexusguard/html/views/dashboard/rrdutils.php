<?php

class fetch_parser {


  function parse($data) 
  {
//    echo var_dump($data);
//    echo strlen($data) . "\n";

    $lines = explode("\n", $data);
    $i = 0;
    $ret = array();
    $timeInterval = array();
    foreach ($lines as $line){
      if($line != ""){
	if($i == 0){
	  $columnNames = preg_split("/[ \t]+/",$line);
	  $columnNames = array_slice($columnNames,1,count($columnNames));
	  for ($x = 0; $x < count($columnNames); $x++) {
	    $ret[$columnNames[$x]]  = array('columnNumber' => $x,
					    'values' => [], 
					    'str_values' => [] );
	  }
	}elseif ($i >= 1){
	  $tmp = explode(":",$line);
/*
	  if($i > 830 ) {
	    echo "count(tmp): " . count($tmp) . "\n";
	    echo "var_dump(tmp):\n";
	    echo var_dump($tmp);
	    echo "tmp[0]: " . $tmp[0] . "\n";
	    echo "tmp[1]: " . $tmp[1] . "\n";
	  }
/* */
	  array_push($timeInterval, $tmp[0]);
	  $data_points = explode(" ", $tmp[1]);
/*
	  if($i > 830 ) {
	    echo "tmp[1]: " . $tmp[1] . "\n";
	    echo "var_dump(\$data_points):\n";
	    echo var_dump($data_points);
	  }
/* */
	  $trunc_data_points = array_slice($data_points,1,count($data_points));
	  for ($x = 0; $x < count($trunc_data_points); $x++) {
/*
	    if($i > 830 && $x < 2) {
//	    	 echo "trunc_data_points[" . $x ."]: " . $trunc_data_points[$x] . "\n";
	    }
/* */
	    array_push($ret[$columnNames[$x]]['values'],floatval($trunc_data_points[$x]));
	    array_push($ret[$columnNames[$x]]['str_values'],$trunc_data_points[$x]);
	  }
                
	}
	$i++;
      }
    }

//    echo "count(\$columnNames): " . count($columnNames) . "\n";
    // echo var_dump($columnNames);
//    echo "count(\$timeInterval): " . count($timeInterval) . "\n";
//    echo "count(\$ret): " . count($ret) . "\n";

    return [$ret, $columnNames, $timeInterval];
  }

  function convertlistNaNtoNone($data){
    for ($x = 0; $x < count($data); $x++) {
      if(is_nan($data[$x])){
	$data[$x] = null;
      } 
    }		  
    return $data;
  }

  function negValueList( $data){
    for ($x = 0; $x < count($data); $x++) {
      if( !is_null($data[$x]) && !is_nan($data[$x])){
	$data[$x] = $data[$x] * -1;
      }
    }
    return $data;
  }

}

function my_rrd_fetch($rrd_dir, $device_hostname, $ifIndex, $startunixtime, $endunixtime) {

  /*
   * rrdtool fetch --end now --start $(date -d "2015-10-01 00:00" +%s) /opt/observium/rrd/ansible-mx5t-wave-131/port-760.rrd AVERAGE
   */
  $rrd_fetch_cmd = "rrdtool fetch" . " -s " . "$startunixtime" . " -e " . "$endunixtime" . " " . "$rrd_dir" . "/" . "$device_hostname" . "/port-" . "$ifIndex" . ".rrd AVERAGE";
  // $rrd_fetch_cmd = "rrdtool fetch" . " -s " . "$startunixtime" . " -e " . "$endunixtime" . " " . "$rrd_dir" . "/" . "$device_hostname" . "/port-" . "$ifIndex" . ".rrd LAST"; // not enough data kept around :(
  // echo var_dump($rrd_fetch_cmd);

  $outputstring = shell_exec($rrd_fetch_cmd);
  // echo var_dump($outputstring);
//  echo var_dump(strlen($outputstring));

  return $outputstring;
}

function extractIfcBWRateSamples($ifIndex, $rrd_fetch_data_string) {

  $a = new fetch_parser();
  $ret = $a->parse($rrd_fetch_data_string);
/*
  echo "count(\$ret): " . count($ret) . "\n";
  echo 'count(array_keys($ret)): ' . count(array_keys($ret)) . "\n";
  echo 'array_keys($ret):' . "\n";
  echo var_dump(array_keys($ret));
/* */
  $obj = [];
  if(count($ret[1]) > 0){
    $StartTime = $ret[2][0];
    $EndTime = $ret[2][count($ret[2])-1 ];
		  
    $ret[0]['OUTOCTETS']['values'] = $a->convertlistNaNtoNone($ret[0]['OUTOCTETS']['values']);
    $ret[0]['INOCTETS']['values'] = $a->convertlistNaNtoNone($ret[0]['INOCTETS']['values']);
		  
    // $ret[0]['OUTOCTETS']['values'] = $a->negValueList($ret[0]['OUTOCTETS']['values']); // do NOT negate values for OUTOCTETS
    $obj = array("ifDescr" => "", "ifIndex" => $ifIndex, "data" => array());
    for ($x = 0; $x < count($ret[2]); $x++) {

      $obj["data"][strval($ret[2][$x])] = array("OUTOCTETS" => $ret[0]['OUTOCTETS']['values'][$x],
						"INOCTETS" => $ret[0]['INOCTETS']['values'][$x],
						"CUSTOMERS" => array());
    }

    // $INOCTETS = json_encode($ret[0]['INOCTETS']['values']);
    // $OUTOCTETS = json_encode($ret[0]['OUTOCTETS']['values']);

  }

  return $obj;

}

/*
 * start of main section
 */

/*
  $output = shell_exec('rrdtool fetch /opt/observium/rrd/mx5t/port-13.rrd AVERAGE -s 1441931014 -e 1442017414');

  $a = new fetch_parser();

  $ret = $a->parse($output);
  if(count($ret[1]) > 0){
  $StartTime = $ret[2][0];
  $EndTime = $ret[2][count($ret[2])-1 ];
		  
  $ret[0]['OUTOCTETS']['values'] = $a->convertlistNaNtoNone($ret[0]['OUTOCTETS']['values']);
  $ret[0]['INOCTETS']['values'] = $a->convertlistNaNtoNone($ret[0]['INOCTETS']['values']);
		  
		  
  $ret[0]['OUTOCTETS']['values'] = $a->negValueList($ret[0]['OUTOCTETS']['values']);
  $obj = array("ifDescr" => "fxp0.0", "ifIndex" => 13, "data" => array());
  for ($x = 0; $x < count($ret[2]); $x++) {
  $obj["data"][strval($ret[2][$x])] = array("OUTOCTETS" => $ret[0]['OUTOCTETS']['values'][$x],
  "INOCTETS" => $ret[0]['INOCTETS']['values'][$x],
  "CUSTOMERS" => array());

  }
  $INOCTETS = json_encode($ret[0]['INOCTETS']['values']);
  $OUTOCTETS = json_encode($ret[0]['OUTOCTETS']['values']);

/* */


?>

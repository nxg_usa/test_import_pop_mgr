
<!--
<link rel="stylesheet" href="nexusguard/css/bwcosts.css"/>
<!-- -->

<div class="bwcosts-stats" id="bwcosts-stats-div">

<table class="bwcosts-stats" id="bwcosts-stats-table">

<tr>

<td>
<div class="stats-container" id="top-projected-billing-domain" style="min-width: 450px; height: 30px; margin: 0 auto"> </div>
</td>

<td>
<div class="stats-container" id="second-highest-projected-billing-domain" style="min-width: 450px; height: 30px; margin: 0 auto"> </div>
</td>

<td>
<div class="stats-container" id="third-highest-projected-billing-domain" style="min-width: 450px; height: 30px; margin: 0 auto"> </div>
</td>

</tr>

<tr>

<td>
<div class="stats-container" id="top-projected-aggregate-bw-charge" style="min-width: 450px; height: 30px; margin: 0 auto"> </div>
</td>

<td>
<div class="stats-container" id="second-highest-projected-aggregate-bw-charge" style="min-width: 450px; height: 30px; margin: 0 auto"> </div>
</td>

<td>
<div class="stats-container" id="third-highest-projected-aggregate-bw-charge" style="min-width: 450px; height: 30px; margin: 0 auto"> </div>
</td>

</tr>

<tr>

<td>
<div class="stats-container" id="top-projected-overage-time" style="min-width: 450px; height: 30px; margin: 0 auto"> </div>
</td>

<td>
<div class="stats-container" id="second-highest-projected-overage-time" style="min-width: 450px; height: 30px; margin: 0 auto"> </div>
</td>

<td>
<div class="stats-container" id="third-highest-projected-overage-time" style="min-width: 450px; height: 30px; margin: 0 auto"> </div>
</td>

</tr>

<tr>

<td>
<div class="stats-container" id="top-projected-bw-charge" style="min-width: 450px; height: 30px; margin: 0 auto"> </div>
</td>

<td>
<div class="stats-container" id="second-highest-projected-bw-charge" style="min-width: 450px; height: 30px; margin: 0 auto"> </div>
</td>

<td>
<div class="stats-container" id="third-highest-projected-bw-charge" style="min-width: 450px; height: 30px; margin: 0 auto"> </div>
</td>

</tr>

<tr>

<td>
<div class="stats-container" id="top-projected-percentile-bw-rate" style="min-width: 450px; height: 30px; margin: 0 auto"> </div>
</td>

<td>
<div class="stats-container" id="second-highest-projected-percentile-bw-rate" style="min-width: 450px; height: 30px; margin: 0 auto"> </div>
</td>

<td>
<div class="stats-container" id="third-highest-projected-percentile-bw-rate" style="min-width: 450px; height: 30px; margin: 0 auto"> </div>
</td>

</tr>

</table>

</div>

<div class="bwcosts-charts" id="bwcosts-charts-div">

<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/modules/no-data-to-display.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>

<table class="bwcosts-charts" id="bwcosts-charts-table">

<tr>

<td>
<div class="neighbor-info-container" id="top-neighbor-details-div" style="min-width: 450px; height: 70px; margin: 0 auto">
<table class="neighbor-info-container">
<tr class="neighbor-info-container"><td class="neighbor-info-container"><div class="neighbor-info-container" id="top-neighbor-local-ifl-div" style="min-width: 450px; height: 18px; margin: 0 auto; text-align: center"></div></td></tr>
<tr class="neighbor-info-container"><td class="neighbor-info-container"><div class="neighbor-info-container" id="top-neighbor-local-router-div" style="min-width: 450px; height: 18px; margin: 0 auto; text-align: center"></div></td></tr>
<tr class="neighbor-info-container"><td class="neighbor-info-container"><div class="neighbor-info-container" id="top-neighbor-remote-org-div" style="min-width: 450px; height: 18px; margin: 0 auto; text-align: center"></div></td></tr>
</table>
</div>
</td>

<td>
<div class="neighbor-info-container" id="second-highest-neighbor-details-div" style="min-width: 450px; height: 70px; margin: 0 auto">
<table class="neighbor-info-container">
<tr class="neighbor-info-container"><td class="neighbor-info-container"><div class="neighbor-info-container" id="second-highest-neighbor-local-ifl-div" style="min-width: 450px; height: 18px; margin: 0 auto; text-align: center"></div></td></tr>
<tr class="neighbor-info-container"><td class="neighbor-info-container"><div class="neighbor-info-container" id="second-highest-neighbor-local-router-div" style="min-width: 450px; height: 18px; margin: 0 auto; text-align: center"></div></td></tr>
<tr class="neighbor-info-container"><td class="neighbor-info-container"><div class="neighbor-info-container" id="second-highest-neighbor-remote-org-div" style="min-width: 450px; height: 18px; margin: 0 auto; text-align: center"></div></td></tr>
</table>
</div>
</td>

<td>
<div class="neighbor-info-container" id="third-highest-neighbor-details-div" style="min-width: 450px; height: 70px; margin: 0 auto">
<table class="neighbor-info-container">
<tr class="neighbor-info-container"><td class="neighbor-info-container"><div class="neighbor-info-container" id="third-highest-neighbor-local-ifl-div" style="min-width: 450px; height: 18px; margin: 0 auto; text-align: center"></div></td></tr>
<tr class="neighbor-info-container"><td class="neighbor-info-container"><div class="neighbor-info-container" id="third-highest-neighbor-local-router-div" style="min-width: 450px; height: 18px; margin: 0 auto; text-align: center"></div></td></tr>
<tr class="neighbor-info-container"><td class="neighbor-info-container"><div class="neighbor-info-container" id="third-highest-neighbor-remote-org-div" style="min-width: 450px; height: 18px; margin: 0 auto; text-align: center"></div></td></tr>
</table>
</div>
</td>

</tr>

<tr>

<td>
<div class="highcharts-container" id="top-customers-by-bps-div" style="min-width: 450px; height: 550px; margin: 0 auto"></div>
</td>

<td>
<div class="highcharts-container" id="top-customers-by-pps-div" style="min-width: 450px; height: 550px; margin: 0 auto"></div>
</td>

<td>
<div class="highcharts-container" id="third-highest-burst-budget-consumption-div" style="min-width: 450px; height: 550px; margin: 0 auto"></div>
</td>

</tr>

<tr>

<td>
<div class="highcharts-container" id="top-burst-participants-div" style="min-width: 450px; height: 550px; margin: 0 auto"></div>
</td>

<td>
<div class="highcharts-container" id="second-highest-burst-participants-div" style="min-width: 450px; height: 550px; margin: 0 auto"></div>
</td>

<td>
<div class="highcharts-container" id="third-highest-burst-participants-div" style="min-width: 450px; height: 550px; margin: 0 auto"></div>
</td>

</tr>

</table>

<?php

include_once("nexusguard/views/dashboard/bwcosts_conf.php"); // get URLs for AJAX queries

/* http://54.186.199.117/nfsen/bwusagequery.php?unixtime=1443753600&deviceName=ansible-mx5t&ifIndex=13&netmask=31 */
/*
$tnow		 = time();
$tnow		-= $tnow % 300;
$tnow		-= 300; // 1 aligned sample time ahead of actual latest existing netflow sample

$BW_USAGE_QUERY_URL .= "?" . "unixtime=" . $tnow . "&" . "deviceName=" . "ansible-mx5t" . "&" . "ifIndex=" . 13 . "&" . "netmask=" . 31;
/* */
?>

<script language="Javascript" type="text/javascript">

$(function () {

//      var cust_bps_query_url = "test/bwusagequery.php";
//      var cust_bps_query_url = <?php /* echo json_encode($BW_USAGE_QUERY_URL); */ ?>;
//      var cust_bps_query_url = <?php echo json_encode($CUMCONSUMEDBWBUDGET_QUERY_URL); ?>;
//	var cust_bps_query_url = <?php /* echo json_encode($CUMCONSUMEDBWBUDGET_QUERY_URL . "?neighborIndex=0&cir=4"); */ ?>;
	var cust_bps_query_url = <?php echo json_encode($CUMCONSUMEDBWBUDGET_QUERY_URL . "?priorityLevel=0"); ?>;

      $.ajax({
//	contentType: 'application/json',
//	data: JSON.stringify({ "series" : ret }),
	      dataType: 'json',
	      type: 'GET',
	      url: cust_bps_query_url,
	      success: function(data, textStatus, jqXHR) {

	      if(data.series[1] == undefined) {
	      	return;
	      }

	      var chartdatalen = data.series[1].data.length;
	      var lasttimeslot = data.series[1].data[chartdatalen-1][0];
/*
	      var lastdatapoint = data.series[1].data[chartdatalen-1];
//	      var penultimatedatapoint = data.series[1].data[chartdatalen-1-1]; // last 5mins
//	      var penultimatedatapoint = data.series[1].data[chartdatalen-1-6]; // last 30mins
	      var penultimatedatapoint = data.series[1].data[chartdatalen-1-288]; // last 24hrs
	      var projectedburstovertime = "<h2>Est. days until overage: -- days</h2>";
	      if( (lastdatapoint[0] - penultimatedatapoint[0]) != 0.0 ) {
//	      	  var rise = (lastdatapoint[1] - penultimatedatapoint[1]);
//		  var run = (lastdatapoint[0] - penultimatedatapoint[0]);
//		  projectedburstovertime = rise.toString(10) + " " + run.toString(10) + " ";
	      	  var slope = (lastdatapoint[1] - penultimatedatapoint[1])/(lastdatapoint[0] - penultimatedatapoint[0]);
		  if( slope != 0.0 ) {
		      var days = (2160 - lastdatapoint[1]) / slope;
		      if ( lastdatapoint[0] + days <= 31.0 ) {
		      	 projectedburstovertime = "<h2>Est. days until overage: " + days.toFixed(1) + " days</h2>"
		      }
		      else {
		      	 projectedburstovertime = "<h2>Est. days until overage: -- days</h2>";
		      }
		  }
		  else {
		       // keep default values
		  }
	      }
	      else {
	      	   projectedburstovertime = "<h2>Est. days until overage: 0 days</h2>"
	      }
/* */
	      var billingdomain = data.neighborDetails.billing_domain;
	      $('#top-projected-billing-domain').html("<h2>Billing Domain: " + billingdomain + "</h2>");
	      var projectedaggregatebwcharge = data.projections.projectedAggregateBWCharge;
	      $('#top-projected-aggregate-bw-charge').html("<h2>Est. aggregate charge: $" + projectedaggregatebwcharge.toFixed(2) + "</h2>");
	      // $('#top-projected-overage-time').html("<h2>5.5 days</h2>");
	      //$('#top-projected-bw-charge').html("<h2>$75.03</h2>");
	      if(data.projections.projectedBurstOverTime === null) {
	      	   var projectedburstovertime = "<h2>Est. days until overage: -- days</h2>";
	      }
	      else {
	      	   var projectedburstovertime = "<h2>Est. days until overage: " + data.projections.projectedBurstOverTime.toFixed(1) + " days</h2>";
	      }
	      $('#top-projected-overage-time').html(projectedburstovertime);
	      // var projectedbwcharge = 75.03;
	      var projectedbwcharge = data.projections.projectedBWCharges;
	      $('#top-projected-bw-charge').html("<h2>Est. billing cycle charge: $" + projectedbwcharge.toFixed(2) + "</h2>");
	      var projectedpercentilebwrate = data.projections.projectedPercentileBWRate;
	      $('#top-projected-percentile-bw-rate').html("<h2>Est. 95% Bandwidth: " + projectedpercentilebwrate.toFixed(1) + " Mbps</h2>");
	      		var local_router_device_id = data.neighborDetails.device_id;
	      		var local_ifl_port_id = data.neighborDetails.port_id;
			var local_ifl = data.neighborDetails.sub_interface_name;
			$('#top-neighbor-local-ifl-div').html("<a href=\"device/device="+local_router_device_id+"/tab=port/port="+local_ifl_port_id+"/\"><h4>"+local_ifl+"</h4></a>");
			var local_router = data.neighborDetails.hostname;
			$('#top-neighbor-local-router-div').html("<h4>"+local_router+"</h4>");
			var remote_org = data.neighborDetails.neighbor_org;
			var cir = data.costDetails.cir;
			$('#top-neighbor-remote-org-div').html("<h4>"+remote_org+" (CDR: "+cir+" bps)</h4>");

			var billing_start_date = data.costDetails.billingStartDate;
			var current_burst_budget = data.costDetails.currentBurstBudget;
	      var lastdatapoint = data.series[1].data[chartdatalen-1];
	      undefined = (function(){})();
	      var y_axis_max = (lastdatapoint[1] < 2500) ? 2500 : undefined;
		
    $('#top-customers-by-bps-div').highcharts({

        chart: {
            type: 'line'
        },
        title: {
            text: 'Burst Budget Consumption'
        },
        subtitle: {
            text: '(cumulative consumption to date)'
        },
        xAxis: {
            title: {
                text: 'Time (days)'
            },
            categories: [
                billing_start_date
            ],
            crosshair: true,
	    max: 31.0,
            plotLines: [{
                color: '#909090',
                width: 1,
		zIndex: 3,
                value: lasttimeslot,
		label: {
		       text: lasttimeslot.toFixed(1)
		}
            }],
            plotBands: [{ // mark the weekend
                color: '#efefef',
                from: 0,
                to: lasttimeslot
            }]
        },
        yAxis: {
            min: 0,
//	    max: 2500.0,
	    max: y_axis_max,
            title: {
                text: 'Burst Budget (minutes)'
            },
            plotLines: [{
                color: '#FF0000',
                width: 4,
                value: current_burst_budget,
		label: {
		       text: current_burst_budget
		}
            }]
/*
            plotBands: [{ // mark the weekend
                color: '#DCFFD5',
                from: 0,
                to: 2160
            }]
/* */
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key:.1f}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} minutes</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
	    series: {
		turboThreshold: 0
	    },
            line: {
                lineWidth: 2
            }
        },
/*
        lang: {
            noData: "No burst data found"
        },
        noData: {
            style: {
                fontWeight: 'bold',
                fontSize: '15px',
                color: '#ff00ff'
            }
        },
/* */
	series: data.series

    });

		 },
		 error: function(jqXHR, textStatus, errorThrown) {
	       // debugger
	       }
	});

      var display_customers_by_pps_chart = {
        chart: {
            type: 'line'
        },
        title: {
            text: 'Burst Budget Consumption'
        },
        subtitle: {
            text: '(cumulative consumption to date)'
        },
        xAxis: {
            title: {
                text: 'Time (days)'
            },
            categories: [
                'January 1'
            ],
            crosshair: true,
	    max: 31.0,
            plotLines: [{
                color: '#909090',
                width: 1,
		zIndex: 3,
                value: 0,
		label: {
		       text: 0
		}
            }],
            plotBands: [{ // mark the weekend
                color: '#efefef',
                from: 0.0,
                to: 0.0
            }]
        },
        yAxis: {
            min: 0,
	    max: 2500.0,
            title: {
                text: 'Burst Budget (minutes)'
            },
            plotLines: [{
                color: '#FF0000',
                width: 4,
                value: 2160,
		label: {
		       text: 2160
		}
            }]
/*
            plotBands: [{ // mark the weekend
                color: '#DCFFD5',
                from: 0,
                to: 2160
            }]
/* */
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key:.1f}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} minutes</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            line: {
                lineWidth: 2
            }
        },
        series: [{
            name: '228.39.12.65 (ABC)',
            data: [990129]

        }, {
            name: '54.78.32.117 (DEF)',
            data: [830666]

        }, {
            name: '128.96.48.54 (GHI)',
            data: [481259]

        }, {
            name: '128.96.48.55 (SDT)',
            data: [18902]

        }, {
            name: '128.96.48.56 (NXG)',
            data: [48.9]

        }, {
            name: '128.96.48.57 (BPP)',
            data: [48.9]

        }, {
            name: '128.96.48.58 (CTJ)',
            data: [48.9]

        }, {
            name: '128.96.48.59 (PLF)',
            data: [48.9]

        }, {
            name: '128.96.48.60 (JPF)',
            data: [48.9]

        }, {
            name: '75.121.73.92 (PSA)',
            data: [42.4]

        }]
    };

//      var second_highest_bursting_query_url = "test/bwusagequery.php";
//	var second_highest_bursting_query_url = "<?php /* $BW_USAGE_QUERY_URL = "test/bwusagequery.php?query=Top10CustomersByPps"; echo $BW_USAGE_QUERY_URL; */ ?>";
//	var second_highest_bursting_query_url = <?php /* include_once './bwcosts_conf.php'; echo json_encode($BW_USAGE_QUERY_URL); */ ?>;
//	var second_highest_bursting_query_url = <?php /* echo json_encode($BW_USAGE_QUERY_URL); */ ?>;
//	var second_highest_bursting_query_url = <?php /* echo json_encode($CUMCONSUMEDBWBUDGET_QUERY_URL . "?neighborIndex=1&cir=2000000"); */ ?>;
	var second_highest_bursting_query_url = <?php echo json_encode($CUMCONSUMEDBWBUDGET_QUERY_URL . "?priorityLevel=1"); ?>;

      $.ajax({
	      dataType: 'json',
	      type: 'GET',
	      url: second_highest_bursting_query_url,
	      success: function(data, textStatus, jqXHR) {

	      if(data.series[1] == undefined) {
	      	return;
	      }

	      var chartdatalen = data.series[1].data.length;
	      var lasttimeslot = data.series[1].data[chartdatalen-1][0];
			var billing_start_date = data.costDetails.billingStartDate;
	         display_customers_by_pps_chart.xAxis.categories[0] = billing_start_date;
	         display_customers_by_pps_chart.xAxis.plotLines[0].value = lasttimeslot;
	         display_customers_by_pps_chart.xAxis.plotLines[0].label.text = lasttimeslot.toFixed(1);
	         display_customers_by_pps_chart.xAxis.plotBands[0].to = lasttimeslot;
	      var lastdatapoint = data.series[1].data[chartdatalen-1];
	      undefined = (function(){})();
	      var y_axis_max = (lastdatapoint[1] < 2500) ? 2500 : undefined;
	         display_customers_by_pps_chart.yAxis.max = y_axis_max;
			var current_burst_budget = data.costDetails.currentBurstBudget;
	         display_customers_by_pps_chart.yAxis.plotLines[0].value = current_burst_budget;
	         display_customers_by_pps_chart.yAxis.plotLines[0].label.text = current_burst_budget;
/*
	      var lastdatapoint = data.series[1].data[chartdatalen-1];
//	      var penultimatedatapoint = data.series[1].data[chartdatalen-1-1]; // last 5mins
	      var penultimatedatapoint = data.series[1].data[chartdatalen-1-6]; // last 30mins
//	      var penultimatedatapoint = data.series[1].data[chartdatalen-1-288]; // last 24hrs
	      var projectedburstovertime = "<h2>Est. days until overage: -- days</h2>";
	      if( (lastdatapoint[0] - penultimatedatapoint[0]) != 0.0 ) {
	      	  var slope = (lastdatapoint[1] - penultimatedatapoint[1])/(lastdatapoint[0] - penultimatedatapoint[0]);
		  if( slope != 0.0 ) {
		      var days = (2160 - lastdatapoint[1]) / slope;
		      if ( lastdatapoint[0] + days <= 31.0 ) {
		      	 projectedburstovertime = "<h2>Est. days until overage: " + days.toFixed(1) + " days</h2>"
		      }
		      else {
		      	 projectedburstovertime = "<h2>Est. days until overage: -- days</h2>";
		      }
		  }
		  else {
		       // keep default values
		  }
	      }
	      else {
	      	   projectedburstovertime = "<h2>Est. days until overage: 0 days</h2>"
	      }
/* */
	      // $('#second-highest-projected-overage-time').html("<h2>5.5 days $75</h2>");
	      //$('#second-highest-projected-bw-charge').html("<h2>Est. billing cycle charge: $10.50</h2>");
	      var billingdomain = data.neighborDetails.billing_domain;
	      $('#second-highest-projected-billing-domain').html("<h2>Billing Domain: " + billingdomain + "</h2>");
	      var projectedaggregatebwcharge = data.projections.projectedAggregateBWCharge;
	      $('#second-highest-projected-aggregate-bw-charge').html("<h2>Est. aggregate charge: $" + projectedaggregatebwcharge.toFixed(2) + "</h2>");
	      if(data.projections.projectedBurstOverTime === null) {
	      	   var projectedburstovertime = "<h2>Est. days until overage: -- days</h2>";
	      }
	      else {
	      	   var projectedburstovertime = "<h2>Est. days until overage: " + data.projections.projectedBurstOverTime.toFixed(1) + " days</h2>";
	      }
	      $('#second-highest-projected-overage-time').html(projectedburstovertime);
	      // var projectedbwcharge = 10.50;
	      var projectedbwcharge = data.projections.projectedBWCharges;
	      $('#second-highest-projected-bw-charge').html("<h2>Est. billing cycle charge: $" + projectedbwcharge.toFixed(2) + "</h2>");
	      var projectedpercentilebwrate = data.projections.projectedPercentileBWRate;
	      $('#second-highest-projected-percentile-bw-rate').html("<h2>Est. 95% Bandwidth: " + projectedpercentilebwrate.toFixed(1) + " Mbps</h2>");

	      		var local_router_device_id = data.neighborDetails.device_id;
	      		var local_ifl_port_id = data.neighborDetails.port_id;
			var local_ifl = data.neighborDetails.sub_interface_name;
			$('#second-highest-neighbor-local-ifl-div').html("<a href=\"device/device="+local_router_device_id+"/tab=port/port="+local_ifl_port_id+"/\"><h4>"+local_ifl+"</h4></a>");
			var local_router = data.neighborDetails.hostname;
			$('#second-highest-neighbor-local-router-div').html("<h4>"+local_router+"</h4>");
			var remote_org = data.neighborDetails.neighbor_org;
			var cir = data.costDetails.cir;
			$('#second-highest-neighbor-remote-org-div').html("<h4>"+remote_org+" (CDR: "+cir+" bps)</h4>");

	      	 display_customers_by_pps_chart.series = data.series;
		 $('#top-customers-by-pps-div').highcharts(display_customers_by_pps_chart);
		 },
		 error: function(jqXHR, textStatus, errorThrown) {
		 // debugger
	         }
	});

      var third_highest_burst_budget_consumption_chart = {
        chart: {
            type: 'line'
        },
        title: {
            text: 'Burst Budget Consumption'
        },
        subtitle: {
            text: '(cumulative consumption to date)'
        },
        xAxis: {
            title: {
                text: 'Time (days)'
            },
            categories: [
                'January 1'
            ],
            crosshair: true,
	    max: 31.0,
            plotLines: [{
                color: '#909090',
                width: 1,
		zIndex: 3,
                value: 0,
		label: {
		       text: 0
		}
            }],
            plotBands: [{ // mark the weekend
                color: '#efefef',
                from: 0.0,
                to: 0.0
            }]
        },
        yAxis: {
            min: 0,
	    max: 2500.0,
            title: {
                text: 'Burst Budget (minutes)'
            },
            plotLines: [{
                color: '#FF0000',
                width: 4,
                value: 2160,
		label: {
		       text: 2160
		}
            }]
/*
            plotBands: [{ // mark the weekend
                color: '#DCFFD5',
                from: 0,
                to: 2160
            }]
/* */
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key:.1f}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} minutes</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            line: {
                lineWidth: 2
            }
        },
        series: [{
            name: '75.121.73.92 (PSA)',
            data: [42.4]

        }]
    };

//	var third_highest_bursting_query_url = <?php /* echo json_encode($CUMCONSUMEDBWBUDGET_QUERY_URL . "?neighborIndex=2&cir=30"); */ ?>;
	var third_highest_bursting_query_url = <?php echo json_encode($CUMCONSUMEDBWBUDGET_QUERY_URL . "?priorityLevel=2"); ?>;

      $.ajax({
	      dataType: 'json',
	      type: 'GET',
	      url: third_highest_bursting_query_url,
	      success: function(data, textStatus, jqXHR) {

	      if(data.series[1] == undefined) {
	      	return;
	      }

	      var chartdatalen = data.series[1].data.length;
	      var lasttimeslot = data.series[1].data[chartdatalen-1][0];
			var billing_start_date = data.costDetails.billingStartDate;
	         third_highest_burst_budget_consumption_chart.xAxis.categories[0] = billing_start_date;
	         third_highest_burst_budget_consumption_chart.xAxis.plotLines[0].value = lasttimeslot;
	         third_highest_burst_budget_consumption_chart.xAxis.plotLines[0].label.text = lasttimeslot.toFixed(1);
	         third_highest_burst_budget_consumption_chart.xAxis.plotBands[0].to = lasttimeslot;
	      var lastdatapoint = data.series[1].data[chartdatalen-1];
	      undefined = (function(){})();
	      var y_axis_max = (lastdatapoint[1] < 2500) ? 2500 : undefined;
	         third_highest_burst_budget_consumption_chart.yAxis.max = y_axis_max;
			var current_burst_budget = data.costDetails.currentBurstBudget;
	         third_highest_burst_budget_consumption_chart.yAxis.plotLines[0].value = current_burst_budget;
	         third_highest_burst_budget_consumption_chart.yAxis.plotLines[0].label.text = current_burst_budget;
/*
	      var lastdatapoint = data.series[1].data[chartdatalen-1];
//	      var penultimatedatapoint = data.series[1].data[chartdatalen-1-1]; // last 5mins
//	      var penultimatedatapoint = data.series[1].data[chartdatalen-1-6]; // last 30mins
	      var penultimatedatapoint = data.series[1].data[chartdatalen-1-288]; // last 24hrs
	      var projectedburstovertime = "<h2>Est. days until overage: -- days</h2>";
	      if( (lastdatapoint[0] - penultimatedatapoint[0]) != 0.0 ) {
	      	  var slope = (lastdatapoint[1] - penultimatedatapoint[1])/(lastdatapoint[0] - penultimatedatapoint[0]);
		  if( slope != 0.0 ) {
		      var days = (2160 - lastdatapoint[1]) / slope;
		      if ( lastdatapoint[0] + days <= 31.0 ) {
		      	 projectedburstovertime = "<h2>Est. days until overage: " + days.toFixed(1) + " days</h2>"
		      }
		      else {
		      	 projectedburstovertime = "<h2>Est. days until overage: -- days</h2>";
		      }
		  }
		  else {
		       // keep default values
		  }
	      }
	      else {
	      	   projectedburstovertime = "<h2>Est. days until overage: 0 days</h2>"
	      }
/* */
	      // $('#third-highest-projected-overage-time').html("<h2>5.5 days $75</h2>");
	      //$('#third-highest-projected-bw-charge').html("<h2>Est. billing cycle charge: $10.50</h2>");
	      var billingdomain = data.neighborDetails.billing_domain;
	      $('#third-highest-projected-billing-domain').html("<h2>Billing Domain: " + billingdomain + "</h2>");
	      var projectedaggregatebwcharge = data.projections.projectedAggregateBWCharge;
	      $('#third-highest-projected-aggregate-bw-charge').html("<h2>Est. aggregate charge: $" + projectedaggregatebwcharge.toFixed(2) + "</h2>");
	      if(data.projections.projectedBurstOverTime === null) {
	      	   var projectedburstovertime = "<h2>Est. days until overage: -- days</h2>";
	      }
	      else {
	      	   var projectedburstovertime = "<h2>Est. days until overage: " + data.projections.projectedBurstOverTime.toFixed(1) + " days</h2>";
	      }
	      $('#third-highest-projected-overage-time').html(projectedburstovertime);
	      // var projectedbwcharge = 10.50;
	      var projectedbwcharge = data.projections.projectedBWCharges;
	      $('#third-highest-projected-bw-charge').html("<h2>Est. billing cycle charge: $" + projectedbwcharge.toFixed(2) + "</h2>");
	      var projectedpercentilebwrate = data.projections.projectedPercentileBWRate;
	      $('#third-highest-projected-percentile-bw-rate').html("<h2>Est. 95% Bandwidth: " + projectedpercentilebwrate.toFixed(1) + " Mbps</h2>");

	      		var local_router_device_id = data.neighborDetails.device_id;
	      		var local_ifl_port_id = data.neighborDetails.port_id;
			var local_ifl = data.neighborDetails.sub_interface_name;
			$('#third-highest-neighbor-local-ifl-div').html("<a href=\"device/device="+local_router_device_id+"/tab=port/port="+local_ifl_port_id+"/\"><h4>"+local_ifl+"</h4></a>");
			var local_router = data.neighborDetails.hostname;
			$('#third-highest-neighbor-local-router-div').html("<h4>"+local_router+"</h4>");
			var remote_org = data.neighborDetails.neighbor_org;
			var cir = data.costDetails.cir;
			$('#third-highest-neighbor-remote-org-div').html("<h4>"+remote_org+" (CDR: "+cir+" bps)</h4>");

	      	 third_highest_burst_budget_consumption_chart.series = data.series;
		 $('#third-highest-burst-budget-consumption-div').highcharts(third_highest_burst_budget_consumption_chart);
		 },
		 error: function(jqXHR, textStatus, errorThrown) {
		 // debugger
	         }
	});


      var top_burst_accounting_chart = {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Top 10 Prefixes by Burst Budget Consumption'
        },
        subtitle: {
            text: '(cumulative consumption to date)'
        },
	legend: {
	    // labelFormat: '{name}<br/>(customer)'
            labelFormatter: function () {
	    	var customer_name = (this.options.extra.customerName === null) ? ' -- ' : this.options.extra.customerName;
                return this.name + '<br/>(' + customer_name + ')';
            }
	},
        xAxis: {
            categories: [
                'Ingress to Date',
                'Egress to Date'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            max: 100,
            title: {
                text: 'Burst Budget (percentage)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.2f}</b>({point.percentage:.0f}%)</td></tr>',
            footerFormat: '</table>',
            shared: false,
            useHTML: true
        },
        plotOptions: {
            column: {
                stacking: 'percent'
//                stacking: 'normal'
            }
        },
        lang: {
            noData: "No bursts above CIR have occurred so far"
        },
        noData: {
            style: {
                fontWeight: 'bold',
                fontSize: '15px',
                color: '#ff00ff'
            }
        },
        series: [{
            name: '75.121.73.92 (PSA)',
            data: [42.4 ]

        }]
    };
//	var top_burst_accounting_url = "nexusguard/views/dashboard/bwusageaccounting.php?neighborIndex=0";
//	var top_burst_accounting_url = <?php /* echo json_encode($BW_ACCOUNTING_QUERY_URL . "?neighborIndex=0"); */ ?>;
	var top_burst_accounting_url = <?php echo json_encode($BW_ACCOUNTING_QUERY_URL . "?priorityLevel=0"); ?>;

	// trigger corresponding burst-participants AJAX query upon completion of burst-budget-consumption AJAX call
	$(document).ajaxSuccess(function(event,xhr,settings) {
	    /* */

		// if( settings.url === top_bursting_query_url ) {
		if( settings.url === cust_bps_query_url ) {

		  $.ajax({
		    dataType: 'json',
			type: 'GET',
			url: top_burst_accounting_url,
			success: function(data, textStatus, jqXHR) {
			top_burst_accounting_chart.series = data.series;
			$('#top-burst-participants-div').highcharts(top_burst_accounting_chart);
		      },
			error: function(jqXHR, textStatus, errorThrown) {
			// debugger
		      }
		    });

		}
	    /* */
	});

      var second_highest_burst_accounting_chart = {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Top 10 Prefixes by Burst Budget Consumption'
        },
        subtitle: {
            text: '(cumulative consumption to date)'
        },
	legend: {
	    // labelFormat: '{name}<br/>(customer)'
            labelFormatter: function () {
	    	var customer_name = (this.options.extra.customerName === null) ? ' -- ' : this.options.extra.customerName;
                return this.name + '<br/>(' + customer_name + ')';
            }
	},
        xAxis: {
            categories: [
                'Ingress to Date',
                'Egress to Date'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            max: 100,
            title: {
                text: 'Burst Budget (percentage)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.2f}</b>({point.percentage:.0f}%)</td></tr>',
            footerFormat: '</table>',
            shared: false,
            useHTML: true
        },
        plotOptions: {
            column: {
                stacking: 'percent'
//                stacking: 'normal'
            }
        },
        lang: {
            noData: "No bursts above CIR have occurred so far"
        },
        noData: {
            style: {
                fontWeight: 'bold',
                fontSize: '15px',
                color: '#ff00ff'
            }
        },
        series: [{
            name: '75.121.73.92 (PSA)',
            data: [42.4 ]

        }]
    };
//	var second_highest_burst_accounting_url = "nexusguard/views/dashboard/bwusageaccounting.php?neighborIndex=1";
//	var second_highest_burst_accounting_url = <?php /* echo json_encode($BW_ACCOUNTING_QUERY_URL . "?neighborIndex=1"); */ ?>;
	var second_highest_burst_accounting_url = <?php echo json_encode($BW_ACCOUNTING_QUERY_URL . "?priorityLevel=1"); ?>;

	// trigger corresponding burst-participants AJAX query upon completion of burst-budget-consumption AJAX call
	$(document).ajaxSuccess(function(event,xhr,settings) {
	    /* */

		if( settings.url === second_highest_bursting_query_url ) {

		  $.ajax({
		    dataType: 'json',
			type: 'GET',
			url: second_highest_burst_accounting_url,
			success: function(data, textStatus, jqXHR) {
			second_highest_burst_accounting_chart.series = data.series;
			$('#second-highest-burst-participants-div').highcharts(second_highest_burst_accounting_chart);
		      },
			error: function(jqXHR, textStatus, errorThrown) {
			// debugger
		      }
		    });

		}
	    /* */
	});

      var third_highest_burst_accounting_chart = {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Top 10 Prefixes by Burst Budget Consumption'
        },
        subtitle: {
            text: '(cumulative consumption to date)'
        },
	legend: {
	    // labelFormat: '{name}<br/>(customer)'
            labelFormatter: function () {
	    	var customer_name = (this.options.extra.customerName === null) ? ' -- ' : this.options.extra.customerName;
                return this.name + '<br/>(' + customer_name + ')';
            }
	},
        xAxis: {
            categories: [
                'Ingress to Date',
                'Egress to Date'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            max: 100,
            title: {
                text: 'Burst Budget (percentage)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.2f}</b>({point.percentage:.0f}%)</td></tr>',
            footerFormat: '</table>',
            shared: false,
            useHTML: true
        },
        plotOptions: {
            column: {
                stacking: 'percent'
//                stacking: 'normal'
            }
        },
        lang: {
            noData: "No bursts above CIR have occurred so far"
        },
        noData: {
            style: {
                fontWeight: 'bold',
                fontSize: '15px',
                color: '#ff00ff'
            }
        },
        series: [{
            name: '75.121.73.92 (PSA)',
            data: [42.4 ]

        }]
    };
//	var third_highest_burst_accounting_url = "nexusguard/views/dashboard/bwusageaccounting.php?neighborIndex=2";
//	var third_highest_burst_accounting_url = <?php /* echo json_encode($BW_ACCOUNTING_QUERY_URL . "?neighborIndex=2"); */ ?>;
	var third_highest_burst_accounting_url = <?php echo json_encode($BW_ACCOUNTING_QUERY_URL . "?priorityLevel=2"); ?>;

	// trigger corresponding burst-participants AJAX query upon completion of burst-budget-consumption AJAX call
	$(document).ajaxSuccess(function(event,xhr,settings) {
	    /* */

		if( settings.url === third_highest_bursting_query_url ) {

		  $.ajax({
		    dataType: 'json',
			type: 'GET',
			url: third_highest_burst_accounting_url,
			success: function(data, textStatus, jqXHR) {
			// var burstparticipantdata = data;
			// var burstparticipantdata = JSON.stringify(data);
			// $('#third-highest-burst-participants-div').html("Burst participant data: " + burstparticipantdata);
			third_highest_burst_accounting_chart.series = data.series;
			$('#third-highest-burst-participants-div').highcharts(third_highest_burst_accounting_chart);
		      },
			error: function(jqXHR, textStatus, errorThrown) {
			// debugger
		      }
		    });

		}
	    /* */
	});


      var top_identified_cost_reform_chart = {
        chart: {
            type: 'line'
        },
        title: {
            text: 'Projected Burst Consumption'
        },
        subtitle: {
            text: '(projected cumulative consumption)'
        },
        xAxis: {
            title: {
                text: 'Time (days)'
            },
            categories: [
                'January 1'
            ],
            crosshair: true,
	    max: 31.0,
            plotLines: [{
                color: '#909090',
                width: 1,
		zIndex: 3,
                value: 0,
		label: {
		       text: 0
		}
            }],
            plotBands: [{ // mark the weekend
                color: '#efefef',
                from: 0.0,
                to: 0.0
            }]
        },
        yAxis: {
            min: 0,
	    max: 2500.0,
            title: {
                text: 'Burst Budget (minutes)'
            },
            plotLines: [{
                color: '#FF0000',
                width: 4,
                value: 2160,
		label: {
		       text: 2160
		}
            }]
/*
            plotBands: [{ // mark the weekend
                color: '#DCFFD5',
                from: 0,
                to: 2160
            }]
/* */
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key:.1f}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} minutes</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            line: {
                lineWidth: 2
            }
        },
        series: [{
            name: '75.121.73.92 (PSA)',
            data: [42.4]

        }]
    };

	var top_identified_projected_impact_query_url = <?php echo json_encode($PROJECTED_IMPACT_QUERY_URL . "?priorityLevel=0&recommendationRanking=0&flowDirection=ingress"); ?>;

	// trigger corresponding burst-participants AJAX query upon completion of recommended-action-text AJAX call
	$(document).ajaxSuccess(function(event,xhr,settings) {
	    /* */

		if( settings.url === top_identified_cost_reform_query_url ) {

      $.ajax({
	      dataType: 'json',
	      type: 'GET',
	      url: top_identified_projected_impact_query_url,
	      success: function(data, textStatus, jqXHR) {

	      if(data.series[1] == undefined || data.series[1].data == undefined) {
	      	return;
	      }

	      var chartdatalen = data.series[1].data.length;
	      var lasttimeslot = data.series[1].data[chartdatalen-1][0];
			var billing_start_date = data.costDetails.billingStartDate;
	         top_identified_cost_reform_chart.xAxis.categories[0] = billing_start_date;
	         top_identified_cost_reform_chart.xAxis.plotLines[0].value = lasttimeslot;
	         top_identified_cost_reform_chart.xAxis.plotLines[0].label.text = lasttimeslot.toFixed(1);
	         top_identified_cost_reform_chart.xAxis.plotBands[0].to = lasttimeslot;
	      var lastdatapoint = data.series[1].data[chartdatalen-1];
	      undefined = (function(){})();
	      var y_axis_max = (lastdatapoint[1] < 2500) ? 2500 : undefined;
	         top_identified_cost_reform_chart.yAxis.max = y_axis_max;
			var current_burst_budget = data.costDetails.currentBurstBudget;
	         top_identified_cost_reform_chart.yAxis.plotLines[0].value = current_burst_budget;
	         top_identified_cost_reform_chart.yAxis.plotLines[0].label.text = current_burst_budget;
	      	 top_identified_cost_reform_chart.series = data.series;
		 $('#top-identified-cost-reform-div').highcharts(top_identified_cost_reform_chart);
		 },
		 error: function(jqXHR, textStatus, errorThrown) {
		 // debugger
	         }
	});

		} // end if settings.url
	    /* */
	});


	var top_identified_cost_reform_query_url = <?php echo json_encode($FIND_BW_COST_REDUCTIONS_QUERY_URL . "?priorityLevel=0&flowDirection=ingress&recommendationRanking=0&recordLimit=10"); ?>;

	// trigger corresponding burst-participants AJAX query upon completion of burst-accounting AJAX call
	$(document).ajaxSuccess(function(event,xhr,settings) {
	    /* */

		if( settings.url === top_burst_accounting_url ) {

      $.ajax({
	      dataType: 'json',
	      type: 'GET',
	      url: top_identified_cost_reform_query_url,
	      success: function(data, textStatus, jqXHR) {
	      	 undefined = (function(){})();

		 if(data.currentOffender.prefixSummary == undefined || data.currentOffender.prefixSummary.length == 0) {
		 	return;
		 }

	      	 var offending_traffic_prefix = ( data.currentOffender.prefixSummary[0].name === null ) ? "none" : data.currentOffender.prefixSummary[0].name;
	      	 var offending_traffic_customer = ( data.currentOffender.prefixSummary[0].customerName === undefined || data.currentOffender.prefixSummary[0].customerName === null ) ? " -- " : data.currentOffender.prefixSummary[0].customerName;
		 var offending_traffic_html = "High Traffic Volumes on Ingress to: "+offending_traffic_customer+"'s prefixes: ("+offending_traffic_prefix+")<br><a href=\"\">Details</a><br>";
	      	 var current_offender = data.currentOffender;
		 var current_offender_html = "Currently via: "+current_offender.neighborDetails.sub_interface_name+" on "+current_offender.neighborDetails.hostname+" via "+current_offender.neighborDetails.neighbor_org+"<br>";
	      	 var recommended_replacements = data.recommendedReplacements[0];
		 var recommended_replacements_html = "Move traffic to: "+recommended_replacements.neighborDetails.sub_interface_name+" on "+recommended_replacements.neighborDetails.hostname+" via "+recommended_replacements.neighborDetails.neighbor_org+"<br>";
		 var tmp_html = offending_traffic_html + current_offender_html + recommended_replacements_html;
	      	 $('#most-preferred-action-text-div').html(tmp_html);
		 },
		 error: function(jqXHR, textStatus, errorThrown) {
		 // debugger
	         }
	});

		} // end if settings.url
	    /* */
	});


      var top_recommended_action_chart = {
        chart: {
            type: 'line'
        },
        title: {
            text: 'Projected Burst Consumption'
        },
        subtitle: {
            text: '(projected cumulative consumption)'
        },
        xAxis: {
            title: {
                text: 'Time (days)'
            },
            categories: [
                'January 1'
            ],
            crosshair: true,
	    max: 31.0,
            plotLines: [{
                color: '#909090',
                width: 1,
		zIndex: 3,
                value: 0,
		label: {
		       text: 0
		}
            }],
            plotBands: [{ // mark the weekend
                color: '#efefef',
                from: 0.0,
                to: 0.0
            }]
        },
        yAxis: {
            min: 0,
	    max: 2500.0,
            title: {
                text: 'Burst Budget (minutes)'
            },
            plotLines: [{
                color: '#FF0000',
                width: 4,
                value: 2160,
		label: {
		       text: 2160
		}
            }]
/*
            plotBands: [{ // mark the weekend
                color: '#DCFFD5',
                from: 0,
                to: 2160
            }]
/* */
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key:.1f}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} minutes</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            line: {
                lineWidth: 2
            }
        },
        series: [{
            name: '75.121.73.92 (PSA)',
            data: [42.4]

        }]
    };

	var top_recommended_projected_impact_query_url = <?php echo json_encode($PROJECTED_IMPACT_QUERY_URL . "?priorityLevel=0&recommendationRanking=1&flowDirection=ingress"); ?>; 

	// trigger corresponding burst-participants AJAX query upon completion of recommended-action-text AJAX call
	$(document).ajaxSuccess(function(event,xhr,settings) {
	    /* */

		if( settings.url === top_recommended_action_query_url ) {

      $.ajax({
	      dataType: 'json',
	      type: 'GET',
	      url: top_recommended_projected_impact_query_url,
	      success: function(data, textStatus, jqXHR) {

	      if(data.series[1] == undefined || data.series[1].data == undefined) {
	      	return;
	      }

	      var chartdatalen = data.series[1].data.length;
	      var lasttimeslot = data.series[1].data[chartdatalen-1][0];
			var billing_start_date = data.costDetails.billingStartDate;
	         top_recommended_action_chart.xAxis.categories[0] = billing_start_date;
	         top_recommended_action_chart.xAxis.plotLines[0].value = lasttimeslot;
	         top_recommended_action_chart.xAxis.plotLines[0].label.text = lasttimeslot.toFixed(1);
	         top_recommended_action_chart.xAxis.plotBands[0].to = lasttimeslot;
	      var lastdatapoint = data.series[1].data[chartdatalen-1];
	      undefined = (function(){})();
	      var y_axis_max = (lastdatapoint[1] < 2500) ? 2500 : undefined;
	         top_recommended_action_chart.yAxis.max = y_axis_max;
			var current_burst_budget = data.costDetails.currentBurstBudget;
	         top_recommended_action_chart.yAxis.plotLines[0].value = current_burst_budget;
	         top_recommended_action_chart.yAxis.plotLines[0].label.text = current_burst_budget;
	      	 top_recommended_action_chart.series = data.series;
		 $('#top-recommended-action-div').highcharts(top_recommended_action_chart);
		 },
		 error: function(jqXHR, textStatus, errorThrown) {
		 // debugger
	         }
	});

		} // end if settings.url
	    /* */
	});


	var top_recommended_action_query_url = <?php echo json_encode($FIND_BW_COST_REDUCTIONS_QUERY_URL . "?priorityLevel=0&flowDirection=ingress&recommendationRanking=1&recordLimit=10"); ?>;

	// trigger corresponding burst-participants AJAX query upon completion of burst-accounting AJAX call
	$(document).ajaxSuccess(function(event,xhr,settings) {
	    /* */

		if( settings.url === top_burst_accounting_url ) {

      $.ajax({
	      dataType: 'json',
	      type: 'GET',
	      url: top_recommended_action_query_url,
	      success: function(data, textStatus, jqXHR) {
	      	 undefined = (function(){})();

		 if(data.currentOffender.prefixSummary == undefined || data.currentOffender.prefixSummary.length == 0) {
		 	return;
		 }

	      	 var offending_traffic_prefix = ( data.currentOffender.prefixSummary[0].name === null ) ? "none" : data.currentOffender.prefixSummary[0].name;
	      	 var offending_traffic_customer = ( data.currentOffender.prefixSummary[0].customerName === undefined || data.currentOffender.prefixSummary[0].customerName === null ) ? " -- " : data.currentOffender.prefixSummary[0].customerName;
		 var offending_traffic_html = "High Traffic Volumes on Ingress to: "+offending_traffic_customer+"'s prefixes: ("+offending_traffic_prefix+")<br><a href=\"\">Details</a><br>";
	      	 var current_offender = data.currentOffender;
		 var current_offender_html = "Currently via: "+current_offender.neighborDetails.sub_interface_name+" on "+current_offender.neighborDetails.hostname+" via "+current_offender.neighborDetails.neighbor_org+"<br>";
	      	 var recommended_replacements = data.recommendedReplacements[0];
		 var recommended_replacements_html = "Move traffic to: "+recommended_replacements.neighborDetails.sub_interface_name+" on "+recommended_replacements.neighborDetails.hostname+" via "+recommended_replacements.neighborDetails.neighbor_org+"<br>";
		 var tmp_html = offending_traffic_html + current_offender_html + recommended_replacements_html;
	      	 $('#more-preferred-action-text-div').html(tmp_html);
		 },
		 error: function(jqXHR, textStatus, errorThrown) {
		 // debugger
	         }
	});

		} // end if settings.url
	    /* */
	});



});

</script>


<?php
$dummy_var = 0;
?>
</div>

<div class="bwcosts-actions" id="bwcosts-actions-div">

<div class="bwcosts-actions" id="bwcosts-actions-header-div" style="min-width: 450px; height: 30px; margin: 0 auto; text-align: center">
<h1>Recommended Actions</h1>
</div>

<table>

<tr>

<td>
<div class="recommended-action" id="most-preferred-action-div" style="min-width: 450px; height: 30px; margin: 0 auto; text-align: center">
<button class="recommended-action" id="most-preferred-action-button" type="button" style="min-width: 50px; width: 60%; height: 25px; margin: 0 auto; background: #50ff50">Recommended Best Action</button>
</div>


<div class="recommended-action" id="most-preferred-action-text-div" style="min-width: 450px; height: 100px; margin: 0 auto">
High Traffic Volumes on (flow-direction) to: (customer)'s prefixes: (prefixes)<br>
Currently via: (ifl) on (router) via (neighbor)<br>
Move traffic to: (ifl) on (router) via (neighbor)<br>
</div>

<div class="highcharts-container" id="top-identified-cost-reform-div" style="min-width: 450px; height: 550px; margin: 0 auto"></div>
</td>

<td>
<div class="highcharts-container" id="second-identified-cost-reform-div" style="min-width: 450px; height: 550px; margin: 0 auto"></div>
</td>

<td>
<div class="highcharts-container" id="third-identified-cost-reform-div" style="min-width: 450px; height: 550px; margin: 0 auto"></div>
</td>

</tr>

<tr>

<td>
<div class="recommended-action" id="more-preferred-action-div" style="min-width: 450px; height: 30px; margin: 0 auto; text-align: center">
<button class="recommended-action" id="more-preferred-action-button" type="button" style="min-width: 50px; width: 60%; height: 25px; margin: 0 auto; background: #50ff50">Recommended Alternative Action</button>
</div>

<div class="recommended-action" id="more-preferred-action-text-div" style="min-width: 450px; height: 100px; margin: 0 auto">
High Traffic Volumes on (flow-direction) to: (customer)'s prefixes: (prefixes)<br>
Currently via: (ifl) on (router) via (neighbor)<br>
Move traffic to: (ifl) on (router) via (neighbor)<br>
</div>

<div class="highcharts-container" id="top-recommended-action-div" style="min-width: 450px; height: 550px; margin: 0 auto"></div>
</td>

<td>
<div class="highcharts-container" id="second-recommended-action-div" style="min-width: 450px; height: 550px; margin: 0 auto"></div>
</td>

<td>
<div class="highcharts-container" id="third-recommended-action-div" style="min-width: 450px; height: 550px; margin: 0 auto"></div>
</td>

</tr>

</table>

</div>

<?php

include_once("bwcosts_conf.php");
include_once("dbutils.php");
include_once("rrdutils.php");
include_once("inetutils.php");
include_once("gputils.php");

function extractUnderlyingNeighborId($priorityLevel) {
  $neighborId = -1;

  if ( $priorityLevel >= 0 ) {
    session_id(345); // ensure AJAX calls use same session id
    $session_started = session_start( [ 'read_and_close' => 1 ] );
    $neighborIndexWithSortCriteria = $_SESSION['neighborIndexWithSortCriteria'];
    session_write_close();

    if ( count($neighborIndexWithSortCriteria) > 0 ) {
      // echo '$priorityLevel: ' . $priorityLevel;
      $neighborIndexMapEntry = array_slice($neighborIndexWithSortCriteria, $priorityLevel, 1, TRUE);
      // echo var_dump($neighborIndexMapEntry);
      // echo 'key: ' . key($neighborIndexMapEntry);
      $neighborIndex = key($neighborIndexMapEntry);
      //     echo '$neighborIndex: ' . $neighborIndex;
      // $neighborIndex = $neighborIndexMapEntry[$neighborIndex]['neighborIndex']; // error-prone -- value = offset relative to first neighbor id in DB
      $neighborIndexKey = "neighborIndex" . "$neighborIndex";
      $neighborId = $neighborIndexMapEntry[$neighborIndex]['neighborId'];
    }
  }

  // echo '$neighborId: ' . $neighborId;
  return $neighborId;
}

function findHighestBurstPrefixes($neighborId, $flowDirection, $countLimit) {

  session_id(347); // ensure AJAX calls use same session id
  $session_started = session_start( [ 'read_and_close' => 1 ] );

  $json_string = $_SESSION['burstContributionInfo'][$neighborId]['prefixSummaries'];
  $prefixSummaries = json_decode($json_string, TRUE);
  
  session_write_close();

  $index = ( $flowDirection === 'egress' ) ? 1 : 0;
  $prefixData = $prefixSummaries['series'];

  // assume prefixes are already sorted in descending order . . .
  $filteredPrefixData = array();
  $cnt = 0;
  foreach($prefixData as $prefixEntry) {
    if( $cnt >= $countLimit ) {
      break;
    }

    $filteredPrefixEntry = array();
    $filteredPrefixEntry['name'] = $prefixEntry['name'];
    $filteredPrefixEntry['data'] = $prefixEntry['data'][$index];
    array_push($filteredPrefixData, $filteredPrefixEntry);

    if($filteredPrefixEntry['data'] != 0) {
      $cnt += 1;
    }
  }

  $filteredPrefixData = mySortFunction( $filteredPrefixData, array('data' => SORT_DESC) );
  $filteredPrefixData = array_slice($filteredPrefixData, 0, $countLimit, FALSE);

  return $filteredPrefixData;
}

/*
function findPrefixAssignedCustomers($prefixList) {
  $prefixesWithAssignedCustomers = array();

  // echo var_dump($prefixList);
  // return;

  $allCustomerPrefixes = getAllCustomerPrefixes();
  // echo var_dump($allCustomerPrefixes);
  // return;

  foreach($prefixList as $prefixEntry) {
    
    foreach($allCustomerPrefixes as $customerPrefixRecord) {
      $netflowPrefix = extractHostAddrAndPort( $prefixEntry['name'] )['host'] . "/32";
      // echo '$netflowPrefix: ' . $netflowPrefix;
      $customerPrefix = $customerPrefixRecord['net_srv_prefix'];
      // echo '$customerPrefix: ' . $customerPrefix;
      $cmp_result = inet_cidr_cmp($netflowPrefix, $customerPrefix, FALSE);
      // echo '$cmp_result: ' . $cmp_result;

      //if( inet_cidr_cmp($netflowPrefix, $customerPrefix) ) {
      if( $cmp_result ) {
	$extendedPrefixEntry = array();
	$extendedPrefixEntry['name'] = $prefixEntry['name'];
	$extendedPrefixEntry['data'] = $prefixEntry['data'];
	$extendedPrefixEntry['customerId'] = $customerPrefixRecord['cust_id'];
	array_push($prefixesWithAssignedCustomers, $extendedPrefixEntry);
	
	break;
      }
    }

  }

  return $prefixesWithAssignedCustomers;
}
/* */

function findAllEligiblePOPs($customerId, $connectionType) {
}

function findCandidateSamePOPNeighborCnxns($priorityLevel) {
  $candidateNeighborCnxns = array();

  if ( $priorityLevel >= 0 ) {
    session_id(345); // ensure AJAX calls use same session id
    $session_started = session_start( [ 'read_and_close' => 1 ] );
    $neighborIndexWithSortCriteria = $_SESSION['neighborIndexWithSortCriteria'];
    session_write_close();

    if ( count($neighborIndexWithSortCriteria) > 0 ) {
      //     echo '$priorityLevel: ' . $priorityLevel;
      $neighborIndexMapEntry = array_slice($neighborIndexWithSortCriteria, $priorityLevel, 1, TRUE);
      //     echo var_dump($neighborIndexMapEntry);
      //     echo 'key: ' . key($neighborIndexMapEntry);
      $neighborIndex = key($neighborIndexMapEntry);
      //     echo '$neighborIndex: ' . $neighborIndex;
      $neighborIndexKey = "neighborIndex" . "$neighborIndex";
    }
  }

  session_id(346); // ensure AJAX calls use same session id
  $session_started = session_start( [ 'read_and_close' => 1 ] );

  $neighborDetails = $_SESSION['bwcostinfo'][$neighborIndexKey]['neighborDetails'];

  session_write_close();

  $neighborId = $neighborDetails['neighbor_id'];
  $popId = $neighborDetails['pop_details_id'];
  $deviceName = $neighborDetails['hostname'];
  $ifIndex = $neighborDetails['ifIndex'];

  $candidateNeighborCnxns = getNeighborCnxnByPOPId($popId);

  return $candidateNeighborCnxns;
}

function findMostEligibleSamePOPNeighborCnxns($candidateNeighborCnxns, $orderedSortCriteriaKeys, $offsetFromTop, $countLimit, $currentOffenderNeighborId) {
  global $CUMCONSUMEDBWBUDGET_QUERY_URL;
  global $BW_ACCOUNTING_QUERY_URL;

  // echo 'finding most eligible neighbors';
  // echo var_dump($candidateNeighborCnxns);
  $candidatePOPId = $candidateNeighborCnxns[0]['pop_details_id'];
  $allNeighborCnxns = getNeighborCnxnByPOPId($candidatePOPId);
  // echo var_dump($allNeighborCnxns);
  // return;
  $currentOffender = array();
  $neighborCnxnRankingInfo = array();  
  foreach($candidateNeighborCnxns as $candidateNeighborCnxnRecord) {
    // find index of $candidateNeighborCnxnRecord within set of all neighbor cnxns returned by DB
    $neighborIndex = 0;
    foreach($allNeighborCnxns as $neighborCnxnDBRecord) {
      if($candidateNeighborCnxnRecord['id'] == $neighborCnxnDBRecord['id']) {
	break;
      }

      $neighborIndex += 1;
    }


    /* TODO: first check for cached/persisted copy of projections from RRD data, before recalculating from scratch */
    $neighborId = $candidateNeighborCnxnRecord['id'];
    $cir = getCIR($neighborId);
    $cirUnitPrice = getCIRUnitPrice($neighborId);
//    $cum_consumed_bw_budget_query_url = $CUMCONSUMEDBWBUDGET_QUERY_URL . "?neighborIndex=" . $neighborIndex . "&" . "cir=" . $cir;
//    $cum_consumed_bw_budget_query_url = "http://54.186.199.117/" . $CUMCONSUMEDBWBUDGET_QUERY_URL . "?neighborIndex=" . $neighborIndex . "&" . "cir=" . $cir;
    $cum_consumed_bw_budget_query_url = "http://localhost/" . $CUMCONSUMEDBWBUDGET_QUERY_URL . "?neighborIndex=" . $neighborIndex . "&" . "cir=" . $cir;
    // echo '$cum_consumed_bw_budget_query_url: ' . $cum_consumed_bw_budget_query_url;
    $curl_get_cmd = '/usr/bin/curl -s -S "' . "$cum_consumed_bw_budget_query_url" . '"';
    $query_response_string = shell_exec($curl_get_cmd); // side effect of REST call is also placing this neighbor's data in $_SESSION (session_id = 345)
    // echo '$query_response_string: ' . $query_response_string;
    $projectionsFromRRDData = json_decode($query_response_string, TRUE);

    $neighborCnxnRankingInfoEntry = array();
    $neighborCnxnRankingInfoEntry['neighborDetails'] = $projectionsFromRRDData['neighborDetails'];
    $neighborCnxnRankingInfoEntry['costDetails'] = $projectionsFromRRDData['costDetails'];
    $neighborCnxnRankingInfoEntry['costDetails']['cirUnitPrice'] = $cirUnitPrice;
    $neighborCnxnRankingInfoEntry['projections'] = $projectionsFromRRDData['projections'];

    $neighborCnxnRankingInfoEntry['projectedBurstOverTime'] = $neighborCnxnRankingInfoEntry['projections']['projectedBurstOverTime'];
    $neighborCnxnRankingInfoEntry['projectedPercentileBWRate'] = $neighborCnxnRankingInfoEntry['projections']['projectedPercentileBWRate'];
    $neighborCnxnRankingInfoEntry['projectedBWCharges'] = $neighborCnxnRankingInfoEntry['projections']['projectedBWCharges'];

    /* */
    // also issue REST call to bwusageaccounting URL to also place this neighbor's prefix data in $_SESSION
    $bw_accounting_query_url = "http://localhost/" . $BW_ACCOUNTING_QUERY_URL . "?neighborIndex=" . $neighborIndex;
    // echo '$bw_accounting_query_url: ' . $bw_accounting_query_url;
    $curl_get_cmd = '/usr/bin/curl -s -S "' . "$bw_accounting_query_url" . '"';
    $query_response_string = shell_exec($curl_get_cmd); // side effect of REST call is also placing this neighbor's prefix data in $_SESSION (session_id = 347)
    // echo '$query_response_string: ' . $query_response_string;
    // $trafficCompositionSummaryData = json_decode($query_response_string, TRUE); // no need to parse, since goal is just to place prefix data in $_SESSION
    /* */
    // $neighborCnxnRankingInfoEntry['prefixSummary'] = $trafficCompositionSummaryData; // don't set here, since better prefix summary will be set again later

    array_push($neighborCnxnRankingInfo, $neighborCnxnRankingInfoEntry);
    // echo '$currentOffenderNeighborId: ' . $currentOffenderNeighborId;
    if( $neighborCnxnRankingInfoEntry['neighborDetails']['neighbor_id'] == $currentOffenderNeighborId ) {
      // echo 'found a match';
      array_push($currentOffender, $neighborCnxnRankingInfoEntry);
    }

  }

  // echo var_dump($neighborCnxnRankingInfo);
  $neighborCnxnRankingInfo = mySortFunction( $neighborCnxnRankingInfo, array('projectedBurstOverTime' => SORT_DESC, 'projectedBWCharges' => SORT_ASC, 'projectedPercentileBWRate' => SORT_ASC) );
  // echo var_dump($neighborCnxnRankingInfo);

  $mostEligibleNeighborCnxns = array();
  if( count($neighborCnxnRankingInfo) > 0 ) {
    // $mostEligibleNeighborCnxns = array_slice($neighborCnxnRankingInfo, 0, $countLimit, FALSE);
    $mostEligibleNeighborCnxns = array_slice($neighborCnxnRankingInfo, $offsetFromTop, $countLimit, FALSE);
  }

  // echo var_dump($mostEligibleNeighborCnxns);

  $currentOffenderAndRecommendedReplacement = array();
  $currentOffenderAndRecommendedReplacement['currentOffender'] = $currentOffender[0];
  $currentOffenderAndRecommendedReplacement['recommendedReplacements'] = $mostEligibleNeighborCnxns;
  // echo var_dump($currentOffenderAndRecommendedReplacement);

  return $currentOffenderAndRecommendedReplacement;
}

function findAllCandidateNeighborCnxns() {
}

function findAllMostEligibleNeighborCnxns() {
}

function findCostMinimizationOpportunities() {

  /*
   * "?priorityLevel=0&neighborId=13&flowDirection=ingress&recommendationRanking=0&recordLimit=10"
   * priorityLevel: optional, if set, searches for neighbor having this offset;
   *    default value of -1 if not set, neighborId will be obtained from neighborId parm in GET request
   * neighborId: optional, defaults value of 0 if not set and priorityLevel is not set, or priorityLevel is
   *    set and no neighbor offset is found
   * flowDirection: optional, default value of 'ingress' if not set
   * recommendationRanking: optional, default value of 0 if not set
   * recordLimit: optional, default value of 10 if not set
   */

  $priorityLevel = -1;
  if( array_key_exists('priorityLevel', $_GET) ) {
      $priorityLevel = $_GET['priorityLevel'];
  }

  $neighborId = 0;
  if( array_key_exists('neighborId', $_GET) ) {
      $neighborId = $_GET['neighborId'];
  }

  $flowDirection = 'ingress'; // 'egress' or (not 'egress')
  if( array_key_exists('flowDirection', $_GET) ) {
      $flowDirection = $_GET['flowDirection'];
  }

  $recordLimit = 10;
  if( array_key_exists('recordLimit', $_GET) ) {
      $recordLimit = $_GET['recordLimit'];
  }

  $recommendationRanking = 0; // default = highest/most preferred recommendation
  if( array_key_exists('recommendationRanking', $_GET) ) {
      $recommendationRanking = $_GET['recommendationRanking'];
  }

  // $neighborId = 13;
  // $flowDirection = 'ingress';
  // $countLimit = 10;

  if(priorityLevel >= 0) {
    $underlyingNeighborId = extractUnderlyingNeighborId($priorityLevel);
    // echo '$underlyingNeighborId: ' . $underlyingNeighborId;
    if($underlyingNeighborId >= 0) {
      $neighborId = $underlyingNeighborId; // if $priorityLevel not -1, override $neighborId from GET request if underlying neighborId not -1
    }
  }
  // echo '$neighborId: ' . $neighborId;

//  echo 'PHP_INT_SIZE: ' . PHP_INT_SIZE;

  $highestIngressPrefixes = findHighestBurstPrefixes($neighborId, $flowDirection, $recordLimit);
  // echo json_encode($highestIngressPrefixes);
  // return;


  // findHighestBurstPrefixes($neighborId, $flowDirection, $countLimit);

  $ingressPrefixesWithCustomers = findPrefixAssignedCustomers($highestIngressPrefixes);
  // echo json_encode($ingressPrefixesWithCustomers);
  // return;

  $candidateNeighborCnxns = findCandidateSamePOPNeighborCnxns($priorityLevel);
  // echo json_encode($candidateNeighborCnxns);
//  return;

  // TODO: get $countLimit value from GET request
  $offsetFromTop = $recommendationRanking;
  $countLimit = 1;
  $currentOffenderNeighborId = $neighborId;
  $currentOffenderAndRecommendedReplacement = findMostEligibleSamePOPNeighborCnxns($candidateNeighborCnxns, $orderedSortCriteriaKeys, $offsetFromTop, $countLimit, $currentOffenderNeighborId);

  $currentOffenderAndRecommendedReplacement['currentOffender']['prefixSummary'] = $ingressPrefixesWithCustomers; // add highest bw prefixes, usage proportions, and associated customers

  $replacementNeighborId = $currentOffenderAndRecommendedReplacement['recommendedReplacements'][0]['neighborDetails']['neighbor_id'];
  $highestIngressPrefixes = findHighestBurstPrefixes($replacementNeighborId, $flowDirection, $recordLimit);
  // echo json_encode($highestIngressPrefixes);
  // return;
  $ingressPrefixesWithCustomers = findPrefixAssignedCustomers($highestIngressPrefixes);
  // echo json_encode($ingressPrefixesWithCustomers);
  // return;
  $currentOffenderAndRecommendedReplacement['recommendedReplacements'][0]['prefixSummary'] = $ingressPrefixesWithCustomers; // add highest bw prefixes, usage proportions, and associated customers

  $json_string = json_encode($currentOffenderAndRecommendedReplacement);

  session_id(348); // ensure AJAX calls use same session id
  session_start( [ 'lazy_write' => 1 ] );

  // $_SESSION['systemRecommendations'] = $json_string;
  if (!array_key_exists('systemRecommendations', $_SESSION) ) {
    $_SESSION['systemRecommendations'] = array();
  }
  $compositeKey = "priorityLevel=$priorityLevel&recommendationRanking=$recommendationRanking&flowDirection=$flowDirection";
  $_SESSION['systemRecommendations'][$compositeKey] = $json_string;

  session_write_close();

  // echo json_encode($currentOffenderAndRecommendedReplacement);
  echo $json_string;
}

/*
 * main starts here
 */

findCostMinimizationOpportunities();

?>

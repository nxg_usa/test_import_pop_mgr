<?php
/* 
configuration specific to bwcosts.php functionality
*/

$ORDERED_SORT_CRITERIA_KEYS = array();

$DEVICE_NAME_MAP = [ 'observium2nfdump' => [ 'ansible-mx5' => 'device_25', 'vmx-wave-113' => 'vmx-wave-00' ], 'nfdump2observium' => [ 'device_25' => 'ansible-mx5', 'vmx-wave-00' => 'vmx-wave-113' ] ];

$BW_USAGE_QUERY_URL = "nfsen/bwusagequery.php";
//$BW_USAGE_QUERY_URL = "http://54.186.199.117/test/bwusagequery.php";

$CUMCONSUMEDBWBUDGET_QUERY_URL = "nexusguard/views/dashboard/costestimation.php";

$CUMCONSUMEDBWBUDGETCHARTDATA = []; // global to store BW budget consumption related info

$BW_ACCOUNTING_QUERY_URL = "nexusguard/views/dashboard/bwusageaccounting.php";

$FIND_BW_COST_REDUCTIONS_QUERY_URL = "nexusguard/views/dashboard/findcostreductions.php";

$PROJECTED_IMPACT_QUERY_URL = "nexusguard/views/dashboard/projectedimpact.php";

?>

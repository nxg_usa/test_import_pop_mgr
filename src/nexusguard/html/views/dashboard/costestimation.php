<?php

include_once("bwcosts_conf.php");
include_once("gputils.php");
include_once("dbutils.php");
include_once("rrdutils.php");

/*
 * key constants and parameters
 */

$BW_COST_PARMS = array();
$BW_COST_PARMS['SAMPLING_INTERVAL'] = 300; // default = 300s sampling intervals
$BW_COST_PARMS['PERCENTILE_KEEP_FRACTION'] = 0.95; // default = 95th percentile burstable billing
$BW_COST_PARMS['CURRENT_BILLING_PERIOD_BURST_ALLOWANCE'] = 2160; // minutes; default = 432 samples * 5 mins/sample
$BW_COST_PARMS['MAX_DAYS_PER_MONTH'] = 31; // max number of days in any month
$BW_COST_PARMS['BILLING_PERIOD_START_DAY'] = 1; // default to 1st day of the month
$BW_COST_PARMS['CURRENT_MONTH'] = 1; // default to January
$BW_COST_PARMS['CURRENT_YEAR'] = 2015; // default to 2015

// echo "Hello World!\n";

function genCumConsumedBWBudgetChartData() {
  global $BW_COST_PARMS;
  global $CUMCONSUMEDBWBUDGETCHARTDATA;

  $priorityLevel = -1;
  if( array_key_exists('priorityLevel', $_GET) ) {
      $priorityLevel = $_GET['priorityLevel'];
  }

  $neighborIndex = 0;
  if( array_key_exists('neighborIndex', $_GET) ) {
      $neighborIndex = $_GET['neighborIndex'];
  }

  $cir = 36;
  if( array_key_exists('cir', $_GET) ) {
      $cir = $_GET['cir'];
  }

  $BW_COST_PARMS['aggregateCharges'] = array();
  $BW_COST_PARMS['BILLING_PERIOD_START_DAY'] = 1;
  $current_date = getdate(); // default timestamp is time() -- now
  $BW_COST_PARMS['CURRENT_MONTH'] = $current_date['mon'];
  $BW_COST_PARMS['CURRENT_YEAR'] = $current_date['year'];
  $startunixtime = (mktime(0, 0, 0, $BW_COST_PARMS['CURRENT_MONTH'], $BW_COST_PARMS['BILLING_PERIOD_START_DAY'], $BW_COST_PARMS['CURRENT_YEAR']) - $BW_COST_PARMS['SAMPLING_INTERVAL']); // beginning of the month - 300s to ensure sample at exactly midnight (00:00) is included
  $totalBillingIntervalSampleSize = getTotalNumSamplesInBillingInterval(getCurrentMonthLenInSeconds(), $BW_COST_PARMS['SAMPLING_INTERVAL']);
  $percentileSampleInfo = getPercentileSampleSizes($totalBillingIntervalSampleSize, $BW_COST_PARMS['PERCENTILE_KEEP_FRACTION']);
  $BW_COST_PARMS['CURRENT_BILLING_PERIOD_BURST_ALLOWANCE'] = convertSecondsToMinutes( convertNumSamplesToSeconds($percentileSampleInfo['discardSampleSize'], $BW_COST_PARMS['SAMPLING_INTERVAL']) );

  $neighborIndexWithSortCriteria = array(); // will order neighborIndexes by estimated burst-over values
  $neighborDetails = array();
  $costDetails = array();
  $costDetails['billingStartDate'] = $current_date['month'] . " " . $BW_COST_PARMS['BILLING_PERIOD_START_DAY'];
  $costDetails['currentBurstBudget'] = $BW_COST_PARMS['CURRENT_BILLING_PERIOD_BURST_ALLOWANCE'];

  // echo var_dump($BW_COST_PARMS);
  $denormalizedNeighborConnectionInfo = getRequiredNeighborConnectionInfo();
  // echo var_dump($denormalizedNeighborConnectionInfo);
  // return;

  if( priorityLevel >= 0 ) {
    /* quickly prescreen all neighbor connections/sessions */
    /* check cached data held in $_SESSION first, before doing full calculations on any particular neighbor connection */
    /* if nothing cached in $_SESSION, quickly decide priority ordering of all neighbors based on each one's projectedBurstOverTime */
    $rrd_dir = "/opt/observium/rrd";
    $estimatedBurstOverTimes = array();
    $neighborCnxnIndex = 0;
    foreach ($denormalizedNeighborConnectionInfo as $neighborCnxnInfo) {

      $device_hostname = $neighborCnxnInfo['hostname'];
      // echo 'hostname: ' . $device_hostname;
      $ifIndex = $neighborCnxnInfo['ifIndex'];
      // echo 'ifIndex: ' . $ifIndex;
      // move before and outside of if block: $startunixtime = (mktime(0, 0, 0, 10, 1, 2015) - 300); // beginning of the month - 300s to ensure sample at exactly midnight (00:00) is included
      $neighbor_id = $neighborCnxnInfo['id'];
      $costDetails['billing_domain'] = $neighborCnxnInfo['billing_domain'];
      $billing_period_start_day = getNeighborCnxnByNeighborId($neighbor_id);
      if(!is_null($billing_period_start_day) && count($billing_period_start_day) > 0) {
	$BW_COST_PARMS['BILLING_PERIOD_START_DAY'] = $billing_period_start_day[0]['billing_start_date'];
      }
      $costDetails['billingStartDate'] = $current_date['month'] . " " . $BW_COST_PARMS['BILLING_PERIOD_START_DAY'];
      $startunixtime = (mktime(0, 0, 0, $BW_COST_PARMS['CURRENT_MONTH'], $BW_COST_PARMS['BILLING_PERIOD_START_DAY'], $BW_COST_PARMS['CURRENT_YEAR']) - $BW_COST_PARMS['SAMPLING_INTERVAL']); // have to put back inside foreach loop because of potentially different start days . . .
      $endunixtime = time(); // now

      $rrd_fetch_data = my_rrd_fetch($rrd_dir, $device_hostname, $ifIndex, $startunixtime, $endunixtime);
      // echo 'count($rrd_fetch_data): ' . count($rrd_fetch_data);
      $bwratesamples = extractIfcBWRateSamples($ifIndex, $rrd_fetch_data);
      // echo var_dump($bwratesamples);
      // return;

      $sampletimeslots = array_keys($bwratesamples['data']);
      $mintimeslot = min($sampletimeslots);
      $maxtimeslot = max($sampletimeslots);

      // echo 'count($bwratesamples): ' . count($bwratesamples);
      // $neighbor_id = $neighborCnxnInfo['id']; // moved a few lines earlier
      $cirFromDB = getCIR($neighbor_id); // ignore any cir value specified in $_GET
      $cirUnitPriceFromDB = getCIRUnitPrice($neighbor_id);
      $eirUnitPriceFromDB = getEIRUnitPrice($neighbor_id);

      $cumconsumedbwbudgettodate = array();
      $bwratesInDescendingOrder = array();
      $burstovercount = 0;
      while (list($key, $value) = each($bwratesamples['data'])) {
	$maxbwrate = max( (((double) 8.0) * $value['INOCTETS']), (((double) 8.0) * $value['OUTOCTETS']) );
	array_push($bwratesInDescendingOrder, (double) $maxbwrate);

	if( $maxbwrate > $cirFromDB ) {
	  // array_push($burstovertimes, $key); // keep saved burst-over timeslots as unix times
	  $burstovercount += 1;
	}
	// $cumbudgetconsumed = 5 * $burstovercount; // budget mins consumed = (samples over CIR) * 5mins
	$cumbudgetconsumed = convertSecondsToMinutes($BW_COST_PARMS['SAMPLING_INTERVAL']) * $burstovercount; // budget mins consumed = (samples over CIR) * 5mins
	// $cumconsumedbwbudgetchartdata[$key] = array();
	// array_push($cumconsumedbwbudgetchartdata[$key], $cumbudgetconsumed);
	$timevalue = ((double) $key - (double) $mintimeslot) / (double) 86400; // convert seconds to days
	$xydatapoint = array();
	array_push($xydatapoint, $timevalue);
	array_push($xydatapoint, $cumbudgetconsumed);
	array_push($cumconsumedbwbudgettodate, $xydatapoint);
      }
      rsort($bwratesInDescendingOrder, SORT_NUMERIC);

      $estimatedBurstOverInfo = array();
      $datalen = count($cumconsumedbwbudgettodate);
      // echo '$datalen: ' . $datalen;
      $lastdatapoint = $cumconsumedbwbudgettodate[$datalen-1];
      // echo var_dump($lastdatapoint);
      $estimatedBurstOverInfo['lastDataPoint'] = $lastdatapoint;
      $lasttimeslot = $lastdatapoint[0];
      // echo var_dump($lastdatapoint);
      // $num_samples = convertSecondsToNumSamples((5 * 60), $BW_COST_PARMS['SAMPLING_INTERVAL']);
      // $penultimatedatapoint = $cumconsumedbwbudgettodate[$datalen-1-$num_samples]; // last 5mins
      // $num_samples = convertSecondsToNumSamples((30 * 60), $BW_COST_PARMS['SAMPLING_INTERVAL']);
      // $penultimatedatapoint = $cumconsumedbwbudgettodate[$datalen-1-$num_samples]; // last 30mins
      $num_samples = convertSecondsToNumSamples((24 * 60 * 60), $BW_COST_PARMS['SAMPLING_INTERVAL']);
      $penultimatedatapoint = $cumconsumedbwbudgettodate[$datalen-1-$num_samples]; // last 24hrs
      // echo var_dump($penultimatedatapoint);
      $estimatedBurstOverInfo['penultimateDataPoint'] = $penultimatedatapoint;
      $projectedBurstOverTime = NULL;
      if( ($lastdatapoint[0] - $penultimatedatapoint[0]) != 0.0 ) {
	$slope = ((double) ($lastdatapoint[1] - $penultimatedatapoint[1]))/((double) ($lastdatapoint[0] - $penultimatedatapoint[0]));
	// echo '$slope: ' . $slope;
	if( $slope != 0.0 ) {
	  // $deltadays = ((double) (2160.0 - $lastdatapoint[1])) / ((double) $slope);
	  $deltadays = ((double) ($BW_COST_PARMS['CURRENT_BILLING_PERIOD_BURST_ALLOWANCE'] - $lastdatapoint[1])) / ((double) $slope);
	  // echo '$deltadays: ' . $deltadays;
	  $firstdatapoint = $cumconsumedbwbudgettodate[0];
	  if ( $deltadays < ( $firstdatapoint[0] - $lastdatapoint[0] ) ) {
	    $deltadays = ( $firstdatapoint[0] - $lastdatapoint[0] ); // limit negative excursion into before this billing cycle . . .
	  }
	  // echo '$lastdatapoint[0]: ' . $lastdatapoint[0];
	  if ( $lastdatapoint[0] + $deltadays <= $BW_COST_PARMS['MAX_DAYS_PER_MONTH'] ) {
	    $projectedBurstOverTime = $deltadays;
	  }
	  else {
	    $projectedBurstOverTime = NULL; // positive excursion after this billing cycle is effectively +INF , , ,
	  }
	}
	else {
	  // keep default values
	}
      }
      else {
	$projectedBurstOverTime = (double) 0.0;
      }
      //	  echo '$projectedBurstOverTime: ' . $projectedBurstOverTime;
      if ( $projectedBurstOverTime === NULL ) {
	$projectedBurstOverTime = INF; // convert NULL-valued burst-over time to +INF -- if NULL, will never burst-over . . .
      }
      //	  echo '$projectedBurstOverTime: ' . $projectedBurstOverTime;

      $estimatedBurstOverInfo['projectedBurstOverTime'] = $projectedBurstOverTime;
      $estimatedBurstOverInfo['neighborIndex'] = $neighborCnxnIndex;
      array_push($estimatedBurstOverTimes, $estimatedBurstOverInfo);

      $sampleCount = count($bwratesInDescendingOrder);
//      echo '$sampleCount: ' . $sampleCount . "\n";
      $estimated95thpercentilebwrate = $bwratesInDescendingOrder[ intval( floor( $percentileSampleInfo['discardFraction'] * $sampleCount ) ) ];

      $estimated95thpercentilebwrate /= 1000000.0; // convert bps to Mbps
    
      /* assumed $2.00/Mbps/month CIR rate, $10.00/Mbps/month burst-over rate, across the board . . . */
      $projectedbwcharges = 0;
      if($burstovercount <= $percentileSampleInfo['discardSampleSize']) { // <= 432 * 5mins = 2160mins (for 30day billing interval)
	$projectedbwcharges = ((double) $cirUnitPriceFromDB) * ((double) $estimated95thpercentilebwrate);
      }
      else {
	$projectedbwcharges = ((double) $eirUnitPriceFromDB) * ((double) $estimated95thpercentilebwrate);
      }

      if(!array_key_exists($neighborCnxnInfo['billing_domain'], $BW_COST_PARMS['aggregateCharges'])) {
	$BW_COST_PARMS['aggregateCharges'][$neighborCnxnInfo['billing_domain']] = array();
      }
      if(!array_key_exists('projectedAggregateBWCharge', $BW_COST_PARMS['aggregateCharges'][$neighborCnxnInfo['billing_domain']])) {
	$BW_COST_PARMS['aggregateCharges'][$neighborCnxnInfo['billing_domain']]['projectedAggregateBWCharge'] = (double) 0;
      }
      $BW_COST_PARMS['aggregateCharges'][$neighborCnxnInfo['billing_domain']]['projectedAggregateBWCharge'] += (double) $projectedbwcharges;

      $neighborIndexMapEntry = array();
      $neighborIndexMapEntry['neighborIndex'] = $neighborCnxnIndex;
      // $neighborIndexMapEntry['neighborId'] = $neighborCnxnInfo['id'];
      $neighborIndexMapEntry['neighborId'] = $neighbor_id;
      $neighborIndexMapEntry['projectedBurstOverTime'] = $projectedBurstOverTime;
      $neighborIndexMapEntry['projectedBWCharges'] = $projectedbwcharges;
      $neighborIndexMapEntry['billingDomain'] = $neighborCnxnInfo['billing_domain'];
      $neighborIndexMapEntry['projectedAggregateBWCharge'] = $BW_COST_PARMS['aggregateCharges'][$neighborCnxnInfo['billing_domain']]['projectedAggregateBWCharge'];
      array_push($neighborIndexWithSortCriteria, $neighborIndexMapEntry);
  
      $neighborCnxnIndex += 1;
    }
    // echo var_dump($neighborIndexWithSortCriteria);
    //  asort($neighborIndexWithSortCriteria, SORT_NUMERIC); // sort estimated times to burstover in ascending order -- negative times before positive times
    // array_msort($neighborIndexWithSortCriteria, array('projectedBurstOverTime'=>SORT_ASC, 'projectedBWCharges'=>SORT_DESC)); // !! DOESN'T WORK !!!
    $neighborIndexWithSortCriteria = mySortFunction($neighborIndexWithSortCriteria, array('projectedBurstOverTime'=>SORT_ASC, 'projectedBWCharges'=>SORT_DESC) );
    // echo var_dump($neighborIndexWithSortCriteria);

    session_id(345); // ensure AJAX calls use same session id
    session_start( [ 'lazy_write' => 1 ] );
    $_SESSION['neighborIndexWithSortCriteria'] = $neighborIndexWithSortCriteria;
    session_write_close();

    /* */
    if ( $priorityLevel >= 0 && count($neighborIndexWithSortCriteria) > 0 ) {
      //     echo '$priorityLevel: ' . $priorityLevel;
      $neighborIndexMapEntry = array_slice($neighborIndexWithSortCriteria, $priorityLevel, 1, TRUE);
      //     echo var_dump($neighborIndexMapEntry);
      //     echo 'key: ' . key($neighborIndexMapEntry);
      $neighborIndex = key($neighborIndexMapEntry);
      // $neighborIndex = $neighborIndexMapEntry['neighborIndex'];
      // echo '$neighborIndex: ' . $neighborIndex;
      $priorityLevel = -1;
    }
    /* */
  } // end if priorityLevel

  /* analyze a single, specified neighbor connection/session */
  if ( $priorityLevel < 0 && count($denormalizedNeighborConnectionInfo) > 0 ) {
    $rrd_dir = "/opt/observium/rrd"; // TODO: move this into a global in rrdutils.php
    $neighborIndexKey = "neighborIndex" . "$neighborIndex";

//    $device_hostname = $denormalizedNeighborConnectionInfo[0]['hostname'];
    $device_hostname = $denormalizedNeighborConnectionInfo[$neighborIndex]['hostname'];
    $neighborDetails['hostname'] = $device_hostname;
    $neighborDetails['device_id'] = $denormalizedNeighborConnectionInfo[$neighborIndex]['pop_edge_router_device_id'];
    $neighborDetails['neighbor_id'] = $denormalizedNeighborConnectionInfo[$neighborIndex]['id'];
    $neighborDetails['neighbor_org'] = $denormalizedNeighborConnectionInfo[$neighborIndex]['name'];
    $neighborDetails['pop_details_id'] = $denormalizedNeighborConnectionInfo[$neighborIndex]['pop_details_id'];
    $neighborDetails['billing_domain'] = $denormalizedNeighborConnectionInfo[$neighborIndex]['billing_domain'];
//    $ifIndex = $denormalizedNeighborConnectionInfo[0]['ifIndex'];
    $ifIndex = $denormalizedNeighborConnectionInfo[$neighborIndex]['ifIndex'];
    $neighborDetails['ifIndex'] = $ifIndex;
    $neighborDetails['offnet_interface'] = $denormalizedNeighborConnectionInfo[$neighborIndex]['offnet_interface'];
    $neighborDetails['sub_interface_name'] = $denormalizedNeighborConnectionInfo[$neighborIndex]['sub_interface_name'];
    $neighborDetails['port_id'] = $denormalizedNeighborConnectionInfo[$neighborIndex]['port_id'];
    $costDetails['billing_domain'] = $denormalizedNeighborConnectionInfo[$neighborIndex]['billing_domain'];
    //  $startunixtime = mktime(0, 0, 0, 10, 1, 2015); // beginning of the month
    // already set at beginning of function -- $startunixtime = (mktime(0, 0, 0, 10, 1, 2015) - 300); // beginning of the month - 300s to ensure sample at exactly midnight (00:00) is included
    $billing_period_start_day = getNeighborCnxnByNeighborId($neighborDetails['neighbor_id']);
    if(!is_null($billing_period_start_day) && count($billing_period_start_day) > 0) {
      $BW_COST_PARMS['BILLING_PERIOD_START_DAY'] = $billing_period_start_day[0]['billing_start_date'];
    }
    $costDetails['billingStartDate'] = $current_date['month'] . " " . $BW_COST_PARMS['BILLING_PERIOD_START_DAY'];
    $startunixtime = (mktime(0, 0, 0, $BW_COST_PARMS['CURRENT_MONTH'], $BW_COST_PARMS['BILLING_PERIOD_START_DAY'], $BW_COST_PARMS['CURRENT_YEAR']) - $BW_COST_PARMS['SAMPLING_INTERVAL']); // have to put back inside foreach loop because of potentially different start days . . .
    $endunixtime = time(); // now

    $rrd_fetch_data = my_rrd_fetch($rrd_dir, $device_hostname, $ifIndex, $startunixtime, $endunixtime);
    $bwratesamples = extractIfcBWRateSamples($ifIndex, $rrd_fetch_data);

    $sampletimeslots = array_keys($bwratesamples['data']);
    $mintimeslot = min($sampletimeslots);
    $maxtimeslot = max($sampletimeslots);

    $cumconsumedbwbudgetchartdata = array();
    array_push($cumconsumedbwbudgetchartdata, array());
    $cumconsumedbwbudgetchartdata[0]['name'] = 'Optimal Cumulative Burst Budget Consumption';
    // $cumconsumedbwbudgetchartdata[0]['data'] = [ [0.0, 0.0], [30.0, 2160.0] ];
    $cumconsumedbwbudgetchartdata[0]['data'] = [ [0.0, 0.0], [ convertSecondsToDays( getCurrentMonthLenInSeconds() ), $BW_COST_PARMS['CURRENT_BILLING_PERIOD_BURST_ALLOWANCE'] ] ];
    $cumconsumedbwbudgetchartdata[0]['color'] = '#000000';
    
    array_push($cumconsumedbwbudgetchartdata, array());
    $cumconsumedbwbudgetchartdata[1]['name'] = 'Cumulative Consumed Burst Budget';
    $cumconsumedbwbudgetchartdata[1]['color'] = '#0000cc';
    $cumconsumedbwbudgetchartdata[1]['data'] = array();
    $burstovercount = 0;
    $burstovertimes = array(); // store burst-over times for use later in determine customer/prefix (proportion) of responsibility for burst-overs to date . . .

//    echo '$cir: ' . $cir;
    $cirFromDB = getCIR($denormalizedNeighborConnectionInfo[$neighborIndex]['id']); // redundant call to DB -- value already fetched earlier . . .
//    echo '$cirFromDB: ' . $cirFromDB;
//    $cir = (double) 36; // bps
    $cirUnitPriceFromDB = getCIRUnitPrice($denormalizedNeighborConnectionInfo[$neighborIndex]['id']);
    $eirUnitPriceFromDB = getEIRUnitPrice($denormalizedNeighborConnectionInfo[$neighborIndex]['id']);
      $costDetails['cir'] = $cirFromDB;

    $overallminbwrate = INF;
    $overallmaxbwrate = -INF;
    $bwratesInDescendingOrder = array();
    while (list($key, $value) = each($bwratesamples['data'])) {
      $maxbwrate = max( (((double) 8.0) * $value['INOCTETS']), (((double) 8.0) * $value['OUTOCTETS']) );
      array_push($bwratesInDescendingOrder, (double) $maxbwrate);
      $overallminbwrate = min($overallminbwrate, $maxbwrate); // eventually find overall min bw sample value
      $overallmaxbwrate = max($overallmaxbwrate, $maxbwrate); // eventually find overall max bw sample value
      if( $maxbwrate > $cirFromDB ) {
      	array_push($burstovertimes, $key); // keep saved burst-over timeslots as unix times
	$burstovercount += 1;
      }
      $cumbudgetconsumed = 5 * $burstovercount; // budget mins consumed = (samples over CIR) * 5mins
      // $cumconsumedbwbudgetchartdata[$key] = array();
      // array_push($cumconsumedbwbudgetchartdata[$key], $cumbudgetconsumed);
      $timevalue = ((double) $key - (double) $mintimeslot) / (double) 86400; // convert seconds to days
      $xydatapoint = array();
      array_push($xydatapoint, $timevalue);
      array_push($xydatapoint, $cumbudgetconsumed);
      array_push($cumconsumedbwbudgetchartdata[1]['data'], $xydatapoint);
    }
    rsort($bwratesInDescendingOrder, SORT_NUMERIC);

    $projections = array();
    /* project days until overage, and bw charges for this billing period, estimated 95th percentile bw rate . . . */
    // echo var_dump($cumconsumedbwbudgetchartdata[1]['data']);
    $chartdatalen = count($cumconsumedbwbudgetchartdata[1]['data']);
    // echo '$chartdatalen: ' . $chartdatalen;
    $lastdatapoint = $cumconsumedbwbudgetchartdata[1]['data'][$chartdatalen-1];
    $lasttimeslot = $lastdatapoint[0];
    // $penultimatedatapoint = $cumconsumedbwbudgetchartdata[1].['data'][$chartdatalen-1-1]; // last 5mins
    // $penultimatedatapoint = $cumconsumedbwbudgetchartdata[1].['data'][$chartdatalen-1-6]; // last 30mins
    $penultimatedatapoint = $cumconsumedbwbudgetchartdata[1]['data'][$chartdatalen-1-288]; // last 24hrs
    $projectedBurstOverTime = NULL;
    if( ($lastdatapoint[0] - $penultimatedatapoint[0]) != 0.0 ) {
      // $rise = ($lastdatapoint[1] - $penultimatedatapoint[1]);
      // $run = ($lastdatapoint[0] - $penultimatedatapoint[0]);
      // $projectedBurstOverTime = $rise . " " . $run . " ";
      $slope = ((double) ($lastdatapoint[1] - $penultimatedatapoint[1]))/((double) ($lastdatapoint[0] - $penultimatedatapoint[0]));
      if( $slope != 0.0 ) {
	// $deltadays = ((double) (2160.0 - $lastdatapoint[1])) / ((double) $slope);
	$deltadays = ((double) ($BW_COST_PARMS['CURRENT_BILLING_PERIOD_BURST_ALLOWANCE'] - $lastdatapoint[1])) / ((double) $slope);
	if ( $lastdatapoint[0] + $deltadays <= $BW_COST_PARMS['MAX_DAYS_PER_MONTH'] ) {
	  $projectedBurstOverTime = $deltadays;
	    }
	else {
	  $projectedBurstOverTime = NULL;
	}
      }
      else {
	// keep default values
      }
    }
    else {
      $projectedBurstOverTime = (double) 0.0;
    }
    $projections['projectedBurstOverTime'] = $projectedBurstOverTime;

    /* for projected bw charges, assume uniform distribution of bw rates from observed min rate to observed max rate to date */
    //$estimated95thpercentilebwrate = ($overallminbwrate + 0.95 * ($overallmaxbwrate - $overallminbwrate)) / 1000000.0; // convert bw rate from bps to Mbps
    /* better method for tracking/estimating 95th percentile bwrate . . . */
    $sampleCount = count($bwratesInDescendingOrder);
//    echo '$sampleCount: ' . $sampleCount . "\n";
/* too abrupt/discontinuous . . .
    if( $sampleCount < (8640 - 864) ) {
    // if( $sampleCount < 433 ) {
    	// not enough bwrate samples yet . . .
	$estimated95thpercentilebwrate = $bwratesInDescendingOrder[ intval( floor( 0.05 * $sampleCount ) ) ];	
    }
    else {
	$estimated95thpercentilebwrate = $bwratesInDescendingOrder[ (433 - 1) ];	// get 433rd highest bwrate value
    }
/* */
    $estimated95thpercentilebwrate = $bwratesInDescendingOrder[ intval( floor( $percentileSampleInfo['discardFraction'] * $sampleCount ) ) ];

    $estimated95thpercentilebwrate /= 1000000.0; // convert bps to Mbps
    $projections['projectedPercentileBWRate'] = $estimated95thpercentilebwrate;
    
    /* assumed $2.00/Mbps/month CIR rate, $10.00/Mbps/month burst-over rate, across the board . . . */
    $projectedbwcharges = 0;
    if($burstovercount <= $percentileSampleInfo['discardSampleSize']) { // <= 432 * 5mins = 2160mins (for 30day billing interval)
      $projectedbwcharges = ((double) $cirUnitPriceFromDB) * ((double) $estimated95thpercentilebwrate);
    }
    else {
      $projectedbwcharges = ((double) $eirUnitPriceFromDB) * ((double) $estimated95thpercentilebwrate);
    }
    $projections['projectedBWCharges'] = $projectedbwcharges;
    $projections['billingDomain'] = $denormalizedNeighborConnectionInfo[$neighborIndex]['billing_domain'];
    $projections['projectedAggregateBWCharge'] = $BW_COST_PARMS['aggregateCharges'][$denormalizedNeighborConnectionInfo[$neighborIndex]['billing_domain']]['projectedAggregateBWCharge'];

    // echo 'count($burstovertimes): ' . count($burstovertimes) . "\n";
    //    echo 'count($cumconsumedbwbudgetchartdata): ' . count($cumconsumedbwbudgetchartdata) . "\n";
    //    echo var_dump($cumconsumedbwbudgetchartdata);
/*
    $CUMCONSUMEDBWBUDGETCHARTDATA[$neighborIndex] = array();
    $CUMCONSUMEDBWBUDGETCHARTDATA[$neighborIndex]['neighborDetails'] = $neighborDetails;
    $CUMCONSUMEDBWBUDGETCHARTDATA[$neighborIndex]['burstovertimes'] = $burstovertimes;
    $CUMCONSUMEDBWBUDGETCHARTDATA[$neighborIndex]['cumbwbudgetconsumed'] = array();
    $CUMCONSUMEDBWBUDGETCHARTDATA[$neighborIndex]['cumbwbudgetconsumed']['series'] = $cumconsumedbwbudgetchartdata;
    // $CUMCONSUMEDBWBUDGETCHARTDATA[$neighborIndex]['cumbwbudgetconsumed']['series'][1]['data'] = array_slice($cumconsumedbwbudgetchartdata[1]['data'],340,15,FALSE);
    echo json_encode($CUMCONSUMEDBWBUDGETCHARTDATA[$neighborIndex]['cumbwbudgetconsumed']);
/* */
//    session_name('OBSID'); // ensure AJAX calls use same session as authenticated GUI pages . . .
//    session_name('BWCOSTS'); // ensure AJAX calls use same session name
    session_id(346); // ensure AJAX calls use same session id
    session_start( [ 'lazy_write' => 1 ] );

    if (!array_key_exists('bwcostinfo', $_SESSION) ) {
       $_SESSION['bwcostinfo'] = array();
    }
    if (!array_key_exists($neighborIndexKey, $_SESSION['bwcostinfo']) ) {
       $_SESSION['bwcostinfo'][$neighborIndexKey] = array();
    }
    $_SESSION['bwcostinfo'][$neighborIndexKey]['neighborDetails'] = $neighborDetails;
//    $_SESSION['bwcostinfo'][$neighborIndexKey]['burstovertimes'] = $burstovertimes;
    $_SESSION['bwcostinfo'][$neighborIndexKey]['burstovertimes'] = json_encode($burstovertimes);

/* -- don't strictly need to save into session or global, so don't save it . . .
    $_SESSION['bwcostinfo'][$neighborIndexKey]['cumbwbudgetconsumed'] = array();
    $_SESSION['bwcostinfo'][$neighborIndexKey]['cumbwbudgetconsumed']['series'] = $cumconsumedbwbudgetchartdata;
    // $_SESSION['bwcostinfo'][$neighborIndexKey]['cumbwbudgetconsumed']['series'][1]['data'] = array_slice($cumconsumedbwbudgetchartdata[1]['data'],340,15,FALSE);
    session_write_close();
    echo json_encode($_SESSION['bwcostinfo'][$neighborIndexKey]['cumbwbudgetconsumed']);
/* */

//    echo var_dump(serialize($_SESSION));
//    return;

    session_write_close();
    $cumbwbudgetconsumed = array();
    $cumbwbudgetconsumed['neighborDetails'] = $neighborDetails;
    $cumbwbudgetconsumed['costDetails'] = $costDetails;
    $cumbwbudgetconsumed['projections'] = $projections;
    $cumbwbudgetconsumed['series'] = $cumconsumedbwbudgetchartdata;
//    echo var_dump(serialize($cumbwbudgetconsumed));
    echo json_encode($cumbwbudgetconsumed);
    return;
  }

  echo json_encode( array( 'neighborDetails' => NULL, 'costDetails' => NULL, 'projections' => NULL, 'series' => [ array( 'name' => 'empty', 'data' => [] ) ] ) ); // return no data when no neighbor connections are found . . .
}


/*
 * main starts here
 */

/*

$denormalizedNeighborConnectionInfo = getRequiredNeighborConnectionInfo();
//echo var_dump($denormalizedNeighborConnectionInfo);
//return;

if ( count($denormalizedNeighborConnectionInfo) > 0 ) {
  $rrd_dir = "/opt/observium/rrd";
  $device_hostname = $denormalizedNeighborConnectionInfo[0]['hostname'];
  $ifIndex = $denormalizedNeighborConnectionInfo[0]['ifIndex'];
//  $startunixtime = mktime(0, 0, 0, 10, 1, 2015); // beginning of the month
  $startunixtime = (mktime(0, 0, 0, 10, 1, 2015) - 300); // beginning of the month - 300s to ensure sample at exactly midnight (00:00) is included
  $endunixtime = time(); // now

  $rrd_fetch_data = my_rrd_fetch($rrd_dir, $device_hostname, $ifIndex, $startunixtime, $endunixtime);
  $bwratesamples = extractIfcBWRateSamples($ifIndex, $rrd_fetch_data);

  // echo var_dump($bwratesamples);
//  echo count($bwratesamples) . "\n";
//  echo var_dump(array_keys($bwratesamples));
//  echo var_dump($bwratesamples['ifDescr']);
//  echo var_dump($bwratesamples['ifIndex']);
//  echo count($bwratesamples['data']) . "\n";
//  echo var_dump($bwratesamples['data']);
//  echo var_dump(array_slice($bwratesamples['data'],870,2));
  $sampletimeslots = array_keys($bwratesamples['data']);
//  echo count($sampletimeslots) . "\n";
  $mintimeslot = min($sampletimeslots);
  $maxtimeslot = max($sampletimeslots);
//  echo "min key: " . $mintimeslot . "\n";
//  echo "max key: " . $maxtimeslot . "\n";
//  echo count($bwratesamples['data'][strval($minsampletimeslot)]) . "\n";
//  echo $bwratesamples['data'][strval($minsampletimeslot)]['OUTOCTETS'] . "\n";
//  echo count($bwratesamples['data'][strval($maxsampletimeslot - 600)]) . "\n";
//  echo $bwratesamples['data'][strval($maxsampletimeslot - 600)]['OUTOCTETS'] . "\n";
//  echo count($bwratesamples['data'][strval($maxsampletimeslot - 300)]) . "\n";
//  echo $bwratesamples['data'][strval($maxsampletimeslot - 300)]['OUTOCTETS'] . "\n";
//  echo count($bwratesamples['data'][strval($maxsampletimeslot)]) . "\n";
//  echo $bwratesamples['data'][strval($maxsampletimeslot)]['OUTOCTETS'] . "\n";

//  $insum = 0;
//  $outsum = 0;
  $cumconsumedbwbudgetchartdata = array();
  $burstovercount = 0;
  $cir = (double) 10; // bps
  while (list($key, $value) = each($bwratesamples['data'])) {
//  	$outsum += $value['OUTOCTETS'];
//  	$insum += $value['INOCTETS'];
	$maxbwrate = max( (((double) 8.0) * $value['INOCTETS']), (((double) 8.0) * $value['OUTOCTETS']) );
	if( $maxbwrate > $cir ) {
	    $burstovercount += 1;
	}
	$cumbudgetconsumed = 5 * $burstovercount; // budget mins consumed = (samples over CIR) * 5mins
	$cumconsumedbwbudgetchartdata[$key] = $cumbudgetconsumed;
	
  }

//  echo '$insum: ' . $insum . "\n";
//  echo '$outsum: ' . $outsum . "\n";

//    echo 'count($cumconsumedbwbudgetchartdata): ' . count($cumconsumedbwbudgetchartdata) . "\n";
//    echo var_dump($cumconsumedbwbudgetchartdata);
}

/* */

genCumConsumedBWBudgetChartData();

?>

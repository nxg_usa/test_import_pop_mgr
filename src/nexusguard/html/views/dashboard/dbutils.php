<?php

include_once("bwcosts_conf.php");
include_once("/opt/observium/includes/defaults.inc.php");
include_once("/opt/observium/config.php");
include_once("/opt/observium/includes/definitions.inc.php");

function array_msort($array, $cols)
{
    $colarr = array();
    foreach ($cols as $col => $order) {
        $colarr[$col] = array();
        foreach ($array as $k => $row) { $colarr[$col]['_'.$k] = strtolower($row[$col]); }
    }
    $eval = 'array_multisort(';
    foreach ($cols as $col => $order) {
        $eval .= '$colarr[\''.$col.'\'],'.$order.',';
    }
    $eval = substr($eval,0,-1).');';
    eval($eval);
    $ret = array();
    foreach ($colarr as $col => $arr) {
        foreach ($arr as $k => $v) {
            $k = substr($k,1);
            if (!isset($ret[$k])) $ret[$k] = $array[$k];
            $ret[$k][$col] = $array[$k][$col];
        }
    }
    return $ret;

}

function myComparisonFunction($arg1, $arg2) {
  global $ORDERED_SORT_CRITERIA_KEYS;
  // echo var_dump($ORDERED_SORT_CRITERIA_KEYS);
  $orderedSortCriteriaKeys = $ORDERED_SORT_CRITERIA_KEYS;
  // echo var_dump($orderedSortCriteriaKeys);

  $numSortCriteria = count($orderedSortCriteriaKeys);
  // echo '$numSortCriteria: ' . $numSortCriteria . "";
  if ( $numSortCriteria < 1 ) {
    return 0;
  }

  $indx = 0;
  foreach($orderedSortCriteriaKeys as $sortkey => $dirvalue) {
  // while( list($sortkey,$dirvalue) = each($orderedSortCriteriaKeys) ) {
    // echo "\$orderedSortCriteriaKeys[$sortkey]: => $dirvalue\n";
    // echo var_dump($arg1);
    // echo var_dump($arg2);
    // echo "\$arg1[\$sortkey]: $arg1[$sortkey]\n";
    // echo "\$arg2[\$sortkey]: $arg2[$sortkey]\n";

    if ( $arg1[$sortkey] < $arg2[$sortkey] ) {
      $retval = ($dirvalue == SORT_ASC) ? -1 : 1;
      // echo "\$retval: $retval\n";
      return $retval;
    }
    if ( $arg1[$sortkey] > $arg2[$sortkey] ) {
      $retval = ($dirvalue == SORT_ASC) ? 1 : -1;
      return $retval;
    }
    if ( $arg1[$sortkey] === $arg2[$sortkey] ) {
      if ( $indx < ($numSortCriteria - 1) ) {
	$indx += 1;
	continue;
      }
      else {
	return 0;
      }
    }

  }

}

function mySortFunction($targetArray, $sortCriteriaKeys) {
  global $ORDERED_SORT_CRITERIA_KEYS;

  $ORDERED_SORT_CRITERIA_KEYS = $sortCriteriaKeys;

  uasort($targetArray, 'myComparisonFunction');

  return $targetArray;
}

function getAllNeighborConnections () {

  $all_neighbors = array();

  foreach (dbFetchRows("select * from nxg_isp_details;") as $neighbor_record)
    {
      array_push($all_neighbors, $neighbor_record);
    }

//  echo var_dump($all_neighbors);
  return $all_neighbors;
}

function getAllNeighborConnectionsByPOPId ($popId) {

  $all_neighbors = array();

  foreach (dbFetchRows( "select * from nxg_isp_details where pop_details_id = ?;", array($popId) ) as $neighbor_record)
    {
      array_push($all_neighbors, $neighbor_record);
    }

//  echo var_dump($all_neighbors);
  return $all_neighbors;
}

function getRequiredNeighborConnectionInfo () {

  $all_neighbors = array();
  /*
  //$query = "select n.*, r.pop_name, r.pop_edge_router_device_id, d.hostname, p.ifIndex from nxg_isp_details as n, nxg_pop_details as r, devices as d, ports as p where n.pop_details_id = r.id and n.offnet_interface = p.ifDescr and r.pop_edge_router_device_id = d.device_id and p.device_id = d.device_id;";
  //$query = "select n.*, r.pop_name, r.pop_edge_router_device_id, d.hostname, p.ifIndex from nxg_isp_details as n, nxg_pop_details as r, devices as d, ports as p inner join n on n.pop_details_id = r.id inner join n on n.offnet_interface = p.ifDescr inner join r on r.pop_edge_router_device_id = d.device_id inner join d on d.device_id = p.device_id;";
  //$query = "select n.*, r.pop_name, r.pop_edge_router_device_id, d.hostname, p.ifIndex from nxg_isp_details as n inner join nxg_pop_details as r on n.pop_details_id = r.id inner join ports as p on n.offnet_interface = p.ifDescr inner join devices as d on r.pop_edge_router_device_id = d.device_id inner join ports as p on d.device_id = p.device_id;";
  /* */
  //$query = "select n.id, n.name, r.pop_name, r.pop_edge_router_device_id from nxg_isp_details as n inner join nxg_pop_details as r on n.pop_details_id = r.id;"; // works!!
  //$query = "select n.*, r.pop_name, r.pop_edge_router_device_id from nxg_isp_details as n inner join nxg_pop_details as r on n.pop_details_id = r.id;"; // works!!
  //$query = "select n.*, r.pop_name, r.pop_edge_router_device_id, d.hostname from nxg_isp_details as n inner join nxg_pop_details as r on n.pop_details_id = r.id inner join devices as d on r.pop_edge_router_device_id = d.device_id;"; // works!!
  //$query = "select distinct n.*, r.pop_name, r.pop_edge_router_device_id, d.hostname, pp.ifIndex from nxg_isp_details as n inner join nxg_pop_details as r on n.pop_details_id = r.id inner join ports as p on r.pop_edge_router_device_id = p.device_id inner join ports as pp on n.offnet_interface = pp.ifDescr inner join devices as d on r.pop_edge_router_device_id = d.device_id;"; // empty if no offnet_interfaces selected from actual ports.ifDescr . . .
  //$query = "select distinct n.*, r.pop_name, r.pop_edge_router_device_id, d.hostname, pp.ifIndex from nxg_isp_details as n inner join nxg_pop_details as r on n.pop_details_id = r.id inner join ports as p on r.pop_edge_router_device_id = p.device_id inner join ports as pp on n.sub_interface_name = pp.ifDescr inner join devices as d on r.pop_edge_router_device_id = d.device_id;"; // empty if no sub_interface_name selected from actual ports.ifDescr . . .
  //$query = "select distinct n.*, r.pop_name, r.pop_edge_router_device_id, d.hostname, p.ifIndex from nxg_isp_details as n inner join nxg_pop_details as r on n.pop_details_id = r.id inner join ports as p on r.pop_edge_router_device_id = p.device_id and n.sub_interface_name = p.ifDescr inner join devices as d on r.pop_edge_router_device_id = d.device_id;"; // empty if no sub_interface_name selected from actual ports.ifDescr . . .
  $query = "select distinct n.*, i.name, i.pop_details_id, i.billing_domain, r.pop_name, r.pop_edge_router_device_id, d.hostname, p.ifIndex, p.port_id from nxg_bgp_details as n inner join nxg_isp_details as i on n.isp_id = i.id inner join nxg_pop_details as r on i.pop_details_id = r.id inner join ports as p on r.pop_edge_router_device_id = p.device_id and n.sub_interface_name = p.ifDescr inner join devices as d on r.pop_edge_router_device_id = d.device_id;"; // empty if no sub_interface_name selected from actual ports.ifDescr . . .

  foreach (dbFetchRows($query) as $neighbor_record)
    {
      array_push($all_neighbors, $neighbor_record);
    }

  // echo var_dump($all_neighbors);
  // echo count($all_neighbors) . "\n";

  return $all_neighbors;

}

function getAllNeighborCnxns() {
  $matching_db_records = array();

  foreach ( dbFetchRows( "select * from nxg_isp_details;" ) as $db_record )
    {
      array_push($matching_db_records, $db_record);
    }

  return $matching_db_records;
}

function getNeighborCnxnByNeighborId($neighborId) {
  $matching_db_records = array();

  foreach ( dbFetchRows( "select * from nxg_isp_details as n where n.id = ?;", array($neighborId) ) as $db_record )
    {
      array_push($matching_db_records, $db_record);
    }

  return $matching_db_records;
}

function getNeighborCnxnByPOPId($popId) {
  $matching_db_records = array();

  foreach ( dbFetchRows( "select * from nxg_isp_details as n where n.pop_details_id = ?;", array($popId) ) as $db_record )
    {
      array_push($matching_db_records, $db_record);
    }

  return $matching_db_records;
}

function getNeighborCnxnByPOPIdAndIfcName($popId, $subInterfaceName) {
  $matching_db_records = array();

  foreach ( dbFetchRows( "select * from nxg_isp_details as n where n.pop_details_id = ? and n.sub_interface_name = ?;", array($popId, $subInterfaceName) ) as $db_record )
    {
      array_push($matching_db_records, $db_record);
    }

  return $matching_db_records;
}

function getAllPOPs () {

  $all_pops = array();

  foreach (dbFetchRows("select * from nxg_pop_details;") as $pop_record)
    {
      array_push($all_pops, $pop_record);
    }

//  echo var_dump($all_pops);

}

function getAllDevices () {

  $all_devices = array();

  foreach (dbFetchRows("select * from devices;") as $device_record)
    {
      array_push($all_devices, $device_record);
    }

//  echo var_dump($all_devices);

}

function getAllPorts () {

  $all_ports = array();

  foreach (dbFetchRows("select * from ports;") as $port_record)
    {
      array_push($all_ports, $port_record);
    }

  //echo var_dump($all_ports);
//  echo count($all_ports) . "\n";

}

function getIfIndex ($device_id, $port_id) {

  $matching_ports = array();

  /* select device_id, port_id, ifDescr, ifName, ifIndex from ports; */
  foreach (dbFetchRows("select p.device_id, p.port_id, p.ifDescr, p.ifName, p.ifIndex from ports as p where p.device_id=? and p.port_id=?;", array($device_id, $port_id) ) as $port_record)
    {
      array_push($matching_ports, $port_record);
    }

  $ifIndex = -1;
  if ( count($matching_ports) > 0 ) {
    $ifIndex = $matching_ports[0]['ifIndex'];
  }
//  echo var_dump($ifIndex);
  return $ifIndex;

}

function getCIR($neighbor_id) {

//  echo '$neighbor_id: ' . $neighbor_id;

  $matching_cost_structs = array();

  foreach (dbFetchRows("select c.committed_information_rate, c.committed_information_cost from nxg_isp_cost_details as c where c.isp_id=?;", array($neighbor_id) ) as $neighbor_price_record)
    {
      array_push($matching_cost_structs, $neighbor_price_record);
    }

  $cir = -1;
  if ( count($matching_cost_structs) > 0 ) {
    $cir = $matching_cost_structs[0]['committed_information_rate'];
  }
//  echo var_dump($cir);
  return $cir;
}

function getCIRUnitPrice($neighbor_id) {

  $matching_cost_structs = array();

  foreach (dbFetchRows("select c.committed_information_rate, c.committed_information_cost from nxg_isp_cost_details as c where c.isp_id=?;", array($neighbor_id) ) as $neighbor_price_record)
    {
      array_push($matching_cost_structs, $neighbor_price_record);
    }

  $cirUnitPrice = -1;
  if ( count($matching_cost_structs) > 0 ) {
    $cirUnitPrice = $matching_cost_structs[0]['committed_information_cost'];
  }
//  echo var_dump($cirUnitPrice);
  return $cirUnitPrice;
}

function getEIRUnitPrice($neighbor_id) {

  $matching_cost_structs = array();

  foreach (dbFetchRows("select c.committed_information_rate, c.committed_information_cost from nxg_isp_cost_details as c where c.isp_id=?;", array($neighbor_id) ) as $neighbor_price_record)
    {
      array_push($matching_cost_structs, $neighbor_price_record);
    }

  $eirUnitPrice = -1;
  if ( count($matching_cost_structs) > 0 ) {
    $eirUnitPrice = $matching_cost_structs[0]['committed_information_cost'];
  }
//  echo var_dump($eirUnitPrice);
  return $eirUnitPrice;
}

function insertPOPs($new_pops) {

  $new_id = dbInsert($new_pops, 'nxg_pop_details');
  return $new_id;
}

function insertNeighbors($new_neighbors) {

  $new_id = dbInsert($new_neighbors, 'nxg_isp_details');
  return $new_id;
}

function insertNeighborCostStructures($new_costs) {

  $new_id = dbInsert($new_costs, 'nxg_isp_cost_details');
  return $new_id;
}

function getAllCustomers() {
  $matching_db_records = array();

  foreach ( dbFetchRows("select * from nxg_customer;") as $db_record )
    {
      array_push($matching_db_records, $db_record);
    }

  return $matching_db_records;
}

function getSpecifiedCustomersById($customerIdList) {
  $matching_db_records = array();
  $whereClause = " where";
  $queryParms = array();
  $queryTerminator = ";";
  $queryString = "select * from nxg_customer as c";

  if( count($customerIdList) < 1 ) {
    return $matching_db_records;
  }

  $cnt = 0;
  foreach($customerIdList as $customerId) {
    if( $cnt > 0 ) {
      $whereClause .= " or";
    }
    $whereClause .= " c.customer_id=?";

    $cnt += 1;
  }
  $queryString = $queryString . $whereClause . $queryTerminator;

  foreach ( dbFetchRows( $queryString, $queryParms ) as $db_record )
    {
      array_push($matching_db_records, $db_record);
    }

  return $matching_db_records;
}

function getAllCustomerPrefixes() {
  $matching_db_records = array();

  foreach ( dbFetchRows("select * from nxg_customer_net_service;") as $db_record )
    {
      array_push($matching_db_records, $db_record);
    }

  return $matching_db_records;
}

function getAllCustomerPrefixesWithCustomerInfo() {
  $matching_db_records = array();

  foreach ( dbFetchRows("select s.net_srv_prefix, s.cust_id, c.name, c.ossid from nxg_customer_net_service as s inner join nxg_customer as c on s.cust_id = c.customer_id;") as $db_record )
    {
      array_push($matching_db_records, $db_record);
    }

  return $matching_db_records;
}

function getSpecifiedCustomerPrefixesByCustomerId() {
  $matching_db_records = array();
  $whereClause = " where";
  $queryParms = array();
  $queryTerminator = ";";
  $queryString = "select * from nxg_customer_net_service as p";

  if( count($customerIdList) < 1 ) {
    return $matching_db_records;
  }

  $cnt = 0;
  foreach($customerIdList as $customerId) {
    if( $cnt > 0 ) {
      $whereClause .= " or";
    }
    $whereClause .= " c.cust_id=?";

    $cnt += 1;
  }
  $queryString = $queryString . $whereClause . $queryTerminator;

  foreach ( dbFetchRows( $queryString, $queryParms ) as $db_record )
    {
      array_push($matching_db_records, $db_record);
    }

  return $matching_db_records;
}

/*
function findPrefixAssignedCustomers($prefixList) {

  /*
   * prefixList format: [{"name": "", "data": ""}, {"name": "", "data": ""}, . . . {"name": "", "data": ""}]
   **

  include_once("inetutils.php");
  // echo 'Hello1';
  $prefixesWithAssignedCustomers = array();

  // echo var_dump($prefixList);
  // return;

  // $allCustomerPrefixes = getAllCustomerPrefixes();
  $allCustomerPrefixes = getAllCustomerPrefixesWithCustomerInfo();
  // echo 'Hello2';
  // echo var_dump($allCustomerPrefixes);
  // return;

  foreach($prefixList as $prefixEntry) {
    
    foreach($allCustomerPrefixes as $customerPrefixRecord) {
      $netflowPrefix = extractHostAddrAndPort( $prefixEntry['name'] )['host'] . "/32";
      echo '$netflowPrefix: ' . $netflowPrefix;
      $customerPrefix = $customerPrefixRecord['net_srv_prefix'];
      echo '$customerPrefix: ' . $customerPrefix;
      $cmp_result = inet_cidr_cmp($netflowPrefix, $customerPrefix, FALSE);
      echo '$cmp_result: ' . $cmp_result;

      //if( inet_cidr_cmp($netflowPrefix, $customerPrefix) ) {
      if( $cmp_result ) {
	$extendedPrefixEntry = array();
	$extendedPrefixEntry['name'] = $prefixEntry['name'];
	$extendedPrefixEntry['data'] = $prefixEntry['data'];
	$extendedPrefixEntry['customerId'] = $customerPrefixRecord['cust_id'];
	$extendedPrefixEntry['customerOSSId'] = $customerPrefixRecord['ossid'];
	$extendedPrefixEntry['customerName'] = $customerPrefixRecord['name'];
	array_push($prefixesWithAssignedCustomers, $extendedPrefixEntry);
	
	break;
      }
    }

    break;
  }

  return $prefixesWithAssignedCustomers;
}
/* */

function getAllCustomerCnxnsByNeighborId($neighborId) {
  $matching_db_records = array();

  foreach ( dbFetchRows( "select cc.conn_id, cc.cust_id, cc.conn_loc, cc.pop_id, i.id, i.name, i.sub_interface_name from nxg_customer_conn as cc inner join nxg_isp_details as i on cc.pop_id = i.pop_details_id where i.id = ?;", array($neighborId) ) as $db_record )
    {
      array_push($matching_db_records, $db_record);
    }

  return $matching_db_records;
}

function getAllSamePOPNeigborCnxnsByCustomerId($customerId) {
  $matching_db_records = array();

  foreach ( dbFetchRows( "select cc.conn_id, cc.cust_id, cc.conn_loc, cc.pop_id, i.id, i.name, i.sub_interface_name from nxg_customer_conn as cc inner join nxg_isp_details as i on cc.pop_id = i.pop_details_id where cc.cust_id = ?;", array($customerId) ) as $db_record )
    {
      array_push($matching_db_records, $db_record);
    }

  return $matching_db_records;
}

function getOtherSamePOPNeighborCnxnsByCustomerId($customerId, $neighborId) {
  $matching_db_records = array();

  foreach ( dbFetchRows( "select cc.conn_id, cc.cust_id, cc.conn_loc, cc.pop_id, i.id, i.name, i.sub_interface_name from nxg_customer_conn as cc inner join nxg_isp_details as i on cc.pop_id = i.pop_details_id where cc.cust_id = ? and i.id <> ?;", array($customerId,$neighborId) ) as $db_record )
    {
      array_push($matching_db_records, $db_record);
    }

  return $matching_db_records;
}

/*
 * main starts here
 */

/*
$new_pops = array();
$pop1 = [
	 //    'id' => NULL, // leave off id
	 'pop_edge_router_device_id' => 19,
	 'pop_srx_router_device_id' => 16,
	 'pop_name' => "RLC_POP1",
	 'pop_description' => "ansible-mx5t-wave-131",
	 'username' => "admin",
	 'password' => "12345678",
	 'device_enrolment_status' => 1
	 ];
$pop2 = [
	 //    'id' => NULL, // leave off id
	 'pop_edge_router_device_id' => 20,
	 'pop_srx_router_device_id' => 16,
	 'pop_name' => "RLC_POP2",
	 'pop_description' => "vmx-wave-113",
	 'username' => "admin",
	 'password' => "12345678",
	 'device_enrolment_status' => 1
	 ];
array_push($new_pops, $pop1);
//insertPOPs($new_pops);
//insertPOPs($pop1);
//insertPOPs($pop2);

$new_neighbors = array();
$new_costs = array();
$neighbor1 = [
	      //    'id' => NULL, // leave off id
	      'name' => 'neighbor1',
	      'description' => "LEVEL3:ge-1/1/0.20",
	      'pop_details_id' => 12,
	      'offnet_interface' => "ae1",
	      'offnet_interface_vlan' => NULL,
	      'billing_cycle' => NULL,
	      'billing_start_date' => NULL,
	      'billing_model' => NULL,
	      'sub_interface_name' => "ae1.20",
	      'offnet_interface_unit' => "20"
	      ];
$neighbor1_costs = [
		    'isp_id' => 12,
		    'committed_information_rate' => 35, // bps
		    'committed_information_cost' => 2.50 // $/Mbps/mo
		    ];
$neighbor2 = [
	      //    'id' => NULL, // leave off id
	      'name' => 'neighbor2',
	      'description' => "ABOVENET:ge-1/1/0.18",
	      'pop_details_id' => 12,
	      'offnet_interface' => "ae1",
	      'offnet_interface_vlan' => NULL,
	      'billing_cycle' => NULL,
	      'billing_start_date' => NULL,
	      'billing_model' => NULL,
	      'sub_interface_name' => "ae1.18",
	      'offnet_interface_unit' => "18"
	      ];
$neighbor2_costs = [
		    'isp_id' => 12,
		    'committed_information_rate' => 5000000, // bps
		    'committed_information_cost' => 2.50 // $/Mbps/mo
		    ];
$neighbor3 = [
	      //    'id' => NULL, // leave off id
	      'name' => 'neighbor3',
	      'description' => "IX1:ge-1/1/0.22",
	      'pop_details_id' => 12,
	      'offnet_interface' => "ae1",
	      'offnet_interface_vlan' => NULL,
	      'billing_cycle' => NULL,
	      'billing_start_date' => NULL,
	      'billing_model' => NULL,
	      'sub_interface_name' => "ae1.22",
	      'offnet_interface_unit' => "22"
	      ];
$neighbor4 = [
	      //    'id' => NULL, // leave off id
	      'name' => 'neighbor4',
	      'description' => "PEER1:ge-1/1/0.38",
	      'pop_details_id' => 12,
	      'offnet_interface' => "ae1",
	      'offnet_interface_vlan' => NULL,
	      'billing_cycle' => NULL,
	      'billing_start_date' => NULL,
	      'billing_model' => NULL,
	      'sub_interface_name' => "ae1.38",
	      'offnet_interface_unit' => "38"
	      ];
$neighbor5 = [
	      //    'id' => NULL, // leave off id
	      'name' => 'neighbor5',
	      'description' => "PEER2:ge-1/1/0.40",
	      'pop_details_id' => 12,
	      'offnet_interface' => "ae1",
	      'offnet_interface_vlan' => NULL,
	      'billing_cycle' => NULL,
	      'billing_start_date' => NULL,
	      'billing_model' => NULL,
	      'sub_interface_name' => "ae1.40",
	      'offnet_interface_unit' => "40"
	      ];
array_push($new_neighbors, $neighbor1);
//insertNeighbors($new_neighbors);
//insertNeighbors($neighbor1);
//insertNeighbors($neighbor2);
//insertNeighbors($neighbor3);
//insertNeighbors($neighbor4);
//insertNeighbors($neighbor5);

array_push($new_costs, $neighbor1_costs);
//insertNeighborCostStructures($new_costs);
//insertNeighborCostStructures($neighbor1_costs);
//insertNeighborCostStructures($neighbor2_costs);


//getAllNeighborConnections();
$denormalizedNeighborConnectionInfo = getRequiredNeighborConnectionInfo();
//echo var_dump($denormalizedNeighborConnectionInfo);
//return;

//getAllPOPs();
//getAllDevices();
//getAllPorts();
//getifindex(20,1233);

/* */

?>


<!--
<link rel="stylesheet" href="nexusguard/css/dashboard.css"/>
<!-- -->

<div class="dashboard-stats" id="dashboard-stats-div">
</div>

<div class="dashboard-charts" id="dashboard-charts-div">

<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>

<table class="dashboard-charts" id="dashboard-charts-table">

<tr>

<td>
<div class="highcharts-container" id="top-customers-by-bps-div" style="min-width: 620px; height: 400px; margin: 0 auto"></div>
</td>

<td>
<div class="highcharts-container" id="top-customers-by-pps-div" style="min-width: 620px; height: 400px; margin: 0 auto"></div>
</td>

</tr>

<tr>

<td>
<div class="highcharts-container" id="top-providers-by-bps-div" style="min-width: 620px; height: 400px; margin: 0 auto"></div>
</td>

<td>
<div class="highcharts-container" id="top-providers-by-pps-div" style="min-width: 620px; height: 400px; margin: 0 auto"></div>
</td>

</tr>

</table>

<script language="Javascript" type="text/javascript">

//var global_nxg_dashboard_array = [];


$(function () {

//      var cust_bps_query_url = "test/nfdumpquery.php?query=Top10CustomersByBps";
      var cust_bps_query_url = <?php echo json_encode($CUST_BPS_QUERY_URL); ?>;

      $.ajax({
//	contentType: 'application/json',
//	data: JSON.stringify({ "series" : ret }),
	      dataType: 'json',
	      type: 'GET',
	      url: cust_bps_query_url,
	      success: function(data, textStatus, jqXHR) {
    $('#top-customers-by-bps-div').highcharts({

        chart: {
            type: 'bar'
        },
        title: {
            text: 'Top 10 Customers by Bandwidth'
        },
        subtitle: {
            text: '(last 24 hours)'
        },
        xAxis: {
            categories: [
                '20150917 - 20150918'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Bandwidth (bps)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} bps</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            bar: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
	series: data.series

    });

		 },
		 error: function(jqXHR, textStatus, errorThrown) {
	       // debugger
	       }
	});
/*
      function run_nfdump_query(query_url) {
      var query_result = {};
      $.ajax({
//	contentType: 'application/json',
//	data: JSON.stringify({ "series" : ret }),
	      dataType: 'json',
	      type: 'GET',
	      url: query_url,
	      success: function(data, textStatus, jqXHR) {
		 query_result = data;
		 },
		 error: function(jqXHR, textStatus, errorThrown) {
	       // debugger
	       }
	});
	return query_result;
      }

      var topCustomersByBpsData = run_nfdump_query("test/nfdumpquery.php");

//    $('#top-customers-by-bps-div').highcharts({

        chart: {
            type: 'bar'
        },
        title: {
            text: 'Top 10 Customers by Bandwidth'
        },
        subtitle: {
            text: '(last 24 hours)'
        },
        xAxis: {
            categories: [
                '20150917 - 20150918'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Bandwidth (bps)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} bps</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            bar: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
	topCustomersByBpsData

//    });
/* */

      var display_customers_by_pps_chart = {
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Top 10 Customers by Bandwidth'
        },
        subtitle: {
            text: '(last 24 hours)'
        },
        xAxis: {
            categories: [
                '20150917 - 20150918'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Bandwidth (pps)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} pps</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            bar: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: '228.39.12.65 (ABC)',
            data: [990129]

        }, {
            name: '54.78.32.117 (DEF)',
            data: [830666]

        }, {
            name: '128.96.48.54 (GHI)',
            data: [481259]

        }, {
            name: '128.96.48.55 (SDT)',
            data: [18902]

        }, {
            name: '128.96.48.56 (NXG)',
            data: [48.9]

        }, {
            name: '128.96.48.57 (BPP)',
            data: [48.9]

        }, {
            name: '128.96.48.58 (CTJ)',
            data: [48.9]

        }, {
            name: '128.96.48.59 (PLF)',
            data: [48.9]

        }, {
            name: '128.96.48.60 (JPF)',
            data: [48.9]

        }, {
            name: '75.121.73.92 (PSA)',
            data: [42.4]

        }]
    };

//      var cust_pps_query_url = "test/nfdumpquery.php?query=Top10CustomersByPps";
//	var cust_pps_query_url = "<?php /* $CUST_PPS_QUERY_URL = "test/nfdumpquery.php?query=Top10CustomersByPps"; echo $CUST_PPS_QUERY_URL; */ ?>";
//	var cust_pps_query_url = <?php /* include_once './dashboard_conf.php'; echo json_encode($CUST_PPS_QUERY_URL); */ ?>;
	var cust_pps_query_url = <?php echo json_encode($CUST_PPS_QUERY_URL); ?>;

      $.ajax({
	      dataType: 'json',
	      type: 'GET',
	      url: cust_pps_query_url,
	      success: function(data, textStatus, jqXHR) {
	      	 display_customers_by_pps_chart.series = data.series;
		 $('#top-customers-by-pps-div').highcharts(display_customers_by_pps_chart);
		 },
		 error: function(jqXHR, textStatus, errorThrown) {
		 // debugger
	         }
	});

      var display_providers_by_bps_chart = {
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Top 10 Providers by Bandwidth'
        },
        subtitle: {
            text: '(last 24 hours)'
        },
        xAxis: {
            categories: [
                '20150917 - 20150918'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Bandwidth (bps)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} bps</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            bar: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: '228.39.12.65 (ABC)',
            data: [990129]

        }, {
            name: '54.78.32.117 (DEF)',
            data: [830666]

        }, {
            name: '128.96.48.54 (GHI)',
            data: [481259]

        }, {
            name: '128.96.48.55 (SDT)',
            data: [18902]

        }, {
            name: '128.96.48.56 (NXG)',
            data: [48.9]

        }, {
            name: '128.96.48.57 (BPP)',
            data: [48.9]

        }, {
            name: '128.96.48.58 (CTJ)',
            data: [48.9]

        }, {
            name: '128.96.48.59 (PLF)',
            data: [48.9]

        }, {
            name: '128.96.48.60 (JPF)',
            data: [48.9]

        }, {
            name: '75.121.73.92 (PSA)',
            data: [42.4]

        }]
    };

      var display_providers_by_pps_chart = {
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Top 10 Providers by Bandwidth'
        },
        subtitle: {
            text: '(last 24 hours)'
        },
        xAxis: {
            categories: [
                '20150917 - 20150918'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Bandwidth (pps)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} bps</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            bar: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: '228.39.12.65 (ABC)',
            data: [990129]

        }, {
            name: '54.78.32.117 (DEF)',
            data: [830666]

        }, {
            name: '128.96.48.54 (GHI)',
            data: [481259]

        }, {
            name: '128.96.48.55 (SDT)',
            data: [18902]

        }, {
            name: '128.96.48.56 (NXG)',
            data: [48.9]

        }, {
            name: '128.96.48.57 (BPP)',
            data: [48.9]

        }, {
            name: '128.96.48.58 (CTJ)',
            data: [48.9]

        }, {
            name: '128.96.48.59 (PLF)',
            data: [48.9]

        }, {
            name: '128.96.48.60 (JPF)',
            data: [48.9]

        }, {
            name: '75.121.73.92 (PSA)',
            data: [42.4]

        }]
    };
      
//      $('#top-customers-by-bps-div').highcharts(display_customers_by_bps_chart);
//      $('#top-customers-by-pps-div').highcharts(display_customers_by_pps_chart);
      $('#top-providers-by-bps-div').highcharts(display_providers_by_bps_chart);
      $('#top-providers-by-pps-div').highcharts(display_providers_by_pps_chart);

});
</script>


<?php
$dummy_var = 0;
?>
</div>

<div class="dashboard-alerts" id="dashboard-alerts-div">
</div>

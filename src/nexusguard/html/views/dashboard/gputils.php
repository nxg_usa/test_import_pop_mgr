<?php

include_once("inetutils.php");

function findPrefixAssignedCustomers($prefixList) {

  /*
   * prefixList format: [{"name": "", "data": ""}, {"name": "", "data": ""}, . . . {"name": "", "data": ""}]
   */

  // echo 'Hello1';
  $prefixesWithAssignedCustomers = array();

  // echo var_dump($prefixList);
  // return;

  // $allCustomerPrefixes = getAllCustomerPrefixes();
  $allCustomerPrefixes = getAllCustomerPrefixesWithCustomerInfo();
  // echo 'Hello2';
  // echo var_dump($allCustomerPrefixes);
  // return;

  foreach($prefixList as $prefixEntry) {

    $foundMatch = FALSE;
    foreach($allCustomerPrefixes as $customerPrefixRecord) {
      $netflowPrefix = extractHostAddrAndPort( $prefixEntry['name'] )['host'] . "/32";
      // echo '$netflowPrefix: ' . $netflowPrefix;
      $customerPrefix = $customerPrefixRecord['net_srv_prefix'];
      // echo '$customerPrefix: ' . $customerPrefix;
      $cmp_result = inet_cidr_cmp($netflowPrefix, $customerPrefix, FALSE);
      // echo '$cmp_result: ' . $cmp_result;

      //if( inet_cidr_cmp($netflowPrefix, $customerPrefix) ) {
      if( $cmp_result ) {
	$foundMatch = TRUE;
	$extendedPrefixEntry = array();
	$extendedPrefixEntry['name'] = $prefixEntry['name'];
	$extendedPrefixEntry['data'] = $prefixEntry['data'];
	$extendedPrefixEntry['customerId'] = $customerPrefixRecord['cust_id'];
	$extendedPrefixEntry['customerOSSId'] = $customerPrefixRecord['ossid'];
	$extendedPrefixEntry['customerName'] = $customerPrefixRecord['name'];
	array_push($prefixesWithAssignedCustomers, $extendedPrefixEntry);
	
	break;
      }
    }

    if( !$foundMatch ) {
      array_push($prefixesWithAssignedCustomers, $prefixEntry);
    }
    // break; // comment out for debugging purposes . . .
  }

  return $prefixesWithAssignedCustomers;
}

function getCurrentMonthLenInSeconds() {
  $current_date = getdate(); // default timestamp arg = time()

  $current_month = $current_date['mon']; // 1 <= month <= 12
  $current_year = $current_date['year'];
  $next_month = ($current_month % 12) + 1;
  $next_month_year = ($current_month == 12) ? ($current_year + 1) : $current_year;

  $current_month_len_in_seconds = mktime(0, 0, 0, $next_month, 1, $next_month_year) - mktime(0, 0, 0, $current_month, 1, $current_year);

  return $current_month_len_in_seconds;
}

function convertSecondsToMinutes($seconds) {
  return ( ((double) $seconds) / ((double) 60.0) );
}

function convertSecondsToDays($seconds) {
  return ( ((double) $seconds) / ((double) 86400.0) );
}

function getTotalNumSamplesInBillingInterval($billingIntervalInSeconds, $samplingIntervalInSeconds) {
  if($samplingIntervalInSeconds == 0) {
    return NULL;
  }

  return intval( round($billingIntervalInSeconds / $samplingIntervalInSeconds) ); // round defaults to 0 places of precision
}

function getPercentileSampleSizes($totalSampleSize, $percentileValue, $isKeepFraction=TRUE) {
  if($percentileValue < 0 || 1 < $percentileValue) {
    return NULL;
  }

  $PRECISION = 2;
  $discardFraction = ($isKeepFraction) ? (1.0 - $percentileValue) : $percentileValue;
  $discardFraction = round($discardFraction, $PRECISION);
  $keepFraction = (1.0 - $discardFraction);
  $keepFraction = round($keepFraction, $PRECISION);

  $sampleInfo = array();
  $sampleInfo['discardFraction'] = $discardFraction;
  $sampleInfo['keepFraction'] = $keepFraction;
  $sampleInfo['totalSampleSize'] = intval( round($totalSampleSize) ); // round defaults to 0 places of precision
  $sampleInfo['discardSampleSize'] = intval( round($discardFraction * $totalSampleSize) ); // round defaults to 0 places of precision
  $sampleInfo['keepSampleSize'] = ($sampleInfo['totalSampleSize'] - $sampleInfo['discardSampleSize']);

  return $sampleInfo;
}

function convertNumSamplesToSeconds($numSamples, $samplingIntervalInSeconds) {
  return ($numSamples * $samplingIntervalInSeconds);
}

function convertSecondsToNumSamples($seconds, $samplingIntervalInSeconds) {
  return intval( round($seconds / $samplingIntervalInSeconds) );
}

?>

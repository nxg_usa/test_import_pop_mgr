<?php
include_once("bwcosts_conf.php");
//include_once("nexusguard/views/dashboard/bwcosts_conf.php"); // get URLs for AJAX queries
include_once("dbutils.php");
include_once("gputils.php");
include_once "/opt/observium/nexusguard/nfsen/utils.inc.php";

function extractSingleSampleProportions ($singleNetflowSampleData) {
//	 return is_array($singleNetflowSampleData);

	 $singleSampleProportions = array();
	 $netflowPrefixData = $singleNetflowSampleData['series'];
	 $tmp = array();

	 $total_bwrate = 0;
	 foreach($netflowPrefixData as $prefix_bw_data) {
		$proportion_info = array();
	 	$prefix = $prefix_bw_data['name'];
		$bwrate = $prefix_bw_data['data'][0];
		$total_bwrate += $bwrate;
		$proportion_info['prefix'] = $prefix;		
		$proportion_info['proportion'] = (double) $bwrate;
		array_push($tmp, $proportion_info);
	 }
//	 return $tmp;
	 foreach($tmp as $tmp_proportion_info) {
	 	$proportion_info = array();
		$proportion_info['prefix'] = $tmp_proportion_info['prefix'];
	 	$proportion_info['proportion'] = ($tmp_proportion_info['proportion']) / ((double) $total_bwrate);
		array_push($singleSampleProportions, $proportion_info);
	 }

	 return $singleSampleProportions;
}

function determineAllBurstContributors() {
  global $CUMCONSUMEDBWBUDGETCHARTDATA;
  global $DEVICE_NAME_MAP;
  global $BW_USAGE_QUERY_URL;

/*
 * inputs:
 *	a specified POP (optional?)
 *	a specified router (required)
 *	a specified (logical/lag/physical) interface (required)
 *	a set of burst-over times -- times where bw rate exceeded CIR (unix or iso format) (required)
 */


  $priorityLevel = -1;
/* */
  if( array_key_exists('priorityLevel', $_GET) ) {
      $priorityLevel = $_GET['priorityLevel'];
  }

  $neighborIndex = 0;
/* */
  if( array_key_exists('neighborIndex', $_GET) ) {
      $neighborIndex = $_GET['neighborIndex'];
  }
  $neighborIndexKey = "neighborIndex" . "$neighborIndex";

/* */
//  $session_status = session_status();
//  $session_id = session_id();
//  $session_name = session_name();
//  echo json_encode([ 'status' => $session_status, 'id' => $session_id, 'name' => $session_name ]);
//  return;

  if ( $priorityLevel >= 0 ) {
    session_id(345); // ensure AJAX calls use same session id
    $session_started = session_start( [ 'read_and_close' => 1 ] );
    $neighborIndexWithSortCriteria = $_SESSION['neighborIndexWithSortCriteria'];
    session_write_close();

    if ( count($neighborIndexWithSortCriteria) > 0 ) {
      //     echo '$priorityLevel: ' . $priorityLevel;
      $neighborIndexMapEntry = array_slice($neighborIndexWithSortCriteria, $priorityLevel, 1, TRUE);
      //     echo var_dump($neighborIndexMapEntry);
      //     echo 'key: ' . key($neighborIndexMapEntry);
      $neighborIndex = key($neighborIndexMapEntry);
      //     echo '$neighborIndex: ' . $neighborIndex;
      $neighborIndexKey = "neighborIndex" . "$neighborIndex";
      $priorityLevel = -1;
    }
  }

//  session_name('OBSID'); // ensure AJAX calls use same session as authenticated GUI pages . . .
//  session_name('BWCOSTS'); // ensure AJAX calls use same session name
  session_id(346); // ensure AJAX calls use same session id
  $session_started = session_start( [ 'read_and_close' => 1 ] );
//  $session_id = session_id();
//  echo json_encode($session_id);
//  echo json_encode($session_started);
//  return;


//  $neighborDetails = $CUMCONSUMEDBWBUDGETCHARTDATA[$neighborIndexKey]['neighborDetails'];
  $neighborDetails = $_SESSION['bwcostinfo'][$neighborIndexKey]['neighborDetails'];
  // echo json_encode($neighborDetails);
  // echo json_encode($DEVICE_NAME_MAP);
//  echo json_encode($BW_USAGE_QUERY_URL);
//  return;
  $neighborId = $neighborDetails['neighbor_id'];
/*
  if ( array_key_exists($neighborDetails['hostname'], $DEVICE_NAME_MAP['observium2nfdump']) ) {
     $deviceName = $DEVICE_NAME_MAP['observium2nfdump'][$neighborDetails['hostname']];
     // echo '$deviceName: ' . $deviceName;
  }
/* */
  $deviceName = generate_nfsen_devicename($neighborDetails['hostname']); // returns NULL if hostname not found in devices table
  // echo '$deviceName: ' . $deviceName;

//  $deviceName = "ansible-mx5t";
  $ifIndex = $neighborDetails['ifIndex'];
//  $ifIndex = 760;
//  echo json_encode($deviceName);
//  return;
  $netmask = 32;

//  $burstovertimes = $CUMCONSUMEDBWBUDGETCHARTDATA[$neighborIndexKey]['burstovertimes'];
//  $burstovertimes = $_SESSION['bwcostinfo'][$neighborIndexKey]['burstovertimes'];
  $burstovertimes = json_decode($_SESSION['bwcostinfo'][$neighborIndexKey]['burstovertimes'], TRUE);
//    echo var_dump(serialize($_SESSION));
//    return;

  session_write_close();
//  $burstovertimes = [ 1443769200 ];
//    echo json_encode(count($_SESSION['bwcostinfo'][$neighborIndexKey]['burstovertimes']));
//  echo json_encode(count($burstovertimes));
//  echo json_encode($burstovertimes);
//  return;

  /*
   * in order to increase query performance, first look for prefix summary info in the $_SESSION, before querying nfdump
   * if found in $_SESSION, return that info, and avoid making a REST call to nfsen
   */
  session_id(347); // ensure AJAX calls use same session id
  session_start( [ 'lazy_write' => 1 ] );
  // session_start( [ 'lazy_write' => 0 ] );

  if (array_key_exists($neighborId, $_SESSION['burstContributionInfo']) ) {
    if (array_key_exists('prefixSummaries', $_SESSION['burstContributionInfo'][$neighborId]) ) {
      $prefix_json_string = $_SESSION['burstContributionInfo'][$neighborId]['prefixSummaries'];
      $prefixSummary = json_decode($prefix_json_string, TRUE);
      $ttl = $prefixSummary['ttl'];
      if( $ttl > 0 ) {
	$lastAccessTime = $prefixSummary['lastAccessTime'];
	$currentTime = time();
	$ttl -= ($currentTime - $lastAccessTime);
	$prefixSummary['ttl'] = $ttl;
	$prefixSummary['lastAccessTime'] = $currentTime;
	$updated_json_string = json_encode($prefixSummary);

	$_SESSION['burstContributionInfo'][$neighborId]['prefixSummaries'] = $updated_json_string;
	session_write_close();

	echo $prefix_json_string;
	return;
      }
    }
  }

  session_write_close();

  $burstoverparticipants = array();
  foreach ($burstovertimes as $singletimeslot) {
//  	$ingress_bw_usage_query_url = $BW_USAGE_QUERY_URL;
  	$ingress_bw_usage_query_url = "http://localhost/" . $BW_USAGE_QUERY_URL . "?" . "unixtime=" . $singletimeslot . "&" . "deviceName=" . $deviceName . "&" . "ifIndex=" . $ifIndex . "&" . "netmask=" . $netmask . "&" . "flowdirection=in";
//	echo '$ingress_bw_usage_query_url: ' . $ingress_bw_usage_query_url . "\n";
//	return;
	$ingress_curl_get_cmd = '/usr/bin/curl -s -S "' . "$ingress_bw_usage_query_url" . '"';
	$ingress_query_response_string = shell_exec($ingress_curl_get_cmd);
//	echo '$ingress_query_response_string: ' . $ingress_query_response_string . "\n";
//	echo var_dump(json_decode($ingress_query_response_string, TRUE));
//	return;

//	$burstoverparticipants[$singletimeslot] = json_decode($ingress_query_response_string, TRUE);
//	$burstoverparticipants[$singletimeslot] = extractSingleSampleProportions(json_decode($ingress_query_response_string, TRUE));
	$burstoverparticipants[$singletimeslot] = array();
	$burstoverparticipants[$singletimeslot]['ingress'] = extractSingleSampleProportions(json_decode($ingress_query_response_string, TRUE));
//	$ingress_extractedInfo = extractSingleSampleProportions(json_decode($ingress_query_response_string, TRUE));
//	echo json_encode($ingress_extractedInfo);
//	return;

  	$egress_bw_usage_query_url = "http://localhost/" . $BW_USAGE_QUERY_URL . "?" . "unixtime=" . $singletimeslot . "&" . "deviceName=" . $deviceName . "&" . "ifIndex=" . $ifIndex . "&" . "netmask=32" . "&" . "flowdirection=out";
//	echo '$egress_bw_usage_query_url: ' . $egress_bw_usage_query_url . "\n";
//	return;
	$egress_curl_get_cmd = '/usr/bin/curl -s -S "' . "$egress_bw_usage_query_url" . '"';
	$egress_query_response_string = shell_exec($egress_curl_get_cmd);
//	echo '$egress_query_response_string: ' . $egress_query_response_string;
	$burstoverparticipants[$singletimeslot]['egress'] = extractSingleSampleProportions(json_decode($egress_query_response_string, TRUE));
  }
  // echo json_encode($burstoverparticipants);
  // return;
  // echo '$burstoverparticipants[0]: ' . json_encode( array_slice($burstoverparticipants, 0, 1, TRUE) );
  // echo '$burstoverparticipants[count($burstoverparticipants)-1]: ' . json_encode( array_slice($burstoverparticipants, (count($burstoverparticipants) - 1), 1, TRUE) );
  // return;

  $summarizedParticipantContributions = summarizeParticipantContributions($burstoverparticipants);

  $summarizedParticipantContributions['neighborIndex'] = $neighborIndex;
  $summarizedParticipantContributions['neighborId'] = $neighborId;
  $summarizedParticipantContributions['deviceName'] = $deviceName;
  $summarizedParticipantContributions['ifIndex'] = $ifIndex;
  $summarizedParticipantContributions['netmask'] = $netmask;

  // $ttl = 0; 
  $ttl = 150; // 150s = 2.5mins -- only make REST call to nfsen/nfdump every 2.5mins, not on every AJAX call to this function . . .
  // $ttl = 300; // 300s = 5mins -- only make REST call to nfsen/nfdump every 5mins, not on every AJAX call to this function . . .
  // $ttl = 600; // 600s = 10mins -- only make REST call to nfsen/nfdump every 10mins, not on every AJAX call to this function . . .
  // $ttl = 900; // 900s = 15mins -- only make REST call to nfsen/nfdump every 15mins, not on every AJAX call to this function . . .
  $summarizedParticipantContributions['ttl'] = $ttl;
  $currentTime = time();
  $summarizedParticipantContributions['lastAccessTime'] = $currentTime;

  $json_string = json_encode($summarizedParticipantContributions);

  session_id(347); // ensure AJAX calls use same session id
  session_start( [ 'lazy_write' => 1 ] );

  if (!array_key_exists('burstContributionInfo', $_SESSION) ) {
    $_SESSION['burstContributionInfo'] = array();
  }
  if (!array_key_exists($neighborId, $_SESSION['burstContributionInfo']) ) {
    $_SESSION['burstContributionInfo'][$neighborId] = array();
  }
  $_SESSION['burstContributionInfo'][$neighborId]['prefixSummaries'] = $json_string;

  session_write_close();

  // echo json_encode($summarizedParticipantContributions);
  echo $json_string;
}

function calculateOverallContributionOfEachPrefix($participantContributions, $prefixProportionsForEachTimeSample, $currentWeightingFactor=1.0, $previousWeightingFactor=1.0) {

  // echo '$currentWeightingFactor: ' . $currentWeightingFactor;
  // echo '$previousWeightingFactor: ' . $previousWeightingFactor;

  // apply scalar $previousWeightingFactor to all elements of $participantContributions = prefix proportions from all previous time samples . . .
  $modifiedParticipantContributions = array();
  // echo var_dump($participantContributions);
  foreach($participantContributions as $key => $value) {
    // echo '$key: ' . $key . ' $value: ' . $value;
    $modifiedParticipantContributions[$key] = ((double) $previousWeightingFactor) * ((double) $value);
  }

  foreach($prefixProportionsForEachTimeSample as $singlePrefixProportion) {
    //			echo json_encode($singlePrefixProportion);
    if( !array_key_exists($singlePrefixProportion['prefix'], $modifiedParticipantContributions) ) {
      $modifiedParticipantContributions[$singlePrefixProportion['prefix']] = ((double) $currentWeightingFactor) * ((double) $singlePrefixProportion['proportion']); // initialize accumulation of each prefix's portion
    }
    else {
      // $participantContributions[$singlePrefixProportion['prefix']] = ((double) $participantContributions[$singlePrefixProportion['prefix']]) + ((double) $singlePrefixProportion['proportion']); // continue accumulation of each prefix's portion
      $prefixPortion = ((double) $modifiedParticipantContributions[$singlePrefixProportion['prefix']]); // $previousWeightingFactor already applied earlier . . .
      $prefixPortion += ((double) $currentWeightingFactor) * ((double) $singlePrefixProportion['proportion']);
      $modifiedParticipantContributions[$singlePrefixProportion['prefix']] = $prefixPortion;
    }
  }

  return $modifiedParticipantContributions;
}

function summarizeParticipantContributions($burstOverParticipants) {

	 $ingressParticipantContributions = array();
	 $egressParticipantContributions = array();
	 $totalBurstOverSamples = 0;
	 foreach($burstOverParticipants as $sampleProportions) {
	 	$totalBurstOverSamples += 1;
		$ingressParticipantContributions = calculateOverallContributionOfEachPrefix($ingressParticipantContributions, $sampleProportions['ingress'], 0.9999, 0.0001);
		$egressParticipantContributions = calculateOverallContributionOfEachPrefix($egressParticipantContributions, $sampleProportions['egress'], 0.9999, 0.0001);
	 }
//	 echo json_encode($totalBurstOverSamples);
	 arsort($ingressParticipantContributions); // arrange in descending order by value (not by key)
//	 return $ingressParticipantContributions;
	 arsort($egressParticipantContributions); // arrange in descending order by value (not by key)
//	 return $egressParticipantContributions;

	 $summarizedContributions = array();
	 foreach($ingressParticipantContributions as $key => $value) {
	 	$overallParticipantProportion = array();
		$overallParticipantProportion['name'] = $key;
		$overallParticipantProportion['data'] = array();
		array_push($overallParticipantProportion['data'], ((double) $value) / ((double) $totalBurstOverSamples)); // will not be 0 if at least one participant, won't reach here if 0 participants
		array_push($overallParticipantProportion['data'], ((double) 0.0)); // prepare for merging of egress data
	 	array_push($summarizedContributions, $overallParticipantProportion);
	 }
	 foreach($egressParticipantContributions as $key => $value) {
	 	if ( !in_array( $key, array_column($summarizedContributions, 'name') ) ) {
		  // egress key not found -- create and insert new record
		  $overallParticipantProportion = array();
		  $overallParticipantProportion['name'] = $key;
		  $overallParticipantProportion['data'] = array();
		  array_push($overallParticipantProportion['data'], ((double) 0.0)); // no ingress data
		  array_push($overallParticipantProportion['data'], ((double) $value) / ((double) $totalBurstOverSamples)); // will not be 0 if at least one participant, won't reach here if 0 participants
		  array_push($summarizedContributions, $overallParticipantProportion);
		}
		else {
		  // egress key found -- update existing record
		  $recordIndex = array_search($key, array_column($summarizedContributions, 'name') );
		  $summarizedContributions[$recordIndex]['data'][1] = ((double) $value) / ((double) $totalBurstOverSamples); // replace initial 0.0 placeholder with value
		}
	 }

//	 return $summarizedContributions;

	 // TODO: obtain these values from bwcosts_conf.php
	 $countLimit = 10;
	 // $discardRemainingItems = FALSE; // TRUE = discard any lesser items; FALSE=aggregate all lesser items into (N+1)th 'other' category
	 $discardRemainingItems = TRUE; // TRUE = discard any lesser items; FALSE=aggregate all lesser items into (N+1)th 'other' category
	 $limitedSummarizedContributions = preserveNHighestItemsOnly($summarizedContributions, $countLimit, $discardRemainingItems);

	 $limitedSummarizedContributions = incorporatePrefixCustomerInfo($limitedSummarizedContributions);

	 $summarizedContributionsChartData = array();
	 $summarizedContributionsChartData['series'] = $limitedSummarizedContributions;

	 return $summarizedContributionsChartData;
}

function preserveNHighestItemsOnly($sortedItemsInDescendingOrder, $countLimit, $discardRemainingItems) {
  if($countLimit <= 0) {
    return $sortedItemsInDescendingOrder;
  }

  $limitedItemList = array();
  $completeTotal = (double) 0.0;
  $nHighestTotal = (double) 0.0;
  $cnt = 0;
  foreach($sortedItemsInDescendingOrder as $prefixDataItem) {
    if($cnt < $countLimit) {
      $nHighestTotal += $prefixDataItem['data'][0];
      array_push($limitedItemList, $prefixDataItem);
    }
    else {
      if($discardRemainingItems) {
	break;
      }
    }

    $completeTotal += $prefixDataItem['data'][0];
    $cnt += 1;
  }
  if( !$discardRemainingItems && count($sortedItemsInDescendingOrder) > $countLimit ) {
    $lastPrefixDataItem = array();
    $lastPrefixDataItem['name'] = 'other';
    $lastPrefixDataItem['data'] = array( ((double) $completeTotal) - ((double) $nHighestTotal) );
    array_push($limitedItemList, $lastPrefixDataItem);
  }

  return $limitedItemList;
}

function incorporatePrefixCustomerInfo($prefixList) {
  /*
   * prefixList format: [{"name": "", "data": ""}, {"name": "", "data": ""}, . . . {"name": "", "data": ""}]
   */

  // echo var_dump($prefixList);
  // return;
  $prefixListWithCustomerInfo = findPrefixAssignedCustomers($prefixList);
  // echo var_dump($prefixListWithCustomerInfo);
  // return;

  /*
   * prefixList format: [{"name": "", "data": "", "customerId": "", "customerOSSId": "", "customerName": "" }, . . . ]
   */

  $prefixListWithReformattedCustomerInfo = array();
  foreach($prefixListWithCustomerInfo as $prefixEntry) {
    if( $prefixEntry['name'] != 'other' ) {
      $reformattedPrefixEntry = array();
      $reformattedPrefixEntry['name'] = $prefixEntry['name'];
      $reformattedPrefixEntry['data'] = $prefixEntry['data'];
      $reformattedPrefixEntry['extra'] = array();
      $reformattedPrefixEntry['extra']['customerId'] = $prefixEntry['customerId']; // may not exist/be set
      $reformattedPrefixEntry['extra']['customerOSSId'] = $prefixEntry['customerOSSId']; // may not exist/be set
      $reformattedPrefixEntry['extra']['customerName'] = $prefixEntry['customerName']; // may not exist/be set
      array_push($prefixListWithReformattedCustomerInfo, $reformattedPrefixEntry);
    }
  }

  // echo var_dump($prefixListWithReformattedCustomerInfo);
  // return;

  return $prefixListWithReformattedCustomerInfo;
}

function determineHighestBurstContributors() {
  global $CUMCONSUMEDBWBUDGETCHARTDATA;

/*
 * inputs:
 *	number of (highest) contributors + contributor metrics returned (required)
 *	a specified POP (optional?)
 *	a specified router (required)
 *	a specified (logical/lag/physical) interface (required)
 *	a set of burst-over times -- times where bw rate exceeded CIR (unix or iso format) (required)
 */

}

/*
 * start main section
 */

determineAllBurstContributors();

?>

<?php

include_once("bwcosts_conf.php");
include_once("gputils.php");
include_once("dbutils.php");
include_once("rrdutils.php");
include_once("inetutils.php");

function determineProjectedImpact() {
  global $CUMCONSUMEDBWBUDGET_QUERY_URL;

  /*
   * "?priorityLevel=0&neighborId=13&flowDirection=ingress&recommendationRanking=0&recordLimit=10"
   * priorityLevel: optional, if set, searches for neighbor having this offset;
   *    default value of -1 if not set, neighborId will be obtained from neighborId parm in GET request
   * neighborId: optional, defaults value of 0 if not set and priorityLevel is not set, or priorityLevel is
   *    set and no neighbor offset is found
   * flowDirection: optional, default value of 'ingress' if not set
   * recommendationRanking: optional, default value of 0 if not set
   * recordLimit: optional, default value of 10 if not set
   */

  $priorityLevel = -1;
  if( array_key_exists('priorityLevel', $_GET) ) {
      $priorityLevel = $_GET['priorityLevel'];
  }

  $neighborId = 0;
  if( array_key_exists('neighborId', $_GET) ) {
      $neighborId = $_GET['neighborId'];
  }

  $flowDirection = 'ingress'; // 'egress' or (not 'egress')
  if( array_key_exists('flowDirection', $_GET) ) {
      $flowDirection = $_GET['flowDirection'];
  }

  $recordLimit = 10;
  if( array_key_exists('recordLimit', $_GET) ) {
      $recordLimit = $_GET['recordLimit'];
  }

  $recommendationRanking = 0; // default = highest/most preferred recommendation
  if( array_key_exists('recommendationRanking', $_GET) ) {
      $recommendationRanking = $_GET['recommendationRanking'];
  }


  session_id(348); // ensure AJAX calls use same session id
  $session_started = session_start( [ 'read_and_close' => 1 ] );

  $compositeKey = "priorityLevel=$priorityLevel&recommendationRanking=$recommendationRanking&flowDirection=$flowDirection";
  $matchingSystemRecommendation = json_decode($_SESSION['systemRecommendations'][$compositeKey], TRUE);

  session_write_close();

  // get burn-down chart data for offender
  $offenderNeighborId = $matchingSystemRecommendation['currentOffender']['neighborDetails']['neighbor_id'];
  $offenderNeighborOrg = $matchingSystemRecommendation['currentOffender']['neighborDetails']['neighbor_org'];
  $offenderCir = getCIR($offenderNeighborId);
  $offenderCirUnitPrice = getCIRUnitPrice($offenderNeighborId);

  $offenderPOPId = $matchingSystemRecommendation['currentOffender']['neighborDetails']['pop_details_id'];
  $allNeighborCnxns = getNeighborCnxnByPOPId($offenderPOPId);

  // find index within set of all neighbor cnxns returned by DB
  $offenderNeighborIndex = 0;
  foreach($allNeighborCnxns as $neighborCnxnDBRecord) {
    if($offenderNeighborId == $neighborCnxnDBRecord['id']) {
      break;
    }

    $offenderNeighborIndex += 1;
  }

  $cum_consumed_bw_budget_query_url = "http://localhost/" . $CUMCONSUMEDBWBUDGET_QUERY_URL . "?neighborIndex=" . $offenderNeighborIndex . "&" . "cir=" . $offenderCir;
  // echo '$cum_consumed_bw_budget_query_url: ' . $cum_consumed_bw_budget_query_url;
  $curl_get_cmd = '/usr/bin/curl -s -S "' . "$cum_consumed_bw_budget_query_url" . '"';
  $query_response_string = shell_exec($curl_get_cmd);
  // echo '$query_response_string: ' . $query_response_string;
  $projectionsFromRRDData = json_decode($query_response_string, TRUE);

  $offenderBurnDownData = $projectionsFromRRDData;
  $offenderBurnDownData['series'][1]['name'] = $offenderNeighborOrg;

  // get burn-down chart data for replacement
  $replacementNeighborId = $matchingSystemRecommendation['recommendedReplacements'][0]['neighborDetails']['neighbor_id'];
  $replacementNeighborOrg = $matchingSystemRecommendation['recommendedReplacements'][0]['neighborDetails']['neighbor_org'];
  // echo var_dump($matchingSystemRecommendation['recommendedReplacements'][0]['neighborDetails']);
  // return;
  $replacementCir = getCIR($replacementNeighborId);
  $replacementCirUnitPrice = getCIRUnitPrice($replacementNeighborId);

  $replacementPOPId = $matchingSystemRecommendation['recommendedReplacements'][0]['neighborDetails']['pop_details_id'];
  $allNeighborCnxns = getNeighborCnxnByPOPId($replacementPOPId);

  // find index within set of all neighbor cnxns returned by DB
  $replacementNeighborIndex = 0;
  foreach($allNeighborCnxns as $neighborCnxnDBRecord) {
    if($replacementNeighborId == $neighborCnxnDBRecord['id']) {
      break;
    }

    $replacementNeighborIndex += 1;
  }

  $cum_consumed_bw_budget_query_url = "http://localhost/" . $CUMCONSUMEDBWBUDGET_QUERY_URL . "?neighborIndex=" . $replacementNeighborIndex . "&" . "cir=" . $replacementCir;
  // echo '$cum_consumed_bw_budget_query_url: ' . $cum_consumed_bw_budget_query_url;
  $curl_get_cmd = '/usr/bin/curl -s -S "' . "$cum_consumed_bw_budget_query_url" . '"';
  $query_response_string = shell_exec($curl_get_cmd);
  // echo '$query_response_string: ' . $query_response_string;
  $projectionsFromRRDData = json_decode($query_response_string, TRUE);

  $replacementBurnDownData = $projectionsFromRRDData;
  $replacementSeries = $replacementBurnDownData['series'][1];
  $replacementSeries['name'] = $replacementNeighborOrg;
  $replacementSeries['color'] = '#cc00cc';
  // echo json_encode($replacementSeries);
  // return;
  array_push($offenderBurnDownData['series'], $replacementSeries);

  // echo var_dump(array_keys($offenderBurnDownData['series']));
  // echo var_dump(array_keys($offenderBurnDownData['series'][1]));
  // echo var_dump(array_keys($offenderBurnDownData['series'][2]));
  // return;

  $currentMonthLenInDays = convertSecondsToDays( getCurrentMonthLenInSeconds() );

  /*
   * project impact of removing specified prefixes from offending neighbor
   */
  $offenderProjectedImpactData = array();
  $offenderProjectedImpactData['name'] = $offenderBurnDownData['series'][1]['name'] . ' projection';
  $offenderProjectedImpactData['color'] = $offenderBurnDownData['series'][1]['color'];
  $offenderProjectedImpactData['dashStyle'] = 'ShortDot';
  $offenderDataLen = count($offenderBurnDownData['series'][1]['data']);
  // echo '$offenderDataLen: ' . $offenderDataLen;
  $offender_lastdatapoint = $offenderBurnDownData['series'][1]['data'][($offenderDataLen-1)];
  $offender_penultimatedatapoint = $offenderBurnDownData['series'][1]['data'][($offenderDataLen-1-48)]; // 48 = 288/6
  $offenderSlope = ((double) $offender_lastdatapoint[1] - (double) $offender_penultimatedatapoint[1]) / ((double) $offender_lastdatapoint[0] - (double) $offender_penultimatedatapoint[0]);
  $highestPrefix = array_slice($matchingSystemRecommendation['currentOffender']['prefixSummary'],0,1,TRUE);
  // echo var_dump($highestPrefix);
  // return;
  $highestPrefixProportion = (double) $highestPrefix[0]['data'];
  $offenderProjectedImpactData['data'] = array();
  array_push($offenderProjectedImpactData['data'], $offender_lastdatapoint);
  // $rise = ((double) 1.0 - (double) $highestPrefixProportion) * ((double) $offenderSlope) * ((double) 30.0 - (double) $offender_lastdatapoint[0]);
  $rise = ((double) 1.0 - (double) $highestPrefixProportion) * ((double) $offenderSlope) * ((double) $currentMonthLenInDays - (double) $offender_lastdatapoint[0]);
  // $rise = (double) 0.0;
  $projectedEndPoint = array();
  // array_push($projectedEndPoint, (double) 30.0);
  array_push($projectedEndPoint, (double) $currentMonthLenInDays);
  array_push($projectedEndPoint, ((double) $offender_lastdatapoint[1] + (double) $rise));
  array_push($offenderProjectedImpactData['data'], $projectedEndPoint);
  array_push($offenderBurnDownData['series'], $offenderProjectedImpactData);

  /*
   * project impact of adding specified prefixes to replacement neighbor
   */
  $replacementProjectedImpactData = array();
  $replacementProjectedImpactData['name'] = $offenderBurnDownData['series'][2]['name'] . ' projection';
  $replacementProjectedImpactData['color'] = $offenderBurnDownData['series'][2]['color'];
  $replacementProjectedImpactData['dashStyle'] = 'ShortDot';
  $replacementDataLen = count($offenderBurnDownData['series'][2]['data']);
  // echo '$replacementDataLen: ' . $replacementDataLen;
  $replacement_lastdatapoint = $offenderBurnDownData['series'][2]['data'][($replacementDataLen-1)];
  $replacement_penultimatedatapoint = $offenderBurnDownData['series'][2]['data'][($replacementDataLen-1-48)]; // 48 = 288/6
  $replacementSlope = ((double) $replacement_lastdatapoint[1] - (double) $replacement_penultimatedatapoint[1]) / ((double) $replacement_lastdatapoint[0] - (double) $replacement_penultimatedatapoint[0]);
  $replacementPrefixSummary = $matchingSystemRecommendation['recommendedReplacements'][0]['prefixSummary'];
  // echo var_dump($replacementPrefixSummary);
  // return;
  $totalPrefixContributions = (double) 0;
  foreach($replacementPrefixSummary as $singlePrefixProportion) {
    $totalPrefixContributions += (double) $singlePrefixProportion['data'];
  }
  $replacementProjectedImpactData['data'] = array();
  array_push($replacementProjectedImpactData['data'], $replacement_lastdatapoint);
  // $rise = ((double) 1.0 + (double) $totalPrefixContributions) / ((double) $totalPrefixContributions) * ((double) $replacementSlope) * ((double) 30.0 - (double) $replacement_lastdatapoint[0]);
  $rise = ((double) 1.0 + (double) $totalPrefixContributions) / ((double) $totalPrefixContributions) * ((double) $replacementSlope) * ((double) $currentMonthLenInDays - (double) $replacement_lastdatapoint[0]);
  // $rise = (double) 0.0;
  $projectedEndPoint = array();
  // array_push($projectedEndPoint, (double) 30.0);
  array_push($projectedEndPoint, (double) $currentMonthLenInDays);
  array_push($projectedEndPoint, ((double) $replacement_lastdatapoint[1] + (double) $rise));
  array_push($replacementProjectedImpactData['data'], $projectedEndPoint);
  array_push($offenderBurnDownData['series'], $replacementProjectedImpactData);


  echo json_encode($offenderBurnDownData);
}

/*
 * main starts here
 */

determineProjectedImpact();

?>

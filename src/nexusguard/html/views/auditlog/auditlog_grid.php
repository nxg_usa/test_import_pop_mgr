<script type="text/javascript">
function getData(strCommitLog)
{
    (function($) {
                        var text =  strCommitLog ;
                        $("#auditlog_config").html(text);
                        jQuery("#popupModel").modal('show');
     })(jQuery);
}
</script>

<?php

/**
 * Observium Network Management and Monitoring System
 * Copyright (C) 2006-2015, Adam Armstrong - http://www.observium.org
 *
 * @package    observium
 * @subpackage webui
 * @author     Adam Armstrong <adama@observium.org>
 * @copyright  (C) 2006-2013 Adam Armstrong, (C) 2013-2015 Observium Limited
 *
 */

?>
<div class="row">
<div class="col-md-12">

<?php

///FIXME. Mike: should be more checks, at least a confirmation click.
//if ($vars['action'] == "expunge" && $_SESSION['userlevel'] >= '10')
//{
//  dbFetchCell('TRUNCATE TABLE `eventlog`');
//  print_message('Event log truncated');
//}

unset($search, $usernames_array, $popnames_array, $popdetails_array);

foreach (dbFetchColumn('SELECT DISTINCT `user_name` FROM `nxg_auditlog`') as $username)
{
  $usernames_array[$username] = $username ;
}
krsort($usernames_array);

$search[] = array('type'    => 'multiselect',
                  'name'    => 'Users',
                  'id'      => 'user_name',
                  'width'   => '125px',
                  'value'   => $vars['user_name'], 
                  'values'  => $usernames_array);

//Message field
$search[] = array('type'    => 'text',
                  'name'    => 'Commit Message',
                  'id'      => 'message',
                  'width'   => '150px',
                  'placeholder' => 'Commit message',
                  'value'   => $vars['message']);

//Message field
$search[] = array('type'    => 'text',
                  'name'    => 'Commit Check Log',
                  'id'      => 'checklog',
                  'width'   => '150px',
                  'placeholder' => 'Commit Check Log',
                  'value'   => $vars['checklog']);

foreach (dbFetchRows('SELECT `id`, `pop_name` FROM `nxg_pop_details`') as $popdetails)
{
  $popid = $popdetails['id'] ;
  $popdetails_array[$popid] = $popdetails['pop_name'] ;
}

//PoP name field
foreach (dbFetchColumn('SELECT DISTINCT `pop_id` FROM `nxg_auditlog`') as $popid)
{
  $popnames_array[$popid] = $popdetails_array[$popid] ;
}
$search[] = array('type'    => 'multiselect',
                  'name'    => 'PoP Name',
                  'id'      => 'pop_id',
                  'width'   => '110px',
                  'subtext' => TRUE,
                  'value'   => $vars['pop_id'],
                  'values'  => $popnames_array);

// Newline
//$search[] = array('type'    => 'newline',
//                  'hr'      => TRUE);

// Datetime field
$search[] = array('type'    => 'datetime',
                  'id'      => 'timestamp',
                  'presets' => TRUE,
                  'min'     => dbFetchCell('SELECT `audit_time` FROM `nxg_auditlog`' . ' ORDER BY `audit_time` LIMIT 0,1;'),
                  'max'     => dbFetchCell('SELECT `audit_time` FROM `nxg_auditlog`' . ' ORDER BY `audit_time` DESC LIMIT 0,1;'),
                  'from'    => $vars['timestamp_from'],
                  'to'      => $vars['timestamp_to']);

print_search($search, 'Auditlog', 'search', 'pop_mgr/view=auditlog_grid/');

// Pagination
$vars['pagination'] = TRUE;

// Print events
print_auditlog_events($vars);

$page_title[] = 'Auditlog';

function print_auditlog_events($vars)
{            
  global $popdetails_array ;
  $events = get_auditlog_events_array($vars);
  if (!$events['count'])
  {
    // There have been no entries returned. Print the warning.
    print_warning('<h4>No audit log entries found!</h4>');
  } 
  else 
  {
    // Entries have been returned. Print the table.
    $string = '<table class="table table-bordered table-striped table-hover table-condensed-more">' . PHP_EOL;
    if (!$events['short'])
    {
      $string .= '  <thead>' . PHP_EOL;
      $string .= '    <tr>' . PHP_EOL;
      $string .= '      <th class="state-marker"></th>' . PHP_EOL;
      $string .= '      <th>Date</th>' . PHP_EOL;
      $string .= '      <th>PoP Name</th>' . PHP_EOL;
      $string .= '      <th>Configuration Changed By</th>' . PHP_EOL;
      $string .= '      <th>Module</th>' . PHP_EOL;
      $string .= '      <th>Commit/Syslog Message</th>' . PHP_EOL;
      $string .= '      <th>Match Status</th>' . PHP_EOL;
      $string .= '      <th>Commit Status</th>' . PHP_EOL;
      $string .= '      <th>Commit Log</th>' . PHP_EOL;
      $string .= '    </tr>' . PHP_EOL;
      $string .= '  </thead>' . PHP_EOL;
    }
    $string   .= '  <tbody>' . PHP_EOL;

    foreach ($events['entries'] as $entry)
    {
      $string .= '  <tr class="'.$entry['html_row_class'].'">' . PHP_EOL;
      $string .= '<td class="state-marker"></td>' . PHP_EOL;

      if ($events['short'])
      {
        $string .= '    <td class="syslog" style="white-space: nowrap">';
        $timediff = $GLOBALS['config']['time']['now'] - strtotime($entry['timestamp']);
        $string .= generate_tooltip_link('', formatUptime($timediff, "short-3"), format_timestamp($entry['timestamp']), NULL) . '</td>' . PHP_EOL;
      } 
      else
      {
        $string .= '    <td style="width: 160px">';
        $string .= format_timestamp($entry['audit_time']) . '</td>' . PHP_EOL;
      }

      $string .= '    <td style="width: 160px">';
      $popid = $entry['pop_id'] ;
      $pname = $popdetails_array[$popid] ; //dbFetchCell('SELECT `name` FROM `nxg_pop_details` WHERE `id` = ' . $entry['pop_id']) ;
      $string .= $pname . '</td>' . PHP_EOL ;

      $string .= '    <td style="width: 160px">';
      $string .= $entry['user_name'] . '</td>' . PHP_EOL ;

      $string .= '    <td style="width: 160px;rowspan="2">';
      $string .= $entry['module_name'] . '</td>' . PHP_EOL ;

      $string .= '    <td>' ;
      $string .= $entry['commit_comment'] . '<br>' . $entry['syslog_commit'] . '</td>' . PHP_EOL;

      //$string .= '    <td style="width: 160px;">';
      //$string .= $entry['syslog_commit'] . '</td>' . PHP_EOL ;

      if(!strcmp($entry['commit_match'], 'Foreign'))
      {
        $string .= '    <td style="width: 160px"><font color="red">';
        $string .= $entry['commit_match'] . '</font></td>' . PHP_EOL ;
      }
      else
      {
        $string .= '    <td style="width: 160px"><font color="green">';
        $string .= $entry['commit_match'] . '</font></td>' . PHP_EOL ;
      }

      if(!strcmp($entry['commit_status'], 'Attempted') || !strcmp($entry['commit_status'], 'Fail'))
      {
        $string .= '    <td style="width: 150px"><font color="red">';
        $string .= $entry['commit_status'] . '</font></td>' . PHP_EOL ;
      }
      else
      {
        $string .= '    <td style="width: 150px">';
        $string .= $entry['commit_status'] . '</td>' . PHP_EOL ;
      }

      $order   = array("\r\n", "\n", "\r");
      $replace = '<br />';
      $sss = str_replace($order, $replace, $entry['commit_log']) ;
      $formatstr = '<pre>' . $sss . '</pre>' ;
      $newformatstr = htmlspecialchars($formatstr, ENT_QUOTES) ;
      $string .= '    <td class="pull_left"><textarea  class="" name="commit_log" type="text" disabled="disable">' . $entry['commit_log'] . '</textarea>' . '<br/><a class="btn btn-success" data-target="#popupModel" role="button" onclick="getData(\'' . $newformatstr . '\');"><i class="icon-circle icon-white"></i> Show </a></td>' . PHP_EOL ;

      //$string .= '    <td style="width: 200px; text-align: left;">' . $entry['commit_log'] . '<br/><a class="btn btn-success" data-target="#popupModel" role="button" onclick="getData(\'' . $entry['commit_log'] . '\'); return false; "><i class="icon-circle icon-white"></i> Show </a></td>' . PHP_EOL ;

      //$string .= $entry['commit_log'] . '</td>' . PHP_EOL ;
      $string .= '  </tr>' . PHP_EOL;
    }

    $string .= '  </tbody>' . PHP_EOL;
    $string .= '</table>';

    // Print pagination header
    if ($events['pagination_html']) { $string = $events['pagination_html'] . $string . $events['pagination_html']; }

    // Print events
    echo $string;
  }
}

function get_auditlog_events_array($vars)
{
  $array = array();

  // Short events? (no pagination, small out)
  $array['short'] = (isset($vars['short']) && $vars['short']);
  // With pagination? (display page numbers in header)
  $array['pagination'] = (isset($vars['pagination']) && $vars['pagination']);
  pagination($vars, 0, TRUE); // Get default pagesize/pageno
  $array['pageno']   = $vars['pageno'];
  $array['pagesize'] = $vars['pagesize'];
  $start    = $array['pagesize'] * $array['pageno'] - $array['pagesize'];
  $pagesize = $array['pagesize'];

  $param = array();
  $where = ' WHERE 1 ';
  foreach ($vars as $var => $value)
  {
    if ($value != '')
    {
      switch ($var)
      {
        case 'pop_id':
          $where .= generate_query_values($value, 'pop_id');
          break;
        case 'user_name':
          $where .= generate_query_values($value, 'user_name');
          break;
        case 'message':
          $where .= generate_query_values($value, 'commit_comment', '%LIKE%') ;
          break;
        case 'checklog':
          $where .= generate_query_values($value, 'commit_log', '%LIKE%') ;
          break ;
        case 'timestamp_from':
          $where .= ' AND `audit_time` >= ?';
          $param[] = $value;
          break;
        case 'timestamp_to':
          $where .= ' AND `audit_time` <= ?';
          $param[] = $value;
          break;
      }
    }
  }

  $query = 'FROM `nxg_auditlog` ';
  $query .= $where ;
  $query_count = 'SELECT COUNT(*) '.$query;
  $query_updated = 'SELECT MAX(`audit_time`) '.$query;

  $query = 'SELECT * '.$query;
  $query .= ' ORDER BY `id` DESC ';
  $query .= "LIMIT $start,$pagesize";
  // Query events
  $array['entries'] = dbFetchRows($query, $param);
  // Query events count
  if ($array['pagination'] && !$array['short'])
  {
    $array['count'] = dbFetchCell($query_count, $param);
    $array['pagination_html'] = pagination($vars, $array['count']);
  } else {
    $array['count'] = count($array['entries']);
  }

  // Query for last timestamp
  $array['updated'] = dbFetchCell($query_updated, $param);

  return $array;
}



?>

<div id="popupModel" class="modal fade" role="dialog">
  <div class="modal-dialog" >

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Commit check log</h4>
      </div>
      <div class="modal-body" id="auditlog_config">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

  </div> <!-- col-md-12 -->

</div> <!-- row -->

<?php
?>

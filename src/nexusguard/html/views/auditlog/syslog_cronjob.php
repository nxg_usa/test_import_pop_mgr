#!/usr/bin/env php
<?php

/**
 * Observium Network Management and Monitoring System
 * Copyright (C) 2006-2015, Adam Armstrong - http://www.observium.org
 *
 * @package    observium
 * @subpackage webui
 * @author     Adam Armstrong <adama@observium.org>
 * @copyright  (C) 2006-2013 Adam Armstrong, (C) 2013-2015 Observium Limited
 *
 */

chdir(dirname($argv[0]));

include_once("../../../../includes/defaults.inc.php");
include_once("../../../../config.php");

include_once("../../../../includes/definitions.inc.php");
include("../../../../includes/functions.inc.php");
include("../../../../includes/discovery/functions.inc.php");

///FIXME. Mike: should be more checks, at least a confirmation click.
//if ($vars['action'] == "expunge" && $_SESSION['userlevel'] >= '10')
//{
//  dbFetchCell('TRUNCATE TABLE `eventlog`');
//  print_message('Event log truncated');
//}
global $config ;

$seqFolderName = $config['html_dir'] . "/nexusguard/views/auditlog/" ;
$seqFileName = $seqFolderName . "seq" ;
$count = 0 ;
if(file_exists($seqFileName))
{
    $seqFileHandle = fopen($seqFileName, "r") ;
    $count = fread($seqFileHandle, filesize($seqFileName)) ;
    fclose($seqFileHandle) ;
}

print("File count = " . $count) ;
$lastSeqNumber = dbFetchCell('SELECT `seq` FROM `syslog` ORDER BY `seq` DESC LIMIT 1') ;
print("Last count = " . $lastSeqNumber . "\n") ;
$seqFileHandle = fopen($seqFileName, "w") ;
fwrite($seqFileHandle, $lastSeqNumber) ;
fclose($seqFileHandle) ;

foreach(dbFetchRows('SELECT `priority`, `program`, `device_id`, `timestamp`, `msg`, `seq` FROM `syslog` WHERE `msg` LIKE \'%UI_COMMIT:%\' and `seq` BETWEEN ' . $count . ' AND ' . $lastSeqNumber . ' ORDER BY `seq` DESC') as $syslogdetails)
{
    $device_commit_status = 'Attempted' ;
    $iscommit_complete = dbFetchCell('SELECT COUNT(*) FROM `syslog` WHERE `msg` LIKE \'%UI_COMMIT_COMPLETED%\' and `seq` BETWEEN ' . $count . ' AND ' . $lastSeqNumber . ' ORDER BY `seq` DESC') ;
    if($iscommit_complete)
    {
        $device_commit_status = 'Success' ;
    }
    // get the device id
    $deviceid = $syslogdetails['device_id'] ;
    // syslog message
    $msg = $syslogdetails['msg'] ;
    // form the syslog message with timestamp and program
    $syslog_message = $syslogdetails['timestamp'] . '  ' . $syslogdetails['program'] . ':' . $msg ;
    // get the pop id from the device id 
    $pop_id = dbFetchCell('SELECT `id` FROM `nxg_pop_details` WHERE `pop_edge_router_device_id` = ' . $deviceid) ;
    // regex the message to get username and user comment
    preg_match('/UI_COMMIT: User \'(.*?)\' requested \'(.*?)\' operation \(comment: ([^\)]*)\)/', $msg, $match) ;
    // user name
    $username = $match[1] ; 
    // commit comment
    $syscomment = $match[3] ;
    print("syslog = " . $syscomment . " pop id = " . $pop_id . " device id = " . $deviceid . "\n") ;
    // if the message already present means the commit from the browser
    $isrowpresent = dbFetchCell('SELECT COUNT(*) FROM `nxg_auditlog` WHERE `pop_id` = ' . $pop_id . ' AND `commit_comment` LIKE \'%' . $syscomment . '%\'') ;    
    if($isrowpresent)
    {
        print("**** updating the row\n") ;
        print("Status = " . $device_commit_status . "\n") ;
        dbUpdate(array('syslog_commit'=>$syslog_message, 'commit_match'=>'Matching'), 'nxg_auditlog', '`pop_id` = ? AND `commit_comment` = ?', array($pop_id, $syscomment)) ;
        break ;
    }
    else
    {
        // Below case will never come as its the incremented count of seq number. Adding just for the safer side in case someone manually changes the seq number from file
        // or delete the file. This will prevent in adding the duplicate entires in the table
        $countquery = 'SELECT COUNT(*) FROM `nxg_auditlog` WHERE `pop_id`=' . $pop_id . ' AND `user_name`=\'' . $username . '\' AND `syslog_commit`=\'' . $syscomment . '\' AND `audit_time`=\'' . $syslogdetails['timestamp'] . '\'' ;
        $insertcount = dbFetchCell($countquery) ;
        if($insertcount == 0)
        {
            print("**** inserting row \n") ;
            $ret = dbInsert(array('pop_id'=>$pop_id, 'user_name'=>$username, 'syslog_commit'=>$syslog_message, 'audit_time'=>$syslogdetails['timestamp'], 'commit_match'=>'Foreign', 'commit_status'=>$device_commit_status), 'nxg_auditlog') ;
        }
    }
}

?>


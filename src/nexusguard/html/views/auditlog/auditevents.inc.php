<?php

/**
 * Observium
 *
 *   This file is part of Observium.
 *
 * @package    observium
 * @subpackage web
 * @copyright  (C) 2006-2013 Adam Armstrong, (C) 2013-2015 Observium Limited
 *
 */

/**
 * Display events.
 *
 * Display pages with device/port/system events on some formats.
 * Examples:
 * print_events() - display last 10 events from all devices
 * print_events(array('pagesize' => 99)) - display last 99 events from all device
 * print_events(array('pagesize' => 10, 'pageno' => 3, 'pagination' => TRUE)) - display 10 events from page 3 with pagination header
 * print_events(array('pagesize' => 10, 'device' = 4)) - display last 10 events for device_id 4
 * print_events(array('short' => TRUE)) - show small block with last events
 *
 * @param array $vars
 * @return none
 *
 */
function print_auditlog_events($vars)
{
 print_warning('<h4>IN function!</h4>') ;
}
function print_auditlog_event_1($vars)
{
  // Get events array
  //$events = get_auditlog_events_array($vars);

  if (!$events['count'])
  {
    // There have been no entries returned. Print the warning.
    print_warning('<h4>No audit log entries found!</h4>');
  } else {
    // Entries have been returned. Print the table.
    $string = '<table class="table table-bordered table-striped table-hover table-condensed-more">' . PHP_EOL;
    if (!$events['short'])
    {
      $string .= '  <thead>' . PHP_EOL;
      $string .= '    <tr>' . PHP_EOL;
      $string .= '      <th class="state-marker"></th>' . PHP_EOL;
      $string .= '      <th>Date</th>' . PHP_EOL;
      $string .= '      <th>POP Name</th>' . PHP_EOL; 
      $string .= '      <th>Configuration Changed By</th>' . PHP_EOL;
      $string .= '      <th>Module</th>' . PHP_EOL;
      $string .= '      <th>Commit Message</th>' . PHP_EOL;
      $string .= '      <th>Commit Status</th>' . PHP_EOL;
      $string .= '      <th>Commit Log</th>' . PHP_EOL;
      $string .= '    </tr>' . PHP_EOL;
      $string .= '  </thead>' . PHP_EOL;
    }
    $string   .= '  <tbody>' . PHP_EOL;

    foreach ($events['entries'] as $entry)
    {

      #$icon = geteventicon($entry['message']);
      #if ($icon) { $icon = '<img src="images/16/' . $icon . '" />'; }

      $string .= '  <tr class="'.$entry['html_row_class'].'">' . PHP_EOL;
      $string .= '<td class="state-marker"></td>' . PHP_EOL;

      if ($events['short'])
      {
        $string .= '    <td class="syslog" style="white-space: nowrap">';
        $timediff = $GLOBALS['config']['time']['now'] - strtotime($entry['timestamp']);
        $string .= generate_tooltip_link('', formatUptime($timediff, "short-3"), format_timestamp($entry['timestamp']), NULL) . '</td>' . PHP_EOL;
      } else {
        $string .= '    <td style="width: 160px">';
        $string .= format_timestamp($entry['timestamp']) . '</td>' . PHP_EOL;
      }

      $string .= '    <td = style="width: 160px">';
      $string .= $entry['pop_name'] . '</td>' . PHP_EOL ;

      $string .= '    <td = style="width: 160px">';
      $string .= $entry['user_name'] . '</td>' . PHP_EOL ;
      
      $string .= '    <td = style="width: 160px">';
      $string .= $entry['module_name'] . '</td>' . PHP_EOL ;

      if ($events['short'])
      {
        $string .= '    <td class="auditlog">';
        if (strpos($entry['commit_comment'], $entry['link']) !== 0)
        {
          $string .= $entry['link'] . ' ';
        }
      } else {
        $string .= '    <td>';
      }
      $string .= escape_html($entry['commit_comment']) . '</td>' . PHP_EOL;

      $string .= '    <td = style="width: 160px">';
      $string .= $entry['commit_status'] . '</td>' . PHP_EOL ;

      $string .= '    <td = style="width: 160px">';
      $string .= $entry['commit_log'] . '</td>' . PHP_EOL ;
      $string .= '  </tr>' . PHP_EOL;
    }

    $string .= '  </tbody>' . PHP_EOL;
    $string .= '</table>';

    // Print pagination header
    if ($events['pagination_html']) { $string = $events['pagination_html'] . $string . $events['pagination_html']; }

    // Print events
    echo $string;
  }
}

/**
 * Display short events.
 *
 * This is use function:
 * print_events(array('short' => TRUE))
 *
 * @param array $vars
 * @return none
 *
 */
function print_events_short($var)
{
  $var['short'] = TRUE;
  print_events($var);
}

/**
 * Params:
 * short
 * pagination, pageno, pagesize
 * device_id, entity_id, entity_type, message, timestamp_from, timestamp_to
 */
function get_auditlog_events_array($vars)
{
  $array = array();

  // Short events? (no pagination, small out)
  $array['short'] = (isset($vars['short']) && $vars['short']);
  // With pagination? (display page numbers in header)
  $array['pagination'] = (isset($vars['pagination']) && $vars['pagination']);
  pagination($vars, 0, TRUE); // Get default pagesize/pageno
  $array['pageno']   = $vars['pageno'];
  $array['pagesize'] = $vars['pagesize'];
  $start    = $array['pagesize'] * $array['pageno'] - $array['pagesize'];
  $pagesize = $array['pagesize'];

  // Begin query generate
  $param = array();
  $where = ' WHERE 1 ';
  foreach ($vars as $var => $value)
  {
    if ($value != '')
    {
      switch ($var)
      {
        case 'pop_id':
          $where .= generate_query_values($value, 'pop_id');
          break;
        case 'user_name':
          $where .= generate_query_values($value, 'user_name');
          break;
        case 'commit_message':
          $where .= generate_query_values($value, 'commit_comment, '%LIKE%') ;
          break;
        case 'timestamp_from':
          $where .= ' AND `audit_time` >= ?';
          $param[] = $value;
          break;
        case 'timestamp_to':
          $where .= ' AND `audit_time` <= ?';
          $param[] = $value;
          break;
      }
    }
  }

  $query = 'FROM `nxg_auditlog` ';
  $query .= $where ;
  $query_count = 'SELECT COUNT(*) '.$query;
  $query_updated = 'SELECT MAX(`audit_time`) '.$query;

  $query = 'SELECT * '.$query;
  $query .= ' ORDER BY `id` DESC ';
  $query .= "LIMIT $start,$pagesize";

  // Query events
  $array['entries'] = dbFetchRows($query, $param);

  // Query events count
  if ($array['pagination'] && !$array['short'])
  {
    $array['count'] = dbFetchCell($query_count, $param);
    $array['pagination_html'] = pagination($vars, $array['count']);
  } else {
    $array['count'] = count($array['entries']);
  }

  // Query for last timestamp
  $array['updated'] = dbFetchCell($query_updated, $param);

  return $array;
}

// EOF

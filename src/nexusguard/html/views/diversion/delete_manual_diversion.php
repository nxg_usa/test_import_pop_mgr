<link rel="stylesheet" href="/nexusguard/css/pop_manager.css" >
<link rel="stylesheet" href="/nexusguard/css/bootstrap-panel.css" >
<script src="/nexusguard/js/common.js"></script>
<?php
include '/opt/observium/nexusguard/db/db_static_route_functions.php';


$diversion_id = $vars['diversion'];
$static_diversion_id =dbFetchRows('select * from nxg_static_route where id='.$diversion_id );



$diversion_type = $static_diversion_id[0]['diversion_type'];
$pop_name = $static_diversion_id[0]['pop_name'];
$pop_id = $static_diversion_id[0]['pop_id'];
$route_name = $static_diversion_id[0]['route_name'];
$network_prefix = $static_diversion_id[0]['network_prefix'];
$network_prefix_subnet = $static_diversion_id[0]['network_prefix_subnet'];
$nexthop = $static_diversion_id[0]['next_hop'];
$protocol = $static_diversion_id[0]['protocol'];
$comment = $static_diversion_id[0]['comment'];

?>



<script>

var button_array=['cancel','decommission'];
$(document).ready(function () {
     $( "#decommission" ).click(function() {
            if(confirm("Confirm Delete Static Route?") !=true){
                return;
            }
            commit_check('delete_static_route','/nexusguard/api/del_static_route.php');
           });

    $( "#cancel_config" ).click(function() {
        $("#isp_config").hide();
        $("#isp_form :input").prop('readonly', false);
    });
    $( "#applyconfig" ).click(function() {
            apply_config('delete_static_route','/nexusguard/api/del_static_route.php',button_array);

    });

});


</script>

<h3 class="form_heading">Delete Static Route</h3>
    <form id="delete_static_route" method="post" >
        <div class="row">
    <div class="col-md-6">

      <div class="widget widget-table">
        <div class="widget-header">
          <i class="oicon-gear"></i><h3>Delete Static Route</h3>
        </div>
            <div style="padding-top: 10px;" class="widget-content">
        <table class="form_table">
           <tr>
                <td  class="pull_right">Diversion Type</td>
                <td class="pull_left">
                     <select name="diversion_type" disabled="disabled" >
                     <?php
                               $str ='<option value="'.$diversion_type.'">'.$diversion_type.'</option>';
                               echo $str;
                      ?>
                     </select>
                </td>
            </tr>

            <tr>
                <td  class="pull_right">PoP</td>
                <td class="pull_left">
                     <select id="pop_id" name="pop_id" disabled="disabled" >
                            <?php
                               $str ='<option value="'.$pop_id.'">'.$pop_name.'</option>';
                               echo $str;

        
                                ?>
                     </select>
                </td>
            </tr>
            <tr>
                <td class="pull_right">Route Name</td>
                <?php
                 $str = '<td><input type="text" name="route_name" disabled="disabled"  id="route_name" value="'.$route_name.'"/></td>';
                echo $str;
                   ?>
            <tr>
            <tr>
                <td class="pull_right">Network Prefix</td>
                <?php
                $str = '<td ><input type="text" disabled="disabled"  class="pull_left" name="network_prefix_ip" value="'.$network_prefix.'"/></td>
                <td>/<input type="text" class="subnet"  disabled="disabled"  name="network_prefix_subnet_ip" value="'.$network_prefix_subnet.'"/></td></tr>';
                echo $str;
                ?>
<!--            <tr>
                <td  class="pull_right">Next hop</td>
                <?php

                 #$str =  '<td><input type="text" disabled="disabled" class="" name="next_hop" value="'.$nexthop.'"/></td>';
                  #  echo $str;
                 ?>-->
            </tr>
             <tr>
                <td  class="pull_right">Protocol</td>
                <?php
                    $str = '<td><input type="text" disabled="disabled"  class="" name="protocol" value="'.$protocol.'"/></td>';
                    echo $str;
                ?>
            </tr>
            <tr>
                <?php
                    $str = '<td><input type="hidden" disabled="disabled"  class="" name="diversion_id" value="'.$diversion_id.'"/></td>';
                    echo $str;
                ?>
            </tr>


            <tr>
                <td  class="pull_right">Comment</td>
                <?php
                    $str = '<td class="pull_left"><textarea disabled="disabled" class="input" name="comment"  value="'.$comment.'"></textarea></td></tr>';
                    echo $str;
                ?>
            </tr>
</table>

</div> <!-- end of widget-content -->
      </div> <!-- End of widget-table -->
    </div> <!-- End of col-md-6 -->
  </div>  <!-- End of row -->

<input type="hidden" name="action" value="commit_check"/>


</form>
<div id="result_panel" class="panel-group">
<?php include "nexusguard/views/includes/commit_check_footer2.php"; ?>
<?php include "nexusguard/views/includes/commit_check_footer.php"; ?>

</div>
<div class="form-actions">
                <input type='button' class='btn btn-primary' onclick="location.href='pop_mgr/view=traffic_diversion/'" name='cancel_button' id='cancel' value="Cancle">
                <input type='button' data-target="#myModal" class='btn btn-primary' name='decommision' id = "decommission" value="Delete"/>
            </div>

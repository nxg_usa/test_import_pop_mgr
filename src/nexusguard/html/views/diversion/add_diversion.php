<link rel="stylesheet" href="/nexusguard/css/pop_manager.css" >
<link rel="stylesheet" href="/nexusguard/css/bootstrap-panel.css" >
<script src="/nexusguard/js/common.js"></script>
<script>

var button_array = ['cancel','commit_check'];
$(document).ready(function(){
     $( "#commit_check" ).click(function() {
            commit_check_exabgp('add_diversion_form','/nexusguard/api/add_diversion.php');
    });
    $( "#cancel_config" ).click(function() {

        $("#isp_config").hide();
        $("#isp_form :input").prop('readonly', false);
         window.location("/pop_mgr/view=traffic_diversion/");
    });
    $( "#applyconfig" ).click(function() {

            apply_config_exabgp('add_diversion_form','/nexusguard/api/add_diversion.php',button_array);

    });
});



</script>

<script>

$(document).ready(function(){
        $('.diversion_type').hide();
        $('#all_diversion_type').hide();
         var pop_type = $("#pop_id option:selected").val();
        if(pop_type=="000"){
            $('#all_diversion_type').show();
            $('.diversion_type').hide();
        }else{
            $('#all_diversion_type').hide();
            $('.diversion_type').show();
        }
     $('.diversion_type').hide();
      $('select[id^="cust_"] ').each(function(){
                $(this).prop("selectedIndex",-1);
            });
     $('#cust_'+pop_type).show();
        
    $('#pop_id').on('change' ,function(){
        pop_type = $("#pop_id option:selected").val();
        if(pop_type=="000"){
        $('.diversion_type').hide();
        $('select[id^="cust_"] ').each(function(){
                $(this).prop("selectedIndex",-1);
            });
         $('#all_diversion_type').show();
        }else{
        $('#all_diversion_type').prop("selectedIndex",-1);
        $('#all_diversion_type').hide();
         $('.diversion_type').hide();
         $('select[id^="cust_"] ').each(function(){
                $(this).prop("selectedIndex",-1);
            });
        $('#cust_'+pop_type).show();
}
    });
});


</script>
<h3 class="form_heading">Add Diversion</h3>
    <form id="add_diversion_form" method="post" >
        <div class="row">
    <div class="col-md-6">

      <div class="widget widget-table">
        <div class="widget-header">
          <i class="oicon-gear"></i><h3>Add Diversion</h3>
        </div>
            <div style="padding-top: 10px;" class="widget-content">
        <table class="form_table">
            <tr>
                <td class="pull_right">Diversion Name *</td>
                <td ><input type="text" name="diversion_name" value="" /></td>
            </tr>

            <tr>
                <td  class="pull_right">PoP</td>
                <td class="pull_left">
                     <select id="pop_id" name="pop_id">


                               <option value="000">All</option>
                               <?php
                               $all_pop=dbFetchRows('select id,pop_name from nxg_pop_details');
                               foreach($all_pop as $pop_name){
                               $str ='<option value='.$pop_name['id'].'>'.$pop_name['pop_name'].'</option>';
                               echo $str;
                               }


                                ?>
                     </select>
                </td>
            </tr>

            <tr>
                <td  class="pull_right">Diversion Type</td>
                <td class="pull_left">
                
                    <?php
                            foreach( $all_pop as  $pop)
                            {
                                $str1="";
                                echo "<select class='diversion_type'  name='diversion_type'  id='cust_".$pop['id']."' style='display :none;'>";
                                $db_data=  dbFetchRows('select * from nxg_router_interface_mapping where scrubber_type ="custom" and  pop_details_id='.$pop['id']);

                                $db_avail_data=  dbFetchRows('select * from nxg_router_interface_mapping where scrubber_type !="custom" and  pop_details_id='.$pop['id']);

                                foreach($db_avail_data as $data)
                                {
                                    $upper=strtoupper($data['scrubber_type']);
                                    if($upper == "TDTMS"){
                                    $upper = "TD+TMS";
                                    }
                                    $str1 .="<option value='".$data['scrubber_type']."'>".$upper."</option>";
                                }

                                foreach($db_data as $data)
                                {
                                    //$upper=strtoupper($data['scrubber_custom_name']);
                                    $str1 .="<option value='custom~~~".$data['scrubber_custom_name']."'>".$data['scrubber_custom_name']."</option>";
                                    
                                }
                                

                                $str1 .="</select>";
                                echo $str1;
                             }
                    ?>
                    <select  name="diversion_type" value="diversion_type" id="all_diversion_type" style='display :none;'  >
                        <!--<option>TD</option>
                        <option>TMS</option>
                        <option value='tdtms'>TD+TMS</option>-->
                       <?php 
                                $pop_count = dbFetchRow('select count(id) as count from nxg_pop_details');
                                $pop_count_for_td= dbFetchRow('select count(pop_details_id) as td_pop from nxg_router_interface_mapping where scrubber_type="td"');
                                $pop_count_for_tms= dbFetchRow('select count(pop_details_id) as tms_pop from nxg_router_interface_mapping where scrubber_type="tms"');
                                $pop_count_for_tdtms= dbFetchRow('select count(pop_details_id) as tdtms_pop from nxg_router_interface_mapping where scrubber_type="tdtms"');
                                $pop_count_for_custom= dbFetchRow('select count(distinct nxg_router_interface_mapping.pop_details_id) as custom_pop from nxg_router_interface_mapping where scrubber_type="custom"');
                                $custom_name="";
                                if($pop_count['count']==$pop_count_for_td['td_pop']){
                                echo "<option value='td'>"."TD"."</option>";            
                                }
                                if($pop_count['count']==$pop_count_for_tms['tms_pop']){
                                echo "<option value='tms'>"."TMS"."</option>";
                                }
                                if($pop_count['count']==$pop_count_for_tdtms['tdtms_pop']){
                                echo "<option value='tdtms'>"."TD+TMS"."</option>";
                                }
                                if($pop_count['count']==$pop_count_for_custom['custom_pop']){
                                       $pop_count_for_custom= dbFetchRows('select * from nxg_router_interface_mapping where scrubber_type="custom"');        
                                        foreach($pop_count_for_custom as $custom){
                                        $custom_name=$custom['scrubber_custom_name'];
                                        break;
                                        }
                            $count_for_custom_name=dbFetchRow('select count(pop_details_id) as custom_pop from nxg_router_interface_mapping where scrubber_type="custom" and scrubber_custom_name="'.$custom_name.'"');
                    
                                        if($pop_count['count']==$count_for_custom_name['custom_pop']){
                                                echo "<option value='custom~~~".$custom_name."'>"."$custom_name"."</option>";
                                        }
                                }

                        ?>
                    </select>

                </td>
            </tr>

            <tr>
                <td class="pull_right">Source IP</td>
                <td ><input type="text" class="pull_left" name="source_ip"/></td>
                <td>/<input type="text" class="subnet" name="source_subnet"/></td></tr>
            <tr>
            <tr>
                <td class="pull_right">Source Port</td>
                <td class="pull_left"><input type="text" name="source_port" value="" /></td>
            </tr>
            <tr>
                <td class="pull_right">Destination IP</td>
                <td ><input type="text" class="pull_left" name="network_prefix_ip"/></td>
                <td>/<input type="text" class="subnet" name="network_prefix_subnet_ip"/></td></tr>
            <tr>
            <tr>
                <td class="pull_right">Destination Port</td>
                <td class="pull_left"><input type="text"  name="port_no" value="" /></td></tr>
            <tr>
                <td  class="pull_right">Protocol</td>
                <td  class="pull_left">
                    <select  name="protocol" value="" >
                            <option value=""></option>
                            <option value="ah">ah</option>
                            <option value="egp">egp</option>
                            <option value="esp">esp</option>
                            <option value="gre">gre</option>
                            <option value="icmp">icmp</option>
                            <option value="icmp">icmp6</option>
                            <option value="igmp">igmp</option>
                            <option value="ipip">ipip</option>
                            <option value="ospf">ospf</option>
                            <option value="pim">pim</option>
                            <option value="rsvp">rsvp</option>
                            <option value="sctp">sctp</option>
                            <option value="tcp">tcp</option>
                            <option value="udp">udp</option>
                   </select>
                </td>
<!--            <tr>
                <td class="pull_right">MD5 Keystring &nbsp;&nbsp;<font size="3" color="red">*</font></td>
                <td class="pull_left"><input type="text"  name="md5_keystring" value=""  /></td>
            </tr>-->
            </tr>
                <td  class="pull_right">Comment</td>
                <td class="pull_left"><textarea class="input" name="comment" value="" ></textarea></td>
            </tr>

</table>
</div> <!-- end of widget-content -->
      </div> <!-- End of widget-table -->
    </div> <!-- End of col-md-6 -->
  </div>  <!-- End of row -->
<input type='hidden' name='action' value='commit_check'/>

</form>
<?php include 'nexusguard/views/includes/commit_check_footer2.php'; ?>
<?php include 'nexusguard/views/includes/commit_check_footer.php'; ?>

 <div class="form-actions">

        <input type="button" class="btn btn-primary" id="cancel" name="cancel" value="Cancel" onclick="location.href='pop_mgr/view=traffic_diversion/'"/>
        <input type="button" class="btn btn-primary" id="commit_check" name="Commit Check" value="Commit Check"/>
    </div>


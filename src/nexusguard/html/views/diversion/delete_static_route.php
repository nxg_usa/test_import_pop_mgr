<?php
include_once('/opt/observium/html/nexusguard/views/includes/common_includes.php');

//retrieve contents

$data = json_decode(file_get_contents('php://input'), true);
$dbdata = dbFetchRows();

$dbdata = dbFetchRows('select route_name,pop_id from nxg_static_route where id='.$data);
//echo  json_encode($dbdata);
$route_name = $dbdata[0]['route_name'];
$pop_id = $dbdata[0]['pop_id'];
$pop_details = get_sinlge_pop_detail($pop_id);
$device_details = dbFetchRow("select hostname from devices where device_id = ?",array($pop_details['pop_edge_router_device_id']));

$device_username = $pop_details['username'];
$hostname = $device_details['hostname'];
$device_password = $pop_details['password'];
$action = "commit";
$epoch_time = time();

$json_tmp_filename="delete_diversion"."_".$epoch_time.".json";
$config_tmp_filename = "delete_diversion"."_".$epoch_time.".set";
$commit_check_tmp_filename = "delete_diversion"."_".$epoch_time."_commit_check.log";
$commit_log_tmp_filename = "delete_diversion"."_".$epoch_time.".log";


$json_filename = $json_dir.$json_tmp_filename;
$config_filename = $config_dir.$config_tmp_filename;
$commit_check_filename = $commit_check_dir.$commit_check_tmp_filename;
$commit_log_filename = $commit_log_dir.$commit_log_tmp_filename;

$db_route_name = array($dbdata[0]['route_name']);
$array = array();
$array['route_name'] =$db_route_name;
$array['conf_file'] = $config_filename;
$array['commit_check_log'] = $commit_check_filename;
$array['commit_log'] = $commit_log_filename;
$array['json_file'] = $json_filename;
$fp = fopen($json_filename,'w');
fwrite($fp, str_replace("\\","", json_encode($array)));
fclose($fp);


$cmd = "/bin/bash -x /opt/observium/nexusguard/ansible/script/nxg_delete_manual_diversion.sh $hostname $device_username $device_password $action $json_filename";
exec($cmd,$result,$status);
if( $status != 0 )
{
    $err = shell_exec("cat ".$commit_log_filename);
    write_error_response("ccerr01","Error in executing commit check<br><pre>".$err."</pre>");
}else{
dbDelete('nxg_static_route', "`id` =$data", array($data));
}

?>


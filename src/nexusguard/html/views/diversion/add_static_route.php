<?php
include '/opt/observium/nexusguard/db/db_static_route_functions.php';
?>
<link rel="stylesheet" href="/nexusguard/css/pop_manager.css" >
<link rel="stylesheet" href="/nexusguard/css/bootstrap-panel.css" >
<script src="/nexusguard/js/common.js"></script>
<script>
$(document).ready(function ()
{
    var form_data ={};
    var button_array = ["cancel","commit_check"];
    $("#commit_check" ).click(function()
    {
        var request_array=[];
        var j=0;
        var pop_type = $("#pop_id option:selected").val();
        if(pop_type == "000")
        {
            $("#pop_id > option").each(function()
            {
             if( this.value !="000")
            {    
                form_data = {};
                form_data = parse_form("add_static_route");
                form_data.pop_id = $(this).val();
                form_data.action = "commit_check";
                req = [];
                req["url"] = "/nexusguard/api/add_static_route.php" ;
                req["apply_url"] = "/nexusguard/api/add_static_route.php" ;
                req["data"] = form_data ;
                req["entity_name"] =  "ADD STATIC ROUTE - " +  $(this).text();
                request_array[j]=req;
                j++;
    
                commit_check2(request_array,button_array);
            }
            });
        }
        else
        {
                form_data = {};
                form_data = parse_form("add_static_route");
                form_data.pop_id = $("#pop_id option:selected").val();
                form_data.action = "commit_check";
                req = [];
                req["url"] = "/nexusguard/api/add_static_route.php" ;
                req["apply_url"] = "/nexusguard/api/add_static_route.php" ;
                req["data"] = form_data ;
                req["entity_name"] = "ADD STATIC ROUTE - " +  $(this).text();
                request_array[j]=req;
                commit_check2(request_array,button_array);
                
        }
    });
});

</script>


<script>

$(document).ready(function(){
        $('.diversion_type').hide();
        $('#all_diversion_type').hide();
        var pop_type = $("#pop_id option:selected").val();
        if(pop_type=="000"){
            $('#all_diversion_type').show();
            $('.diversion_type').hide();
        }else{
            $('#all_diversion_type').hide();
            $('.diversion_type').show();
        }
        $('.diversion_type').hide();
        $('select[id^="cust_"] ').each(function(){
                $(this).prop("selectedIndex",-1);
            });

        $('#cust_'+pop_type).show();

        $('#pop_id').on('change' ,function(){
        pop_type = $("#pop_id option:selected").val();
        if(pop_type=="000"){
        $('.diversion_type').hide();
        $('select[id^="cust_"] ').each(function(){
                $(this).prop("selectedIndex",-1);
            });
         $('#all_diversion_type').show();
        }else{
        $('#all_diversion_type').prop("selectedIndex",-1);
        $('#all_diversion_type').hide();
        $('.diversion_type').hide();
        $('select[id^="cust_"] ').each(function(){
                $(this).prop("selectedIndex",-1);
            });
        $('#cust_'+pop_type).show();
}
    });
});

</script>

<h3 class="form_heading">Add Static Route</h3>
    <form id="add_static_route" method="post" >
        <div class="row">
             <div class="col-md-6">
                 <div class="widget widget-table">
                    <div class="widget-header">
                      <i class="oicon-gear"></i><h3>Add Static Route</h3>
                    </div>
                 <div style="padding-top: 10px;" class="widget-content">
                 <table class="form_table" id="#form_data">
                    <tr>
                        <td  class="pull_right">PoP</td>
                        <td class="pull_left">
                            <select id="pop_id" name="pop_id">
                                <option value="000">All</option>
                            <?php
                                $pop_names=getPop();
                                foreach($pop_names as $pop_name)
                                {
                                    $str ='<option value="'.$pop_name['id'].'">'.$pop_name['pop_name'].'</option>';
                                    echo $str;
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td  class="pull_right">Diversion Type</td>
                        <td class="pull_left">
                        <?php
                            foreach( $pop_names as  $pop)
                            {
                                $str1="";
                                echo "<select class='diversion_type'  name='diversion_type'  id='cust_".$pop['id']."' syyle='display :none;'>";
                                $db_data=  dbFetchRows('select * from nxg_router_interface_mapping where scrubber_type !="custom" and  pop_details_id='.$pop['id']);
                                $avail_custom_type =  dbFetchRows('select * from nxg_router_interface_mapping where scrubber_type ="custom" and  pop_details_id='.$pop['id']);

                                foreach($db_data as $data)
                                {
                                    $upper = strtoupper($data['scrubber_type']);
                                    $str1 .="<option value='".$data['scrubber_type']."'>".$upper."</option>";
                                }
                                 foreach($avail_custom_type as $data)
                                {

                                    $str1 .="<option value='".$data['scrubber_custom_name']."'>".$data['scrubber_custom_name']."</option>";
                                }

                                $str1 .="</select>";
                                echo $str1;
                             }
                        ?>
                                 <select  name="diversion_type" value="diversion_type" id="all_diversion_type" style='display :none;'  >
                                     <option>TD</option>
                                     <option>TMS</option>
                                     <option value='tdtms'>TD+TMS</option>
                                 </select>
                         </td>
                    </tr>
                    <tr>
                         <td class="pull_right">Route Name * </td>
                         <td><input type="text" name="route_name" id="route_name"/></td>
                    <tr>
                    <tr>
                        <td class="pull_right">Network Prefix * </td>
                        <td ><input type="text" class="pull_left" name="network_prefix_ip"/></td>
                        <td>/<input type="text" class="subnet" name="network_prefix_subnet_ip"/></td></tr>
                    <tr>
                        <td  class="pull_right">Protocol</td>
                        <td>
                            <select name="protocol">
                                <option value=""></option>
                                <option value="ah">ah</option>
                                <option value="egp">egp</option>
                                <option value="esp">esp</option>
                                <option value="gre">gre</option>
                                <option value="icmp">icmp</option>
                                <option value="icmp">icmp6</option>
                                <option value="igmp">igmp</option>
                                <option value="ipip">ipip</option>
                                <option value="ospf">ospf</option>
                                <option value="pim">pim</option>
                                <option value="rsvp">rsvp</option>
                                <option value="sctp">sctp</option>
                                <option value="tcp">tcp</option>
                                <option value="udp">udp</option>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <td  class="pull_right">Comment</td>
                        <td class="pull_left"><textarea class="input" name="comment"></textarea></td></tr>
                    </tr>
                </table>

            </div> <!-- end of widget-content -->
        </div> <!-- End of widget-table -->
    </div> <!-- End of col-md-6 -->
  </div>
  <!-- End of row -->
  <input type="hidden" name="action" value="commit_check" />
</form>
    <?php include 'nexusguard/views/includes/commit_check_footer2.php'; ?>
    <div class="form-actions">

        <input type="button" class="btn btn-primary" id="cancel" name="cancel" value="Cancel" onclick="location.href='pop_mgr/view=traffic_diversion/'"/>
        <input type="button" class="btn btn-primary" id="commit_check" name="Commit Check" value="Commit Check"/>
    </div>


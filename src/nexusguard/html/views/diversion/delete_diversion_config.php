<?php

include_once("/opt/observium/html/nexusguard/api/common/common.inc.php");
include_once("/opt/observium/includes/defaults.inc.php");
include_once("/opt/observium/config.php");
include_once("/opt/observium/includes/definitions.inc.php");
include_once("/opt/observium/nexusguard/common/functions.inc.php");
include_once '/opt/observium/nexusguard/resource/ip_mgmt_config.php';
include_once('/opt/observium/nexusguard/db/db_add_diversion.php');



function delete_diversion_config($diversion_id,$diversion_type){


$previous_diversion_all_config_for_local = array();
$previous_diversion_all_config_for_global = array();


$previous_config_from_database_pop_id = dbFetchRows("select distinct pop from nxg_diversion_details");


foreach ( $previous_config_from_database_pop_id as $content )
{
$previous_all_config_from_database = dbFetchRows('select name,source_ip,source_port,source_subnet,protocol,port,network_ip,network_prefix,target,local_speaker_ip,local_asn,peer_asn,global_speaker_ip,local_asn_global,peer_asn_global,local_server_ip,global_server_ip from  nxg_exabgp_details left join nxg_diversion_details on nxg_exabgp_details.pop_details_id=nxg_diversion_details.pop left join nxg_pop_interface_ip_mgmt on nxg_pop_interface_ip_mgmt.pop_id=nxg_exabgp_details.pop_details_id  left join nxg_router_interface_mapping on nxg_pop_interface_ip_mgmt.interface_unit=nxg_router_interface_mapping.vlan_id and nxg_pop_interface_ip_mgmt.interface_name=nxg_router_interface_mapping.device_interface_name where nxg_router_interface_mapping.scrubber_type="'.$diversion_type.'" and nxg_diversion_details.pop='.$content ['pop'].' and nxg_diversion_details.id !='.$diversion_id );
foreach($previous_all_config_from_database as $previous_config_from_database)
{
    
        $previous_diversion_array_for_local['diversion_name'] = $previous_config_from_database['name'];
        $previous_diversion_array_for_local['pop_id'] = $content ['pop'];
        $previous_diversion_array_for_local['network_prefix_ip'] = $previous_config_from_database['network_ip'];
        $previous_diversion_array_for_local['network_prefix_subnet_ip'] = $previous_config_from_database['network_prefix'];
        $previous_diversion_array_for_local['source_ip'] = $previous_config_from_database['source_ip'];
        $previous_diversion_array_for_local['source_subnet'] = $previous_config_from_database['source_subnet'];
        $previous_diversion_array_for_local['source_port'] = $previous_config_from_database['source_port'];
        $previous_diversion_array_for_local['protocol'] = $previous_config_from_database['protocol'];
        $previous_diversion_array_for_local['port_no'] = $previous_config_from_database['port'];
        $previous_diversion_array_for_local['local_speaker_ip']=$previous_config_from_database['local_speaker_ip'] ;
        $previous_diversion_array_for_local['local_server_ip']= $previous_config_from_database['local_server_ip'];
        $previous_diversion_array_for_local['local_asn']= $previous_config_from_database['local_asn'];
        $previous_diversion_array_for_local['peer_asn']= $previous_config_from_database['peer_asn'];
        $previous_diversion_array_for_local['target'] = "100:".$previous_config_from_database['target'];
        $previous_diversion_all_config_for_local[]=$previous_diversion_array_for_local;

        $previous_diversion_array_for_global['diversion_name'] = $previous_config_from_database['name'];
        $previous_diversion_array_for_global['pop_id'] = $content ['pop'];
        $previous_diversion_array_for_global['network_prefix_ip'] = $previous_config_from_database['network_ip'];
        $previous_diversion_array_for_global['network_prefix_subnet_ip'] = $previous_config_from_database['network_prefix'];
        $previous_diversion_array_for_global['source_ip'] = $previous_config_from_database['source_ip'];
        $previous_diversion_array_for_global['source_subnet'] = $previous_config_from_database['source_subnet'];
        $previous_diversion_array_for_global['source_port'] = $previous_config_from_database['source_port'];
        $previous_diversion_array_for_global['protocol'] = $previous_config_from_database['protocol'];
        $previous_diversion_array_for_global['port_no'] = $previous_config_from_database['port'];
        $previous_diversion_array_for_global['global_speaker_ip']=$previous_config_from_database['global_speaker_ip'] ;
        $previous_diversion_array_for_global['global_server_ip']= $previous_config_from_database['global_server_ip'];
        $previous_diversion_array_for_global['local_asn_global']= $previous_config_from_database['local_asn_global'];
        $previous_diversion_array_for_global['peer_asn_global']= $previous_config_from_database['peer_asn_global'];
        $previous_diversion_array_for_global['target'] = "100:".$previous_config_from_database['target'];
        $previous_diversion_all_config_for_global[]=$previous_diversion_array_for_global;

}
}



$result=array();
$result['local_server']['prev']=$previous_diversion_all_config_for_local;
$result['global_server']['prev']=$previous_diversion_all_config_for_global;
return $result;
}

 ?>

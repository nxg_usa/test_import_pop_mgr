<link rel="stylesheet" href="/nexusguard/css/pop_manager.css" >
<script src="/nexusguard/js/common.js"></script>
<script>
var button_array=['cancel','decommission'];

$(document).ready(function () {
     $( "#decommission" ).click(function() {
    
            if(confirm("Confirm Delete Exabgp Diversion?") !=true){
                return;
            }

            commit_check('delete_exabgp_route','/nexusguard/api/delete_exabgp_diversion_api.php');
           });

    $( "#cancel_config" ).click(function() {
        $("#isp_config").hide();
        $("#isp_form :input").prop('readonly', false);
    });
    $( "#applyconfig" ).click(function() {
            apply_config('delete_exabgp_route','/nexusguard/api/delete_exabgp_diversion_api.php',button_array);

    });

});


</script>
<?php
include_once('/opt/observium/html/nexusguard/views/includes/common_includes.php');

$unique_id = $vars['diversion'];

$data_id =dbFetchRows('select * from nxg_diversion_details where id='.$unique_id );

 
$diversion_name = $data_id[0]['name'];
$diversion_type = $data_id[0]['diversion_type'];
$network_ip = $data_id[0]['network_ip'];
$network_subnet_ip = $data_id[0]['network_prefix'];
$protocol = $data_id[0]['protocol'];
$port = $data_id[0]['port'];
$comment = $data_id[0]['comment'];
$pop = $data_id[0]['pop'];
$source_ip = $data_id[0]['source_ip'];
$source_subnet = $data_id[0]['source_subnet'];
$source_port = $data_id[0]['source_port'];
$md5_keystring = $data_id[0]['md5_keystring'];


?>

<h3 class="form_heading">Delete Diversion</h3>
    <form id="delete_exabgp_route" method="post" >
        <div class="row">
    <div class="col-md-6">

      <div class="widget widget-table">
        <div class="widget-header">
 <i class="oicon-gear"></i><h3>Delete Exabgp Route</h3>
        </div>
      <div style="padding-top: 10px;" class="widget-content">
        <table class="form_table">
            <tr>
                <td class="pull_right">Diversion Name</td>
                <?php
                $str = '<td ><input type="text"  name="diversion_name" value="'.$diversion_name.'" readonly/></td>
                                <td ><input type="text" style="display:none" name="unique_id" value="'.$unique_id.'" readonly/></td>';
                echo $str;
                 ?>
            </tr>

   <tr>
               <td  class="pull_right">Diversion Type</td>
                <td class="pull_left">
                    <select  name="Diversion_type" disabled="disabled">
                    <?php
                    $str = '<option value="'.$diversion_type.'">'.$diversion_type.'</option>';
                    echo $str;
                       ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td  class="pull_right">PoP</td>
                <td class="pull_left">
                    <select name="pop_name" value="" disabled="disabled">
                    <?php
                    $db =dbFetchRows( 'select pop_name from nxg_pop_details where id='.$pop.'');

                $pop_name =$db[0]['pop_name'];
               $str='<option>'.$pop_name.' </option>';
                echo $str;
                       ?>
                    
                     </select>
                </td>
           </tr>
            <tr>
                <td class="pull_right">Source IP</td>
                <?php
                echo $str = '<td ><input type="text" class="pull_left" name="source_ip" disabled="disabled" value="'.$source_ip.'"/></td>
                <td>/<input type="text" class="subnet" name="source_subnet" disabled="disabled" value="'.$source_subnet.'"/></td></tr>';
                ?>
            <tr>
            <tr>
                <td class="pull_right">Source Port</td>
                <?php	
		if ( $source_port != 0 ) 
		{
                echo $str = '<td class="pull_left"><input type="text" name="source_port" disabled="disabled" value="'.$source_port.'" /></td>';
		}
		else
		{
		 echo $str = '<td class="pull_left"><input type="text" name="source_port" disabled="disabled" value="" /></td>';
		}
                 ?>
            </tr>

            <tr>
                <td class="pull_right">Destination IP</td>
                 <?php
                echo $str = '<td ><input type="text" class="pull_left" name="network_prefix_ip" disabled="disabled" value="'.$network_ip.'" readonly/></td>
                <td>/<input type="text" class="subnet" name="network_prefix_subnet_ip" disabled="disabled" value="'. $network_subnet_ip.'" readonly/></td>';
                ?>
            </tr>
            <tr>
                <td class="pull_right">Destination Port</td>
                <?php
		if ($port != 0 )
		{
                echo $str = '<td class="pull_left"><input type="text" disabled="disabled" name="port_no" value="'.$port.'" /></td></tr>';
		}
		else
		{
		echo $str = '<td class="pull_left"><input type="text" disabled="disabled" name="port_no" value="" /></td></tr>';
		}
                ?>
            <tr>


            <tr>
                <td  class="pull_right">Protocol</td>
                <td class="pull_left">   
                       <select  name="protocol" disabled="disabled" >
                    <?php
                       $str='<option value="'.$protocol.'"> '.$protocol.'</option>';
                       echo $str;
                  ?>
                        </select>
                </td>
            </tr>
            <tr>
                <td class="pull_right">MD5 Keystring</td>
                <?php
                echo $str = '<td class="pull_left"><input type="text" disabled="disabled" name="md5_keystring" value="'.$md5_keystring.'" /></td></tr>';
                ?>

            <tr>
                <td  class="pull_right">Comment</td>
                <td class="pull_left">
                <?php
                $str='<textarea class="input" name="comment" value="'.$comment.'" readonly> '.$comment.'</textarea></td></tr>';
                echo $str;
                ?>
            </td></tr>
            <tr>
            <input type="hidden" name="action" value="commit_check"/>
            <?php
               echo $str='<input type="hidden" name="pop_id" value="'.$pop.'"/>'
            ?>
            </tr>
</table>
</div> 
</div>
   </div>
  </div>

</form>

<?php include 'nexusguard/views/includes/commit_check_footer.php'; ?>

 <div class="form-actions">

    <input type="button" id="cancel" class="btn btn-primary" value="Cancel" name="cancel" onclick="location.href='/pop_mgr/view=traffic_diversion/'"; />
<input type="button" id="decommission" class="btn btn-primary" value="Delete" name="delete" />
    </div>


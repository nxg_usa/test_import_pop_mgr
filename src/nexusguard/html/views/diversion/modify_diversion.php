<link rel="stylesheet" href="/nexusguard/css/pop_manager.css" >

<script>
$(document).ready( function(){
$('#delete').on('click', function(){
        var data ={};
        var elem = document.getElementById('modify_diversion').elements;
        for(var i = 0; i < elem.length; i++)
        {
            data[ elem[i].name]= elem[i].value;
        }
            $.ajax({ url: '/nexusguard/views/diversion/delete_dynamic_diversion.php',
            data: JSON.stringify(data),
            type: 'post',
            success: function(output) {
                window.location.href="/pop_mgr/view=traffic_diversion/";
}
       });
});


$('#cancel').on('click', function(){
                window.location.href="/pop_mgr/view=traffic_diversion/";
       });
});

</script>
<?php
include_once('/opt/observium/html/nexusguard/views/includes/common_includes.php');

//$data = json_decode(file_get_contents('php://input'), true);
$unique_id = $vars['diversion'];

$data_id =dbFetchRows('select * from nxg_diversion_details where id='.$unique_id );

 
$diversion_name = $data_id[0]['name'];
$diversion_type = $data_id[0]['diversion_type'];
$network_ip = $data_id[0]['network_ip'];
$network_subnet_ip = $data_id[0]['network_prefix'];
$protocol = $data_id[0]['protocol'];
$port = $data_id[0]['port'];
$comment = $data_id[0]['comment'];
$pop = $data_id[0]['pop'];

$modify_form ='';

$modify_form .='<form id="modify_diversion" >';
$modify_form .=' <div class="row">';
$modify_form .='  <div class="col-md-6">';
$modify_form .=' <div class="widget widget-table">';
$modify_form .='        <div class="widget-header">';
$modify_form .=' <i class="oicon-gear"></i>';
$modify_form .='<h3>Modify Diversion</h3>';
$modify_form .='      </div>';
$modify_form .='      <div style="padding-top: 10px;" class="widget-content">';
$modify_form .='        <table class="form_table">';
$modify_form .='            <tr>';
$modify_form .='                <td class="pull_right">Name</td>';
$modify_form .='                <td ><input type="text"  name="diversion_name" value="'.$diversion_name.'" readonly/></td>';
$modify_form .='                                <td ><input type="text" style="display:none" name="unique_id" value="'.$unique_id.'" readonly/></td>';
$modify_form .='            </tr>';
$modify_form .='   <tr>';
$modify_form .='                <td  class="pull_right">Diversion Type</td>';
$modify_form .='                <td class="pull_left">';
$modify_form .='                    <select  name="Diversion_type" readonly>';
$modify_form .='                    <option value="'.$diversion_type.'">'.$diversion_type.'</option>';
$modify_form .='                    </select>';
$modify_form .='                </td>';
$modify_form .='            </tr>';
$modify_form .='            <tr>';
$modify_form .='                <td  class="pull_right">PoP</td>';
$modify_form .='                <td class="pull_left">';
$modify_form .='                     <select name="pop_name" value="" readonly>';
                    $db =dbFetchRows( 'select pop_name from nxg_pop_details where id='.$pop.'');

$pop_name =$db[0]['pop_name'];
$modify_form .='               <option>'.$pop_name.' </option>';


                    
$modify_form .='                     </select>';
$modify_form .='                </td>';
$modify_form .='            </tr>';

$modify_form .='            <tr>';
$modify_form .='                <td class="pull_right">Network Prefix</td>';
$modify_form .='                <td ><input type="text" class="pull_left" name="network_prefix_ip" value="'.$network_ip.'" readonly/></td>';
$modify_form .='                <td>/<input type="text" class="subnet" name="network_prefix_subnet_ip" value="'. $network_subnet_ip.'" readonly/></td></tr>';
$modify_form .='            <tr>';
$modify_form .='                <td  class="pull_right">Protocol</td>';
$modify_form .='                <td  class="pull_left">';
$modify_form .='                        <select  name="protocol" value="'.$protocol.'" readonly>';
$modify_form .='                        <option value="'.$protocol.'"> '.$protocol.'</option>';
$modify_form .='                        </select>';
$modify_form .='                </td>';
$modify_form .='            </tr>';
      $modify_form .='      <tr>';
$modify_form .='                <td class="pull_right">Port</td>';
$modify_form .='                <td class="pull_left"><input type="text"  name="port" value="'.$port.'" readonly/></td></tr>';
            $modify_form .='<tr>';
$modify_form .='                <td  class="pull_right">Comment</td>';
$modify_form .='                <td class="pull_left"><textarea class="input" name="comment" value="'.$comment.'" readonly> '.$comment.'</textarea></td></tr>';
$modify_form .='            <tr>';
$modify_form .='                <td class="pull_right"><input type="button" id="cancel" class="btn btn-default" value="Cancel" name="cancel"/></td>';
$modify_form .='                <td  class="pull_left"><input type="button" id="delete" class="btn btn-default" value="Delete" name="delete" /></td>';
$modify_form .='            </tr>';
$modify_form .='</table>';
$modify_form .='</div> ';
$modify_form .='</div>';
$modify_form .='    </div>';
$modify_form .='  </div>';

$modify_form .='</form>';
 
echo $modify_form;
?>



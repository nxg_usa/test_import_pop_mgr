
<?php
include_once('/opt/observium/html/nexusguard/views/includes/common_includes.php');

//retrieve contents

$data = json_decode(file_get_contents('php://input'), true);

$validation_result = validate_static_diversion_form($data);
$diversion_type = strtolower($data['diversion_type']);

$pop_id=$data['pop_id'];
$manual_diversion_config = array();

$manual_diversion_config = dbFetchRows('select diversion_name as community  from  nxg_router_interface_mapping left join nxg_pop_interface_ip_mgmt on nxg_pop_interface_ip_mgmt.pop_id=nxg_router_interface_mapping.pop_details_id  and nxg_pop_interface_ip_mgmt.interface_unit=nxg_router_interface_mapping.vlan_id and nxg_pop_interface_ip_mgmt.interface_name=nxg_router_interface_mapping.device_interface_name where nxg_router_interface_mapping.scrubber_type="'.$diversion_type.'" and nxg_router_interface_mapping.pop_details_id='.$data['pop_id']);
$json_array = array();

$i=0;
foreach($manual_diversion_config as $config){
$json_array [$i]['community'] = $manual_diversion_config[$i]['community'];
$json_array[$i]['route']=$data['route_name'];
$json_array[$i]['destination'] = $data['network_prefix_ip'];
$json_array[$i]['network_prefix_subnet'] = $data['network_prefix_subnet_ip'];
$json_array[$i]['protocol'] = $data['protocol'];
$i=$i+1;
}

$pop_details = get_sinlge_pop_detail($pop_id);
$device_details = dbFetchRow("select hostname from devices where device_id = ?",array($pop_details['pop_edge_router_device_id']));

$device_username = $pop_details['username'];
$hostname = $device_details['hostname'];
$device_password = $pop_details['password'];
$action = "commit_check";
$epoch_time = time();

$json_tmp_filename="manual_diversion"."_".$epoch_time.".json";
$config_tmp_filename = "manual_diversion"."_".$epoch_time.".conf";
$commit_check_tmp_filename = "manual_diversion"."_".$epoch_time."_commit_check.log";
$commit_log_tmp_filename = "manual_diversion"."_".$epoch_time.".log";


$json_filename = $json_dir.$json_tmp_filename;
$config_filename = $config_dir.$config_tmp_filename;
$commit_check_filename = $commit_check_dir.$commit_check_tmp_filename;
$commit_log_filename = $commit_log_dir.$commit_log_tmp_filename;


$array = array();
$array['routing_options'] =$json_array;
$array['conf_file'] = $config_filename;
$array['commit_check_log'] = $commit_check_filename;
$array['commit_log'] = $commit_log_filename;
$array['json_file'] = $json_filename;
$fp = fopen($json_filename,'w');
fwrite($fp, str_replace("\\","", json_encode($array)));
fclose($fp);


$cmd = "/bin/bash -x /opt/observium/nexusguard/ansible/script/nxg_add_manual_diversion.sh $hostname $device_username $device_password $action $json_filename $config_filename $commit_check_filename";
exec($cmd,$result,$status);
if( $status != 0 )
{
    $err = shell_exec("cat ".$commit_log_filename);
    write_error_response("ccerr01","Error in executing commit check<br><pre>".$err."</pre>");
}

unset($result);

$cmd = "cat ".$commit_check_filename;
$result = shell_exec($cmd);
if( $result == "" )
{
    write_error_response("ccerr01","Error in getting commit check output");
}

$output = array('output'=>$result);

 $cmd = ("cat ".$commit_check_filename);
    $result .= shell_exec($cmd);

    $result .= "<br/><br/>";

    $output = $result;
?>




<h4>Commit Check Output</h4>
    <pre style="height:150px;overflow:auto">
        <?php echo $result; ?>
    </pre>
    <input type="hidden" name="json_file" value="<?php echo $json_filename; ?>"/>
    <input type="hidden" name="config_file" value="<?php echo $config_filename; ?>"/>
    <input type="hidden" name="check_file" value="<?php echo $commit_check_filename; ?>"/>
    <input type="hidden" name="log_file" value="<?php echo $commit_log_filename; ?>"/>

<link rel="stylesheet" href="/nexusguard/css/pop_manager.css" >


<script>

$(document).ready(function(){
var i = 0;
$('#btn_id').on('click' ,function(){
      var arr = [];
       $('.check:checked').each(function () {
           arr[i++] = $(this).val();
       });

    if( arr.length !== 1){
    $('#error_msg').html("Select only one at time.");
       }
    else{
             $.ajax({ url: '/nexusguard/views/diversion/delete_static_route.php',
         data: JSON.stringify(arr[0]),
         type: 'post',
         success: function(output){
        location.reload();
}
        });
    }
   });
});


$(document).ready(function() {
$('.add_diversion_test').on('click' ,function(){
        var  text_data = $(this).attr('id');;
 var data={ "id" : text_data };

     $.ajax({ url: '/nexusguard/views/diversion/delete_exabgp_diversion.php',
         data: JSON.stringify(data),
       type: 'post',
         success: function(output){
}
       });


});
});



$(document).ready(function() {
$('.delete_static_diversion_test').on('click' ,function(){
        var  text_data = $(this).attr('id');;
 var data={ "id" : text_data };

     $.ajax({ url: '/nexusguard/views/diversion/delete_manual_diversion.php',
         data: JSON.stringify(data),
       type: 'post',
         success: function(output){
}
       });


});
});

$(document).ready(function() {
$('.test').on('click' ,function(){
 var id = $(this).attr('id');
});
});

</script>
<?php
//session_start();
//$user=$_SESSION['username'];
 ?>
    <h3 id="traffic_diversion_common">Traffic Diversion</h3>
        <h4 id="traffic_diversion_common" style="margin-top:35px">BGP Flow Spec Diversion</h4>
            <div class="pull_right" style="float: right;">  <a href="/pop_mgr/view=add_diversion/refresh=0/" class="btn" role="button">Add Diversion</a></div>
                <table class="table table-hover table-striped table-bordered table-condensed table-rounded" style="margin-top:30px">
                    <thead>  
                        <tr>
                            <th>Diversion Name</th>
                            <th>PoP Name</th>
                            <th>Diversion Type</th>
                            <th>Protocol</th>
                            <th>Destination IP</th>
                            <th>Destination Port</th>
                            <th>Source IP</th>
                            <th>Source Port</th>
                            <th>Date</th>
                            <th>Admin Name</th>
                            <th>Comment</th>
                        </tr>
                     </thead>
        <?php 
         include_once("/opt/observium/html/nexusguard/api/common/common.inc.php");
         $extract_pop_name= dbFetchRows("select * from nxg_pop_details"); 
         $all_nxg_diversion= dbFetchRows("select * from nxg_diversion_details");
         foreach($all_nxg_diversion as $diversion){
         $popid=$diversion['pop'];
         $extract_pop_name= dbFetchRows("select pop_name from nxg_pop_details where id=$popid");
         $str ='<tr>';
         $str .= '<td><a href="/pop_mgr/view=modify_diversion/diversion='.$diversion['id'].'/refresh=0/"  class="add_diversion_test" id="'.$diversion['id'].'">'.$diversion['name'].'</a></td>';
         $str .= '<td>'.$extract_pop_name[0]['pop_name'].'</td>';
         $str .= '<td>'.$diversion['diversion_type'].'</td>';
         $str .= '<td>'.$diversion['protocol'].'</td>';
         if (!empty($diversion['network_ip']))
         {
         $str .= '<td>'.$diversion['network_ip'].'/'.$diversion['network_prefix'].'</td>';
         }
         else
         {
          $str .= '<td></td>';
         }
         if ($diversion['port'] != 0)
         {
         $str .= '<td>'.$diversion['port'].'</td>';
         }
         else
         {
            $str .= '<td></td>';
         }

         if (!empty($diversion['source_ip']))
         {
         $str .= '<td>'.$diversion['source_ip'].'/'.$diversion['source_subnet'].'</td>';
         }
            else
            {
                $str .= '<td></td>';

            }
          if ($diversion['source_port'] != 0)
          {
             $str .= '<td>'.$diversion['source_port'].'</td>';
          }
         else
          {
            $str .= '<td></td>';

          }
         $str .= '<td>'.$diversion['add_date'].'</td>';
         $str .= '<td>'.$diversion['admin_name'].'</td>';
         $str .= '<td>'.$diversion['comment'].'</td></tr>';
         echo $str;
         }
        ?>
                 </table>
         
        <h4 id="traffic_diversion_common" style="margin-top:35px">Static Route</h4>
            <div class="pull_right" style="float: right;">  <a href="/pop_mgr/view=add_static_route/refresh=0/" class="btn" role="button">Add Static Route</a></div>
                <table class="table table-hover table-striped table-bordered table-condensed table-rounded" style="margin-top:20px">
                        <thead>
                            <tr>
                                <th>Route Name</th>
                                <th>PoP Name</th>
                                <th>Diversion Type</th>
                                <th>Protocol</th>
                                <th>Network Prefix</th>
                                <th>Date</th>
                                <th>Admin Name</th>
                                <th>Comment</th>
                            </tr>
                        </thead>
                <?php
                         $all_nxg_diversion= dbFetchRows("select * from nxg_static_route");
                         foreach($all_nxg_diversion as $diversion){
                         $str ='<tr>';
                         $str .= '<td ><a href="/pop_mgr/view=delete_manual_diversion/diversion='.$diversion['id'].'/refresh=0/"  class="delete_static_diversion_test" id="'.$diversion['id'].'"> '.$diversion['route_name'].'</a></td>';
                         $str .= '<td>'.$diversion['pop_name'].'</td>';
                         $str .= '<td>'.$diversion['diversion_type'].'</td>';
                         $str .= '<td>'.$diversion['protocol'].'</td>';
                         $str .= '<td>'.$diversion['network_prefix'].'/'.$diversion['network_prefix_subnet'].'</td>';
                         $str .= '<td>'.$diversion['add_date'].'</td><td>'.$diversion['admin_name'].'</td>';
                         $str .= '<td>'.$diversion['comment'].'</td></tr>';
                         echo $str;
                         }
                ?> 
                        </table>
                    </div> <!-- end of widget-content -->
<div id="error_msg"></div>

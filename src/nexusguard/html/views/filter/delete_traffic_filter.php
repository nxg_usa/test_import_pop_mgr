
<?php
include_once('/opt/observium/nexusguard/db/db_pop_functions.php');
include_once("common/common.inc.php");
        $filter_id = $vars['filter'];
        if($filter_id !=null)
        {
           $traffic_filter = dbFetchRow('select * from nxg_traffic_filter where id='.$filter_id);
        }
 ?>

<link rel="stylesheet" href="/nexusguard/css/pop_manager.css" >
<link rel="stylesheet" href="/nexusguard/css/bootstrap-panel.css" >
<script src="/nexusguard/js/common.js"></script>
<script>
var button_array=['cancel','decommission'];

$(document).ready(function(){
     $( "#decommission" ).click(function() {
            if(confirm("Confirm Delete Filter?") !=true){
                return;
            }
            commit_check_add_filter('delete_traffic_filter_form','/nexusguard/api/delete_traffic_filter.php');
    });
    $( "#cancel_config" ).click(function() {

        $("#isp_config").hide();
        $("#isp_form :input").prop('readonly', false);
         window.location("/pop_mgr/view=traffic_filter/");
    });
    $( "#applyconfig" ).click(function() {

            apply_config_delete_filter('delete_traffic_filter_form','/nexusguard/api/delete_traffic_filter.php',button_array);
    });
});

$(document).ready(function(){
$('#source').show();

var filter_type=$('#filter_type option:selected').text(); 
    if(filter_type== "global")
    {
    $('#source').hide();
    $('#destination').hide();
    $('#source').show();
    }
    else if(filter_type== "local")
    {
    $('#source').hide();
    $('#destination').hide();
    $('#destination').show();  
    }
    else
    {
    $('#source').hide();
    $('#destination').hide();
    $('#destination').show();
    }
});
$(document).ready(function(){
    var protocol =$('#protocol option:selected').val();
    if( protocol == "icmp"){
        $('#port_id').hide();
    }
});
</script>   
<h3 class="form_heading">Delete Filter</h3> 
    <form id="delete_traffic_filter_form" method="post">
        <div class="row">
            <div class="col-md-6">
                <div class="widget widget-table">
                    <div class="widget-header">
                        <i class="oicon-gear"></i>
                        <h3>Delete Filter</h3>
                    </div>
                    <div class="widget-content" style="padding-top:10px;" >
                        <table class="form_table">
                            <tr>
                                <td class="pull_right">Filter Name</td>
                                <td ><input type="text" name="filter_name" value="<?php echo $traffic_filter['filter_name']; ?>" disabled /></td>
                            </tr>
                            <tr>
                                <td  class="pull_right">Filter Type</td>
                                <td class="pull_left" id="filter_type" value=" <?php echo $traffic_filter['filter_type'];?>" disabled>
                                    <select  name="filter_type" disabled >
                                        <option value="<?php echo $traffic_filter['filter_type'];?>"><?php echo $traffic_filter['filter_type'];?></option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td  class="pull_right">PoP</td>
                                <td class="pull_left">
                                     <select id="pop_id" name="pop_id" disabled>
                                        <?php $pop_name=dbFetchRow('select * from nxg_pop_details where id='.$traffic_filter['pop_id'].''); ?>
                                        <option value="<?php echo $pop_name['id']; ?>"><?php echo $pop_name['pop_name']; ?></option>
                                     </select>
                                </td>
                            </tr>
                            <tr>
                                <td  class="pull_right">Customer</td>
                                <td class="pull_left" >
                                    <input type="text" name="customer" value="<?php echo $traffic_filter['customer']; ?>" disabled />
                                </td>
                            </tr>
                            <tr>
                                 <td class="pull_right" id="destination" style="display:none;">Destination Network Prefix</td>
                                  <td class="pull_right" id="source" style="display:none;">Source Network Prefix</td>
                                 <td ><input type="text" class="pull_left" name="network_prefix_ip" value="<?php echo $traffic_filter['destination_prefix']; ?>" disabled/></td>
                                 <td>/<input type="text" class="subnet" name="network_prefix_subnet_ip" value="<?php echo $traffic_filter['destination_subnet']; ?>" disabled/></td>
                            </tr>
                            <tr>
                                <td  class="pull_right">Services</td>
                                <td  class="pull_left">
                                    <select  name="service"  value=" <?php echo $traffic_filter['services']; ?>"  disabled/>
                                        <option value="<?php echo $traffic_filter['services']; ?>" ><?php echo $traffic_filter['services']; ?></option>
                                    </select>
                                </td>
                            </tr>
            
                             <tr>
                                  <td><input type="hidden" disabled="disabled"  class="" name="filter_id" value="<?php echo $filter_id;?>"/></td>
                            </tr>

                            <tr >
                                <td  class="pull_right">Protocol</td>
                                <td  class="pull_left">
                                    <select  name="protocol"  value="<?php echo $traffic_filter['protocol']; ?>"  id="protocol" disabled/>
                                       <option value="<?php echo $traffic_filter['protocol']; ?>"><?php echo $traffic_filter['protocol']; ?></option>
                                    </select>
                                </td>
                            </tr>
                            <tr id="port_id">
                                <td class="pull_right">Port</td>
                                <td class="pull_left"><input type="text"  name="port_no" value=" <?php echo $traffic_filter['port']; ?>"  disabled/></td></tr>
                            <tr>
                                <td  class="pull_right">Comment</td>
                                <td class="pull_left"><textarea class="input" name="comment" value=" <?php echo $traffic_filter['comment']; ?>" disabled ></textarea></td>
                            </tr>
                    </table>
               </div> <!-- end of widget-content -->
          </div> <!-- End of widget-table -->
    </div> <!-- End of col-md-6 -->
</div>  <!-- End of row -->

<input type="hidden" name="action" value="commit_check"/>
</form>
<?php include "nexusguard/views/includes/commit_check_footer2.php"; ?>

<?php include "nexusguard/views/includes/commit_check_footer.php"; ?>

<div class="form-actions">
            <td class="pull_right">
                <input type='button' class='btn btn-primary' onclick="location.href='pop_mgr/view=traffic_filter/'" name='cancel_button' id='cancel' value="Cancel"></td>
            <td class="pull_right">
                <input type='button' data-target="#myModal" class='btn btn-primary' name='decommission' id="decommission" value="Delete"/>
            </td>
            </div>


<script type="text/javascript">
function getData(strCommitLog)
{
    (function($) {
                        var text =  strCommitLog ;
                        $("#traffic_filter_config").html(text);
                        jQuery("#popupModel").modal('show');
     })(jQuery);
}
</script>
<div class="row">
<div class="col-md-12">
<?php
unset($search, $usernames_array, $popdetails_array,$customers_array,$filter_name_array);

foreach (dbFetchColumn('SELECT DISTINCT `user` FROM `nxg_traffic_filter`') as $username)
{
  $usernames_array[$username] = $username ;
}

krsort($usernames_array);

$search[] = array('type'    => 'multiselect',
                  'name'    => 'Users',
                  'id'      => 'user',
                  'width'   => '150px',
                  'value'   => $vars['user'],
                  'values'  => $usernames_array);

$protocol_array = array('ah'=>'ah','egp'=>'egp','esp'=>'esp','gre'=>'gre','icmp'=>'icmp','icmp6'=>'icmp6','igmp'=>'igmp','ipip'=>'ipip','ospf'=>'ospf','pim'=>'pim','rsvp'=>'rsvp','sctp'=>'sctp','tcp'=>'tcp','udp'=>'udp');

$search[] = array('type'    => 'multiselect',
                  'name'    => 'Protocol',
                  'id'      => 'protocol',
                  'width'   => '150px',
                                  'value'   => $vars['protocol'],
                  'values'  => $protocol_array);
$filter_array = array( 'global'=>'Global','local'=>'Local','customer'=>'Customer');
$search[] = array('type'    => 'multiselect',
                  'name'    => 'Filter-Type',
                  'id'      => 'filter_type',
                  'width'   => '150px',
                  'value'   => $vars['filter_type'],
                  'values'  => $filter_array);
        
foreach (dbFetchRows('SELECT `id`, `pop_name` FROM `nxg_pop_details`') as $popdetails)
{
  $popid = $popdetails['id'] ;
  $popdetails_array[$popid] = $popdetails['pop_name'] ;
}

$search[] = array('type'    => 'multiselect',
                  'name'    => 'PoP',
                  'id'      => 'pop_id',
                  'width'   => '150px',
                  //'subtext' => TRUE,
                  'value'   => $vars['pop_id'],
                  'values'  => $popdetails_array);
foreach (dbFetchColumn('SELECT  `name` FROM `nxg_customer`') as $customer)
{
    $customers_array[$customer] = $customer ;
}
$search[] = array('type'    => 'multiselect',
                  'name'    => 'Customer',
                  'id'      => 'customer',
                  'width'   => '150px',
                  //'subtext' => TRUE,
                  'value'   => $vars['customer'],
                  'values'  => $customers_array);
foreach (dbFetchColumn('select `filter_name` From `nxg_traffic_filter`') as $filter_name)
{
    $filter_name_array[$filter_name] = $filter_name ;
}
$search[] = array('type'    => 'multiselect',
                  'name'    => 'Filter Name',
                  'id'      => 'filter_name',
                  'width'   => '150px',
                  //'subtext' => TRUE,
                  'value'   => $vars['filter_name'],
                  'values'  => $filter_name_array);

$search[] = array('type'    => 'text',
                  'name'    => 'Destination Prefix',
                  'id'      => 'destination_prefix',
                  'width'   => '150px',
                  'placeholder' => 'Destination Prefix',
                  'value'   => $vars['destination_prefix']);

$search[] = array('type'    => 'text',
                  'name'    => 'Port',
                  'id'      => 'port',
                  'width'   => '150px',
                  'placeholder' => 'Port',
                  'value'   => $vars['port']);
$search[] = array('type'    => 'datetime',
                  'id'      => 'timestamp',
                  'presets' => TRUE,
                  'min'     => dbFetchCell('SELECT `date` FROM `nxg_traffic_filter`' . ' ORDER BY `date` LIMIT 0,1;'),
                  'max'     => dbFetchCell('SELECT `audit_time` FROM `nxg_auditlog`' . ' ORDER BY `audit_time` DESC LIMIT 0,1;'),
                  'from'    => $vars['timestamp_from'],
                  'to'      => $vars['timestamp_to']);
print_search($search, 'TrafficFilter', 'search', 'pop_mgr/view=traffic_filter/');
$vars[' agination'] = TRUE;
?>

<div class="pull_right" style="float: right;">  <a href="pop_mgr/view=add_filter/refresh=0/" class="btn" role="button">Add Filter</a></div>
<?php
        $vars[' agination'] = TRUE;
        
        $page_title[] = 'FilterGrid';
        
        print_traffic_filter_events($vars);

        function print_traffic_filter_events($vars){
         
         global $popdetails_array ;
         $events = get_traffic_filter_events_array($vars);
         if (!$events['count'])
              {
                print_warning('<h4>No Filter entries found!</h4>');
              }else{
                $string = '<table class="table table-bordered table-striped table-hover table-condensed-more">' . PHP_EOL;
            if (!$events['short'])
                {
                  $string .= '  <thead>' . PHP_EOL;
                  $string .= '    <tr>' . PHP_EOL;
                  $string .= '      <th class="state-marker"></th>' . PHP_EOL;
                  $string .= '      <th>Filter Name</th>' . PHP_EOL;
                  $string .= '      <th>Filter Type</th>' . PHP_EOL;
                  $string .= '      <th>PoP Name</th>' . PHP_EOL;
                  $string .= '      <th>Customer Name</th>' . PHP_EOL;
                  $string .= '      <th>Admin Name</th>' . PHP_EOL;
                  $string .= '      <th>Destination Prefix</th>' . PHP_EOL;
                  $string .= '      <th>Service</th>' . PHP_EOL;
                  $string .= '      <th>Protocol</th>' . PHP_EOL;
                  $string .= '      <th>Port</th>' . PHP_EOL;
                  $string .= '      <th>Comment</th>' . PHP_EOL;
                  $string .= '      <th>Date</th>' . PHP_EOL;
                  $string .= '    </tr>' . PHP_EOL;
                  $string .= '  </thead>' . PHP_EOL;
                 }   
            $string   .= '  <tbody>' . PHP_EOL;
                foreach ($events['entries'] as $entry)
                    {
                      $string .= '  <tr class="'.$entry['html_row_class'].'">' . PHP_EOL;
                      $string .= '<td class="state-marker"></td>' . PHP_EOL;

                            $string .= '    <td style=""><a id="'.$entry['id'].'" href="/pop_mgr/view=delete_traffic_filter/refresh=0/filter='.$entry['id'].'">';
                            $string .= $entry['filter_name'] . '</a></td>' . PHP_EOL ;
        
                            $string .= '    <td style="">';
                            $string .= $entry['filter_type'] . '</td>' . PHP_EOL ;
                            $string .= '    <td style="">';
                            $popid = $entry['pop_id'] ;
                            $pop_name = dbFetchRow('select pop_name from nxg_pop_details where id='.$popid.''); 
                            $string .= $pop_name['pop_name']. '</td>' . PHP_EOL ;
                            $string .= '    <td style="">';
                            $string .= $entry['customer'] . '</td>' . PHP_EOL ;
                            $string .= '    <td style="">';
                            $string .= $entry['user'] . '</td>' . PHP_EOL ;
                            $string .= '    <td style="">';
                            $string .= $entry['destination_prefix'] . '/'.$entry['destination_subnet'].'</td>' . PHP_EOL ;
                            $string .= '    <td style="">';
                            $string .= $entry['services'] .'</td>' . PHP_EOL ;


                            $string .= '    <td style="">';
                            $string .= $entry['protocol'] .'</td>' . PHP_EOL ;
                            $string .= '    <td style="">';
                            $string .= $entry['port'] . '</td>' . PHP_EOL ;
                            $string .= '    <td style="">';
                            $string .= $entry['comment'] . '</td>' . PHP_EOL ;
                             if ($events['short'])
                               {
                                $string .= '    <td class="syslog" style="white-space: nowrap">';
                                $timediff = $GLOBALS['config']['time']['now'] - strtotime($entry['timestamp']);
                                $string .= generate_tooltip_link('', formatUptime($timediff, "short-3"), format_timestamp($entry['timestamp']), NULL) . '</td>' . PHP_EOL;
                                 }
                        else
                           {
                            $string .= '    <td style="">';
                            $string .= format_timestamp($entry['date']) . '</td>' . PHP_EOL;
                            }

                    }
                    $string   .= '  </tbody>' . PHP_EOL;
                    $string   .= '  </table>' . PHP_EOL;
            echo $string;
        }
   }



        function  get_traffic_filter_events_array($vars){
        $array = array();
        $array['short'] = (isset($vars['short']) && $vars['short']);
        $array['pagination'] = (isset($vars['pagination']) && $vars['pagination']);
          pagination($vars, 0, TRUE); // Get default pagesize/pageno
          $array['pageno']   = $vars['pageno'];
          $array['pagesize'] = $vars['pagesize'];
          $start    = $array['pagesize'] * $array['pageno'] - $array['pagesize'];
          $pagesize = $array['pagesize'];
            


           $param = array();
              $where = ' WHERE 1 ';
                  foreach ($vars as $var => $value)
                      {
                        if ($value != '')
                        {
                           switch ($var)
                                 {
                                    case 'pop_id':
                                        $where .= generate_query_values($value, 'pop_id');
                                          break;
                                    case 'user':
                                      $where .= generate_query_values($value, 'user');
                                             break;
                                    case 'protocol':
                                      $where .= generate_query_values($value, 'protocol') ;
                                             break;
                                    case 'filter_type':
                                        $where .= generate_query_values($value, 'filter_type');
                                          break;
                                    case 'customer':
                                      $where .= generate_query_values($value, 'customer');
                                             break;
                                    case 'filter_name':
                                      $where .= generate_query_values($value, 'filter_name') ;
                                             break;
                                    case 'destination_prefix':
                                      $where .= generate_query_values($value, 'destination_prefix') ;
                                             break ;
                                    case 'port':
                                        $where .= generate_query_values($value, 'port');
                                              break;
                                     case 'timestamp_from':
                                          $where .= ' AND `date` >= ?';
                                          $param[] = $value;
                                              break;
                                    case 'timestamp_to':
                                          $where .= ' AND `date` <= ?';
                                          $param[] = $value;
                                              break;

                                  }
                         }
                     }
            $query = 'FROM `nxg_traffic_filter` ';
            $query .= $where ;
            $query_count = 'SELECT COUNT(*) '.$query;
           // $query_updated = 'SELECT MAX(`audit_time`) '.$query;

            $query = 'SELECT * '.$query;
            $query .= ' ORDER BY `id` DESC ';
            $query .= "LIMIT $start,$pagesize";
            $array['entries'] = dbFetchRows($query, $param);
            if ($array['pagination'] && !$array['short'])
            {
                $array['count'] = dbFetchCell($query_count, $param);
                $array['pagination_html'] = pagination($vars, $array['count']);
            } else {
                $array['count'] = count($array['entries']);
            }
         $array['updated'] = dbFetchCell($query_updated, $param);

  return $array;
}
?>

<div id="popupModel" class="modal fade" role="dialog">
  <div class="modal-dialog" >

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Commit check log</h4>
      </div>
      <div class="modal-body" id="traffic_filter_config">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

  </div> <!-- col-md-12 -->

</div> <!-- row -->

<?php
?>


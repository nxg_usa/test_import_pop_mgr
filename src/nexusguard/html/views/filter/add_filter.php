<link rel="stylesheet" href="/nexusguard/css/pop_manager.css" >

<link rel="stylesheet" href="/nexusguard/css/bootstrap-panel.css" >
<script src="/nexusguard/js/common.js"></script>
<script>
$(document).ready(function ()
{

    var form_data ={};
    var button_array = ["cancel","commit_check"];
    $("#commit_check" ).click(function()
    {

    
        var request_array=[];
        var j=0;
        var pop_type = $("#pop_id option:selected").val();
        if(pop_type === "000")
        {
            $("#pop_id > option").each(function()
            {
             if( this.value !=="000")
            {
                form_data = {};
                form_data = parse_form("add_filter_form");
                form_data.pop_id = $(this).val();
                form_data.action = "commit_check";
                req = [];
                req["url"] = "/nexusguard/api/add_filter.php" ;
                req["apply_url"] = "/nexusguard/api/add_filter.php" ;
                req["data"] = form_data ;
                req["entity_name"] =  "ADD Filter - " +  $(this).text();
                request_array[j]=req;
                j++;
                commit_check2(request_array,button_array);
            }
            });
        }
        else
        {
                form_data = {};
                form_data = parse_form("add_filter_form");
                form_data.pop_id = $("#pop_id option:selected").val();
                form_data.action = "commit_check";
                req = [];
                req["url"] = "/nexusguard/api/add_filter.php" ;
                req["apply_url"] = "/nexusguard/api/add_filter.php" ;
                req["data"] = form_data ;
                req["entity_name"] = "ADD FILTER - " +  $(this).text();
                request_array[j]=req;
                commit_check2(request_array,button_array);
        }
    });
});
</script>
<script>
$(document).ready(function(){
 var service_type = $("#service option:selected").text();
 var service = service_type.split("(");
 var res2 = service[1].split(")");
 var changeval=res2[0].split("/");
    $("#port_no").val(changeval[0]).prop("disabled",true);
    $("#protocol").val(changeval[1]).prop("disabled",true);

    $('#service').on('change' , function(){
    service_type = $("#service option:selected").text();
    
    if(service_type == "ICMP-ANY"){
        $("#port_no").val("").hide();
        $("#protocol").val("").hide();
        $("#port_no_td").hide();
        $("#protocol").val("icmp");
        $("#protocol").show();
    }else if(service_type == "Custom"){
        $("#port_no_td").show();
        $("#protocol_td").show();
        $("#port_no").val("").show().prop("disabled",false);
        $("#protocol").val("").show().prop("disabled",false);
    }else{
        $("#port_no_td").show();
        $("#protocol_td").show();
        $("#port_no").val("").show();
        $("#protocol").val("").show()
         service = service_type.split("(");
         res2 = service[1].split(")");
         changeval=res2[0].split("/");
        $("#port_no").val(changeval[0]).prop("disabled",true);
        $("#protocol").val(changeval[1]).prop("disabled",true);
     }
    });

});


$(document).ready(function(){
    var filter_type = $("#filter_type option:selected").text();
    $('.customer').hide();
    var pop_type = $("#pop_id option:selected").val();
    $('#cust_'+pop_type).show();
    $('#pop_id').on('change', function(){
        pop_type = $("#pop_id option:selected").val();
        filter_type = $("#filter_type option:selected").text();
        if( filter_type == "Customer" )
        {
            pop_type = $("#pop_id option:selected").val();
            $('.customer').hide();
            $('select[id^="cust_"] ').each(function(){
                $(this).prop("selectedIndex",-1);
            });
            $('#cust_'+pop_type).show();
            $('#cust_'+pop_type).prop('disabled',false);
            var count = $("#pop_id :selected").length;
            for(var x=0 ; x<count; x++){
                if( pop_type == x){
                  $('#cust_'+x).show();
                }
                 else{
                  $('#cust_'+x).hide();
                }
             }
        }
    });
});

$(document).ready(function(){
    var filter_type = $("#filter_type option:selected").text();

        $('#pop_id').prop("selectedIndex",-1);
        $("<option value='000'>ALL</option>").insertBefore('#pop_id option:first-child');
    
        var pop_type = $("#pop_id option:selected").val();
        $('.customer').prop("selectedIndex",-1);
        $('.customer').hide();
        $('#cust_'+pop_type).show();
        $('#cust_'+pop_type).prop('disabled',true);
        pop_type = $("#pop_id option:selected").val();
        $('#source').show();
        $('#filter_type').on('change' , function(){
            filter_type = $("#filter_type option:selected").text();
            pop_type = $("#pop_id option:selected").val();
            if( filter_type == "Global" ){
                $('#source').hide();
                $('#destination').hide();
                $('#source').show();
                $("#pop_id option[value='000']").remove();
                $("<option value='000'>ALL</option>").insertBefore('#pop_id option:first-child');
                pop_type = $("#pop_id option:selected").val();
                $('#pop_id').prop("selectedIndex",-1);
                $('.customer').prop("selectedIndex",-1);
                $('.customer').hide();
                $('#cust_'+pop_type).show();
                $('#cust_'+pop_type).prop('disabled',true);
            }else if(filter_type == "Local" ){
                $('#source').hide();
                $('#destination').hide();
                $('#destination').show();
                
                $("#pop_id option[value='000']").remove();
                pop_type = $("#pop_id option:selected").val();
                if(pop_type == null){
                    $('.customer').prop("selectedIndex",-1);
                $('.customer').hide();
                $('#cust_000').show();
                $('#cust_000').prop('disabled',true);
                }else{
                $('.customer').prop("selectedIndex",-1);
                $('.customer').hide();
                $('#cust_'+pop_type).show();
                $('#cust_'+pop_type).prop('disabled',true);
}
            }else{
                $('#source').hide();
                $('#destination').hide();
                $('#destination').show();

                $("#pop_id option[value='000']").remove();
                pop_type = $("#pop_id option:selected").val();
                $('.customer').hide();
                $('select[id^="cust_"] ').each(function(){
                $(this).prop("selectedIndex",-1);
            });
                $('#cust_'+pop_type).show();
                $('#cust_'+pop_type).prop('disabled',false);
            }
        });
});
</script>
<?php 
session_start();
$user = $_SESSION['username'];
$pop_names=dbFetchRows("select pop_name from nxg_pop_details");
?>

<h3 class="form_heading">Add Filter</h3>
    <form id="add_filter_form" method="post" >
        <div class="row">
            <div class="col-md-6">
                <div class="widget widget-table">
                    <div class="widget-header">
                        <i class="oicon-gear"></i><h3>Add Filter</h3>
                    </div>
                    <div class="widget-content" style="padding-top:10px;">
                        <table class="form_table">
                            <tr>
                                <td class="pull_right">Filter Name * </td>
                                <td ><input type="text" name="filter_name" value="" id="filter_name"  /></td>
                            </tr>
                            <tr>
                                <td  class="pull_right">Filter Type</td>
                                <td class="pull_left" id="filter_type" value="">
                                    <select  name="filter_type" >
                                        <option value="global">Global</option>
                                        <option value="local">Local</option>
                                        <option value="customer">Customer</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td  class="pull_right">PoP</td>
                                <td class="pull_left" id="pop">
                                    <select id="pop_id" name="pop_id"  >
                                        <?php
                                            $all_pop=dbFetchRows('select id,pop_name from nxg_pop_details');
                                                foreach($all_pop  as $pop)
                                                {
                                                    echo "<option value='".$pop['id']."'>".$pop['pop_name']."</option>";
                                                }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td  class="pull_right">Customer</td>
                                <td class="pull_left" id="customers">
                                <?php
                                    foreach( $all_pop as  $pop)
                                    {
                                        $str1="";
                                        echo "<select class='customer'  name='customer'  id='cust_".$pop['id']."' style='display:none;'>";
                                        $db_data=  dbFetchRows('select distinct name  from nxg_customer left join  nxg_customer_conn on nxg_customer.customer_id=nxg_customer_conn.cust_id where pop_id='.$pop['id']);
                                        foreach($db_data as $data)
                                        { 
                                            $str1 .="<option value='".$data['name']."'>".$data['name']."</option>";
                                        }
                                        $str1 .="</select>";
                                        echo $str1;
                                    }
                                ?>
                                    <select class='customer'  name='customer'  id='cust_000' style='display:none;'>
                                    <option value="000"></option>
                                    </select>
                                </td>
                            </tr>
                            <tr>                
                                <td class="pull_right" id="destination" style="display:none;">Destination Network Prefix * </td>
                                <td class="pull_right" id="source" style="display:none;">Source Network Prefix *</td>
                                <td ><input type="text" class="pull_left" name="network_prefix_ip" /></td>
                                <td>/<input type="text" class="subnet" name="network_prefix_subnet_ip" /></td>
                            </tr>

                            <tr>
                                <td  class="pull_right">Services</td>
                                <td  class="pull_left">
                                    <select  name="service" value="" id="service" >
                                        <option value="DNS-UDP">DNS-UDP (53/udp)</option>
                                        <option value="DNS-TCP">DNS-TCP (53/tcp)</option>
                                        <option value="FTP">FTP- (21/tcp)</option>
                                        <option value="FTP-Data">FTP-Data (20/tcp)</option>
                                        <option value="HTTP">HTTP- (80/tcp)</option>
                                        <option value="HTTPS">HTTPS- (443/tcp)</option>
                                        <option value="ICMP-ANY">ICMP-ANY</option>
                                        <option value="POP3">POP3- (110/tcp)</option>
                                        <option value="SMTP">SMTP- (25/tcp)</option>
                                        <option value="SSH">SSH- (22/tcp)</option>
                                        <option value="TELNET">TELNET- (23/tcp)</option>
                                        <option value="Custom">Custom</option>
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td  class="pull_right" id="protocol_td">Protocol</td>
                                <td  class="pull_left">
                                    <input type="text" id="protocol"  name="protocol" value="" />
                                </td>
                            </tr>
                            <tr>
                                <td class="pull_right" id="port_no_td">Port</td>
                                <td class="pull_left"><input type="text" id="port_no"  name="port_no" value="" /></td>
                            </tr>
                            <tr>
                                <td  class="pull_right">Comment</td>
                                <td class="pull_left"><textarea class="input" name="comment" value="" ></textarea></td>
                            </tr>
                            <tr>
                                <td class="pull_left"><input class="input" name="user_name" value="<?php echo $user;?>" style="display:none;"/></td>
                            </tr>

                    </table>
                </div> <!-- end of widget-content -->
            </div> <!-- End of widget-table -->
        </div> <!-- End of col-md-6 -->
    </div>  <!-- End of row -->
    <input type="hidden" name="action" value="commit_check"/>
</form>
    <?php include 'nexusguard/views/includes/commit_check_footer2.php'; ?>
    <div class="form-actions">
 
         <input type="button" class="btn btn-primary" id="cancel" name="cancel" value="Cancel" onclick="location.href='pop_mgr/view=traffic_filter/'"/>
         <input type="button" class="btn btn-primary" id="commit_check" name="Commit Check" value="Commit Check"/>

    </div>









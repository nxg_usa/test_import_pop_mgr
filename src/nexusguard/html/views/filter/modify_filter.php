<link rel="stylesheet" href="/nexusguard/css/pop_manager.css" >
    <form >
        <div class="row">
    <div class="col-md-6">

      <div class="widget widget-table">
        <div class="widget-header">
          <i class="oicon-gear"></i><h3>Modify Filter</h3>
        </div>
            <div style="padding-top: 10px;" class="widget-content">
        <table class="form_table">
            <tr>
                <td class="pull_right">Name</td>
                <td ><input type="text" class="pull_center" name="name" value=""/></td>
            </tr>
            <tr>
                <td  class="pull_right">Filter Type</td>
                <td class="pull_left">
                    <select  name="filter_type">
                        <option>Global</option>
                         <option>Global</option>
                          <option>Global</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td  class="pull_right">PoP</td>
                <td class="pull_left">
                     <select name="pop">
                           <option>Global</option>
                             <option>Global</option>
                          <option>All</option>
                     </select>
                </td>
            </tr>
            <tr>
                <td class="pull_right">Customer</td>
                <td class="pull_left">
                     <select  name="customer">
                        <option>All</option>
                         <option>Global</option>
                     </select>
                </td>
            </tr>

            <tr>
                <td class="pull_right">Dest Network Prefix</td>
                <td ><input type="text" class="pull_left" name="ip"/></td>
                <td>/<input type="text" class="subnet" name="subnet_ip"/></td></tr>
            <tr>
                <td  class="pull_right">Protocol</td>
                <td  class="pull_left"><select  name="protocol">
                        <option>TCP</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="pull_right ">Port</td>
                <td class="pull_left"><input type="text"  name="port"/></td></tr>
            <tr>
                <td  class="pull_right">Comment</td>
                <td class="pull_left"><textarea class="input" name="comment"></textarea></td></tr>
            <tr>
                <td class="pull_right"><input type="button" class="btn btn-default" value="Cancel"/></td>
                <td  class="pull_left"><input type="button" class="btn btn-default" value="Submit"/></td>
            </tr>
</table>
</div> <!-- end of widget-content -->
      </div> <!-- End of widget-table -->
    </div> <!-- End of col-md-6 -->
  </div>  <!-- End of row -->
</form>


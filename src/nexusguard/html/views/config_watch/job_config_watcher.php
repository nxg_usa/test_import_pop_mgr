#!/usr/bin/env php
<?php


chdir(dirname($argv[0]));

include_once("../../../../includes/defaults.inc.php");
include_once("../../../../config.php");

include_once("../../../../includes/definitions.inc.php");
include("../../../../includes/functions.inc.php");
include("../../../../includes/discovery/functions.inc.php");
include_once ('/opt/observium/nexusguard/var/popmgr_var.php');
///FIXME. Mike: should be more checks, at least a confirmation click.
//if ($vars['action'] == "expunge" && $_SESSION['userlevel'] >= '10')
//{
//  dbFetchCell('TRUNCATE TABLE `eventlog`');
//  print_message('Event log truncated');
//}
global $config ;
global $var_popmgr_key_to_login;


$ymlFolderName = $config['html_dir'] . "/nexusguard/views/config_watch/ansible/" ;
$inventoryFileName = $ymlFolderName . "inventory" ;
$ymlInterfaceFileName = $ymlFolderName . "mgmtinterface.yml" ;
$ymlRouteFileName = $ymlFolderName . "mgmtroute.yml" ;
$ymlRouteInstanceFileName = $ymlFolderName . "mgmtroute_instance.yml" ;
$logFolderName = $config['html_dir'] . "/nexusguard/views/config_watch/configlogs/" ;

$files = glob($logFolderName . '*'); // get all file names
  foreach($files as $file){ // iterate files
  if(is_file($file))
  unlink($file); // delete file
}

foreach(dbFetchRows('SELECT `hostname`, `username`, `password` FROM `nxg_pop_details` AS P, `devices` AS D WHERE P.pop_edge_router_device_id = D.device_id') as $popdetail)
{
    $handle = fopen($inventoryFileName, "w") ;
    fwrite($handle, "[hosts]\n") ;
    $fileStr = $popdetail['hostname'] . " ansible_ssh_user=" . $popdetail['username'] ;
    fwrite($handle, $fileStr) ;
    fclose($handle) ;
    print($fileStr . "\n") ;
    // call yml file to generate interface log file
    $returnval = shell_exec("sudo ansible-playbook -i " . $inventoryFileName . " " . $ymlInterfaceFileName . " --private-key='$var_popmgr_key_to_login' " . "--extra-vars \"LOGFOLDERNAME=" . $logFolderName .  "\"") ;
    print($returnval . "\n") ;
    // call yml file to generate route log file
    $returnval = shell_exec("sudo ansible-playbook -i " . $inventoryFileName . " " . $ymlRouteFileName . " --private-key='$var_popmgr_key_to_login' " . " --extra-vars \"LOGFOLDERNAME=" . $logFolderName . "\"") ;
    print($returnval . "\n") ;
    // call yml file to generate route instance log file
    $returnval = shell_exec("sudo ansible-playbook -i " . $inventoryFileName . " " . $ymlRouteInstanceFileName . " --private-key='$var_popmgr_key_to_login' " . " --extra-vars \"LOGFOLDERNAME=" . $logFolderName . "\"") ;
    print($returnval . "\n") ;
    unlink($inventoryFileName) ;
}

?>


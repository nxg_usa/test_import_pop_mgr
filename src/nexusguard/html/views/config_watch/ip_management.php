<script type="text/javascript">
function getData(strCommitLog)
{
    (function($) {
                        var text =  strCommitLog ;
                        $("#config_watch_grid").html(text);
                        jQuery("#popupModel").modal('show');
     })(jQuery);
}
</script>
<div class="row">
<div class="col-md-12">
<?php
unset($search, $popdetails_array,$interface_name_array,$diversion_name_array,$isp_id_array,$routing_instance_array,$conn_id_array);

foreach (dbFetchRows('SELECT `id`, `name` FROM `nxg_isp_details`') as $ispdetails)
{
  $ispid = $ispdetails['id'] ;
  $ispdetails_array[$ispid] = $ispdetails['name'] ;
}

$search[] = array('type'    => 'multiselect',
                  'name'    => 'ISP',
                  'id'      => 'isp_id',
                  'width'   => '150px',
                  //'subtext' => TRUE,
                  'value'   => $vars['isp_id'],
                  'values'  => $ispdetails_array);

foreach (dbFetchRows('SELECT `id`, `pop_name` FROM `nxg_pop_details`') as $popdetails)
{
  $popid = $popdetails['id'] ;
  $popdetails_array[$popid] = $popdetails['pop_name'] ;
}

$search[] = array('type'    => 'multiselect',
                  'name'    => 'PoP',
                  'id'      => 'pop_id',
                  'width'   => '150px',
                  //'subtext' => TRUE,
                  'value'   => $vars['pop_id'],
                  'values'  => $popdetails_array);

foreach (dbFetchColumn('SELECT  Distinct `interface_name` FROM `nxg_pop_interface_ip_mgmt` where interface_name IS NOT NULL and interface_name !=""') as $interface_name)
{
    $interface_name_array[$interface_name] = $interface_name ;
}
$search[] = array('type'    => 'multiselect',
                  'name'    => 'Interface Name',
                  'id'      => 'interface_name',
                  'width'   => '150px',
                  //'subtext' => TRUE,
                  'value'   => $vars['interface_name'],
                  'values'  => $interface_name_array);

foreach (dbFetchColumn('SELECT  Distinct `diversion_name` FROM `nxg_pop_interface_ip_mgmt` where diversion_name IS NOT NULL and diversion_name !=""') as $diversion_name)
{
    $diversion_name_array[$diversion_name] = $diversion_name ;
}
$search[] = array('type'    => 'multiselect',
                  'name'    => 'Diversion Name',
                  'id'      => 'diversion_name',
                  'width'   => '150px',
                  //'subtext' => TRUE,
                  'value'   => $vars['diversion_name'],
                  'values'  => $diversion_name_array);

foreach (dbFetchColumn('SELECT  distinct `routing_instance` FROM `nxg_pop_interface_ip_mgmt` where routing_instance is not null and routing_instance!=""') as $routing_instance)
{
    $routing_instance_array[$routing_instance] = $routing_instance ;
}
$search[] = array('type'    => 'multiselect',
                  'name'    => 'Route Instance',
                  'id'      => 'routing_instance',
                  'width'   => '150px',
                  //'subtext' => TRUE,
                  'value'   => $vars['routing_instance'],
                  'values'  => $routing_instance_array);
foreach (dbFetchColumn('SELECT  Distinct `conn_id` FROM `nxg_pop_interface_ip_mgmt` where conn_id IS NOT NULL and conn_id !="" ') as $conn_id)
{
    $conn_id_array[$conn_id] = $conn_id ;
}
$search[] = array('type'    => 'multiselect',
                  'name'    => 'Connection ID',
                  'id'      => 'conn_id',
                  'width'   => '150px',
                  //'subtext' => TRUE,
                  'value'   => $vars['conn_id'],
                  'values'  => $conn_id_array);

foreach (dbFetchColumn('SELECT  Distinct `type` FROM `nxg_pop_interface_ip_mgmt` where type IS NOT NULL and type !="" ') as $type)
{
    $type_array[$type] = $type ;
}
$search[] = array('type'    => 'multiselect',
                  'name'    => 'Type',
                  'id'      => 'type',
                  'width'   => '150px',
                  //'subtext' => TRUE,
                  'value'   => $vars['type'],
                  'values'  => $type_array);

foreach (dbFetchColumn('SELECT  Distinct `subtype` FROM `nxg_pop_interface_ip_mgmt` where subtype IS NOT NULL and subtype !="" ') as $subtype)
{
    $subtype_array[$type] = $subtype ;
}
$search[] = array('type'    => 'multiselect',
                  'name'    => 'Subtype',
                  'id'      => 'subtype',
                  'width'   => '150px',
                  //'subtext' => TRUE,
                  'value'   => $vars['subtype'],
                  'values'  => $subtype_array);



$search[] = array('type'    => 'text',
                  'name'    => 'Target',
                  'id'      => 'target',
                  'width'   => '150px',
                  'placeholder' => 'Target',
                  'value'   => $vars['target']);

$search[] = array('type'    => 'text',
                  'name'    => 'Route Distinguisher',
                  'id'      => 'route_distinguisher',
                  'width'   => '150px',
                  'placeholder' => 'Route Distinguisher',
                  'value'   => $vars['route_distinguisher']);


print_search($search, 'PoP Interface Management', 'search', 'pop_mgr/view=ip_management/');
$vars[' agination'] = TRUE;

    $page_title[] = 'PoP Interface IP Management';
    print_config_grid_watch($vars);
    
    function print_config_grid_watch($vars)
    {
        global $popdetails_array ;
        $events = get_pop_interface_ip_management_events_array($vars);    
        if (!$events['count'])
              {
                print_warning('<h4>No Filter entries found!</h4>');
              }else{
                $string = '<table class="table table-bordered table-hover ">' . PHP_EOL;
            if (!$events['short'])
                {
                 $string .= '  <thead>' . PHP_EOL;
                  $string .= '    <tr>' . PHP_EOL;
                  $string .= '      <th class="state-marker"></th>' . PHP_EOL;
                  $string .= '      <th>ISP Name</th>' . PHP_EOL;
                  $string .= '      <th>PoP Name</th>' . PHP_EOL;
                  $string .= '      <th>Interface Name</th>' . PHP_EOL;
                  $string .= '      <th>Diversion Name</th>' . PHP_EOL;
                  $string .= '      <th>Routing Instance Name</th>' . PHP_EOL;
                  $string .= '      <th>Route Distinguisher</th>' . PHP_EOL;
                  $string .= '      <th>Target</th>' . PHP_EOL;
                  $string .= '      <th>Connection ID</th>' . PHP_EOL;
                  $string .= '      <th>Type</th>' . PHP_EOL;
                  $string .= '      <th>Subtype</th>' . PHP_EOL;
                  $string .= '    </tr>' . PHP_EOL;
                  $string .= '  </thead>' . PHP_EOL;
                 }
             $string   .= '  <tbody>' . PHP_EOL;
                foreach ($events['entries'] as $entry)
                    {
                      $string .= '  <tr class="'.$entry['html_row_class'].'">' . PHP_EOL;
                      $string .= '<td class="state-marker"></td>' . PHP_EOL;


                            $string .= '    <td >';
                            $ispid = $entry['isp_id'] ;
                            $isp_name = dbFetchRow('select name from nxg_isp_details where id='.$ispid.'');
                            $string .= $isp_name['name']. '</td>' . PHP_EOL ;
                            
                            $string .= '    <td >';
                            $popid = $entry['pop_id'] ;
                            $pop_name = dbFetchRow('select pop_name from nxg_pop_details where id='.$popid.'');
                            $string .= $pop_name['pop_name']. '</td>' . PHP_EOL ;
        
                            $string .= '    <td >';
                            $string .= $entry['interface_name'].'.'.$entry['interface_unit']. '</td>' . PHP_EOL ;

                            $string .= '    <td >';
                            $string .= $entry['diversion_name'] . '</td>' . PHP_EOL ;
                        
                            $string .= '    <td >';
                            $string .= $entry['routing_instance'] . '</td>' . PHP_EOL ;
                        
                            $string .= '    <td >';
                            $string .= $entry['route_distinguisher'] . '</td>' . PHP_EOL ;
                    

                            $string .= '    <td >';
                            $string .= $entry['target'] . '</td>' . PHP_EOL ;

                            $string .= '    <td >';
                            $string .= $entry['conn_id'] . '</td>' . PHP_EOL ;

                            $string .= '    <td >';
                            $string .= $entry['type'] . '</td>' . PHP_EOL ;

                            $string .= '    <td >';
                            $string .= $entry['subtype'] . '</td>' . PHP_EOL ;


                    }
        
                $string   .= '  </tbody>' . PHP_EOL;
                $string   .= '  </table>' . PHP_EOL;
            echo $string;
         }
    }

    function get_pop_interface_ip_management_events_array($vars)
    {
    
        $array = array();
        $array['short'] = (isset($vars['short']) && $vars['short']);
        $array['pagination'] = (isset($vars['pagination']) && $vars['pagination']);
          pagination($vars, 0, TRUE); // Get default pagesize/pageno
          $array['pageno']   = $vars['pageno'];
          $array['pagesize'] = $vars['pagesize'];
          $start    = $array['pagesize'] * $array['pageno'] - $array['pagesize'];
          $pagesize = $array['pagesize'];
        
            $param = array();
              $where = ' WHERE 1 ';

                foreach ($vars as $var => $value)
                      {
                        if ($value != '')
                        {
                           switch ($var)
                                 {

                                    case 'pop_id':
                                        $where .= generate_query_values($value, 'pop_id');
                                           break;
                                    case 'target':
                                      $where .= generate_query_values($value, 'target');
                                             break;
                                    case 'route_distinguisher':
                                      $where .= generate_query_values($value, 'route_distinguisher');
                                             break;
                                    case 'interface_name':
                                      $where .= generate_query_values($value, 'interface_name');
                                             break;
                                    case 'diversion_name':
                                      $where .= generate_query_values($value, 'diversion_name');
                                             break;
                                    case 'routing_instance':
                                      $where .= generate_query_values($value, 'routing_instance');
                                             break;
                                    case 'isp_id':
                                      $where .= generate_query_values($value, 'isp_id');
                                             break;
                                    case 'conn_id':
                                      $where .= generate_query_values($value, 'conn_id');
                                             break;
                                    
                                    case 'type':
                                      $where .= generate_query_values($value, 'type');
                                             break;
                                    case 'subtype':
                                      $where .= generate_query_values($value, 'subtype');
                                             break;
                                   }
                         }
                      }
            $query = 'FROM `nxg_pop_interface_ip_mgmt` ';
            $query .= $where ;
            $query_count = 'SELECT COUNT(*) '.$query;
            $query = 'SELECT * '.$query;
            $query .= ' ORDER BY `id` DESC ';
         //   $query .= "LIMIT $start,$pagesize";
            $array['entries'] = dbFetchRows($query, $param);
             if ($array['pagination'] && !$array['short'])
            {
                $array['count'] = dbFetchCell($query_count, $param);
                $array['pagination_html'] = pagination($vars, $array['count']);
            } else {
                $array['count'] = count($array['entries']);
            }
         $array['updated'] = dbFetchCell($query_updated, $param);

  return $array;
    }
?>
<div id="popupModel" class="modal fade" role="dialog">
  <div class="modal-dialog" >

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Commit check log</h4>
      </div>
      <div class="modal-body" id="config_watch_grid">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

  </div> <!-- col-md-12 -->

</div> <!-- row -->


<div class="row">
<div class="col-md-12">
<?php
unset($search, $popdetails_array, $popdevices);
global $config ;
foreach(dbFetchRows('SELECT `id`, `pop_name`, `pop_edge_router_device_id` FROM `nxg_pop_details`') as $popdetails)
{
  $popid = $popdetails['id'] ;
  $popdetails_array[$popid] = $popdetails['pop_name'] ;
  $popdevices[$popid] = $popdetails['pop_edge_router_device_id'] ;
}

$search[] = array('type'    => 'multiselect',
                  'name'    => 'PoP',
                  'id'      => 'pop_id',
                  'width'   => '110px',
                  'subtext' => TRUE,
                  'value'   => $vars['pop_id'],
                  'values'  => $popdetails_array);

krsort($popdetails_array);
print_search($search, '', 'search', 'pop_mgr/view=config_watch/');


echo('    <h3>Routing Instance</h3>' . PHP_EOL);
print_pop_instance_route_status($vars);

echo('  </div>' . PHP_EOL);
echo('</div>' . PHP_EOL);

function print_pop_instance_route_status ($vars)
{
    global $popdevices, $popdetails_array ;
     $string  = '<table class="table table-bordered table-hover" >' . PHP_EOL;
    foreach ($vars as $var => $value)
    {
        if($var == 'pop_id')
        {
            if (!is_array($value)) { $value = array((string)$value); }
            foreach($value as $val)
            {
                $pop_id = $val ;
                $devicename = dbFetchCell('SELECT `hostname` FROM `devices` WHERE `device_id` = '. $popdevices[$pop_id]) ;

                if($devicename != '')
                {
                   $route_instances = dbFetchRows('select routing_instance from nxg_pop_interface_ip_mgmt where  pop_id = ' . $pop_id) ;
                    $all_route_instances = array();
                    $route_names=array();
                    foreach($route_instances as $route_instance)
                     {
                            if(isset($route_instance))
                                {
                                 $all_route_instances[] = $route_instance['routing_instance'];
                                }
                     }
                    foreach($all_route_instances as $new_instance)
                     {
                            if(!in_array($new_instance,$route_names) && !is_null($new_instance))
                                {
                                $route_names[]=$new_instance;
                                }
                     }
                    $string .= '<tr><td class="entity"></td><td></td><td></td> <td></td></tr><tr>' . PHP_EOL;
                    $string .= '    <td class="entity">' . $devicename.'</td><td></td><td></td><td></td>' . PHP_EOL;
                    $string .= '  </tr>' . PHP_EOL;
                    $flag=0;
                    $route="";
                    foreach($route_names as $route_name)
                    {
                    $route=$route_name;
                    $routing_instances = dbFetchRows('select * from nxg_pop_interface_ip_mgmt where routing_instance="'.$route_name.'" and pop_id = ' . $pop_id) ;
                    $matchstring = parse_xml_pop_routing_instance($devicename,$routing_instances) ;
                            if($matchstring != "")
                            {
                            $string .= $matchstring;
                            }
                            else{
                                $flag=1;
                            }
                     }
                if($flag==1){
                     $string .= '  <tr><td></td><td class="entity">'.$route.'</td><td>Unable to fetch data from device.</td><td></td></tr>' . PHP_EOL;
                }
                }
            }
        }
    }
  $string .= '</table>';
            echo($string) ;
}


function parse_xml_pop_routing_instance($devicename,$route_instances)
{
    global $config ;
    $route_match= array () ;
    $xmlFile = $config['html_dir'] . "/nexusguard/views/config_watch/configlogs/" . $devicename . "_ri.xml" ;
    $interfaces = simplexml_load_file($xmlFile) ;
    $route_name="";
        $flag1=0;
         if($interfaces)
         {
           $arr =array();
           foreach($route_instances as $route)
             {
               $arr[]=$route['interface_name'].'.'.$route['interface_unit'];
             }

           foreach($route_instances as $route_instance)
             {
               $route_name =  $route_instance['routing_instance'];
             }
           foreach($arr as $array)
             {
               $flag = 0;
                foreach($interfaces->configuration->{'routing-instances'}->instance as $instance)
                 {
                    if( $route_name == $instance->name)
                        {
                           foreach($instance->interface as $int)
                              {
                                 if($int->name==$array)
                                    {
                                     $flag = 1;
                                    }

                               }
                           if($flag==1)
                              {
                                 $route_match[] = array('interface'=>$array, 'status'=>"Matched",'class'=>'success');
                              }else{
                                 $route_match[] = array('interface'=>$array, 'status'=>"Not Matched",'class'=>'important');
                              }
                            $flag1=1;
                        }
                    else{
                        continue;
                        }
                 }

             }
              if($flag1==0){
                    $route_match[] = array('interface'=>"Routing Instance Not matched on device.", 'status'=>NULL,'class'=>'');
                    }

            $string ="";

                foreach($route_match as $match)
                {
                if(!isset($match['status']))
                {
                    $string .= "<tr><td></td><td class='entity'>".$route_name."</td><td>".$match['interface']."</td><td></td></tr>";
                }
                else{
                 $string .=  '<tr><td></td><td class="entity">'. $route_name.'</td><td>'."  Route Interface name = " . $match['interface'] .'</td>' . PHP_EOL;
                 $string .= '<td><strong> <span class=" label label-'.$match['class'] .'">' . $match['status'] . '</span><strong></td></tr>' . PHP_EOL;
                }
            }
            return $string ;
        }else{
            return "";
        }
}







?>

</div> <!-- col-md-12 -->

</div> <!-- row -->


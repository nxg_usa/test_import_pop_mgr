<link rel="stylesheet" href="/nexusguard/css/pop_manager.css" >
<script src="/nexusguard/js/common.js"></script>
<link rel="stylesheet" href="/nexusguard/css/jquery.tokenize.css" >
<script src="/nexusguard/js/tokenizer/jquery.tokenize.js"></script>
<?php include '/opt/observium/html/nexusguard/api/common/common.inc.php' ;?>
<script type="text/javascript">
var rowCount = 1;
var slabRowCount = 1;
var data ;

function page_reload()
{
    window.location.reload();
}
function show_member_ports(id)
{
    $("#rowCount"+id+"6").show();
}
function add_community()
{
    var community_name = document.getElementById("community_name").value;
    var community_string = document.getElementById("community_string").value;
    $('#bgp_community').tokenize().tokenAdd(community_name +"~~~"+community_string ,community_name +" "+community_string );
     document.getElementById("community_name").value = "";
    document.getElementById("community_string").value = "";
}


function addMoreRows(frm)
{
    rowCount =rowCount+1;
    var pop_intf = $("#service_interface").attr("list");
    var pop_id = $("#pop_id ").val();
    var options = document.getElementById("offnet_ge_interfaces"+pop_id).options;
    var select_box = '<select id="member_ports'+rowCount+'" multiple="multiple" class="ae_interface" name="lag_interface">';
    for(i=0;i<options.length;i++)
    {
       select_box = select_box+'<option value="'+options[i].value+'">'+options[i].text+'<options>';
    }
    select_box = select_box+'</select>';
    var recRow = '<tr id="rowCount'+rowCount+'1"> ' +
                    ' <td class="pull_right">Service Interface *</td>' +
                    ' <td class="pull_left"> '+
                       ' <input type="text" id = "service_interface" name="service_interface" list="' +pop_intf+ '" /> <input type="button" class="btn btn-mini btn-success select_port"  value="Show Ports" id="select_port" name="select_port" onClick="show_port_modal(); return false;"/><br> <input type="button" class="btn btn-mini btn-success" name="add_button" id="add_more" onClick="show_member_ports('+rowCount+'); return false;" value="Add Member Ports"/></td> '+
                       ' <td class="pull_right">Service Interface Unit Number *</td>'+
                       ' <td class="pull_left" colspan="2"><input type="text" name="service_interface_unit" class="small" value="0"/> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+
                        ' Service interface vlan * &nbsp; &nbsp;'+
                        ' <input type="text" name="service_interface_vlan" class="small"/></td> </tr>'+
                        '<tr id="rowCount'+rowCount+'6" style="display:none" ><td></td> '+
                        '<td>';
        recRow += select_box +'<br><input type="button" class="btn btn-mini btn-success select_port"  value="Show Ports" id="select_port" name="select_port" onClick="show_port_modal(); return false;"/>';
        recRow +=   '<tr id="rowCount'+rowCount+'2"> <td class="pull_right">Local IP Address *</td> '+
                    ' <td class="pull_left"><input type="text" name="local_ip" class="ip_address"/>&nbsp;/&nbsp;<input type="text" name="local_ip_subnet" class="subnet" />'+
                    ' </td> '+
                    ' <td class="pull_right">Neighbor IP Address *</td> '+
                    ' <td class="pull_left"><input type="text" name="neighbour_ip" class="ip_address"/>&nbsp;&nbsp;<input type="text"  name="neighbour_ip_subnet" class="subnet" style="display:none"/>'+
                    ' </td> '+
                  ' </tr>' +
                  ' <tr id="rowCount'+rowCount+'3"> '+
                        ' <td class="pull_right">Local ASN *</td> '+
                        ' <td class="pull_left"><input type="text" name="local_as" class="small"/></td>' +
                        ' <td class="pull_right">Peer ASN *</td>' +
                        ' <td class="pull_left"><input name="peer_as" type="text" class="small"/> </td> </tr>'+
                        '<tr  id="rowCount'+rowCount+'5"> <td class="pull_right">MD5 Passphrase</td>' +
                        ' <td class="pull_left"><input type="text" name="md5_passphrase" /></td>'+
                    ' <td class="pull_right">Graceful Restart Timer *</td>'+
                    ' <td class="pull_left" colspan="2"><input name="restart_timer" type="text" class="small" value="300"/>'+
                    ' &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Graceful restart Stale Routes Time(sec) *'+
                    ' &nbsp; &nbsp; <input name="stale_routes_time" type="text" class="small" />'+

                        '<td class="pull_left">'+
                            '<input type="button" class="btn-mini btn-danger isp_buttons" onclick="removeAnotherSlabs('+rowCount+');return false;" name="remove_button" id="remove" value="Remove"/>'  +
                        '</td>' +
                 '</tr>'+
                 '<tr id="rowCount'+rowCount+'4" ><td colspan="12"><hr></td></tr>';
    $('#isp_form_table').append(recRow);
    $('#member_ports'+rowCount).tokenize();

}


function addMoreSlabs(thisform)
{
    slabRowCount++;
    var recRow2 = '<tr id="slabRowCount'+slabRowCount+'"> </td >'+
                        '<td  class="pull_right">&nbsp;</td>'+
                        '<td  class="pull_left">Tier Rate   &nbsp; &nbsp; '+
                        '<input type="text" name="slab_rate" class="cost">&nbsp;&nbsp;bps'+
                        ' &nbsp; &nbsp;  &nbsp; &nbsp;  &nbsp; &nbsp;  &nbsp; Cost &nbsp; &nbsp; <input type="text" name="slab_cost" class="cost"/>&nbsp;&nbsp;USD</td>'+
                        '<td class="pull_left"><input type="button" class="btn-mini btn-danger isp_buttons" onclick="removeSlabs('+slabRowCount+');return false;" name="remove_button" id="remove" value="Remove"/></td>'+
                   '</tr>';

    $('#isp_form_cost_table > tbody > tr').eq(5+slabRowCount+-1).after(recRow2);
}

function removeSlabs(id)
{
    jQuery('#slabRowCount'+id).remove();
    slabRowCount--;
}

function removeAnotherSlabs(id)
{
    jQuery('#rowCount'+id+'1').remove();
    jQuery('#rowCount'+id+'2').remove();
    jQuery('#rowCount'+id+'3').remove();
    jQuery('#rowCount'+id+'4').remove();
    jQuery('#rowCount'+id+'5').remove();
    jQuery('#rowCount'+id+'6').remove();
    rowCount= rowCount-1;
}

</script>
<script>
var button_array=["cancel","commit_check","delete","make_online","make_offline","decommission"];
$(document).ready(function () {
     $( "#decommission" ).click(function() {
            if(confirm("Confirm Delete Neighbor?") !=true){
                return;
            }

            $("#applyconfig").attr('action','del');
            var action = $("#applyconfig").attr('action');
            var url = '/nexusguard/api/del_isp.php';
            commit_check('isp_form',url);
     });

    $( "#cancel_config" ).click(function() {
        $("#isp_config").hide();
        $("#isp_form :input").prop('readonly', false);
    });
    $( "#applyconfig" ).click(function() {
            var action = $("#applyconfig").attr('action');
            var url = '/nexusguard/api/'+action+'_isp.php';
            apply_config('isp_form',url,button_array);

    });
    $( "#commit_check" ).click(function() {

            commit_check('isp_form','/nexusguard/api/mod_isp.php');
            $("#applyconfig").attr('action','mod');
     });

    $( "#make_offline" ).click(function() {

            commit_check('isp_form','/nexusguard/api/offline_isp.php');
            $("#applyconfig").attr('action','offline');
     });
    $( "#make_online" ).click(function() {

            commit_check('isp_form','/nexusguard/api/online_isp.php');
            $("#applyconfig").attr('action','online');
     });
    $("#pop_id").change(function () {
        var selectedValue = $(this).val();
        $("[id=service_interface]").attr('list','offnet_interfaces'+selectedValue);
    });
    var bgp_count = $("#bgp_row_count").val()-1;
    rowCount = rowCount +bgp_count;
    var slab_cnt = $("#slab_row").val()-1;
    slabRowCount = slabRowCount +slab_cnt*1;
    get_port_table_data();
});
  function get_port_table_data()
    {

        $("#port_table").html('<img src="nexusguard/img/ajax-loader.gif">loading...');

        $.ajax({ url: "nexusguard/api/get_port_table.php?pop_id="+$("#pop_id").val(),
            data: '',
            type: 'post',
            success: function(output) {
                $("#port_table").html(output);
            },
            error:function(data){
                alert("Error in processing request.");
            }
        });

    }
function show_port_modal()
{
    $("#portsModal").modal('show');
}

</script>

<!--Show error messages in eoormsg devices-->
<?php 
include_once "/opt/observium/nexusguard/db/db_pop_functions.php";
include_once "/opt/observium/nexusguard/db/db_isp_functions.php";
if(isset($vars['isp']))
{
    $isp_id = $vars['isp'];
    $isp_details = get_isp_details($isp_id);
    $isp_bgp_details = get_isp_details_with_bgp($isp_id);
    $isp_cost_details = get_isp_details_with_cost($isp_id);
    $all_pop = get_sinlge_pop_detail($isp_details['pop_details_id']);
}
?>
<h3 class="form_heading">Modify a Neighbor</h3>
<form class='form_isp' id='isp_form' method='post'>
<div class="row">
    <div class="col-md-12">

      <div class="widget widget-table">
        <div class="widget-header">
          <i class="oicon-gear"></i><h3>General</h3>
        </div>
            <div style="padding-top: 10px;" class="widget-content"> 

    <input type="hidden" name = "isp_id" id="isp_id" value="<?php echo $isp_id; ?>"/>
    <table id="isp_table" class="form_table">
        <tr>
            <td class="pull_right">Type</td>
            <td class="pull_left"><select name="isp_type" readonly>
                  <?php if ($isp_details['isp_type']=="1")
                            echo '<option value="1" >TYPE-1</option>';
                       else if  ($isp_details['isp_type']=="2")
                           echo '<option value="2" >TYPE-2</option>';
                       else
                           echo '<option value="0" >NONE</option>';
                ?> 
       </td>
            <td class="pull_right">Notes</td>
            <td class="pull_left" rowspan="3" style="vertical-align:top;"><textarea type='text' class='notes_textarea' name='notes' /><?php echo $isp_details['notes'] ;?></textarea></td>
        </tr>

        <tr>
            <td class="pull_right">Name *</td>
            <td class="pull_left"><input type='text' class='input' name='isp_name' value="<?php echo $isp_details['name'] ;?>" readonly/></td>
            <!--<td class="pull_right">Notes</td>
            <td class="pull_left" rowspan="3" style="vertical-align:top;"><textarea type='text' class='notes_textarea' name='notes' /><?php echo $isp_details['notes'] ;?></textarea></td> -->
        
        </tr>
        <tr>
            <td class="pull_right">Description</td>
            <td class="pull_left"><textarea class='textarea' name='isp_description' > <?php echo $isp_details['description'] ;?></textarea></td>
        </tr>
        <tr>
            <td class="pull_right">PoP</td>
            <td class="pull_left"><input type="text" value="<?php echo $all_pop['pop_name'];?>" name='pop_name' id="pop_name" readonly>
                                    <input type="hidden" value="<?php echo $all_pop['id'];?>" name='pop_id' id="pop_id"/>
            </td>
        </tr>
    </table>


            </div> <!-- end of widget-content -->
      </div> <!-- End of widget-table -->
    </div> <!-- End of col-md-6 -->
  </div>  <!-- End of row --> 

    
<div class="row">
    <div class="col-md-12">
      <div class="widget widget-table">
        <div class="widget-header">
          <i class="oicon-gear"></i><h3>BGP Details</h3>
        </div>
            <div style="padding-top: 10px;" class="widget-content"> 
<?php
//Interface List tobe populated from database
$all_pop_details = get_all_pops();
foreach($all_pop_details  as $pop)
{
    if(empty($pop_id))
    {
        $pop_id = $pop['id'];
    }
     echo '<datalist id="offnet_interfaces'.$pop['id'].'"  class="" >';
    $all_ports_info = get_pop_ports_info($pop['id']);
    foreach($all_ports_info as $ports_info)
    {
        echo "<option value='".$ports_info['ifName']."'>".$ports_info['ifName']."</option>";
    }
    echo "</datalist>";
    echo '<datalist id="offnet_ge_interfaces'.$pop['id'].'"  class="" >';
    $all_ports_info = get_pop_ge_ports_info($pop['id']);
    foreach($all_ports_info as $ports_info)
    {
        echo "<option value='".$ports_info['ifName']."'>".$ports_info['ifName']."</option>";
    }
    echo "</datalist>";

}
?> 
        <table id="isp_form_table" class="form_table">
       <?php
            $i = 0;
            $cnt = count($isp_bgp_details);
            for($i=0;$i<$cnt;$i++)
            {
        ?>

            <tr id="rowCount<?php echo $i."1";?>">
                <td class="pull_right">Service Interface *</td>
                <td class="pull_left">

<!--<select name='service_interface'>	
				</select>-->
                    <input type="hidden" name="bgp_id" value="<?php echo $isp_bgp_details[$i]['bgp_group'];?>"/> 
                    <input type="text" id = "service_interface" name="service_interface" list="offnet_interfaces<?php echo $pop_id;?>"  value="<?php echo $isp_bgp_details[$i]['service_interface'];?>"/> <input type="button" class="btn btn-mini btn-success select_port"  value="Show Ports" id="select_port" name="select_port" onClick="show_port_modal(); return false;"/><br> <input type='button' class='btn btn-mini btn-success' name='add_button' id='add_more' onClick="show_member_ports(0); return false" value="Add Member Ports"/>
                </td>
                <td class="pull_right">Service Interface Unit Number *</td>
                <td class="pull_left" colspan="2"><input type='text' name='service_interface_unit' class="small" value="<?php echo $isp_bgp_details[$i]['service_interface_unit'];?>"/> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Service Interface vlan &nbsp; &nbsp; <input type='text' name='service_interface_vlan' class="small" value="<?php echo $isp_bgp_details[$i]['service_interface_vlan'];?>"/></td>
            </tr>
             <tr id="rowCount<?php echo $i."6";?>" >
                <td class="pull_right">
              </td>
                <td >

<?php
  echo '<select id="member_ports'.$i.'"  multiple="multiple" class="ae_interface" name="lag_interface" >';
    $all_ports_info = get_pop_ge_ports_info($all_pop['id']);
    foreach($all_ports_info as $ports_info)
    {
        echo "<option value='".$ports_info['ifName']."'>".$ports_info['ifName']."</option>";
    }
    echo "</select>";

?>
               <script type="text/javascript">
               $("#member_ports<?php echo $i;?>").tokenize();
             </script>
<?php
        $members_port = get_interface_member_ports($all_pop['id'],$isp_bgp_details[$i]['service_interface'],$isp_bgp_details[$i]['bgp_group']);
        foreach($members_port as $port)
        {
?>
               <script type="text/javascript">
               $("#member_ports<?php echo $i;?>").tokenize().tokenAdd("<?php echo $port['member_interface_name']; ?>","<?php echo $port['member_interface_name']; ?>");
             </script>
<?php
}
?>
<br>
<input type="button" class="btn btn-mini btn-success select_port"  value="Show Ports" id="select_port" name="select_port" onClick="show_port_modal(); return false;"/>
             </td>
            </tr>

            <tr id="rowCount<?php echo $i."2";?>">
                    <td class="pull_right">Local IP Address *</td>
                    <td class="pull_left">
                        <input type='text' name='local_ip' class="ip_address" value="<?php echo $isp_bgp_details[$i]['local_ip'];?>"/>&nbsp;/&nbsp;<input type='text' name='local_ip_subnet' class='subnet' value="<?php echo $isp_bgp_details[$i]['subnet_mask'];?>"/>
                    </td>
                    <td class="pull_right">Neighbor IP Address *</td>
                    <td class="pull_left">
                        <input type='text' name='neighbour_ip' class="ip_address" value="<?php echo $isp_bgp_details[$i]['neighbor_ip'];?>" />&nbsp;&nbsp;<input type='text'  name='neighbour_ip_subnet' class='subnet' value="<?php echo $isp_bgp_details[$i]['subnet_mask_neighbor'];?>" style="display:none"/>
                    </td>
                </tr>
                <tr id="rowCount<?php echo $i."3";?>">
                    <td class="pull_right">Local ASN *</td>
                    <td class="pull_left"><input type='text' name='local_as' class="small" value="<?php echo $isp_bgp_details[$i]['local_as'];?>"/></td>
                    <td class="pull_right">Peer ASN *</td>
                    <td class="pull_left"><input name='peer_as' type='text' class="small" value="<?php echo $isp_bgp_details[$i]['peer_as'];?>"/>
                </tr>
                <tr id="rowCount<?php echo $i."4";?>">
                    <td class="pull_right">MD5 Passphrase </td>
                    <td class="pull_left"><input type='text' name='md5_passphrase' class="small" value="<?php echo $isp_bgp_details[$i]['md5_passphrase'];?>"/></td>
                    <td class="pull_right">Graceful Restart Timer *</td>
                    <td class="pull_left" colspan="2"><input name='restart_timer' type='text' class="small" value="<?php echo $isp_bgp_details[$i]['restart_timer'];?>"/>
                    &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Graceful restart Stale Routes Time(sec) * &nbsp; &nbsp;
                    <input name="stale_routes_time" type="text" class="small" value="<?php echo $isp_bgp_details[$i]['bgp_stale_routes_time'];?>"/> </td>
                <?php
                     if($i != 0)
                     {
                ?>
                <td>
                    <input type="button" class="btn-mini btn-danger isp_buttons" onclick='removeAnotherSlabs("<?php echo $i; ?>");return false;' name="remove_button" id="remove" value="Remove"/></td>
                <?php
                    }
                ?> 
                </tr>
                <tr><td class="pull_right" colspan="12"><hr></td></tr>
            <?php
                }
                echo "<input type='hidden' id ='bgp_row_count' value='".$i."'/>";
            ?>
                </table>
                <table>
                
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="pull_left" colspan="2" style="padding-left:30px">
                        <input type='button' name='add' class='btn btn-mini btn-success isp_buttons' onclick='addMoreRows(this.form); return false;' value="Add more"/> </input>    
                    &nbsp;&nbsp; (application in case of internet exchange)</td>

                </tr>
    </table>
  </div> <!-- end of widget-content -->
      </div> <!-- End of widget-table -->
    </div> <!-- End of col-md-6 -->
  </div>  <!-- End of row -->

<div class="row">
    <div class="col-md-12">

      <div class="widget widget-table" class="form_table">
        <div class="widget-header">
          <i class="oicon-gear"></i><h3>BGP Communities</h3>
        </div>
            <div style="padding-top: 10px;" class="widget-content">
                <table name="bgp_communities">
                    <tr>
                        <td class="pull_right" >
                            BGP Community Name *
                        </td>
                        <td class="pull_right" > <input type="text" name="community_name" id ="community_name" /> </td>
                        <td class="pull_right" rowspan="3">
                            <select id="bgp_community" multiple="multiple" class="bgp_community" name="bgp_community_names">
                            </select>
                            <script type="text/javascript">
                        <?php 
                            $all_bgp_community = get_isp_bgp_communities($isp_id);
                            foreach($all_bgp_community as $bgp_community)
                            {
                        ?>
                                $('#bgp_community').tokenize();
                                $('#bgp_community').tokenize().tokenAdd("<?php echo $bgp_community['community_name']."~~~".$bgp_community['community_member']."~~~".$bgp_community['community_id']; ?>","<?php echo $bgp_community['community_name']." ". $bgp_community['community_member']; ?>");
                        <?php
                            }
                        ?>
                            </script>
                        </td>
                    </tr>
                    </tr>
                    <tr>
                        <td class="pull_right" > BGP Community Members * </td>
                        <td class="pull_right" ><input type="text" name="comminuty_string" id ="community_string" />
                        </td>
                    <tr>
                        <td>&nbsp;</td>
                        <td class="pull_left"  ><input type='button' name='add' class='btn btn-mini btn-success isp_buttons' onclick='add_community(); return false;' value="Add"/>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>

                </table>
  </div> <!-- end of widget-content -->
      </div> <!-- End of widget-table -->
    </div> <!-- End of col-md-6 -->
  </div>  <!-- End of row -->

<div class="row">
    <div class="col-md-12">

      <div class="widget widget-table" class="form_table">
        <div class="widget-header">
          <i class="oicon-gear"></i><h3>Cost Details</h3>
        </div>
            <div style="padding-top: 10px;" class="widget-content">

    <table class="form_table" id="isp_form_cost_table">       
        <tr>
            <td class="isp_billing_heading">Billing Domain *</td>
            <td class="pull_right"><input type="text" name="billing_domain" list="billing_domain_list" value="<?php echo $isp_details['billing_domain'];?>"/></td>
            <td class="pull_right">Billing Cycle *</td>
            <td class="pull_left"><input type='text' name='billing_cycle' class="small" value="<?php echo $isp_details['billing_cycle'];?>"/> &nbsp;&nbsp;days &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            Starting &nbsp; &nbsp;<input type='text' name='starting_date'  value="<?php echo $isp_details['billing_start_date'];?>" class="small"/> &nbsp;&nbsp;Every Month </td>
        <tr>
                <td>&nbsp;</td>
        </tr>
    </table >       
    <table class="form_table" id="isp_form_cost_table">       
        <tr>
            <td class="isp_billing_heading">Billing Model</td>
            <td></td>
        </tr>

        <tr>
            <td >
                <input type='radio' id="committed_information_rate" name='billing_model' class='' value="committed_information_rate" <?php if ($isp_details['billing_model'] == "committed_information_rate" ) { ?> checked="checked" <?php } ?> /> 95th Percentile Rate
            </td>
        </tr>
        <tr>
            <td class="pull_right">Rate</td>
            <td class="pull_left"><input type='text' name='slab_rate' class="cost" value="<?php echo $isp_cost_details[0]['committed_information_rate']; ?>" /> &nbsp;&nbsp;bps</td>
        </tr>
        <tr>
            <td class="pull_right">Cost</td>
            <td class="pull_left"><input type='text' name='slab_cost' class="cost" value="<?php echo $isp_cost_details[0]['committed_information_cost']; ?>" /> &nbsp;&nbsp;USD</td>
        </tr>

        <tr>
            <td   class="pull_right">Burst Rate</td>
        </tr>
        <?php
            for($i=1;$i<count($isp_cost_details);$i++)
            {
        ?>
             <tr id="slabRowCount<?php echo $i;?>">
                <td class="pull_right">&nbsp;</td>
                <td class="pullleft">Tier Rate &nbsp; &nbsp; <input type='text' name='slab_rate' class="cost"  value="<?php echo $isp_cost_details[$i]['committed_information_rate']; ?>" />&nbsp;&nbsp;bps
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  Cost  &nbsp; &nbsp; <input type='text' name='slab_cost' class="cost"  value="<?php echo $isp_cost_details[$i]['committed_information_cost']; ?>" />&nbsp;&nbsp;USD</td>
            <td class="pull_left"><input type="button" class="btn-mini btn-danger isp_buttons" onclick="removeSlabs(<?php echo $i;?>);return false;" name="remove_button" id="remove" value="Remove"/></td> 
        </tr>
        <?php
            }
            echo "<input type='hidden' id='slab_row' value='".$i."'/>";
        ?>
        <tr>
            <td class="pull_right">&nbsp;</td>
            <td class="pull_left">
                <input type='button' class='btn btn-mini btn-success' name='add_button' id='admore'  onclick='addMoreSlabs(this.form); return false;' value="Add More"/>
            </td>
        </tr>

        <tr>
            <td >
                <input type='radio' id="data_transfer" value="data_transfer" name='billing_model'  <?php if ($isp_details['billing_model'] == "data_transfer" ) { ?> checked="checked" <?php } ?> /> &nbsp;&nbsp;Data Transferred
            </td>
        </tr>

        <tr>
            <td class="pull_right">Data In</td>
            <td class="pull_left"><input type='text' name='input_data' class="isp_data" value="<?php echo $isp_cost_details[0]['data_input']; ?>" /> &nbsp;&nbsp; GB</td>
        </tr>
        <tr>
            <td class="pull_right">Data Out</td>
            <td class="pull_left"><input type='text' name='output_data' class="isp_data" value="<?php echo $isp_cost_details[0]['data_output']; ?>" /> &nbsp;&nbsp; GB</td>
        </tr>
        <tr>
            <td class="pull_right">Cost</td>
            <td class="pull_left"><input type='text' name='data_transfer_cost' class="isp_data" value="<?php echo $isp_cost_details[0]['total_data_transfered_cost']; ?>" /> &nbsp;&nbsp; USD</td>
        </tr>

        <tr>
            <td class=""><input value="fixed" id="fixed" type='radio'  name="billing_model" <?php if ($isp_details['billing_model'] == "fixed" ) { ?> checked="checked" <?php } ?>  /> &nbsp;&nbsp;Fixed </td>
        </tr>

        <tr>
            <td class="pull_right">Cost</td>
            <td class="pull_left"><input type='text' name='fixed_cost' class="isp_data" value="<?php echo $isp_cost_details[0]['fixed_cost']; ?>" />  &nbsp;&nbsp;USD</td>
        </tr>
      
    </table>
</div> <!-- end of widget-content -->
      </div> <!-- End of widget-table -->
    </div> <!-- End of col-md-6 -->
  </div>
<input type="hidden" name="action" value="commit_check"/>
<input type="hidden" name="ip_interface_mgmt_update_time" value="<?php echo get_ip_interface_mgmt_time(); ?>" />
<?php include 'nexusguard/views/includes/commit_check_footer.php'; ?> 
<div class="form-actions">
            <td class="pull_right">

                <input type='button' class='btn btn-primary' onclick="location.href='pop_mgr/view=pop_isp_grid/'" name='cancel_button' id='cancel' value="Cancel"/>
                <button type='button' data-target="#myModal" class='btn btn-primary' name='commit' id = "commit_check" >Modify</button>
                <button type='button' data-target="#myModal" class='btn btn-primary' name='commit' id = "decommission" >Decommission</button>
                <?php
                    if($isp_details['offline']==1)
                    {
                        echo "<button type='button' data-target='#myModal' class='btn btn-primary' name='commit' id = 'make_online' >Make Online</button>";
                    }
                    else
                    {
                        echo "<button type='button' data-target='#myModal' class='btn btn-primary' name='commit' id = 'make_offline' >Take Offline</button>";
                    }
                ?>
            </td>


            </div>
 

</form>

<div id="confirm" class="modal hide fade">
  <div class="modal-body">
    Do you want to delete <?php echo $isp_details['name']; ?> ?
  </div>
  <div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn">Cancel</button>
    <button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Delete</button>
  </div>
</div>
<?php include "/opt/observium/html/nexusguard/views/common/ports_dialog.inc.php";  ?>

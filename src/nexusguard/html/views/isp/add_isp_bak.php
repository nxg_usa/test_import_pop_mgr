<link rel="stylesheet" href="/nexusguard/css/add_isp.css" >

<script type="text/javascript">
var rowCount = 2;
var data ;

function page_reload()
{
    window.location.reload();
}

function addMoreRows(frm)
{
    rowCount =rowCount+2;
    var recRow = '<tr id="rowCount'+rowCount+'1"><td></td><td>Local IP</td><td><input type="text" name="local_ip[]">&nbsp;/&nbsp;<input type="text" name="local_ip_subnet[]" class="small_input" style="width: 30px;"></td><td colspan="2">Neighbour IP <input type="text" name="neighbour_ip[]" style="padding-left: 20px;">&nbsp;/&nbsp;<input type="text" name="neighbour_ip_subnet[]" class="small_input" style="width: 30px;"></td></tr><tr id="rowCount'+rowCount+'2"><td></td><td>Local as</td><td><input type="text" name="local_as[]"></td><td colspan="2">Peer as &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="peer_as[]"></td></tr>';
    $('#isp_form_table > tbody > tr').eq(6+rowCount-1).after(recRow);
}

$('#remove').live('click', function () {
    $(this).closest('tr').remove();
}); 

var rocount=1;
$('#admore').live('click', function () {
   alert( $(this).closest('tr').index());
data=$(this).closest('tr').index();
});



function addMore(thisform){
rocount++;
var recRow2 = '<tr id=" rocount'+rocount+'1"> </td ><td>EIR Slab 1</td><td> Rate</td><td><input type="text" name="info_rate'+rocount+'1">&nbsp;&nbsp;mbps</td><td><input type="button" class="rem-button" name="remove_button" id="remove"> remove</td></tr>';
console.log(data);
 $('#isp_form_table > tbody > tr').eq(data+rocount-2).after(recRow2);
};

function getResult(form)
   {
   (function($) {

            var frm = $(document.myform);
            var dat = JSON.stringify(frm.serializeArray());
            var datastring = $("#dataform").serialize();
            $.ajax({ url: 'nexusguard/views/isp/process_isp_details.php',
            data: datastring,
            type: 'post',
            success: function(output) {
                   $('#errormsg').html(output);
                   if(output.indexOf('Error')==-1)
                   {
                           window.location.href="pop_mgr/view=add_isp_bak/";
                   }
                    document.write(output);
                     }
           });
       })(jQuery);
   }



</script>
</head>

<body>
<?php
echo "<div id='errormsg'></div>";
	echo "<form class='form_isp' id='dataform' method='post'>";
		echo "<table id=\"isp_form_table\">";
			echo "<tr>";
 				echo "<td colspan='3'>";
					echo "<h4>General</h4>";
				 echo "</td>";								
			echo "</tr>";
			echo "<tr>";
				 echo "<td >";
				 echo "</td >";

				echo "<td >";
					echo "Name";
				echo "</td>";
			        echo "<td >";
        				echo "<input type='text' class='input' name='name'>";
        			echo "</td>";
			echo "</tr>";



			echo "<tr>";
				 echo "<td >";
 				echo "</td >";
     
   				echo "<td>";
        				echo "Description";
       				 echo "</td>";
        			echo "<td>";
        				echo "<textarea class='textarea' name='description'> </textarea>";
        			echo "</td>";
			echo "</tr>";

			echo "<tr>";

 				echo "<td >";
				 echo "</td >";
        			echo "<td>";
       					 echo "Pop";
        			echo "</td>";
        			echo "<td>";
        				echo "<select name='pop'>";
        					echo " <option>Pop-A</option>";
        				echo "</select>";
        			echo "</td>";
			echo "</tr>";
                         echo "<tr>";
  				echo "<td colspan='3'>";
					echo "<h4>Router Details</h4>";
				 echo "</td>";
                         echo "</tr>";
			echo "<tr>";
  
 
				 echo "<td >";
				 echo "</td >";
			         echo "<td>";
				        echo "Offnet Interface";
			        echo "</td>";
			        echo "<td>";
				        echo "<select name='off-inf'>";	
				        echo " <option>ge-0/0/0</option>";
				        echo "</select>";
			        echo "</td>";
			echo "</tr>";

			 echo "<tr>";
				 echo "<td >";
				 echo "</td >";	
			        echo "<td>";	
				        echo "Offnet interface vlan";
			        echo "</td>";
        			echo "<td>";
				        echo "<input type='text' name='off-net-vlan'>";
			        echo "</td>";
			echo "</tr>";	
			echo "<tr>";	
				 echo "<td colspan='3'>";
				echo "<h4>BGP Details</h4>";
				 echo "</td>";
	
			echo "</tr>";
			echo "<tr>";	

			        echo "<td>";
				echo "</td>";
 				echo "<td>";
				        echo "Local IP";

                                echo "</td>";
    				 echo "<td>";
					echo"<input type='text' name='local_ip[]'>&nbsp;/&nbsp;<input type='text' name='local_ip_subnet[]' class='small_input' style='width: 30px;'>";
				echo "</td>";    
			        echo "<td colspan='2'>";	
				        echo " Neighbour IP <input type='text' name='neighbour_ip[]' style='padding-left: 20px;'>&nbsp;/&nbsp;<input type='text'  name='neighbour_ip_subnet[]' class='small_input' style='width: 30px;'>";
			        echo "</td>";
			 echo "</tr>";


			echo "<tr>";
				echo "<td>";
				echo "</td>";
	echo "<td>";
                                        echo "Local as";

                                echo "</td>";
                                 echo "<td>";
                                        echo"<input type='text' name='local_as[]'>";
                                echo "</td>";
                                echo "<td colspan='2'>";
                                        echo " Peer as &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;<input name='peer_as[]' type='text'>";
                                echo "</td>";
	
			 echo "</tr>";
echo "<div id='addedRows'> </div>";
echo "<tr>";
  echo "<td>";
   echo "</td>";
   echo "<td colspan='4'>";
         echo "<input type='button' name='add' class='rem-button' onclick='addMoreRows(this.form); return false;'> Add more (application in case of internet exchange)";
         echo "</td>";

 echo "</tr>";




echo "<tr>";
 echo "<td colspan='3'>";
echo "<h4>Cost Details</h4>";
 echo "</td>";
echo "</tr>";
echo "<tr>";
 echo "<td>";
echo "<h6>Billing Model</h6>";
 echo "</td>";
echo "</tr>";


echo "<tr>";
 echo "<td>";
echo "<input type='radio' name='billing-model' class='radio' style='text-align: center; padding-left: 15px;'> Commited Information Rate";
 echo "</td>";
echo "</tr>";

 echo "<tr>";
 echo "<td >";
 echo "</td >";
        echo "<td>";
        echo " Rate";
        echo "</td>";
        echo "<td>";
        echo "<input type='text' name='rate'> mbps";
        echo "</td>";
echo "</tr>";


echo "<tr>";
 echo "<td >";
 echo "</td >";
        echo "<td>";
        echo " Cost";
        echo "</td>";
        echo "<td>";
        echo "<input type='text' name='cost'> USD";
        echo "</td>";
echo "</tr>";

echo "<tr>";
 echo "<td >";
 echo "</td >";

        echo "<td>";
        echo " Excess Information Rate";
        echo "</td>";
echo "</tr>";
echo "<tr>";
 echo "<td >";
 echo "</td >";
         echo "<td>";
         echo "EIR Slab 1";
         echo "</td>";
         echo "<td>";
         echo "Rate";
         echo "</td>";
          echo "<td>";
         echo "<input type='text' name='info_rate'>&nbsp;&nbsp;mbps";
         echo "</td>";
         echo "<td>";
         echo "<input type='button' class='rem-button' name='remove_button' id='remove'> remove";
         echo "</td>";

echo "</tr>";

echo "<tr>";
 echo "<td >";
 echo "</td >";
echo "<td>";
         echo "<input type='button' class='rem-button' name='add_button' id='admore'  onclick='addMore(this.form); return false;'> Add More";
         echo "</td>";
echo "</tr>";





 echo "</td>";
 echo "<td>";
echo "<input type='radio' class='radio' style='text-align: center; padding-left: 15px;' name='data_transfer'> Data Transferred";
 echo "</td>";
echo "</tr>";

echo "<tr>";
 echo "<td >";
 echo "</td >";
        echo "<td>";
        echo "Data In";
        echo "</td>";
        echo "<td>";
        echo "<input type='text' name='input_data'> GB";
        echo "</td>";
echo "</tr>";
echo "<tr>";
 echo "<td >";
 echo "</td >";
        echo "<td>";
        echo "Data Out";
        echo "</td>";
        echo "<td>";
        echo "<input type='text' name='output_data'> GB";
        echo "</td>";
echo "</tr>";
echo "<tr>";
 echo "<td >";
 echo "</td >";
        echo "<td>";
        echo "Cost";
        echo "</td>";
        echo "<td>";
        echo "<input type='text' name='data_transfer_cost'> USD";
        echo "</td>";
echo "</tr>";

echo "<tr>";
 echo "<td>";
echo "<input type='radio' class='radio' style='text-align: center; padding-left: 15px;'> Fixed ";
 echo "</td>";
echo "</tr>";

echo "<tr>";
 echo "<td>";
 echo "</td>";
        echo "<td>";
        echo "Cost";
        echo "</td>";
        echo "<td>";
        echo "<input type='text' name='fixed_cost'> USD";
        echo "</td>";
echo "</tr>";
echo "<tr>";
echo "<td>";
        echo "Billing Cycle";
        echo "</td>";
        echo "<td>";
        echo "<input type='text' name='days'> days";
        echo "</td>";

         echo "<td style='padding-left:50px;'>";
        echo "Starting";
        echo "</td>";


        echo "<td>";
        echo "<input type='text' name='starting_date'>Every Month";
        echo "</td>";
 echo "</tr>";
 
echo "<tr>";
  echo "<td>";
   echo "</td>";

echo "<td>";
         echo "<button type='button' class='commit-button' name='cancel_button'> Cancel</button>";
         echo "</td>";
	echo "<td>";
         echo "<button type='submit' class='commit-button' name='commit' onclick=\"getResult(this.form);return false;\"> Commit Check </button>";
         echo "</td>";

 echo "</tr>";

echo "</table>";
echo "</form>";
echo "</div>";
?>


<?php

include_once '/opt/observium/html/nexusguard/views/includes/common_includes.php';

$isp_id = $vars['isp_id'];

function delete_isp($isp_id)
{
    global $json_dir;
    $delete_isp_config = delete_isp_config($isp_id);

    $isp_details = get_isp_details ($isp_id);
    $pop_details = get_sinlge_pop_detail($isp_details['pop_details_id']);

    //get device details
    $device_details = dbFetchRow("select hostname from devices where device_id = ?",array($pop_details['pop_edge_router_device_id']));
    $hostname = $device_details['hostname'];

    $device_username = $pop_details['username'];
    $device_password = $pop_details['password'];

    $isp_name = $delete_isp_config['name'];
    $epoch_time = time();
    $action= "commit";

    $json_tmp_filename = "delete_".$isp_name."_".$epoch_time.".json";
    $conf_tmp_filename = "delete_".$isp_name."_".$epoch_time.".set";
    $commit_tmp_filename = "delete._".$isp_name.".".$epoch_time.".commit_check.log";
    $commit_log_tmp_filename = "delete._".$isp_name.".".$epoch_time.".commit.log";
    $json_file = $json_dir.$json_tmp_filename;
    $conf_file = $json_dir.$conf_tmp_filename;
    $commit_check_file = $json_dir.$commit_tmp_filename;
    $commit_log_check_file = $json_dir.$commit_log_tmp_filename;
    echo $json_file;
    $delete_isp_config['conf_file'] = $conf_file;
    $delete_isp_config['commit_check_log'] = $commit_check_file;
    $delete_isp_config['commit_log'] = $commit_log_check_file;

    $fp = fopen($json_file,'w');
    fwrite($fp,str_replace("\\","", json_encode($delete_isp_config)));
    fclose($fp);

    $cmd = "/bin/bash /opt/observium/nexusguard/ansible/nxg_del_isp.sh  $hostname $device_username $device_password $action $json_file";
    echo $cmd;
    exec($cmd,$result,$status);
    if($status !=0 )
    {
        write_error_response("ISPDelErr01","Error in deleting isp ");
    } else
    {
        delete_isp_db($isp_id);
        header("Location: /pop_mgr/view=pop_isp_grid/");
    }
}
delete_isp($isp_id);
?>

<?php

include_once "/opt/observium/html/nexusguard/views/includes/common_includes.php";


//retrieve contents
$data = json_decode(file_get_contents('php://input'), true);
//Validate isp details
$errormsg=validate_isp_details_form($data);

$pop_id = $data['pop_id'];
$pop_details = get_sinlge_pop_detail($pop_id);

//get device details
$device_details = dbFetchRow("select hostname from devices where device_id = ?",array($pop_details['pop_edge_router_device_id']));
$hostname = $device_details['hostname'];

$device_username = $pop_details['username'];
$device_password = $pop_details['password'];
$action = "commit_check";

$isp_name = $data['isp_name'];
//get ISP configuration
$isp_config = get_isp_config($data);

$epoch_time = time();

$json_tmp_filename = $isp_name."_".$epoch_time.".json";
$config_tmp_filename = $isp_name."_".$epoch_time.".conf";
$commit_check_tmp_filename = $isp_name."_".$epoch_time."_commit_check.log";
$commit_log_tmp_filename = $isp_name."_".$epoch_time.".log";

$json_filename = $json_dir.$json_tmp_filename;
$config_filename = $config_dir.$config_tmp_filename;
$commit_check_filename = $commit_check_dir.$commit_check_tmp_filename;
$commit_log_filename = $commit_log_dir.$commit_log_tmp_filename;

$isp_config ['conf_file'] = $config_filename; 
$isp_config ['commit_check_log'] = $commit_check_filename; 
$isp_config ['commit_log'] = $commit_log_filename;

//Write json file
$fp = fopen($json_filename,'w');
fwrite($fp, str_replace("\\","", json_encode($isp_config)));
fclose($fp);


$cmd = "/bin/bash -x /opt/observium/nexusguard/ansible/nxg_add_isp.sh $hostname $device_username $device_password $action $json_filename $config_filename $commit_check_filename";
exec($cmd,$result,$status);
if( $status != 0 )
{
    $err = shell_exec("cat ".$commit_log_filename);
    write_error_response("ccerr01","Error in executing commit check<br><pre>".$err."</pre>");
}

unset($result);

$cmd = "cat ".$commit_check_filename;
$result = shell_exec($cmd); 
if( $result == "" )
{
    write_error_response("ccerr01","Error in getting commit check output");
}

$output = array('output'=>$result);
//echo json_encode($output);

?>
<!--<form id="isp_config_form">-->
    <h4>Commit Check Output</h4>
    <pre style="height:150px;overflow:auto">
        <?php echo $result; ?>
    </pre>
    <input type="hidden" name="json_file" value="<?php echo $json_filename; ?>"/>
    <input type="hidden" name="config_file" value="<?php echo $config_filename; ?>"/>
    <!--<button type='button' class='btn btn-default' name='apply_config' onclick="applyConfig(this.form);return false;"> Apply Config</button>-->
<!--/form>-->

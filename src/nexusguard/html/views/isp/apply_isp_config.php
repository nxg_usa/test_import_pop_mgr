<?php

include_once "/opt/observium/html/nexusguard/views/includes/common_includes.php";


//retrieve contents

$data = json_decode(file_get_contents('php://input'), true);

validate_isp_details_form($data);
$json = $data['json_file'];
$conf = $data['config_file'];

$pop_id = $data['pop_id'];
$pop_details = get_sinlge_pop_detail($pop_id);

//get device details
$device_details = dbFetchRow("select hostname from devices where device_id = ?",array($pop_details['pop_edge_router_device_id']));
$hostname = $device_details['hostname'];

$device_username = $pop_details['username'];
$device_password = $pop_details['password'];
$action = "commit";

$cmd = "/bin/bash -x /opt/observium/nexusguard/ansible/nxg_add_isp.sh $hostname $device_username $device_password $action $json";
echo $cmd;

$isp_config = json_decode(file_get_contents($json),true);
exec($cmd,$result,$status);
//echo $status;
if($status == 0)
{
    $ret = add_an_isp($data);
    update_config($isp_config,$ret,$pop_id);
    //$ret = add_to_audit_log();
}


?>


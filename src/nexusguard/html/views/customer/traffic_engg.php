<?php include '/opt/observium/nexusguard/db/db_customer_functions.php' ;?>

<?php
$cust_id = $vars['customer_id'];
$cust_details = get_db_cust_details($cust_id);
$prefix_list = dbFetchRow("select group_concat(net_srv_prefix separator '\n') as prefixes from nxg_customer_net_service where cust_id=? and net_srv_prefix != 'ALL-PREFIXES' ",array($cust_id));
?>




<link rel="stylesheet" href="/nexusguard/css/jquery.tokenize.css" >
<link rel="stylesheet" href="/nexusguard/css/pop_manager.css" >
<link rel="stylesheet" href="/nexusguard/css/bootstrap-panel.css" >
<script src="/nexusguard/js/common.js"></script> 
<script src="/nexusguard/js/tokenizer/jquery.tokenize.js"></script>

<script>
var inboundRow =0;
var rows_loaded = 0;
var outbound_te_changed = 0;
var inbound_te_changed = 0;

function addRowsToInbound()
{
        inbound_te_changed=1;
        inboundRow = inboundRow+1;
        pop_option = $('<select class="input_box" id="inbound_pop'+inboundRow+'" name="inbound_pop" onChange="pop_change('+inboundRow+')"></select>');

        <?php
            foreach (dbFetchRows('select distinct id, pop_name from nxg_pop_details left join nxg_customer_conn on nxg_customer_conn.pop_id = nxg_pop_details.id where nxg_customer_conn.cust_id = ? order by pop_name',array($cust_id)) as $data)
            {
                echo('       pop_option.append(" <option value=\"'.$data['id'].'\" >'.$data['pop_name'].'</option>");');
            }
        ?>
         
        if(inboundRow ==3)
             $('#nxg_inbound_services').append('<tr><th>PoP</th><th>Prefix</th><th>Neighbor</th><th>Community</th><th>ASN Level </th</tr>');

        row = $('<tr id="inbound_rules'+inboundRow+'" ></tr>');
        pop_col= $('<td valign="top" class="pull_left" ></td>').append(pop_option);
        //row.append('<td class="pull_right" >PoP</td>');
        row.append(pop_col);


        prefix_option = $('<td valign="top" clasis="pull_left"><select multiple="multiple" class="isp_token" id="inbound_prefix'+inboundRow+'" name="inbound_prefix"></select></td>');
        ///row.append('<td class="pull_right" >Prefix</td>');
        row.append(prefix_option);

        isp_option = $('<td valign="top" class="pull_left"><select multiple="multiple" class="isp_token" id="inbound_isp'+inboundRow+'" name="inbound_isp"></select></td>');
        //row.append('<td class="pull_right" >ISP</td>');
        row.append(isp_option);

        community_option = $('<td valign="top" class="pull_left"><select  multiple="multiple" class="input_box" id="inbound_community'+inboundRow+'" name="inbound_community"></select></td>');
        //row.append('<td class="pull_right" >Community</td>');
        row.append(community_option);

        asn_option = $('<td valign="top" class="pull_left"><select class="input_box small" id="inbound_asn'+inboundRow+'" name="inbound_asn"></select></td>');
        //row.append('<td class="pull_right" >ASN Level</td>');
        row.append(asn_option);


        row.append('<td valign="bottom" ><input type="button" id="remove'+inboundRow+'" class="btn btn-mini btn-danger" onclick="removeInboundService('+inboundRow+');return false;" value="Remove"/></input><input type="hidden" name="inbound_action" id="action'+inboundRow+'" value="add" /></td>');

        $('#nxg_inbound_services').append(row);
       
         pop_change(inboundRow);

        
        <?php
            foreach (dbFetchRows('select id, net_srv_prefix  from nxg_customer_net_service where cust_id = ? ',array($cust_id)) as $data)
            {
                echo('       $("#inbound_prefix"+inboundRow).append(" <option value=\"'.$data['net_srv_prefix'].'\" >'.$data['net_srv_prefix'].'</option>");');
            }
        ?>


/*        $("#inbound_prefix"+inboundRow).append("<option value='1'>111.111.11.0/24</option>");
        $("#inbound_prefix"+inboundRow).append("<option value='2'>111.111.12.113/24</option>");
        $("#inbound_prefix"+inboundRow).append("<option value='3'>111.111.13.113/24</option>");
/*
        $("#inbound_community"+inboundRow).append("<option value='1'>BLACKHOLE-6461:5990</option>");
        $("#inbound_community"+inboundRow).append("<option value='2'>NO-EXPORT-65535:65281</option>");
        $("#inbound_community"+inboundRow).append("<option value='3'>NO-EXPORT-ASIA-2914:4429</option>");
*/

        $("#inbound_asn"+inboundRow).append("<option value='none'>none</option>");
        $("#inbound_asn"+inboundRow).append("<option value='0'>0</option>");
        $("#inbound_asn"+inboundRow).append("<option value='1'>1</option>");
        $("#inbound_asn"+inboundRow).append("<option value='2'>2</option>");
        $("#inbound_asn"+inboundRow).append("<option value='3'>3</option>");
        $("#inbound_asn"+inboundRow).append("<option value='4'>4</option>");
        $("#inbound_asn"+inboundRow).append("<option value='5'>5</option>");


        $('#inbound_prefix'+inboundRow).tokenize({newElements:false });
        $('#inbound_community'+inboundRow).tokenize({newElements:false });
        $('#inbound_isp'+inboundRow).tokenize({newElements:false});
        $('#inbound_isp'+inboundRow).data("index",inboundRow);

}


function generic_token_add(value, text, e)
{
    id = $(e.select).data("index");
    if(rows_loaded)
        do_action_as_modify(id);
}

function generic_token_remove(value, e)
{
    id = $(e.select).data("index");
    if(rows_loaded)
        do_action_as_modify(id);
}


function ispAddToken(value, text, e){

           id = $(e.select).data("index");
           len = e.toArray().length;
           if(len>1)
           {
                $('#inbound_community'+id).tokenize().clear();
                $('#inbound_community'+id).tokenize().disable();
           } 

    if(rows_loaded)
        do_action_as_modify(id);

}

function ispRemoveToken(value, e){

           id = $(e.select).data("index");
           len = e.toArray().length;
           if(len<2)
           {
                $('#inbound_community'+id).tokenize().enable();
           }
    if(rows_loaded)
        do_action_as_modify(id);

}

function  do_action_as_modify(id)
{
    inbound_te_changed =1;
    if( $('#action'+id).val() == "none")
         $('#action'+id).val("modify");
}

function removeInboundService(id)
{
    
    if( $('#action'+id).val() == "add")
    {
        $('#inbound_rules'+id).remove();
    }
    else
    {
        $('#remove'+id).val("Removed");
        $('#remove'+id).hide();
        $('#remove'+id).parent().append("<span style='color:red;'>Marked for Removal</span>");
        $('#action'+id).val("delete");
    }
    inbound_te_changed = 1;
}

/*function addRowsToInbound1()
{
//   outboundRow =outboundRow+1;
  inboundRow = inboundRow+1;
    var recRow = '<tr id="inboundRow'+inboundRow+'">  <td class="pull_right">PoP</td><td  class="pull_left"><select class="input_box" id="inbound_pop'+inboundRow+'" name="inbound_pop" onChange="pop_change('+inboundRow+')"></select></td><td class="pull_right">Neighbor&nbsp;</td><td class="pull_center" ><select class="input_box" id="inbound_isp'+inboundRow+'" name="inbound_isp"></select></td>              \
 <td class="pull_right">ASN Level</td>                                          \
         <td colspan="1" class="pull_center" >                                  \
            <select class="input_box" id="isp_asn_level" name="isp_asn_level">  \
                <option value="0">0</option>            \
                <option value="1">1</option>            \
                <option value="2">2</option>            \
                <option value="3">3</option>            \
                <option value="4">4</option>            \
            </select>                                   \
        </td>                                           \
        <td><input type="button" id="inboundRow'+inboundRow+'" class="btn btn-mini btn-danger" onclick="removeInboundService('+inboundRow+');return false;" value="Remove"/></input> <input type="hidden" id="inbound_action" name="action'+inboundRow+'" value="add" /> </td>                                       \
        </tr>';
        


    $('#nxg_inbound_services > tbody > tr').eq(1+inboundRow-3).after(recRow);

    var $options = $("#inbound_pop0 > option").clone();
    $('#inbound_pop'+inboundRow).append($options);    
    $('#inbound_pop'+inboundRow).val($("#inbound_pop0").val());
    pop_change(inboundRow);

}*/


function pop_change(id)
{
    pop_id = $("#inbound_pop"+id).val();
    $("#outbound_pop"+id).val(pop_id)
    $('#inbound_isp'+id).find('option').remove();
    $('#inbound_community'+id).find('option').remove();
    switch(parseInt(pop_id))
    {
         <?php
        $db_pop_id = -1;
        foreach (dbFetchRows('select pop_details_id,id, name from nxg_isp_details  order by pop_details_id,name') as $data)
                {
            if($db_pop_id !=$data[pop_details_id])
            {
                if($db_pop_id != -1)
                    echo "break;\r\n";

                    $db_pop_id= $data[pop_details_id];
                    echo "case ".$db_pop_id .":\r\n";
                    echo "$('#inbound_isp').find('option').remove();\r\n";
                    echo "$('#inbound_community').find('option').remove();\r\n";
            }
            echo "$('#inbound_isp'+id).find('option').end().append('<option value=\"".$data['id']. "\" >".$data['name']."</option>');\r\n";
            foreach (dbFetchRows('select nxg_isp_details.pop_details_id as pop_details_id , community_id, community_name,community_member from nxg_isp_bgp_community left join nxg_isp_details on nxg_isp_details.id = nxg_isp_bgp_community.isp_id left join nxg_pop_details on nxg_pop_details.id= nxg_isp_details.pop_details_id where nxg_isp_details.pop_details_id =? and nxg_isp_bgp_community.isp_id =?',array($data['pop_details_id'],$data['id'])) as $data)
            {
                echo "$('#inbound_community'+id).find('option').end().append('<option value=\"".$data['community_id']. "\" >".$data['community_name']." ".$data['community_member']."</option>');\r\n";
            }

        }
        ?>

    }
}



$(document).ready(function () {
     cnt= parseInt($('#inbound_row_count').val());
     pop_change(cnt);
    if(cnt != 0 )
        inboundRow = inboundRow +cnt +1;

   $( "#cancel_config" ).click(function() {
        $("#isp_config").hide();
        $("#isp_form :input").prop('readonly', false);
    });
    $( "#applyconfig" ).click(function() {
            var action = $("#applyconfig").attr('action');
            var url = '/nexusguard/api/'+action+'_isp.php';
            apply_config('isp_form',url);

    });
    $( "#commit_check" ).click(function() {

        var request_array= [];
        if(inbound_te_changed == 1 && outbound_te_changed ==1)
        {
            alert("Both Outbound and Inbound Traffic Engineering data has changed. System can process only one type of request at time. Only Inbound Traffic Engineering data will be submitted.");

        }

        if(inbound_te_changed == 1)
            request_array = form_inbound_te_request();
        else
        if( outbound_te_changed == 1)
           request_array = form_outbound_te_request();
        

        if(request_array.length >0)
        {
            button_array = ["cancel","commit_check"];
            commit_check2(request_array,button_array);
        }
        else
            alert("No rules changed, so not commiting any changes");

    });

    function form_outbound_te_request()
    {
        var request_array= [];
        var i = 0;

        rows = $('tr', "#outbound_te_table");
        for(i=0;i<rows.length;i++)
        {
            row = rows.eq(i);
            pop_id = $(row).find(":nth-child(1)").html();
            pop_name = $(row).find(":nth-child(2)").html();
            select = $(row).find(":nth-child(3)").find(":first-child");
            if($(select).data("changed")== "yes")
            {
                req_data = {};
                req_data.pop_id = pop_id;
                req_data.outbound_type = $(select).val();
                req_data.cust_id = <?php echo $cust_id; ?>;
                req_data.action = "commit_check";


                req = [];
                req["url"] = "/nexusguard/api/outbound_traffic_engg.php" ;
                req["apply_url"] = "/nexusguard/api/outbound_traffic_engg.php" ;
                req["data"] = req_data ;
                req["entity_name"] =  pop_name ;
                request_array[i]=req;
                i++;
            }
        }

        return request_array;

    }

    function form_inbound_te_request()
    {
            var form_data = {};
            var pop_name_array = {};
    var cust_data = parse_form('traffic_engg_form');
    $('#nxg_inbound_services tr').each(function() {

        var cells = $(this).find('td input, select, textarea');
        var data ={};
        var pop_id = "";
        cells.each(function(){

        var value = $(this).val();
        var name = $(this).attr('name');
        if(name=="inbound_pop")
        {
            pop_id = value;
            pop_name_array[pop_id]= $(this).find('option:selected').text();
        }
        if (data[name] !== undefined) {
            if (!data[name].push) {
                data[name] = [data[name]];
            }
            else
                data[name].push(value || '');
        }
        else
            data[name] = value || '';

        });
        if (form_data[pop_id] !== undefined) {
            if (!form_data[pop_id].push) {
                form_data[pop_id] = [form_data[pop_id]];
                form_data[pop_id].push(data || '');
            }
            else
                form_data[pop_id].push(data || '');
        }
        else
            form_data[pop_id] = data || '';

     });
     var request_array= [];
        var i = 1;
        $.each(form_data, function(key, value) {
            if(key)
            {
              if( Array.isArray(value))
                {
                    pop_changed = 0;
                    $.each(value, function(rule_id, rule_value) {

                        if(rule_value.inbound_action != "none")
                             pop_changed = 1;
                    });

                    if(pop_changed ==0)
                        return;
            }
            else
                {
                    if(value.inbound_action == "none")
                        return;
                }

            cust_data.inbound_traffic_data = value;
            req = [];
            req["url"] = "/nexusguard/api/traffic_engg.php" ;
            req["apply_url"] = "/nexusguard/api/traffic_engg.php" ;
            req["data"] = cust_data ;
            req["entity_name"] =  pop_name_array[key] ;
            request_array[i-1]=req;
            i++;
            }
        });

       return request_array;

    }


    $(".outbound_te_select").change(function(){
            $(this).data("changed","yes");
            outbound_te_changed = 1;
    });

});

function create_in_traffic_pop_data()
{

    var data = {};
    var elem = document.getElementById(form_info).elements;
    for(var i = 0; i < elem.length; i++)
    {
        if (data[elem[i].name] !== undefined) {
        }
        else
        {
        }
    }
}
</script>
<h3 class="form_heading">Traffic Engineering</h3>
<form id='traffic_engg_form' method='post'>
    <div class="row">
    <div class="col-md-12">

      <div class="widget widget-table">
        <div class="widget-header">
          <i class="oicon-gear"></i><h3>General</h3>
        </div>
            <div style="padding-top: 10px;" class="widget-content"> 
    <table class="form_table" >
    <tr>
        <td class="pull_right">OSS Client ID</td>
        <td class="pull_left"><input disabled type="text" class="input" name="oss_id" value="<?php echo $cust_details['ossid'];?>"/></td>
        <td class="pull_right">Name</td>
        <td class="pull_left"><input disabled type="text" class="input" name="customer_name" value="<?php echo $cust_details['name'];?>"/><input type="hidden" value="<?php echo $cust_id;?>" name="cust_id"/> </td>
        
        <td class="pull_right">Notes</td>
        <td rowspan="3" class="pull_left" valign="top" ><textarea disabled rows="8" cols="2000" class="notes_textarea"type="text" name="notes" value=""><?php echo $cust_details['notes'];?></textarea></td>
    </tr>
     <tr>
        <td class="pull_right">Net Service Prefix</td><td colspan="3" class="pull_left"><textarea disabled class="textarea"type="text" name="net_srv_prefix" value=""><?php echo $prefix_list['prefixes'];?></textarea></td>
    </tr>
</table>
        
 </div> <!-- end of widget-content -->
      </div> <!-- End of widget-table -->
    </div> <!-- End of col-md-6 -->
  </div>  <!-- End of row --> 

<div class="row">
    <div class="col-md-12">

      <div class="widget widget-table">
        <div class="widget-header">
          <i class="oicon-gear"></i><h3>Inbound Traffic</h3>
        </div>
            <div style="padding-top: 10px;" class="widget-content">

    <table id="nxg_inbound_services"  class="form_table">
<?php
$data = dbFetchRows("select nxg_customer_inbound_rule.*,nxg_pop_details.pop_name as pop_name ,group_concat(nxg_customer_rule_prefix.prefix) as prefix_list, group_concat(nxg_customer_rule_isp.isp_id) as isp_list,group_concat(nxg_customer_rule_community.community_id) as community_list from nxg_customer_inbound_rule left join nxg_customer_rule_prefix on nxg_customer_inbound_rule.rule_id = nxg_customer_rule_prefix.rule_id left join nxg_customer_rule_isp on nxg_customer_rule_isp.rule_id = nxg_customer_inbound_rule.rule_id left join nxg_customer_rule_community on nxg_customer_rule_community.rule_id = nxg_customer_inbound_rule.rule_id left join nxg_pop_details on nxg_customer_inbound_rule.pop_id = nxg_pop_details.id where cust_id= ? group by nxg_customer_inbound_rule.rule_id",array($cust_id));
if(count($data) > 0 )
{
    echo "<tr><th>PoP </th><th>Prefix</th><th>Neighbor</th><th>Community</th><th>ASN Level</th</tr>";
}
$row_count = 1;
foreach($data as $row)
{
    echo '<tr id="inbound_rules'.($row_count+1).'">';
    echo '<td valign="top" class="pull_left" >';
    echo '<input type="hidden" name="rule_id" value="'.$row['rule_id'].'"/>';
    echo '<select class="input_box" id="inbound_pop'.$row_count.'" name="inbound_pop" onChange="pop_change('.$row_count.')" readonly>';
    foreach(dbFetchRows('select distinct id, pop_name from nxg_pop_details left join nxg_customer_conn on nxg_customer_conn.pop_id = nxg_pop_details.id where nxg_customer_conn.cust_id = ? order by pop_name',array($cust_id)) as $pop_data)
    {
        if($row['pop_id'] == $pop_data['id'])
        {
            echo '<option value="'.$pop_data['id'].'" selected=selected >'.$pop_data['pop_name'].'</option>';
        }
        else
        {
            echo '<option value="'.$pop_data['id'].'" >'.$pop_data['pop_name'].'</option>';
        }
    }
    echo "</select></td>"; 
    echo '<td valign="top" clasis="pull_left"><select multiple="multiple" class="isp_token" id="inbound_prefix'.$row_count.'" name="inbound_prefix">';
    foreach(dbFetchRows("select id, net_srv_prefix  from nxg_customer_net_service where cust_id = ? ",array($cust_id)) as $prefix_data)
    {
        echo '<option value="'.$prefix_data['net_srv_prefix'].'" >'.$prefix_data['net_srv_prefix'].'</option>';
    }
    echo '</select></td>
        <td valign="top" class="pull_left"><select multiple="multiple" class="isp_token" id="inbound_isp'.$row_count.'" name="inbound_isp">';
    foreach(dbFetchRows("select id,name from nxg_isp_details where pop_details_id = ? ",array($row['pop_id']))as $isp_data)
    {
        echo "<option value='".$isp_data['id']."'>".$isp_data['name']."</option>";
    }


    echo '</select>';
    $isp_count = 0;
/*    foreach(dbFetchRows("select id,name from nxg_isp_details where id in ( ? )",array(explode(",",$row['isp_list']))) as $isp_name)
    {
        echo "<script type='text/javascript'>";
        echo "$('#inbound_isp".$row_count."').tokenize().tokenAdd('".$isp_name['id']."','".$isp_name['name']."')";
        echo "</script>";
        $isp_count++;
    }
*/
    echo '</td>';
    /*if($isp_count > 1)
      echo '<td valign="top" class="pull_left"><select  multiple="multiple" class="input_box" id="inbound_community'.$row_count.'" name="inbound_community" disabled>';
      else
     */
    echo '<td valign="top" class="pull_left"><select  multiple="multiple" class="input_box" id="inbound_community'.$row_count.'" name="inbound_community">';

    foreach (dbFetchRows('select nxg_isp_details.pop_details_id as pop_details_id , community_id, community_name,community_member from nxg_isp_bgp_community left join nxg_isp_details on nxg_isp_details.id = nxg_isp_bgp_community.isp_id left join nxg_pop_details on nxg_pop_details.id= nxg_isp_details.pop_details_id where nxg_isp_details.pop_details_id =?',array($row['pop_id'])) as $prefix_data)
    {
        echo "<option value=\"".$prefix_data['community_id']. "\" >".$prefix_data['community_name']." ".$prefix_data['community_member']."</option>";
    }
    echo '</select></td>';

    $asn_levels = array('none','0','1','2','3','4','5');

    echo '<td valign="top" class="pull_left"><select class="input_box small" id="inbound_asn'.$row_count.'" name="inbound_asn" onchange="do_action_as_modify('.$row_count.');"  >';
    foreach($asn_levels as $level)
    {
        if($level == $row['asn_level'] )
        {
            echo "<option value='".$level."' selected=selected>".$level."</option>";
        }
        else
        {
            echo "<option value='".$level."'>".$level."</option>";
        }
    }
    echo '</select></td>';
    echo '<td><input type="button" id="remove'.($row_count).'" class="btn btn-mini btn-danger" onclick="removeInboundService('.($row_count).');return false;" value="Remove"/></input> <input type="hidden" id="action'.($row_count).'" name="inbound_action" value="none" /> </td>';
    foreach(explode(",",$row['prefix_list']) as $prefix)
    {
        echo "<script type='text/javascript'>";
        echo "$('#inbound_prefix".$row_count."').tokenize({newElements:false,onAddToken:generic_token_add,onRemoveToken:generic_token_remove}).tokenAdd('".$prefix."','".$prefix."');";
        echo "$('#inbound_prefix".$row_count."').data('index',".$row_count.");";
        echo "</script>";
    }
    echo "<script type='text/javascript'>";
    echo "$('#inbound_community".$row_count."').tokenize({newElements:false,onAddToken:generic_token_add,onRemoveToken:generic_token_remove});";
    echo "$('#inbound_community".$row_count."').data('index',".$row_count.");";
    echo "</script>";
    foreach(dbFetchRows("select * from nxg_isp_bgp_community where community_id in ( ? )",array(explode(",",$row['community_list']))) as $community_details)
    {
        echo "<script type='text/javascript'>";
        echo "$('#inbound_community".$row_count."').tokenize().tokenAdd('".$community_details['community_id']."','".$community_details['community_name']." ".$community_details['community_member']."');";
        echo "</script>";
    }

        echo "<script type='text/javascript'>";
        echo "$('#inbound_isp".$row_count."').tokenize({newElements:false,onAddToken:ispAddToken,onRemoveToken:ispRemoveToken});";
        echo "$('#inbound_isp".$row_count."').data('index',".$row_count.");";
        echo "</script>";
         foreach(dbFetchRows("select id,name from nxg_isp_details where id in ( ? )",array(explode(",",$row['isp_list']))) as $isp_name)
    {
        echo "<script type='text/javascript'>";
        echo "$('#inbound_isp".$row_count."').tokenize().tokenAdd('".$isp_name['id']."','".$isp_name['name']."');";
        echo "</script>";
        $isp_count++;
    }

    $row_count++;

}
echo "<input type='hidden' id='inbound_row_count' value='".$row_count."'/>";
echo "<script>rows_loaded = 1;</script>";
?>
    </table>

   <table id="nxg_inbound_services1"  class="form_table">
    <tr>
        <td colspan="1" class="pull_right"><input type="button"class='btn btn-mini btn-success' id="inbound" onClick="addRowsToInbound(); return false;" value="Add More"/></td>
    </tr>
</table>




            </div> <!-- end of widget-content -->
      </div> <!-- End of widget-table -->
    </di
v> <!-- End of col-md-6 -->
  </div>  <!-- End of row -->

<div class="row">
    <div class="col-md-12">

      <div class="widget widget-table">
        <div class="widget-header">
          <i class="oicon-gear"></i><h3>Outbound Traffic</h3>
        </div>
            <div style="padding-top: 10px;" class="widget-content">


<table id="outbound_te_table" class="form_table">
<?php foreach (dbFetchRows('select nxg_customer_conn.*,pop_name from nxg_customer_conn,nxg_pop_details where nxg_customer_conn.pop_id=nxg_pop_details.id and nxg_customer_conn.customer_conn="tunnel" and nxg_customer_conn.cust_id='.$cust_id) as $data)
            {
    ?>
<tr><td style="display:none;" ><?php echo $data["pop_id"]; ?></td><td class="pull_right"><?php echo $data["pop_name"]; ?></td><td class="pull_right"><select class="outbound_te_select" name="outbnound_te"><?php 
                    if($data["outbound_te"] == 0) 
                        echo "<option value=0 selected=selected>Best Path Routing</option>";
                    else
                        echo "<option value=0 >Best Path Routing</option>";
                        
                    if($data["outbound_te"] == 1)
                        echo "<option value=1 selected=selected>Type 1 Neighbours</option>";
                    else
                        echo "<option value=1 >Type 1 Neighbours</option>";

                    if($data["outbound_te"] == 2)
                        echo "<option value=2 selected=selected>Type 2 Neighbours</option>";
                    else
                        echo "<option value=2 >Type 2 Neighbours</option>";
                ?></select></td></tr>
<?php } ?></table>

</div> <!-- end of widget-content -->
      </div> <!-- End of widget-table -->
    </div> <!-- End of col-md-6 -->
  </div>  <!-- End of row -->
</form>

<?php include 'nexusguard/views/includes/commit_check_footer2.php'; ?>

<div class="form-actions">
    <input type="button" class="btn btn-primary"  onclick="window.location.href='/pop_mgr/view=customer_grid/'" id="cancel" name="cancel" value="Cancel"/ >
    <input type="button" class="btn btn-primary" id="commit_check" name="Commit Check" value="Commit Check"/>
    <input type="hidden" name="action" value="commit_check" />
</div>

<script type="text/javascript">
function getData(strCommitLog)
{
    (function($) {
                        var text =  strCommitLog ;
                        $("#customer_grid_filter").html(text);
                        jQuery("#popupModel").modal('show');
     })(jQuery);
}


selected_cust_id=-1;
more_selected=false;
$( document ).ready(function() {
    $("#traffic_engg").click(function () {
         var check_count =0;
        $('.customer_check').each(function( index ) {
                if($(this).prop('checked'))
                {
                    selected_cust_id  = $(this).val();
                    check_count++;
                }            
        });

        if(check_count>1)
        {
            alert('Please select only one Customer');
        }else if(check_count==0)
        {
            alert('Please select any Customer');
        }else
        {
            location.href='pop_mgr/view=traffic_engg/customer_id='+selected_cust_id+'/refresh=0/';
        }
        return false;
    });

});

</script>
<style>
.selectpicker{
     margin-right: 50px;
 }
.isp_details{
float:left;
padding-top:20px;
clear:left;
}
.intf_graph{
float:left;
padding-top:20px;
padding-left:30px;
}
.intf_stats{
float:right;
padding-left:30px;
}
.pull_right{
float:right;
}
.isp_details1{
float:left;
padding-top:20px;
clear:left;
}
</style>

<div  style="padding-top:15px;padding-left:15px">
<div class="col-md-12">
            <?php
                            unset($search, $customers_array,$popdetails_array);
                            foreach (dbFetchColumn('SELECT `ossid` FROM `nxg_customer`') as $customer)
                                {
                                  $customers_array[$customer] = $customer ;
                                }
                                    krsort($customers_array);

                                $search[] = array('type'    => 'multiselect',
                                                  'name'    => 'OSS Client ID',
                                                   'id'      => 'ossid',
                                                  'width'   => '125px',
                                                  'value'   => $vars['ossid'],
                                                  'values'  => $customers_array);

                                $search[] = array('type'    => 'text',
                                                  'name'   => 'Customer',
                                                  'id'      => 'name',
                                                  'width'   => '150px',
                                                  'placeholder' => 'Customer Name',
                                                  'value'   => $vars['name']);
   
                                foreach (dbFetchRows('SELECT `id`, `pop_name` FROM `nxg_pop_details`') as $popdetails)
                                {
                                  $popid = $popdetails['id'] ;
                                  $popdetails_array[$popid] = $popdetails['pop_name'] ;
                                }

                                $search[] = array('type'    => 'multiselect',
                                                  'name'    => 'PoP',
                                                  'id'      => 'pop_id',
                                                  'width'   => '150px',
                                                  'value'   => $vars['pop_id'],
                                                  'values'  => $popdetails_array);
 
                    

                               print_search($search, 'Customer Filter', 'search', 'pop_mgr/view=customer_grid/');
?>
<span class="pull_right">
    <a href="/pop_mgr/view=add_customer/refresh=0/" class="btn" role="button">Add Customer</a> &nbsp;&nbsp;&nbsp;
    <a  id="traffic_engg"  class="btn" role="button">Traffic Engineering</a>
</span>

<?php
                                $vars[' agination'] = TRUE;
                                $page_title[] = 'CustomerGrid';
                        print_customer_filter_events($vars);
    
               function print_customer_filter_events($vars){
                            global $customers_array;
                            $events = get_customer_filter_events_array($vars);
                        if (!$events['count'])
                              {
                                print_warning('<h4>No Customer entries found!</h4>');
                              }else{
                                $string = '<table class="table table-bordered table-striped table-hover table-condensed-more">' . PHP_EOL;
                                    if (!$events['short'])
                                  {
                                    $string .= '  <thead>' . PHP_EOL;
                                    $string .= '    <tr>' . PHP_EOL;
                                    $string .= '      <th class="state-marker"></th>' . PHP_EOL;
                                    $string .= '      <th></th>' . PHP_EOL;
                                    $string .= '      <th>OSS Client ID</th>' . PHP_EOL;
                                    $string .= '      <th>Customer</th>' . PHP_EOL;
                                    $string .= '      <th>Connectivity</th>' . PHP_EOL;
                                    $string .= '      <th>NXG Net Prefix</th>' . PHP_EOL;
                                    $string .= '      <th>Filters</th>' . PHP_EOL;
                                    $string .= '    </tr>' . PHP_EOL;
                                    $string .= '  </thead>' . PHP_EOL;
                                  }
                                $string   .= '  </tbody>' . PHP_EOL;

                      foreach ($events['entries'] as $entry)
                              {
                                $remove_repetation = array();
                                $remove_n =array();
                                

                               $string .= '  <tr class="'.$entry['html_row_class'].'">' . PHP_EOL;
                               $string .= '<td class="state-marker"></td>' . PHP_EOL;
                               $string .= '<td><input class="customer_check" type="checkbox" value="'.$entry['customer_id'].'" ></td>' . PHP_EOL;
                               $string .= '    <td ><a href="/pop_mgr/view=modify_customer/customer_id='.$entry['customer_id'].'/refresh=0/">';
                               $string .= $entry['ossid'] . '</a></td>' . PHP_EOL ;

                               
                               $string .= '    <td >';
                               $string .= $entry['name'] . '</td>' . PHP_EOL ;
               
                               $pop_id =dbFetchRow('select id from nxg_pop_details where pop_name="'.$entry['pop_name'].'"');
                               $string .= '    <td >';
                               $customer_id= $entry['customer_id'];
                            
                                if(!empty($vars['pop_id']) && !is_array($vars['pop_id']))
                                {
                                    $multi_customer = dbFetchRows('select conn_id ,cust_id,pop_id,customer_conn from nxg_customer_conn where cust_id='.$customer_id.' and pop_id='.$pop_id['id'].'');
                                }else{
                                    $multi_customer = dbFetchRows('select conn_id ,cust_id,pop_id,customer_conn from nxg_customer_conn where cust_id='.$customer_id.'');
                                }
                                $str_pop="";
 
                                   foreach($multi_customer as $customers){
                                        $pop_data=dbFetchRow('select pop_name from nxg_pop_details where id='.$customers['pop_id']);
                                        $str_pop .=$pop_data['pop_name']." - ".$customers['customer_conn'] ."<br />";
                                    }
                        
                               $string .= $str_pop;
                               $string .= '<td>';
                               $string .= $entry['all_srv_prefix']. '</td>' . PHP_EOL ;
                               
                               $string .= '    <td ><a href="/pop_mgr/view=traffic_filter/refresh=0/customer='.$entry['name'].'" >';
                               $string .= $entry['filter_count'] . '</a></td>' . PHP_EOL ;
                            }
                               $string   .= '  </tbody>' . PHP_EOL;
                               $string   .= '  </table>' . PHP_EOL;
        
                    echo $string;
                    }
                }   
                    
                    function get_customer_filter_events_array($vars){
                          $array = array();
                          $array['short'] = (isset($vars['short']) && $vars['short']);
                          $array['pagination'] = (isset($vars['pagination']) && $vars['pagination']);
                          pagination($vars, 0, TRUE); // Get default pagesize/pageno
                          $array['pageno']   = $vars['pageno'];
                          $array['pagesize'] = $vars['pagesize'];
                          $start    = $array['pagesize'] * $array['pageno'] - $array['pagesize'];
                          $pagesize = $array['pagesize'];
                          $param = array();
                          $where = ' WHERE 1';
                            foreach ($vars as $var => $value)
                              {
                                if ($value != '')
                                 {
                                  switch ($var)
                                   {
                                    case 'ossid':
                                        $where .= generate_query_values($value, 'ossid');
                                          break;
                                    case 'name':
                                        $where .= generate_query_values($value, 'name');
                                          break;
                                    case 'pop_id':
                                        $where .= generate_query_values($value, 'nxg_pop_details.id');
                                          break;

                                    }
                                 }
                            }
                          $query = ',(count(distinct(nxg_customer_net_service.net_srv_prefix))-1) as all_srv_prefix,COUNT(distinct(nxg_traffic_filter.filter_name)) as filter_count from `nxg_customer` left join nxg_customer_net_service on nxg_customer_net_service.cust_id=nxg_customer.customer_id left join nxg_customer_conn on nxg_customer_conn.cust_id = nxg_customer.customer_id left join  nxg_traffic_filter on nxg_traffic_filter.customer=nxg_customer.name left join nxg_pop_details on nxg_pop_details.id=nxg_customer_conn.pop_id';
                          $query .= $where ;

                          $query = 'SELECT * '.$query;
                          $query .= ' group by customer_id ORDER BY `name` ASC ';

                             $array['entries'] = dbFetchRows($query, $param);
                             if ($array['pagination'] && !$array['short'])
                                {
                                    $array['count'] = dbFetchCell($query_count, $param);
                                    $array['pagination_html'] = pagination($vars, $array['count']);
                                } else {
                                    $array['count'] = count($array['entries']);
                                }
                
                          $array['updated'] = dbFetchCell($query_updated, $param);
                return $array;
            }
    ?>
<div id="popupModel" class="modal fade" role="dialog">
  <div class="modal-dialog" >

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Commit check log</h4>
      </div>
      <div class="modal-body" id="customer_grid_filter">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

  </div> <!-- col-md-12 -->

</div> <!-- row -->




<link rel="stylesheet" href="/nexusguard/css/pop_manager.css" >

<script type="text/javascript">

var rowCount = 1;
var serviceSlab =1;
var customRow =1;
var inboundRow =1;
var outboundRow =1;
function addMoreRows()
{
    rowCount =rowCount+1;
var recRow = '<tr id="rowCount'+rowCount+'"> <td class="pull_right" >Network Prefix 1</td><td class="pull_right"><input type="text" class="input" name="nxg_network_prefix[]"/></td><td><input type="text" class="subnet" name="nxg_network_prefix_subnet[]"/></td><td class="pull_right">Protocol</td><td class="pull_right"><select class="input" name="nxg_protocol[]"><option>TCP</option></select></td><td colspan="1" class="pull_right">Port</td><td><input type="text" class="inpu" name="nxg_port[]"/></td><td class="pull_left"><input type="button" id="rowCount'+rowCount+'" class="btn btn-mini btn-danger" onclick="removeSlabs('+rowCount+');return false;" value="Remove"/></td></tr>';
    $('#nxg_app_services > tbody > tr').eq(1+rowCount-3).after(recRow);



}
function removeSlabs(id)
{
    jQuery('#rowCount'+id).remove();
    rowCount--;
}


function addRowsToNet()
{
    serviceSlab =serviceSlab+1;
    var recRow = '<tr id="serviceSlab'+serviceSlab+'"><td  class="pull_right">Dest Network Prefix 1</td> <td class="pull_right"><input type="text" class="input" name="dest_network_prefix[]" value=""/></td><td ><input type="text" class="subnet" name="dest_network_prefix_subnet[]" value=""/></td><td><input type="button" id="serviceSlab'+serviceSlab+'" class="btn btn-mini btn-danger" onclick="removeServiceSlab('+serviceSlab+');return false;" value="Remove"/></td></tr>';
    $('#nxg_net_services > tbody > tr').eq(1+serviceSlab-3).after(recRow);

}

function removeServiceSlab(id)
{
    jQuery('#serviceSlab'+id).remove();
    serviceSlab--;
}


function addRowsToBlockServices(){

customRow = customRow+1;
var recRow = '<tr id="customRow'+customRow+'"><td class="pull_right">Custom</td><td  class="pull_right">Protocol</td> <td class="pull_right"><select class="input" name="protocol[]"><option>TCP</option></select></td><td class="pull_right">Port</td><td class="pull_left"><input type="text" class="input" name="port[]" value=""/></td><td><input  type="button" id="customRow'+customRow+'" class="btn btn-mini btn-danger" onclick="removeRowFromBlockServices('+customRow+');return false;" value="Remove"/></td></tr>';

if( serviceSlab === 1){

 $('#nxg_net_services > tbody > tr').eq(4+customRow-3).after(recRow);

}else{
$('#nxg_net_services > tbody > tr').eq(3+customRow+serviceSlab-3).after(recRow);
}
}
function removeRowFromBlockServices(id){
 jQuery('#customRow'+id).remove();
customRow--;
}





function addRowsToInbound()
{
//   outboundRow =outboundRow+1;
  inboundRow = inboundRow+1;
    var recRow = '<tr id="inboundRow'+inboundRow+'">  <td class="pull_right">PoP</td><td  class="pull_left"><select class="input_box" name="inbound_pop[]"><option>TCP</option></select></td><td class="pull_right">ISP&nbsp;</td><td class="pull_center" ><select class="input_box" name="inbound_isp[]"><option>TCP</option></select></td><td><input type="button" id="inboundRow'+inboundRow+'" class="btn btn-mini btn-danger" onclick="removeInboundService('+inboundRow+');return false;" value="Remove"/></input></td></tr>';
    $('#nxg_inbound_services > tbody > tr').eq(1+inboundRow-3).after(recRow);
addRowsToOutbound();

}

function addRowsToOutbound(){
outboundRow =outboundRow+1;
var recRow = '<tr id="outboundRow'+outboundRow+'">  <td class="pull_right">PoP</td><td  class="pull_left"><select class="input_box" name="inbound_pop[]"><option>TCP</option></select></td><td colspan="1" class="pull_right">ISP</td><td  ><select class="input_box" name="inbound_isp[]"><option>TCP</option></select></td><td><input type="button" id="outboundRow'+outboundRow+'" class="btn btn-mini btn-danger" onclick="removeInboundService('+outboundRow+');return false;" value="Remove"/></input></td></tr>';

 $('#outbound_table > tbody > tr').eq(1+outboundRow-2).after(recRow);

}


function removeInboundService(id){
 jQuery('#inboundRow'+id).remove();
inboundRow--;
removeOutboundService(id);
}
function removeOutboundService(id){

jQuery('#outboundRow'+id).remove();
outboundRow--;
}

 function getResult(form)
   {
   (function($) {

            var frm = $(document.myform);
            var dat = JSON.stringify(frm.serializeArray());
            var datastring = $("#isp_form").serialize();
            $.ajax({ url: 'nexusguard/views/customer/process_customer_details.php',
            data: datastring,
            type: 'post',
            success: function(output) {
                   $('#errormsg').html(output);
/*                   if(output.indexOf('Error')==-1)
                   {
                           window.location.href="pop_mgr/view=pop_template_grid/";
                   }*/
                     }
           });
       })(jQuery);
   }


</script>
<div id='errormsg'></div>
<form class='form_isp' id='isp_form' method='post'>
    <div class="row">
    <div class="col-md-8">

      <div class="widget widget-table">
        <div class="widget-header">
          <i class="oicon-gear"></i><h3>Modify a Customer</h3>
        </div>
            <div style="padding-top: 10px;" class="widget-content">
    <table class="form_table" >
    <tr>
        <td class="pull_right">OSS ID</td>
        <td class="pull_right"><input type="text" class="input" name="oss_id" value=""/></td>
    </tr>
    <tr>
        <td class="pull_right">Name</td>
        <td class="pull_right"><input type="text" class="input" name="customer_name" value=""/></td>
        <td class="pull_right">Email</td>
        <td class="pull_right"><input type="text" class="input" name="customer_email" value=""/></td>
    </tr>
    <tr>
        <td class="pull_right">Location</td>
        <td class="pull_right"><input type="text" class="input" name="customer_location" value=""/></td>
        <td class="pull_right">Phone</td>
        <td class="pull_right"><input type="text" class="input" name="customer_phone" value=""/></td>
    </tr>
    <tr>
        <td class="pull_right">Servicves</td>
        <td class="pull_right">NexusGuard App <input type="checkbox" class="checkbox" name="nexus_app" value=""/></td>
        <td class="pull_right">NexusGuard Net <input type="checkbox" class="checkbox"  name="nexus_net" value=""/></td>
        <td class="pull_right">Others <input type="checkbox" class="checkbox"  name="others" value=""/></td>
    </tr>
     <tr>
        <td class="pull_right">Description</td><td class="pull_right"><textarea class="textarea"type="text" name="description" value=""></textarea></td>
    </tr>
</table>
 </div> <!-- end of widget-content -->
      </div> <!-- End of widget-table -->
    </div> <!-- End of col-md-6 -->
  </div>  <!-- End of row -->

<div class="row">
    <div class="col-md-8">

      <div class="widget widget-table">
        <div class="widget-header">
          <i class="oicon-gear"></i><h3>Tunnel Connected</h3>
        </div>
            <div style="padding-top: 10px;" class="widget-content">

<table class="form_table" >
   <tr>
        <td class="pull_right">Local as</td>
        <td class="pull_right"><input type="text" class="input" name="tunnel_local" value=""/></td>
        <td class="pull_right">Peer as</td>
        <td class="pull_right"><input type="text" class="input" name="tunnel_peer" value=""/></td>
   </tr>
   <tr>
        <td class="pull_right">Tun End-Point IP Local</td>
        <td class="pull_right"><input type="text" class="input" name="endpoint_local_ip" value=""/></td>
        <td class="pull_right">Tun End-Point IP Remote</td>
        <td class="pull_right" ><input type="text" class="input"  name="endpoint_remote_ip" value=""/></td>
    </tr>
    <tr>
        <td class="pull_right">Local IP</td>
        <td class="pull_right"><input type="text"  class="input" name="tunnel_local_ip" value=""/></td>
        <td class="pull_right">Remote IP</td>
        <td class="pull_right"><input type="text" class="input" name="tunnel_remote_ip" value=""/></td>
    </tr>
</table>
 </div> <!-- end of widget-content -->
      </div> <!-- End of widget-table -->
    </div> <!-- End of col-md-6 -->
  </div>  <!-- End of row -->


<div class="row">
    <div class="col-md-8">

      <div class="widget widget-table">
        <div class="widget-header">
          <i class="oicon-gear"></i><h3>Direct Connect/IX/Private-Peer</h3>
        </div>
            <div style="padding-top: 10px;" class="widget-content">

<table class="form_table">
   <tr>
        <td  class="pull_right">VLAN-ID</td>
        <td class="pull_right"><input type="text"  class="input" name="vlan_id" value=""/></td>
   </tr>

   <tr>
        <td class="pull_right">Local IP</td>
        <td class="pull_right"><input type="text"  class="input" name="direct_connect_local_ip" value=""/></td>
        <td class="pull_right">Remote IP</td>
        <td class="pull_right"><input type="text"   class="input" name="direct_connect_remote_ip" value=""/></td>
   </tr>
    <tr>
        <td class="pull_right">Local as</td>
        <td class="pull_right"><input type="text"   class="input" name="direct_connect_local_as" value=""/></td>
        <td class="pull_right">Peer as</td>
        <td class="pull_right"><input type="text"  class="input" name="direct_connect_peer_as" value=""/></td>
    </tr>
    <tr>
        <td class="pull_right">Local IP</td>
        <td class="pull_right"><input type="text"  class="input" name="direct_connect_local_ip" value=""/></td>
        <td class="pull_right">Remote IP</td>
        <td class="pull_right"><input type="text"  class="input" name="direct_connect_remote_ip" value=""/></td>
   </tr>
</table>
</div> <!-- end of widget-content -->
      </div> <!-- End of widget-table -->
    </div> <!-- End of col-md-6 -->
  </div>  <!-- End of row -->

<div class="row">
    <div class="col-md-12">

      <div class="widget widget-table">
        <div class="widget-header">
          <i class="oicon-gear"></i><h3>NXG-Net Service</h3>
        </div>
            <div style="padding-top: 10px;" class="widget-content">


<table id="nxg_net_services" class="form_table" >
   <tr>
        <td  class="pull_right">Dest Network Prefix 1</td>
        <td class="pull_right"><input type="text" class="input" name="dest_network_prefix[]" value=""/></td><td ><input type="text" class="subnet" name="dest_network_prefix_subnet[]" value=""/></td>
   </tr>
    <tr>
        <td class="pull_right"><input type="button" name='add'  class='btn btn-mini btn-success' id="remove_nxg_app" onClick="addRowsToNet(); return false;" value="Add More"/></td>
    </tr>
     <tr>
        <td class="pull_right">Blocked Servicves</td>
        <td class="pull_right">ICMP <input type="checkbox" class="input" name="icmp" value=""/></td>
        <td class="pull_right">DNS<input type="checkbox" class="input"  name="dns" value=""/></td>
        <td class="pull_right">FTP <input type="checkbox" class="input"  name="ftp" value=""/></td>
        <td class="pull_right">SSH <input type="checkbox" class="input"  name="ssh" value=""/></td>
    </tr>
    <tr>
        <td class="pull_right">Custom</td>
         <td  class="pull_right">Protocol</td>
         <td class="pull_left">
            <select class="input" name="protocol[]">
                    <option>TCP</option>
            </select>
        </td>
        <td class="pull_right">Port</td><td class="pull_left"><input type="text" class="input" name="port[]" value=""/></td>
    </tr>
    <tr>
        <td class="pull_right"><input type="button" class='btn btn-mini btn-success' value="Add More" id="add_nxg_block" onClick="addRowsToBlockServices(); return false;"/></td>
    </tr>
</table>
</div> <!-- end of widget-content -->
      </div> <!-- End of widget-table -->
    </div> <!-- End of col-md-6 -->
  </div>  <!-- End of row -->

<div class="row">
    <div class="col-md-12">

      <div class="widget widget-table">
        <div class="widget-header">
          <i class="oicon-gear"></i><h3>NXG-App Service</h3>
        </div>
            <div style="padding-top: 10px;" class="widget-content">
<table id="nxg_app_services"  class="form_table">
    <tr>
        <td class="pull_right" >Network Prefix 1</td>
        <td class="pull_right"><input type="text" class="input" name="nxg_network_prefix[]"/></td><td><input type="text" class="subnet" name="nxg_network_prefix_subnet[]"/></td>
         <td class="pull_right">Protocol</td>
         <td class="pull_right">
            <select class="input" name="nxg_protocol[]">
                    <option>TCP</option>
            </select>
        </td>
        <td class="pull_right">Port</td><td><input type="text" class="inpu" name="nxg_port[]"/></td>

    </tr>
     <tr>
        <td class="pull_right"><input type="button" class='btn btn-mini btn-success' id="remove_nxg_app" onClick="addMoreRows(); return false;" value="Add More"/></td>
    </tr>

</table>
</div> <!-- end of widget-content -->
      </div> <!-- End of widget-table -->
    </div> <!-- End of col-md-6 -->
  </div>  <!-- End of row -->

<div class="row">
    <div class="col-md-12">

      <div class="widget widget-table">
        <div class="widget-header">
          <i class="oicon-gear"></i><h3>Inbound (Ingress) Traffic</h3>
        </div>
            <div style="padding-top: 10px;" class="widget-content">

<table id="nxg_inbound_services"  class="form_table">

<tr>
        <td  class="pull_right">PoP</td>
         <td class="pull_right">
            <select class="input" name="inbound_pop[]">
                    <option>TCP</option>
            </select>
        </td>
<td class="pull_right">ISP</td>
         <td colspan="1" class="pull_center" >
            <select class="input_box" name="inbound_isp[]">
                    <option>TCP</option>
            </select>
        </td>

</tr>
    <tr>
        <td colspan="1" class="pull_right"><input type="button"class='btn btn-mini btn-success' id="inbound" onClick="addRowsToInbound(); return false;" value="Add More"/></td>
    </tr>
</table>
</div> <!-- end of widget-content -->
      </div> <!-- End of widget-table -->
    </div> <!-- End of col-md-6 -->
  </div>  <!-- End of row -->

<div class="row">
    <div class="col-md-12">

      <div class="widget widget-table">
        <div class="widget-header">
          <i class="oicon-gear"></i><h3>Outbound (Egress) Traffic</h3>
        </div>
            <div style="padding-top: 10px;" class="widget-content">

<table id="outbound_table" class="form_table">

<tr>
        <td class="pull_right">PoP</td>
         <td  class="pull_right">
            <select class="" name="outbound_protocol[]">
                    <option>TCP</option>
            </select>
        </td>
 <td colspan="1" class="pull_right"><input type="radio" name="radio" >&nbsp;Best Path Routing</input></td>
 <td colspan="1" class="pull_right"><input type="radio" name="radio" >&nbsp;Provider</input></td>
<td colspan="1" class="pull_right">ISP</td>
         <td colspan="1" class="pull_left">
            <select class="input_box" name="outbound_isp[]">
                    <option>TCP</option>
            </select>
        </td>
</tr>
<tr>
        <td  class="pull_right">PoP</td>
         <td  class="pull_right">
            <select class="" name="outbound_pop">
                    <option>TCP</option>
            </select>
        </td>
 <td class="pull_right"><input type="radio" name="radio_button" >&nbsp;Best Path Routing</input></td>
 <td  class="pull_right"><input type="radio"  name="radio_button">&nbsp;Provider</input></td>
<td class="pull_right">ISP</td>
         <td class="pull_left" name="outbound_isp">
            <select class="input_box">
                    <option>TCP</option>
            </select>
        </td>
</tr>
<tr>
  <td class="pull_right"><button type="submit" class="btn btn-default" onclick="getResult(this.form);return false;">Commit Check</button></td>
 <td class="pull_left"><button type="submit" class="btn btn-default">Cancel</button></td>
</tr>


</table>

</form>


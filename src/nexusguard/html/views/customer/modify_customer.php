<?php include '/opt/observium/nexusguard/db/db_customer_functions.php' ;?>
<?php include '/opt/observium/html/nexusguard/api/common/common.inc.php' ;?>

<?php
 $cust_id = $vars['customer_id'];
 $cust_details = get_db_cust_details($cust_id);
?>




<link rel="stylesheet" href="/nexusguard/css/pop_manager.css" >
<link rel="stylesheet" href="/nexusguard/css/bootstrap-panel.css" >
<script src="/nexusguard/js/common.js"></script> 

<script type="text/javascript">

var appRowCount = 1;
var serviceSlab =1;
var blockSrvRow =1;
var inboundRow =1;
var outboundRow =1;
var connrow = 0;
var ignore_conn_type_change = 0;
var cust_conn_data=[];
var net_srv_prefix_changed =0;
var button_array = ["cancel","commit_check","delete"];
var db_form = {};
var net_srv_form = {};
var net_srv_prefix_changed =0;


function addRowsToBlockServices(){

blockSrvRow = blockSrvRow+1;
//var recRow = '<tr id="blockSrvRow'+customRow+'"><td class="pull_right">Custom</td><td  class="pull_right">Protocol</td> <td class="pull_right"><select class="input" name="protocol[]"><option>TCP</option></select></td><td class="pull_right">Port</td><td class="pull_left"><input type="text" class="input" name="port[]" value=""/></td><td><input  type="button" id="customRow'+customRow+'" class="btn btn-mini btn-danger" onclick="removeRowFromBlockServices('+customRow+');return false;" value="Remove"/></td></tr>';


var recRow = '<tr  id="blockSrvRow'+blockSrvRow+'" > \
         <td  class="pull_right">Protocol</td>                  \
         <td class="pull_left" colspan="2">                     \
            <select size="1" name="blocked_srv[]" id="blockSrvSelect'+blockSrvRow+'" onChange="blockedSrvChange('+blockSrvRow+');return false;">              \
                </option><option value="DNS">DNS-UDP (53/udp)   \
                </option><option value="DNS">DNS-TCP (53/tcp)   \
                </option><option value="FTP">FTP (21/tcp)       \
                </option><option value="FTP">FTP-Data (20/tcp)  \
                </option><option value="HTTP">HTTP (80/tcp)     \
                </option><option value="HTTPS">HTTPS (443/tcp)  \
                </option><option value="ICMP-ANY">ICMP-ANY      \
                </option><option value="POP3">POP3 (110/tcp)    \
                </option><option value="SMTP">SMTP (25/tcp)     \
                </option><option value="SSH">SSH (22/tcp)       \
                </option><option value="TELNET">TELNET (23/tcp) \
                </option><option value="CUSTOM">Custom          \
                </option>                                       \
            </select>                                           \
                                                                \
        <span id="cutom_blocked_span'+blockSrvRow+'" style="display:none;">    \
                                                                \
        &nbsp;&nbsp;&nbsp;Name&nbsp;<input type="text"  class="input" name="custom_blocked_name[]" value=""/>   \
        &nbsp;&nbsp;&nbsp;Protocol&nbsp;                                                                        \
                <select name="custom_blccked_protocol[]" class="small">                                                     \
                    <option value="TCP">TCP</option>                                                            \
                    <option value="UDP">UDP</option>                                                            \
                </select>                                                                                       \
        &nbsp;&nbsp;&nbsp;Port&nbsp;<input type="text" class="port" name="custom_blocked_port[]" value=""/>     \
                                                                                                                \
                                                                                                                \
        </span>                                                                                                 \
        &nbsp;&nbsp;&nbsp;<input  type="button" id="blockSrvRow'+blockSrvRow+'" class="btn btn-mini btn-danger" onclick="removeRowFromBlockServices('+blockSrvRow+');return false;" value="Remove"/></td>               \
        </tr>';




    
if( serviceSlab === 1){
    $('#nxg_net_services > tbody > tr').eq(4+blockSrvRow-3).after(recRow);
}else{
    $('#nxg_net_services > tbody > tr').eq(3+blockSrvRow+serviceSlab-3).after(recRow);
}
    $("#add_nxg_block").val("Add More");
}

function removeRowFromBlockServices(id){
    jQuery('#blockSrvRow'+id).remove();
    blockSrvRow--;
    if(blockSrvRow == 1)
    { 
         $("#add_nxg_block").val("Add");  
    }
}

function blockedSrvChange(id)
{
     if( $("#blockSrvSelect"+id).val() == "CUSTOM")
         $("#cutom_blocked_span"+id).show();
     else
         $("#cutom_blocked_span"+id).hide();
}

</script>

<script>
$(document).ready(function () {

    function is_valid_connection()
    {
        bempty = false;
        $("#cust_conn_form input[type=text]").each(function( index ) {
            
            if(false == $(this).prop('disabled')) // ignore disabled inputs
            {
                if($(this).prop('name') != "conn_if_vlan")// ignore vlan as it optional 
                {
                    input_val = $( this ).val(); 
                    if("" == input_val.trim())
                        bempty =true;
                }
            }

        });

        if(bempty)
        {
            alert("All inputs are mandatory (except vlan and disabled fields). Please enter valid data.");
            return false;
        }


        conn_row = $("#conn_row").val();
        {
            rows = $('tr', "#conn_table");
            pop_selected = $("#pop_id").val();
            pop_used = false;
            for(i=1;i<rows.length;i++)
            {
                if(conn_row == i)
                    continue;
                row = rows.eq(i);
                pop_id_row = $(row).find(":nth-child(2)").text();
                conn_id_row = $(row).find(":nth-child(15)").text();
                action_row = $(row).find(":nth-child(16)").text();
                if(pop_selected == pop_id_row && action_row != "no-action") 
                    pop_used = true;
            }   

            if(pop_used == true)
            {
                alert("Cannot create/modify more than one new connection to same pop in one commit. Please commit current data first.");
                return false;
            }   
        }


        return true;
    }

    function reset_conn_form()
    {
       $("#cust_conn_form input[type=text]").val("");
       $("#cust_conn_form input[type=hidden]").val("");
       $("input[name='customer_conn'][value='tunnel']").prop('checked', true);

        $("#pop_id").attr("disabled",false);
        $("input[name='customer_conn'][value='tunnel']").attr("disabled",false);
        $("input[name='customer_conn'][value='direct']").attr("disabled",false);
        $("#connnection_type").attr("disabled",false);
        $("#interface").attr("disabled",false);
        $("#unit").attr("disabled",false);

    }
    

    $("#add_conn").click(function(){
            if(net_srv_prefix_changed ==1)
            {
                alert("Dest Network Prefix has changed, Please apply these changes first.");
                return;
            }
            else
                $("#net_srv_prefix").attr("disabled",true);

             reset_conn_form();   
             $("#del_conn").hide();
             $("#myModal").modal('show');
             $("#pop_id").trigger("change");
    });

    $("input[name='modify_conn']").click(function (){

            if(net_srv_prefix_changed ==1)
            {
                alert("Dest Network Prefix has changed, Please apply these changes first.");
                return;
            }
            else
                $("#net_srv_prefix").attr("disabled",true);


                    clicktr = $(this).parent().parent();

                    copy_conn_data_from_table_to_form(clicktr);

                    $("#del_conn").show();
                    $("#myModal").modal('show');

                });



    $("#del_conn").click(function(){
        conn_id = $("#conn_id").val();
        selected_row_id = $("#conn_row").val();
        if(conn_id == "")
        {
            $("#conn_table tr:eq("+selected_row_id+")").remove();
        }
        else
        {
            $("#conn_table tr:eq("+selected_row_id+") td:first-child").html("<span style='color:red'>deleted</span>");
            $("#conn_table tr:eq("+selected_row_id+") td:last-child").html("delete");
        }
        $("#myModal").modal('hide');
   });

    $("#save_conn").click(function(){

       if(!is_valid_connection())
            return;

        newrow = $('<tr><td> <input type="button" class="btn btn-mini btn-success"  value="Modify" id="modify_conn" name="modify_conn" /></td></tr>');
        newrow.append('<td style="display:none;" >'+$("#pop_id").val()+'</td>'); 
        newrow.append('<td>'+$("#pop_id :selected").text()+'</td>'); 
        newrow.append('<td>'+$("#location").val()+'</td>'); 
        newrow.append('<td>'+$("input[name='customer_conn']:checked").val()+'</td>'); 
        newrow.append('<td>'+$("#interface").val()+'</td>'); 
        newrow.append('<td>'+$("#unit").val()+'</td>'); 
        newrow.append('<td>'+$("#vlan").val()+'</td>'); 
        newrow.append('<td>'+$("#endpoint_ip_local").val()+'</td>'); 
        newrow.append('<td>'+$("#endpoint_ip_remote").val()+'</td>'); 
        newrow.append('<td>'+$("#inside_ip_local").val()+'</td>'); 
        newrow.append('<td>'+$("#inside_ip_remote").val()+'</td>'); 
        newrow.append('<td>'+$("#asn_local").val()+'</td>'); 
        newrow.append('<td>'+$("#asn_remote").val()+'</td>'); 
        newrow.append('<td style="display:none;">'+$("#conn_id").val()+'</td>'); 

        selected_row_id = $("#conn_row").val();

        if( selected_row_id == "")
        {
            newrow.append('<td style="display:none;">add</td>');
            $("#conn_table").append(newrow);
        }
        else
           {
                newrow.append('<td style="display:none;">modify</td>');
                $("#conn_table tr:eq("+selected_row_id+")").replaceWith(newrow);
           }

        $("#myModal").modal('hide');
        reset_conn_form();

        $("input[name='modify_conn']").unbind( "click" );

             $("input[name='modify_conn']").click(function (){

                    clicktr = $(this).parent().parent();
                
                    copy_conn_data_from_table_to_form(clicktr);

                    $("#del_conn").show();
                    $("#myModal").modal('show');

                });

    });

    function copy_conn_data_from_table_to_form(clicktr)
    {
        $("#conn_row").val($(clicktr).index()+1);
        $("#pop_id").val($(clicktr).find(":nth-child(2)").text());
        $("#location").val($(clicktr).find(":nth-child(4)").text());
        $("input[name='customer_conn'][value='" + $(clicktr).find(":nth-child(5)").text()  + "']").prop('checked', true);
        $("#interface").val($(clicktr).find(":nth-child(6)").text());
        $("#unit").val($(clicktr).find(":nth-child(7)").text());
        $("#vlan").val($(clicktr).find(":nth-child(8)").text());
        $("#endpoint_ip_local").val($(clicktr).find(":nth-child(9)").text());
        $("#endpoint_ip_remote").val($(clicktr).find(":nth-child(10)").text());
        $("#inside_ip_local").val($(clicktr).find(":nth-child(11)").text());
        $("#inside_ip_remote").val($(clicktr).find(":nth-child(12)").text());
        $("#asn_local").val($(clicktr).find(":nth-child(13)").text());
        $("#asn_remote").val($(clicktr).find(":nth-child(14)").text());
        $("#conn_id").val($(clicktr).find(":nth-child(15)").text());
        $("#conn_action").val($(clicktr).find(":nth-child(16)").text());

        // modify screen case hence disable the some of the input fields
        if($("#conn_id").val() !="")
        {
            $("#pop_id").attr("disabled",true);
            $("input[name='customer_conn'][value='tunnel']").attr("disabled",true);
            $("input[name='customer_conn'][value='direct']").attr("disabled",true);
            $("#connnection_type").attr("disabled",true);
            $("#interface").attr("disabled",true);
            $("#unit").attr("disabled",true);
        }

        
    }

    $("#pop_id").change(function() {

         $("input[name='customer_conn'][value='tunnel']").prop('checked', true);
         $("#interface").val("loading....");
         $("#unit").val("loading...");
         $("#vlan").val("loading...");
         $("#endpoint_ip_local").val("loading...");
         $("#inside_ip_local").val("loading...");
         $("#nside_ip_remote").val("loading...");

         get_port_table_data();
        
        $.ajax({ url: "nexusguard/api/get_customer_conn_data.php",
            data: '{ "pop_id": "'+$("#pop_id").val()+'" }',
            type: 'post',
            success: function(output) {

                        outputjson = JSON.parse(output);
                        if(outputjson.error_code)
                        {
                            alert(outputjson.message);
                        }else
                        {
                            cust_conn_data=outputjson.output;

                            if($("input[name='customer_conn']:checked").val() =="tunnel")
                            {
                            $("#interface").val(cust_conn_data.tunnel_if);
                            $("#unit").val(cust_conn_data.tunnel_unit);
                            $("#endpoint_ip_local").val(cust_conn_data.pop_public_ip);
                            $("#inside_ip_local").val(cust_conn_data.tunnel_ip_local);
                            $("#inside_ip_remote").val(cust_conn_data.tunnel_ip_remote);
                            }else
                            {
                                $("#interface").val(cust_conn_data.direct_if);
                                $("#unit").val(cust_conn_data.direct_unit);
                                $("#vlan").val("");
                                $("#inside_ip_local").val(cust_conn_data.direct_ip_local);
                                $("#inside_ip_remote").val(cust_conn_data.direct_ip_remote);
                            }

                            $("input[name='customer_conn']:radio").trigger("change");
                        }
            },
            error:function(data){
                alert("Error processing the request.");
            }
    });

    });

    $("input[name='customer_conn']:radio").change(function () {
     
        if($("#conn_row").val() == "") // add case
        { 
        if($("input[name='customer_conn']:checked").val() =="tunnel")
        {
            $("#interface").val(cust_conn_data.tunnel_if);
            $("#unit").val(cust_conn_data.tunnel_unit);
            $("#endpoint_ip_local").val(cust_conn_data.pop_public_ip);
            $("#inside_ip_local").val(cust_conn_data.tunnel_ip_local);
            $("#inside_ip_remote").val(cust_conn_data.tunnel_ip_remote);
        }else
            {
                $("#interface").val(cust_conn_data.direct_if);
                $("#unit").val(cust_conn_data.direct_unit);
                $("#vlan").val("");
                $("#inside_ip_local").val(cust_conn_data.direct_ip_local);
                $("#inside_ip_remote").val(cust_conn_data.direct_ip_remote);
            }
        }
        
        if($("input[name='customer_conn']:checked").val() =="tunnel")
        {
                 $("#vlan").prop('disabled', true);
                 $("#vlan").val("");
                 $("#endpoint_ip_local").prop('disabled',  false);
                 $("#endpoint_ip_remote").prop('disabled',  false);;
        }
        else
            {
                 $("#vlan").prop('disabled',  false);
                 $("#endpoint_ip_local").val("");
                 $("#endpoint_ip_local").prop('disabled',  true);

                 $("#endpoint_ip_remote").val("");
                 $("#endpoint_ip_remote").prop('disabled',true);
            }
    
    });

    $("#net_srv_prefix").change(function() {
                net_srv_prefix_changed=1;
    });


    $( "#commit_check" ).click(function() {

        rows = $('tr', "#conn_table");

        var request_array= [];

        j = 0
        if(Object.keys(db_form).length>0)
        {
                db_form.cust_id =  <?php echo  $vars['customer_id']; ?>;
                req = [];
                req["url"] = "/nexusguard/api/modify_customer_db.php" ;
                req["data"] = db_form ;
                req["entity_name"] =  "Database Changes" ;
                request_array[j]=req;
                j++;
        }

       if(net_srv_prefix_changed ==1)
        {
            for(i=1;i<rows.length;i++)
            {
                row = rows.eq(i);
                copy_conn_data_from_table_to_form(row);
                conn_data =  parse_form("cust_conn_form");

                net_srv_form = {};
                net_srv_form.action = "commit_check";

                net_srv_form.cust_id =  <?php echo  $vars['customer_id']; ?>;
                net_srv_form.net_srv_prefix = $("#net_srv_prefix").val();
                net_srv_form.pop_id = $("#pop_id").val();
                req = [];
                req["url"] = "/nexusguard/api/modify_customer_net_prefix.php" ;
                req["apply_url"] = "/nexusguard/api/modify_customer_net_prefix.php" ;
                req["data"] =  net_srv_form;
                req["entity_name"] =  "NXG-Net Service - " +  $("#pop_id option:selected").text();
                request_array[j]=req;
                j++;
            }
        }
        
        for(i=1;i<rows.length;i++)
        {
            form_data = {};
            form_data.action ="commit_check";
            form_data.cust_id = <?php echo  $vars['customer_id']; ?>;
            form_data.customer_name = $("#customer_name").val();
            form_data.ip_interface_mgmt_update_time = $("#ip_interface_mgmt_update_time").val();

            row = rows.eq(i);
            
            copy_conn_data_from_table_to_form(row);
            conn_data =  parse_form("cust_conn_form");
            if(conn_data.conn_action != "no-action")
            {
                form_data.connection = conn_data;

                req = [];
                req["url"] = "/nexusguard/api/"+form_data.connection.conn_action+"_customer.php" ;
                req["apply_url"] = "/nexusguard/api/"+form_data.connection.conn_action+"_customer.php" ;
                
                req["data"] = form_data ;
                req["entity_name"] =  $("#pop_id option:selected").text() ;
                request_array[j]=req;
                j++;
            }
        }


     for(k=1;k<rows.length;k++)
        {
            rowk = rows.eq(k);
            actk = rowk.find(":nth-child(16)").text();
            popk = rowk.find(":nth-child(2)").text();

            in_req = 0;
            if(actk != "no-action")
            {
                 for(l=1;l<rows.length;l++)
                 {
                    rowl = rows.eq(l);
                    actl = rowl.find(":nth-child(16)").text();
                    popl = rowl.find(":nth-child(2)").text();

                    if(actl != "no-action" && popk == popl)
                    {
                        in_req++;
                    }
                }
            }

            if(in_req > 1)
            {
                alert("Cannot perform more than one request on a single pop in single commit.");
                return;    
            }
            
        }

        
        if(request_array.length> 0)
            commit_check2(request_array,button_array);
        else
            alert("No information changed so not taking any action");

    });

    $( "#delete" ).click(function() {

            rows = $('tr', "#conn_table");

            if(rows.length > 1)
            {
               alert("Customer delete not allowed as there are existing PoP connections. Delete all PoP connections from the device before deleteing customer.");
               return; 
            }
            if(confirm("Confirm Delete Customer?") != true){
                    return;
            }

            form_data = {};
            form_data.action ="db";
            form_data.cust_id = <?php echo  $vars['customer_id']; ?>;

            req = [];
            req["url"] = "/nexusguard/api/delete_customer.php" ;
            req["data"] = form_data ;
            req["entity_name"] =  "Deleting Customer";

            request_array=[];
            request_array[0]=req;

            commit_check2(request_array,button_array);
   
    });


    $("input.changable").change(function () {
        db_form[$(this).attr("name")] = $(this).val(); 
    });

    $("textarea.changable").change(function () {
        db_form[$(this).attr("name")] = $(this).val();
    });


    $("#select_port").click(function (){

        $("#portsModal").modal('show');

    });


    function get_port_table_data()
    {

        $("#port_table").html('<img src="nexusguard/img/ajax-loader.gif">loading...');

        $.ajax({ url: "nexusguard/api/get_port_table.php?pop_id="+$("#pop_id").val(),
            data: '',
            type: 'post',
            success: function(output) {
                $("#port_table").html(output);
            },
            error:function(data){
                alert("Error in processing request.");
            }
        });

    }


});
</script>


<form id='cust_form' method='post'>

 <input type="hidden" id="ip_interface_mgmt_update_time" name="ip_interface_mgmt_update_time" value="<?php echo get_ip_interface_mgmt_time(); ?>" />

    <h3 class="form_heading">Modify Customer</h3>
    <div class="row">
    <div class="col-md-12">

      <div class="widget widget-table">
        <div class="widget-header">
          <i class="oicon-gear"></i><h3>General</h3>
        </div>
            <div style="padding-top: 10px;" class="widget-content"> 
    <table class="form_table" >
    <tr>
        <td class="pull_right">OSS Client ID *</td>
        <td class="pull_left"><input type="text" class="input" name="oss_id" disabled value="<?php echo $cust_details['ossid']; ?>"/></td>
        <td class="pull_right">Name *</td>
        <td class="pull_left"><input type="text" class="input" id="customer_name" name="customer_name" disabled value="<?php echo $cust_details['name']; ?>"/></td>
        <td class="pull_right">Notes</td>
        <td rowspan="3" class="pull_left" valign="top" ><textarea rows="8" cols="2000" class="notes_textarea changable" type="text" name="notes"><?php echo $cust_details['notes']; ?></textarea></td>
    </tr>
    <tr>
        <td class="pull_right">Email</td>
        <td class="pull_right"><input type="text" class="input changable" name="customer_email" value="<?php echo $cust_details['email']; ?>"/></td>
        <td class="pull_right">Phone</td>
        <td class="pull_right"><input type="text" class="input changable" name="customer_phone" value="<?php echo $cust_details['phone']; ?>"/></td>
    </tr>
<!--
    <tr>
        <td class="pull_right">Servicves</td>
        <td class="pull_right">NexusGuard App <input type="checkbox" class="checkbox" name="nexus_app" value=""/></td>
        <td class="pull_right">NexusGuard Net <input type="checkbox" class="checkbox"  name="nexus_net" value=""/></td>
        <td class="pull_right">Others <input type="checkbox" class="checkbox"  name="others" value=""/></td>
    </tr>
-->
     <tr>
        <td class="pull_right">Description</td><td colspan="3" class="pull_left"><textarea class="textarea changable"type="text" name="description" ><?php echo $cust_details['description']; ?></textarea>
        </td>
    </tr>
</table>
        
 </div> <!-- end of widget-content -->
      </div> <!-- End of widget-table -->
    </div> <!-- End of col-md-6 -->
  </div>  <!-- End of row --> 

<div class="row">
    <div class="col-md-12">

      <div class="widget widget-table">
        <div class="widget-header">
          <i class="oicon-gear"></i><h3>Customer Connection</h3>
        </div>
            <div style="padding-top: 10px;" class="widget-content">

    <table>
       <tr><td>&nbsp;&nbsp; <input type="button" class='btn btn-mini btn-success'  value="Add" id="add_conn" /></td></tr>
    </table>
    <table id="conn_table" class="table table-hover table-striped table-bordered table-condensed table-rounded">
        <thead>
        <tr>
                <th></th>
                <th>PoP</th>
                <th>Location</th>
                <th>Connection</th>
                <th>Interface</th>
                <th>Unit</th>
                <th>VLAN</th>
                <th>Tunnel Endpoint IP Local</th>
                <th>Tunnel Endpoint IP Remote</th>
                <th>Inside IP Local</th>
                <th>Inside IP Remote</th>
                <th>ASN Local</th>
                <th>ASN Remote</th>
        </tr>
        </thead>

        <?php
            foreach (dbFetchRows('select nxg_customer_conn.*,pop_name from nxg_customer_conn,nxg_pop_details where nxg_customer_conn.pop_id=nxg_pop_details.id and nxg_customer_conn.cust_id='.$cust_id) as $data)
            {
                echo('<tr>');
                echo('<td> <input type="button" class="btn btn-mini btn-success"  value="Modify" id="modify_conn" name="modify_conn" /></td>');
                echo('<td style="display:none;" >'.$data["pop_id"].'</td>');
                echo('<td>'.$data["pop_name"].'</td>');
                echo('<td>'.$data["conn_loc"].'</td>');
                echo('<td>'.$data["customer_conn"].'</td>');
                echo('<td>'.$data["conn_if"].'</td>');
                echo('<td>'.$data["conn_if_unit"].'</td>');
                echo('<td>'.$data["conn_if_vlan"].'</td>');
                echo('<td>'.$data["endpoint_ip_local"].'</td>');
                echo('<td>'.$data["endpoint_ip_remote"].'</td>');
                echo('<td>'.$data["inside_ip_local"].'</td>');
                echo('<td>'.$data["inside_ip_remote"].'</td>');
                echo('<td>'.$data["asn_local"].'</td>');
                echo('<td>'.$data["asn_remote"].'</td>');
                echo('<td style="display:none;" >'.$data["conn_id"].'</td>');
                echo('<td style="display:none;" >no-action</td>');
                echo('</tr>');
            }
        ?>

        <tbody>
        </tbody>
</table>
    <p>&nbsp;</p>

</div> <!-- end of widget-content -->
      </div> <!-- End of widget-table -->
    </div> <!-- End of col-md-6 -->
  </div>  <!-- End of row -->

<div class="row">
    <div class="col-md-12">

      <div class="widget widget-table">
        <div class="widget-header">
          <i class="oicon-gear"></i><h3>NXG-Net Service</h3>
        </div>
            <div style="padding-top: 10px;" class="widget-content">


<table id="nxg_net_services" class="form_table" >
   <tr>
        <td  class="pull_right">Dest Network Prefix *</td>
        <td class="pull_right">
                <textarea class="textarea" type="text" id="net_srv_prefix" name="net_srv_prefix" value="" ><?php 
                            foreach (dbFetchRows('select * from nxg_customer_net_service where net_srv_prefix != \'ALL-PREFIXES\' and cust_id='.$cust_id) as $data) 
                            {
echo($data["net_srv_prefix"]."\n");
                            }        
                ?></textarea>
        </td>
   </tr>
     

<!--
    <tr>
        <td class="">Blocked Servicves</td>
    </tr>
    <tr></tr>
    <tr>
        <td class="pull_right"><input type="button" class='btn btn-mini btn-success'  value="Add" id="add_nxg_block" onClick="addRowsToBlockServices(); return false;"/></td>
    </tr>
-->

</table>
</div> <!-- end of widget-content -->
      </div> <!-- End of widget-table -->
    </div> <!-- End of col-md-6 -->
  </div>  <!-- End of row -->

  <input type="hidden" name="action" value="commit_check"/>

</form>

<?php include 'nexusguard/views/includes/commit_check_footer2.php'; ?>


<div class="form-actions">
             <td class="pull_right">
                    <input type="button" class="btn btn-primary" id="cancel" name="cancel" value="Cancel" onclick="location.href='pop_mgr/view=customer_grid/'"/ >
            </td>
            <td class="pull_right">
                <button type='button' data-target="#myModal" class='btn btn-primary' name='commit_button' id = "commit_check" >Modify</button>
                <button type='button' data-target="#myModal" class='btn btn-primary' name='delete_button' id = "delete" >Delete</button>
            </td>
</div>



<div id="myModal" class="modal fade" role="dialog" style="background-color: transparent;width: auto; margin-left: 0px; top: 0px;bottom: 0px; left: 0px; right: 0px; display:none; border-width: 0px;"  >
  <div class="modal-dialog modal-lg" style="position: relative; margin: 30px auto; width: 800px; background-color: white;" id="pop_modal">
    <div class="modal-content">

      <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Customer connection</h4>
      </div><!-- end of modal-header -->
    
     <div class="modal-body">
<form id="cust_conn_form" >
    <table class="form_table"  border="0" >
   <tr>
        <td class="pull_right">PoP *</td>
        <td class="pull_left">
                    <select class="input" id="pop_id" name="pop_id">
                                    <?php

                                        foreach (dbFetchRows('select id, pop_name from nxg_pop_details order by pop_name') as $data)
                                            {
                                                echo('        <option value="'.$data['id'].'" >'.$data['pop_name'].'</option>');
                                            }
                                    ?>
                    </select>
        </td>
        <td class="pull_right">Location *</td>
        <td class="pull_left"><input type="text" class="input" id="location" name="conn_loc" value=""/></td>
   </tr>
    <tr>
         <td class="pull_right">Conn Type</td> <td class="pull_left"><input type="radio" checked="checked" value="tunnel"  id="connnection_type" name="customer_conn">Tunnel</input>&nbsp;&nbsp;&nbsp;<input type="radio" value="direct" id="connnection_type"  name="customer_conn">&nbsp;Direct</input></td>

        <td class="pull_right">Interface *</td><td class="pull_left"><input type="text" class="input" id="interface" name="conn_if" value=""/>
             <br>  <input type="button" class='btn btn-mini btn-success'  value="Show Ports" id="select_port" name="select_port" />
        </td>
    </tr>
    <tr>
             <td colspan="2"></td>
             <td class="pull_right">Unit *</td><td class="pull_left"><input type="text" class="input small" id="unit" name="conn_if_unit" value=""/>
         &nbsp;&nbsp;VLAN&nbsp;<input type="text" class="input small" id="vlan" name="conn_if_vlan" value=""/></td>
    </tr>

   <tr>
        <td class="pull_right">Tunnel EndPoint IP Local *</td>
        <td class="pull_left"><input type="text" class="input" id="endpoint_ip_local" name="endpoint_ip_local" value=""/></td>
        <td class="pull_right">Tunnel EndPoint IP Remote *</td>
        <td class="pull_left" ><input type="text" class="input" id="endpoint_ip_remote"  name="endpoint_ip_remote" value=""/></td>
    </tr>
    <tr>
        <td class="pull_right">Inside IP Local *</td>
        <td class="pull_left"><input type="text"  class="input" id="inside_ip_local" name="inside_ip_local" value=""/></td>
        <td class="pull_right">Inside IP Remote *</td>
        <td class="pull_left"><input type="text" class="input" id="inside_ip_remote" name="inside_ip_remote" value=""/></td>
    </tr>
    <tr>
        <td class="pull_right">ASN Local *</td>
        <td class="pull_left"><input type="text" class="input" id="asn_local" name="asn_local" value=""/></td>
        <td class="pull_right">ASN Peer *</td>
        <td class="pull_left"><input type="text" class="input" id="asn_remote" name="asn_remote" value=""/></td>
    </tr>
    <tr>
        <td class="pull_right"></td><td colspan="3" class="pull_left">
                <input type="hidden"  name="conn_row" value="" id="conn_row" />
                <input type="hidden"  name="conn_id" value="" id="conn_id" />
                <input type="hidden"  name="conn_action" value="" id="conn_action" />
        </td>
    </tr>
    </table>
</form>

      </div> <!-- modal body-->
      <div class="modal-footer">
                <input type="button" class='btn btn-mini btn-success'  value="Save to Grid" id="save_conn" />
                <input type="button" class='btn btn-mini btn-success'  value="Delete from Grid" id="del_conn" style="display:none;"/>
      </div> <!-- end of modal-footer -->

    </div>  <!-- end of modal-content -->
  </div> <!-- end of pop_modal -->
</div> <!-- end of myModal -->

<?php include "/opt/observium/html/nexusguard/views/common/ports_dialog.inc.php";  ?>

<?php
include_once("/opt/observium/includes/defaults.inc.php");
include_once("/opt/observium/html/nexusguard/api/common/common.inc.php");
include_once("/opt/observium/config.php");
include_once("/opt/observium/includes/definitions.inc.php");
include_once("/opt/observium/nexusguard/validator/validate_pop.php");
include_once("/opt/observium/nexusguard/config/pop_config.php");
include_once("/opt/observium/nexusguard/db/db_pop_functions.php");
include_once("/opt/observium/nexusgaurd/logger/logger.php");
include_once("/opt/observium/includes/functions.inc.php");
$enroll_device_status=0;
$output=array();
$msg="";
$pop_data = get_input_json();
$errormsg=validate_add_pop($pop_data);

if(!empty($errormsg))
{
    $output= generate_error_response('validation_error',"Validation Errors",$errormsg);
    $output['scope'] = $pop_data["scope"];    
    $output['pop_id'] = $pop_data["pop_id"];
}
else
{
    $enroll_device_status=enroll_new_device($pop_data);
    if($enroll_device_status != 0)
    {
        $output= generate_error_response('enroll_error',"Enrollment failed.","Enrollment failed.","Enrollment failed.");
    }
    else
    {
        $output= generate_success_response("Enrollment Successful.","Enrollment Successful.");
    }
    $array=add_pop_template($pop_data);
    $status = $array['status'];
    $pop_id = $array['pop_id'];
    dbDelete('nxg_pop_interface_ip_mgmt','pop_id = ?' ,array($pop_id));
    $pop_config = generate_new_pop_config($pop_data, $pop_id); // added so that generated if and unit info is saved in db

    $output['scope'] = "preenrolled_edit";
    $output['pop_id'] = $pop_id;
    echo "--JSON---";
}
    
echo json_encode($output);
?>

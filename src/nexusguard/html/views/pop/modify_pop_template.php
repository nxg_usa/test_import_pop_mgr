<script src="/nexusguard/js/common.js"></script>
<link rel="stylesheet" href="/nexusguard/css/jquery.tokenize.css" >
<link rel="stylesheet" href="nexusguard/css/pop_manager.css"/>
<script src="/nexusguard/js/tokenizer/jquery.tokenize.js"></script>

<?php
    include_once("/opt/observium/nexusguard/var/popmgr_var.php");    
    include_once("/opt/observium/nexusguard/db/db_pop_functions.php");
    include_once('/opt/observium/html/nexusguard/api/common/common.inc.php'); 
    include_once('/opt/observium/html/nexusguard/api/common/common.inc.php');
    global $var_popmgr_pvt_key;
    global $var_popmgr_user;
    global $var_device_user;
	$pop_id=$vars['pop'];
	$scope=$vars['scope'];	
    $cmd = "sudo cat ".$var_popmgr_pvt_key;
    $ssh_key= shell_exec($cmd);
    $edge_router_username=dbFetchRow("select username from nxg_auth_details");
    $router_username=$edge_router_username['username'];
    $prefix_list="";
	if(isset($vars['pop']))
	{
		$pop_data=dbFetchRow(" select nxg_pop_details.*,nxg_isp_details.*,nxg_router_interface_mapping.*,nxg_exabgp_details.*,nxg_bgp_details.*,nxg_pop_mgmt_config.* from nxg_pop_details left join nxg_isp_details on nxg_isp_details.pop_details_id=nxg_pop_details.id left join nxg_router_interface_mapping on  nxg_router_interface_mapping.pop_details_id=nxg_pop_details.id left join nxg_pop_mgmt_config on nxg_pop_mgmt_config.pop_id=nxg_pop_details.id left join nxg_exabgp_details on nxg_exabgp_details.pop_details_id=nxg_pop_details.id left join nxg_bgp_details on nxg_bgp_details.isp_id=nxg_isp_details.id where nxg_pop_details.id=?",array($pop_id));

		$pop_edge_router_id=$pop_data['pop_edge_router_device_id'];
		$pop_edge_router_data=dbFetchRow("select hostname,snmp_community,snmp_authpass,snmp_authlevel,snmp_authname,snmp_authalgo,snmp_cryptopass,snmp_cryptoalgo,snmp_version,snmp_port,snmp_timeout,snmp_retries from devices where device_id=?",array ($pop_edge_router_id));
		$pop_edge_router_selected=$pop_edge_router_data['hostname'];
		$pop_interface_data=dbFetchRows("select nxg_pop_details.*,nxg_router_interface_mapping.* from nxg_pop_details left join nxg_router_interface_mapping on nxg_router_interface_mapping.pop_details_id=nxg_pop_details.id where nxg_pop_details.id=?",array($pop_id));

        $service_prefix_list=dbFetchRows("select prefix from nxg_app_service_prefix_list where pop_id =?",array($pop_id));
        foreach ($service_prefix_list as $prefixes)
        {
            $prefix=$prefixes['prefix'];
            $prefix_list.="\n$prefix";
        }

         $pl_agg_prefix_list=dbFetchRows("select prefix from nxg_pl_agg_prefix_list where pop_id =?",array($pop_id));
        foreach ($pl_agg_prefix_list as $prefixes)
        {
            $prefix=$prefixes['prefix'];
            $agg_prefix_list.="$prefix\n";
        }



        $pop_edge_router_data['snmp_timeout']=1;
        $pop_edge_router_data['snmp_retries']=5;
	}

    

?>
<script type="text/javascript">
var button_array = ['cancel','commit_check','decommission'];
var rowCount = 1;
var row = 2;
function page_reload()
{
    window.location.reload();
}
function show_member_ports(id,type)
{
    $("#rowCount0"+id+type).show();
}
function show_port_modal()
{
    $("#portsModal").modal('show');
}


function setValue()
{
        ip=document.getElementById("syslog_host_ip").value;
        document.getElementById("netflow_server_ip_readonly").value=ip;
        port=document.getElementById("netflow_port").value;
        document.getElementById("netflow_port_readonly").value=port;


}


$(document).ready(function () {
        setValue();
		$( "#commit_check" ).click(function() {

			commit_check('add_pop_form','/nexusguard/api/modify_pop.php');

			});

		$( "#cancel_config" ).click(function() {
			$("#pop_config").hide();
			$("#add_pop_form :input").prop('readonly', false);
			});
		$( "#applyconfig" ).click(function() {

			apply_config('add_pop_form','/nexusguard/api/modify_pop.php',button_array);
            $("#cancel_btn").html('Back');
            $("#commit_check").hide();
            $("#decommission").hide();

			});
            get_port_table_data();
		});

function get_port_table_data()
    {

        $("#port_table").html('<img src="nexusguard/img/ajax-loader.gif">loading...');

        $.ajax({ url: "nexusguard/api/get_port_table.php?pop_id="+$("#pop_id").val(),
            data: '',
            type: 'post',
            success: function(output) {
                $("#port_table").html(output);
            },
            error:function(data){
                alert("Error in processing request.");
            }
        });

    }

   
function getResult(form)
{
	(function($) {

	 var frm = $(document.myform);
	 var dat = JSON.stringify(frm.serializeArray());
	 var datastring = $("#add_pop_form").serialize();
	 $.ajax({ url: 'nexusguard/views/pop/process_pop_template.php',
		 data:datastring+"&action=submit",
		 type: 'post',
		 success: function(output) {
		 $('#errormsg').html(output);
		 /*                    if(output.indexOf('err')==-1)
				       {
				       var obj = JSON.parse(output);
				       if(obj.error_code!= "")
				       {
				       $('#errormsg').html(obj.error_message);
				       }
				       }
				       else
				       {
				       window.location.href="pop_mgr/view=pop_isp_grid/";
				       }
				       if(output.indexOf('Error')==-1)
				       {
				       window.location.href="pop_mgr/view=pop_template_grid/";
				       }*/
		 }
	 });
	})(jQuery);
}

$(document).ready(function(){

    rowCount = $('#traffic_to_processor_row_id').val();
   $('#decommission').on('click',function(){

             if(confirm("Confirm Delete PoP?") != true){
                    return;
                }

    var datastring = parse_form("add_pop_form");
    var json_data = JSON.stringify(datastring);
	 $.ajax({ url: 'nexusguard/views/pop/process_decommission_pop.php',
		 data:json_data,
		 type: 'post',
		 success: function(output) {
		 $('#errormsg').html(output);
                if(output.indexOf('errorcode')==-1)
                {
                     var obj = JSON.parse(output);
                    $('#errormsg').html(obj.msg);
                    $('#error_wrap').show();
                    window.location.href="pop_mgr/view=pop_isp_grid/";
                }
                else
                {
                    var obj = JSON.parse(output);
                    $('#errormsg').html(obj.msg);
                    $('#error_wrap').show();
                }
		 }
	 });
});
});

function generateConfig(form)
{
	(function($) {
	 var frm = $(document.myform);
	 var dat = JSON.stringify(frm.serializeArray());
	 var datastring = $("#add_pop_form").serialize();
	 $.ajax({ url: 'nexusguard/views/pop/generate_config.php',
		 data: datastring,
		 type: 'post',
		 success: function(output) {
		 $('#errormsg').html(output);
		 /*                    if(output.indexOf('Error')==-1)
				       {
				       $("#pop_config").html(output);
				       }
				       else
				       {
				       $("#pop_modal").modal('hide');
				       }
		 /*                   $("#pop_modal").modal('show');

		 /*                   if(output.indexOf('Error')==-1)
		 {
		 window.location.href="pop_mgr/view=pop_template_grid/";
		 }*/
		 }
		 });
	})(jQuery);
}


function addMoreRows()
{
       var options = document.getElementById("intf_to_processor_ref").options;
       var div_select_box = '<select id="member_ports_div'+rowCount+'" multiple="multiple" class="ae_interface" name="lag_interface">';
       for(i=0;i<options.length;i++)
       {
           div_select_box = div_select_box+'<option value="'+options[i].value+'">'+options[i].text+'<options>';
       }
       div_select_box = div_select_box+'</select>';

       var ins_select_box = '<select id="member_ports_ins'+rowCount+'" multiple="multiple" class="ae_interface" name="lag_interface_ins">';
       for(i=0;i<options.length;i++)
       {
           ins_select_box = ins_select_box+'<option value="'+options[i].value+'">'+options[i].text+'<options>';
       }
       ins_select_box = ins_select_box+'</select>';

       var str = '<tr id="rowId'+rowCount+'1">';
       str += '<td  class="new_pull_right">Type</td>';
       str += '<td colspan="2"><select id="type'+rowCount+'" name="type" class="input-small" onChange="change_intf('+rowCount+')" >';
       str += '<option value="td" >TD</option>';
       str += '<option value="tms" >TMS</option>';
       str += '<option value="tdtms" >TD+TMS</option>';
       str += '<option value="custom" >CUSTOM</option>';

       str += '</select>';
       str += '&nbsp;&nbsp;<span style="display:none" class="new_pull_right" id="cust_table2'+rowCount +'" >Name <input type="text" class="" name="traffic_name"/></span>';
       str += '</td>';
       str += '</tr>';
       str += '<tr id="rowId'+rowCount+'2">';

       str += '<td class="new_pull_right">Diversion Interface *</td>';
       str += '<td> <input type="text" id= "intf_to_processor_div" name="intf_to_processor" list="intf_to_processor_ref_ae" class="input-small"/>';
        
       str += '<br> <input type="button" class="btn btn-mini btn-success select_port"  value="Show Ports" id="select_port" name="select_port" onClick="show_port_modal(); return false;"/> <input type="button" class="btn btn-mini btn-success" name="add_button" id="add_more_member_div" onClick="show_member_ports('+rowCount+',0); return false" value="Add Member Ports"/>';
       str += '<td>Vlan *<input type="text" id="div_vlan" class="input-small" name="vlan"/></td>';
       str += '</td>';

       str += '<td>Insertion Interface *</td>';
       str += '<td>';
       str += '<input type="text" name="ins_intf" id="intf_to_processor_ins" list="intf_to_processor_ref_ae" class="input-small"/>'
       str += '<br> <input type="button" class="btn btn-mini btn-success select_port"  value="Show Ports" id="select_port" name="select_port" onClick="show_port_modal(); return false;"/> <input type="button" class="btn btn-mini btn-success" name="add_button" id="add_more_member_ins" onClick="show_member_ports('+rowCount+',1); return false" value="Add Member Ports"/>';
       str += '</td>';
       str += '<td>Vlan *<input type="text" id="ins_vlan" class="input-small"  name="ins_vlan" id="interface" />';

       str += '</td>';
       str += '<td class="new_pull_left"> <button class="btn-mini btn-danger" id="remove_btn" name="add" value="save" onclick="removeRow('+rowCount+'); return false;">Remove</button></td>';
       str += '</tr>';

       str += '<tr id="rowId'+rowCount+'3">';
       str += '<td>&nbsp;</td>';
       str += '<td class="pull_left" id="rowCount0'+rowCount+'" >'
       str += '<span id="rowCount0'+rowCount+'0" style="display:none" >';
           str += div_select_box;

       str += '</span></td>';
       str += '<td>&nbsp;</td>';
       str += '<td>&nbsp;</td>';
       str += '<td class="pull_left" id="rowCount0'+rowCount+'1" style="display:none">';
       str += ins_select_box;
       str += '</td>';
       str += '</tr>';

   //$('#addedRows > tbody > tr').eq(1+rowCount-3).after(recRow);
        $('#addedRows').append(str);
       $('#member_ports_div'+rowCount).tokenize();
           $('#member_ports_ins'+rowCount).tokenize();


      rowCount = parseInt(rowCount)+1;

}
function setVlan(id)
{
    div_vlan=document.getElementById("vlan_div"+id).value;
    document.getElementById("interface"+id).value=div_vlan;
}
function removeRow(id)
{
    if(rowCount ==1)
    {
        alert("Cannot remove last row, Atleast one traffic to processor interface is needed");
        return;
        
    }
      
     jQuery('#rowId'+id+'1').remove();
    jQuery('#rowId'+id+'2').remove();
    jQuery('#rowId'+id+'3').remove();
	// jQuery('#rowCount'+removeNum).remove();
    rowCount = parseInt(rowCount)-1;
}



function changeContent(){
	var x = document.getElementById('type');
	var value = x.options[x.selectedIndex].text;


}
function change_intf(row)
{
	var elem = document.getElementById("type"+row);
	var hiddenDiv = document.getElementById("cust_table2"+row);
	var hiddenDiv1 = document.getElementById("cust_table"+row);
	hiddenDiv.style.display = (elem.value == "custom") ? "":"none";
//	hiddenDiv1.style.display = (elem.value == "custom") ? "":"none";
}




function addRows(thisform){
	row= row++;
	var rowstring = ' <tr id="rowCount'+row+'">  <td class="pull_right">Local IP</td><td class="pull_right"> <input type="text" class="input" name="local_ip[]" /></td><td>/<input type="text" class="subnet" name="local_ip_subnet[]" /></td><td class="pull_center">Neighbour IP</td><td class="pull_right"><input type="text" name="neighbour_ip[]" class="input"/></td><td>/<input type="text"  name="neighbour_ip_subnet[]" class="subnet"/></td></tr><tr id="rowCount'+row+'"><td class="pull_right" >Local as</td><td class="pull_right"><input type="text" name="local_as[]" class="input"/></td><td class="pull_right" colspan="2">Peer as </td><td class="pull_right"><input name="peer_as[]" type="text" class="input"/></td></tr>';

	$('#add_pop_row > tbody > tr').eq(6+row-2).after(rowstring);
}

</script>
        <form id ="add_pop_form" action="" method="post">
            <h3>Modify PoP</h3>
        <div class="row">
    <div class="col-md-6">

      <div class="widget widget-table">
        <div class="widget-header">
          <i class="oicon-gear"></i><h3>Basic Configuration</h3>
        </div>
            <div style="padding-top: 10px;" class="widget-content">
            <input type="hidden" name="scope" value="<?php echo $scope; ?>" />
            <input type="hidden" name="pop_id" value="<?php echo $pop_id; ?>" id="pop_id"/>
            <input type="hidden" name="action" value="commit_check" />
                 <table class="form_table" id="add_pop" >
                <tr>
                    <td class="pull_right">PoP Name *</td>
                    <td class="pull_left"> <input  class="input" name="pop_name" type="text" value="<?php echo $pop_data['pop_name'];?>" readonly/></td>
                    <td class="pull_right">OSS Device Id *</td>
                    <td class="pull_left"> <input  class="input-small" name="oss_dev_id" type="text" value="<?php echo $pop_data['oss_device_id'];?>"/></td>
                </tr>
                <tr>
                    <td  class="pull_right"> Description</td>
                    <td class="pull_left"><textarea  class="" name="pop_description" type="text" ><?php echo $pop_data['pop_description'];?></textarea></td>
                    <td  class="pull_right"> Notes</td>
                    <td class="pull_left" valign ="top" rowspan="5"><textarea  class="" style="height:200px" name="pop_notes" type="text" ><?php echo $pop_data['pop_notes'];?></textarea></td>


                </tr>
                <tr>
                <td  class="pull_right" style="display:none"> Existing &nbsp;&nbsp;<input type="radio" name="edge_router_type" value="existing_router" id="edge_router_type_exist"/></td>
                    <td  class="pull_right" style="display:none" >Add New &nbsp;&nbsp;<input type="radio" name="edge_router_type" value="new_router" id="edge_router_type" checked="checked"/></td>

                </tr>

                <tr id="add">
                    <td class="pull_right" id="name_block">Edge Router Name *</td>
                    <td class="pull_right" id="inp_block"><input class="input" type="text"  name="new_name" value= "<?php echo $pop_edge_router_selected;?>" readonly/></td>
                </tr>

<!--                <tr id="add">
                    <td class="pull_right" id="name_block">Edge Router Username</td>
                    <td class="pull_right" id="inp_block"><input class="input" type="text"  name="username"/></td>
                </tr> -->

                    <td  class="pull_right" style="display:none"> Existing &nbsp;&nbsp;<input type="radio" name="srx_type" id="exist_srx" value="existing_srx" checked="checked"/></td>
                    <td  class="pull_right" style="display:none">Add New &nbsp;&nbsp;<input type="radio" name="srx_type" id="new_srx" value="new_srx"/></td>

                </tr>
                <tr id="add_new_srx" style="display:none">
                    <td class="pull_right" id="name_block_srx">Name</td>
                    <td class="pull_right" id="inp_block_srx"><input class="input" type="text"  name="new_name_srx"/></td>
                    <td class="pull_right" id="ip_block_srx"> IP</td>
                    <td class="pull_right" id="inp_block_ip_srx"><input class="input" type="text" name="new_ip_srx"/></td>
                </tr>

                <tr id="pop_router_srx" style="display:none">

                    <td class="pull_right">
                            PoP SRX </td>
                    <td class="pull_left">
                                        <select name="srx" class="input">
                                         <?php

                                        foreach (dbFetchRows('SELECT `device_id`, `hostname` FROM `devices` GROUP BY `hostname` ORDER BY `hostname`') as $data)
                                            {
                                                  if (device_permitted($data['device_id']))
                                                  {
                                                        echo('        <option value="'.$data['device_id'].'"');
                                                        if ($data['device_id'] == $vars['device_id'] || in_array($data['device_id'], $vars['device_id']) ) { echo(' selected'); }
                                                         echo('>'.escape_html($data['hostname']).'</option>');
                                                  }
                                            }
                                    ?>
                                        </select>
                    </td>
                </tr>
                        <tr>
                 <td class="pull_right" id="username">Service Prefix List *</td>
                <td class="pull_left"><textarea  class="" name="prefix_list" type="text" placeholder="a.b.c.d/xx" ><?php echo $prefix_list;?></textarea></td>

                </tr>

            
                <tr>
                         <td class="pull_right">VR Core Public IP *</td>
                          <td class="pull_right">
                                <input type="text" name="loopback_ip" value="<?php echo $pop_data['loopback_ip'];?>"/></td>
                             <td class="pull_left" id="pl_agg_prefix_list">PL Aggregate Prefix List *</td>
                                <td class="pull_left"><textarea  class="" name="pl_agg_prefix_list" type="text"><?php echo $agg_prefix_list; ?></textarea></td>


                        </tr>
                <tr>



        </table>

          </div> <!-- end of widget-content-->
        </div> <!-- End of widget -->
    </div> <!-- End of col-md-6 -->

    <div class="col-lg-6 pull-right">

      <div class="widget widget-table">
        <div class="widget-header">
          <i class="oicon-gear"></i><h3>SNMP</h3>
        </div>
            <div style="padding-top: 10px;" class="widget-content">

        <table class="form_table">
              <tr>
                        <td  class="pull_right">Protocol Version</td>
                            <td class="pull_right"><select name="proto_version" type="text" class="input" selected="<?php echo $pop_edge_router_data['snmp_version'] ?>" readonly>
                                         <option value="v3">v3</option>
                                </select>
                            </td>
                        <td class="pull_right">Auth Level</td>

                        <td class="pull_right">    <select name="auth_level" type="text" class="input" selected="<?php echo $pop_edge_router_data['snmp_authlevel'] ?>" readonly>

                        <?php 
                             if($pop_edge_router_data['snmp_authlevel'] == "noAuthNoPriv")
                              {
                                    $nanp_selected="selected";
                              }
                              elseif($pop_edge_router_data['snmp_authlevel'] == "authNoPriv")
                              {
                                    $anp_selected="selected";

                              }
                              else
                              {
                                    $ap_selected="selected";
                                }

                             ?>           

                                         <option value="noAuthNoPriv" <?php echo $nanp_selected ?> >noAuthNoPriv</option>
                                         <option value="authNoPriv" <?php echo $anp_selected  ?>>authNoPriv</option>
                                         <option value="authPriv" <?php echo $ap_selected ?> >authPriv</option>
                                </select>
                        </td>
              </tr>
              <tr>
                            <td  class="pull_right">Transport</td>
                            <td class="pull_right">    <select name="transport" type="text"  class="input" value="<?php echo $pop_edge_router_data['snmp_transport'] ?>" readonly>
                                         <option value="udp">UDP</option>
                                         <option value="udp6">UDP6</option>
                                         <option value="tcp">TCP</option>
                                          <option value="tcp6">TCP6</option>

                                </select>
                            </td>
                                 <td  class="pull_right">Auth Name *</td>

                            <td class="pull_right">
                                <input type="text" name="user_name" class="input" value="<?php echo $pop_edge_router_data['snmp_authname'] ?>" readonly>
                            </td>


             </tr>
             <tr>
                             <td  class="pull_right">Port</td>
                             <td class="pull_right" ><input type="text"  class="input"  name="port" value="<?php echo $pop_edge_router_data['snmp_port'] ?>" readonly /></td>
                             <td class="pull_right">Auth Password *</td>
                              <td  class="pull_right"><input type="password"  class="input" name="auth_password" value="<?php echo $pop_edge_router_data['snmp_authpass'] ?>" readonly/></td>
             </tr>
             <tr>
                             <td class="pull_right">Time Out</td>
                             <td class="pull_right"><input type="text" name="timeout"  class="input" value="<?php echo $pop_edge_router_data['snmp_timeout'] ?>" readonly/></td>
                             <td class="pull_right">Auth Algorithm</td>
                              <td  class="pull_right"><select name="auth_algo" type="text"  class="input" value="<?php echo $pop_edge_router_data['snmp_authalgo'] ?>" readonly/>
                                         <option value="md5">MD5</option>
                                         <option value="sha">SHA</option>
                                </select>
                            </td>
             </tr>
             <tr>
                              <td  class="pull_right">Retries</td>
                              <td class="pull_right" ><input type="text" name="tries"  class="input" value="<?php echo $pop_edge_router_data['snmp_retries'] ?>" readonly/></td>
             </tr>
    </table>


            </div> <!-- end of widget-content -->
      </div> <!-- End of widget-table -->
    </div> <!-- End of col-md-6 -->

  </div>  <!-- End of row -->

<div class="row">
    <div class="col-md-6" style="width:100%;">
      <div class="widget widget-table">
        <div class="widget-header">
          <i class="oicon-gear"></i><h3>Management Interface Configuration</h3>
        </div>
            <div style="padding-top: 10px;" class="widget-content">
                <table id="addedRow" class="form_table">
                    <tr>
                        <td class="new_pull_right">Interface *</td>
                        <td class="new_pull_right">
                            <input type="text" name="mgmt_interface" list="mgmt_interfaces" value="<?php echo $pop_data['mgmt_interface'];?>" readonly/>
                                        <datalist id="mgmt_interfaces"  class="" >
                                            <?php $i=0;for($i=0;$i<48;$i++){echo "<option value='ge-0/0/".$i."'>ge-0/0/".$i."</option>";}?>
                                        </datalist>
                        </td>
                        <td class="new_pull_right">IP *</td>
                        <td class="new_pull_right">
                                <input type="text" name="mgmt_ip" value="<?php echo $pop_data['mgmt_ip'];?>" readonly/>&nbsp;&nbsp;/&nbsp;&nbsp;<input type="text" name="mgmt_ip_subnet" value="<?php echo $pop_data['mgmt_ip_subnet'];?>" readonly/>
                        </td>
                        <td class="new_pull_right">Gateway *</td>
                        <td class="new_pull_right">
                                <input type="text" name="mgmt_gateway" value="<?php echo $pop_data['mgmt_gateway'];?>" readonly/>
                        </td>
                    </tr>
                </table>
            </div> <!-- end of widget-content -->
      </div> <!-- End of widget-table -->
    </div> <!-- End of col-md-6 -->

  </div>  <!-- End of row -->


<div class="row">
    <div class="col-md-8" style="width:100%;">
      <div class="widget widget-table">
        <div class="widget-header">
          <i class="oicon-gear"></i><h3>Traffic Processor Interface</h3>
        </div>
            <div style="padding-top: 10px;" class="widget-content">

        <table id="addedRows" class="form_table">
<?php $cnt=0;foreach($pop_interface_data as $interface){?>

                <tr id="rowId<?php echo $cnt;?>1">
                        <td  class="new_pull_right">
                             Type
                        </td>
                        <td colspan="2">
                                         <select id="type<?php echo $cnt; ?>" name="type" class="input-small" onChange="change_intf(<?php echo $cnt; ?>)" readonly>
                                        <?php
                                            $db_scrubber= $interface['scrubber_type'];
                                            $scrubber_type_array= array('td'=>'TD','tms'=>'TMS','tdtms'=>'TD+TMS','custom'=>'Custom');
                                            foreach($scrubber_type_array as $type=>$value)
                                            {
                                                if($type==$db_scrubber)
                                                {
                                                    echo "<option value='".$type."' selected=selected>".$value."</option>";
                                                }
                                                else
                                                {
                                                    //echo "<option value='".$type."' >".$value."</option>";
                                                }
                                            }
                                        ?>
                                        </select>

                    <?php if(  $interface['scrubber_type'] =="custom")
                        {
                            echo '&nbsp;&nbsp;<span  class="new_pull_right" id="cust_table2'.$cnt .'" >Name <input type="text" class="" name="traffic_name"value="'.$interface['scrubber_custom_name'].'" readonly/></span>';
                        }
                        else
                        {
                            echo '&nbsp;&nbsp;<span  style="display:none" class="new_pull_right" id="cust_table2'.$cnt.'" >Name <input type="text" class="" name="traffic_name"value="'.$interface['scrubber_custom_name'].'"/></span>';
                        }
                    ?>
                       </td>

                </tr>
                <tr id="rowId<?php echo $cnt;?>2">

                      <td class="new_pull_right">
                            <input type="hidden" name="gui_row_id" value="<?php echo $interface['row_id'];?>"/>
                             Diversion Interface *
                        </td>
                        <td>
 <input type="text" id= "intf_to_processor_div" name="intf_to_processor" list="intf_to_processor_ref_ae" value="<?php echo $interface['device_interface_name'];?>" readonly class="input-small"/>

                                        <datalist id="intf_to_processor"  class=""  >
                                            <?php
                                                $i=0;
                                                for($i=0;$i<48;$i++)
                                                {
                                                    echo "<option value='ge-0/0/".$i."'>ge-0/0/".$i."</option>";
                                                }
                                             ?>

                                        </datalist>
    
                          <br> <input type="button" class="btn btn-mini btn-success select_port"  value="Show Ports" id="select_port" name="select_port" onClick="show_port_modal(); return false;"/>
                            <input type='button' class='btn btn-mini btn-success' name='add_button' id='add_more_member_div' onClick="show_member_ports(<?php echo $cnt;?>,0); return false" value="Add Member Ports"/>
                       </td>

                        <td>Vlan *
                       <input type="text" id="div_vlan" class="input-small" name="vlan"  value="<?php echo $interface['vlan_id'];?>" readonly/>
                         </td>

                        <td>Insertion Interface *</td>
                        <td>
                                <input type="text" name="ins_intf" id="intf_to_processor_ins" list="intf_to_processor_ref_ae"  value="<?php echo $interface['insertion_interface'];?>"  class="input-small" readonly/> <br>
 <input type="button" class="btn btn-mini btn-success select_port"  value="Show Ports" id="select_port" name="select_port" onClick="show_port_modal(); return false;"/>
<input type="button" class="btn btn-mini btn-success" name="add_button" id="add_more_member_ins" onClick="show_member_ports(<?php echo $cnt;?>,1); return false" value="Add Member Ports"/>
                        </td>
                        <td>Vlan * <input type="text" id="ins_vlan" class="input-small"  name="ins_vlan" id="interface" value="<?php echo $interface['insertion_vlan'];?>" readonly/></td>
                <td class="new_pull_left"> <button class="btn-mini btn-danger" id="remove_btn" name="add" value="save" onclick="removeRow(<?php echo $cnt;?>); return false;">Remove</button></td>


             </tr>
                <tr id="rowId<?php echo $cnt;?>3">
                <td>&nbsp;</td>

                <td class="pull_left" id="rowCount0<?php echo $cnt;?>" style="">


             <?php


        $ae_member_ports_div=get_all_ae_members($pop_id,$interface['device_interface_name']);
        $ae_members_cnt=count($ae_member_ports_div);

        if(!isset($pop_id) || $ae_members_cnt==0)
        {

            echo '<span id="rowCount0'. $cnt.'0" style="display:none">';
            echo '<select id="member_ports_div'.$cnt.'"  multiple="multiple" class="ae_interface" name="lag_interface">';

            $all_ports_info = get_pop_ge_ports_info($pop_id);
            foreach($all_ports_info as $ports_info)
            {
                echo "<option value='".$ports_info['ifName']."'>".$ports_info['ifName']."</option>";
            }

            echo "</select>";
        }
        else
        {

             echo '<span id="rowCount0'. $cnt.'0">';
             echo '<select id="member_ports_div'. $cnt.'"  multiple="multiple" class="ae_interface" name="lag_interface">';
             $all_ports_info = get_pop_ge_ports_info($pop_id);
             foreach($all_ports_info as $ports_info)
             {
                 echo "<option value='".$ports_info['ifName']."'>".$ports_info['ifName']."</option>";
             }

             echo "</select>";

            foreach($ae_member_ports_div as $ae_member)
            {
                 echo '<script type="text/javascript">';
                 echo '$("#member_ports_div'. $cnt.'").tokenize().tokenAdd("'.$ae_member['member_interface_name'].'","'.$ae_member['member_interface_name'].'");';
                 echo '</script>';
            }



        }
        echo '</span>';
        echo '<script type="text/javascript">';
        echo '$("#member_ports_div'.$cnt.'").tokenize();';
        echo '</script>';


?>
             </td>


                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td class="pull_left" id="rowCount0<?php echo $cnt;?>" style="">



<?php


        $ae_member_ports_div=get_all_ae_members($pop_id,$interface['insertion_interface']);
        $ae_members_cnt=count($ae_member_ports_div);


        if(!isset($pop_id) || $ae_members_cnt==0)
        {

            echo '<span id="rowCount0'. $cnt.'1" style="display:none">';
            echo '<select id="member_ports_ins'. $cnt.'"  multiple="multiple" class="ae_interface" name="lag_interface_ins">';
            $all_ports_info = get_pop_ge_ports_info($pop_id);
            foreach($all_ports_info as $ports_info)
            {
                echo "<option value='".$ports_info['ifName']."'>".$ports_info['ifName']."</option>";
            }

            echo "</select>";
        }
        else
        {

             echo '<span id="rowCount0'. $cnt.'1">';
             echo '<select id="member_ports_ins'. $cnt.'"  multiple="multiple" class="ae_interface" name="lag_interface_ins">';
             $all_ports_info = get_pop_ge_ports_info($pop_id);
             var_dump($all_ports_info);
             foreach($all_ports_info as $ports_info)
             {
                 echo "<option value='".$ports_info['ifName']."'>".$ports_info['ifName']."</option>";
             }



             echo "</select>";
            foreach($ae_member_ports_div as $ae_member)
            {
                 echo '<script type="text/javascript">';
                 echo '$("#member_ports_ins'. $cnt.'").tokenize().tokenAdd("'.$ae_member['member_interface_name'].'","'.$ae_member['member_interface_name'].'");';
                 echo '</script>';
            }

        }
        echo '</span>';
        echo '<script type="text/javascript">';
        echo '$("#member_ports_ins'. $cnt.'").tokenize();';
        echo '</script>';

?>


                </td>
            </tr>


<?php $cnt++;}
echo "<input type='hidden' id='traffic_to_processor_row_id' value='".$cnt."'/>";
?>
</table>
<table>
                <tr>
                    <td class="pull_left" colspan="1">&nbsp;&nbsp;<button class="btn btn-mini btn-success" id="add_btn"  name="add" value="save" onclick="addMoreRows(); return false;" >Add More</button></td>
                </tr>
		
    </table>

            </div> <!-- end of widget-content -->
      </div> <!-- End of widget-table -->
    </div> <!-- End of col-md-6 -->
  </div>  <!-- End of row -->


 <div class="row">
    <div class="col-lg-6 pull-right" style="width:100%">
      <div class="widget widget-table">
        <div class="widget-header">
          <i class="oicon-gear"></i><h3>FlowSpec Manager</h3>
        </div>


            <div style="padding-top: 10px;" class="widget-content">
        <table class="form_table">
            <tr><td colspan="2"><b>Local Speaker Details:</b> </td></tr>
            <tr>
                <td class="pull_right">Interface *</td>
                <td class="pull_right">

                     <input type="text" name="local_interface" list="exabgp_local_interfaces" value="<?php echo $pop_data['local_speaker_interface'];?>"/>
                                        <datalist id="exabgp_local_interfaces"  class="" >
                                            <?php $i=0;for($i=0;$i<48;$i++){echo "<option value='ge-0/0/".$i."'>ge-0/0/".$i."</option>";}?>
                                        </datalist>

                <td class="pull_right">Vlan *</td>
                <td class="pull_left"><input name="local_vlan" type="text" class="input-small" value="<?php echo $pop_data['local_vlan'];?>"/></td>
                
                 <td class="pull_right">Stale Routes Time *</td>
                <td class="pull_left"><input  type="text" name="stale_routes_time_local" class="input-small" value="<?php echo $pop_data['stale_routes_time_local'];?>"/></td>

                <td class="pull_right">Local ASN *</td>
                <td class="pull_left"><input  type="text" name="local_asn" class="input-small" value="<?php echo $pop_data['local_asn'];?>"/></td>

                <td class="pull_right">Speaker IP *</td>
                <td class="pull_right"><input  type="text" name="local_speaker_ip" class="input" value="<?php echo $pop_data['local_speaker_ip'];?>"/></td>

            </tr>
            <tr>

                <td class="pull_right">Server IP *</td>
                <td class="pull_right"><input  type="text" name="local_server_ip" class="input" value="<?php echo $pop_data['local_server_ip'];?>"/></td>

                <td class="pull_right">Graceful Restart Timer *</td>
                <td class="pull_left"><input  type="text" name="restart_timer_local" class="input-small" value="<?php echo $pop_data['restart_time_local'];?>"/></td>

                
                <td class="pull_right">MD5 Keystring *</td>
                <td class="pull_left"><input  type="text" name="local_server_md5" style="width:660%;" class="input" value="<?php echo $pop_data['local_speaker_md5_key_string'];?>"/></td>


            </tr>
            <tr>
                        <tr><td colspan="2"><b>Global Speaker Details:</b> </td></tr>
                <td class="pull_right">Interface *</td>
                <td class="pull_right">

                     <input type="text" name="global_speaker_interface" list="global_speaker_interfaces" value="<?php echo $pop_data['global_speaker_interface'];?>"/>
                                        <datalist id="global_speaker_interfaces"  class="" >
                                            <?php $i=0;for($i=0;$i<48;$i++){echo "<option value='ge-0/0/".$i."'>ge-0/0/".$i."</option>";}?>
                                        </datalist>

                <td class="pull_right">Vlan *</td>
                <td class="pull_left"><input name="global_vlan" type="text" class="input-small" value="<?php echo $pop_data['global_vlan'];?>"/></td>

                <td class="pull_right">Stale Routes Time *</td>
                <td class="pull_left"><input  type="text" name="stale_routes_time_global" class="input-small" value="<?php echo $pop_data['stale_routes_time_global'];?>"/></td>
                <td class="pull_right">Local ASN *</td>
                <td class="pull_left"><input  type="text" name="local_asn_global" class="input-small" value="<?php echo $pop_data['local_asn_global'];?>"/></td>

                <td class="pull_right">Speaker IP *</td>
                <td class="pull_right"><input  type="text" name="global_speaker_ip" class="input" value="<?php echo $pop_data['global_speaker_ip'];?>"/></td>


            </tr>

            <tr>
                <td class="pull_right">Server IP *</td>
                <td class="pull_right"><input  type="text" name="global_server_ip" class="input" value="<?php echo $pop_data['global_server_ip'];?>"/></td>
                <td class="pull_right">Graceful Restart Time *</td>
                <td class="pull_left"><input  type="text" name="restart_timer_global" class="input-small" value="<?php echo $pop_data['restart_time_global'];?>"/></td>
                
                <td class="pull_right">MD5 Keystring *</td>
                <td class="pull_left"><input  type="text" name="global_speaker_md5" style="width:660%;" class="input" value="<?php echo $pop_data['global_speaker_md5_key_string'];?>"/></td>

            </tr>

    </table>
            </div> <!-- end of widget-content -->
      </div> <!-- End of widget-table -->
    </div> <!-- End of col-md-6 -->



 <div class="row">
    <div class="col-lg-6 pull-right" style="width:100%">
      <div class="widget widget-table">
        <div class="widget-header">
          <i class="oicon-gear"></i><h3>NetFlow & Syslog Configuration</h3>
        </div>

            <div style="padding-top: 10px;" class="widget-content">
        <table class="form_table">

        <tr><td colspan="2"><b>Edge Router Netflow Configuration :</b> </td></tr>
            <tr>
                <tr>

                
                <td class="pull_left">Netflow Source Address *</td>
                <td class="pull_right"><input  type="text" id="netflow_source_address" name="netflow_source_address" class="input" value="<?php echo $pop_data['netflow_source_address']; ?>"/>


                <td class="pull_left">Netflow Server IP *</td>
                <td class="pull_right"><input  type="text" id="netflow_server_ip" name="netflow_server_ip" class="input" value="<?php echo $pop_data['netflow_server_ip']; ?>"/>

                <td class="pull_left">Port *</td>
                <td class="pull_right"><input  type="text" id="netflow_port" name="netflow_port" class="input" value="<?php if(!empty($pop_data['netflow_port'])){echo $pop_data['netflow_port'];}else{ echo "9995"; }  ?>"  onChange="setValue(); return false;"/>
             </tr>


                <tr><td colspan="2"><b>Edge Router Syslog Configuration :</b> </td></tr>
                <td class="pull_left">Syslog Host IP *</td>
                <td class="pull_right"><input  type="text" id="syslog_host_ip" name="syslog_host_ip" class="input" value="<?php echo $pop_data['syslog_host_ip']; ?>" onChange="setValue(); return false;"/>
                </tr>

                <tr><td colspan="2"><b>PoP Manager Netflow Configuration :</b> </td></tr>
                <td class="pull_left">Netflow Source IP *</td>
                <td class="pull_right"><input  type="text" id="flow_mgr_ip" name="flow_mgr_ip" class="input" value="<?php echo $pop_data['flow_mgr_ip']; ?>"  placeholder="a.b.c.d"/> 

                <td class="pull_left">Netflow Server IP </td>
                <td class="pull_right"><input  type="text" id="netflow_server_ip_readonly" name="netflow_server_ip_readonly" class="input" readonly/>

                <td class="pull_left">Port </td>
                <td class="pull_right"><input  type="text" id="netflow_port_readonly" name="netflow_port_readonly" class="input" readonly/>
 
                </tr>



            </tr>

            </table>
            </div> <!-- end of widget-content -->
      </div> <!-- End of widget-table -->
    </div> <!-- End of col-md-6 -->
  </div>  <!-- End of row -->





    <div class="row">
    <div class="col-lg-6 pull-right" style="width:100%">
      <div class="widget widget-table">
        <div class="widget-header">
          <i class="oicon-gear"></i><h3>PoP Manager keys</h3>
        </div>



    <div style="padding-top: 10px;" class="widget-content">
        <table class="form_table">

            <tr>
                <tr>
                <td class="pull_left">Username for Edge Router: <?php echo $var_device_user ?></td>
                </tr>
                <tr>
                <td class="pull_left">Username for ExaBGP Server: <?php echo $var_popmgr_user ?></td>
                </tr>
                <tr>
                <td class="pull_left">PoP Manager Host IP : <?php echo $var_syslog_host_ip ?></td>
                </tr>
                <td class="pull_left">SSH Public Key:<input  type="text" id="ssh_key" name="ssh_key" style="width:470%;" class="input" value="<?php echo $ssh_key; ?>" readonly/>
<!--                <button class="button" style="float: left;" onclick="copyToClipboard(document.getElementById('ssh_key').innerHTML);">Copy</button></td> -->
            </tr>

            </table>
            </div> <!-- end of widget-content -->
      </div> <!-- End of widget-table -->
    </div> <!-- End of col-md-6 -->
  </div>  <!-- End of row -->

<input type="hidden" name="ip_interface_mgmt_update_time" value="<?php echo get_ip_interface_mgmt_time(); ?>" />


</form>

<?php include 'nexusguard/views/includes/commit_check_footer.php'; ?>


<div class="form-actions">
                    <input type='button' class='btn btn-primary' onclick="location.href='pop_mgr/view=pop_isp_grid/'" id="cancel" name='cancel_button' value="Cancel"/>
                    <input type='button' data-target="#myModal" class='btn btn-primary'  name='commit_check' id = "commit_check" value="Modify"/>
                    <input type="button" class="btn btn-primary" name="decommission" value="Decommission" id="decommission"/>
</div>


<!--  <div class="form-actions">
        <input type='button' class='btn btn-primary' onclick="location.href='pop_mgr/view=pop_isp_grid/'" name='cancel' id="cancel" value="Cancel"/>
       <input type="button" class="btn btn-primary" id="commit_check" name="commit_check" value="Modify PoP" >
       <input type="button" class="btn btn-primary" name="decommission" value="Decommission" onclick="decommission_pop(this.form);return false;" id="dec_btn"/></button></td>
 </div>-->


      <!--  <div id="errormsg"></div> -->

<div class="alert alert-error" id="error_wrap" style="display:none">
        <div class="pull-left" style="padding:0 5px 0 0"><i class="oicon-exclamation-red"></i></div>
        <div id='errormsg'></div>
</div>

<datalist id="intf_to_processor_ref"  class=""  >
<?php
$all_ports_info = get_pop_ge_ports_info($pop_id);
foreach($all_ports_info as $ports_info)
{
    echo "<option value='".$ports_info['ifName']."'>".$ports_info['ifName']."</option>";
}

?>

</datalist>
<datalist id="intf_to_processor_ref_ae"  class=""  >
<?php
$all_ports_info = get_pop_ports_info($pop_id);
foreach($all_ports_info as $ports_info)
{
    if(!empty($ports_info['ifName']))
    echo "<option value='".$ports_info['ifName']."'>".$ports_info['ifName']."</option>";
}

?>

</datalist>

<?php include "/opt/observium/html/nexusguard/views/common/ports_dialog.inc.php";  ?>


<?php
include_once ("/opt/observium/nexusguard/db/db_grid_function.php");
?>
<style>
.selectpicker{
     margin-right: 50px;
 }
.isp_details{
float:left;
padding-top:20px;
clear:left;
}
.intf_graph{
float:left;
padding-top:20px;
padding-left:30px;
}
.intf_stats{
float:right;
padding-left:30px;
/*margin-top:-65px;*/
}
.pull_right{
float:right;
}
</style>
<div style="padding-top:15px;padding-left:15px">
<h3>PoP & Neighbor</h3>
<?php
unset($search, $usernames_array, $popnames_array, $popdetails_array);

foreach (dbFetchColumn('SELECT  `name` FROM `nxg_isp_details`') as $username)
{
  $usernames_array[$username] = $username ;
}
krsort($usernames_array);

$search[] = array('type'    => 'multiselect',
                  'name'    => 'Neighbor',
                  'id'      => 'isp_name',
                  'width'   => '125px',
                  'value'   => $vars['isp_name'],
                  'values'  => $usernames_array);


foreach (dbFetchRows('SELECT `id`, `pop_name` FROM `nxg_pop_details`') as $popdetails)
{
  $popid = $popdetails['id'] ;
  $popdetails_array[$popid] = $popdetails['pop_name'] ;
}

$search[] = array('type'    => 'multiselect',
                  'name'    => 'PoP',
                  'id'      => 'pop_id',
                  'width'   => '110px',
                  //'subtext' => TRUE,
                  'value'   => $vars['pop_id'],
                  'values'  => $popdetails_array);

foreach (dbFetchRows('SELECT DISTINCT `ifName` FROM `ports`') as $portsdetails)
{
  $portsdetails_array[$portsdetails['ifName']] = $portsdetails['ifName'] ;
}

$search[] = array('type'    => 'multiselect',
                  'name'    => 'Interface',
                  'id'      => 'intf_name',
                  'width'   => '110px',
                  //'subtext' => TRUE,
                  'value'   => $vars['intf_name'],
                  'values'  => $portsdetails_array);

$scrubber_array = array('td'=>'TD','tms'=>'TMS','tdtms'=>'TD+TMS');

$search[] = array('type'    => 'multiselect',
                  'name'    => 'Scrubber',
                  'id'      => 'scrubber_type',
                  'width'   => '110px',
                  //'subtext' => TRUE,
                  'value'   => $vars['scrubber_type'],
                  'values'  => $scrubber_array);

print_search($search, 'Filter', 'search', 'pop_mgr/view=pop_isp_grid/');
// Pagination
$vars['pagination'] = TRUE;
?>
</div>
<span class="pull_right">
    <a href="/pop_mgr/view=add_pop_template/refresh=0/" class="btn" role="button">Add a PoP</a> &nbsp;&nbsp;&nbsp;
    <a href="/pop_mgr/view=add_neighbor/refresh=0/" class="btn" role="button">Add a Neighbor</a>
</span>
<table class="table table-hover table-striped table-bordered table-condensed table-rounded" style="margin-top:40px">
  <thead>

    <tr>
      <th class="state-marker"></th>
      <th>PoP</th>
      <th>Neighbor</th>
      <th></th>
      <th>Scrubbers</th>
      <th>Customers</th>
      <th>Global Filters</th>
      <th>Local Filters</th>
      <th>Customer Filters</th>

    </tr>
  </thead>

<?php

$get_pop_details= get_pop_grid_details($vars);
$all_pop_details = $get_pop_details['entries'];
foreach($all_pop_details as $pop_details)
{

    $pop_id = $pop_details['pop_id'];
	$str = '<tr>';
	$str .='<td></td>';

	if($pop_details['device_enrolment_status']==0)
        {
		$str .= "<td><a href='/pop_mgr/view=add_pop_template/pop=".$pop_details['id']."/scope=".preenrolled_edit."/refresh=0/' >".$pop_details['pop_name']."</a><br> (Pre-Enroll State)</td>";
	}
	else
        {
		 $str .= "<td><a href='/pop_mgr/view=modify_pop_template/pop=".$pop_details['id']."/scope=".postenrolled_edit."/refresh=0/' >".$pop_details['pop_name']."</a></td>";
	}
//	$str .='<td>'.$pop_details['pop_name'].'</td>';
    $vars['pop_id']=$pop_details['id'];
    $pop_isp_details = get_pop_and_isp_details($vars);
    $cnt_result = dbFetchRow("select count(*) as cnt from nxg_isp_details where pop_details_id = ?",array($pop_details['id']));
	$str .='<td><span class="pull_left"><b>'.$cnt_result['cnt'].' Provider(s)</b></span><br>';
    $isp_name = "";
    foreach($pop_isp_details as $pop_isp)
    {
        if($isp_name != $pop_isp['name'])
        {
            $offline_status = dbFetchRow('select offline from nxg_isp_details where name="'.$pop_isp['name'].'"');
            $condition ="";
            if($offline_status['offline']==1){
                $condition=" (Offline)";
            }
            $str .= "<span class='isp_details'> <a href='/pop_mgr/view=modify_neighbor/isp=".$pop_isp['isp_id']."/refresh=0/'>".$pop_isp['name']." ".$condition."</a>";
            if($pop_isp['description'])
            { 
                $str .= " [ ". $pop_isp['description']  ." ] ";
            }
            $str .= "</span>";
            $isp_name = $pop_isp['name'];

        }
        if( !empty($pop_isp['device_id']) && !empty($pop_isp['port_id']))
        {
            $str .= "<span class='isp_details'> [ <a href='/device/device=".$pop_isp['device_id']."/tab=port/port=".$pop_isp['port_id']."'>".$pop_isp['sub_interface_name']." </a>] </span>";
        }
        else
        {
            $str .= "<span class='isp_details'>[ ".$pop_isp['sub_interface_name']." ] </span>";
        }
 
        if($pop_isp['port_id'])
        {
            $pop_isp['bps_in_style']=  'color: #008C00;';
            $pop_isp['bps_out_style'] = 'color: #394182;';
            $pop_isp['pps_in_style'] = 'color: #740074;';
            $pop_isp['pps_out_style'] = 'color: #FF7400;';
            $pop_isp['graph_type'] = "port_bits";
            $str .= "<span class='intf_graph'>";
            $str .= generate_port_link($pop_isp, "<img src='/graph.php?type=port_bits&amp;id=".$pop_isp['port_id']."&amp;from=".$config['time']['day']."&amp;to=".$config['time']['now']."&amp;width=100&amp;height=20&amp;legend=no' alt=\"\" />")."<br>";
            $pop_isp['graph_type'] = "port_upkts";
            $str .= generate_port_link($pop_isp, "<img src='/graph.php?type=port_upkts&amp;id=".$pop_isp['port_id']."&amp;from=".$config['time']['day']."&amp;to=".$config['time']['now']."&amp;width=100&amp;height=20&amp;legend=no' alt=\"\" />")."<br>";
            $pop_isp['graph_type'] = "port_errors";
            $str .= generate_port_link($pop_isp, "<img src='/graph.php?type=port_errors&amp;id=".$pop_isp['port_id']."&amp;from=".$config['time']['day']."&amp;to=".$config['time']['now']."&amp;width=100&amp;height=20&amp;legend=no' alt=\"\" />")."</span><span class='intf_stats'>";
            $str .= '<i class="icon-circle-arrow-down" style="'.$pop_isp['bps_in_style']. '"></i> <span class="small" style="'.$pop_isp['bps_in_style']. '">' . formatRates($pop_isp['in_rate']) . '</span><br />';
            $str .= '<i class="icon-circle-arrow-up"   style="'.$pop_isp['bps_out_style'].'"></i> <span class="small" style="'.$pop_isp['bps_out_style'].'">' . formatRates($pop_isp['out_rate']). '</span><br />';
            $str .= '<i class="icon-circle-arrow-down" style="'.$pop_isp['pps_in_style']. '"></i> <span class="small" style="'.$pop_isp['pps_in_style']. '">' . format_bi($pop_isp['ifInUcastPkts_rate']). 'pps</span><br />';
            $str .= '<i class="icon-circle-arrow-up"   style="'.$pop_isp['pps_out_style'].'"></i> <span class="small" style="'.$pop_isp['pps_out_style'].'">' . format_bi($pop_isp['ifOutUcastPkts_rate']).'pps</span></span>';
        }
        $str .= "<br>";
    }
	$str .='</td>';
	$str .='<td></td>';
    $pop_scrubber_details = get_pop_and_processors_details($vars);
	$str .='<td> <span class="pull_left"><b>'.count($pop_scrubber_details).' Scrubbers & Proxies</b></span><br>';
    foreach($pop_scrubber_details as $pop_scrubber)
    {
        if($pop_scrubber['scrubber_type']=='custom')
        {
            if( !empty($pop_scrubber['device_id']) && !empty($pop_scrubber['port_id']))
            {
                $str .= "<span class='isp_details'>".$pop_scrubber['scrubber_custom_name']." [  <a href='/device/device=".$pop_scrubber['device_id']."/tab=port/port=".$pop_scrubber['port_id']."'>".$pop_scrubber['sub_interface_name']."</a> ]</span>";
            }
            else
            {
                $str .= "<span class='isp_details'>".$pop_scrubber['scrubber_custom_name']." [  ".$pop_scrubber['sub_interface_name']." ]</span>";
            }
        }
        else
        {
            if(!empty($pop_scrubber['device_id']) && !empty($pop_scrubber['port_id']))
            {
                $str .= "<span class='isp_details'>".strtoupper($pop_scrubber['scrubber_type'])." [ <a href='/device/device=".$pop_scrubber['device_id']."/tab=port/port=".$pop_scrubber['port_id']."'>".$pop_scrubber['sub_interface_name']."</a> ]</span>";
            }
            else
            {
                $str .= "<span class='isp_details'>".strtoupper($pop_scrubber['scrubber_type'])." [ ".$pop_scrubber['sub_interface_name']." ]</span>";
            }
        }
        if($pop_scrubber['port_id'])
        {
            $pop_scrubber['bps_in_style']=  'color: #008C00;';
            $pop_scrubber['bps_out_style'] = 'color: #394182;';
            $pop_scrubber['pps_in_style'] = 'color: #740074;';
            $pop_scrubber['pps_out_style'] = 'color: #FF7400;';
            $pop_scrubber['graph_type'] = "port_bits";

            $str .= "<span class='intf_graph'>";
            $str .= generate_port_link($pop_scrubber, "<img src='/graph.php?type=port_bits&amp;id=".$pop_scrubber['port_id']."&amp;from=".$config['time']['day']."&amp;to=".$config['time']['now']."&amp;width=100&amp;height=20&amp;legend=no' alt=\"\" />")."<br>";
            $pop_scrubber['graph_type'] = "port_upkts";
            $str .= generate_port_link($pop_scrubber, "<img src='/graph.php?type=port_upkts&amp;id=".$pop_scrubber['port_id']."&amp;from=".$config['time']['day']."&amp;to=".$config['time']['now']."&amp;width=100&amp;height=20&amp;legend=no' alt=\"\" />")."<br>";
            $pop_scrubber['graph_type'] = "port_errors";
            $str .= generate_port_link($pop_scrubber, "<img src='/graph.php?type=port_errors&amp;id=".$pop_scrubber['port_id']."&amp;from=".$config['time']['day']."&amp;to=".$config['time']['now']."&amp;width=100&amp;height=20&amp;legend=no' alt=\"\" />")."</span><span class='intf_stats'>";
            $str .= '<i class="icon-circle-arrow-down" style="'.$pop_scrubber['bps_in_style']. '"></i> <span class="small" style="'.$pop_scrubber['bps_in_style']. '">' . formatRates($pop_scrubber['in_rate']) . '</span><br />';
            $str .= '<i class="icon-circle-arrow-up"   style="'.$pop_scrubber['bps_out_style'].'"></i> <span class="small" style="'.$pop_scrubber['bps_out_style'].'">' . formatRates($pop_scrubber['out_rate']). '</span><br />';
            $str .= '<i class="icon-circle-arrow-down" style="'.$pop_scrubber['pps_in_style']. '"></i> <span class="small" style="'.$pop_scrubber['pps_in_style']. '">' . format_bi($pop_scrubber['ifInUcastPkts_rate']). 'pps</span><br />';
            $str .= '<i class="icon-circle-arrow-up"   style="'.$pop_scrubber['pps_out_style'].'"></i> <span class="small" style="'.$pop_scrubber['pps_out_style'].'">' . format_bi($pop_scrubber['ifOutUcastPkts_rate']).'pps</span></span>';
        }
        $str .= '<br><br>';
    }

   $cust_name= dbFetchRow('select * from nxg_customer where customer_id='.$pop_details['cust_id'].'');
   $global_type = dbFetchRow('select count(*) as  count from nxg_traffic_filter where pop_id='.$pop_details['id'].' and filter_type="global"');
   $local_type = dbFetchRow('select count(*) as count from nxg_traffic_filter where pop_id='.$pop_details['id'].' and filter_type="local"');
   $customer_type = dbFetchRow('select count(*) as count from nxg_traffic_filter where pop_id='.$pop_details['id'].' and filter_type="customer" and customer="'.$cust_name['name'].'"');

    $customers_count =dbFetchRow('select count(name) as count from nxg_customer left join nxg_customer_conn on nxg_customer.customer_id = nxg_customer_conn.cust_id where pop_id='.$pop_details['id'].'');
    
	$str .='</td>';
	$str .='<td><a href="/pop_mgr/view=customer_grid/refresh=0/pop_id='.$pop_details['id'].'">';
    $str .= $customers_count['count'];
	$str .='</a></td>';
	$str .='<td><a href="/pop_mgr/view=traffic_filter/refresh=0/filter_type=global/pop_id='.$pop_details['id'].'"">';
    $str .= $global_type['count'];
	$str .='</a></td>';
	$str .='<td><a href="/pop_mgr/view=traffic_filter/refresh=0/filter_type=local/pop_id='.$pop_details['id'].'"">';
    $str .= $local_type['count'];
	$str .='</a></td>';
	$str .='<td><a href="/pop_mgr/view=traffic_filter/refresh=0/filter_type=customer/cust_id='.$pop_details['cust_id'].'/pop_id='.$pop_details['id'].'"">';
    $str .= $customer_type['count'];
	$str .='</a></td>';
	$str .='</tr>';
    echo $str;
}
?>
</table>
</div>


<?php
include_once("/opt/observium/includes/defaults.inc.php");
include_once("/opt/observium/html/nexusguard/api/common/common.inc.php");
include_once("/opt/observium/config.php");
include_once("/opt/observium/includes/definitions.inc.php");
include_once("/opt/observium/nexusguard/validator/validate_pop.php");
include_once("/opt/observium/nexusguard/config/pop_config.php");
include_once("/opt/observium/nexusguard/db/db_pop_functions.php");
include_once("/opt/observium/nexusgaurd/logger/logger.php");
$enroll_device_status=0;
$pop_data = get_input_json();
	$errormsg=validate_add_pop($pop_data);
    
	if(!empty($errormsg))
	{
        $output= generate_error_response('validation_error',"Validation Errors",$errormsg);
	}
    else
    {

        $array=add_pop_template($pop_data);	
        $status = $array['status'];
        $pop_id = $array['pop_id'];
        dbDelete('nxg_pop_interface_ip_mgmt','pop_id = ?' ,array($pop_id));
        $pop_config = generate_new_pop_config($pop_data, $pop_id);

        $output= generate_success_response("PoP details saved to database successfully."," PoP details saved to database successfully.");
    }

    echo json_encode($output);

?>

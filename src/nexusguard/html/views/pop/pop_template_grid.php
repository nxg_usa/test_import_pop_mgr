<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<style type="text/css" title="currentStyle">
	tbody tr{
		height: 95px;
		}
	#example_paginate {
			float: right;
			padding: 15px;
			}

	#example_paginate a {
			padding-left: 15px;
			}

                </style>
		
		<!--
			loading neccessary jQuery and Plugin files
		-->		
<script type="text/javascript" language="javascript" src="data_grids/jq_data_table_js/jquery.js"></script>
<script type="text/javascript" language="javascript" src="data_grids/jq_data_table_js/datatables.js"></script>
<script type="text/javascript" charset="utf-8">
			/*
				jQuery document ready
			*/
$(document).ready(function()
{
				/*
					initialize plugin
				*/
$('#example').dataTable(
{
	"bJQueryUI": true,
	"sPaginationType": "full_numbers",
	"bInfo": "false"

});
});
</script>
</head>
<body id="dt_example">
<div id="container">
<div id="demo">
<table id="example" class= "table table-hover table-striped table-bordered table-condensed table-rounded"> 
	<thead>
	<tr>
		<th>Edge Router Management IP</th>
		<th>Pop Manager IP</th>
		<th>NXG-App Anycast IP</th>
		<th>Interface to Processor</th>
		<th>Traffic</th>
        </tr>
	</thead>
<?php
$all_pop_template=dbFetchRows("select * from pop_template");
echo"<tbody>";
foreach($all_pop_template as $pop_template)
{
    $str .= '<tr>';
    $str .= '<td>'.$pop_template['edge_router_management_ip'].'</td>';
    $str .= '<td>'.$pop_template['pop_manager_ip'].'</td>';
    $str .= '<td>'.$pop_template['nxg_anycast_ip'].'</td>';
    $all_pop_interfcaces=dbFetchRows("select * from nxg_router_interface_mapping where `pop_template_id` = ? ", array($pop_template['id']));
    $str .= '<td>';
    foreach($all_pop_interfcaces  as $pop_interface )
    {

       $str .= 'Interface Name:'. $pop_interface['device_interface_name']. ' Vlan Name:'.$pop_interface['vlan_name'].' Vlan Number:'.$pop_interface['vlan_number'].' Type:'.strtoupper($pop_interface['scrubber_type']);
        if($pop_interface['scrubber_type']=="custom")
        {
            $str .= ' Name:'. $pop_interface['scrubber_custom_name'];
        }
	$str .= '<br>';
    }
    $str .= '</td>';
    $str .='<td><img src="nexusguard/img/graph.png" ><br><i class="icon-circle-arrow-down" style="color: #008C00;"></i><span class="small" style="color: #008C00;">85.9kbps</span><br><i class="icon-circle-arrow-up" style="color: #394182;"></i> <span class="small" style="color: #394182;">85.9kbps</span><br><i class="icon-circle-arrow-down" style="color: #740074;"></i> <span class="small" style="color: #740074;">55pps</span><br><i class="icon-circle-arrow-up" style="color: #FF7400;"></i> <span class="small" style="color: #FF7400;">54pps</span></td>';
 $str .= '</tr>';
    echo $str;
}
echo"</tbody></table>";
?>
</div>
</div>
<br/><br/>
</body>
</html>

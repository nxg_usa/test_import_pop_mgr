<?php
include_once("/opt/observium/nexusguard/db/db_pop_functions.php");
include_once("/opt/observium/html/nexusguard/api/common/common.inc.php");
$output=array();

$pop_data = get_input_json();

$isp_name=get_isp_name($pop_data['pop_id']);

if(empty($isp_name))
{
    $decommission_status=decomission_pop($pop_data['pop_id']);
    if($decommission_status==0)
    {
        $successmsg="POP has been deleted from POP Manager. However Device is available is Observium, Rancid, NfSen.";
        $output['msg'] = $successmsg;
    }
    else
    {
        $errormsg="Error while decommissioning PoP";
        $output['errorcode'] = "decommissionfailed";
        $output['msg'] = $errormsg;
    }
}
else
{

    $errormsg="Cannot delete PoP,Please delete the ISP's associated with this PoP";
    $output['errorcode'] = "ispexists";
    $output['msg'] = $errormsg;

}
echo json_encode($output);

?>



<div id="inprogress" style="display:none;"> <img src="nexusguard/img/ajax-loader.gif">&nbsp;&nbsp;&nbsp;Processing request...</div>

 <form id="add_config_form">

    <div class="alert alert-error" id="error_wrap" style="display:none">
        <div class="pull-left" style="padding:0 5px 0 0"><i class="oicon-exclamation-red"></i></div>
        <div id='errormsg'></div>
    </div>

    <div class="alert alert-success" id="success_wrap" style="display:none">
        <button type="button" class="close" data-dismiss="alert"></button>
        <div id="successmsg" ></div>
    </div>

    <div style="display:none" id="add_config" class="modal-body">
        <span id="add_commit_check"></span>
    </div>

    <div id="commit_form_action" style="display:none"  class="form-actions">
        <input type="hidden" id="json_file" name="json_file" value="" />
        <input type="button" class="btn btn-primary" id="applyconfig" name="Apply Config" value="Apply Config" />
    </div>
</form>


<div id="portsModal" class="modal fade" role="dialog" style="background-color: transparent;width: auto; margin-left: 0px; top: 0px;bottom: 0px; left: 0px; right: 0px; display:none; border-width: 0px;" >
  <div class="modal-dialog" id="pop_modal"  style="position: relative; margin: 30px auto; width: 1200px; background-color: white;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Ports</h4>
      </div>
      <div class="modal-body" id="port_table">
      </div>
      <div class="modal-footer">
        
<!--        <button id="submit" class="btn btn-primary" value="Submit" name="submit" type="submit">Submit</button> -->
        <button type="button" class="btn btn-primary" data-dismiss="modal" id="close_btn" value="close">Close</button>
      </div>
    </div>

  </div>
</div>

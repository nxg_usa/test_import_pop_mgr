var global_form_data = [];
var global_button_array;

function parse_form(form_info)
{
     var data ={};
     var elem = document.getElementById(form_info).elements;
        for(var i = 0; i < elem.length; i++)
        {
         /*   data[ elem[i].name]= elem[i].value;
            delete data["undefined"];*/
        if(elem[i].type=="radio" && !elem[i].checked)
        {
            continue;
        }
        if(elem[i].type=="select-one" && (elem[i].value==""))
        {
            continue;
        }
        if (data[elem[i].name] !== undefined) {
            if (!data[elem[i].name].push) {
                data[elem[i].name] = [data[elem[i].name]];
            }
            if(elem[i].type=="select-multiple")
            {
                var selected_values = {};
                selected_values[0] = [];
                var options =  elem[i].options;
                options = document.getElementById(elem[i].id).options;
                for(var j=0;j<options.length;j++)
                {
                    if(options[j].selected)
                    {
                        if(options[j].value!="")
                            selected_values[0].push(options[j].value);
                    }
                }
                if(selected_values[0].length> 0)
                    data[elem[i].name].push(selected_values || ''); 
            }
            else
                data[elem[i].name].push(elem[i].value || '');
        } else {
            if(elem[i].type=="select-multiple")
            {
                var selected_values = {};
                selected_values[0] = [];
                var options =  elem[i].options;
                options = document.getElementById(elem[i].id).options;
                for(var j=0;j<options.length;j++)
                {
                    if(options[j].selected)
                    {
                        selected_values[0].push(options[j].value);
                    }
                }
               data[elem[i].name] = selected_values || '';
            }
            else
                data[elem[i].name] = elem[i].value || '';
        }
        }
    return data;
}
function commit_check(form_id,url)
{
    var form_data = parse_form(form_id);
    var form_json = JSON.stringify(form_data);
    $("#inprogress").show();
    $("#error_wrap").hide();
    $("#success_wrap").hide();
    $('#commit_form_action').hide();
    $("#add_config").hide();

    $.ajax({ url: url,
            data: form_json,
            type: 'post',
            success: function(output) {
                
                                    $("#inprogress").hide();
                                    outputjson = JSON.parse(output);

                                    if(outputjson.error_code)
                                    {
                                         $('#errormsg').html(outputjson.message)
                                         $("#error_wrap").show()
                                    }
                                    else
                                    {
                                        $("#success_wrap").show();
                                         $('#successmsg').html(outputjson.message);
                                        $('#commit_form_action').show();
                                    }

                                    $("#add_commit_check").html("<pre>"+outputjson.output+"</pre>");
                                    $("#add_config").show();
                                    $("#json_file").val(outputjson.json_file);
                     }
           });

}
function apply_config(form_id,url,button_arrray)
{
    global_button_array = button_arrray;
    var form_data = parse_form(form_id);
    form_data["action"]="commit";
    form_data["json_file"]=$("#json_file").val();
    var form_json = JSON.stringify(form_data);

    $("#inprogress").show();
    $("#error_wrap").hide();
    $("#success_wrap").hide();
    $('#commit_form_action').hide();
    $("#add_config").hide();

    $.ajax({ url: url,
            data: form_json,
            type: 'post',
            success: function(output) {

                                    $("#inprogress").hide();
                                    outputjson = JSON.parse(output);

                                    if(outputjson.error_code)
                                    {
                                         $('#errormsg').html(outputjson.message)
                                         $("#error_wrap").show()
                                    }
                                    else
                                    {
                                        $("#success_wrap").show();
                                        $('#successmsg').html(outputjson.message);
                                        remove_buttons();
                                       // $('#commit_form_action').show();
                                    }

                                    $("#add_commit_check").html("<pre>"+outputjson.output+"</pre>");
                                    $("#add_config").show();
                                    $("#json_file").val(outputjson.json_file);
                     }
           });
}


function commit_check2(data_array,button_arrray)
{
    global_button_array = button_arrray;
    $("#result_panel").html("");
    for ( i = 0; i < data_array.length; i++) {

        req = data_array[i];

        url = req["url"];
        form_data =  req["data"];

        form_data["request_id"]=i;
        global_form_data[i] = req; // store it so that we can access it when apply is clicked
        form_json = JSON.stringify(form_data);
        entity = req["entity_name"];
        $("#result_panel").append('<div class="panel panel-default">            \
                                            <div class="panel-heading">         \
                                              <h4 class="panel-title">          \
                                                <a data-toggle="collapse" href="#collapse'+i+'">Request '+i+'('+entity+')</a>  <div id="inprogress'+i+'" style=""> <img src="nexusguard/img/ajax-loader.gif">&nbsp;&nbsp;&nbsp;Processing request...</div>  \
                                              </h4>                             \
                                            </div>                              \
                                            <div id="collapse'+i+'" class="panel-collapse collapse">            \
                                                <div class="panel-body">        \
                                                    <form id="add_config_form"> \
                                                    <div class="alert alert-error" id="error_wrap'+i+'" style="display:none">    \
                                                        <div class="pull-left" style="padding:0 5px 0 0"><i class="oicon-exclamation-red"></i></div>    \
                                                        <div id="errormsg'+i+'"></div>   \
                                                    </div>    \
                                                    <div class="alert alert-success" id="success_wrap'+i+'" style="display:none">   \
                                                        <button type="button" class="close" data-dismiss="alert"></button>      \
                                                        <div id="successmsg'+i+'" ></div>        \
                                                    </div>          \
                                                    <div style="display:none" id="add_config'+i+'" class="modal-body">   \
                                                        <span id="add_commit_check'+i+'"></span> \
                                                    </div>  \
                                                    <div id="commit_form_action'+i+'" style="display:none"  class="form-actions">    \
                                                        <input type="hidden" id="json_file'+i+'" name="json_file" value="" />    \
                                                        <input type="button" class="btn btn-primary" id="applyconfig'+i+'" request_id="'+i+'" name="Apply Config" value="Apply Config" />   \
                                                    </div>                          \
                                                    </form>                         \
                                                </div>                              \
                                                <div class="panel-footer"></div>    \
                                           </div>                                   \
                                     </div>');
        $.ajax({ url: url,
            data: form_json,
            type: 'post',
            success: function(output) {
            outputjson = JSON.parse(output);
            id=outputjson.request_id;
            req_data = global_form_data[id];
            action = req_data.data.action;
            $("#inprogress"+id).hide();
            if(outputjson.error_code)
            {
                $('#errormsg'+id).html(outputjson.message)
                $("#error_wrap"+id).show()
            }
            else
            {
                $("#success_wrap"+id).show();
                $("#successmsg"+id).html(outputjson.message);
                $("#json_file"+id).val(outputjson.json_file);
                if(action == "commit_check")
                    $("#commit_form_action"+id).show();
                else
                    remove_buttons();//done with the actions so remove all buttons
            }
            
            $("#add_commit_check"+id).html("<pre>"+outputjson.output+"</pre>");
            $("#add_config"+id).show();
            },
            error:function(data){
                alert("Error processing the request.");     
            }
    });

        $("#applyconfig"+i).unbind("click")
        $("#applyconfig"+i).click(function (){

                rqid=$(this).attr("request_id");
                req_data = global_form_data[rqid];
                apply_form_data = req_data["data"];
                apply_url =  req_data["apply_url"];
                apply_form_data["action"]="commit";
                apply_form_data["json_file"]=$("#json_file"+rqid).val();
                var apply_form_json = JSON.stringify(apply_form_data);

                $("#inprogress"+rqid).show();
                $("#error_wrap"+rqid).hide();
                $("#success_wrap"+rqid).hide();
                $('#commit_form_action'+rqid).hide();
                $("#add_config"+rqid).hide();

                $.ajax({ url: apply_url,
                    data: apply_form_json,
                    type: 'post',
                    success: function(output) {
                                outputjson = JSON.parse(output);
                                id=outputjson.request_id;

                                $("#inprogress"+id).hide();
                                if(outputjson.error_code)
                                {
                                    $('#errormsg'+id).html(outputjson.message)
                                    $("#error_wrap"+id).show()
                                }
                                else
                                {
                                    $("#success_wrap"+id).show();
                                    $("#successmsg"+id).html(outputjson.message);
                                    remove_buttons();//at least one apply is success so remove all buttons
                                }

                                $("#add_commit_check"+id).html("<pre>"+outputjson.output+"</pre>");
                                $("#add_config"+id).show();
                }
                });
        });

    }

    


}


function remove_buttons()
{
    for(i = 0; i < global_button_array.length;i++)
    {
        if(i == 0)
            $("#"+global_button_array[i]).val("Back");
        else
             $("#"+global_button_array[i]).hide();
    }

}




function commit_check_exabgp(form_id,url)
{
    var form_data = parse_form(form_id);
    
    var form_json = JSON.stringify(form_data);
     $("#result_panel").html("");
    $("#inprogress").show();
    $("#error_wrap").hide();
    $("#success_wrap").hide();
    $('#commit_form_action').hide();
    $("#add_config").hide(); 
    $.ajax({ url: url,
            data: form_json,
            type: 'post',
            success: function(output) {
                                    $("#inprogress").hide();
                                    outputjson = JSON.parse(output);
                                    for (i = 0; i < outputjson.output.length; i++)
                                    {
                                        var str = '<div class="panel panel-default">     <div class="panel panel-default">';
                                        str += ' <div class="panel-heading">'
                                        if ( outputjson.output[i].pop_name === undefined )
                                        {
                                        str += '<h4 class="panel-title"> <a data-toggle="collapse" href="#collapse'+i+'">'+ outputjson.output[i].message +'</a>  </h4> '
                                        }
                                        else
                                        {
                                        str += '<h4 class="panel-title"> <a data-toggle="collapse" href="#collapse'+i+'">'+ outputjson.output[i].pop_name  +'</a>  </h4> '
                                        }
                                        str += '</div>'
                                        str += '<div id="collapse'+i+'" class="panel-collapse collapse">'
                                        str += '<div class="panel-body">'
                                        if(outputjson.output[i].error_code)
                                        {
                                        str += '<div class="alert alert-error" id="error_wrap'+i+'" style="">'
                                        str += '<div class="pull-left" style="padding:0 5px 0 0"><i class="oicon-exclamation-red"></i></div>'        
                                        str += '<div id="errormsg'+i+'"><pre>'+outputjson.output[i].output +'</pre></div>'
                                        str += '</div>'
                                        $('#applyconfig').hide();

                                        }
                                        else
                                        {
                                        
                                        str += '<div class="alert alert-success" id="success_wrap'+i+'" style="">'
                                        str += '<button type="button" class="close" data-dismiss="alert"></button>'
                                        str += '<div id="successmsg'+i+'" ><pre>'+ outputjson.output[i].output +'</pre></div>'
                                        str += '</div>'
            
                                        $('#applyconfig').show();
                                        }
                                        str += '</div>'
                                        str += '<div class="panel-footer"></div>'
                                        str += '</div>'
                                        str += '</div>'
                                        $("#result_panel").append(str);


            
                                        $("#add_config").show();
                                    }
                                        var str ='<div id="commit_form_action" style="display:block  class="form-actions">';
                                        str +='<input type="hidden" id="json_file" name="json_file" value="'+outputjson.output[0].json_file+'" /> '
                                        $("#commit_form_action").show();
                                        $("#result_panel").append(str);


                     }
           });  

}





// we are not using this function in add filter
function commit_check_add_filter(form_id,url)
{
    var form_data = parse_form(form_id);

    var form_json = JSON.stringify(form_data);
     $("#result_panel").html("");
    $("#inprogress").show();
    $("#error_wrap").hide();
    $("#success_wrap").hide();
    $('#commit_form_action').hide();
    $("#add_config").hide();
    $.ajax({ url: url,
            data: form_json,
            type: 'post',
            success: function(output) {
                                    $("#inprogress").hide();
                                    outputjson = JSON.parse(output);
                                    for (i = 0; i < outputjson.output.length; i++)
                                    {
                                        var str = '<div class="panel panel-default">     <div class="panel panel-default">';
                                        str += ' <div class="panel-heading">'
                                        if ( outputjson.output[i].pop_name === undefined )
                                        {
                                        str += '<h4 class="panel-title"> <a data-toggle="collapse" href="#collapse'+i+'">'+ outputjson.output[i].message +'</a>  </h4> '
                                        }
                                        else
                                        {
                                        str += '<h4 class="panel-title"> <a data-toggle="collapse" href="#collapse'+i+'">'+ outputjson.output[i].pop_name  +'</a>  </h4> '
                                        }
                                        str += '</div>'
                                        str += '<div id="collapse'+i+'" class="panel-collapse collapse">'
                                        str += '<div class="panel-body">'
                                        if(outputjson.output[i].error_code)
                                        {
                                        str += '<div class="alert alert-error" id="error_wrap'+i+'" style="">'
                                        str += '<div class="pull-left" style="padding:0 5px 0 0"><i class="oicon-exclamation-red"></i></div>'
                                        str += '<div id="errormsg'+i+'"><pre>'+outputjson.output[i].output +'</pre></div>'
                                        str += '</div>'
                                        
                                        str += '</div>'
                                        str += '<div class="panel-footer"></div>'
                                        str += '</div>'
                                        str += '</div>'
                                        $("#result_panel").append(str);

                                        }
                                        else
                                        {
                                        str += '<div class="alert alert-success" id="success_wrap'+i+'" style="">'
                                        str += '<button type="button" class="close" data-dismiss="alert"></button>'
                                        str += '<div id="successmsg'+i+'" ><pre>'+ outputjson.output[i].output +'</pre></div>'
                                        str += '</div>' 
                                        str += '</div>'
                                        str += '<div class="panel-footer"></div>'
                                        str += '</div>'
                                        str += '</div>'
                                        $("#result_panel").append(str);
                                        $("#add_config").show();
                                        $("#commit_form_action").show();
                                        }

                                    }
                                       var str ='<div id="commit_form_action" style="display:block  class="form-actions">';
                                       str +='<input type="hidden" id="json_file" name="json_file" value="'+outputjson.output[0].json_file+'" /> '
                                        $("#result_panel").append(str);


                     }
           });

}




function apply_config_exabgp(form_id,url,button_arrray)
{
   global_button_array = button_arrray;
    var form_data = parse_form(form_id);
    form_data["action"]="commit";
    form_data["json_file"]=$("#json_file").val();
    var form_json = JSON.stringify(form_data);
    $("#result_panel").html("");

    $("#inprogress").show();
    $("#error_wrap").hide();
    $("#success_wrap").hide();
    $('#commit_form_action').hide();
    $("#add_config").hide();

    $.ajax({ url: url,
            data: form_json,
            type: 'post',
            success: function(output) {

                                    $("#inprogress").hide();
                                    outputjson = JSON.parse(output);
                                    
                                    for (i = 0; i < outputjson.output.length; i++)
                                    {
                                     var str = '<div class="panel panel-default">     <div class="panel panel-default">';
                                        str += ' <div class="panel-heading">'
                                        if(outputjson.output[i].error_code)
                                        {
                                         if (outputjson.output[i].pop_name === undefined)
                                         {
                                            str += '<h4 class="panel-title"> <a data-toggle="collapse" href="#collapse'+i+'">'+ outputjson.output[i].message + '</a>  </h4> '
                                         }
                                         else
                                         {
                                         str += '<h4 class="panel-title"> <a data-toggle="collapse" href="#collapse'+i+'">'+ outputjson.output[i].pop_name + "---> " + outputjson.output[i].message + '</a>  </h4> '
                                        }
                                        str += '</div>'
                                        str += '<div id="collapse'+i+'" class="panel-collapse collapse">'
                                        str += '<div class="panel-body">'
                                        str += '<div class="alert alert-error" id="error_wrap'+i+'" style="">'
                                        str += '<div class="pull-left" style="padding:0 5px 0 0"><i class="oicon-exclamation-red"></i></div>'
                                        str += '<div id="errormsg'+i+'"><pre>'+ outputjson.output[i].output +'<pre></div>'
                                        str += '</div>'

                                        }
                                        else
                                        {
                                          
                                        if (outputjson.output[i].pop_name === undefined)
                                         {
                                            str += '<h4 class="panel-title"> <a data-toggle="collapse" href="#collapse'+i+'">'+ outputjson.output[i].message + '</a>  </h4> '
                                         }
                                         else
                                         {
                                         str += '<h4 class="panel-title"> <a data-toggle="collapse" href="#collapse'+i+'">'+ outputjson.output[i].pop_name + "---> " + outputjson.output[i].message + '</a>  </h4> '
                                        } 
                                        str += '</div>'
                                        str += '<div id="collapse'+i+'" class="panel-collapse collapse">'
                                        str += '<div class="panel-body">'
                                        str += '<div class="alert alert-success" id="success_wrap'+i+'" style="">'
                                        str += '<button type="button" class="close" data-dismiss="alert"></button>'
                                        str += '<div id="successmsg'+i+'" ><pre>'+ outputjson.output[i].output +'</pre></div>'
                                        str += '</div>'

                                        remove_buttons();
                                        }
                                        str += '</div>'
                                        str += '<div class="panel-footer"></div>'
                                        str += '</div>'
                                        str += '</div>'
                                        $("#result_panel").append(str);
                                     //   $("#add_commit_check").html("<pre>"+outputjson.output.output+"</pre>");
                                      //  $("#add_config").show();
                                        //$("#json_file").val(outputjson.output[0].json_file);
                                    }
                     }
           });
}




function apply_config_add_filter(form_id,url)
{
    var form_data = parse_form(form_id);
    form_data["action"]="commit";
    form_data["json_file"]=$("#json_file").val();
    var form_json = JSON.stringify(form_data);
    $("#result_panel").html("");

    $("#inprogress").show();
    $("#error_wrap").hide();
    $("#success_wrap").hide();
    $('#commit_form_action').hide();
    $("#add_config").hide();

    $.ajax({ url: url,
            data: form_json,
            type: 'post',
            success: function(output) {

                                    $("#inprogress").hide();
                                    outputjson = JSON.parse(output);

                                    for (i = 0; i < outputjson.output.length; i++)
                                    {
                                     var str = '<div class="panel panel-default">     <div class="panel panel-default">';
                                        str += ' <div class="panel-heading">'
                                        if(outputjson.output[i].error_code)
                                        {
                                         if (outputjson.output[i].pop_name === undefined)
                                         {
                                            str += '<h4 class="panel-title"> <a data-toggle="collapse" href="#collapse'+i+'">'+ outputjson.output[i].message + '</a>  </h4> '
                                         }
                                         else
                                         {
                                         str += '<h4 class="panel-title"> <a data-toggle="collapse" href="#collapse'+i+'">'+ outputjson.output[i].pop_name + "---> " + outputjson.output[i].message + '</a>  </h4> '
                                        }
                                        str += '</div>'
                                        str += '<div id="collapse'+i+'" class="panel-collapse collapse">'
                                        str += '<div class="panel-body">'
                                        str += '<div class="alert alert-error" id="error_wrap'+i+'" style="">'
                                        str += '<div class="pull-left" style="padding:0 5px 0 0"><i class="oicon-exclamation-red"></i></div>'
                                        str += '<div id="errormsg'+i+'"><pre>'+ outputjson.output[i].output +'<pre></div>'
                                        str += '</div>'

                                        }
                                        else
                                        {

                                        if (outputjson.output[i].pop_name === undefined)
                                         {
                                            str += '<h4 class="panel-title"> <a data-toggle="collapse" href="#collapse'+i+'">'+ outputjson.output[i].message + '</a>  </h4> '
                                         }
                                         else
                                         {
                                         str += '<h4 class="panel-title"> <a data-toggle="collapse" href="#collapse'+i+'">'+ outputjson.output[i].pop_name + "---> " + outputjson.output[i].message + '</a>  </h4> '
                                        }
                                        str += '</div>'
                                        str += '<div id="collapse'+i+'" class="panel-collapse collapse">'
                                        str += '<div class="panel-body">'
                                        str += '<div class="alert alert-success" id="success_wrap'+i+'" style="">'
                                        str += '<button type="button" class="close" data-dismiss="alert"></button>'
                                        str += '<div id="successmsg'+i+'" ><pre>'+ outputjson.output[i].output +'</pre></div>'
                                        str += '</div>'


                                        }
                                        str += '</div>'
                                        str += '<div class="panel-footer"></div>'
                                        str += '</div>'
                                        str += '</div>'
                                        $("#result_panel").append(str);
                                    }
                     }
           });
}

function apply_config_delete_filter(form_id,url,button_arrray)
{
    global_button_array = button_arrray;
    var form_data = parse_form(form_id);
    form_data["action"]="commit";
    form_data["json_file"]=$("#json_file").val();
    var form_json = JSON.stringify(form_data);
    $("#result_panel").html("");

    $("#inprogress").show();
    $("#error_wrap").hide();
    $("#success_wrap").hide();
    $('#commit_form_action').hide();
    $("#add_config").hide();

    $.ajax({ url: url,
            data: form_json,
            type: 'post',
            success: function(output) {

                                    $("#inprogress").hide();
                                    outputjson = JSON.parse(output);

                                    for (i = 0; i < outputjson.output.length; i++)
                                    {
                                     var str = '<div class="panel panel-default">     <div class="panel panel-default">';
                                        str += ' <div class="panel-heading">'
                                        if(outputjson.output[i].error_code)
                                        {
                                         if (outputjson.output[i].pop_name === undefined)
                                         {
                                            str += '<h4 class="panel-title"> <a data-toggle="collapse" href="#collapse'+i+'">'+ outputjson.output[i].message + '</a>  </h4> '
                                         }
                                         else
                                         {
                                         str += '<h4 class="panel-title"> <a data-toggle="collapse" href="#collapse'+i+'">'+ outputjson.output[i].pop_name + "---> " + outputjson.output[i].message + '</a>  </h4> '
                                        }
                                        str += '</div>'
                                        str += '<div id="collapse'+i+'" class="panel-collapse collapse">'
                                        str += '<div class="panel-body">'
                                        str += '<div class="alert alert-error" id="error_wrap'+i+'" style="">'
                                        str += '<div class="pull-left" style="padding:0 5px 0 0"><i class="oicon-exclamation-red"></i></div>'
                                        str += '<div id="errormsg'+i+'"><pre>'+ outputjson.output[i].output +'<pre></div>'
                                        str += '</div>'
  }
                                        else
                                        {

                                        if (outputjson.output[i].pop_name === undefined)
                                         {
                                            str += '<h4 class="panel-title"> <a data-toggle="collapse" href="#collapse'+i+'">'+ outputjson.output[i].message + '</a>  </h4> '
                                         }
                                         else
                                         {
                                         str += '<h4 class="panel-title"> <a data-toggle="collapse" href="#collapse'+i+'">'+ outputjson.output[i].pop_name + "---> " + outputjson.output[i].message + '</a>  </h4> '
                                        }
                                        str += '</div>'
                                        str += '<div id="collapse'+i+'" class="panel-collapse collapse">'
                                        str += '<div class="panel-body">'
                                        str += '<div class="alert alert-success" id="success_wrap'+i+'" style="">'
                                        str += '<button type="button" class="close" data-dismiss="alert"></button>'
                                        str += '<div id="successmsg'+i+'" ><pre>'+ outputjson.output[i].output +'</pre></div>'
                                        str += '</div>'

                                    
                                            remove_buttons();
                                        }
                                        str += '</div>'
                                        str += '<div class="panel-footer"></div>'
                                        str += '</div>'
                                        str += '</div>'
                                        $("#result_panel").append(str);
                                     //   $("#add_commit_check").html("<pre>"+outputjson.output.output+"</pre>");
                                      //  $("#add_config").show();
                                        //$("#json_file").val(outputjson.output[0].json_file);
                                    }
                     }
           });
}


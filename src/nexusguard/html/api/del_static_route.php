<?php
include_once("common/common.inc.php");
include_once('/opt/observium/nexusguard/db/add_to_auditlog.php');
include_once('/opt/observium/nexusguard/config/delete_static_route.php');
include_once('/opt/observium/nexusguard/db/db_static_route_functions.php');

$del_static_route_data = get_input_json();


$diversion_id = $del_static_route_data['diversion_id'];
$comment = $del_static_route_data['comment'];
$action = $del_static_route_data['action'];
$pop_id = $del_static_route_data['pop_id'];

$pop_details = get_sinlge_pop_detail($pop_id);


$device_details = dbFetchRow("select hostname from devices where device_id = ?",array($pop_details['pop_edge_router_device_id']));


$hostname = $device_details['hostname'];
$device_password = $pop_details['password'];
$device_username = $pop_details['username'];



if($action == "commit_check")
{
    $delete_static_route_array=generate_delete_static_config($diversion_id,$pop_id);
    $output = do_ansible_commit_check_with_cmd("manual_diversion","nxg_delete_manual_diversion.sh", $hostname, $device_username, $device_password,$delete_static_route_array,$action);
    
}
else
{
    $json_filename=$del_static_route_data['json_file'];    
    $output =  do_ansible_commit("manual_diversion","nxg_delete_manual_diversion.sh", $hostname, $device_username, $device_password,$json_filename,$action);




    if($output['error_code']=="")
    {
           dbDelete('nxg_static_route', "`id` = ?", array($diversion_id));
    } 


}



echo json_encode($output);

?>


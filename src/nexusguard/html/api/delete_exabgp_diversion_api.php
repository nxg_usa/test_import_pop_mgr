<?php
include_once('/opt/observium/html/nexusguard/views/includes/common_includes.php');
include_once("common/common.inc.php");
include_once('/opt/observium/nexusguard/db/db_pop_functions.php');
include_once("/opt/observium/nexusguard/config/delete_diversion_config.php");
include_once('/opt/observium/nexusguard/var/popmgr_var.php');

global $var_popmgr_user;


$delete_exabgp_data = get_input_json();

$diversion_id = $delete_exabgp_data['unique_id'];
$diversion_type = $delete_exabgp_data['Diversion_type'];
$pop_id = $delete_exabgp_data['pop_id'];
$action = $delete_exabgp_data['action'];


$delete_diversion_config = delete_diversion_config($diversion_id,$diversion_type,$pop_id);


$local_server_string=dbFetchRow("select local_server_ip from nxg_exabgp_details where pop_details_id= ?",array($pop_id));

$global_server_string=dbFetchRow("select global_server_ip from nxg_exabgp_details where pop_details_id= ?",array($pop_id));

$pop_name=dbFetchRow("select pop_name from nxg_pop_details where id= ?",array($pop_id));




$pop_details = dbfetchRow("select * from nxg_exabgp_details where pop_details_id=$pop_id");






if ( $action == "commit_check" )
{

       $local_server_ip = (explode ('/',$local_server_string['local_server_ip']));
        $global_server_ip = (explode ('/',$global_server_string['global_server_ip'])); 
        $local_server_username = $var_popmgr_user;
        $global_server_username = $var_popmgr_user;

        $output = do_ansible_commit_check_with_cmd_for_diversion("delete_exabgp_diversion","nxg_delete_exabgp_diversion.sh",$pop_name['pop_name'],$local_server_ip[0],$local_server_username,$global_server_ip[0],$global_server_username,$delete_diversion_config,$action);

}
else
{
        $local_server_ip = (explode ('/',$local_server_string['local_server_ip']));
        $global_server_ip = (explode ('/',$global_server_string['global_server_ip']));
	$local_server_username = $var_popmgr_user;
        $global_server_username = $var_popmgr_user;

        $json_file=$delete_exabgp_data['json_file'];
$output = do_ansible_commit_for_diversion("delete_exabgp_diversion","nxg_delete_exabgp_diversion.sh",$pop_name['pop_name'],$local_server_ip[0],$local_server_username,$global_server_ip[0],$global_server_username,$json_file,$action);
if($output['error_code']=="")
{
    dbDelete('nxg_diversion_details', "`id` =?", array($diversion_id));
}
}


#$output = ansible_exabgp_remove($hostname,$device_username,$device_password);

#echo json_encode($output);
#if($output['error_code']=="")
#{
#
#    dbDelete('nxg_diversion_details', "`id` =?", array($id));
#
#}

echo json_encode($output);

?>


<?php 

include_once('/opt/observium/nexusguard/db/db_pop_functions.php');
include_once("common/common.inc.php");
include_once('/opt/observium/nexusguard/var/popmgr_var.php');
include_once('/opt/observium/nexusguard/config/add_filter_config.php');
include_once('/opt/observium/nexusguard/validator/validate_add_filter.php');
include_once('/opt/observium/nexusguard/db/db_add_filter.php');
include_once('/opt/observium/nexusguard/db/add_to_auditlog.php');
        $add_filter_data = get_input_json();
        $pop_id = $add_filter_data['pop_id'];
        $action = $add_filter_data['action'];

        if ($action == "commit_check")
        {
            $errmsg = validate_add_filter($add_filter_data);
            if($errmsg !="")
            {
                $output = generate_error_response("validation_error","Error in entered data, please fix below validation errors.",$errmsg);
                echo json_encode($output);
                exit;

            }
        }

        $pop_details = get_sinlge_pop_detail($pop_id);

        $device_details = dbFetchRow("select hostname from devices where device_id = ?",array($pop_details['pop_edge_router_device_id']));
        $hostname = $device_details['hostname'];

        $device_username = $pop_details['username'];
        $device_password = $pop_details['password'];

        $data_array = array();

$json_config= get_add_filter_config($add_filter_data);

            if($action == "commit_check")
            {
                $json_config= get_add_filter_config($add_filter_data);
                $output = do_ansible_commit_check_with_cmd("add_filter","nxg_add_filter_management.sh", $hostname, $device_username, $device_password,$json_config,$action);
            }else
            {
                $json_file = $add_filter_data['json_file'];
                $output = do_ansible_commit("add_filter","nxg_add_filter_management.sh", $hostname, $device_username, $device_password,$json_file,$action);
                
                    if($output['error_code']=="")
                    {
                        $admin_name=get_admin_name();
                        $ret = add_filter($add_filter_data,$admin_name);
                    }

            }

echo json_encode($output);
?>

<?php
include_once("common/common.inc.php");
include_once("/opt/observium/nexusguard/db/db_pop_functions.php");
include_once("/opt/observium/nexusguard/config/cust_config.php");
include_once("/opt/observium/nexusguard/validator/validate_customer_details.php");


$del_cust_conn_data = get_input_json();

$action = $del_cust_conn_data['action'];
$cust_id = $del_cust_conn_data["cust_id"];

if($action == "db")
{
// Todo Validation
delete_customer($cust_id);
$output =  generate_success_response("Deleted Customer successfully." ,"", "","");
echo json_encode($output);
exit();
}


$cust_id = $del_cust_conn_data["cust_id"];
$pop_id = $del_cust_conn_data["connection"]['pop_id'];
$conn_id = $del_cust_conn_data["connection"]['conn_id'];

$pop_details = get_sinlge_pop_detail($pop_id);


//get device details
$device_details = dbFetchRow("select hostname from devices where device_id = ?",array($pop_details['pop_edge_router_device_id']));
$hostname = $device_details['hostname'];

$device_username = $pop_details['username'];
$device_password = $pop_details['password'];


if($action == "commit_check")
{

    $errmsg =  validate_delete_customer($cust_id,$pop_id,$conn_id);
    if($errmsg !="")
    {
        $output = generate_error_response("validation_error","Error in entered data, please fix below validation errors.",$errmsg);
        echo json_encode($output);
        exit;
    }
    
    $del_cust_conn_config = get_delete_conn_config($del_cust_conn_data);
    $output = do_ansible_commit_check_with_cmd("customer","nxg_del_cust.sh", $hostname, $device_username, $device_password,$del_cust_conn_config,$action);
}
else
{
    $del_cust_conn_json_file = $del_cust_conn_data['json_file'];
    $output = do_ansible_commit("isp","nxg_del_cust.sh", $hostname, $device_username, $device_password,$del_cust_conn_json_file,$action);
    if(!isset($output["error_code"]))
    {
        //$cust_config = json_decode(file_get_contents($del_cust_conn_json_file),true);
        delete_pop_interface_ip_mgmt($conn_id);
        $ret = delete_cust_conn($conn_id);
    }
}

echo json_encode($output);

?>

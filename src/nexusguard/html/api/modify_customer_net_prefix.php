<?php
include_once("common/common.inc.php");
include_once("/opt/observium/nexusguard/validator/validate_customer_details.php");
include_once("/opt/observium/nexusguard/config/cust_config.php");
include_once("/opt/observium/nexusguard/db/db_customer_functions.php");
include_once("/opt/observium/nexusguard/validator/validate_customer_details.php");


$cust_data = get_input_json();

$action = $cust_data["action"];
$cust_id = $cust_data["cust_id"];
$prefix = $cust_data["net_srv_prefix"];

$pop_id =  $cust_data["pop_id"];
$pop_details = get_sinlge_pop_detail($pop_id);
//get device details
$device_details = dbFetchRow("select hostname from devices where device_id = ?",array($pop_details['pop_edge_router_device_id']));
$hostname = $device_details['hostname'];
$device_username = $pop_details['username'];
$device_password = $pop_details['password'];


$prefixes =  explode("\n",$prefix);
$modified_data =  get_prefix_modfied_data($cust_id,$prefixes);
if($action =="commit_check")
{
    $errmsg = validate_net_srv_prefix_modify($cust_id,$modified_data);
    if($errmsg != "")
    {
        $output = generate_error_response("validation_error","Error in entered data, please fix below validation errors.",$errmsg);
        echo json_encode($output);
        exit;
    }
    else
    {
        $customer_details = get_db_cust_details($cust_id);

        $prefix_config["prefix_data"] = $modified_data;
        $prefix_config["customer_name"] = $customer_details["name"];
        $output = do_ansible_commit_check_with_cmd("customer-prefix","nxg_modify_customer_prefix.sh", $hostname, $device_username, $device_password,$prefix_config,$action);
    }
}
else
{
    $json_file =  $cust_data["json_file"];
    $output = do_ansible_commit("customer-prefix","nxg_modify_customer_prefix.sh", $hostname, $device_username, $device_password,$json_file,$action);
    if(!isset($output["error_code"]))
    {
///    $cust_config = json_decode(file_get_contents($json_file),true);
        $count = count($modified_data["delete"]);
        for($i = 0 ; $i< $count;$i++)
             dbDelete('nxg_customer_net_service', '`cust_id` = ? and `net_srv_prefix`=?', array($cust_id,$modified_data["delete"][$i]));

        $count = count($modified_data["add"]);
        for($i = 0 ; $i< $count;$i++)
            add_nxg_net_data($cust_id,$modified_data["add"][$i]);
    }
}



echo json_encode($output);

?>

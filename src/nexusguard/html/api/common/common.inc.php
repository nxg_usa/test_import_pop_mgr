<?php

//session_start();
include_once("/opt/observium/config.php");
include_once("/opt/observium/includes/definitions.inc.php");

include_once($config['html_dir'] . "/includes/functions.inc.php");
include_once($config['html_dir'] . "/includes/authenticate.inc.php");

$JSON_DIR            = "/opt/observium/nexusguard/ansible/output/";

$logtype_common ="COMMON";
validate_session();

function validate_session()
{
        //    log_trace($logtype_common, __FILE__, __FUNCTION__, "Begin")

session_start();
  //  log_trace($logtype_common, __FILE__, __FUNCTION__, "End")
//$id=$_SESSION['user_id'];
//$username=dbFetchRow("select username from users where user_id=$id");
if (!isset($_SESSION['username'])) {
 $data=get_input_json();
 $output = generate_error_response("ccerr01","Session is not valid , please login","");
 $output['request_id'] = $data['request_id'] ;
echo json_encode($output);

 die();
}

}

function get_input_json()
{
    //log_trace($logtype_common, __FILE__, __FUNCTION__, "Begin");
   
    $data = json_decode(file_get_contents('php://input'), true);

    $_REQUEST["request_id"]= $data["request_id"];

    //log_debug($logtype_common, __FILE__, __FUNCTION__, "Input JSON Data->".$data);
    $data = trim_spaces($data);
    return $data;

   // log_trace($logtype_common, __FILE__, __FUNCTION__, "Begin")
}

function trim_spaces($data)
{
    foreach($data as $key=>$value)
    {
        if(is_array($value))
        {
            $data[$key] = trim_spaces($value);
        }
        else
        {
            $data[$key]=trim($value);
        }
    }
    return $data;
}

function do_ansible_commit_check($entity_name,$script_name,$hostname, $device_username, $device_password, $json_config,$action)
{
    global $JSON_DIR;
    $epoch_time = time();

    $json_tmp_filename = $entity_name."_".$epoch_time.".json";
    $config_tmp_filename = $entity_name."_".$epoch_time.".conf";
    $commit_check_tmp_filename = $entity_name."_".$epoch_time."_commit_check.log";
    $commit_log_tmp_filename = $entity_name."_".$epoch_time.".log";

    $json_filename = $JSON_DIR.$json_tmp_filename;
    $config_filename = $JSON_DIR.$config_tmp_filename;
    $commit_check_filename = $JSON_DIR.$commit_check_tmp_filename;
    $commit_log_filename = $JSON_DIR.$commit_log_tmp_filename;

    $json_config ['conf_file'] = $config_filename;
    $json_config ['commit_check_log'] = $commit_check_filename;
    $json_config ['commit_log'] = $commit_log_filename;

    //Write json file
    $fp = fopen($json_filename,'w');
    fwrite($fp, str_replace("\\","", json_encode($json_config)));
    fclose($fp);

    $cmd = "/bin/bash -x /opt/observium/nexusguard/ansible/script/$script_name $hostname $device_username $device_password $action $json_filename $config_filename $commit_check_filename 2>&1";
    $err = "";
    exec($cmd,$result,$status);
    $ansible_output=implode ( "\n" , $result );
    if( $status != 0 )
    {
       // $err = shell_exec("cat ".$commit_log_filename);
        $cmd = "cat ".$config_filename;
        $config_output = shell_exec($cmd);

        $ans_result = $ansible_output . "\n\n\n--------------Config----------------\n\n\n" .  $config_output;
        $output = generate_error_response("ccerr01","Error while doing commit check",$ans_result);

    }
    else
    {
        if($script_name == "nxg_add_pop.sh")
        {
            $cmd = "cat ".$config_filename;
            $commit_out = shell_exec($cmd);
        }
        else
        {
            $cmd = "cat ".$commit_check_filename;
            $commit_out = shell_exec($cmd);
        }

        $ans_result = $ansible_output . "\n\n\n--------------Commit Check----------------\n\n\n" .  $commit_out;
        $output = generate_success_response("Commit check successful",$ans_result,$json_filename,$config_filename);
    }
    
    return $output;
}


function do_ansible_commit($entity_name,$script_name,$hostname, $device_username, $device_password, $json_file,$action,$commit_comment,$pop_id)
{
        $admin_name=get_admin_name();
        $date=date('Y-m-d H:i:s');
        $commit_status="attempt";
        $device_id=dbfetchRow(" select device_id from devices where hostname= ?",array($hostname));
        $pop_id=dbfetchRow(" select id from nxg_pop_details where pop_edge_router_device_id = ?",array($device_id['device_id']));
        dbInsert(array('audit_time'=>$date,'user_name'=>$admin_name,'pop_id'=>$pop_id,'module_name'=>$entity_name,'commit_comment'=>$commit_comment,'commit_log'=>$commit_out,'commit_status'=>$commit_status),'nxg_auditlog');    
        $comment=dbfetchRow("select id,commit_comment from nxg_auditlog where audit_time = ?",array($date));
        $comment_to_script = "##".$comment['id']."##_commiting_changes_for_module_$entity_name"."_in_POP_$pop_id[id]";
    $cmd = "/bin/bash -x /opt/observium/nexusguard/ansible/script/$script_name $hostname $device_username $device_password $action $json_file $config_filename $commit_check_filename '$comment_to_script' 2>&1";
    $err = "";

    $config = json_decode(file_get_contents($json_file),true);
    $config_filename = $config['config_file'];
    $commit_log_file = $config['commit_log'];
    $commit_check_log = $config['commit_check_log'];
    $read = "cat ".$commit_check_log;
    $config_diff_file = shell_exec($read);
    exec($cmd,$result,$status);
    $ansible_output=implode ( "\n" , $result );
    if( $status != 0 )
    {
        // $err = shell_exec("cat ".$commit_log_filename);
        $cmd = "cat ".$commit_log_file;
        $config_output = shell_exec($cmd);

        $ans_result =$ansible_output . "\n\n\n--------------Config----------------\n\n\n" .  $config_output;
        $output = generate_error_response("ccerr01","Error while doing commit",$ans_result);
        
        $commit_status="Fail";
         dbUpdate(array('audit_time'=>$date,'user_name'=>$admin_name,'pop_id'=>$pop_id,'module_name'=>$entity_name,'commit_comment'=>$comment_to_script,'commit_log'=>$config_diff_file,'commit_status'=>$commit_status),'nxg_auditlog','`id`=?',array($comment['id']));


    }
    else
    {
        $cmd = "cat ".$commit_log_file;
        $commit_out = shell_exec($cmd);

        $ans_result =  $ansible_output . "\n\n\n--------------Commit Output----------------\n\n\n" .  $commit_out;
        $output = generate_success_response("Configuration applied successfully",$ans_result,$json_filename,$config_filename);
        $commit_status="Success";
        dbUpdate(array('audit_time'=>$date,'user_name'=>$admin_name,'pop_id'=>$pop_id,'module_name'=>$entity_name,'commit_comment'=>$comment_to_script,'commit_log'=>$config_diff_file,'commit_status'=>$commit_status),'nxg_auditlog','`id`=?',array($comment['id'])); 
    }

    return $output;


}

function do_ansible_commit_check_with_cmd($entity_name,$script_name,$hostname, $device_username, $device_password, $json_config,$action)
{
    global $JSON_DIR;
    $epoch_time = time();

    $json_tmp_filename = $entity_name."_".$epoch_time.".json";
    $config_tmp_filename = $entity_name."_".$epoch_time.".set";
    $commit_check_tmp_filename = $entity_name."_".$epoch_time."_commit_check.log";
    $commit_log_tmp_filename = $entity_name."_".$epoch_time.".log";

    $json_filename = $JSON_DIR.$json_tmp_filename;
    $config_filename = $JSON_DIR.$config_tmp_filename;
    $commit_check_filename = $JSON_DIR.$commit_check_tmp_filename;
    $commit_log_filename = $JSON_DIR.$commit_log_tmp_filename;

    $json_config ['conf_file'] = $config_filename;
    $json_config ['commit_check_log'] = $commit_check_filename;
    $json_config ['commit_log'] = $commit_log_filename;

    //Write json file
    $fp = fopen($json_filename,'w');
    fwrite($fp, str_replace("\\","", json_encode($json_config)));
    fclose($fp);

    $cmd = "/bin/bash -x /opt/observium/nexusguard/ansible/script/$script_name $hostname $device_username $device_password $action $json_filename $config_filename $commit_check_filename 2>&1";
    $err = "";
    exec($cmd,$result,$status);
    $ansible_output=implode ( "\n" , $result );
    if( $status != 0 )
    {
       // $err = shell_exec("cat ".$commit_log_filename);
        $cmd = "cat ".$config_filename;
        $config_output = shell_exec($cmd);

        $ans_result = $ansible_output . "\n\n\n--------------Config----------------\n\n\n" .  $config_output;
        $output = generate_error_response("ccerr01","Error while doing commit check",$ans_result);
    }
    else
    {
        $cmd = "cat ".$commit_check_filename;
        $commit_out = shell_exec($cmd);

        $ans_result = $ansible_output . "\n\n\n--------------Commit Check----------------\n\n\n" .  $commit_out;
        $output = generate_success_response("Commit check successful",$ans_result,$json_filename,$config_filename);
    }

    return $output;
}

function generate_error_response($error_code, $message,$output)
{
    $error["request_id"] =  $_REQUEST["request_id"];
    $error["error_code"] = $error_code;
    $error["message"] = $message;
    $error["output"] = $output;

    return $error;
}

function generate_success_response($message ,$output, $json_file,$config_filename)
{
    $success["request_id"] =  $_REQUEST["request_id"];
    $success['message'] = $message;
    $success["output"] = $output;
    $success["json_file"] = $json_file;
    $success["config_file"] = $config_filename;
    return $success;
}

function ansible_exabgp_remove($hostname, $device_username, $device_password)
{
    
    $cmd = "/bin/bash  /opt/observium/nexusguard/ansible/script/nxg_delete_exabgp_diversion.sh $hostname $device_username $device_password 2>&1";
     $err = "";
    exec($cmd,$result,$status);
    $ansible_output=implode ( "\n" , $result );
     if( $status != 0 )
    {
        $output = generate_error_response("ccerr01","Error while doing commit check",$ans_result);
    }
    else
    {
        $cmd = "cat ".$commit_check_filename;
        $commit_out = shell_exec($cmd);

        $ans_result = $ansible_output . "\n\n\n--------------Commit Check----------------\n\n\n" .  $commit_out;
        $output = generate_success_response("Deleted successful",$ans_result,$json_filename,$config_filename);
    }

    return $output;


}

function do_ansible_commit_check_with_cmd_for_diversion($entity_name,$script_name,$pop_name,$local_server_ip,$local_server_username,$global_server_ip,$global_server_username,$json_config,$action)
{
    global $JSON_DIR;
    $epoch_time = time();

    $json_tmp_filename = $entity_name."_".$epoch_time.".json";
    $local_config_tmp_filename = "local_".$entity_name."_".$epoch_time.".conf";
    $global_config_tmp_filename = "global_".$entity_name."_".$epoch_time.".conf";
    $commit_check_tmp_filename = $entity_name."_".$epoch_time."_commit_check.log";
    $commit_log_tmp_filename = $entity_name."_".$epoch_time.".log";

    $json_filename = $JSON_DIR.$json_tmp_filename;
    $local_config_filename = $JSON_DIR.$local_config_tmp_filename;
    $global_config_filename = $JSON_DIR.$global_config_tmp_filename;
    $commit_check_filename = $JSON_DIR.$commit_check_tmp_filename;
    $commit_log_filename = $JSON_DIR.$commit_log_tmp_filename;

    $json_config ['local_conf_file'] = $local_config_filename;
    $json_config ['global_conf_file'] = $global_config_filename;
    $json_config ['commit_check_log'] = $commit_check_filename;
    $json_config ['commit_log'] = $commit_log_filename;

    //Write json file
    $fp = fopen($json_filename,'w');
    fwrite($fp, str_replace("\\","", json_encode($json_config)));
    fclose($fp);
    $cmd = "/bin/bash  /opt/observium/nexusguard/ansible/script/$script_name $pop_name $local_server_ip $local_server_username $global_server_ip $global_server_username $action $json_filename $local_config_filename $global_config_filename $commit_check_filename 2>&1";
    $err = "";
    exec($cmd,$result,$status);
    $ansible_output=implode ( "\n" , $result );
    if( $status != 0 )
    {
       // $err = shell_exec("cat ".$commit_log_filename);
        $cmd = "cat ".$config_filename;
        $config_output = shell_exec($cmd);

        $ans_result = $ansible_output . "\n\n\n--------------Config----------------\n\n\n" .  $config_output;
        $output = generate_error_response("ccerr01","Error while doing commit check , No diff found",$ans_result);
        $output['pop_name'] = $pop_name;
    }
    else
    {
        $cmd_local = "cat ".$local_config_filename;
        $commit_out = "##### Local config File ####\n";
        $commit_out .= shell_exec($cmd_local);
        $cmd_global = "cat ".$global_config_filename;
        $commit_out .= "#### Global config File ####\n";
        $commit_out .= shell_exec($cmd_global);
        $ans_result = $ansible_output . "\n\n\n--------------Commit Check----------------\n\n\n" .  $commit_out;
       $output = generate_success_response("Commit check successful",$ans_result,$json_filename,$config_filename);
        $output['pop_name'] = $pop_name;
    }

    return $output;
}

function do_ansible_commit_for_diversion($entity_name,$script_name,$pop_name,$local_server_ip,$local_server_username,$global_server_ip,$global_server_username,$json_file,$action)
{

        $admin_name=get_admin_name();
        $date=date('Y-m-d H:i:s');
        $commit_status="attempt";
        $device_id=dbfetchRow(" select device_id from devices where hostname= ?",array($hostname));
        $pop_id=dbfetchRow(" select id from nxg_pop_details where pop_edge_router_device_id = ?",array($device_id['device_id']));
        dbInsert(array('audit_time'=>$date,'user_name'=>$admin_name,'pop_id'=>$pop_id,'module_name'=>$entity_name,'commit_comment'=>$commit_comment,'commit_log'=>$commit_out,'commit_status'=>$commit_status),'nxg_auditlog');
        $comment=dbfetchRow("select id,commit_comment from nxg_auditlog where audit_time = ?",array($date));
        $comment_to_script = "##".$comment['id']."##_commiting_changes_for_module_$entity_name"."_in_POP_$pop_id[id]";


    $cmd = "/bin/bash /opt/observium/nexusguard/ansible/script/$script_name $pop_name $local_server_ip $local_server_username $global_server_ip $global_server_username $action $json_file 2>&1";
    $err = "";

    $config = json_decode(file_get_contents($json_file),true);
    $config_filename = $config['config_file'];
    $commit_log_file = $config['commit_log'];
    
    exec($cmd,$result,$status);
    $ansible_output=implode ( "\n" , $result );
    if( $status != 0 )
    {
        // $err = shell_exec("cat ".$commit_log_filename);
        $unique_filename="/opt/observium/nexusguard/ansible/output/ADD_NXG_LATEST_LOCAL_CONF.cfg";
        $cmd = "cat ".$unique_filename;
        $config_output = shell_exec($cmd);

        $ans_result =$ansible_output . "\n\n\n--------------Config----------------\n\n\n" .  $config_output;
        $output = generate_error_response("ccerr01","Error while doing commit",$ans_result);
        $output['pop_name'] = $pop_name;
        $commit_status="Fail";
         dbUpdate(array('audit_time'=>$date,'user_name'=>$admin_name,'pop_id'=>$pop_id,'module_name'=>$entity_name,'commit_comment'=>$comment_to_script,'commit_log'=>$config_output,'commit_status'=>$commit_status),'nxg_auditlog','`id`=?',array($comment['id']));

    }
    else
    {
        $unique_filename_local="/opt/observium/nexusguard/ansible/output/$pop_name'_ADD_NXG_LATEST_LOCAL_CONF.cfg'";
        $unique_filename_global="/opt/observium/nexusguard/ansible/output/$pop_name'_ADD_NXG_LATEST_GLOBAL_CONF.cfg'";
        $commit_out = "##### Local config File ###\n";
        $cmd_local = "cat ".$unique_filename_local;
        $cmd_global = "cat ".$unique_filename_global;
        $commit_out .= shell_exec($cmd_local);
        $commit_out .= "##### Global config File ###\n";
        $commit_out .= shell_exec($cmd_global);
        $ans_result =  $ansible_output . "\n\n\n--------------Cofiguration File Output----------------\n\n\n" .  $commit_out;
      $output = generate_success_response("Configuration applied successfully",$ans_result,$json_filename,$config_filename);
        $output['pop_name'] = $pop_name;
        $commit_status="Success";
         dbUpdate(array('audit_time'=>$date,'user_name'=>$admin_name,'pop_id'=>$pop_id,'module_name'=>$entity_name,'commit_comment'=>$comment_to_script,'commit_log'=>$commit_out,'commit_status'=>$commit_status),'nxg_auditlog','`id`=?',array($comment['id']));
    }

    return $output;


}

function generate_commit_comment($entity_name,$pop_id)
{
    $comment = "Committing_changes_for_module_".$entity_name."_in_POP_".$pop_id;
    return $comment;

}

function get_admin_name()
{

    return $_SESSION['username'];

}


function get_ip_interface_mgmt_time()
{
    $result = dbFetchRow("select max(last_update_time) as last_update_time from nxg_pop_interface_ip_mgmt;");

    if(count($result) > 0)
        return $result["last_update_time"];    
    else 
        return "null";

}


function lock_ip_interface_mgmt($last_update_time)
{
    $r = dbUpdate(array('state'=>'locked'),'nxg_pop_interface_ip_mgmt_lock','state=?', array('unlocked'));


 /*   if($r == FALSE)
        return -1;  // DB Query failed...

echo  "checking ..";*/
    if($r == 0)
        return -2; // Table is already locked

    // table is locked now check last update time...
    if(get_ip_interface_mgmt_time() != $last_update_time)
    {
        unlock_ip_interface_mgmt();
        return -3; // someone  has modified the table so as user to reload his data. 
    }
    else
    {
            return 0;
    }
}


function get_lock_error_code_message($err_code)
{

    if($err_code == -1)
        return "Database failure while trying to acquire lock for IP, Interface management.";

    if($err_code == -2)
        return "IP, Interface management table is locked,please try after some time.";

    if($err_code == -3)
        return "Some other admin has changed IP,Interface management values. You need to reload the screen and try again.";

}


function unlock_ip_interface_mgmt()
{
    dbUpdate(array('state'=>'unlocked'),'nxg_pop_interface_ip_mgmt_lock');
}

function validate_and_lock_ip_interface_mgmt($last_update_time)
{
   $r =  lock_ip_interface_mgmt($last_update_time);
   if($r == 0)
    return "";
   else
    return get_lock_error_code_message($r);     
}

function validate_ip_interface_mgmt_time($last_update_time)
{
     if(get_ip_interface_mgmt_time() != $last_update_time)
        return get_lock_error_code_message(-3); // someone  has modified the table so as user to reload his data.
     else
        return "";    
}




?>



<?php 

include_once('/opt/observium/nexusguard/db/db_pop_functions.php');
include_once("common/common.inc.php");
include_once('/opt/observium/nexusguard/var/popmgr_var.php');
include_once('/opt/observium/nexusguard/config/delete_filter_config.php');
include_once('/opt/observium/nexusguard/db/add_to_auditlog.php');


$del_filter_data = get_input_json();
$filter_id =   $del_filter_data['filter_id'];
$comment = $del_filter_data['comment'];
$action = $del_filter_data['action'];
$pop_id = $del_filter_data['pop_id'];


$pop_details = get_sinlge_pop_detail($pop_id);

$device_details = dbFetchRow("select hostname from devices where device_id = ?",array($pop_details['pop_edge_router_device_id']));


$hostname = $device_details['hostname'];
$device_password = $pop_details['password'];
$device_username = $pop_details['username'];

if($action == "commit_check")
{
    $delete_filter_array=get_delete_filter_config($del_filter_data);
    $output['output'][] = do_ansible_commit_check_with_cmd("delete_filter","nxg_delete_filter_management.sh", $hostname, $device_username, $device_password,$delete_filter_array,$action);

}
else
{
    $json_filename=$del_filter_data['json_file'];
   $output['output'][] =  do_ansible_commit("delete_filter","nxg_delete_filter_management.sh", $hostname, $device_username, $device_password,$json_filename,$action);

    if($output['error_code']=="")
    {
           dbDelete('nxg_traffic_filter', "`id` = ?", array($filter_id));
     }


}

echo json_encode($output);
?>

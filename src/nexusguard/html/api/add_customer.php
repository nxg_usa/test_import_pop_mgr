<?php
include_once("common/common.inc.php");
include_once("/opt/observium/nexusguard/config/cust_config.php");
include_once("/opt/observium/nexusguard/db/db_customer_functions.php");
include_once("/opt/observium/nexusguard/validator/validate_customer_details.php");


$add_cust_data = get_input_json();

$cust_id = $add_cust_data["cust_id"];
$action = $add_cust_data["action"];
$ip_interface_mgmt_update_time = $add_cust_data["ip_interface_mgmt_update_time"];

$errmsg .= validate_ip_interface_mgmt_time($ip_interface_mgmt_update_time);
if($errmsg !="")
{
    $output = generate_error_response("error","Error in genrarating automatic interface and ip values.",$errmsg);
    echo json_encode($output);
    exit;
}



if(isset($cust_id))
{
    // existing customer case 
    if($action == "commit_check")
    {
        $errmsg .= validate_customer_connection($add_cust_data);
        if($errmsg !="")
        {
            $output = generate_error_response("validation_error","Error in entered data, please fix below validation errors.",$errmsg);
            echo json_encode($output);
            exit;
        }
    }
}
else
{
    // new customer case 
    if($action == "commit_check")
    {
        $errmsg = validate_customer_details_form($add_cust_data);
        $errmsg .= validate_customer_connection($add_cust_data);
        if($errmsg !="")
        {
            $output = generate_error_response("validation_error","Error in entered data, please fix below validation errors.",$errmsg);
            echo json_encode($output);
            exit;
        }

        $errmsg = validate_customer_business($add_cust_data);
        if($errmsg !="")
        {
            $output = generate_error_response("validation_error","Error in entered data, please fix below validation errors.",$errmsg);
            echo json_encode($output);
            exit;
        }
    }
}

    $conn_data = $add_cust_data["connection"];
    $pop_id =  $conn_data["pop_id"];

    $pop_details = get_sinlge_pop_detail($pop_id);
    //get device details
    $device_details = dbFetchRow("select hostname from devices where device_id = ?",array($pop_details['pop_edge_router_device_id']));
    $hostname = $device_details['hostname'];
    $device_username = $pop_details['username'];
    $device_password = $pop_details['password'];

    if($action =="commit_check")
    {
        $add_cust_config = get_cust_config($add_cust_data);
        $output = do_ansible_commit_check_with_cmd("customer","nxg_add_customer.sh", $hostname, $device_username, $device_password,$add_cust_config,$action);
    }
    else
    {
        $lock_error = validate_and_lock_ip_interface_mgmt($ip_interface_mgmt_update_time);
        if($lock_error != "")
        {
            $output = generate_error_response("error","Error in genrarating automatic interface and ip values.",$lock_error);
            echo json_encode($output);
            exit;
        }
                
        $json_file =  $add_cust_data["json_file"];
        $output = do_ansible_commit("customer","nxg_add_customer.sh", $hostname, $device_username, $device_password,$json_file,$action);
        if(!isset($output["error_code"]))
        {
            $cust_config = json_decode(file_get_contents($json_file),true);

            if(!isset($cust_id)) // new customer case
            {
                $cust_id = add_customer($cust_config);
                $dest_network_prefix  = explode("\n",$add_cust_data['net_srv_prefix']); // read prefixes from form instead of file as file reading do not give correct values
                for($i = 0 ; $i < count($dest_network_prefix) ;$i++)
                {
                    $prefix_id = add_nxg_net_data( $cust_id , $dest_network_prefix[$i]);
                }
                $all_prefixes_id = add_nxg_net_data( $cust_id , "ALL-PREFIXES");
               add_inbound_traffic($cust_id,$pop_id,"ALL-PREFIXES","0");
            }

            $new_conn_id = add_update_customer_conn($cust_id,$cust_config);
            add_customer_if_ip_mgmt_config($pop_id,$new_conn_id,$cust_config);
        }

        unlock_ip_interface_mgmt();
    }


echo json_encode($output);

?>

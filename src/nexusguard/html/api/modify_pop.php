<?php
include_once("common/common.inc.php");
include_once("/opt/observium/nexusguard/config/modify_pop_config.php");
include_once("/opt/observium/nexusguard/db/db_pop_functions.php");
include_once("/opt/observium/nexusguard/validator/validate_pop.php");
include_once("/opt/observium/nexusguard/db/db_ip_mgmt_functions.php");
$interface_list=array();
$interfaces_add=array();
$interfaces_del=array();
$interfaces_add=array();
$db_data=array();
$gui_data=array();
$gui_insertion_data=array();
$action="";
$db_operation="";
$pop_data = get_input_json();
$errmsg =  validate_add_pop($pop_data);
if(!empty($errmsg))
{
   $output =  generate_error_response("vlderr01","Validation error is present on entered data. Please re enter data", $errmsg);
    echo json_encode($output);
    exit;
}

$ip_interface_mgmt_update_time = $pop_data["ip_interface_mgmt_update_time"];

$errmsg .= validate_ip_interface_mgmt_time($ip_interface_mgmt_update_time);
if($errmsg !="")
{
    $output = generate_error_response("error","Error in genrarating automatic interface and ip values.",$errmsg);
    echo json_encode($output);
    exit;
}


$pop_id=$pop_data['pop_id'];
$pop_interface_data_db=get_pop_interface_data($pop_id);

$intf_to_processors=$pop_data['intf_to_processor'];
$intf_to_processors_vlan = $pop_data['vlan'];
$scrubber_type= $pop_data['type'];
$custom_name= $pop_data['traffic_name'];
$gui_row_id= $pop_data['gui_row_id'];
$insertion_intf=$pop_data['ins_intf'];
$insertion_vlan=$pop_data['ins_vlan'];

if(is_string($intf_to_processors))
{
	$tmp=(explode(" ",$intf_to_processors));
	$intf_to_processors=$tmp;
    $tmp=(explode(" ",$insertion_intf));
    $insertion_intf=$tmp;
	$tmp=(explode(" ",$intf_to_processors_vlan));
	$intf_to_processors_vlan=$tmp;
    $tmp=(explode(" ",$scrubber_type));
    $scrubber_type=$tmp;
    $tmp=(explode(" ",$custom_name));
    $custom_name=$tmp;
    $tmp=(explode(" ",$insertion_vlan));
    $insertion_vlan=$tmp;
     $tmp=(explode(" ",$gui_row_id));
    $gui_row_id=$tmp;

}

$db_data = array();
$db_interfaces = array();
foreach($pop_interface_data_db as $interface)
{
	$intf=$interface['device_interface_name'];
	$unit=$interface['vlan_id'];
	$key=$intf.".".$unit;
    $db_data[$key] = $interface;
}

for($i=0;$i<count($intf_to_processors);$i++)
{
	$intf=$intf_to_processors[$i];
	$unit=$intf_to_processors_vlan[$i];
	$key=$intf.".".$unit;
    $scrubber_type=$type[$i];
    $custom_name=$custom_name[$i];
    $ins_intf=$insertion_intf[$i];
    $ins_vlan=$insertion_vlan[$i];
	array_push($gui_data,$key);
	$ins_intf=$insertion_intf[$i];
	$ins_vlan=$insertion_vlan[$i];
	$key_ins=$ins_intf.".".$ins_vlan;
	array_push($gui_insertion_data,$key_ins);

}
    $i=0;
	foreach($gui_data as $key)
	{
		$ins_key=$gui_insertion_data[$i];		
		if(!(in_array($key,$db_data)))
		{
			$interfaces_add[]=$key;
			$ins_interfaces_add[]=$ins_key;
		}
        else
        {
           $db_interfaces[] =  $key;
        }
		$i++;
	}
	foreach($db_data as $key=>$value)
	{
		if(!(in_array($key,$gui_data)))
		{
			$interfaces_del[]=$key;
		}
	}

if(is_string($ins_interfaces_add))
{
	$tmp=(explode(" ",$ins_interfaces_add));
	$ins_interfaces_add=$tmp;
}
$pop_id = $pop_data['pop_id'];
$device_details = dbFetchRow("select username,password from nxg_pop_details where id = ?",array($pop_id));
$device_username = $device_details['username'];
$device_password = $device_details['password'];
$hostname= $pop_data['new_name'];
$action =  $pop_data['action'];

$netflow_port=$pop_data['netflow_port'];
$flow_mgr_ip=$pop_data['flow_mgr_ip'];
$pop_edge_router_id=dbFetchRows("select device_id from devices where UPPER(hostname)=UPPER('$hostname');");
$device_id=$pop_edge_router_id[0]['device_id'];

$output = array();

if($action == "commit_check")
{
    $interface_config=generate_interface_config($pop_data,$interfaces_del,$action,$ins_interfaces_add,$db_data,$db_interfaces);
	$output = do_ansible_commit_check_with_cmd("pop","nxg_mod_pop.sh", $hostname, $device_username, $device_password,$interface_config,$action);
}
else
{

    $lock_error = validate_and_lock_ip_interface_mgmt($ip_interface_mgmt_update_time);
    if($lock_error != "")
    {
        $output = generate_error_response("error","Error in genrarating automatic interface and ip values.",$lock_error);
        echo json_encode($output);
        exit;
    }
	$action="commit";
	$pop_json_file = $pop_data['json_file'];
	$output = do_ansible_commit("mod_pop_commit","nxg_mod_pop.sh", $hostname, $device_username, $device_password,$pop_json_file,$action);

	if($output['error_code']=="")
	{
       $update_status_exabgp= update_exabgp_config($pop_data);
       $update_status_interfaces=update_interfaces_db($pop_data);
       $update_status_netflow_syslog=update_netflow_syslog_db($pop_data);
       $pop_config = json_decode(file_get_contents($pop_json_file),true);
       add_pop_config($pop_id, $pop_config);
       delete_pop_ae_member_port($pop_id,$pop_config);
       update_nfsen_config($pop_id,$hostname,$device_id,$flow_mgr_ip,$netflow_port);       
	}
     unlock_ip_interface_mgmt();
}
echo json_encode($output);

?>



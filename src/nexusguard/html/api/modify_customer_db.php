<?php
include_once("common/common.inc.php");
include_once("/opt/observium/nexusguard/config/cust_config.php");
include_once("/opt/observium/nexusguard/db/db_customer_functions.php");
include_once("/opt/observium/nexusguard/validator/validate_customer_details.php");


$add_cust_data = get_input_json();

$cust_id = $add_cust_data["cust_id"];
$email =  $add_cust_data["customer_email"];
$phone = $add_cust_data["customer_phone"];
$notes = $add_cust_data["notes"];
$desc = $add_cust_data["description"];

$query = "";
$values = array();
$i = 0;

if(isset($email))
{
    $values["email"] = $email;
    $i++;
}

if(isset($phone))
{
    $values["phone"] = $phone;
    $i++;
}


if(isset($notes))
{
    $values["notes"] = $notes;
    $i++;
}

if(isset($desc))
{
    $values["description"] = $desc;
    $i++;
}

$where[0]=$cust_id;

if($i>0)
 dbUpdate($values,'nxg_customer'," customer_id=? ",$where);

$output =  generate_success_response("Database updated successfully." ,"", null,null);


echo json_encode($output);


?>

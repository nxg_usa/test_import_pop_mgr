<?php
include_once('/opt/observium/nexusguard/db/db_pop_functions.php');

include_once("common/common.inc.php");
include_once("/opt/observium/nexusguard/config/isp_config.php");
include_once("/opt/observium/nexusguard/validator/validate_isp_details.php");

$add_isp_data = get_input_json();

//validate_isp_details_form($add_isp_data);


$pop_id = $add_isp_data['pop_id'];

$pop_details = get_sinlge_pop_detail($pop_id);

//get device details
$device_details = dbFetchRow("select hostname from devices where device_id = ?",array($pop_details['pop_edge_router_device_id']));
$hostname = $device_details['hostname'];

$device_username = $pop_details['username'];
$device_password = $pop_details['password'];
$action = $add_isp_data['action'];

$isp_id = $add_isp_data['isp_id'];
if($action == "commit_check")
{
    $add_isp_config = offline_isp_config($add_isp_data);
    $output = do_ansible_commit_check_with_cmd("isp","nxg_offline_isp.sh", $hostname, $device_username, $device_password,$add_isp_config,$action);
}
else
{
    $isp_json_file = $add_isp_data['json_file'];
    $output = do_ansible_commit("isp","nxg_offline_isp.sh", $hostname, $device_username, $device_password,$isp_json_file,$action);
    if($output['error_code']=="")
    {
        $ret = toggle_isp_status($isp_id,1);
    }
}

echo json_encode($output);

?>

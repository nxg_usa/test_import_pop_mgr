<?php
include_once("common/common.inc.php");
include_once("/opt/observium/nexusguard/var/popmgr_var.php");
include_once ('/opt/observium/html/nexusguard/api/ubersmith_oss/class.uber_api_client.php');
include_once ('/opt/observium/nexusguard/var/popmgr_var.php');

global $popmgr_config;


$service_code =$popmgr_config["oss_cient_service_code"];
$cpl_meta_data_name= $popmgr_config["oss_cient_service_meta_data"];
$prefix_separator = $popmgr_config["oss_cient_cpl_separator"];


$client = new uber_api_client($popmgr_config["oss_api_url"],$popmgr_config["oss_api_user"],$popmgr_config["oss_api_password"]);
$client_id = $_GET["client_id"];
try {

        $result = $client->call('client.get',array(
       'client_id' => $client_id,
   ));


    $services = $client->call('client.service_list',array(
       'client_id' => $client_id,
   ));

    $keys=array_keys($services);
    foreach($keys as $key)
    {
        if($services[$key]["code"] == $service_code)
        {
             $service_id = $services[$key]["packid"];
             $service_details = $client->call('client.service_get',array(
                 'client_id' => $client_id,'service_id' =>  $service_id, "metadata" => array ("1")
             ));

            $prefix_list = $service_details["metadata"][$cpl_meta_data_name];
            $prefix_list_array = explode($prefix_separator, $prefix_list);
            $result["prefix_list"] = $prefix_list_array;

            break;
        }
    }
        
        
    $resp =  generate_success_response("success" ,$result, null,null);
    echo json_encode($resp);


} catch (Exception $e) {

        $output = generate_error_response("ERROR: ".$e->getCode(), $e->getMessage(),"");
        echo json_encode($output);
        exit;

    //print 'Error: '. $e->getMessage() .' ('. $e->getCode() .')';
}


?>

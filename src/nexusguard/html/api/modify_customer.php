<?php
include_once("common/common.inc.php");
include_once("/opt/observium/nexusguard/config/cust_config.php");
include_once("/opt/observium/nexusguard/db/db_customer_functions.php");
include_once("/opt/observium/nexusguard/validator/validate_customer_details.php");


$add_cust_data = get_input_json();

$cust_id = $add_cust_data["cust_id"];
$action = $add_cust_data["action"];

if(isset($cust_id))
{
    // existing customer case 
    if($action == "commit_check")
    {
        $errmsg .= validate_customer_connection($add_cust_data);
        if($errmsg !="")
        {
            $output = generate_error_response("validation_error","Error in entered data, please fix below validation errors.",$errmsg);
            echo json_encode($output);
            exit;
        }
    }
}
else
{
    // new customer case 
    if($action == "commit_check")
    {
        $errmsg = validate_customer_details_form($add_cust_data);
        $errmsg .= validate_customer_connection($add_cust_data);
        if($errmsg !="")
        {
            $output = generate_error_response("validation_error","Error in entered data, please fix below validation errors.",$errmsg);
            echo json_encode($output);
            exit;
        }

        $errmsg = validate_customer_business($add_cust_data);
        if($errmsg !="")
        {
            $output = generate_error_response("validation_error","Error in entered data, please fix below validation errors.",$errmsg);
            echo json_encode($output);
            exit;
        }
    }
}

    $conn_data = $add_cust_data["connection"];
    $pop_id =  $conn_data["pop_id"];

    $pop_details = get_sinlge_pop_detail($pop_id);
    //get device details
    $device_details = dbFetchRow("select hostname from devices where device_id = ?",array($pop_details['pop_edge_router_device_id']));
    $hostname = $device_details['hostname'];
    $device_username = $pop_details['username'];
    $device_password = $pop_details['password'];

    if($action =="commit_check")
    {
        $add_cust_config = get_cust_conn_modify_config($add_cust_data);
        $output = do_ansible_commit_check_with_cmd("customer","nxg_modify_customer.sh", $hostname, $device_username, $device_password,$add_cust_config,$action);
    }
    else
    {
        $json_file =  $add_cust_data["json_file"];
        $output = do_ansible_commit("customer","nxg_modify_customer.sh", $hostname, $device_username, $device_password,$json_file,$action);
        if(!isset($output["error_code"]))
        {
            $cust_config = json_decode(file_get_contents($json_file),true);

            add_update_customer_conn($cust_id,$cust_config);
           // update_cust_config($pop_id,$new_conn_id,$cust_config);
        }
    }


echo json_encode($output);

?>

<?php
include_once("common/common.inc.php");
include_once("/opt/observium/nexusguard/config/cust_config.php");

$del_cust_conn_data = get_input_json();

$pop_id = $del_cust_conn_data["connection"]['pop_id'];

$pop_details = get_sinlge_pop_detail($pop_id);


//get device details
$device_details = dbFetchRow("select hostname from devices where device_id = ?",array($pop_details['pop_edge_router_device_id']));
$hostname = $device_details['hostname'];

$device_username = $pop_details['username'];
$device_password = $pop_details['password'];

$action = $del_cust_conn_data['action'];
$conn_id = $del_cust_conn_data["connection"]['conn_id'];

if($action == "commit_check")
{
    $del_cust_conn_config = get_delete_config($conn_id);
    $output = do_ansible_commit_check_with_cmd("customer","nxg_del_cust.sh", $hostname, $device_username, $device_password,$del_cust_conn_config,$action);
}
else
{
    $isp_json_file = $add_isp_data['json_file'];
    $output = do_ansible_commit("isp","nxg_del_cust.sh", $hostname, $device_username, $device_password,$isp_json_file,$action);
    if($output['error_code']=="")
    {
        $ret = delete_customer($isp_id);
    }
}

echo json_encode($output);

?>

<?php
include_once('/opt/observium/nexusguard/db/db_pop_functions.php');

include_once("common/common.inc.php");
include_once("/opt/observium/nexusguard/config/cust_config.php");
include_once("/opt/observium/nexusguard/validator/validate_cust_details.php");

$cust_data = get_input_json();

//validate_isp_details_form($add_isp_data);


$pop_id = $cust_data['pop_id'];

$pop_details = get_sinlge_pop_detail($pop_id);

//get device details
$device_details = dbFetchRow("select hostname from devices where device_id = ?",array($pop_details['pop_edge_router_device_id']));
$hostname = $device_details['hostname'];

$device_username = $pop_details['username'];
$device_password = $pop_details['password'];
$action = $cust_data['action'];

$cust_id = $cust_data['cust_id'];
if($action == "commit_check")
{
    $add_cust_config = offline_cust_config($cust_data);
    $output = do_ansible_commit_check_with_cmd("cust","nxg_offline_cust.sh", $hostname, $device_username, $device_password,$add_cust_config,$action);
}
else
{
    $cust_json_file = $cust_data['json_file'];
    $output = do_ansible_commit("cust","nxg_offline_cust.sh", $hostname, $device_username, $device_password,$cust_json_file,$action);
    if($output['error_code']=="")
    {
        $ret = toggle_cust_status($cust_id,1);
    }
}

echo json_encode($output);

?>

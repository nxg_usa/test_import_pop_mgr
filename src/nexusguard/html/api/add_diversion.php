<?php
include_once('/opt/observium/nexusguard/db/db_pop_functions.php');
include_once('/opt/observium/nexusguard/db/db_add_diversion.php');
include_once("common/common.inc.php");
include_once('/opt/observium/nexusguard/validator/validate_add_diversion.php');
include_once("/opt/observium/nexusguard/config/add_diversion_config.php");
include_once('/opt/observium/nexusguard/var/popmgr_var.php');

global $var_popmgr_user;


$add_diversion_data  = get_input_json();
$pop_id = $add_diversion_data['pop_id'];
$action = $add_diversion_data['action'];

if ($action == "commit_check")
{
    $errmsg = validate_add_diversion($add_diversion_data);
    if($errmsg !="")
    {
            $output['output'][] = generate_error_response("validation_error","Error in entered data, please fix below validation errors.",$errmsg);
            echo json_encode($output);
    exit;
            
    }
}

############################## FOR ALL ######################################
if ( $pop_id == "000" )
{
    $add_diversion_array = array();
    $add_diversion_all_config = array();
    $pop_names=dbFetchRows('select distinct id from nxg_pop_details');
    foreach ($pop_names as $pop_ids)
    {
        
        $add_diversion_array['pop_id'] = $pop_ids['id'];
        $add_diversion_array['diversion_name'] = $add_diversion_data['diversion_name'];
        $add_diversion_array['diversion_type'] = $add_diversion_data['diversion_type'];
        $add_diversion_array['network_prefix_ip'] = $add_diversion_data['network_prefix_ip'];
        $add_diversion_array['network_prefix_subnet_ip'] = $add_diversion_data['network_prefix_subnet_ip'];
        $add_diversion_array['source_ip'] = $add_diversion_data['source_ip'];
        $add_diversion_array['source_port'] = $add_diversion_data['source_port'];
        $add_diversion_array['source_subnet'] = $add_diversion_data['source_subnet'];
        $add_diversion_array['protocol'] = $add_diversion_data['protocol'];
        $add_diversion_array['port_no'] = $add_diversion_data['port_no'];
        $add_diversion_array['comment'] = $add_diversion_data['comment'];
        $add_diversion_array['action'] = $add_diversion_data['action'];
        $add_diversion_array['md5_keystring'] = $add_diversion_data['md5_keystring'];
        $add_diversion_array['all_pop_value'] = "000";

$pop_details = get_sinlge_pop_detail($pop_ids['id']);




$local_server_string=dbFetchRow("select local_server_ip from nxg_exabgp_details where pop_details_id= ?",array($pop_ids['id']));

$global_server_string=dbFetchRow("select global_server_ip from nxg_exabgp_details where pop_details_id= ?",array($pop_ids['id']));

$pop_name=dbFetchRow("select pop_name from nxg_pop_details where id= ?",array($pop_ids['id']));

if($action == "commit_check")
{
       	$local_server_ip = (explode ('/',$local_server_string['local_server_ip']));
        $global_server_ip = (explode ('/',$global_server_string['global_server_ip']));
	$local_server_username=$var_popmgr_user;
        $global_server_username=$var_popmgr_user;

        $add_diversion_config = get_add_diversion_config($add_diversion_array,$pop_ids['id']);
        $target_value=$add_diversion_config['local_server']['gui'][0]['target_value'];
        $output['output'][] = do_ansible_commit_check_with_cmd_for_diversion("exabgp_diversion","nxg_exabgp_diversion.sh",$pop_name['pop_name'],$local_server_ip[0],$local_server_username,$global_server_ip[0],$global_server_username,$add_diversion_config,$action);


}
else
{
        $local_server_ip = (explode ('/',$local_server_string['local_server_ip']));
        $global_server_ip = (explode ('/',$global_server_string['global_server_ip']));
    	$local_server_username = $var_popmgr_user;
        $global_server_username = $var_popmgr_user;	
        $json_file=$add_diversion_data['json_file'];
        $json_contents=file_get_contents($json_file);
        $json_array=json_decode($json_contents, true);
        $custom_diversion_type=(explode ('~~~',$add_diversion_data['diversion_type']));

        $target=$json_array['local_server']['gui'][0]['target_value'];
        $output['output'][] = do_ansible_commit_for_diversion("exabgp_diversion","nxg_exabgp_diversion.sh",$pop_name['pop_name'],$local_server_ip[0],$local_server_username,$global_server_ip[0],$global_server_username,$json_file,$action);
        if($output['output']['0']['error_code']=="")
        {
            $admin_name=get_admin_name();
            $ret = db_insert_exabgp_diversion($add_diversion_array,$admin_name,$target,$custom_diversion_type[0],$custom_diversion_type[1]);
        }


}
}
  
}


################################ FOR LOCAL ##############################
else
{





$local_server_string=dbFetchRow("select local_server_ip from nxg_exabgp_details where pop_details_id= ?",array($pop_id));

$global_server_string=dbFetchRow("select global_server_ip from nxg_exabgp_details where pop_details_id= ?",array($pop_id));

$pop_name=dbFetchRow("select pop_name from nxg_pop_details where id= ?",array($pop_id));

if($action == "commit_check")
{
        $local_server_ip = (explode ('/',$local_server_string['local_server_ip']));
        $global_server_ip = (explode ('/',$global_server_string['global_server_ip']));
	$local_server_username = $var_popmgr_user;
        $global_server_username = $var_popmgr_user;

        $add_diversion_config = get_add_diversion_config($add_diversion_data,$pop_id);
        $output['output'][] = do_ansible_commit_check_with_cmd_for_diversion("exabgp_diversion","nxg_exabgp_diversion.sh",$pop_name['pop_name'],$local_server_ip[0],$local_server_username,$global_server_ip[0],$global_server_username,$add_diversion_config,$action);

}
else
{
        $local_server_ip = (explode ('/',$local_server_string['local_server_ip']));
        $global_server_ip = (explode ('/',$global_server_string['global_server_ip']));
	$local_server_username = $var_popmgr_user;
        $global_server_username = $var_popmgr_user;

        $json_file=$add_diversion_data['json_file'];
        $json_contents=file_get_contents($json_file);
        $json_array=json_decode($json_contents, true);
        $target=$json_array['local_server']['gui'][0]['target_value'];
        $custom_diversion_type=(explode ('~~~',$add_diversion_data['diversion_type']));

        $output['output'][] = do_ansible_commit_for_diversion("exabgp_diversion","nxg_exabgp_diversion.sh",$pop_name['pop_name'],$local_server_ip[0],$local_server_username,$global_server_ip[0],$global_server_username,$json_file,$action);
        if($output['output']['0']['error_code']=="")
        {
            $admin_name=get_admin_name();
            $ret = db_insert_exabgp_diversion($add_diversion_data,$admin_name,$target,$custom_diversion_type[0],$custom_diversion_type[1]);
        }


}


}

echo json_encode($output);
?>

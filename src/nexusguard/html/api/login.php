<?php 

//include_once("/opt/observium/html/includes/api/functions.inc.php");
//include_once("/opt/observium/html/nexusguard/api/common/common.inc.php");
include_once("/opt/observium/config.php");
include_once("/opt/observium/includes/definitions.inc.php");

//include_once($config['html_dir'] . "/includes/api/functions.inc.php");
//include_once($config['html_dir'] . "/includes/functions.inc.php");
//include_once($config['html_dir'] . "/includes/authenticate.inc.php");

@ini_set('session.gc_maxlifetime','0');    // Session will not expire until the browser is closed
@ini_set('session.hash_function', '1');    // Use sha1 to generate the session ID
@ini_set('session.referer_check', '');     // This config was causing so much trouble with Chrome
@ini_set('session.name', 'OBSID');         // Session name
@ini_set('session.use_cookies', '1');      // Use cookies to store the session id on the client side
@ini_set('session.use_only_cookies', '1'); // This prevents attacks involved passing session ids in URLs
@ini_set('session.use_trans_sid', '0');    // Disable SID (no session id in url)

function get_input_json()
{
    //log_trace($logtype_common, __FILE__, __FUNCTION__, "Begin");

    $data = json_decode(file_get_contents('php://input'), true);

    $_REQUEST["request_id"]= $data["request_id"];

    //log_debug($logtype_common, __FILE__, __FUNCTION__, "Input JSON Data->".$data);
    $data = trim_spaces($data);
    return $data;

   // log_trace($logtype_common, __FILE__, __FUNCTION__, "Begin")
}

function trim_spaces($data)
{
    foreach($data as $key=>$value)
    {
        if(is_array($value))
        {
            $data[$key] = trim_spaces($value);
        }
        else
        {
            $data[$key]=trim($value);
        }
    }
    return $data;
}


$cred = get_input_json();

$username = $cred['username'];
$password = $cred['password'];

$row = dbFetchRow("SELECT user_id, username, password, level FROM `users` WHERE `username` = ?", array($username));


$encrypt_password = crypt($password,$row['password']);


if ($encrypt_password == $row['password'])
{
  session_start();
	$_SESSION['username']=$username;
    $_SESSION['authenticated'] = true;
  echo json_encode($_SESSION);
}


?>

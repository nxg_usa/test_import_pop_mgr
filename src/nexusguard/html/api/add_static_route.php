<?php 
include_once('/opt/observium/nexusguard/db/db_pop_functions.php');
include_once("common/common.inc.php");
include_once('/opt/observium/nexusguard/validator/validate_static_route.php');
include_once('/opt/observium/nexusguard/db/db_static_route_functions.php');
include_once('/opt/observium/nexusguard/config/add_static_config.php');
include_once('/opt/observium/nexusguard/db/add_to_auditlog.php');

$add_static_data  = get_input_json();
$action = $add_static_data['action'];


if($action == "commit_check")
    {
        $errmsg .= validate_static_diversion_form($add_static_data);;
        if($errmsg !="")
        {
            $output = generate_error_response("validation_error","Error in entered data, please fix below validation errors.",$errmsg);
            echo json_encode($output);
            exit;
        }
    }


$pop_id = $add_static_data['pop_id'];
$pop_details = get_sinlge_pop_detail($pop_id);

$device_details = dbFetchRow("select hostname from devices where device_id = ?",array($pop_details['pop_edge_router_device_id']));
$hostname = $device_details['hostname'];

$device_username = $pop_details['username'];
$device_password = $pop_details['password'];

$conf_file="";
$data_array = array();
if($action == "commit_check")
{
$add_static_config = get_static_route_config($add_static_data);
$output = do_ansible_commit_check("static_route","nxg_add_manual_diversion.sh", $hostname, $device_username, $device_password,$add_static_config,$action);
}
else
{
    $static_route_json_file = $add_static_data['json_file'];
    $conf_file= $add_static_data['config_file'];
    $output = do_ansible_commit("static_route","nxg_add_manual_diversion.sh", $hostname, $device_username, $device_password,$static_route_json_file,$action);
  
    if($output['error_code']=="")
    {
        $admin_name=get_admin_name();
        $ret = add_static_route($add_static_data,$admin_name);
    }   
}

echo json_encode($output);
?>

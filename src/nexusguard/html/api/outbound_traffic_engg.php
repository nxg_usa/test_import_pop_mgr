<?php
include_once('/opt/observium/nexusguard/db/db_traffic_engg.php');

include_once("common/common.inc.php");
include_once("/opt/observium/nexusguard/config/traffic_engg_config.php");
include_once("/opt/observium/nexusguard/validator/validate_traffic_engg.php");

$traffic_engg_data = get_input_json();

$pop_id = $traffic_engg_data['pop_id'];

/*if(is_array($traffic_data))
    $pop_id = $traffic_data[0]['inbound_pop'];
else
*/  
$pop_details = get_sinlge_pop_detail($pop_id);

//get device details
$device_details = dbFetchRow("select hostname from devices where device_id = ?",array($pop_details['pop_edge_router_device_id']));
$hostname = $device_details['hostname'];

$device_username = $pop_details['username'];
$device_password = $pop_details['password'];
$action = $traffic_engg_data['action'];
if($action == "commit_check")
{
    $out_traffic_engg_config = get_outbound_engg_config($traffic_engg_data);
    $output = do_ansible_commit_check_with_cmd("outbound_traffic_engg","nxg_outbound_traffic_engg.sh", $hostname, $device_username, $device_password,$out_traffic_engg_config,$action);

}
else
{
    $traffic_engg_json_file = $traffic_engg_data['json_file'];
    $output = do_ansible_commit("outbound_traffic_engg","nxg_outbound_traffic_engg.sh", $hostname, $device_username, $device_password,$traffic_engg_json_file,$action);
    if($output['error_code']=="")
    {
        $ret = add_outbound_traffic_engg_data($traffic_engg_data);
    }
}
echo json_encode($output);

?>

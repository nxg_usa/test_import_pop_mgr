<?php
include_once("common/common.inc.php");
include_once("/opt/observium/nexusguard/config/cust_config.php");
include_once("/opt/observium/nexusguard/db/db_customer_functions.php");
include_once("/opt/observium/nexusguard/validator/validate_customer_details.php");


$data = get_input_json();
$pop_id=$data["pop_id"];

if($pop_id == "")
{
    $output = generate_error_response("validation_error","Error in entered data, please fix below validation errors.","Invalid PoP id");
    echo json_encode($output);
    exit;
}

$pop_details = get_sinlge_pop_detail($pop_id);
$pop_public_ip= $pop_details["loopback_ip"];

$direct_if = "";//get_next_available_gige_if($pop_id);
$db_ip= get_ip_address_by_subnet($pop_id,$direct_if,"31","customer","ge");
$direct_ip_local = $db_ip[0];
$direct_ip_remote = $db_ip[1];
$direct_unit =  "";//get_unit_by_type($pop_id,$direct_if,"customer");

$tunnel_if = get_interface_name("customer","gr"); // "gr-0/0/10";//get_next_available_gr_if($pop_id);
$db_ip= get_ip_address_by_subnet($pop_id,$tunnel_if,"31","customer","gr");
$tunnel_ip_local = $db_ip[0]."/31";
$tunnel_ip_remote =$db_ip[1]."/31";
$tunnel_unit =  get_unit_by_type($pop_id,$tunnel_if,"customer");


$output["pop_public_ip"] = $pop_public_ip;
$output["direct_if"]= $direct_if;
$output["direct_ip_local"]=$direct_ip_local;
$output["direct_ip_remote"]=$direct_ip_remote;
$output["direct_unit"]=$direct_unit;
$output["tunnel_if"]=$tunnel_if;
$output["tunnel_ip_local"]=$tunnel_ip_local;
$output["tunnel_ip_remote"]=$tunnel_ip_remote;
$output["tunnel_unit"]=$tunnel_unit;

$resp =  generate_success_response("success" ,$output, null,null);

echo json_encode($resp);

?>

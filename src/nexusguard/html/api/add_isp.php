<?php
include_once("common/common.inc.php");
include_once("/opt/observium/nexusguard/config/isp_config.php");
include_once("/opt/observium/nexusguard/validator/validate_isp_details.php");

$add_isp_data = get_input_json();


$ip_interface_mgmt_update_time = $add_isp_data["ip_interface_mgmt_update_time"];

$errmsg .= validate_ip_interface_mgmt_time($ip_interface_mgmt_update_time);
if($errmsg !="")
{
    $output = generate_error_response("error","Error in genrarating automatic interface and ip values.",$errmsg);
    echo json_encode($output);
    exit;
}

validate_isp_details_form($add_isp_data);

$pop_id = $add_isp_data['pop_id'];

$pop_details = get_sinlge_pop_detail($pop_id);


//get device details
$device_details = dbFetchRow("select hostname from devices where device_id = ?",array($pop_details['pop_edge_router_device_id']));
$hostname = $device_details['hostname'];

$device_username = $pop_details['username'];
$device_password = $pop_details['password'];
$action = $add_isp_data['action'];

if($action == "commit_check")
{
    $add_isp_config = get_isp_config($add_isp_data);
    $output = do_ansible_commit_check("isp","nxg_add_isp.sh", $hostname, $device_username, $device_password,$add_isp_config,$action);
}
else
{
    $lock_error = validate_and_lock_ip_interface_mgmt($ip_interface_mgmt_update_time);
    if($lock_error != "")
    {
        $output = generate_error_response("error","Error in genrarating automatic interface and ip values.",$lock_error);
        echo json_encode($output);
        exit;
    }
    $isp_json_file = $add_isp_data['json_file'];
    $output = do_ansible_commit("isp","nxg_add_isp.sh", $hostname, $device_username, $device_password,$isp_json_file,$action);
    if($output['error_code']=="")
    {
        $isp_config = json_decode(file_get_contents($isp_json_file),true);
        $ret = add_an_isp($add_isp_data);
       //update_config($isp_config,$ret,$pop_id);
        add_isp_if_ip_mgmt_config($pop_id,$ret,$isp_config);
    }
    unlock_ip_interface_mgmt();
}

echo json_encode($output);

?>

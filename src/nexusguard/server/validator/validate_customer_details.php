<?php
include_once "/opt/observium/nexusguard/validator/functions.php";
include_once "/opt/observium/nexusguard/logger/logger.php";
include_once "/opt/observium/nexusguard/common/functions.inc.php";
include_once "/opt/observium/nexusguard/db/db_customer_functions.php";


/*
This function will validatei add customer form details.
*/
function validate_customer_details_form($customer_data)
{
    $logtype="Validator";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    //Get Form names from input data. Validate each keys value.
    $keys=array_keys($customer_data);
    $errmsg="";
    foreach($keys as $key)
    {
        switch($key)
        {
            case 'customer_name' :
                if(empty($customer_data[$key]))
                {
                    $errmsg .= "<br>"."Customer name cannot be blank";
                }
                else
                    if(strpos($customer_data[$key], " ") != false)
                    {
                         $errmsg .= "<br>"."Customer name cannot have space in the name";
                    }

                break;
             case 'oss_id' :
                if(empty($customer_data[$key]))
                {
                    $errmsg .= "<br>"."OSS Client ID cannot be blank";
                }
                break;

             case 'net_srv_prefix' :
                if(empty($customer_data[$key]))
                {
                    $errmsg .= "<br>"."Network Prefix cannot be blank";
                }
                break;
            }
    }

     return $errmsg ;


}

function validate_customer_connection($customer_data)
{
    $logtype="Validator";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    $conn_data= $customer_data["connection"];
    $keys=array_keys($conn_data);
    $errmsg="";
    foreach($keys as $key)
    {
        switch($key)
        {

            case 'conn_if' :
                if(empty($conn_data[$key]))
                {
                    $errmsg .= "<br>"."Customer connection interface cannot be blank";
                }
                break;
            case 'conn_if_unit' :
                if(empty($conn_data[$key]) && $conn_data[$key] !=0)
                {
                    $errmsg .= "<br>"."Customer connection unit cannot be blank";
                }
                break;
            case 'asn_local' :
                $ret=validate_AS($conn_data[$key]);
                if($ret!=0)
                {
                    $errmsg .= "<br>"."Please provide valid Local ASN value";
                }
                else
                {
                    if( 0 == strcasecmp($conn_data[$key],$conn_data["asn_remote"]))
                        $errmsg .= "<br>"."Local ASN  cannot be same as Remote  ASN ";

                }

                break;
            case 'asn_remote' :
                $ret=validate_AS($conn_data[$key]);
                if($ret!=0)
                {
                    $errmsg .= "<br>"."Please provide valid Peer ASN value";
                }
                break;
             case 'endpoint_ip_local' :

                if($conn_data["customer_conn"]!="tunnel")
                    break;

                $ret=validate_ipv4_address($conn_data[$key]);
                if($ret!=0)
                {
                    $errmsg .= "<br>"."Please provide valid IP for Tunnel End-Point IP Local";
                }
                else
                {
                    if( 0 == strcasecmp($conn_data[$key],$conn_data["endpoint_ip_remote"]))
                        $errmsg .= "<br>"."Tunnel End-Point IP Local cannot be same as Tunnel End-Point IP remote";

                }
                break;
             case 'endpoint_ip_remote' :

                if($conn_data["customer_conn"]!="tunnel")
                    break;

                $ret=validate_ipv4_address($conn_data[$key]);
                if($ret!=0)
                {
                    $errmsg .= "<br>"."Please provide valid IP for Tunnel End-Point IP Remote";
                }
                break;

                case 'inside_ip_local' :
                $ret=validate_subnet_mask($conn_data[$key]);
                //$ret=validate_ipv4_address($conn_data[$key]);
                if($ret!=0)
                {
                    $errmsg .= "<br>"."Please provide valid IP for Inside Local IP";
                }
                else
                {
                    if( 0 == strcasecmp($conn_data[$key],$conn_data["inside_ip_remote"]))
                        $errmsg .= "<br>"."Inside IP Local cannot be same as Inside IP remote";

                }

                break;
             case 'inside_ip_remote' :
                $ret=validate_subnet_mask($conn_data[$key]);
                //$ret=validate_ipv4_address($conn_data[$key]);
                if($ret!=0)
                {
                    $errmsg .= "<br>"."Please provide valid IP for Inside Remote IP";
                }
                break;
            case 'conn_if_vlan' :

                if($conn_data["customer_conn"] != "direct" ||  empty($conn_data[$key]))
                    break;
                
                $ret=validate_vlan($conn_data[$key]);
                if($ret!=0)
                {
                    $errmsg .= "<br>"."Please provide valid vlan id";
                }
                break;
/*                 case 'direct_connect_local_ip' :
                $ret=validate_ipv4_address($customer_data['direct_connect_local_ip']);
                if($ret!=0)
                {
                    $errmsg .= "<br>"."Please provide valid IP Local IP under Direct Connect/IX/Private-Peer section";
                }
                break;

                 case 'direct_connect_remote_ip' :
                $ret=validate_ipv4_address($customer_data['direct_connect_remote_ip']);
                if($ret!=0)
                {
                    $errmsg .= "<br>"."Please provide valid IP for Remote IP under Direct Connect/IX/Private-Peer section";
                }
                break;
                 case 'direct_connect_local_as' :
                $ret=validate_AS($customer_data['direct_connect_local_as']);
                if($ret!=0)
                {
                    $errmsg .= "<br>"."Please provide valid Local AS value under Direct Connect/IX/Private-Peer section";
                }
                break;
            case 'direct_connect_peer_as' :
                $ret=validate_AS($customer_data['direct_connect_peer_as']);
                if($ret!=0)
                {
                    $errmsg .= "<br>"."Please provide valid Peer AS value under Direct Connect/IX/Private-Peer section";
                }
                break;*/

        }

    }

    return $errmsg ;
}


function validate_customer_business($customer_data)
{
    $errmsg = "";
    
    $oss_id = $customer_data['oss_id'];
    if(0 != is_present_in_db("select * from nxg_customer where LOWER(ossid)=LOWER('".$oss_id."');"))
        $errmsg .= "<br>"."OSS Client ID already present in database";


    $customer_name = $customer_data['customer_name'];
    if(0 != is_present_in_db("select * from nxg_customer where LOWER(name)=LOWER('".$customer_name."');"))
        $errmsg .= "<br>"."Customer name already present in database";

    $dest_network_prefix  = explode("\n",$customer_data['net_srv_prefix']);

    for($i = 0 ; $i < count($dest_network_prefix) ;$i++)
    {
        $errmsg .=validate_subnet_mask($dest_network_prefix[$i]);
    }
    
    return $errmsg ;
}


function validate_net_srv_prefix_modify($cust_id,$modified_data)
{
   $errmsg = ""; 

   $deleted_data =  $modified_data["delete"];
   $added_data =  $modified_data["add"];

  if(count($deleted_data) ==0 && count($added_data)==0)
  {
        $errmsg .= "<br>"."Dest Network Prefix not changed";
        return $errmsg;
  }

  for($i = 0 ; $i<count($deleted_data);$i++)
  {
        if(is_prefix_used($cust_id, $deleted_data[$i]))
           $errmsg .= "<br>"."Prefix ". $deleted_data[$i] ." cannot be deleted as it is used in Traffic Engineering policy.";
  }

  for($i = 0 ; $i<count( $added_data);$i++)
  {
        $errmsg .= validate_subnet_mask($added_data[$i]);
  }
    return $errmsg;
}

function is_prefix_used($cust_id,$prefix)
{
    $result = dbFetchRows("select * from nxg_customer_rule_prefix where rule_id in (select rule_id from nxg_customer_inbound_rule where cust_id =?) and prefix=?",array($cust_id,$prefix));

    if(count($result) > 0)
        return true;
    else
        return false;
}

function get_prefix_modfied_data($cust_id,$prefixes)
{
    $modified_data = [];
    $result = dbFetchRows("select net_srv_prefix from nxg_customer_net_service where cust_id=? and net_srv_prefix!='ALL-PREFIXES'",array($cust_id));
    $prefixes_from_db = [];
    $count =  count($result);
    for($i = 0 ; $i < $count ;$i++)
    {
        $prefixes_from_db[$i] = $result[$i]["net_srv_prefix"];
    }

    $count =  count($prefixes_from_db);
    $j=0;
    for($i = 0 ; $i < $count ;$i++)
    {
        if(!in_array($prefixes_from_db[$i],$prefixes))
        {
            $modified_data["delete"][$j] = $prefixes_from_db[$i];
            $j++;
        }
    }

    $count =  count($prefixes);
    $j=0;
    for($i = 0 ; $i < $count ;$i++)
    {
        if(!in_array($prefixes[$i],$prefixes_from_db))
        {
            $modified_data["add"][$j] = $prefixes[$i];
            $j++;
        }
    }

    return $modified_data;    
}

function validate_delete_customer($cust_id,$pop_id,$conn_id)
{

    $errmsg = "";
    $results = dbFetchRows("select * from nxg_customer_conn where cust_id= ? and pop_id=? order by conn_id asc",array($cust_id, $pop_id));
    if(count($results) > 1)
    {
        for($i = 0; $i< count($results);$i++)
        {
            if($results[$i]["conn_id"] == $conn_id)
            {
                if($i== 0)
                {
                    $errmsg .= "<br>This connection was created first, due to dependancy issues this should be deleted last. Please delete other pop connections first.";
                    break;
                }
            }
        }
    }

    $customer_name=dbFetchRow('select * from nxg_customer where customer_id='.$cust_id.'');
    $filter_availability =dbFetchRow('select *  from nxg_traffic_filter left join nxg_customer on nxg_traffic_filter.customer=nxg_customer.name where customer="'.$customer_name['name'].'"');

    if($filter_availability['filter_name'] !=null || $filter_availability['filter_name'] !="")
    {
        $errmsg .= "<br>Delete Filter for this Customer first then delete Customer.";
    }

    $rule_count =dbFetchRow('select count(*) as c from nxg_customer_inbound_rule where cust_id=?',array($cust_id));
    if($rule_count["c"] > 0)
            $errmsg .= "<br>Customer has inbound traffic engineering rules, please remove inbound traffic engineering rules before deleting customer.";

    return $errmsg;
}

?>

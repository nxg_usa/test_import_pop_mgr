<?php
include_once('/opt/observium/nexusguard/validator/functions.php');
include_once( '/opt/observium/nexusguard/logger/logger.php');
include_once ('/opt/observium/nexusguard/common/functions.inc.php');
include_once"/opt/observium/nexusguard/db/db_static_route_functions.php";

function validate_static_diversion_form($data)
{
$logtype="Validator";

log_trace($logtype, __FILE__, __FUNCTION__, "Begin");
  //Get Form names from input data. Validate each keys value.
    $keys=array_keys($data);
    $errmsg="";

    foreach($keys as $key)
     {

        switch($key)
        {
             case 'diversion_type':
                     if (is_null($data['diversion_type'])) {
                        $errmsg .= "<br>"."Diversion type can not left blank";
                    }
            break;
            case 'pop_id':
                  if(empty($data['pop_id']))
                    {
                        $errmsg .= "<br>"."Pop Cannot be left blank";
                    }
                    if (!array_key_exists('diversion_type',$data))
                     {
                         $errmsg .= "<br>"."Diversion type can not left blank";
                      }
                    break;
             case 'route_name':
                  if(empty($data['route_name']))
                    {
                        $errmsg .= "<br>"."Route name Cannot be left blank";
                    }
                    if( is_numeric($data['route_name']) )
                        {
                        $errmsg .= '<br>'.'Invalid Route name.';
                        }

                    if( $data['route_name']!= '')
                    {
                        $route_names=dbFetchRows('select * from nxg_static_route where route_name="'.$data['route_name'].'" and pop_id='.$data['pop_id'].'');
                        foreach( $route_names as $route_name)
                        {
                            $matchValue = strcmp($data['route_name'],$route_name['route_name']);
                            $match_pop_id = strcmp($data['pop_id'],$route_name['pop_id']);

                               if( $matchValue == 0 && $match_pop_id == 0){
                                     $errmsg .= '<br>'.'Route name already exist.';
                                }
                        }
                      }

                    break;
            case 'network_prefix_ip':
                     if(!empty($data['network_prefix_ip']))
                    {
                        $ret=validate_ipv4_address($data['network_prefix_ip']);
                         if($ret!=0)
                        {
                            $errmsg .= "<br>"."Invalid IP Address for Network Prefix. Please enter correct ip address";
                        }
                    }else
                    {
                        $errmsg .= "<br>"."IP Address for Network Prefix  Cannot be left blank.";
                    }

                    break;  
            
            case 'network_prefix_subnet_ip':

                    if($data['network_prefix_subnet_ip']=="0")
                    {
                        $errmsg .= "<br>"."Invalid Network Prefix Subnet.Please enter correct Network Prefix Subnet.";
                    }else if(empty($data['network_prefix_subnet_ip'])){
                         $errmsg .= "<br>"."Network Prefix Subnet Cannot be left blank .";
                    }else{
                        $ret=validate_subnet($data['network_prefix_subnet_ip']);
                            if($ret!=0)
                            {
                                $errmsg .= "<br>"."Invalid Network Prefix Subnet.Please enter correct Network Prefix Subnet.";
                            }
                    }

                break;
            case 'comment':
                break;
             case 'protocol':
                break;
            case 'default':
                    break;
        }
     }
return $errmsg;
}
?>


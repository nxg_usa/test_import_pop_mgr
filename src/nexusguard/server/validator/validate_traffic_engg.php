<?php

include_once "/opt/observium/nexusguard/validator/functions.php";
include_once "/opt/observium/nexusguard/logger/logger.php";
include_once "/opt/observium/nexusguard/common/functions.inc.php";



function validate_traffic_engg_data($cust_traffic_engg_data)
{
    $logtype="Validator";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    $customer_name = $cust_traffic_engg_data['customer_name'];
    $inbound_traffic_engg_data = $cust_traffic_engg_data['inbound_traffic_data'];
    $cust_id = $cust_traffic_engg_data['cust_id'];
    
    if(isset($inbound_traffic_engg_data['inbound_pop']))
        $inbound_traffic_engg_data = array($inbound_traffic_engg_data);

    $i = 0;
    $errmsg = "";
    foreach($inbound_traffic_engg_data as $data)
    {
        foreach($data as $key=>$value)
        {
        $row = $i +1;
        switch($key)
        {
            case 'inbound_pop':
                if(empty($value))
                {
                    $errmsg .= "\n"."Inbound POP can not left blank at $row row";
                }
                break;
            case 'inbound_prefix':
                if(empty($value[0]))
                {
                    $errmsg .= "\n"."Inbound prefix can not left blank at $row row";
                }
                break;
            case 'inbound_isp':
                if(empty($value[0]))
                {
                    $errmsg .= "\n"."Inbound isp can not left blank at $row row";
                }
                break;

            case 'defualt':
                break;

        }
        }
        $inbound_isp = $inbound_traffic_engg_data[$i]['inbound_isp'];
        $inbound_community = $inbound_traffic_engg_data[$i]['inbound_community'];
        $inbound_asn = $inbound_traffic_engg_data[$i]['inbound_asn'];

        if((count($inbound_isp) > 1 ) && (!empty($inbound_community[0])))
        {
            $errmsg .= "\n"."Invalid rule. For multiple inbound ISP, inbound community must be blank and ASN expansion should be 'none' ";
        }
        if((empty($inbound_community[0] )) && ($inbound_asn == "none" ))
        {
            $errmsg .= "\n"."Invalid rule. ASN Expansion level can not be none if inbound community is blank";
        }
        $i++;
    }

    log_trace($logtype, __FILE__, __FUNCTION__, "End");
    return $errmsg;
}

?>

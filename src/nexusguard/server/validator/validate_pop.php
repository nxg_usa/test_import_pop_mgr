<?php
include_once "/opt/observium/nexusguard/validator/functions.php";
include_once "/opt/observium/nexusguard/logger/logger.php";
include_once "/opt/observium/nexusguard/common/functions.inc.php";
include_once("/opt/observium/includes/defaults.inc.php");
include_once("/opt/observium/config.php");
include_once("/opt/observium/includes/definitions.inc.php");

//Get Form names from input pop data. Validate each keys value.
function validate_add_pop($pop_data)
{

    $logtype="Validator";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    $used_interfaces=[];
    $keys=array_keys($pop_data);
    $errmsg="";
    if($pop_data['scope']=="new")
    {
    	$pop_existence_check=is_pop_exists($pop_data['pop_name']);
	    if($pop_existence_check==1)
    	{
		    $errmsg .= "<br>"."Pop name already exists";
    		return $errmsg;
	    }   
    	$device_existence_check=validate_device($pop_data['new_name']);
	    if(!empty($device_existence_check))
    	{
		    $errmsg .= "<br>"."Device ".$pop_data['new_name']." is already enrolled. Please enter different name";
    		return $errmsg;
	    }   
    }	

    foreach($keys as $key)
    {
        switch($key)
        {
	     case 'edge-router':

		 if(empty($pop_data['edge-router']))
                    {
                        $errmsg .= "<br>"."Select atleast one Pop edge router";
                    }

	     break;
	     case 'srx':
                 if(empty($pop_data['srx']))
                    {
                        $errmsg .= "<br>"."Select atleast one Pop SRX";
                    }

             break;
        
             case 'new_name':

         //       if($pop_data['action'] == "gen_config")
                {

                    if(empty($pop_data['new_name']))
                    {
                        $errmsg .= "<br>"."Edge router name cannot be blank";

                    }


                }

             break;
            
            case 'pop_name':
                 if(empty($pop_data['pop_name']))
                    {
                        $errmsg .= "<br>"."Pop name cannot be left blank";
                    }
                    else
                    {
                        $err = validate_pop_name($pop_data['pop_name']);
                        if(! empty($err) )
                        {
                           $errmsg .= "<br>".$err;
                        }
                    }
                break;
              case 'edge_router':
                if(empty($pop_data['edge_router']))
                    {
                        $errmsg .= "<br>"."edge router cannot be left blank";
                    }
                break;
            case 'srx':
                if(empty($pop_data['srx']))
                    {
                        $errmsg .= "<br>"."PoP SRX cannot be left blank";
                    }
                break;

             case 'type' :
                $ret=validate_custom_name($pop_data['type'],$pop_data['traffic_name']);
                if($ret!=0)
                {
                    $errmsg .="<br>"."Name cannot be left blank when Type is Custom under Traffic Processor Interface";

                }
                $ret=validate_scrubber_type($pop_data['type'],$pop_data['traffic_name']);
                if($ret!=0)
                {
                    $errmsg .="<br>"."Please use unique scrubber types under Traffic Processor Interface";

                }

                break;

            case 'intf_to_processor' :
                $ret=validate_all_strings($pop_data['intf_to_processor']);
                 if($ret!=0)
                {
                    $errmsg .= "<br>"."Diversion Interface under Traffic to Processor interface cannot be blank.";

                }

                break;


/*            case 'traffic_name' :
                if(count($pop_data['traffic_name']))
                {
                    $ret=validate_custom_name($pop_data['type'],$pop_data['traffic_name']);
                        if($ret!=0)
                        {
                            $errmsg .= "<br>"."Name Cannot be left blank when Type is Custom";

                        }
                }
                break;
*/


             case 'general_name':
                if(empty($pop_data['general_name']))
                    {
                        $errmsg .= "<br>"."ISP name cannot be left blank";
                    }
                break;

            case 'nb_interface':
                if(empty($pop_data['nb_interface']))
                    {
                        $errmsg .= "<br>"."Northbound interface cannot be left blank";
                    }
                break;
           case 'nb_vlan_interface':
                if(empty($pop_data['nb_vlan_interface']))
                    {
                        $errmsg .= "<br>"."Northbound VLAN interface cannot be left blank";
                    }
                break;

           case 'local_as':
                $ret=validate_all_AS($pop_data['local_as']);
                 if($ret!=0)
                    {
                        $errmsg .= "<br>"."Enter correct Local AS values";

                    }
                break;

           case 'peer_as':
                $ret=validate_all_AS($pop_data['peer_as']);
                 if($ret!=0)
                    {
                        $errmsg .= "<br>"."Enter correct Peer AS values";

                    }
                break; 
            
            case 'local_ip':
             $ret=validate_all_ipv4_address($pop_data['local_ip']);
            if($ret!=0)
                    {
                        $errmsg .="<br>"."Please enter valid ip address and prefix for Local IP";

                    }
                break;

            case 'neighbour_ip':
            $ret=validate_all_ipv4_address($pop_data['neighbour_ip']);
            if($ret!=0)
                    {
                        $errmsg .="<br>"."Please enter valid ip address and prefix for Neighbor IP";

                    }
                break;
            case 'local_ip_subnet':
            $ret=validate_all_subnet($pop_data['local_ip_subnet']);
             if($ret!=0)
                    {
                        $errmsg .="<br>"."Please enter valid prefix for Local IP";

                    }

    
            break;

             case 'neighbour_ip_subnet':
            $ret=validate_all_subnet($pop_data['neighbour_ip_subnet']);
             if($ret!=0)
                    {
                        $errmsg .="<br>"."Please enter valid prefix for Neighbour IP";

                    }

            break;
              case 'proto_version':

              	//if(($_POST['edge_router_type']=="new_router") & (!(empty($_POST['new_name']))))
                {
                        if(empty($pop_data['auth_level']))
                        {
                            $errmsg .= "<br>"."Auth level cannot be left blank in SNMP section";
                        }
                        if(empty($pop_data['user_name']))
                        {
                            $errmsg .= "<br>"."Auth username cannot be left blank in SNMP section";
                        }
                        if(empty($pop_data['auth_password']))
                        {
                            $errmsg .= "<br>"."Auth password cannot be left blank in SNMP section";
                        }
                        else
                        {
                            $err=validate_password($pop_data['auth_password']);
                            if(!empty($err))
                            {
                                $errmsg .=$err;
                            }

                        }

                        if(!(($pop_data['auth_algo']!="MD5") | ($pop_data['auth_algo']!="SHA")))
                        {
                            $errmsg .= "<br>"."Please enter valid Authentication algorithm md5 or sha in SNMP section";
                        }
			if($pop_data['auth_level']=="authPriv")
			{
                        	if(empty($pop_data['crypto_password']))
                        	{
	                            $errmsg .= "<br>"."Crypto password cannot be left blank when Auth level is authPriv in SNMP section";
                        	}
				
                        	if(!((strcasecmp($pop_data['crypto_algo'],'aes128') == 0) | (strcasecmp($pop_data['crypto_algo'],'des') == 0)))
                        	{
                            	$errmsg .= "<br>"."Please enter valid crypto algorithm aes or des in SNMP section";
                        	}
                	}
                         if(!((strcasecmp($pop_data['transport'],'udp')) | (strcasecmp($pop_data['transport'],'udp6')) | (strcasecmp($pop_data['transport'],'tcp')) | (strcasecmp($pop_data['transport'],'tcp6'))))
                        {
                            $errmsg .= "<br>"."Please enter valid transport protocol";
                        }
                        $ret=validate_number($pop_data['port']);
                       if($ret!=0)
                        {
                             $errmsg .="<br>"."Provide valid port number in SNMP section";
                        }
                   }
            
                break;
 
                case 'exabgp_interface' :
                    if(empty($pop_data['exabgp_interface']))
                    {
                        $errmsg .= "<br>"."exabgp interface cannot be left blank";
                    }
                break;
                case 'vlan' :
                    if(!empty($pop_data['vlan']))
                    {
                        $ret=validate_all_vlans($pop_data['vlan']);
                         if($ret!=0)
                         {
                            $errmsg .="<br>"."Please provide valid vlan number under Traffic Processor Interface";
                         }
                    }
                break;

                case 'vlan_intf' :
                 $ret=validate_vlan($pop_data['vlan_intf']);
                 if($ret!=0)
                    {
                         $errmsg .="<br>"."Please provide valid ExaBGP vlan ";
                    }
                break;

                 case 'exa_ip':
                    $ret=validate_ipv4_address($pop_data['exa_ip']);
                    if($ret!=0)
                    {

                         $errmsg .="<br>"."Invalid IP Address. Please enter correct ip address for ExaBGP IP";

                    }
                break;

                case 'server_peer':
                    $ret=validate_ipv4_address($pop_data['server_peer']);
                    if($ret!=0)
                    {

                         $errmsg .="<br>"."Invalid IP Address. Please enter correct ip address for Server IP under ExaBGP Config";

                    }
                break;
            
                 case 'exa_local':
                $ret=validate_AS($pop_data['exa_local']);
                 if($ret!=0)
                    {
                        $errmsg .="<br>"."Enter correct Local AS value under ExaBGP Config ";

                    }
                break;

                case 'exa_peer':
                $ret=validate_AS($pop_data['exa_peer']);
                 if($ret!=0)
                    {
                        $errmsg .= "<br>"."Enter correct Peer AS value under ExaBGP Config ";

                    }
                break;

                case 'ins_intf':
                $ret=validate_all_strings($pop_data['ins_intf']);
                 if($ret!=0)
                {
                    $errmsg .= "<br>"."Insertion interface cannot be blank under Traffic Processor Interface";

                }
                break;
            
                case 'ins_vlan':
                $ret=validate_all_vlans($pop_data['ins_vlan']);
                 if($ret!=0)
                {
                    $errmsg .= "<br>"."Please provide valid vlan number under Traffic Processor Interface";

                }
                 if(empty($pop_data['ins_vlan']))
                {
                    $errmsg .= "<br>"."Insertion vlan cannot be blank under Traffic Processor Interface";

                }

                break;

                case 'mgmt_interface' :
                    if ($pop_data['edge_router_type'] == "new_router")
                    {
                        if(empty($pop_data['mgmt_interface']))
                        {
                            $errmsg .= "<br>"."Management Interface cannot be left blank";
                        }
                    }
                    break;
                 case 'mgmt_ip' :
                    if ($pop_data['edge_router_type'] == "new_router")
                    {
                        if(empty($pop_data['mgmt_ip']))
                        {
                            $errmsg .= "<br>"."Management IP cannot be left blank";
                        }
                        else
                        {
                            $ret = validate_ipv4_address($pop_data['mgmt_ip']);
                            if($ret != 0 )
                            {
                                $errmsg .= "<br>"."Invalid IP Address.Please enter valid Management IP"; 
                            }
                        }
                    }
                    break;
                case 'mgmt_gateway' :
                    if ($pop_data['edge_router_type'] == "new_router")
                    {
                        if(empty($pop_data['mgmt_gateway']))
                        {
                            $errmsg .= "<br>"."Management Gateway cannot be left blank";
                        }
                        else
                        {
                            $ret = validate_ipv4_address($pop_data['mgmt_gateway']);
                            if($ret != 0 )
                            {
                                $errmsg .= "<br>"."Invalid IP Address.Please enter valid Management Gateway";
                            }
                        }
                    }
                    break;

                case 'local_vlan' :

                    if(!empty($pop_data['local_vlan']))
                        {
                                if(!(is_numeric($pop_data['local_vlan'])))
                                {
                                        $errmsg .="<br>"."Please provide valid vlan number for Local Speaker";
                                }
                                else
                                {
                                     $ret=validate_all_vlans($pop_data['local_vlan']);
                                    if($ret!=0)
                                    {
                                         $errmsg .="<br>"."Please provide valid vlan number under Local Speaker Details";
                                    }
                                }
                        }

                break;

                case 'global_vlan' :
                    if(!empty($pop_data['global_vlan']))
                        {
                                if(!(is_numeric($pop_data['global_vlan'])))
                                {
                                        $errmsg .="<br>"."Please provide valid vlan number for Global Speaker";
                                }
                                else
                                {
                                    $ret=validate_all_vlans($pop_data['global_vlan']);
                                    if($ret!=0)
                                    {
                                         $errmsg .="<br>"."Please provide valid vlan number under Global Speaker Details";
                                    }
                                }
    
                        }
            
                break;
                case 'stale_routes_time_local' :
                    $ret=validate_time_exabgp($pop_data['stale_routes_time_local']);
                     if($ret!=0)
                    {
                         $errmsg .="<br>"."Please provide valid Stale routes time for local speaker i.e between 300 to 600";
                    }          
            
                break;

                case 'stale_routes_time_global' :
                     $ret=validate_time_exabgp($pop_data['stale_routes_time_global']);
                     if($ret!=0)
                    {
                         $errmsg .="<br>"."Please provide valid Stale routes time for global speaker i.e between 300 to 600";
                    }            

                break;

                case 'restart_timer_global' :

                    $ret=validate_time_exabgp($pop_data['restart_timer_global']);
                     if($ret!=0)
                    {
                         $errmsg .="<br>"."Please provide valid Graceful restart time for global speaker i.e between 300 to 600";
                    }

                break;
                case 'restart_timer_local' :

                    $ret=validate_time_exabgp($pop_data['restart_timer_local']);
                     if($ret!=0)
                    {
                         $errmsg .="<br>"."Please provide valid Graceful restart time for local speaker i.e between 300 to 600";
                    }

                break;



                case 'local_asn' :
                    $ret=validate_AS($pop_data['local_asn']);
                     if($ret!=0)
                    {
                        $errmsg .= "<br>"."Enter correct Local ASN value for Local Speaker";

                    }
                    break;
                case 'local_asn_global' :
                    $ret=validate_AS($pop_data['local_asn_global']);
                     if($ret!=0)
                    {
                        $errmsg .= "<br>"."Enter correct Local ASN value for Global Speaker";

                    }
                    break;

                case 'global_speaker_ip' :
                     $err = validate_subnet_mask($pop_data['global_speaker_ip']);
                            if(!empty($err))
                            {
                                $errmsg .= "<br>"."Please provide valid IP with subnet mask for Global speaker under Flowspec Manager";
                            }
                    break;



                case 'local_speaker_ip' :
                     $err = validate_subnet_mask($pop_data['local_speaker_ip']);
                            if(!empty($err))
                            {
                                $errmsg .= "<br>"."Please provide valid IP with subnet mask for Local speaker under Flowspec Manager";
                            }
                    break;
 
                case 'local_server_ip' :
                     $ret = validate_ipv4_address($pop_data['local_server_ip']);
                            if($ret != 0 )
                            {
                                $errmsg .= "<br>"."Please provide valid IP for Local server";
                            }
                    break;

                case 'global_server_ip' :

                        $ret = validate_ipv4_address($pop_data['global_server_ip']);
                            if($ret != 0 )
                            {
                                $errmsg .= "<br>"."Please provide valid IP for Global server under Flowspec Manager";
                            }
                    break;



                case 'local_speaker_md5' :
                        if(empty($pop_data['local_speaker_md5']))
                        {
                            $errmsg .= "<br>"."MD5 Keystring for Local speaker cannot be blank";
                        }
                    break;
                 case 'global_speaker_md5' :
                        if(empty($pop_data['global_speaker_md5']))
                        {
                            $errmsg .= "<br>"."MD5 Keystring for Global speaker cannot be blank";
                        }
                    break;

                case 'local_interface' :
                        if(empty($pop_data['local_interface']))
                        {
                            $errmsg .= "<br>"."Interface for Local speaker cannot be blank";
                        }
                    break;
                case 'global_speaker_interface' :
                        if(empty($pop_data['global_speaker_interface']))
                        {
                            $errmsg .= "<br>"."Interface for Global speaker cannot be blank";
                        }
                    break;

                case 'mgmt_ip_subnet' :
                        if(empty($pop_data['mgmt_ip_subnet']))
                        {
                            $errmsg .= "<br>"."Management IP Subnet cannot be left blank";
                        }
                        else
                        {
                            $ret = validate_subnet($pop_data['mgmt_ip_subnet']);
                            if($ret != 0 )
                            {
                                $errmsg .= "<br>"."Please provide proper Management IP Subnet";
                            }
                        }
                    break;
                  case 'oss_dev_id' :
                        $ret= validate_number($pop_data['oss_dev_id']);
                        if($ret != 0)
                        {
                            $errmsg .= "<br>"."Please provide valid OSS Device ID";
                        }
                    break;
                  case 'loopback_ip':


                        if(empty($pop_data['loopback_ip']))
                        {
                            $errmsg .= "<br>"."VR Core Public IP cannot be blank";
                        }
                        else
                        {
                            $ret = validate_ipv4_address($pop_data['loopback_ip']);
                            if($ret != 0 )
                            {
                                $errmsg .= "<br>"."Invalid IP Address. Please enter correct ip address for VR Core Public IP ";
                            }
                        }
                     break;


                    case 'netflow_source_address':

                        if(empty($pop_data['netflow_source_address']))
                        {
                            $errmsg .= "<br>"."Netflow source address cannot be blank";
                        }
                        else
                        {
                            $ret = validate_ipv4_address($pop_data['netflow_source_address']);
                            if($ret != 0 )
                            {
                                $errmsg .= "<br>"."Invalid IP Address. Please enter correct ip address for Netflow source address";
                            }
                        }
                     break;

                    case 'netflow_server_ip':


                        if(empty($pop_data['netflow_server_ip']))
                        {
                            $errmsg .= "<br>"."Netflow server ip cannot be blank";
                        }
                        else
                        {
                            $ret = validate_ipv4_address($pop_data['netflow_server_ip']);
                            if($ret != 0 )
                            {
                                $errmsg .= "<br>"."Invalid IP Address. Please enter correct ip address for Netflow server ip";
                            }
                        }
                     break; 



                    case 'syslog_host_ip':


                        if(empty($pop_data['syslog_host_ip']))
                        {
                            $errmsg .= "<br>"."Syslog host IP cannot be blank";
                        }
                        else
                        {
                            $ret = validate_ipv4_address($pop_data['syslog_host_ip']);
                            if($ret != 0 )
                            {
                                $errmsg .= "<br>"."Invalid IP Address. Please enter correct ip address for Syslog host IP";
                            }
                        }
                     break;
    



                    case 'oss_dev_id':
                        if((!is_numeric($pop_data['oss_dev_id'])) || empty($pop_data['oss_dev_id']))
                        {
                            $errmsg .= "<br>"."Please enter valid OSS Device Id ";
                    
                        }
                       break;       
                            
                    case 'prefix_list':
                       if(empty($pop_data['prefix_list']))
                       {
                           $errmsg .= "<br>"."NXG App Service Prefix cannot be left blank";
                       }
                       else
                        {
                            $service_prefix_list  = explode("\n",$pop_data['prefix_list']);
                            for($i = 0 ; $i < count($service_prefix_list) ;$i++)
                            {
                                $errmsg .=validate_subnet_mask($service_prefix_list[$i]);
                            }
                        }
                        break;
                    case 'pl_agg_prefix_list':
                       if(empty($pop_data['pl_agg_prefix_list']))
                       {
                           $errmsg .= "<br>"."PL Aggregate Prefix List cannot be left blank";
                       }
                       else
                        {
                            $pl_agg_prefix_list  = explode("\n",$pop_data['pl_agg_prefix_list']);
                            for($i = 0 ; $i < count($pl_agg_prefix_list) ;$i++)
                            {
                                $errmsg .=validate_subnet_mask($pl_agg_prefix_list[$i]);
                            }
                        }
                        break;

                    case 'netflow_port':
                         $ret=validate_number($pop_data['netflow_port']);
                          if($ret != 0)
                          {
                                $errmsg .= "<br>"."Please enter valid netflow port";

                          }
                          elseif($pop_data['netflow_port'] <= 0 || $pop_data['netflow_port'] >= 65537)
                           {

                                $errmsg .= "<br>"."Please enter valid netflow port";



                           }
                         break;

                    case 'flow_mgr_ip':
                         $ret=validate_all_ipv4_address($pop_data['flow_mgr_ip']);
                         if($ret!=0)
                         {
                            $errmsg .="<br>"."Please enter valid ip address for Netflow Source IP";

                         }
                        break;


                    
            }
        }
        $traffic_to_process = $pop_data['intf_to_processor'];
        $isp_interface = $pop_data ['nb_interface'];
        $insertion_interface = $pop_data['ins_intf'];
        $exabgp_interface = $pop_data['exabgp_interface'];
        $ins_vlan = $pop_data['ins_vlan'];
        $local_as = $pop_data['local_as'];
        $peer_as = $pop_data['peer_as'];
        $traffic_to_process_vlan = $pop_data['vlan']; 
        $exabgp_local_as = $pop_data['exa_local'];
        $exabgp_peer_as = $pop_data['exa_peer'];
        $exabgp_local_interface = $pop_data['local_interface'];
        $exabgp_global_interface = $pop_data['global_speaker_interface'];        
        $lag_interface=$pop_data['lag_interface'];
        $lag_interface_ins=$pop_data['lag_interface_ins'];
        
        if(is_string($traffic_to_process))
        {
            $tmp=(explode(" ",$traffic_to_process));
            $traffic_to_process=$tmp;
            $tmp=(explode(" ",$intf_to_processors_vlan));
            $intf_to_processors_vlan=$tmp;
            $tmp=(explode(" ",$insertion_interface));
            $insertion_interface=$tmp;
            $tmp=(explode(" ",$ins_vlan));
            $ins_vlan=$tmp;
            $tmp=(explode(" ",$custom_names));
            $custom_names=$tmp;
            $tmp=(explode(" ",$scrubber_type));
            $scrubber_type=$tmp;

        }
        if(!is_array($lag_interface))
        {
            $lag_interface = array($lag_interface);
        }
        if(!is_array($lag_interface_ins))
        {
            $lag_interface_ins = array($lag_interface_ins);
        }

        $errmsg .= validate_uniq_interface($traffic_to_process,$isp_interface,$insertion_interface,$exabgp_local_interface,$exabgp_global_interface);
        $errmsg .= validate_processor_interface($traffic_to_process,$traffic_to_process_vlan);        
        $errmsg .= validate_processor_interface($insertion_interface,$ins_vlan); 
        $errmsg .= validate_ge_interfaces_div($traffic_to_process,$lag_interface);
        $errmsg .= validate_ge_interfaces_ins($insertion_interface,$lag_interface_ins);
        //$errmsg .= validate_exabgp_ip($local_server_ip,$global_server_ip);
        //$errmsg .= validate_peer($local_as,$peer_as);       

    return $errmsg;

 }

/*function validate_exabgp_ip($local_server_ip,$global_server_ip)
{
     $logtype="Validation";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    $err = "";

    if ($local_server_ip == $global_server_ip)
    {
        $err = "<br>"."Please use different Server IP's for FlowSpec Manager";
    }

    log_trace($logtype, __FILE__, __FUNCTION__, "End");
    return $err;


}
*/

function validate_peer($local_as,$peer_as)
{

    $logtype="Validation";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");
  
    $err = "";

    if ($local_as == $peer_as)
    {
        $err = "<br>"."Please specify different AS values for local & peer";
    }
    
	log_trace($logtype, __FILE__, __FUNCTION__, "End");
    return $err;
} 
    

function validate_pop_name($pop_name)
{

    $logtype="Validation";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    $pop_name = trim($pop_name);
    $pop_details = get_pop_details_by_name($pop_name);
    $err = "";

/*    if(!empty($pop_details))
    {
        $err .= "<br>"."Pop with $pop_name is already enrolled. Please Enter different name";
    }*/
    $strlen = strlen($pop_name);

    if($strlen > 256)
    {
        $err .= "<br>"."Pop Name should contains max 256 chars";
    }

    if(strpos($pop_name, " "))
    {
        $err .= "<br>"."POP name should not contain white spaces";
    }

    log_trace($logtype, __FILE__, __FUNCTION__, "End");
    return $err;
}
function validate_password($password)
{
    $logtype="Validation";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    $strlen = strlen($password);
    $err = "";

    if($strlen < 8 )
    {
        $err .= "<br>"."Password must contain more than 8 characters";
    }

    log_trace($logtype, __FILE__, __FUNCTION__, "End");

    return $err;

}
function validate_device($device_name)
{
    $logtype="Validation";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");
    
    $device_name = trim($device_name);
    $err  = "";

    $device = get_device_by_name($device_name); 
    if(empty($device) == false)
    {
       $err .= "Device $device_name is already enrolled. Please enter different name";
    }
    if(strpos($device_name, " "))
    {
        $err .= "<br>"."POP name should not contain white spaces";
    }

    log_trace($logtype, __FILE__, __FUNCTION__, "End");

    return $err;
}

function validate_uniq_interface($traffic_to_process,$isp_interface,$insertion_interface,$exabgp_local_interface,$exabgp_global_interface)
{
    $logtype="Validation";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");
    $err  = "";
    $i= 0;
    $cnt = count($traffic_to_process);
	
    for($i=0;$i<$cnt;$i++)
    {
        $traffic_interface = $traffic_to_process[$i];
        $ins_interface = $insertion_interface[$i];

        if( $traffic_interface == $ins_interface || $traffic_interface == $exabgp_local_interface || $ins_interface == $exabgp_local_interface || $traffic_interface == $exabgp_global_interface || $ins_interface == $exabgp_global_interface)
        {
            $err = "<br>"."Please use different interfaces for Diversion Interface,Insertion Interface,Local Speaker Interface and Global Speaker Interface";
            break;
        }
    }

    return $err;    

    log_trace($logtype, __FILE__, __FUNCTION__, "End");
}

function validate_processor_interface($interface,$vlan)
{
    $logtype="Validation";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    $i = 0;
    $j = 1;
    $cnt = count($interface);
    
    for($i = 0 ;$i<$cnt; $i++)
    {
        $j = $i+1;
        if(($interface[$i] == $interface[$j]) && ( $vlan[$i] == $vlan[$j] ))
        {
            $err = "<br>Cant have same interface & vlan in Traffic to processor interface";
            break;
        }
    }

    log_trace($logtype, __FILE__, __FUNCTION__, "End");

    return $err;
}

function validate_scrubber_type($scrubber_types,$custom_names)
{
$i=0;
foreach($scrubber_types as $type)
{
    $custom_name = strtoupper($custom_names[$i]);
    $key=$type.".".$custom_name;
    $scrubber_array[$i]=$key;
    $i++;
}
$unique_scrubber_array=array_unique($scrubber_array);
if(count($unique_scrubber_array) == count($scrubber_array))
{
    return 0;
}
else
{
    return 1;

}

}
function validate_ge_interfaces_div($traffic_to_process,$lag_interface)
{
    $x=0;
    $flag=0;
    $errormsg="";
    foreach($traffic_to_process as $interface)
    {
        
        $pos=strpos($interface,'ge');
        if (strpos($interface,'ge') !== false) 
        {
            if(!empty($lag_interface[$x][0]))
            {
                $flag=1;
            }
        }
        $x++;
    }
    if($flag==1)
    {
        $errormsg="<br>"."Member ports can't be added when diversion interface type is ge";
    }
    return $errormsg;
}
function validate_ge_interfaces_ins($insertion_interfaces,$lag_interface)
{
    $x=0;
    $flag=0;
    $errormsg="";
    foreach($insertion_interfaces as $interface)
    {

        $pos=strpos($interface,'ge');
        if (strpos($interface,'ge') !== false)
        {
            if(!empty($lag_interface[$x][0]))
            {
                $flag=1;
            }
        }
        $x++;
    }
    if($flag==1)
    {
        $errormsg="<br>"."Member ports can't be added when insertion interface type is ge";
    }
    return $errormsg;
}




?>


<?php

function is_pop_exists($pop_name)
{
        $pop_data=dbFetchRows("select count(*) from nxg_pop_details where pop_name='$pop_name';");
        $pop_cnt=$pop_data[0]['count(*)'];
        if($pop_cnt==0)
        {
                return 0;
        }
        else
        {
                return 1;
        }
}

function validate_ipv4_address($ip_adress, $check_private_ip)
{
    if(!filter_var($ip_adress, FILTER_VALIDATE_IP) === false)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

/* Accepts input as array of multiple ipv4 addresses with flag to check private or public address
*/
function validate_all_ipv4_address($ip_adresses, $check_private_ip)
{
    if(is_array($ip_adresses))
    {
        foreach($ip_adresses as $ip_address)
        {
            if(validate_ipv4_address($ip_address,$check_private_ip))
            {
                return 1;
            }
        }
    }
    else
    {
        return validate_ipv4_address($ip_adresses,$check_private_ip);
    }
}

function validate_subnet($subnet)
{

    if(validate_number($subnet))
        return 1;

    $flag=0;
    if($subnet != "")
    {
        $netmask=(int)$subnet;
        if($netmask <=0 || $netmask >32)
        {
            $flag=1;
        }
    }
    else
    {
        $flag=1;
    }
    return $flag;
}

/* Accepts input as array of multiple subnet mask and also checks subnet range
*/
function validate_all_subnet($subnet_array)
{
    $flag=0;
    if(is_array($subnet_array))
    {
        foreach($subnet_array as $subnet)
        {
            if(validate_subnet($subnet))
            {
                $flag=1;
            }
        }   
    }
    else
    {
        $flag = validate_subnet($subnet_array);
    }
    return $flag;

}

function validate_ipv6($ip_address)
{



}
function validate_number($string)
{
    if (!is_numeric($string))
    {
         return 1;
    }
    else
    {
        return 0;    
    }

}
/*function  validate_vlan($vlan)
{
    if(!empty($vlan))
    {
        if(!validate_number($vlan))
        {
            if($vlan >0 && $vlan <= 4094)
            {
                return 0;
            }
        }
        else
        {
            return 1;
        }
    }
    else
    {
        return 1;
    }
}*/
function  validate_vlan($vlan)
{
    if(!empty($vlan) || $vlan ==0 )
    {
        if(!validate_number($vlan))
        {
            if($vlan <= 0 || $vlan >= 4095)
            {
                return 1;
            }
        }
        else
        {
            return 1;
        }
    }
    else
    {
        return 1;
    }
}





function  validate_all_vlans($vlans)
{
if(!is_array($vlans))
{
    $vlans = array($vlans);
}
foreach($vlans as $vlan)
{
    if(!empty($vlan))
    {
        if(!validate_number($vlan))
        {
            if($vlan <0 || $vlan >= 4095)
            {
                return 1;
            }
        }
        else
        {
            return 1;
        }
    }
    else
    {
        return 0;
    }
}

}


function validate_AS($as)
{
    $flag=0;
    if(!empty($as))
    {
        if(validate_number($as))
            return 1;
        $int=(int)$as;

        if (!is_numeric($int)) 
        {
            $flag=1;
        }
        else
        {
            if ($int < 0 || $int > 65535)
            {
                $flag=1;
            }
        }
    }
    else
    {
        $flag= 1;
    }
    return $flag;
}

function validate_all_AS($as_number)
{
    $flag=0;
    if(is_array($as_number))
    {
        foreach($as_number as $as)
        {
            if (validate_AS($as))
            {
                $flag=1;
            }
        }
    }
    else
    {
        $flag = validate_AS($as_number);
    }
    return $flag;

}

function validate_int_array($int_array)
{
    $flag=0;
    if(is_array($int_array))
    {
        foreach($int_array as $int)
        {
            if (validate_number($int))
            {
                $flag=1;
            }
        }
    }
    else
        $flag = validate_number($int_array);

    return $flag;
} 


function validate_email($email)
{

if (!filter_var($email, FILTER_VALIDATE_EMAIL) === false) 
{
     return 0;
}
 else 
{
     return 1;
}

}


function validate_ip_and_prefix($ip, $cidr)
{
    list($subnet, $mask) = explode('/', $cidr);

    if ((ip2long($ip) & ~((1 << (32 - $mask)) - 1) ) == ip2long($subnet))
    { 
        return 0;
    }

    return 1;
}

function validate_all_ip_and_prefix($ip_addresses, $prefixes)
{
    $flag=0;
    $cnt_ip=count($ip_addresses);
    if($cnt_ip==0)
    {
        return 1;
    }
    $cnt=count($ip_addresses);
    for($i=0;$i<10;$i++)
    {
        $ip=$ip_addresses[$i];
        $cidr=$prefixes[$i];
        $network = long2ip((ip2long($ip)) & ((-1 << (32 - (int)$cidr))));
        if (!((ip2long($ip) & ~((1 << (32 - $cidr)) - 1) ) == ip2long($network)))
        {
            $flag=1;  
        }
    }
   return $flag;
}

function validate_custom_name($types, $custom_names)
{
    $i=0;
    if(!is_array($types))
    {
        $types = array( $types );
        $custom_names = array( $custom_names );
    }
    foreach($types as $type)
    {
        if($type == 'custom')
        {
            if($custom_names[$i]=="")
            {
                return 1;
            }
        }
        $i++;
    }
}

function validate_all_strings($strings)
{
    $i=0;
    foreach($strings as $string)
    {
            if($strings[$i]=="")
            {
                return 1;
            }
        $i++;
    }
}

function validate_subnet_mask($prefix)
{
    $errmsg = "";
    
    $entry =  explode("/",$prefix);
    $subnet = $entry[0];
    $mask = $entry[1];

    if(validate_ipv4_address($subnet))
        $errmsg .= "<br>"."Please enter valid Network Prefix (".$prefix.")";
    if(validate_subnet(trim($mask)))
        $errmsg .= "<br>"."Please enter valid Network Subnet Mask (".$prefix.")";

//    if(!validate_network_and_mask($subnet,$mask))
 //       $errmsg .= "<br>"."Please enter valid Dest Network Prefix (row ".$i.") host bits are not zero";

   return $errmsg ;
}

function validate_network_and_mask($subnet, $mask)
{

    $ipLong = ip2long32($subnet);
/*    echo "ipLong=".$ipLong;
    echo "test=" . (ip2long32("1.0.0.0")<<8);
    echo "subnet =" . $subnet;
    echo "mask=" .$mask;
    echo "sv = " . (($ipLong << $mask)^0);*/
    if ( ( ($ipLong << $mask) ^ 0) == true ) {
        return 1;//IP and prefix pair is not valid
    }

    return 0;

}

function ip2long32($ip) {
    return ( ip2long($ip) & 0xFFFFFFFF );
}

function print_custom_messages($message,$type)
{
        if($type=="error")
        {
                $msg  =  '<div class="alert alert-error">'. PHP_EOL;
                $msg .=  '<div class="pull-left" style="padding:0 5px 0 0"><i class="oicon-exclamation-red"></i></div>'. PHP_EOL;
                $msg .=  '<div>Errors:'.$message.'</div>'. PHP_EOL;
                $msg .=   '</div>'. PHP_EOL;
        }
        else
        {
                $msg ='<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>'.PHP_EOL;
                $msg .='<div>'.$message.'</div>'.PHP_EOL;
                $msg .='</div>';
        }
        echo $msg;
}

function validate_time_exabgp($time)
{
    if(!empty($time))
    {
        if(!validate_number($time))
        {
            if($time >=1 && $time <= 600)
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }
        else
        {
            return 1;
        }
    }
    else
    {
        return 1;
    }

}

function validate_duplicate($data)
{
    if(!is_array($data))
    {
        $data = array($data);
    }
    if(count(array_unique($data))<count($data))
    {
        return 1;
    }
    else
    {
        return 0;
    }

}



?>

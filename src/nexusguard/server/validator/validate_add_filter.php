<?php

include_once("/opt/observium/nexusguard/validator/functions.php");
include_once("/opt/observium/nexusguard/logger/logger.php");
include_once("/opt/observium/nexusguard/common/functions.inc.php");

function validate_add_filter($data)
{
$logtype="Filter_validator";

log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    $keys=array_keys($data);
    $errmsg="";

    foreach($keys as $key)
     {

        switch($key)
        {
            case 'filter_name':
                  if(empty($data['filter_name']))
                  {
                       $errmsg .= "<br>"."Filter name Cannot be left blank.";
                  }


                    if(is_numeric($data['filter_name']))
                    {
                        $errmsg .= "<br>"."Invalid Filter name.";
                    }

                  if( $data['filter_name']!= '')
                   {
                       $filter_names=dbFetchRows('select * from nxg_traffic_filter where pop_id='.$data['pop_id'].' and  filter_name="'.$data['filter_name'].'"');
                       foreach($filter_names as $filter_name)
                        {
                          $matchValue = strcmp($data['filter_name'],$filter_name['filter_name']);
                          if( $matchValue == 0)
                            {
                               $errmsg .= '<br>'.'Filter name already exist.';
                            }
                        }

                    }
                    break;
            case 'filter_type':
                        if (!array_key_exists('pop_id',$data))
                         {
                            $errmsg .= "<br>"."PoP name cannot be left blank."; 
                         }
                    break;
            case 'pop_id':
                        if(is_null($data['pop_id'])) 
                        {
                            $errmsg .= "<br>"."PoP name cannot be left blank.";
                        }
                    break;
            case 'customer_name':
                    break;
            case 'network_prefix_ip':
                    if(!empty($data['network_prefix_ip'])){
                    $ret=validate_ipv4_address($data['network_prefix_ip']);
                    if($ret!=0)
                    {
                        $errmsg .= "<br>"."Invalid IP Address for Network Prefix. Please enter correct ip address";
                    }
                    }else{
                        $errmsg .= "<br>"."IP Address for Network Prefix Cannot be left blank.";
                    }
                    break;
            case 'network_prefix_subnet_ip':
                    if($data['network_prefix_subnet_ip']=="0")
                    {
                        $errmsg .= "<br>"."Invalid Subnet.Please enter correct Subnet";
                    }else if(empty($data['network_prefix_subnet_ip'])){
                         $errmsg .= "<br>"."Subnet Cannot be left blank .";
                    }else{
                        $ret=validate_subnet($data['network_prefix_subnet_ip']);
                            if($ret!=0)
                            {
                                $errmsg .= "<br>"."Invalid Subnet.Please enter correct Subnet";
                            }
                    }

                     break;
            case 'protocol':
                    break;
             case 'port_no':
                    break;
            case 'comment':
                    break;
            case 'user_name':
                    break;
            case 'default':
                    break;
        }
     }
  return $errmsg;
}
?>

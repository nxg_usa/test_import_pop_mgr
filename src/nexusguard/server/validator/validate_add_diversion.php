<?php
include_once("/opt/observium/nexusguard/validator/functions.php");
include_once("/opt/observium/nexusguard/logger/logger.php");
include_once("/opt/observium/nexusguard/common/functions.inc.php");

function validate_add_diversion($data)
{
$logtype="Validator";

log_trace($logtype, __FILE__, __FUNCTION__, "Begin");
  //Get Form names from input data. Validate each keys value.
    $keys=array_keys($data);
    $errmsg="";

    foreach($keys as $key)
     {

        switch($key)
        {
            case 'diversion_name':
                  if(empty($data['diversion_name']))
                    {
                        $errmsg .= "<br>"."Diversion name Cannot be left blank";
                    }
                     if (is_numeric($data['diversion_name']))
                  {
                      $errmsg .= "<br>"."Invalid Diversion name.";
                  }

                  if( $data['diversion_name']!= '')
                  {
                       if($data['pop_id']=="000")
                        {
                        $diversion_names=dbFetchRows('select * from nxg_diversion_details where name="'.$data['diversion_name'].'"');

                           foreach($diversion_names as $diversion_name)
                          {
                              $matchValue = strcmp($data['diversion_name'],$diversion_name['name']);
                                if( $matchValue == 0)
                                {
                                $errmsg .= '<br>'.'Diversion name already exist.';
                                break;
                                }
                           }
                        } 
                    else{
                        $diversion_names=dbFetchRows('select * from nxg_diversion_details where name="'.$data['diversion_name'].'" and pop='.$data['pop_id'].'');
                        foreach( $diversion_names as $diversion_name)
                        {
                            $matchValue = strcmp($data['diversion_name'],$diversion_name['name']);     
                            $match_pop_id = strcmp($data['pop_id'],$diversion_name['pop']);
 
                               if( $matchValue == 0 && $match_pop_id == 0){
                                     $errmsg .= '<br>'.'Diversion name already exist.';
                                }
                        }
                      }
                    }
                    if (!array_key_exists('diversion_type',$data)) {
                        $errmsg .= "<br>"."Diversion type can not left blank";
                    }

                    if ($data['network_prefix_ip']!=""  ) 
                    {
                        if($data['network_prefix_subnet_ip']=="0")
                    {
                        $errmsg .= "<br>"."Invalid Destination Subnet.Please enter Destination correct Subnet.";
                    }else if(empty($data['network_prefix_subnet_ip'])){
                         $errmsg .= "<br>"."Destination Subnet Cannot be left blank .";
                    }else{
                        $ret=validate_subnet($data['network_prefix_subnet_ip']);
                            if($ret!=0)
                            {
                                $errmsg .= "<br>"."Invalid Destination Subnet.Please enter correct Destination Subnet.";
                            }
                        }
                    }
                     if ($data['source_ip']!=""  )
                    {
                        if($data['source_subnet']=="0")
                    {
                        $errmsg .= "<br>"."Invalid Source Subnet.Please enter correct Source Subnet.";
                    }else if(empty($data['source_subnet'])){
                         $errmsg .= "<br>"." Source Subnet Cannot be left blank .";
                    }else{
                        $ret=validate_subnet($data['source_subnet']);
                            if($ret!=0)
                            {
                                $errmsg .= "<br>"."Invalid Source Subnet.Please enter correct Source Subnet.";
                            }
                        }
                    }

                    if ($data['network_prefix_subnet_ip']!="" ) {
                        if($data['network_prefix_ip']==""){
                        $errmsg .= "<br>"."Destination IP can not left blank if Destination Subnet entered.";
                    }
                    }
                    if ($data['source_subnet']!="" ) {
                        if($data['source_ip']==""){
                        $errmsg .= "<br>"."Source IP can not left blank if Source Subnet  entered.";
                    }
                    }
                    break;
            case 'diversion_type':
                    if (is_null($data['diversion_type'])) {
                        $errmsg .= "<br>"."Diversion type can not left blank.";
                    }
                    break;
            case 'pop_name':
                  if(empty($data['pop_name']))
                    {
                        $errmsg .= "<br>"."POP Name Cannot be left blank.";
                    }
                    break;
            case 'network_prefix_ip':
                    break;
            case 'network_prefix_subnet_ip':
                     break;
            case 'source_ip':
                    break;
            case 'source_subnet':
                    break;
            case 'protocol':
                    break;
              case 'port_no':
                    break;
              case 'source_port':
                    break;
            case 'comment':
                    break;
            case 'default':
                    break;
        }
     }
        return $errmsg;
}
?>

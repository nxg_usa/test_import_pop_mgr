<?php

include_once "/opt/observium/nexusguard/validator/functions.php";
include_once "/opt/observium/nexusguard/logger/logger.php";
include_once "/opt/observium/nexusguard/common/functions.inc.php";
include_once "/opt/observium/nexusguard/db/db_pop_functions.php";


/*
This function will validatei add isp form details. 
*/
function validate_isp_details_form($data)
{
    $logtype="Validator";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    //Get Form names from input data. Validate each keys value.
    $keys=array_keys($data);
    $errmsg="";
    $billing_model = $data['billing_model'];
    $isp_id = $data['isp_id'];
    foreach($keys as $key)
    {
        switch($key)
        {
            case 'isp_name' :  
                if(empty($data['isp_name']))
                {
                    $errmsg .= "\n"."Name Cannot be left blank";
                }
                else
                {
                    if(empty($isp_id))
                    {
                        $msg = validate_name($data['pop_id'],$data['isp_name']);
                        if(!empty($msg))
                        {
                            $errmsg .= "\n".$msg;
                        }
                    }
                }
                break;
            case 'isp_description' :  
                /*if(empty($data['name']))
                {
                    $errmsg="Name Cannot be left blank";
                }*/
                break;
             case 'pop' :
                if(empty($data['pop']))
                {
                    $errmsg .= "\n"."Pop value cannot be empty";
                }
                break;
            case 'service_interface' :  
                $ret = validate_interface($data['pop_id'], $data['service_interface'],$isp_id,$data['service_interface_unit']);
                if($ret != "" )
                {   
                    $errmsg .= $ret;
                }
                break;
            case 'service_interface_vlan' :
                if(empty($data['service_interface_vlan']))
                {
                    continue;
                }
                else
                {
                    $vlan=$data['service_interface_vlan'];
                    $ret = validate_all_vlans($vlan);
                    if($ret != 0)
                    {
                        $errmsg .= "\n"."Service interface vlan validation error. Vlan Should be between 1 to 4095";
                    }
                }
                break;


            case 'service_interface_unit' :  
                if(empty($data['service_interface_unit']) && $data['service_interface_unit']!= 0 )
                {
                    $errmsg .= "\n"."Service interface unit cannot be left blank";
                    
                }
                else
                {
                    $vlan=$data['service_interface_unit'];
                    $ret = validate_int_array($vlan);
                    if($ret != 0)
                    {
                        $errmsg .= "\n"."Service interface unit validation error. Unit Should be between 1 to 4095";
                    }
                }
                break;

            case 'local_ip' :  
                $ret=validate_all_ipv4_address($data['local_ip'],false);
                if($ret!=0)
                {
                    $errmsg .= "\n"."Invalid IP Address for Local IP Address. Please enter correct ip address";

                }
                $ret = validate_duplicate($data['local_ip']);
                if($ret != 0 )
                {
                    $errmsg .= "\n"."Invalid BGP Configuration. Please use unique ip address Local addresses";
                }
                    
                break;
            case 'local_ip_subnet' :
                $ret=validate_all_subnet($data['local_ip_subnet']);
                if($ret!=0)
                {
                    $errmsg .= "\n"."Invalid subnet for Local IP Address. Please enter correct subnet. Subnet value should be between 0 to 32";

                }

                break;

            case 'neighbour_ip' :  
                $ret=validate_all_ipv4_address($data['neighbour_ip'],false);
                if($ret!=0)
                {
                    $errmsg .= "\n"."Invalid IP Address for Neighbor IP Address. Please enter correct ip address";

                }
                $ret = validate_duplicate($data['neighbour_ip']);
                if($ret != 0 )
                {
                    $errmsg .= "\n"."Invalid BGP Configuration. Please use unique ip address Neighbor addresses";
                }
                break;
 /*           case 'neighbour_ip_subnet' :
                $ret=validate_all_subnet($data['neighbour_ip_subnet']);
                if($ret!=0)
                {
                    $errmsg .= "\n"."Invalid subnet for Neighbor IP Address. Please enter correct subnet. Subnet value should be between 0 to 32";

                }

                break;
*/
            case 'local_as' :  
                $ret=validate_all_AS($data['local_as']);
                if ($ret!=0) 
                {
                    $errmsg .= "\n"."Incorrect value for Local ASN";    
                }
                break;
             case 'peer_as' :
                $ret=validate_all_AS($data['peer_as']);
                if ($ret!=0)
                {
                    $errmsg .= "\n"."Incorrect value for Peer ASN";
                }
                break;
           case 'billing_fix_rate' :
                $ret=validate_number($data['billing_fix_rate']);
                if ($ret!=0)
                {
                    $errmsg .= "\n"."Incorrect value for rate";
                }
                break;
           case 'billing_fix_cost' :
                $ret=validate_number($data['billing_fix_cost']);
                if ($ret!=0)
                {
                    $errmsg .= "\n"."Incorrect value for cost";
                }


            break;
             case 'slab_rate' :
                if($billing_model == "committed_information_rate")
                {
                    $ret=validate_int_array($data['slab_rate']);
                    if ($ret!=0)
                    {
                        $errmsg .= "\n"."Incorrect value for Excess info rate";
                    }
                }
            break;
           case 'slab_cost' :
                if($billing_model == "committed_information_rate")
                {
                    $ret=validate_int_array($data['slab_cost']);
                    if ($ret!=0)
                    {
                        $errmsg .= "\n"."Incorrect value for cost";
                    }

                }
            break;
            case 'input_data' :
                if($billing_model === "data_transfer")
                {
                    $ret=validate_number($data['input_data']);
                    if ($ret!=0)
                    {
                        $errmsg .= "\n"."Incorrect value for Data in ";
                    }
                }
            break;
             case 'output_data' :
                if($billing_model === "data_transfer")
                {
                    $ret=validate_number($data['output_data']);
                    if ($ret!=0)
                    {
                        $errmsg .= "\n"."Incorrect value for Data out ";
                    }
                }
            break;
            case 'data_transfer_cost' :
                if($billing_model === "data_transfer")
                {
                    $ret=validate_number($data['data_transfer_cost']);
                    if ($ret!=0)
                    {
                        $errmsg .= "\n"."Incorrect value for Cost ";
                    }
                }
            break;
            case 'fixed_cost' :
                if($billing_model === "fixed")
                {
                    $ret=validate_number($data['fixed_cost']);
                    if ($ret!=0)
                    {
                        $errmsg .= "\n"."Incorrect value for Fixed cost ";
                    }
                }

            break;

            case 'billing_cycle' :
                 $ret=validate_number($data['billing_cycle']);
                if ($ret!=0)
                {
                    $errmsg .= "\n"."Incorrect value for days in Billing cycle ";
                }


            break;
            case 'starting_date' :
            $ret=validate_number($data['starting_date']);
                if ($ret!=0)
                {
                    $errmsg .= "\n"."Incorrect value for starting date ";
                }
                else
                {
                    $start_date = $data['starting_date'];
                    if( $start_date < 0  || $start_date > 31 )
                    {
                        $errmsg .= "\n"."Incorrect value for starting date ";
                    }
                }
            break; 
/*           case 'md5_passphrase':
            $ret = validate_md5passphrase($data['md5_passphrase']);
            if($ret != "" )
            {
                $errmsg .= $ret;
            }
            break; 
*/
           case 'restart_timer':
            $ret = validate_graceful_restart($data['restart_timer']);
            if($ret != "" )
            {
                $errmsg .= $ret;
            }
            break; 
           case 'stale_routes_time':
            $ret = validate_stale_timeout($data['stale_routes_time']);
            if($ret != "" )
            {
                $errmsg .= $ret;
            }
            break; 
            case 'billing_domain':
            if(empty($data['billing_domain']))
            {
                $errmsg .= "\nBilling domain can not be left blank";
            }
            break;
            case 'lag_interface':
/*            if(validate_lag_interface($data['lag_interface'],$data['service_interface']))
            {
                $errmsg .= "\nInvalid member port combination. Please use unique port for member ports for ae port";
            }
*/
            break;

            case 'bgp_community_names':
            if(empty($data['bgp_community_names']))
            {
                $errmsg .= "\nBGP communities can not be left blank. Atleast one community must be present.";
            }
            else
            {
                $err = validate_bgp_community($data['bgp_community_names'][0]);
                if($err != "")
                {
                    $errmsg .= $err;
                }
            }
            break;
           case 'isp_type':
            if(empty($data['isp_type']) && $data['isp_type'] != 0)
            {
                $errmsg .= "\n ISP Type can not be left blank";
            }
           case 'Submit' :  
                break;
            case 'default' :  
                break;
        }
    }
   $ret = validate_bgp_as_config($data['local_as'],$data['peer_as']);
    if($ret != 0 )
    {
        $errmsg .= "\n"."Local AS and Peer AS should not be same";
    }
        
   $ret = validate_bgp_ip_config($data['local_ip'],$data['neighbour_ip']);
    if($ret != "" )
    {
        $errmsg .= "\n".$ret;
    }
    $ret = validate_service_interface($data['service_interface'],$data['service_interface_unit']);
    if($ret != "" )
    {
        $errmsg .= "\n".$ret;
    }

    if(!empty($errmsg))
        write_error_response("ISPErr01",$errmsg);
}

function validate_interface($pop_id,$interfaces,$isp_id,$units)
{
    $logtype="Validator";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    $err = "";
    $i = 0;
    if(!is_array($interfaces))
    {
        $interfaces = array($interfaces);
        $units = array($units);
    }
    foreach($interfaces  as $interface)
    {
        if(!empty($interface))
        {
            $intf_unit = $units[$i];
            if($isp_id) 
                $interface_from_db = is_isp_interface_used($pop_id,$interface,$isp_id,$intf_unit);
            else
                $interface_from_db = is_interface_used($pop_id,$interface,$intf_unit);

            if(empty($interface_from_db))
            {
				$i++; 
                continue;
            }
            else
            {
                $err .= "\n"."Service Interface $interface has been used. Please select another interface";
            }
        }
        else
        {
            $err .= "\n"."Service Interface at row $i can not be left blank";
        }
        $i++; 
    }
    log_trace($logtype, __FILE__, __FUNCTION__, "End");
    return $err;
}

function validate_name($pop_id,$isp_name)
{
    $logtype="Validator";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");
    
    $err = "";

    $isp_from_db = is_ispname_used($pop_id,$isp_name);

    $lenth = strlen($isp_name);

    if($lenth > 128 )
    {
        $err .= "\n ISP name should not be more than 128 chars";
    }
    if(!empty($isp_from_db))
    {
        $err .= "\n ISP with $isp_name is already configure. Please provide different name";
    }
    if((strpos($isp_name, " ")))
    {
        $err .= "\nISP name should not contain space";
    }

    return $err;
    log_trace($logtype, __FILE__, __FUNCTION__, "End");
}
      
function validate_bgp_as_config($local_as, $peer_as)
{
    $logtype="Validator";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    $ret = 0;
    
    if(!is_array($local_as))
    {
        $local_as = array($local_as);
        $peer_as = array($peer_as);
    }
    $cnt = count($local_as);
    for($i=0;$i<$cnt;$i++)
    {
        if( $local_as[$i] == $peer_as[$i] )
        {
            $ret = 1;
            break;
        }
    }
    log_trace($logtype, __FILE__, __FUNCTION__, "End");
    return $ret;
}
function validate_md5passphrase($md5_passphrase)
{
    $logtype="Validator";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");
    
    $err="";
    if(!is_array($md5_passphrase))
    {
        $md5_passphrase = array($md5_passphrase);
    }
    foreach($md5_passphrase as $md5)
    {
        if(empty($md5))
        {
            $err .= "\n"."MD5 Passphrase can not be left blank";
        }
    }
    log_trace($logtype, __FILE__, __FUNCTION__, "End");
    return $err;
    
}
function validate_graceful_restart($restart_timer)
{

    $logtype="Validator";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");
    $err="";
    if(!is_array($restart_timer))
    {
        $restart_timer = array($restart_timer);
    }
    foreach($restart_timer as $value)
    {
        if(empty($value))
        {
                continue;
        }
        $ret = validate_time_exabgp($value);
        if($ret != 0)
        {
            $err .= "\n"."Invalid value for Graceful Restart Timer. Timer value should be between 1 to 600";
        }
    } 
    log_trace($logtype, __FILE__, __FUNCTION__, "End");
    return $err;
}
function validate_stale_timeout($stale_timeout)
{
    $logtype="Validator";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");
    $err="";
    
    if(!is_array($stale_timeout))
    {
        $stale_timeout = array($stale_timeout);
    }

    foreach($stale_timeout as $timeout)
    {
        $ret = validate_time_exabgp($timeout);
        if($ret != 0)
        {
            $err .= "\n"."Invalid value for  Stale Routes Time. Time value should be between 1 to 600";
        }
    }

    log_trace($logtype, __FILE__, __FUNCTION__, "End");
    return $err;
}

function validate_bgp_community ($bgp_community)
{
    $logtype="Validator";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");
    $err="";
    
    if(!is_array($bgp_community))
    {
        $bgp_community = array($bgp_community);
    }

    foreach($bgp_community  as $config )
    {
        $tmp = explode("~~~",$config);
        $community_name = trim($tmp[0]);
        $community_members = trim($tmp[1]);
        $tmp_member = explode(":",$community_members);
        $community_members_1 = trim($tmp_member[0]);
        $community_members_2 = trim($tmp_member[1]);
        if(empty($community_name) || $community_name == "" )
        {
            $err .= "\n"."BGP Community Name can not be left blank";
        }
        else
        {
            if((strpos($community_name, " ")))
            {
                $err .= "\n"."BGP Community Name can not contain white spaces";
            }
        }
        if( (empty($community_members_1) && $community_members_1 != 0 ) || ( empty($community_members_2) && $community_members_2 != 0  ))
        {
            $err .= "\n"."BGP Community members should contain two 4 bytes number separeted by ':'";
        }
        else
        {
            if(validate_number($community_members_1) || validate_number($community_members_2))
            {
                $err .= "\n"."BGP Community members should contain two 4 bytes number separeted by ':'";
            }
            else
            {
                if($community_members_1 < 0 || $community_members_1 > 65535 )
                {
                    $err .= "\n"."BGP Community members must be between 0 to 65535";
                }
                if($community_members_2 < 0 || $community_members_2 > 65535 )
                {
                    $err .= "\n"."BGP Community members must be between 0 to 65535";
                }

            }
        }
    }      
    log_trace($logtype, __FILE__, __FUNCTION__, "End");
    return $err;
   
}
function validate_bgp_ip_config($local_ip ,$neighbor_ip)
{
    $logtype="Validator";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");
    $err="";

    if(!is_array($local_ip))
    {
        $local_ip = array($local_ip);
        $neighbor_ip = array($neighbor_ip);
    }
    for($i=0;$i<count($local_ip);$i++)
    {
        $lo_ip = $local_ip[$i];
        for($j=0;$j<count($neighbor_ip);$j++)
        {
            $nbh_ip = $neighbor_ip[$j];
            if($lo_ip == $nbh_ip)
            {
                $err .= "Local & Neighbor IP Address should not be same";
                break;
            }
        }
        break;
    }
    return $err;
}
function validate_lag_interface($member_interfaces,$service_interface)
{
    $logtype="Validator";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");
      
    $array = array();
    if(!is_array($member_interfaces))
    {
        $member_interfaces = array($member_interfaces);
    }
    if(!is_array($service_interface))
    {
        $member_interfaces = array($service_interface);
    }
   
 
    for($i =0 ;$i<count($member_interfaces);$i++)
    {
        $interfaces = $member_interfaces[$i][0];
        if(!is_array($interfaces))
        {
            $interfaces = array($interfaces);
        }
        foreach($interfaces as $value)
        {
            $array[] = $value;
        }
    }
    $ret = validate_duplicate($array);
    
    return $ret;
}
function validate_service_interface($interfaces,$vlans)
{
    $err = "";

    if(!is_array($interfaces))
    {
        $interfaces = array($interfaces);
        $vlans = array($vlans);
    }

    for($i=0;$i<count($interfaces);$i++)
    {
        for($j=0;$j<count($interfaces);$j++)
        {
        if($i==$j)
            continue;
            if(($interfaces[$i] == $interfaces[$j]) && ($vlans[$i]==$vlans[$j]))
            {
                $err = "Please use unique unit numbers for same interfaces";
            }
        }
    }
    return $err;
}  

function validate_delete_isp($isp_id)
{

    $err = "";
    $cnt = get_active_cust_conn($isp_id);

    if($cnt != 0 )
    {
        $err = "Can not delete neighbor if active customer connection is present";

    }
    return $err;
}
?>

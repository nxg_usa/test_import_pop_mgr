NXG_DB=observium
NXG_USER=root
NXG_PASSWORD=s3rr0$
IPAM_DB=phpipam
SUBNET_FILE=/tmp/ausm_subnet.txt
SUBNET_EXTRACTED_FILE=/tmp/ausm_subnet_extracted.txt
IP_ADDRESSES_FILE=/tmp/ausm_subnet_ip_addesses.txt



######################## INSERT INTO SECTIONS #####################

POP_ID=`echo "select distinct GROUP_CONCAT(id) as id  from nxg_pop_details;" |  mysql -D $NXG_DB  -u $NXG_USER -p$NXG_PASSWORD | grep -v 'id'`


for POP_ID_IN_NXG in $(echo $POP_ID | sed "s/,/ /g")
do
    POP_NAME_IN_NXG=`echo "select pop_name from nxg_pop_details where id=$POP_ID_IN_NXG;" |  mysql -D $NXG_DB  -u $NXG_USER -p$NXG_PASSWORD | grep -v 'pop_name'`

    POP_NAME_IN_IPAM=`echo "select distinct name from sections;" | mysql -D $IPAM_DB  -u $NXG_USER -p$NXG_PASSWORD | grep -v 'name'`

    SECTION=`echo $POP_NAME_IN_IPAM | grep -w $POP_NAME_IN_NXG`
    if [ x"$SECTION" = x ]
    then
        echo "INSERT INTO sections (id, name, description, masterSection, permissions, strictMode, subnetOrdering, sections.order, editDate, showVLAN, showVRF) VALUES (NULL,'$POP_NAME_IN_NXG','',0,'{\"2\":\"1\"}','1','default',NULL,NULL,1,1);" | mysql -D $IPAM_DB  -u $NXG_USER -p$NXG_PASSWORD
    fi


###################### END INSERT INTO SECTIONS #######################

###########################   INSERT INTO SUBNETS ##########################
    echo "select ip_address from nxg_pop_interface_ip_mgmt where pop_id=$POP_ID_IN_NXG;" | mysql -D $NXG_DB -u $NXG_USER -p$NXG_PASSWORD | grep -v 'ip_address' > $SUBNET_FILE

    rm -rf $SUBNET_EXTRACTED_FILE

    while read line
    do
       subnets=`echo $line | awk -F '.' '{print $1"."$2"."$3}'` 
       prev_subnets=`cat $SUBNET_EXTRACTED_FILE | grep $subnets`
       if [ x"$prev_subnets" = x ]
       then
             echo $subnets >> $SUBNET_EXTRACTED_FILE
       fi

    done < $SUBNET_FILE

    while read line
    do
        section_id=`echo "select id from sections where name='$POP_NAME_IN_NXG';" |  mysql -D $IPAM_DB -u $NXG_USER -p$NXG_PASSWORD | grep -v 'id'`
        subnet=$line".0"
        subnet_from_database=`echo "select inet_ntoa(subnet) from subnets where sectionId=$section_id;" | mysql -D $IPAM_DB -u $NXG_USER -p$NXG_PASSWORD | grep -v 'inet_ntoa(subnet)'`
        subnet_from_database_match=`echo $subnet_from_database | grep -w $subnet`
        if [ $? != 0 ]
        then
        echo "INSERT INTO subnets (id, subnet, mask, sectionId, description, vrfId, masterSubnetId, allowRequests, vlanId, showName, permissions, pingSubnet, discoverSubnet, isFolder, editDate) VALUES (NULL,inet_aton('$subnet'),'24',$section_id,'',0,0,0,0,0,'{\"3\":\"1\",\"2\":\"2\"}',0,'0',0,NULL);" | mysql -D $IPAM_DB  -u $NXG_USER -p$NXG_PASSWORD
        fi
    


    done < $SUBNET_EXTRACTED_FILE

#################### END INSERT INTO SUBNETS ###########################

#################### INSERT INTO IP ADDRESSES  #######################

echo "select ip_address from nxg_pop_interface_ip_mgmt where pop_id=$POP_ID_IN_NXG;" | mysql -D $NXG_DB -u $NXG_USER -p$NXG_PASSWORD | grep -v 'ip_address' > $IP_ADDRESSES_FILE



looping_in_sectionid=`echo "select id from sections where name='$POP_NAME_IN_NXG';" | mysql -D $IPAM_DB -u $NXG_USER -p$NXG_PASSWORD | grep -v 'id'`


subnets_from_ipam_database=`echo "select GROUP_CONCAT(inet_ntoa(subnet)) as subnet from subnets where sectionId=$looping_in_sectionid;" | mysql -D $IPAM_DB -u $NXG_USER -p$NXG_PASSWORD | grep -v 'subnet'`

for ipam_subnet in $(echo $subnets_from_ipam_database | sed "s/,/ /g")
do
subnet_id=`echo "select id from subnets where subnet=inet_aton('$ipam_subnet') and sectionId=$looping_in_sectionid;" |  mysql -D $IPAM_DB -u $NXG_USER -p$NXG_PASSWORD | grep -v 'id'`

    while read line 
    do
        ip_to_match=`echo $line | awk -F '.' '{print $1"."$2"."$3}'`
        subnet_to_match=`echo $ipam_subnet | awk -F '.' '{print $1"."$2"."$3}'`
        if [ $ip_to_match = $subnet_to_match ]
        then
                ip_addr_list=` echo "select inet_ntoa(ip_addr) from ipaddresses where subnetId=$subnet_id;" | mysql -D $IPAM_DB -u $NXG_USER -p$NXG_PASSWORD | grep -v 'ip_addr'`    
                ip_addr_list_match=`echo $ip_addr_list | grep -w $line`                    
                if [ x"$ip_addr_list_match" = x ]
                then
                 echo "INSERT INTO ipaddresses (id, subnetId, ip_addr, description, dns_name, mac, owner, state, switch, port, note, lastSeen, excludePing, editDate) VALUES (NULL,$subnet_id,inet_aton('$line'),'','','','','','1','','','','',NULL);" | mysql -D $IPAM_DB  -u $NXG_USER -p$NXG_PASSWORD                
               fi
               
       fi
       
   done < $IP_ADDRESSES_FILE


done

#####################  END INSERT INTO IP ADDRESSES ####################

done


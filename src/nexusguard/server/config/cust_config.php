<?php
include_once("/opt/observium/includes/defaults.inc.php");
include_once("/opt/observium/config.php");
include_once("/opt/observium/includes/definitions.inc.php");
include_once("/opt/observium/nexusguard/common/functions.inc.php");
include_once('/opt/observium/nexusguard/db/db_isp_functions.php');
include_once('/opt/observium/nexusguard/db/db_pop_functions.php');
include_once('/opt/observium/nexusguard/db/db_customer_functions.php');
include_once('/opt/observium/nexusguard/resource/ip_mgmt_config.php');


function get_cust_config($cust_data)
{
    $logtype="CUSTConfig";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");


/*    if(is_array( $cust_data['inbound_pop[]']) )
        $pop_id = $cust_data['inbound_pop[]'][0];
    else
        $pop_id = $cust_data['inbound_pop[]'];
*/
    $config = $cust_data;
    $conn_data = $cust_data["connection"];
    $pop_id =  $conn_data["pop_id"];
    $cust_id = $cust_data["cust_id"];
    $pop_details = get_sinlge_pop_detail($pop_id);
    $config["pop_name"] = $pop_details["pop_name"];
    $config["interfaces"]= array();

    $gr_interface = array();
    $gr_interface["interface_name"] = $conn_data["conn_if"];
    $gr_interface["interface_configuration"] = array();
    $gr_unit =$conn_data["conn_if_unit"];
    $gr_interface["interface_configuration"][0]["unit"] = $gr_unit;
    $gr_interface["interface_configuration"][0]["description"] = $cust_data["customer_name"] . " : CPE : ".$conn_data["conn_if"]." -Clean Data ";
    $gr_interface["interface_configuration"][0]["vrname"] = "VR-C-".$cust_data["customer_name"];

    $subtype ="ge";
    if($conn_data["customer_conn"] =="tunnel")
    {
        $gr_interface["interface_configuration"][0]["tunnel"] =  array();
        $gr_interface["interface_configuration"][0]["tunnel"]["source"] = $conn_data["endpoint_ip_local"]; 
        $gr_interface["interface_configuration"][0]["tunnel"]["destination"] =  $conn_data["endpoint_ip_remote"];
        $gr_interface["interface_configuration"][0]["tunnel"]["routing_instance_destination"]= "VR-CORE";
        $subtype ="gr";
    }

    $gr_ip=$conn_data["inside_ip_local"];
    $gr_interface["interface_configuration"][0]["ipv4_address"]= $gr_ip;
    $gr_interface["interface_configuration"][0]["vrname"] = "VR-C-".$cust_data["customer_name"];
    $gr_interface["interface_configuration"][0]["iftype"] =  $subtype;
    $gr_interface["interface_configuration"][0]["dbtype"] =  "customer";
    $config["interfaces"][0]= $gr_interface;
//    add_config_to_db($pop_id, $gr_unit,$gr_interface["interface_name"],$gr_ip,"VR-C-".$cust_data["customer_name"],"customer",$subtype);

    $first_conn = "yes";
    $conn_number = 1;
    if(isset($cust_id))
    {
        $results = dbFetchRows("select * from nxg_customer_conn where cust_id= ? and pop_id=?",array($cust_id, $pop_id));
        if(count($results) > 0)
        {
            $first_conn = "no";
            $conn_number = count($results) + 1;
        }
    }

    if($first_conn == "yes")
    {
        $lt_interface = array();
        $lt_interface["interface_name"] = get_interface_name("customer","ifl"); // TBD
        $lt_interface["interface_configuration"] = array();
        $lt_cust_isp_unit = get_unit_by_type($pop_id,$lt_interface["interface_name"],"customer");
        $lt_isp_cust_unit = $lt_cust_isp_unit + 1;
        $lt_interface["interface_configuration"][0]["unit"] =  $lt_cust_isp_unit;
        $lt_interface["interface_configuration"][0]["description"] = "Interface for " . $cust_data["customer_name"] . "  to VR-CORE (description is TBD)";
        $lt_interface["interface_configuration"][0]["peer_unit"] = $lt_isp_cust_unit;
        $lt0_ip=get_ip_address_by_subnet($pop_id,$gr_interface["interface_name"],"31","customer","ifl");
        $lt_interface["interface_configuration"][0]["ipv4_address"] = $lt0_ip[0]."/31";
        $lt_interface["interface_configuration"][0]["vrname"] = "VR-C-".$cust_data["customer_name"];
        $lt_interface["interface_configuration"][0]["iftype"] = "ifl";
        $lt_interface["interface_configuration"][0]["dbtype"] = "customer";
        //add_config_to_db($pop_id, $lt_cust_isp_unit,$lt_interface["interface_name"],$lt0_ip[0],"VR-C-".$cust_data["customer_name"],"customer","ifl");

        $lt_interface["interface_configuration"][1]["unit"] = $lt_isp_cust_unit;
        $lt_interface["interface_configuration"][1]["description"] = "Interface for VR-CORE to " . $cust_data["customer_name"] ." (description is TBD)";
        $lt_interface["interface_configuration"][1]["peer_unit"] = $lt_cust_isp_unit;
        $lt1_ip=$lt0_ip[1];
        $lt_interface["interface_configuration"][1]["ipv4_address"] = $lt1_ip."/31";
        $lt_interface["interface_configuration"][1]["vrname"] = "VR-CORE";
        $lt_interface["interface_configuration"][1]["iftype"] = "ifl";
        $lt_interface["interface_configuration"][1]["dbtype"] = "customer";
//        add_config_to_db($pop_id, $lt_isp_cust_unit,$lt_interface["interface_name"],$lt1_ip,"VR-CORE","customer","ifl");

        $lt_cust_insrtn_unit =  $lt_isp_cust_unit+1;
        $lt_insrtn_cust_unit =  $lt_cust_insrtn_unit + 1;
        $lt_interface["interface_configuration"][2]["unit"] = $lt_cust_insrtn_unit;
        $lt_interface["interface_configuration"][2]["description"] = "Interface for " . $cust_data["customer_name"] . "  to VR-INSERTION (description is TBD)";
        $lt_interface["interface_configuration"][2]["peer_unit"] = $lt_insrtn_cust_unit;
        $lt2_ip= increment_ip_by_one($lt1_ip);
        $lt_interface["interface_configuration"][2]["ipv4_address"] = $lt2_ip."/31";
        $lt_interface["interface_configuration"][2]["vrname"] = "VR-C-".$cust_data["customer_name"];;
        $lt_interface["interface_configuration"][2]["iftype"] = "ifl";
        $lt_interface["interface_configuration"][2]["dbtype"] = "customer";
//        add_config_to_db($pop_id, $lt_cust_insrtn_unit,$lt_interface["interface_name"],$lt2_ip,"VR-C-".$cust_data["customer_name"],"customer","ifl");

        $lt_interface["interface_configuration"][3]["unit"] = $lt_insrtn_cust_unit;
        $lt_interface["interface_configuration"][3]["description"] = "Interface for VR-INSERTION to " . $cust_data["customer_name"] ." (description is TBD)";
        $lt_interface["interface_configuration"][3]["peer_unit"] = $lt_cust_insrtn_unit;
        $l3_ip = increment_ip_by_one($lt2_ip);;
        $lt_interface["interface_configuration"][3]["ipv4_address"] = $l3_ip."/31";
        $lt_interface["interface_configuration"][3]["vrname"] = "VR-INSERTION";
        $lt_interface["interface_configuration"][3]["iftype"] = "ifl";
        $lt_interface["interface_configuration"][3]["dbtype"] = "customer";
        //add_config_to_db($pop_id, $lt_insrtn_cust_unit,$lt_interface["interface_name"],$l3_ip,"VR-INSERTION","customer","ifl");

        $config["interfaces"][1]= $lt_interface;

        /*$l0_interface = array();
        $l0_interface["interface_name"] = "lo0";
        $l0_interface["interface_configuration"] = array();
        $l0_unit = get_unit_by_type($pop_id,$l0_interface["interface_name"],"lo0");
        $l0_interface["interface_configuration"][0]["unit"] =  $l0_unit;
        $l0_interface["interface_configuration"][0]["description"] = $cust_data["customer_name"] ."  : Loopback ";
        $l0_ip=get_ip_address_by_subnet($pop_id,$l0_interface["interface_name"],"32","lo0","lo0");
        $l0_interface["interface_configuration"][0]["ipv4_address"] =$l0_ip[0]."/32";
        $config["interfaces"][2]= $l0_interface;
        add_config_to_db($pop_id, $l0_unit,$l0_interface["interface_name"],$l0_ip[0],"VR-C-".$cust_data["customer_name"],"lo0","lo0");*/
    }

    $config["first_conn"] =  $first_conn;
    $config["conn_number"] = $conn_number;

/*    if(is_array($cust_data["dest_network_prefix[]"]))
    {
        for($i=0;$i< count($cust_data["dest_network_prefix[]"]);$i++)
        {
            $config["destnetprefix"][$i]["network"] =  $cust_data["dest_network_prefix[]"][$i];
            $config["destnetprefix"][$i]["mask"] =  $cust_data["dest_network_prefix_subnet[]"][$i];
        }
    }else
    {
            $config["destnetprefix"][0]["network"] =  $cust_data["dest_network_prefix[]"];
            $config["destnetprefix"][0]["mask"] =  $cust_data["dest_network_prefix_subnet[]"];
    }*/

    if(isset($cust_data['cust_id']))
    {
        $cust_id = $cust_data["cust_id"];
    //    $result = get_db_cust_details($cust_id);
    //     $config["customer_name"]=$result["name"];

        $i=0;
        foreach (dbFetchRows("select * from nxg_customer_net_service where cust_id= ?",array($cust_id)) as $entry)
        {
            $config["net_srv_prefix_array"][$i] = $entry["net_srv_prefix"];
            $i = $i + 1;
        }
    }
    else
    if(isset($cust_data['net_srv_prefix']))
    { 
            $dest_network_prefix  = explode("\n",$cust_data['net_srv_prefix']);
            if(is_array($dest_network_prefix))
            {
            for($i = 0 ; $i < count($dest_network_prefix) ;$i++)
            {
                  $config["net_srv_prefix_array"][$i]=$dest_network_prefix[$i];
            }
            }
            else
            {
            $config["net_srv_prefix_array"][0]=$dest_network_prefix;
            }
    }

    $inside_ip_only_local =  explode("/",$cust_data['connection']['inside_ip_local']);
    $config["connection"]["inside_ip_only_local"] = $inside_ip_only_local[0];


    $inside_ip_only_remote =  explode("/",$cust_data['connection']['inside_ip_remote']);
    $config["connection"]["inside_ip_only_remote"] = $inside_ip_only_remote[0];

        // Export this customer from all ISP's at ASN level 0 by default
        $i=0;
        foreach (dbFetchRows("select * from nxg_isp_details where pop_details_id = ?",array($pop_id)) as $isp_detail)
        {
            $config["isp"][$i]["isp_id"] =  $isp_detail["id"];
            $config["isp"][$i]["isp_name"] =  $isp_detail["name"];
            $config["isp"][$i]["isp_asn_level"] = 0;
            $i = $i + 1;
        }
   
        return $config;

}

function get_cust_conn_modify_config($cust_data)
{
    $logtype="CUSTConfig";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    $conn_data = $cust_data["connection"];
    $cust_id = $cust_data["cust_id"];
    $pop_id =  $conn_data["pop_id"];
    $conn_id =  $conn_data["conn_id"];

    $config = $cust_data;
    $config["interfaces"]= array();

    $gr_interface = array();
    $gr_interface["interface_name"] = $conn_data["conn_if"];
    $gr_interface["interface_configuration"] = array();
    $gr_unit =$conn_data["conn_if_unit"];
    $gr_interface["interface_configuration"][0]["unit"] = $gr_unit;
    $gr_interface["interface_configuration"][0]["description"] = $cust_data["customer_name"] . " : CPE : ". $conn_data["conn_if"] ." -Clean Data ";

    $subtype ="ge";
    if($conn_data["customer_conn"] =="tunnel")
    {
        $gr_interface["interface_configuration"][0]["tunnel"] =  array();
        $gr_interface["interface_configuration"][0]["tunnel"]["source"] = $conn_data["endpoint_ip_local"];
        $gr_interface["interface_configuration"][0]["tunnel"]["destination"] =  $conn_data["endpoint_ip_remote"];
        $gr_interface["interface_configuration"][0]["tunnel"]["routing_instance_destination"]= "VR-CORE";
        $subtype ="gr";
    }

    $gr_ip=$conn_data["inside_ip_local"];
    $gr_interface["interface_configuration"][0]["ipv4_address"]= $gr_ip;
    $config["interfaces"][0]= $gr_interface;
    //add_config_to_db($pop_id, $gr_unit,$gr_interface["interface_name"],$gr_ip,"VR-C-".$cust_data["customer_name"],"customer",$subtype);

    $inside_ip_only_local =  explode("/",$cust_data['connection']['inside_ip_local']);
    $config["connection"]["inside_ip_only_local"] = $inside_ip_only_local[0];


    $inside_ip_only_remote =  explode("/",$cust_data['connection']['inside_ip_remote']);
    $config["connection"]["inside_ip_only_remote"] = $inside_ip_only_remote[0];

    $config["conn_number"] = get_customer_conn_number($cust_id,$pop_id,$conn_id);

    $results = dbFetchRows("select * from nxg_customer_conn where cust_id= ? and pop_id=? order by conn_id asc",array($cust_id, $pop_id));
    if(count($results) > 0)
    {
        for($i = 0; $i< count($results);$i++)
        {
            if($results[$i]["conn_id"] == $conn_id)
            {
                if($results[$i]["inside_ip_local"] != $cust_data['connection']['inside_ip_local'])
                    $config["interfaces"][0]["interface_configuration"][0]["ipv4_address_old"] = $results[$i]["inside_ip_local"];

                if($results[$i]["inside_ip_remote"] != $cust_data['connection']['inside_ip_remote'])
                {       
                            $inside_ip_only_remote_old =  explode("/",$results[$i]["inside_ip_remote"]);
                            $config["connection"]["inside_ip_only_remote_old"] =  $inside_ip_only_remote_old[0];
                }
                if($results[$i]["endpoint_ip_remote"] != $cust_data['connection']['endpoint_ip_remote'])
                {
                            $inside_ip_only_remote_old =  explode("/",$results[$i]["endpoint_ip_remote"]);
                            $config["connection"]["endpoint_ip_remote_old"] =  $inside_ip_only_remote_old[0];
                }


                break;
            }
        }
    }



    return $config;
}


function get_delete_conn_config($form_data)
{
    $cust_id =$form_data["cust_id"];
    $conn_id =$form_data["connection"]["conn_id"];
    $pop_id =$form_data["connection"]["pop_id"];

    $config=$form_data;

    $pop_details = get_sinlge_pop_detail($pop_id);
    $config["pop_name"] = $pop_details["pop_name"];


    $cust_details = get_db_cust_details($cust_id);
    $conn_config = get_db_conn_if_config($pop_id,$conn_id);
    $cust_name = $cust_details['name'];

    $config['customer_name']=$cust_name;

    $vr_core_interface = array();
    $vr_intsertion_details = array();
    $interfaces = array();
    
    $intf_array = array();

    foreach($conn_config as $int_config)
    {
        $interface_name = $int_config['interface_name'];
        $interface_unit = $int_config['interface_unit'];
        if(array_key_exists($interface_name ,$intf_array))
        {
            $interface_config = $intf_array[$interface_name]['interface_configuration'];
            $cnt = count($interface_config);
            $unit_added=0;
            for($k = 0 ; $k < $cnt ;$k++)
            {    
               if($interface_config[$k]["unit"] == $interface_unit)
                    $unit_added=1;
            }
            if($unit_added==0)
            {
                $interface_config[$cnt] = array('unit'=>$interface_unit);
                $intf_array[$interface_name] = array('interface_name'=>$interface_name,'interface_configuration'=>$interface_config);
            }
        }
        else
        {
            $intf_array[$interface_name] = array('interface_name'=>$interface_name,'interface_configuration'=>array(array('unit'=>$interface_unit)));
        }
    }

    foreach($intf_array as $itf=>$ifconfig)
    {
        $interfaces[] = $ifconfig;
    }


    $last_conn = "yes";
    $conn_number = 1;
    if(isset($cust_id))
    {
        $results = dbFetchRows("select * from nxg_customer_conn where cust_id= ? and pop_id=? order by conn_id asc",array($cust_id, $pop_id));
        if(count($results) > 1)
        {
            $last_conn = "no";
           for($i = 0; $i< count($results);$i++)
           {
                if($results[$i]["conn_id"] == $conn_id)
                {
                    $conn_number = $i + 1;
                    break;
                }
           }
        }
    }

    if( $last_conn == "yes")
    {

        foreach($conn_config as $int_config)
        {
            $interface_name = $int_config['interface_name'];
            $interface_unit = $int_config['interface_unit'];
            if($int_config['routing_instance'] == 'VR-CORE')
            {
                $vr_core_interface = array('interface_name'=>$interface_name,'interface_unit'=>$interface_unit);
            }
            if($int_config['routing_instance'] == 'VR-INSERTION')
            {
                $vr_intsertion_details = array('interface_name'=>$interface_name,'interface_unit'=>$interface_unit);
            }
        }

        $config['vr_core_interface']=$vr_core_interface;
        $config['vr_intsertion_details']=$vr_intsertion_details;

        $i=0;
        foreach (dbFetchRows("select * from nxg_customer_net_service where cust_id= ? and net_srv_prefix!='ALL-PREFIXES'",array($cust_id)) as $entry)
        {
            $config["net_srv_prefix_array"][$i] = $entry["net_srv_prefix"];
            $i = $i + 1;
        }

    }

    $config['interfaces'] =$interfaces;

    $config["last_conn"] =  $last_conn;
    $config["conn_number"] = $conn_number;


    return $config;
}
function offline_cust_config($cust_data)
{
    $isp_details= array();
    $cust_id = $cust_data['cust_id'];

    $isp_names = get_db_cust_isp_details($cust_id);
    foreach($isp_names as $isp)
    {
        $isp_details[] = array('isp_name'=>$isp['name'],'isp_asn_level'=>$isp['isp_asn_level']);
    }

    $config['isp_details']=$isp_details;
    $config['name']=$cust_data['customer_name'];
    return $config;
}


function get_customer_conn_number($cust_id,$pop_id,$conn_id)
{

    $conn_number = 1;
    if(isset($cust_id))
    {
        $results = dbFetchRows("select * from nxg_customer_conn where cust_id= ? and pop_id=? order by conn_id asc",array($cust_id, $pop_id));
        if(count($results) > 1)
        {
           for($i = 0; $i< count($results);$i++)
           {
                if($results[$i]["conn_id"] == $conn_id)
                {
                    $conn_number = $i + 1;
                    break;
                }
           }
        }
    }

    return  $conn_number;

}


?>

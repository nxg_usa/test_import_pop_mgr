<?php

include_once("/opt/observium/includes/defaults.inc.php");
include_once("/opt/observium/config.php");
include_once("/opt/observium/includes/definitions.inc.php");
include_once("/opt/observium/nexusguard/db/db_pop_functions.php");
include_once("/opt/observium/nexusguard/common/functions.inc.php");
include_once("/opt/observium/nexusguard/config/isp_config.php");
include_once("/opt/observium/nexusguard/var/popmgr_var.php");

function generate_new_pop_config($pop_data, $pop_id)
{
    $logtype="POPConfig";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");
    global $var_popmgr_pvt_key;
    global $var_device_user;
    global $var_device_password;
    global $var_ip_pool;
    $cmd = "sudo cat ".$var_popmgr_pvt_key;
    $ssh_key= shell_exec($cmd);
    $pop_name = $pop_data['pop_name'];

    $intf_to_processors = $pop_data['intf_to_processor'];
    $intf_to_processors_vlan = $pop_data['vlan'];
    $ins_intf = $pop_data['ins_intf'];
    $ins_vlan = $pop_data['ins_vlan'];
    $scrubber_type = $pop_data['type'];
   // ExaBGP Details
    $global_server_ip = $pop_data['global_server_ip'];
    $global_server_username = $pop_data['global_server_username'];
    $global_speaker_interface = $pop_data['global_speaker_interface'];
    $global_speaker_md5 = $pop_data['global_speaker_md5'];
    $global_speaker_ip= $pop_data['global_speaker_ip'];
    $global_vlan = $pop_data['global_vlan'];
    $local_asn = $pop_data['local_asn'];
    $local_asn_global = $pop_data['local_asn_global'];
    $local_speaker_interface = $pop_data['local_interface'];
    $local_server_ip = $pop_data['local_server_ip'];
    $local_server_username = $pop_data['local_server_username'];
    $local_speaker_ip = $pop_data['local_speaker_ip'];
    $local_speaker_ip_local = $pop_data['local_speaker_ip_local'];
    $local_speaker_md5 = $pop_data['local_speaker_md5'];
    $local_vlan = $pop_data['local_vlan'];
    $restart_timer_global = $pop_data['restart_timer_global'];
    $restart_timer_local = $pop_data['restart_timer_local'];
    $stale_routes_time_local =  $pop_data['stale_routes_time_local'];
    $stale_routes_time_global =  $pop_data['stale_routes_time_global'];
    $edge_router_username = $pop_data['edge_router_username'];
    $custom_names = $pop_data['traffic_name'];
    $div_nxg=array();
    $insertion_nxg=array();
    $intf_array = array();
    $interface = array();
    $insertion_vr = array();
    $routing_options = array();
    $insertion_interface_ip_adress="";
    $prefix_list =$pop_data['prefix_list'];
    $service_prefix_list  = explode("\n",$prefix_list);
    $pl_agg_prefix_list=$pop_data['pl_agg_prefix_list'];
    $agg_prefix_list= explode("\n",$pl_agg_prefix_list);
    $subnet = "31";
    $ip_address_type  = "pop";

     //Netflow $ syslog details

    $netflow_server_ip=$pop_data['netflow_server_ip'];
    $netflow_source_address=$pop_data['netflow_source_address'];
    $netflow_port=$pop_data['netflow_port'];
    $syslog_host_ip=$pop_data['syslog_host_ip'];

        $lag_interface_ins=$pop_data['lag_interface_ins'];
        $lag_interface=$pop_data['lag_interface'];
     if(is_string($intf_to_processors))
    {
        $tmp=(explode(" ",$intf_to_processors));
        $intf_to_processors=$tmp;
        $tmp=(explode(" ",$intf_to_processors_vlan));
        $intf_to_processors_vlan=$tmp;
        $tmp=(explode(" ",$ins_intf));
        $ins_intf=$tmp;
        $tmp=(explode(" ",$ins_vlan));
        $ins_vlan=$tmp;
        $tmp=(explode(" ",$custom_names));
        $custom_names=$tmp;
        $tmp=(explode(" ",$scrubber_type));
        $scrubber_type=$tmp;
        $lag_interface = array($lag_interface);
        $lag_interface_ins = array($lag_interface_ins);
    }

    if(!is_array($lag_interface))
    {
        $lag_interface = array($lag_interface);
    }
    if(!is_array($lag_interface_ins))
    {
        $lag_interface_ins = array($lag_interface_ins);
    }

    $member_port_config = array();
    /*Iterate over diverisons interface to generate interface configuration */
    for($i=0;$i<count($intf_to_processors);$i++)
    {
        //Get diversion interafce and insertion interface from data
        $div_name = $intf_to_processors[$i];
        $div_vlan_id = $intf_to_processors_vlan[$i];
        $insertion_name = $ins_intf[$i];
        $insertion_vlan_id = $ins_vlan[$i];
        $type = $scrubber_type[$i];
        $custom_name = $custom_names[$i];
        /*get IP Address for new POP */
        if($initialize == 0)
            $ip_address = get_ip_address_by_subnet($pop_id,"ge",$subnet,$ip_address_type,"ge",$pop_name);
        else
            $ip_address = get_next_ip_addresses($interface_ip,$subnet);

        $initialize=1;
        if(empty($div_vlan_id))
        {
            $div_vlan_id=0;
            $insertion_vlan_id=0;
            
            //interface configuration for diversion interface
            $div_intf_config = array('unit'=>$div_vlan_id,'ipv4_address'=>$ip_address[0]."/31");
                        
            $ins_intf_config = array('unit'=>$insertion_vlan_id,'ipv4_address'=>$ip_address[1]."/31");
        }
        else
        {
            //interface configuration for diversion interface
            $div_intf_config = array('unit'=>$div_vlan_id,'vlan_id'=>$div_vlan_id,'ipv4_address'=>$ip_address[0]."/31");

            //interface configuration for insertion  interface
            $ins_intf_config = array('unit'=>$insertion_vlan_id,'vlan_id'=>$insertion_vlan_id,'ipv4_address'=>$ip_address[1]."/31");
        }

            $member_ports_div = $lag_interface[$i][0];
            $member_ports_ins = $lag_interface_ins[$i][0];
            if(!is_array($member_ports_div))
            {
                $member_ports_div = array($member_ports_div);
            }
            if(!is_array($member_ports_ins))
            {
                $member_ports_ins = array($member_ports_ins);
            }
            $member_ports_config_div = array();
            $member_ports_config_ins = array();
            for($x=0;$x<count($member_ports_div);$x++)
            {
                if(!empty($member_ports_div[$x]))
                    $member_port_config[$div_name][]=$member_ports_div[$x];
            }
            for($y=0;$y<count($member_ports_ins);$y++)
            {
                if(!empty($member_ports_ins[$y]))
                    $member_port_config[$insertion_name][]=$member_ports_ins[$y];
            }

        

        $interface_ip = $ip_address[1];

        $key_exists = 0;

        /*check whether we have processed interface*/
        if(array_key_exists($div_name,$intf_array))
        {
            $key_exists = 1;


            /* Create DIversion for new sub interface */
            $community_instace = gen_policy_community($pop_id, $i ,$type,$custom_name);


            /* Create routing instance for each diverison */
            $routing_instance = gen_routing_instance($pop_id, $routing_array, $ip_address[1],$i,$div_name,$div_vlan_id,$type,$custom_name);

            //Add interface in insertion VR
            $insertion_vr['interfaces'][] = get_insertion_vr($insertion_name,$insertion_vlan_id);

            //$community_id = $community_instace['target'][1]['value'];
            $community_id = explode(":",$community_instace['target']['value'])[1];
            $comminuty_name = $community_instace['name'];
            $routing_distinguisher = $routing_instance['route_distinguisher'];

            $div_desc = $pop_name." : ". $comminuty_name." : ".$insertion_name.".".$insertion_vlan_id;
            $ins_desc = $pop_name." : VR-INSERTION : ".$div_name.".".$div_vlan_id;

            //Add desciption in configuration
            $div_intf_config['description'] = $div_desc;
            $ins_intf_config['description'] = $ins_desc;

            //update diversion interface config
            $intf_config = $intf_array[$div_name]['interface_configuration'];
            $cnt = count($intf_config);
            $intf_config[$cnt] = $div_intf_config;
            $intf_array[$div_name]['interface_configuration'] = $intf_config;
            $intf_array[$div_name] =  array('interface_name'=>$div_name,'interface_configuration'=>$intf_config);

            


            //update insertion interface config
    
             if(!(array_key_exists($insertion_name,$intf_array)))
                {
                    $ins_intf_details = array('interface_name'=>$insertion_name,'interface_configuration'=>array($ins_intf_config));
                    $intf_array[$insertion_name] =$ins_intf_details;
                }
                else
                {
                    $intf_config = $intf_array[$insertion_name]['interface_configuration'];
                    $cnt = count($intf_config);
                    $intf_config[$cnt] = $ins_intf_config;
                    $intf_array[$insertion_name]['interface_configuration'] = $intf_config;
                    $intf_array[$insertion_name] =  array('interface_name'=>$insertion_name,'interface_configuration'=>$intf_config);
                }

        //if pop_id is provided then update config in db
            if( $pop_id )
            {
                add_pop_interface_config($pop_id,$div_name,$div_vlan_id,$ip_address[0],$community_id,$routing_distinguisher,$comminuty_name,"pop",'ge');
                add_pop_interface_config($pop_id,$insertion_name,$insertion_vlan_id,$ip_address[1],$community_id,$routing_distinguisher,"VR-INSERTION","pop",'ge');
            }
            $policy_array[] = $community_instace;
            $routing_array [] = $routing_instance;
            //$insertion_vr[] = get_insertion_vr($insertion_name,$insertion_vlan_id);
        }
       if($key_exists == 0)
        {

            /* Create DIversion for new sub interface */
            $community_instace = gen_policy_community($pop_id, $i,$type,$custom_name);

            /* Create routing instance for each diverison */
            $routing_instance = gen_routing_instance($pop_id, $routing_array, $ip_address[1],$i,$div_name,$div_vlan_id,$type,$custom_name);

            //Add interface in insertion VR
            $insertion_vr['interfaces'][] = get_insertion_vr($insertion_name,$insertion_vlan_id);

            $community_id = explode(":",$community_instace['target']['value'])[1];
            $comminuty_name = $community_instace['name'];
            $routing_distinguisher = $routing_instance['route_distinguisher'];

            //Add desciption in configuration
            $div_desc = $pop_name." : ". $comminuty_name." : ".$insertion_name.".".$insertion_vlan_id;
            $ins_desc = $pop_name." : VR-INSERTION : ".$div_name.".".$div_vlan_id;

            //Add desciption in configuration
            $div_intf_config['description'] = $div_desc;
            $ins_intf_config['description'] = $ins_desc;

            //update diversion interface config
            $intf_details = array('interface_name'=>$div_name,'interface_configuration'=>array($div_intf_config));
            $intf_array[$div_name] =$intf_details;

            //update insertion interface config
            $ins_intf_details = array('interface_name'=>$insertion_name,'interface_configuration'=>array($ins_intf_config));


            if(!(array_key_exists($insertion_name,$intf_array)))
                {
                    $intf_array[$insertion_name] =$ins_intf_details;
                }
                else
                {
                    $intf_config = $intf_array[$insertion_name]['interface_configuration'];
                    $cnt = count($intf_config);
                    $intf_config[$cnt] = $ins_intf_config;
                    $intf_array[$insertion_name]['interface_configuration'] = $intf_config;
                    $intf_array[$insertion_name] =  array('interface_name'=>$insertion_name,'interface_configuration'=>$intf_config);
                }

            //if pop_id is provided then update config in db
            if( $pop_id )
            {
                add_pop_interface_config($pop_id,$div_name,$div_vlan_id,$ip_address[0],$community_id,$routing_distinguisher,$comminuty_name,"pop",'ge');
                add_pop_interface_config($pop_id,$insertion_name,$insertion_vlan_id,$ip_address[1],$community_id,$routing_distinguisher,"VR-INSERTION","pop",'ge');
            }

            $policy_array[] = $community_instace;
            $routing_array [] = $routing_instance;
        }

        $insertion_interface_ip_adress = $ip_address['insertion_interface_ip_adress'];
   }

    $member_config = array();
    foreach ($member_port_config as $interface=>$member)
    {
        $temp = array_unique($member);
        $names = array();
        foreach($member as $intf_port)
        {
            $names[]['name'] = $intf_port;
        }
        $member_config[] = array('interface_name'=>$interface,'member_ports'=>$names);
    }


    $exabgp_interface = $pop_data['local_interface'];
    $exabgp_vlan = $pop_data['local_vlan'];
    $exabgp_local_ip = $pop_data['local_speaker_ip'];
    $exabgp_server_ip = $pop_data['local_server_ip'];
    $exabgp_local_as = $pop_data['local_asn'];
    $exabgp_peer_as = $pop_data['local_asn'];
    $exabgp_routing_interface = "";
    $exabgp_desc_local= $pop_name.": LOCAL-ExaBGP-SERVER-".$exabgp_server_ip;

    if($exabgp_vlan)
    {
        $exabgp_intf_config = array(array('unit'=>$exabgp_vlan, 'ipv4_address'=>$exabgp_local_ip,'vlan_id'=>$exabgp_vlan,'description'=>$exabgp_desc_local));
        $exabgp_routing_interface = $exabgp_interface.".".$exabgp_vlan;
    }
    else
    {
        $exabgp_intf_config = array(array('unit'=>0, 'ipv4_address'=>$exabgp_local_ip,'description'=>$exabgp_desc_local));
        $exabgp_routing_interface = $exabgp_interface.".0";
    }
    $intf_array[$exabgp_interface] = array('interface_name'=>$exabgp_interface,'interface_configuration'=>$exabgp_intf_config);


    $exabgp_interface_global = $pop_data['global_speaker_interface'];
    $exabgp_vlan_global = $pop_data['global_vlan'];
    $exabgp_local_ip_global = $pop_data['global_speaker_ip'];
    $exabgp_server_ip_global = $pop_data['global_server_ip'];
    $exabgp_local_as_global = $pop_data['local_asn_global'];
    $exabgp_peer_as_global = $pop_data['local_asn_global'];
    $exabgp_desc_global= $pop_name.": REMOTE-ExaBGP-SERVER-".$exabgp_server_ip_global;

    $exabgp_intf_config_global = array(array('unit'=>$exabgp_vlan_global, 'ipv4_address'=>$exabgp_local_ip_global,'vlan_id'=>$exabgp_vlan_global,'description'=>$exabgp_desc_global));
    $intf_array[$exabgp_interface_global] = array('interface_name'=>$exabgp_interface_global,'interface_configuration'=>$exabgp_intf_config_global);
    $exabgp_global_interface  = "";

    if($exabgp_vlan_global)
    {
        $exabgp_intf_config_global = array(array('unit'=>$exabgp_vlan_global, 'ipv4_address'=>$exabgp_local_ip_global,'vlan_id'=>$exabgp_vlan_global,'description'=>$exabgp_desc_global));
        $exabgp_global_interface = $exabgp_interface_global.".".$exabgp_vlan_global;
    }
    else
    {
        $exabgp_intf_config_global = array(array('unit'=>0, 'ipv4_address'=>$exabgp_local_ip_global,'description'=>$exabgp_desc_global));
        $exabgp_global_interface = $exabgp_interface_global.".0";
    }
    $intf_array[$exabgp_interface_global] = array('interface_name'=>$exabgp_interface_global,'interface_configuration'=>$exabgp_intf_config_global);

    $mgmt_interface = $pop_data['mgmt_interface'];
    $mgmt_ip = $pop_data['mgmt_ip'];
    $mgmt_ip_subnet = $pop_data['mgmt_ip_subnet'];
    $mgmt_gateway = $pop_data['mgmt_gateway'];
    $edge_router_type = $pop_data['edge_router_type'];
    if ( $edge_router_type =='new_router' )
    {
        $edge_router_ip = $mgmt_ip;
        $mgmt_intf_config = array('0'=>array('unit'=>0, 'ipv4_address'=>$edge_router_ip."/".$mgmt_ip_subnet));
        $intf_array[$mgmt_interface]= array('interface_name'=>$mgmt_interface,'interface_configuration'=>$mgmt_intf_config);
        $routing_options = array('static'=>array('destination'=>'0.0.0.0/0','nexthop'=>$mgmt_gateway));
    }

    $loopback_core_unit = 0;
    $loopback_core_ip = $pop_data['loopback_ip'];
    $loopback_intf['unit'] = $loopback_core_unit;
    $loopback_intf['ipv4_address'] = $loopback_core_ip;
    $loopback_intf['description'] = " VR-CORE : Loopback ";
    if($pop_id)
        add_config_to_db($pop_id, $loopback_intf['unit'],'lo0',$loopback_intf['ipv4_address'],"VR-CORE","lo0-public",'lo0');

    $intf_array = add_interface_to_array($intf_array,$loopback_intf);

    $lt_interface=$var_ip_pool["pop"]["ifl"]["name"];
    $lt_interface_config=array();
    $lt_unit = get_unit_by_type(NULL,$lt_interface,"pop");
    $lt_intf_gen_ip = get_ip_address_by_subnet(NULL,$lt_interface,"31","pop","ifl");
    $ip1=$lt_intf_gen_ip[0];
    $ip2=$lt_intf_gen_ip[1];
    
   for($i=1;$i<3;$i++)
   {
        $peer_unit=$lt_unit+1; 
        
        $description=$pop_name . ': VR-CORE:' . $lt_interface . '.' . $peer_unit;
        $lt_interface_config[] = array('name'=>$lt_interface,'unit'=>$lt_unit,'ipv4_address'=>$ip1."/31",'peer_unit'=>$peer_unit,'description'=>$description); 
         if($pop_id)
         add_config_to_db($pop_id, $lt_unit,$lt_interface,$ip1,"VR-E-TYPE-".$i,"pop","ifl");

        $peer_unit=$lt_unit;
        $lt_unit+=1;
        
        $description=$pop_name . ' : VR-E-TYPE-' . $i .':' . $lt_interface . '.' . $peer_unit; 
        $lt_interface_config[] = array('name'=>$lt_interface,'unit'=>$lt_unit,'ipv4_address'=>$ip2."/31",'peer_unit'=>$peer_unit,'description'=>$description);
        if($pop_id)
        add_config_to_db($pop_id, $lt_unit,$lt_interface,$ip2,"VR-CORE","pop","ifl");
 
        $lt_unit+=1;
        $ip1=increment_ip_by_one($ip2);
        $ip2=increment_ip_by_one($ip1);
   }

   $intf_array['lt_interface_config'] = array('interface_name'=>$lt_interface,'interface_configuration'=>$lt_interface_config);
 

    $bgp_routing_instance = array('name'=>'VR-CORE','bgpgroup'=>'ExaBGP-FlowSpec','neighbour'=>$exabgp_server_ip,'peer_as'=>$exabgp_peer_as,'local_as'=>$exabgp_local_as,'ospf_area'=>'0.0.0.0','interface'=>$exabgp_routing_interface,'loopback'=>'lo0.'.$loopback_core_unit,'exabgp_global_interface'=>$exabgp_global_interface);

//Create loopback interfaces

    $policy_options = array('community'=>$policy_array);

    $system = get_system_config($pop_data,$ssh_key,$var_device_user,$var_device_password);
    $snmp = get_snmp_config($pop_data);

    foreach($intf_array  as $intf=>$config)
    {
        $interfaces [] = $config;
    }
   
 

    $global_speaker_config = array('global_server_ip'=>$global_server_ip,'global_speaker_interface'=>$global_speaker_interface,'global_speaker_md5'=>$global_speaker_md5,'global_speaker_ip'=>$global_speaker_ip,'global_vlan'=>$global_vlan,'restart_time'=>$restart_timer_global,'stale_routes_time_global'=>$stale_routes_time_global,'local_asn'=>$local_asn_global,'peer_asn'=>$local_asn_global);

     $local_speaker_config = array('local_server_ip'=>$local_server_ip,'local_speaker_interface'=>$local_speaker_interface,'local_speaker_md5'=>$local_speaker_md5,'local_speaker_ip'=>$local_speaker_ip,'local_vlan'=>$local_vlan,'restart_time'=>$restart_timer_local,'stale_routes_time_local'=>$stale_routes_time_local,'local_asn'=>$local_asn,'peer_asn' => $local_asn);
    $common_params=array('device_username'=>$edge_router_username,'bgpgroup_local'=>'ExaBGP-FlowSpec-Local','bgpgroup_remote'=>'ExaBGP-FlowSpec-Remote','vr_core_pub_ip'=>$loopback_core_ip."/32",'lt_interface'=>$lt_interface);

    $netflow_config = array('netflow_server_ip'=>$netflow_server_ip,'netflow_source_address'=>$netflow_source_address,'netflow_port'=>$netflow_port);

    $syslog_config = array('syslog_host_ip'=>$syslog_host_ip);


    $config = array('pop_name'=>$pop_name,'interfaces'=>$interfaces,'policy_options'=>array($policy_options), 'vr_routing_instance'=>$routing_array,'bgp_routing_instance'=>array($bgp_routing_instance),'insertion_vr'=>$insertion_vr,'system'=>$system,'snmp'=>$snmp,'routing_options'=>$routing_options,'service_prefix_list'=>$service_prefix_list,'pl_agg_prefix_list'=> $agg_prefix_list,'netflow_config' => $netflow_config,'syslog_config' => $syslog_config,'global_speaker_config'=>$global_speaker_config,'local_speaker_config'=>$local_speaker_config,'common_params' => $common_params,'lt_interface_config'=>$lt_interface_config,'member_ports_config'=>$member_config);



    log_trace($logtype, __FILE__, __FUNCTION__, "Config for pop=".json_encode($config));
    log_trace($logtype, __FILE__, __FUNCTION__, "End");



    $pop_config['pop_config'] = $config;
    $pop_config['isp_config']= $isp_config;
    return $pop_config;
}

function get_interface_ip_address($pop_id=NULL,$insertion_interface_ip_adress=NULL,$div_name,$div_vlan_id)
{
    $logtype="POPConfig";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    if($pop_id==NULL && $insertion_interface_ip_adress == NULL)
    {
        $div_interface_ip_address = "10.2.1.2";
        $insertion_interface_ip_adress = "10.2.1.3";
    }
    else if($pop_id==NULL)
    {
        $long = ip2long($insertion_interface_ip_adress);
        $long += 1;
        $div_interface_ip_address = long2ip($long);
        $long = ip2long($div_interface_ip_address);
        $long += 1;
        $insertion_interface_ip_adress = long2ip($long);
    }
    else
    {
        if($insertion_interface_ip_adress == NULL)
        {
            $div_interface_ip_address = "10.2.1.2";
            $insertion_interface_ip_adress = "10.2.1.3";
        }
        else
        {
            $long = ip2long($insertion_interface_ip_adress);
            $long += 1;
            $div_interface_ip_address = long2ip($long);
            $long = ip2long($div_interface_ip_address);
            $long += 1;
            $insertion_interface_ip_adress = long2ip($long);
        }
        //get ip details from database;
    }
    log_trace($logtype, __FILE__, __FUNCTION__, "End");
    return  array('div_interface_ip_address'=>$div_interface_ip_address,'insertion_interface_ip_adress'=>$insertion_interface_ip_adress);
}

function get_max_target_from_db($pop_id)
{

    $max_target=dbFetchRow("select MAX(target) AS target from nxg_pop_interface_ip_mgmt where pop_id='$pop_id';");
    return $max_target;

}



function get_target_id($pop_id=NULL,$last_used_target_id=NULL)
{
    $logtype="POPConfig";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    $target = 1;

    if($pop_id==NULL && $last_used_target_id == NULL)
    {
        $target = 1;
    }
    else
    {
        $target = $last_used_target_id + 1;
    }
    log_trace($logtype, __FILE__, __FUNCTION__, "End");
    return  $target;
}

function get_route_distinguisher($pop_id=NULL,$last_used_ip_adress=NULL)
{
    $logtype="POPConfig";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    $route_dustinguisher_ip_address='1.1.1.1';

    if($pop_id==NULL && $last_used_ip_adress == NULL)
    {
        $route_dustinguisher_ip_address = "1.1.1.1";
    }
    else if($pop_id==NULL)
    {
        $long = ip2long($last_used_ip_adress);
        $long += 1;
        $route_dustinguisher_ip_address = long2ip($long);
    }
    log_trace($logtype, __FILE__, __FUNCTION__, "End");
    return  $route_dustinguisher_ip_address;
}
function get_diversion_names($pop_id=NULL,$last_div_id=NULL)
{
    $logtype="POPConfig";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    if($pop_id==NULL && $last_div_id == NULL)
    {
        $last_div_id = 1;
    }
    else
    {
        $last_div_id = $last_div_id + 1;
    }
    log_trace($logtype, __FILE__, __FUNCTION__, "End");
    return  $last_div_id;
}

function gen_policy_community($pop_id=NULL, $community_count,$scrubber_type,$custom_name)
{

    $logtype="POPConfig";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    $community_id = get_diversion_names($pop_id, $community_count);
//    $community_name = "VR-DIV-".$community_id;
     $community_name = "VR-DIV-".strtoupper($scrubber_type);

    if($scrubber_type == "custom")
        $community_name = "VR-DIV-".strtoupper($scrubber_type)."-".strtoupper($custom_name);
        

    if($pop_id)
    {
        $max_target_id = get_max_target_from_db($pop_id);
        $target_id=$max_target_id['target']+1;
    }
    else
    {
        $target_id = get_target_id($pop_id,$community_count);
    }

    $community = array('name'=>$community_name, 'target'=>array('value'=>'100:'.$target_id));
    log_trace($logtype, __FILE__, __FUNCTION__, "End");
    return $community;
}

function gen_routing_instance($pop_id=NULL, $routing_array, $ip_address,$community_count,$div_name,$div_vlan_id,$scrubber_type,$custom_name)
{

    $logtype="POPConfig";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    $community_id = get_diversion_names($pop_id, $community_count);
//    $community_name = "VR-DIV-".$community_id;

    if($scrubber_type=="custom")
        $community_name = "VR-DIV-".strtoupper($scrubber_type)."-".strtoupper($custom_name);
    else
        $community_name = "VR-DIV-".strtoupper($scrubber_type);
        
    $route_distinguisher = get_route_distinguisher($pop_id,$ip_address);

    if($pop_id)
    {
        $max_target_id = get_max_target_from_db($pop_id);
        $target_id=$max_target_id['target']+1;
    }
    else
    {
        $target_id = get_target_id($pop_id,$community_count);
    }


    $community = array('name'=>$community_name, 'route_distinguisher'=>$route_distinguisher.':'.$target_id, 'interface'=>$div_name.".".$div_vlan_id,'vrf_target'=>'100:'.$target_id, 'route_details'=>array('type'=>'static','destination'=>'0.0.0.0/0','nexthop'=>$ip_address));
    log_trace($logtype, __FILE__, __FUNCTION__, "End");
    return $community;
}


function get_snmp_config($pop_data)
{

    $logtype="POPConfig";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");
    $user = $pop_data['user_name'];
    $password = $pop_data['auth_password'];
    $proto_version = $pop_data['proto_version'];
    $auth_level = $pop_data['auth_level'];
    $snmp_community = $pop_data['snmp_community'];
    $transport = $pop_data['transport'];
    $timeout = $pop_data['timeout'];
    $auth_algo = $pop_data['auth_algo'];
    $tries = $pop_data['tries'];
    $crypto_password = $pop_data['crypto_password'];
    $port = $pop_data['port'];
    $crypto_algo = $pop_data['crypto_algo'];

    $snmp = array('username'=>$user,'proto_version'=>$proto_version,'auth_level'=>$auth_level,'snmp_community'=>$snmp_community,'transport'=>$transport,'timeout'=>$timeout,'auth_algo'=>$auth_algo,'tries'=>$tries,'port'=>$port,'password'=>$password);

    if($auth_level=="authPriv")
    {
            $snmp['crypto_password'] = $crypto_password;
            $snmp['crypto_algo'] = $crypto_algo;
    }
    return $snmp;
}

function get_system_config($pop_data,$ssh_key,$var_device_user,$var_device_password)
{

    $logtype="POPConfig";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");
    $user = $var_device_user;
    $password = $var_device_password;
    $device_name = $pop_data['new_name'];
    return array('hostname'=>$device_name,'username'=>$user,'password'=>$password,'ssh_key'=>$ssh_key);
}

function get_insertion_vr($interface,$unit)
{

    $logtype="POPConfig";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");
    return array('interface'=>$interface.".".$unit);
}

function add_interface_to_array($intf_array, $loopback_config)
{
    $div_name="lo0";
    if(array_key_exists("lo0",$intf_array))
    {
        $intf_config = $intf_array[$div_name]['interface_configuration'];
        $cnt = count($intf_config);
        $intf_config[$cnt] = $loopback_config;
        $intf_array[$div_name]['interface_configuration'] = $intf_config;
        $intf_array[$div_name] =  array('interface_name'=>$div_name,'interface_configuration'=>$intf_config);
    }
    else
    {
            $intf_details = array('interface_name'=>$div_name,'interface_configuration'=>array($loopback_config));
            $intf_array[$div_name] =$intf_details;

    }
    return $intf_array;
}

?>

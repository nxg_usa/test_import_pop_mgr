<?php

include_once("/opt/observium/includes/defaults.inc.php");
include_once("/opt/observium/config.php");
include_once("/opt/observium/includes/definitions.inc.php");
include_once("/opt/observium/nexusguard/db/db_pop_functions.php");
include_once("/opt/observium/nexusguard/common/functions.inc.php");
include_once("/opt/observium/nexusguard/resource/ip_mgmt_config.php");
include_once("/opt/observium/nexusguard/config/pop_config.php");
include_once("/opt/observium/nexusguard/resource/ip_mgmt_config.php");
function generate_interface_config($pop_data,$interface_array,$action,$gui_insertion_data,$db_data,$db_interfaces)
{
	$logtype="ModifyPOPConfig";
	$config=array();
    $del_local_speaker_interface = array();
    $del_local_speaker_md5 = array();
    $del_restart_time_local = array();
    $del_stale_time_local = array();
    $del_local_asn = array();
    $del_peer_asn = array();
    $del_local_server_ip = array();
    $del_local_speaker_ip = array();
    $del_netflow_server_ip = array();
    $del_netflow_source_address = array();
    $del_netflow_port = array();
    $del_syslog_host_ip = array();



    $add_local_speaker_interface = array();
    $add_local_speaker_md5 = array();
    $add_restart_time_local = array();
    $add_stale_time_local = array();
    $add_local_asn = array();
    $add_peer_asn = array();
    $add_local_server_ip = array();
    $add_local_speaker_ip =array();
    $add_netflow_server_ip = array();
    $add_netflow_source_address = array();
    $add_netflow_port = array();
    $add_syslog_host_ip =array();


    $del_global_speaker_interface = array();
    $del_global_speaker_md5 = array();
    $del_global_server_ip = array();
    $del_restart_time_global = array();
    $del_stale_time_global = array();
    $del_local_asn_global = array();
    $del_peer_asn_global = array();
    $del_global_speaker_ip = array();
    $isp_names = array();
    $add_global_speaker_interface = array();
    $add_global_speaker_md5 = array();
    $add_global_server_ip = array();
    $add_restart_time_global = array();
    $add_stale_time_global = array();
    $add_local_asn_global = array();
    $add_peer_asn_global = array();
    $add_global_speaker_ip =array();

	$div_nxg=array();
	$insertion_nxg=array();
	$intf_array = array();
	$interface = array();
	$insertion_vr = array();
	$routing_options = array();
	$insertion_interface_ip_adress="";
	$add_config = array();
	$div_intf_config = array();
    $add_member_port_config = array();
    $del_member_port_config = array();

	$pop_name = $pop_data['pop_name'];
	$pop_id=$pop_data['pop_id'];
	$action = $pop_data['action'];
	$intf_to_processors = $pop_data['intf_to_processor'];
	$intf_to_processors_vlan = $pop_data['vlan'];
	$ins_intf = $pop_data['ins_intf'];
	$ins_vlan = $pop_data['ins_vlan'];
	$scrubber_type = $pop_data['type'];
	$custom_names= $pop_data['traffic_name'];
	log_trace($logtype, __FILE__, __FUNCTION__, "Begin");
	$pop_id=$pop_data['pop_id'];
	$db_keys=array();
	$gui_keys=array();
	$del_config=array();
	$pop_id=$pop_data['pop_id'];
	$config=array();
	$flag=0;


     // ExaBGP Details
    $global_server_ip = $pop_data['global_server_ip'];
    $global_server_username = $pop_data['global_server_username'];
    $global_speaker_interface = $pop_data['global_speaker_interface'];
    $global_speaker_md5 = $pop_data['global_speaker_md5'];
    $global_speaker_ip= $pop_data['global_speaker_ip'];
    $global_vlan = $pop_data['global_vlan'];
    $local_asn = $pop_data['local_asn'];
    $peer_asn = $local_asn;
    $local_asn_global = $pop_data['local_asn_global'];
    $peer_asn_global = $local_asn_global;
 
    $local_speaker_interface = $pop_data['local_interface'];
    $local_server_ip = $pop_data['local_server_ip'];
    $local_server_username = $pop_data['local_server_username'];
    $local_speaker_ip = $pop_data['local_speaker_ip'];
    $local_speaker_ip_local = $pop_data['local_speaker_ip_local'];
    $local_speaker_md5 = $pop_data['local_server_md5'];
    $local_vlan = $pop_data['local_vlan'];
    $restart_timer_global = $pop_data['restart_timer_global'];
    $restart_timer_local = $pop_data['restart_timer_local'];
    $stale_routes_time_local =  $pop_data['stale_routes_time_local'];
    $stale_routes_time_global =  $pop_data['stale_routes_time_global'];
    $edge_router_username = $pop_data['username'];
    $ssh_key = file_get_contents('/home/ubuntu/mangesh/pop_key.pub', true);
    $gui_row_id= $pop_data['gui_row_id'];

    //Netflow $ syslog details
    $netflow_server_ip=$pop_data['netflow_server_ip'];
    $netflow_source_address=$pop_data['netflow_source_address'];
    $netflow_port=$pop_data['netflow_port'];
    $syslog_host_ip=$pop_data['syslog_host_ip'];
    
    $subnet = "31";
    $ip_address_type  = "pop";

     $lag_interface_ins=$pop_data['lag_interface_ins'];
     $lag_interface=$pop_data['lag_interface'];

    if(is_string($intf_to_processors))
    {
            $tmp=(explode(" ",$intf_to_processors));
            $intf_to_processors=$tmp;
        $tmp=(explode(" ",$intf_to_processors_vlan));
                $intf_to_processors_vlan=$tmp;
        $tmp=(explode(" ",$ins_intf));
                $ins_intf=$tmp;
        $tmp=(explode(" ",$ins_vlan));
                $ins_vlan=$tmp;
        $tmp=(explode(" ",$custom_names));
                $custom_names=$tmp;
        $tmp=(explode(" ",$scrubber_type));
                $scrubber_type=$tmp;
         $tmp=(explode(" ",$gui_row_id));
         $gui_row_id=$tmp;

    }

    if(!is_array($lag_interface))
    {
        $lag_interface = array($lag_interface);
    }
    if(!is_array($lag_interface_ins))
    {
        $lag_interface_ins = array($lag_interface_ins);
    }

    $db_interface_data=get_all_interface_data($pop_id);
		foreach($db_interface_data as $intf)
		{
			$div_name_db=$intf['device_interface_name'];
			$div_vlan_db=$intf['vlan_id'];
			$key=$div_name_db.".".$div_vlan_db;
			$db_keys[]=$key;
		}

	if(!empty($interface_array))
	{
		foreach($interface_array as $interface)
		{
			
			$div_config=array();
			$insertion_config=array();
			$tmp = explode('.',$interface);
			$div_interface=$tmp[0];
			$div_vlan_id=$tmp[1];
			$interface_data=get_interface_data($pop_id,$div_interface,$div_vlan_id);
			$div_name=$interface_data[0]['diversion_name'];
			$div_unit=array('unit'=>$div_vlan_id);
			$div_config=array('interface_name'=>$div_interface,'vr_name'=>$div_name,'interface_configuration'=>array($div_unit));
//			$insertion_interface=get_insertion_interface($pop_id,$div_interface,$div_unit);
			$insertion_interface=get_single_pop_data($pop_id,$div_interface,$div_vlan_id);
			$insertion_name=$insertion_interface[0]['insertion_interface'];
			$insertion_vlan=$insertion_interface[0]['insertion_vlan'];
			$unit=array('unit'=>$insertion_vlan);
			$insertion_config=array('interface_name'=>$insertion_name,'vr_name'=>'VR-INSERTION','interface_configuration'=>array($unit));
            /*$db_mem_ports = dbFetchRows("select * from nxg_pop_ae_members where pop_id = ? and row_id= ? and type='div'",array($pop_id,$db_data[$interface]['row_id']));
            foreach($db_mem_ports as $port)
            {
                $del_member_port_config[$div_interface][] = $port['member_interface_name'];
            }
            $db_mem_ports = dbFetchRows("select * from nxg_pop_ae_members where pop_id = ? and row_id= ? and type='ins'",array($pop_id,$db_data[$interface]['row_id']));
            foreach($db_mem_ports as $port)
            {
                $del_member_port_config[$insertion_name][] = $port['member_interface_name'];
            }*/

			array_push($del_config,$div_config);
			array_push($del_config,$insertion_config);
			//db_delete_interface($pop_id,$div_name,$div_unit);
		        	
		}
	}
	$del_configuration['delete_interfaces']=$del_config;
	for($i=0;$i<count($intf_to_processors);$i++)
	{
		//Get diversion interafce and insertion interface from data
		$div_name = $intf_to_processors[$i];
		$div_vlan_id = $intf_to_processors_vlan[$i];
		$insertion_name = $ins_intf[$i];
		$insertion_vlan_id = $ins_vlan[$i];
		$key=$div_name.".".$div_vlan_id;
		$type=$scrubber_type[$i];
		$custom_name=$custom_names[$i];
		$db_flag=0;

		
		if(in_array($key,$db_keys))
		{
            
		/*	$pop_interface_data=get_single_pop_data($pop_id,$div_name,$div_vlan_id);			
			foreach($pop_interface_data as $intf)
			{
                echo "";
			}	
        */
            $member_ports_div = $lag_interface[$i][0];
            $tmp_member_ports_div = array();
            $tmp_member_ports_ins = array();
            $member_ports_ins = $lag_interface_ins[$i][0];
            if(!is_array($member_ports_div))
            {
                $member_ports_div = array($member_ports_div);
            }
            if(!is_array($member_ports_ins))
            {
                $member_ports_ins = array($member_ports_ins);
            }
             for($x=0;$x<count($member_ports_div);$x++)
            {
                if(!empty($member_ports_div[$x]))
                {
                    $is_member_present = dbFetchRow("select count(*) as cnt from nxg_pop_ae_members where pop_id = ? and ae_interface_name =? and member_interface_name =? ",array($pop_id,$div_name,$member_ports_div[$x]));

                    if($is_member_present['cnt'] <= 0)
                    {
                        $add_member_port_config[$div_name][]=$member_ports_div[$x];
                    }
                    $tmp_member_ports_div[] = $member_ports_div[$x];
                }
            }
            for($y=0;$y<count($member_ports_ins);$y++)
            {
                if(!empty($member_ports_ins[$y]))
                {
                    $is_member_present = dbFetchRow("select count(*) as cnt from nxg_pop_ae_members where pop_id = ? and ae_interface_name =? and member_interface_name =? ",array($pop_id,$insertion_name,$member_ports_ins[$y]));

                    if($is_member_present['cnt'] <= 0)
                    {

                        $add_member_port_config[$insertion_name][]=$member_ports_ins[$y];
                    }
                    $tmp_member_ports_ins[] = $member_ports_ins[$y];
                }
            }

            if(count($tmp_member_ports_div) > 0)
            {
                if(count($tmp_member_ports_div)== 1)
                {

                    $tmp_member_ports_div[0] = "'".$tmp_member_ports_div[0]."'";
                }

                $db_mem_ports = dbFetchRows("select member_interface_name from nxg_pop_ae_members where member_interface_name not in (?) and pop_id = ? and ae_interface_name = ?", array($tmp_member_ports_div,$pop_id, $div_name));
            }
            else
            {
                $db_mem_ports = dbFetchRows("select member_interface_name from nxg_pop_ae_members where pop_id = ? and ae_interface_name = ?", array($pop_id, $div_name));

            }
            foreach($db_mem_ports as $port)
            {
                $del_member_port_config[$div_name][] = $port['member_interface_name'];
            }

            if(count($tmp_member_ports_ins) > 0)
            {
                if(count($tmp_member_ports_ins)== 1)
                {

                    $tmp_member_ports_ins[0] = "'".$tmp_member_ports_ins[0]."'";
                }
                $db_mem_ports = dbFetchRows("select member_interface_name from nxg_pop_ae_members where member_interface_name not in (?) and pop_id = ? and ae_interface_name = ?", array($tmp_member_ports_ins,$pop_id, $insertion_name));
            }
            else
            {
                $db_mem_ports = dbFetchRows("select member_interface_name from nxg_pop_ae_members where pop_id = ? and ae_interface_name = ?", array($pop_id, $insertion_name));
            }
            foreach($db_mem_ports as $port)
            {
                $del_member_port_config[$insertion_name][] = $port['member_interface_name'];
            }


		}
		elseif(!(in_array($key,$db_keys)))
		{
			$flag=1;
			/*get IP Address for new POP */
//			$ip_address_div = get_ip_address($pop_id,$div_name);
            //if($i == 0)
            $ip_address = get_ip_address_by_subnet($pop_id,"ge",$subnet,$ip_address_type,"ge");
            //else
            //$ip_address = get_next_ip_addresses($interface_ip,$subnet);

			//interface configuration for diversion interface
			$div_intf_config = array('unit'=>$div_vlan_id,'vlan_id'=>$div_vlan_id,'ipv4_address'=>$ip_address[0]."/31");
//			$ip_address_ins=increment_ip_by_one($ip_address_div);
			//interface configuration for insertion  interface
			$ins_intf_config = array('unit'=>$insertion_vlan_id,'vlan_id'=>$insertion_vlan_id,'ipv4_address'=>$ip_address[1]."/31");	

            $ip_address_div=$ip_address[0]; 
            $ip_address_ins=$ip_address[1];
			$key_exists = 0;

            $member_ports_div = $lag_interface[$i][0];
            $member_ports_ins = $lag_interface_ins[$i][0];
             if(!is_array($member_ports_div))
            {
                $member_ports_div = array($member_ports_div);
            }
            if(!is_array($member_ports_ins))
            {
                $member_ports_ins = array($member_ports_ins);
            }

             for($x=0;$x<count($member_ports_div);$x++)
            {
                if(!empty($member_ports_div[$x]))
                    $add_member_port_config[$div_name][]=$member_ports_div[$x];
            }
            for($y=0;$y<count($member_ports_ins);$y++)
            {
                if(!empty($member_ports_ins[$y]))
                    $add_member_port_config[$insertion_name][]=$member_ports_ins[$y];
            }


			/*check whether we have processed interface*/
			if(array_key_exists($div_name,$intf_array))
			{

				//echo "------------------------------------- exists $div_name in $intf_array<br>";
				$key_exists = 1;	

				/* Create DIversion for new sub interface */
				$community_instace = gen_policy_community($pop_id, $i,$type,$custom_name);

				/* Create routing instance for each diverison */
				$routing_instance = gen_routing_instance($pop_id, $routing_array, $ip_address_ins,$i,$div_name,$div_vlan_id,$type,$custom_name);
				//Add interface in insertion VR
				$insertion_vr['interfaces'][] = get_insertion_vr($insertion_name,$insertion_vlan_id);

				$community_id = explode(":",$community_instace['target']['value'])[1];
				$comminuty_name = $community_instace['name'];
				$routing_distinguisher = $routing_instance['route_distinguisher'];

				$div_desc = $pop_name." : ". $comminuty_name." : ".$insertion_name.".".$insertion_vlan_id;
				$ins_desc = $pop_name." : VR-INSERTION : ".$div_name.".".$div_vlan_id;

				//Add desciption in configuration
				$div_intf_config['description'] = $div_desc;
				$div_intf_config['target'] = $community_id;
				$div_intf_config['vr_div_name'] = $comminuty_name;
				$div_intf_config['route_distinguisher'] = $routing_distinguisher;
				$div_intf_config['type'] = "pop";
				$div_intf_config['subtype'] = 'ge';


				$ins_intf_config['description'] = $ins_desc;
				$ins_intf_config['target'] = $community_id;
				$ins_intf_config['vr_div_name'] = "VR_INSERTION";
				$ins_intf_config['route_distinguisher'] = $routing_distinguisher;
				$ins_intf_config['type'] = "pop";
				$ins_intf_config['subtype'] = 'ge';



				//update diversion interface config
				$intf_config = $intf_array[$div_name]['interface_configuration'];
				$cnt = count($intf_config);
				$intf_config[$cnt] = $div_intf_config;
				$intf_array[$div_name]['interface_configuration'] = $intf_config;
				$intf_array[$div_name] =  array('interface_name'=>$div_name,'interface_configuration'=>$intf_config);


				//update insertion interface config
				//echo "#########################".$insertion_name."_______________________";
				if(!(array_key_exists($insertion_name,$intf_array)))
				{
					//	echo $insertion_name."******\n";
					$ins_intf_details = array('interface_name'=>$insertion_name,'interface_configuration'=>array($ins_intf_config));
					$intf_array[$insertion_name] =$ins_intf_details;
				}
				else
				{
					$intf_config = $intf_array[$insertion_name]['interface_configuration'];
					$cnt = count($intf_config);
					$intf_config[$cnt] = $ins_intf_config;
					$intf_array[$insertion_name]['interface_configuration'] = $intf_config;
					$intf_array[$insertion_name] =  array('interface_name'=>$insertion_name,'interface_configuration'=>$intf_config);
				}


				//if pop_id is provided then update config in db
/*				if( $pop_id )
				{
					add_pop_interface_config($pop_id,$div_name,$div_vlan_id,$ip_address_div,$community_id,$routing_distinguisher,$comminuty_name,'pop','ge');
					add_pop_interface_config($pop_id,$insertion_name,$insertion_vlan_id,$ip_address_ins,$community_id,$routing_distinguisher,"VR-INSERTION",'pop','ge');
				}
*/
				$policy_array[] = $community_instace;
				$routing_array [] = $routing_instance;
			}
			if($key_exists == 0)
			{

				//echo "<br>------------------------------------- $div_name not exists in $intf_array";
				/* Create DIversion for new sub interface */
				$community_instace = gen_policy_community($pop_id, $i,$type,$custom_name);
				/* Create routing instance for each diverison */
				$routing_instance = gen_routing_instance($pop_id, $routing_array, $ip_address_ins,$i,$div_name,$div_vlan_id,$type,$custom_name);

				//Add interface in insertion VR
				$insertion_vr['interfaces'][] = get_insertion_vr($insertion_name,$insertion_vlan_id);

				$community_id = explode(":",$community_instace['target']['value'])[1];
				$comminuty_name = $community_instace['name'];
				$routing_distinguisher = $routing_instance['route_distinguisher'];

				//Add desciption in configuration
				$div_desc = $pop_name." : ". $comminuty_name." : ".$insertion_name.".".$insertion_vlan_id;
				$ins_desc = $pop_name." : VR-INSERTION : ".$div_name.".".$div_vlan_id;

				//Add desciption in configuration
				$div_intf_config['description'] = $div_desc;
				$div_intf_config['target'] = $community_id;
				$div_intf_config['vr_div_name'] = $comminuty_name;
				$div_intf_config['route_distinguisher'] = $routing_distinguisher;
				$div_intf_config['type'] = "pop";
				$div_intf_config['subtype'] = 'ge';

				$ins_intf_config['description'] = $ins_desc;
				$ins_intf_config['target'] = $community_id;
				$ins_intf_config['vr_div_name'] = "VR_INSERTION";
				$ins_intf_config['route_distinguisher'] = $routing_distinguisher;
				$ins_intf_config['type'] = "pop";
				$ins_intf_config['subtype'] = 'ge';

				//update diversion interface config
				$intf_details = array('interface_name'=>$div_name,'interface_configuration'=>array($div_intf_config));
				$intf_array[$div_name] =$intf_details;


				//update insertion interface config
				//					$ins_intf_details = array('interface_name'=>$insertion_name,'interface_configuration'=>array($ins_intf_config));
				//					$intf_array[$insertion_name] =$ins_intf_details;

				if(!(array_key_exists($insertion_name,$intf_array)))
				{
					//echo $insertion_name."******\n";
					$ins_intf_details = array('interface_name'=>$insertion_name,'interface_configuration'=>array($ins_intf_config));
					$intf_array[$insertion_name] =$ins_intf_details;
				}
				else
				{
					$intf_config = $intf_array[$insertion_name]['interface_configuration'];
					$cnt = count($intf_config);
					$intf_config[$cnt] = $ins_intf_config;
					$intf_array[$insertion_name]['interface_configuration'] = $intf_config;
					$intf_array[$insertion_name] =  array('interface_name'=>$insertion_name,'interface_configuration'=>$intf_config);
				}
				//if pop_id is provided then update config in db
/*				if( $pop_id )
				{
					add_pop_interface_config($pop_id,$div_name,$div_vlan_id,$ip_address_div,$community_id,$routing_distinguisher,$comminuty_name,'pop','ge');
					add_pop_interface_config($pop_id,$insertion_name,$insertion_vlan_id,$ip_address_ins,$community_id,$routing_distinguisher,"VR-INSERTION",'pop','ge');
				}

*/				$policy_array[] = $community_instace;
				$routing_array [] = $routing_instance;

			}
			$insertion_interface_ip_adress = $ip_address_ins;

		}


	}

    $exabgp_interface = $pop_data['local_interface'];
    $exabgp_vlan = $pop_data['local_vlan'];
    $exabgp_local_ip = $pop_data['local_speaker_ip'];
    $exabgp_server_ip = $pop_data['local_server_ip'];
    $exabgp_local_as = $pop_data['local_asn'];
    $exabgp_peer_as = $pop_data['local_asn'];
    $exabgp_intf_config = array(array('unit'=>$exabgp_vlan, 'ipv4_address'=>$exabgp_local_ip,'vlan_id'=>$exabgp_vlan));
 //   $intf_array[$exabgp_interface] = array('interface_name'=>$exabgp_interface,'interface_configuration'=>$exabgp_intf_config);
    $exabgp_interface_global = $pop_data['global_speaker_interface'];
    $exabgp_vlan_global = $pop_data['global_vlan'];
    $exabgp_local_ip_global = $pop_data['global_speaker_ip'];
    $exabgp_server_ip_global = $pop_data['global_server_ip'];
    $exabgp_local_as_global = $pop_data['local_asn_global'];
    $exabgp_peer_as_global = $pop_data['local_asn_global'];

    $exabgp_intf_config_global = array(array('unit'=>$exabgp_vlan_global, 'ipv4_address'=>$exabgp_local_ip_global,'vlan_id'=>$exabgp_vlan_global));
//    $intf_array[$exabgp_interface_global] = array('interface_name'=>$exabgp_interface_global,'interface_configuration'=>$exabgp_intf_config_global);

    if(empty($global_vlan))
    {
        $global_vlan=0;
    }
    if(empty($local_vlan))
    {
        $local_vlan=0;
    }

    

	foreach($intf_array  as $intf=>$config)
	{
		$interfaces [] = $config;
	}
          $exabgp_details=get_exabgp_details($pop_id);
          $netflow_syslog_details=get_netflow_syslog_details($pop_id);
            
          $bgp_group_name_local=$exabgp_details['bgp_group_name_local'];
          $bgp_group_name_global=$exabgp_details['bgp_group_name_global'];


        if($netflow_server_ip != $netflow_syslog_details['netflow_server_ip'])
        {
    
            $del_netflow_server_ip[] = array('netflow_server_ip'=>$netflow_syslog_details['netflow_server_ip'],'netflow_source_address'=>$netflow_syslog_details['netflow_source_address'],'netflow_port'=>$netflow_syslog_details['netflow_port']);
            $add_netflow_server_ip[] = array('netflow_server_ip'=>$netflow_server_ip,'netflow_source_address'=>$netflow_source_address,'netflow_port'=>$netflow_port);

        }

       if($netflow_source_address != $netflow_syslog_details['netflow_source_address']) 
        {   
            $del_netflow_source_address[] = array('netflow_server_ip'=>$netflow_syslog_details['netflow_server_ip'],'netflow_source_address'=>$netflow_syslog_details['netflow_source_address']);
            
            $add_netflow_source_address[] = array('netflow_server_ip'=>$netflow_server_ip,'netflow_source_address'=>$netflow_source_address);
        }

        if($netflow_port != $netflow_syslog_details['netflow_port'])
        {
            $del_netflow_port[] = array('netflow_server_ip'=>$netflow_syslog_details['netflow_server_ip'],'netflow_port'=>$netflow_syslog_details['netflow_port']);

            $add_netflow_port[] = array('netflow_server_ip'=>$netflow_server_ip,'netflow_port'=>$netflow_port);
        }
        if($syslog_host_ip != $netflow_syslog_details['syslog_host_ip'])
        {

           $del_syslog_host_ip[] =array('syslog_host_ip' =>$netflow_syslog_details['syslog_host_ip']);
           $add_syslog_host_ip[] =array('syslog_host_ip' =>$syslog_host_ip);


        }



        if(($local_speaker_interface != $exabgp_details['local_speaker_interface']) || ($local_vlan !=  $exabgp_details['local_vlan']))
        {
            $exabgp_desc_local= $pop_name.": LOCAL-ExaBGP-SERVER-".$exabgp_server_ip;
            if(($exabgp_details['local_speaker_interface'] != $global_speaker_interface) || ($exabgp_details['local_vlan'] != $global_vlan))
                $del_local_speaker_interface[] = array('bgp_group_name'=>$exabgp_details['bgp_group_name_local'],'unit'=>$exabgp_details['local_vlan'],'local_speaker_ip' =>$exabgp_details['local_speaker_ip'] ,'local_speaker_interface'=>$exabgp_details['local_speaker_interface']);

            $add_local_speaker_interface[] = array('bgp_group_name'=>$exabgp_details['bgp_group_name_local'],'unit'=>$local_vlan,'local_speaker_ip' =>$local_speaker_ip ,'local_speaker_interface'=>$local_speaker_interface,'vlan_id'=>$local_vlan,'description'=>$exabgp_desc_local);

        }
         elseif($local_speaker_ip != $exabgp_details['local_speaker_ip'])
        {
             $del_local_speaker_ip[] = array('bgp_group_name'=>$exabgp_details['bgp_group_name_local'],'unit'=>$exabgp_details['local_vlan'],'local_speaker_ip' =>$exabgp_details['local_speaker_ip'] ,'local_speaker_interface'=>$exabgp_details['local_speaker_interface']);

             $add_local_speaker_ip[] = array('bgp_group_name'=>$exabgp_details['bgp_group_name_local'],'unit'=>$local_vlan,'local_speaker_ip' =>$local_speaker_ip ,'local_speaker_interface'=>$local_speaker_interface);


        }


        if(($global_speaker_interface != $exabgp_details['global_speaker_interface']) || ($global_vlan !=  $exabgp_details['global_vlan']))
        {
            $exabgp_desc_global= $pop_name.": REMOTE-ExaBGP-SERVER-".$exabgp_server_ip_global;
            if(($exabgp_details['global_speaker_interface'] != $local_speaker_interface) && $exabgp_details['local_speaker_interface']!= $exabgp_details['global_speaker_interface'])
            $del_global_speaker_interface[] = array('bgp_group_name'=>$exabgp_details['bgp_group_name_global'],'unit'=>$exabgp_details['global_vlan'],'global_speaker_ip' =>$exabgp_details['global_speaker_ip'] ,'global_speaker_interface'=>$exabgp_details['global_speaker_interface']);
            $add_global_speaker_interface[] = array('bgp_group_name'=>$exabgp_details['bgp_group_name_global'],'unit'=>$global_vlan,'global_speaker_ip' =>$global_speaker_ip ,'global_speaker_interface'=>$global_speaker_interface ,'vlan_id'=>$global_vlan,'description'=>$exabgp_desc_global);

        }
        elseif($global_speaker_ip != $exabgp_details['global_speaker_ip'])
        {
             $del_global_speaker_ip[] = array('bgp_group_name'=>$exabgp_details['bgp_group_name_global'],'unit'=>$exabgp_details['global_vlan'],'global_speaker_ip' =>$exabgp_details['global_speaker_ip'] ,'global_speaker_interface'=>$exabgp_details['global_speaker_interface']);

             $add_global_speaker_ip[] = array('bgp_group_name'=>$exabgp_details['bgp_group_name_local'],'unit'=>$global_vlan,'global_speaker_ip' =>$global_speaker_ip ,'global_speaker_interface'=>$global_speaker_interface);


        }

        if($global_speaker_md5 != $exabgp_details['global_speaker_md5_key_string'])
        {
            $del_global_speaker_md5[] = array('bgp_group_name'=>$exabgp_details['bgp_group_name_global'],'neighbor'=>$exabgp_details['global_server_ip'],'global_speaker_md5'=>$exabgp_details['global_speaker_md5_key_string']);
            $add_global_speaker_md5[] = array('bgp_group_name'=>$exabgp_details['bgp_group_name_global'],'neighbor'=> $global_server_ip,'global_speaker_md5'=>$global_speaker_md5);

        }

        if($global_server_ip != $exabgp_details['global_server_ip'])
        {

            $del_global_server_ip[] = array('bgp_group_name'=>$exabgp_details['bgp_group_name_global'],'neighbor'=>$exabgp_details['global_server_ip'],'global_speaker_md5' => $exabgp_details['global_speaker_md5_key_string'],'restart_time_global'=>$exabgp_details['restart_time_global'],'stale_time_global'=>$exabgp_details['stale_routes_time_global'],'local_as'=>$exabgp_details['local_asn_global'],'peer_as'=>$exabgp_details['local_asn_global']);
            $add_global_server_ip[] = array('bgp_group_name'=>$exabgp_details['bgp_group_name_global'],'neighbor'=> $global_server_ip,'global_speaker_md5'=> $global_speaker_md5,'restart_time_global'=> $restart_timer_global,'stale_time_global'=> $stale_routes_time_global,'local_as'=>$local_asn_global,'peer_as'=>$local_asn_global);

        }






        if($restart_timer_global != $exabgp_details['restart_time_global'])
        {
            $del_restart_time_global[] = array('bgp_group_name'=>$exabgp_details['bgp_group_name_global'],'neighbor'=>$exabgp_details['global_server_ip'],'restart_time_global'=>$exabgp_details['restart_time_global']);
            $add_restart_time_global[] = array('bgp_group_name'=>$exabgp_details['bgp_group_name_global'],'neighbor'=>$global_server_ip,'restart_time_global'=>$restart_timer_global);

        }

        if($stale_routes_time_global != $exabgp_details['stale_routes_time_global'])
        {

             $del_stale_time_global[] = array('bgp_group_name'=>$exabgp_details['bgp_group_name_global'],'neighbor'=>$exabgp_details['global_server_ip'],'stale_routes_time_global'=>$exabgp_details['stale_routes_time_global']);
            $add_stale_time_global[] = array('bgp_group_name'=>$exabgp_details['bgp_group_name_global'],'neighbor'=>$global_server_ip,'stale_routes_time_global'=>$stale_routes_time_global);

        }


        if($local_asn_global != $exabgp_details['local_asn_global'])
        {
            $del_local_asn_global[] = array('bgp_group_name'=>$exabgp_details['bgp_group_name_global'],'local_asn'=>$exabgp_details['local_asn_global'],'neighbor'=>$exabgp_details['global_server_ip'],'peer_asn'=>$exabgp_details['peer_asn_global']);
            $add_local_asn_global[] = array('bgp_group_name'=>$exabgp_details['bgp_group_name_global'],'local_asn'=>$local_asn_global,'peer_asn'=>$local_asn_global,'neighbor'=>$global_server_ip);
        
        //    $del_peer_asn_global[] = array('bgp_group_name'=>$exabgp_details['bgp_group_name_global'],'peer_asn'=>$exabgp_details['peer_asn_global']);
         //   $add_peer_asn_global[] = array('bgp_group_name'=>$exabgp_details['bgp_group_name_global'],$local_asn_global);


        }

        if($local_speaker_md5 != $exabgp_details['local_speaker_md5_key_string'])
        {
              $del_local_speaker_md5[] = array('bgp_group_name'=>$exabgp_details['bgp_group_name_local'],'neighbor'=>$exabgp_details['local_server_ip'],'local_speaker_md5'=>$exabgp_details['local_speaker_md5_key_string']);
            $add_local_speaker_md5[] = array('bgp_group_name'=>$exabgp_details['bgp_group_name_local'],'neighbor'=> $local_server_ip,'local_speaker_md5'=>$local_speaker_md5);

        }


        if($restart_timer_local != $exabgp_details['restart_time_local'])
        {
            $del_restart_time_local[] = array('bgp_group_name'=>$exabgp_details['bgp_group_name_local'],'neighbor'=>$exabgp_details['local_server_ip'],'restart_time_local'=>$exabgp_details['restart_time_local']);
            $add_restart_time_local[] = array('bgp_group_name'=>$exabgp_details['bgp_group_name_local'],'neighbor'=>$local_server_ip,'restart_time_local'=>$restart_timer_local);

        }
        if($stale_routes_time_local != $exabgp_details['stale_routes_time_local'])
        {
             $del_stale_time_local[] = array('bgp_group_name'=>$exabgp_details['bgp_group_name_local'],'neighbor'=>$exabgp_details['local_server_ip'],'stale_routes_time_local'=>$exabgp_details['stale_routes_time_local']);
            $add_stale_time_local[] = array('bgp_group_name'=>$exabgp_details['bgp_group_name_local'],'neighbor'=>$local_server_ip,'stale_routes_time_local'=>$stale_routes_time_local);

        }
        if($local_asn != $exabgp_details['local_asn'])
        {
             $del_local_asn[] = array('bgp_group_name'=>$exabgp_details['bgp_group_name_local'],'local_asn'=>$exabgp_details['local_asn'],'peer_asn'=>$exabgp_details['peer_asn'],'neighbor'=>$exabgp_details['local_server_ip']);
             $add_local_asn[] = array('bgp_group_name'=>$exabgp_details['bgp_group_name_local'],'local_asn'=>$local_asn,'peer_asn'=>$local_asn,'neighbor'=>$local_server_ip);
        }

        if($local_server_ip != $exabgp_details['local_server_ip'])
        {
            $del_local_server_ip[] = array('bgp_group_name'=>$exabgp_details['bgp_group_name_local'],'neighbor'=>$exabgp_details['local_server_ip'],'local_speaker_md5' => $exabgp_details['local_speaker_md5_key_string'],'restart_time_local'=>$exabgp_details['restart_time_local'],'stale_time_local'=>$exabgp_details['stale_routes_time_local'],'local_as' => $exabgp_details['local_asn'],'peer_as'=>$exabgp_details['local_asn']);
            $add_local_server_ip[] = array('bgp_group_name'=>$exabgp_details['bgp_group_name_local'],'neighbor'=> $local_server_ip,'local_speaker_md5'=>$local_speaker_md5,'restart_time_local'=>$restart_timer_local,'stale_time_local'=>$stale_routes_time_local,'local_as'=>$local_asn,'peer_asn'=>$local_asn);

        }


          $global_speaker_config = array('global_server_ip'=>$global_server_ip,'global_speaker_interface'=>$global_speaker_interface,'global_speaker_md5'=>$global_speaker_md5,'global_speaker_ip'=>$global_speaker_ip,'global_vlan'=>$global_vlan,'restart_time'=>$restart_timer_global,'stale_routes_time_global'=>$stale_routes_time_global,'local_asn'=>$local_asn,'peer_asn'=>$local_asn,'ssh_key'=>$ssh_key,'bgp_group_name_global'=>$bgp_group_name_global);

     $local_speaker_config = array('local_server_ip'=>$local_server_ip,'local_speaker_interface'=>$local_speaker_interface,'local_speaker_md5'=>$local_speaker_md5,'local_speaker_ip'=>$local_speaker_ip,'local_vlan'=>$local_vlan,'restart_time'=>$restart_timer_local,'stale_routes_time_local'=>$stale_routes_time_local,'local_asn'=>$local_asn_global,'peer_asn' => $local_asn_global,'ssh_key'=>$ssh_key,'bgp_group_name_local' => $bgp_group_name_local);

$netflow_config = array('netflow_server_ip'=>$netflow_server_ip,'netflow_source_address'=>$netflow_source_address,'netflow_port'=>$netflow_port);

$syslog_config = array('syslog_host_ip'=>$syslog_host_ip);



    $db_pop_details = get_sinlge_pop_detail($pop_id);
    $loopback_config = array();

    if($db_pop_details['loopback_ip'] !=  $pop_data['loopback_ip'])
        $loopback_config[] =array('loopback_ip'=>$pop_data['loopback_ip'],'loopback_ip_db'=>$db_pop_details['loopback_ip'],'interface_name'=>'lo0','unit'=>'0');


    $prefix_list = $pop_data['prefix_list'];
    $db_prefix_list = dbFetchRows("select * from nxg_app_service_prefix_list where pop_id = ?",array($pop_id));
    $service_prefix_list  = explode("\n",$prefix_list);

    $pl_agg_prefix_list = $pop_data['pl_agg_prefix_list'];
    $agg_prefix_list  = explode("\n",$pl_agg_prefix_list);
    $db_pl_agg_prefix_list = dbFetchRows("select * from nxg_pl_agg_prefix_list where pop_id = ?",array($pop_id));
    



    $del_prefix_list = array();
    $add_prefix_list = array();

    $del_agg_prefix_list = array();
    $add_agg_prefix_list = array();  


 
    $db_prefix_cnt=count($db_prefix_list); 
    $service_prefix_list_cnt=count($service_prefix_list);
    for($i =0 ;$i<$db_prefix_cnt;$i++)
    {
        $is_exist = 0;
        for($j=0;$j<$service_prefix_list_cnt;$j++)
        {
            if($db_prefix_list[$i]['prefix'] ==$service_prefix_list[$j])
            {
                unset($service_prefix_list[$j]);
                unset($db_prefix_list[$i]);
                $is_exist = 1;
                break;
            }
        }
        if($is_exist != 1)
        {
            $del_prefix_list[] = array('prefix'=>$db_prefix_list[$i]['prefix']);
        }

    }
    foreach($service_prefix_list  as $prefix)
    {
        $add_prefix_list[] = array('prefix'=>$prefix);
    }



 $db_pl_agg_prefix_cnt=count($db_pl_agg_prefix_list);
 $agg_prefix_list_cnt=count($agg_prefix_list);

 for($i =0 ;$i<$db_pl_agg_prefix_cnt;$i++)
    {
        $is_exist = 0;
        for($j=0;$j<$agg_prefix_list_cnt;$j++)
        {
            if($db_pl_agg_prefix_list[$i]['prefix'] ==$agg_prefix_list[$j])
            {
                unset($agg_prefix_list[$j]);
                unset($db_pl_agg_prefix_list[$i]);
                $is_exist = 1;
                break;
            }
        }
        if($is_exist != 1)
        {
            $del_agg_prefix_list[] = array('prefix'=>$db_pl_agg_prefix_list[$i]['prefix']);
        }

    }
    foreach($agg_prefix_list  as $prefix)
    {
        $add_agg_prefix_list[] = array('prefix'=>$prefix);
    }


$isp_list=get_all_isps($pop_id);
for($i=0;$i<count($isp_list);$i++)
    {
        $isp_names[] = $isp_list[$i]['name'];
    }

    $add_member_config = array();
    foreach ($add_member_port_config as $interface=>$member)
    {
        $temp = array_unique($member);
        $names = array();
        foreach($temp as $intf_port)
        {
            $names[]['name'] = $intf_port;
        }
        $add_member_config[] = array('interface_name'=>$interface,'member_ports'=>$names);
    }
    $del_member_config = array();
    foreach ($del_member_port_config as $interface=>$member)
    {
        $temp = array_unique($member);
        $names = array();
        foreach($temp as $intf_port)
        {
            $names[]['name'] = $intf_port;
        }
        $del_member_config[] = array('interface_name'=>$interface,'member_ports'=>$names);
    }


if($flag==1)
{
         $config['add_interfaces']=$interfaces; 

        $config = array('add_interfaces'=>$interfaces,'delete_interfaces'=>$del_config,'vr_routing_instance'=>$routing_array,'insertion_vr'=>$insertion_vr,'loopback_config'=>$loopback_config,'del_local_speaker_interface'=>$del_local_speaker_interface,'del_local_speaker_md5'=>$del_local_speaker_md5,'del_restart_time_local'=>$del_restart_time_local,'del_stale_time_local'=>$del_stale_time_local,'del_local_asn'=>$del_local_asn,'del_peer_asn'=>$del_peer_asn,'del_local_server_ip'=>$del_local_server_ip,'del_global_speaker_interface'=>$del_global_speaker_interface,'del_global_speaker_md5'=>$del_global_speaker_md5,'del_global_server_ip'=>$del_global_server_ip,'del_restart_time_global'=>$del_restart_time_global,'del_stale_time_global'=>$del_stale_time_global,'del_local_asn_global'=>$del_local_asn_global,'del_peer_asn_global'=>$del_peer_asn_global,'add_local_speaker_interface'=>$add_local_speaker_interface,'add_local_speaker_md5'=>$add_local_speaker_md5,'add_restart_time_local'=>$add_restart_time_local,'add_stale_time_local'=>$add_stale_time_local,'add_local_asn'=>$add_local_asn,'add_peer_asn'=>$add_peer_asn,'add_local_server_ip'=>$add_local_server_ip,'add_global_speaker_interface'=>$add_global_speaker_interface,'add_global_speaker_md5'=>$add_global_speaker_md5,'add_global_server_ip'=>$add_global_server_ip,'add_restart_time_global'=>$add_restart_time_global,'add_stale_time_global'=>$add_stale_time_global,'add_local_asn_global'=>$add_local_asn_global,'add_peer_asn_global'=>$add_peer_asn_global,'del_local_speaker_ip'=>$del_local_speaker_ip,'add_local_speaker_ip'=>$add_local_speaker_ip,'del_global_speaker_ip'=>$del_global_speaker_ip,'add_global_speaker_ip'=>$add_global_speaker_ip,'del_prefix_list'=>$del_prefix_list,'add_prefix_list'=>$add_prefix_list,'add_netflow_server_ip'=>$add_netflow_server_ip,'del_netflow_server_ip'=>$del_netflow_server_ip,'add_netflow_source_address'=>$add_netflow_source_address,'del_netflow_source_address' => $del_netflow_source_address,'add_netflow_port'=>$add_netflow_port,'del_netflow_port'=>$del_netflow_port,'add_syslog_host_ip'=>$add_syslog_host_ip,'del_syslog_host_ip'=>$del_syslog_host_ip,'add_agg_prefix_list'=>$add_agg_prefix_list,'del_agg_prefix_list'=>$del_agg_prefix_list,'isp_names'=>$isp_names,'add_member_port_config'=>$add_member_config,'del_member_port_config'=>$del_member_config);

		//db_insert_interface($pop_id,$div_name,$type,$custom_name=NULL,$insertion_name,$insertion_vlan_id,$div_vlan_id);
	}
	else
	{
		$config['add_interfaces']=$add_config;
        $config = array('add_interfaces'=>$add_config,'delete_interfaces'=>$del_config,'del_local_speaker_interface'=>$del_local_speaker_interface,'del_local_speaker_md5'=>$del_local_speaker_md5,'del_restart_time_local'=>$del_restart_time_local,'del_stale_time_local'=>$del_stale_time_local,'del_local_asn'=>$del_local_asn,'del_peer_asn'=>$del_peer_asn,'del_local_server_ip'=>$del_local_server_ip,'del_global_speaker_interface'=>$del_global_speaker_interface,'del_global_speaker_md5'=>$del_global_speaker_md5,'del_global_server_ip'=>$del_global_server_ip,'del_restart_time_global'=>$del_restart_time_global,'del_stale_time_global'=>$del_stale_time_global,'del_local_asn_global'=>$del_local_asn_global,'del_peer_asn_global'=>$del_peer_asn_global,'add_local_speaker_interface'=>$add_local_speaker_interface,'add_local_speaker_md5'=>$add_local_speaker_md5,'add_restart_time_local'=>$add_restart_time_local,'add_stale_time_local'=>$add_stale_time_local,'add_local_asn'=>$add_local_asn,'add_peer_asn'=>$add_peer_asn,'add_local_server_ip'=>$add_local_server_ip,'add_global_speaker_interface'=>$add_global_speaker_interface,'add_global_speaker_md5'=>$add_global_speaker_md5,'add_global_server_ip'=>$add_global_server_ip,'add_restart_time_global'=>$add_restart_time_global,'add_stale_time_global'=>$add_stale_time_global,'add_local_asn_global'=>$add_local_asn_global,'add_peer_asn_global'=>$add_peer_asn_global,'del_local_speaker_ip'=>$del_local_speaker_ip,'add_local_speaker_ip'=>$add_local_speaker_ip,'del_global_speaker_ip'=>$del_global_speaker_ip,'add_global_speaker_ip'=>$add_global_speaker_ip,'del_prefix_list'=>$del_prefix_list,'add_prefix_list'=>$add_prefix_list,'loopback_config'=>$loopback_config,'add_netflow_server_ip'=>$add_netflow_server_ip,'del_netflow_server_ip'=>$del_netflow_server_ip,'add_netflow_source_address'=>$add_netflow_source_address,'del_netflow_source_address' => $del_netflow_source_address,'add_netflow_port'=>$add_netflow_port,'del_netflow_port'=>$del_netflow_port,'add_syslog_host_ip'=>$add_syslog_host_ip,'del_syslog_host_ip'=>$del_syslog_host_ip,'add_agg_prefix_list'=>$add_agg_prefix_list,'del_agg_prefix_list'=>$del_agg_prefix_list,'isp_names'=>$isp_names,'add_member_port_config'=>$add_member_config,'del_member_port_config'=>$del_member_config);

    }
	
	$pop_config['pop_config'] = $config;
    //$config['delete_interfaces']=$del_config;
    //$conf=json_encode($config);
    //print_r($conf);
    
    return $config;
} 


function get_interface_data($pop_id,$interface,$unit)
{
	$interface_data=dbFetchRows("select * from nxg_pop_interface_ip_mgmt where pop_id=? and interface_name=? and interface_unit=?",array($pop_id,$interface,$unit));
	return $interface_data;

}
function get_single_pop_data($pop_id,$interface,$vlan_id)
{
	$insertion_interface=dbFetchRows("select * from nxg_router_interface_mapping where pop_details_id=? and device_interface_name =? and vlan_id = ?",array($pop_id,$interface,$vlan_id));
        return $insertion_interface;
}
function get_all_interface_data($pop_id)
{
        $insertion_interface=dbFetchRows("select * from nxg_router_interface_mapping where pop_details_id=?",array($pop_id));
        return $insertion_interface;
}
function get_exabgp_details($pop_id)
{
         $bgpgroup_names=dbFetchRow("select nxg_pop_details.loopback_ip,nxg_exabgp_details.* from nxg_pop_details INNER JOIN nxg_exabgp_details ON nxg_pop_details.id =nxg_exabgp_details.pop_details_id where nxg_pop_details.id=?",array($pop_id));

        return $bgpgroup_names;
}

function get_netflow_syslog_details($pop_id)
{
         $netflow_syslog_details=dbFetchRow("select netflow_server_ip,netflow_source_address,netflow_port,syslog_host_ip from nxg_pop_details where id=?",array($pop_id));

        return $netflow_syslog_details;
}

?>

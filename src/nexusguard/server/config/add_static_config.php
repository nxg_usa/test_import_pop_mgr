<?php

include_once("/opt/observium/html/nexusguard/api/common/common.inc.php");
include_once("/opt/observium/includes/defaults.inc.php");
include_once("/opt/observium/config.php");
include_once("/opt/observium/includes/definitions.inc.php");
include_once("/opt/observium/nexusguard/common/functions.inc.php");
include_once '/opt/observium/nexusguard/resource/ip_mgmt_config.php';
include_once('/opt/observium/nexusguard/db/db_add_diversion.php');

function get_static_route_config($static_data){
 $logtype="StaticRoute_config";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

$diversion_type = strtolower($static_data ['diversion_type']);
$pop_id=$static_data['pop_id'];
$add_static_config= array();

$all_isp_names = dbFetchRows("select name from nxg_isp_details where pop_details_id= ?",array($pop_id));


$manual_diversion_final_config_for_isp=array();
$manual_diversion_final_config_for_core=array();

foreach($all_isp_names as $parameter)
{
    if($diversion_type != "td" && $diversion_type != "tms" && $diversion_type != "tdtms")
    {
        $manual_diversion_config = dbFetchRow('select diversion_name as community  from  nxg_router_interface_mapping left join nxg_pop_interface_ip_mgmt on nxg_pop_interface_ip_mgmt.pop_id=nxg_router_interface_mapping.pop_details_id  and nxg_pop_interface_ip_mgmt.interface_unit=nxg_router_interface_mapping.vlan_id and nxg_pop_interface_ip_mgmt.interface_name=nxg_router_interface_mapping.device_interface_name where nxg_router_interface_mapping.scrubber_type="custom" and nxg_router_interface_mapping.scrubber_custom_name="'.$diversion_type.'"and  nxg_router_interface_mapping.pop_details_id='.$static_data['pop_id']);

    }
    else
    {
        $manual_diversion_config = dbFetchRow('select diversion_name as community  from  nxg_router_interface_mapping left join nxg_pop_interface_ip_mgmt on nxg_pop_interface_ip_mgmt.pop_id=nxg_router_interface_mapping.pop_details_id  and nxg_pop_interface_ip_mgmt.interface_unit=nxg_router_interface_mapping.vlan_id and nxg_pop_interface_ip_mgmt.interface_name=nxg_router_interface_mapping.device_interface_name where nxg_router_interface_mapping.scrubber_type="'.$diversion_type.'" and nxg_router_interface_mapping.pop_details_id='.$static_data['pop_id']);
    }

    $manual_diversion_array['isp_name'] = $parameter['name'];
    $manual_diversion_array['community'] = $manual_diversion_config['community'];
    $manual_diversion_array['route'] = $static_data['route_name'];
    $manual_diversion_array['destination'] = $static_data['network_prefix_ip'];
    $manual_diversion_array['network_prefix_subnet'] = $static_data['network_prefix_subnet_ip'];


    if (!empty($static_data['protocol']))
    {
        $manual_diversion_array['protocol'] = $static_data['protocol'];
    }
$manual_diversion_final_config_for_isp[]=$manual_diversion_array;
}
if ($all_isp_names != NULL)
{
    $manual_diversion_final_config_for_core['community'] = $manual_diversion_final_config_for_isp[0]['community'];
}
else
{
    
     if($diversion_type != "td" && $diversion_type != "tms" && $diversion_type != "tdtms")
    {
        $manual_diversion_config = dbFetchRow('select diversion_name as community  from  nxg_router_interface_mapping left join nxg_pop_interface_ip_mgmt on nxg_pop_interface_ip_mgmt.pop_id=nxg_router_interface_mapping.pop_details_id  and nxg_pop_interface_ip_mgmt.interface_unit=nxg_router_interface_mapping.vlan_id and nxg_pop_interface_ip_mgmt.interface_name=nxg_router_interface_mapping.device_interface_name where nxg_router_interface_mapping.scrubber_type="custom" and nxg_router_interface_mapping.scrubber_custom_name="'.$diversion_type.'"and  nxg_router_interface_mapping.pop_details_id='.$static_data['pop_id']);

    }
    else
    {
        $manual_diversion_config = dbFetchRow('select diversion_name as community  from  nxg_router_interface_mapping left join nxg_pop_interface_ip_mgmt on nxg_pop_interface_ip_mgmt.pop_id=nxg_router_interface_mapping.pop_details_id  and nxg_pop_interface_ip_mgmt.interface_unit=nxg_router_interface_mapping.vlan_id and nxg_pop_interface_ip_mgmt.interface_name=nxg_router_interface_mapping.device_interface_name where nxg_router_interface_mapping.scrubber_type="'.$diversion_type.'" and nxg_router_interface_mapping.pop_details_id='.$static_data['pop_id']);
    }
    
    $manual_diversion_final_config_for_core['community'] = $manual_diversion_config['community'];

}
$manual_diversion_final_config_for_core['route'] = $static_data['route_name'];
$manual_diversion_final_config_for_core['destination'] = $static_data['network_prefix_ip'];
$manual_diversion_final_config_for_core['network_prefix_subnet'] = $static_data['network_prefix_subnet_ip'];
if (!empty($static_data['protocol']))
{
$manual_diversion_final_config_for_core['protocol'] = $static_data['protocol'];
}





$manual_diversion_final_config_array['manual_diversion_for_isp']=$manual_diversion_final_config_for_isp;
$manual_diversion_final_config_array['manual_diversion_for_core']=$manual_diversion_final_config_for_core;

return $manual_diversion_final_config_array;
}
 ?>


<?php

include_once("/opt/observium/html/nexusguard/api/common/common.inc.php");
include_once("/opt/observium/includes/defaults.inc.php");
include_once("/opt/observium/config.php");
include_once("/opt/observium/includes/definitions.inc.php");
include_once("/opt/observium/nexusguard/common/functions.inc.php");
include_once '/opt/observium/nexusguard/resource/ip_mgmt_config.php';

function get_delete_filter_config($delete_filter_data)
{

$services=$delete_filter_data['service'];
$service= explode('-',$services);
$pop_name =dbFetchRow('select pop_name from nxg_pop_details where id='.$delete_filter_data['pop_id'].'');
$config_json = array();

if($delete_filter_data['filter_type']== "customer")
{
$count = dbFetchRow('select count(*) as count from nxg_traffic_filter where destination_prefix="'.$delete_filter_data['network_prefix_ip'].'" and destination_subnet="'.$delete_filter_data['network_prefix_subnet_ip'].'" and customer="'.$delete_filter_data['customer'].'" and filter_type="'.$delete_filter_data['filter_type'].'"');

$count_for_term = dbFetchRow('select count(*) as count_for_term from nxg_traffic_filter where filter_type="'.$delete_filter_data['filter_type'].'" and services="'.$service[0].'" and customer="'.$delete_filter_data['customer'].'" and pop_id='.$delete_filter_data['pop_id'].';');

$config_json['filter_name']=$delete_filter_data['filter_name'];
$config_json['filter_type']=$delete_filter_data['filter_type'];
$config_json['pop_id']=$delete_filter_data['pop_id'];
$config_json['pop_name']=$pop_name['pop_name'];
$config_json['customer']=$delete_filter_data['customer'];
$config_json['network_prefix_ip']=$delete_filter_data['network_prefix_ip'];
$config_json['network_prefix_subnet_ip']=$delete_filter_data['network_prefix_subnet_ip'];
$config_json['protocol']=$delete_filter_data['protocol'];
$config_json['port']=$delete_filter_data['port_no'];
$config_json['service']=$service[0];
$config_json['count']=$count['count'];
$config_json['count_for_term']=$count_for_term['count_for_term'];
}else{

$count = dbFetchRow('select count(*) as count from nxg_traffic_filter where destination_prefix="'.$delete_filter_data['network_prefix_ip'].'" and destination_subnet="'.$delete_filter_data['network_prefix_subnet_ip'].'" and pop_id='.$delete_filter_data['pop_id'].' and filter_type="'.$delete_filter_data['filter_type'].'"');

$count_for_term = dbFetchRow('select count(*) as count_for_term from nxg_traffic_filter where filter_type="'.$delete_filter_data['filter_type'].'" and services="'.$service[0].'" and pop_id='.$delete_filter_data['pop_id'].';');

$config_json['filter_name']=$delete_filter_data['filter_name'];
$config_json['filter_type']=$delete_filter_data['filter_type'];
$config_json['pop_id']=$delete_filter_data['pop_id'];
$config_json['pop_name']=$pop_name['pop_name'];
$config_json['customer']=$delete_filter_data['customer'];
$config_json['network_prefix_ip']=$delete_filter_data['network_prefix_ip'];
$config_json['network_prefix_subnet_ip']=$delete_filter_data['network_prefix_subnet_ip'];
$config_json['protocol']=$delete_filter_data['protocol'];
$config_json['port']=$delete_filter_data['port_no'];
$config_json['service']=$service[0];
$config_json['count']=$count['count'];
$config_json['count_for_term']=$count_for_term['count_for_term'];

}

$config =array('filter'=>$config_json);

return $config;
}
?>


<?php
include_once("/opt/observium/includes/defaults.inc.php");
include_once("/opt/observium/config.php");
include_once("/opt/observium/includes/definitions.inc.php");
include_once("/opt/observium/nexusguard/common/functions.inc.php");
include_once '/opt/observium/nexusguard/db/db_isp_functions.php';
include_once '/opt/observium/nexusguard/db/db_pop_functions.php';
include_once '/opt/observium/nexusguard/resource/ip_mgmt_config.php';

function generate_pop_isp_config($pop_id, $isp_data,$last_used_ip_address,$loopback_unit,$loopback_ip)
{
    $logtype="ISPConfig";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");
    
    $interface = 'lt-0/0/10';

    $name=$isp_data['general_name'];
    $pop_name = $isp_data['pop_name'];

    $isp_to_core_unit = get_unit($pop_id,$interface);
    $isp_to_core_ip = get_ip_address($pop_id, $interface);
    $core_to_isp_unit = $isp_to_core_unit+1;
    $core_to_isp_ip = increment_ip_by_one($isp_to_core_ip);

    $isp_interface_name = $isp_data['nb_interface'];
    $isp_vlan = $isp_data['nb_vlan_interface'];
    $local_ip = $isp_data['local_ip'][0];
    $neighbour_ip = $isp_data['neighbour_ip'][0];
    $local_subnet = $isp_data['local_ip_subnet'][0];
    $neighbour_subnet = $isp_data['neighbour_ip_subnet'][0];
    $local_as = $isp_data['local_as'][0];
    $peer_as = $isp_data['peer_as'][0];
    
    $loopback_ip = increment_ip_by_one($loopback_ip);
    $loopback_unit = $loopback_unit + 1;
    if($pop_id)
    {
        add_config_to_db($pop_id, $isp_to_core_unit,$interface,$isp_to_core_ip);
        add_config_to_db($pop_id, $core_to_isp_unit,$interface,$core_to_isp_ip);
    }


    $isp_to_core_desc = $pop_name." : VR-CORE : ".$interface.".".$core_to_isp_unit;
    $core_to_isp_desc = $pop_name." : VR-".$name ." : ".$interface.".".$isp_to_core_unit;

    $isp_to_core_intf_config[]=array('unit'=>$isp_to_core_unit,'description'=>$isp_to_core_desc, 'peer_unit'=>$core_to_isp_unit,'ipv4_address'=>$isp_to_core_ip."/31");
    $isp_to_core_intf_config[]=array('unit'=>$core_to_isp_unit,'description'=>$core_to_isp_desc ,'ipv4_address'=>$core_to_isp_ip."/31",'peer_unit'=>$isp_to_core_unit);
    

    
    $loopback_intf_config[]=array('unit'=>$loopback_unit, 'ipv4_address'=>$loopback_ip."/32");
    $loopback = array('interface_name'=>'lo0','interface_configuration'=>$loopback_intf_config);

    $isp_to_core_config=array('interface_name'=>$interface,'interface_configuration'=>$isp_to_core_intf_config);
    $core_to_isp_config = array('interface_name'=>$isp_interface_name,'interface_configuration'=>array(array('unit'=>0,'vlan_id'=>$isp_vlan,'ipv4_address'=>$local_ip."/".$neighbour_subnet)));

    $routing_interface[] = array('name'=>$interface.".".$isp_to_core_unit);
    $routing_interface[] = array('name'=>$isp_interface_name.".".$isp_vlan);
    

    $isp_routing_instance = array('interfaces'=>$routing_interface,'local_address'=>$local_ip,'local_as'=>$local_as,'peer_as'=>$peer_as,'neighbor_ip'=>$neighbour_ip,'ospf_interface_state'=>'state','loopback'=>'lo0.'.$loopback_unit);

    $vr_core_routing_instance= array('interface'=>$interface.".".$core_to_isp_unit);

    $interfaces[] = $isp_to_core_config;
    $interfaces[] = $core_to_isp_config;
    $interfaces[] = $loopback;
    $config = array('name'=>$name,'interfaces'=>$interfaces,'isp_routing_instance'=>$isp_routing_instance,'vr_core_routing_instance'=>$vr_core_routing_instance);

    
    return $config;
}

function get_isp_config($isp_data)
{
    $logtype="ISPConfig";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    $pop_id = $isp_data['pop_id'];

    $pop_details = get_sinlge_pop_detail($pop_id);
    $pop_name = $pop_details['pop_name'];


    $interface = 'lt-0/0/10';

    $name=$isp_data['isp_name'];
    $pop_id = $isp_data['pop_id'];

    $isp_interface_name = $isp_data['service_interface'];
    $isp_vlan = $isp_data['service_interface_vlan'];
    $isp_unit = $isp_data['service_interface_unit'];
    $local_ip = $isp_data['local_ip'];
    $neighbour_ip = $isp_data['neighbour_ip'];
    $local_subnet = $isp_data['local_ip_subnet'];
    $neighbour_subnet = $isp_data['neighbour_ip_subnet'];
    $local_as = $isp_data['local_as'];
    $peer_as = $isp_data['peer_as'];
    $lag_interface = $isp_data['lag_interface'];
    $md5_passphrase = $isp_data['md5_passphrase'];
    $restart_timer = $isp_data['restart_timer'];
    $stale_routes_time = $isp_data['stale_routes_time'];
    $isp_type = strtoupper($isp_data['isp_type']);

    $pop_id_id=$pop_id;
    $isp_to_core_unit = get_unit_by_type($pop_id,$interface,"isp");
    $isp_gen_ip = get_ip_address_by_subnet($pop_id, $interface,"31","isp","ifl");
    $isp_to_core_ip = $isp_gen_ip[0];
    $core_to_isp_unit = $isp_to_core_unit+1;
    $core_to_isp_ip = $isp_gen_ip[1];
    $routing_interface = array();


//    add_config_to_db($pop_id, $isp_to_core_unit,$interface,$isp_to_core_ip,'VR_CORE',"isp","ifl");
//    add_config_to_db($pop_id, $core_to_isp_unit,$interface,$core_to_isp_ip,'VR-'.$name,"isp","ifl");


    $routing_interface[]['name'] = $interface.".".$isp_to_core_unit;
    $isp_to_core_desc = $pop_name." : VR-CORE : ".$interface.".".$core_to_isp_unit;
    $core_to_isp_desc = $pop_name." : VR-".$name ." : ".$interface.".".$isp_to_core_unit;

    $isp_to_core_intf_config[]=array('unit'=>$isp_to_core_unit,'description'=>$isp_to_core_desc, 'peer_unit'=>$core_to_isp_unit,'ipv4_address'=>$isp_to_core_ip."/31",'vrname'=>'VR-'.$name,'iftype'=>'ifl');
    $isp_to_core_intf_config[]=array('unit'=>$core_to_isp_unit,'description'=>$core_to_isp_desc ,'ipv4_address'=>$core_to_isp_ip."/31",'peer_unit'=>$isp_to_core_unit,'vrname'=>'VR-CORE','iftype'=>'ifl');


    $isp_to_core_config=array('interface_name'=>$interface,'interface_configuration'=>$isp_to_core_intf_config);

    if(!is_array($isp_interface_name))
    {
        $isp_interface_name = array($isp_interface_name);
        $isp_vlan = array($isp_vlan);
        $isp_unit = array($isp_unit);
        $local_ip = array($local_ip);
        $neighbour_ip = array($neighbour_ip);
        $local_subnet = array($local_subnet);
        $neighbour_subnet = array($neighbour_subnet);
        $local_as = array($local_as);
        $peer_as = array($peer_as);
        $lag_interface = array($lag_interface);
        $md5_passphrase = array($md5_passphrase);
        $restart_timer = array($restart_timer);
        $stale_routes_time = array($stale_routes_time);
    }

    $intf_array = array();    
    $bgp_group = array();
    $passive_interfaces = array();

    for($i=0;$i<count($local_ip);$i++)
    {
        $interface_name = $isp_interface_name[$i];
        $vlan = $isp_vlan[$i];
        $unit = $isp_unit[$i];
        $local_intf_ip = $local_ip[$i];
        $neighbour_intf_ip = $neighbour_ip[$i];
        $local_ip_sub = $local_subnet[$i];
        $neighbour_ip_sub = $neighbour_subnet[$i];
        $local_as_no = $local_as[$i];
        $peer_as_no = $peer_as[$i];
        $member_ports = $lag_interface[$i][0];
        $md5 = $md5_passphrase[$i];
        $timer = $restart_timer[$i];
        $timeout = $stale_routes_time[$i];
        if(array_key_exists($interface_name,$intf_array))
        {
            $intf_config = $intf_array[$interface_name]['interface_configuration'];
            $cnt = count($intf_config);
            $intf_config[$cnt]['unit'] = $unit;
            if($vlan)
                $intf_config[$cnt]['vlan_id'] = $vlan;
            $intf_config[$cnt]['ipv4_address'] = $local_intf_ip."/".$local_ip_sub;
            $intf_array[$interface_name]['interface_configuration'] = $intf_config;
            $intf_array[$interface_name]['interface_name']=$interface_name;
            foreach($member_ports as $ports)
            {
                if(!is_array($ports))
                {
                    $ports = array($ports);
                }
                foreach($ports as $port)
                {
                    $intf_array[$interface_name]['members_port'][]['name'] = $port;
                    add_config_to_db($pop_id, NULL,$ports,NULL,NULL);
                }
            }
        }
        else
        {
            $intf_config = array();
            $intf_config['unit'] = $unit;
            if($vlan )
                $intf_config['vlan_id'] = $vlan;
            $intf_config['ipv4_address'] = $local_intf_ip."/".$local_ip_sub;
            $intf_array[$interface_name]['interface_configuration'] = array($intf_config);
            $intf_array[$interface_name]['interface_name']=$interface_name;
            foreach($member_ports as $ports)
            {
                if(!is_array($ports))
                {
                    $ports = array($ports);
                }
                foreach($ports as $port)
                {
                    $intf_array[$interface_name]['members_port'][]['name'] = $port; 
                    add_config_to_db($pop_id, NULL,$ports,NULL,NULL);
                }
            }

        }
        $bgp_details = array();
        $bgp_details['local_as'] = $local_as_no;
        $bgp_details['peer_as'] = $peer_as_no;
        $bgp_details['local_address'] = $local_intf_ip;
        $bgp_details['neighbor_ip'] = $neighbour_intf_ip;
    
        if(!empty($md5))
            $bgp_details['md5_passphrase'] = $md5;
        if(!empty($timer))
            $bgp_details['restart_timer'] = $timer;

        $bgp_details['stale_routes_time'] = $timeout;
        $bgp_details['group_name'] = $name.".".$i;
        $bgp_group[] = $bgp_details;

        $passive_interfaces[]['name'] = $interface_name.".".$unit;
        $routing_interface[]['name'] = $interface_name.".".$unit;  

    }

//    $core_to_isp_config = array('interface_name'=>$isp_interface_name,'interface_configuration'=>array(array('unit'=>0,'vlan_id'=>$isp_vlan,'ipv4_address'=>$local_ip."/".$neighbour_subnet)));

    $bgp_community_array = array();
    $bgp_community_names = $isp_data['bgp_community_names'][0];
    foreach($bgp_community_names as $community)
    {
        $array = array();
        $tmp = explode("~~~",$community);
        $community_name = trim($tmp[0]);
        $community_member = trim($tmp[1]);
        $array['name'] = $community_name;
        $array['member'] = $community_member;
        $bgp_community_array[] = $array;
    }

    $vr_core_routing_instance= array('interface'=>$interface.".".$core_to_isp_unit);

    $interfaces[] = $isp_to_core_config;
    foreach ($intf_array as $intf=>$config)
    {
        $interfaces[] = $config;
    }
/*    $loopback_ip = get_ip_address_by_subnet($pop_id,'lo0',"32",'lo0','lo0')[0];
    $loopback_unit = get_unit_by_type($pop_id,'lo0',"lo0");

    $loopback_intf_config[]=array('unit'=>$loopback_unit, 'ipv4_address'=>$loopback_ip."/32");

    $loopback = array('interface_name'=>'lo0','interface_configuration'=>$loopback_intf_config);


    add_config_to_db($pop_id, $loopback_unit,'lo0',$loopback_ip,'VR-'.$name,"lo0","lo0");

    $interfaces[] = $loopback;
*/
    $isp_routing_instance = array('interfaces'=>$routing_interface,'bgp_protocol'=>$bgp_group,'ospf_interface_state'=>'state');

    $pl_export_list = dbFetchRows("select * from nxg_pl_agg_prefix_list where pop_id = ?",array($pop_id));
    $route_aggregate_export = array();

    foreach($pl_export_list as $list)
    {
        $route_aggregate_export[]['route'] = $list['prefix'];
    }
    
    $config = array('name'=>$name,'pop_name'=>$pop_name,'interfaces'=>$interfaces,'isp_routing_instance'=>$isp_routing_instance,'vr_core_routing_instance'=>$vr_core_routing_instance,'passive_interfaces'=>$passive_interfaces,'bgp_communities'=>$bgp_community_array,'route_aggregate'=>$route_aggregate_export,'isp_type'=>$isp_type);

    return $config;
}

function delete_isp_config($isp_id)
{
    $isp_details = get_isp_details($isp_id);
    $isp_config = get_isp_generated_config($isp_id);
    $isp_name = $isp_details['name'];
    $all_interface_config = array();
    $members_ports = array();
    $bgp_communities = array();
    $vr_core_intf = "";

    foreach($isp_config as $config)
    {
        $intf_name = $config['interface_name'];
        $interface_unit = $config['interface_unit'];

        $intf_unit_config = array();
        if(array_key_exists($intf_name,$all_interface_config))
        {
            $intf_unit_config = $all_interface_config[$intf_name]['interface_configuration'];
            $cnt = count($intf_unit_config);
            $intf_unit_config[$cnt] = array('unit'=>$interface_unit);
        }
        else
        {
            $intf_unit_config[] = array('unit'=>$interface_unit);
        }
        $intf_config = array('interface_name'=>$intf_name,'interface_configuration'=>$intf_unit_config);
        $all_interface_config[$intf_name] = $intf_config;
        if($config['routing_instance'] == "VR-CORE")
        {
            $vr_core_intf = $intf_name.".".$interface_unit;
        }
    }


    $bgp_config = get_isp_details_with_bgp($isp_id);
    $member_check_array = array();

    foreach($bgp_config as $config)
    {
        $intf_name = $config['service_interface'];
        $interface_unit = $config['service_interface_unit'];

        $intf_unit_config = array();
        if(array_key_exists($intf_name,$all_interface_config))
        {
            $intf_unit_config = $all_interface_config[$intf_name]['interface_configuration'];
            $cnt = count($intf_unit_config);
            $intf_unit_config[$cnt] = array('unit'=>$interface_unit);
        }
        else
        {
            $intf_unit_config[] = array('unit'=>$interface_unit);
        }
        $intf_config = array('interface_name'=>$intf_name,'interface_configuration'=>$intf_unit_config);
        $all_interface_config[$intf_name] = $intf_config;

        $bgp_members_ports = get_interface_member_ports($isp_details['pop_id'],$intf_name);
        foreach($bgp_members_ports as $port)
        {
            $mport_name = $port['member_interface_name'];
            if(array_key_exists($mport_name,$member_check_array))
                continue;

            $members_ports[]['name']=$mport_name;
            $member_check_array[$mport_name]="yes";
        }
    }

    $db_bgp_communities = get_isp_bgp_communities($isp_id);
    foreach($db_bgp_communities as $communities)
    {
       $bgp_communities[]['name'] = $communities['community_name'];
    } 

    $interfaces = array();
    foreach($all_interface_config as $interace => $value)
    {
        $interfaces[] = $value;
    }

    $delete_isp_json = array('interfaces'=>$interfaces,'name'=>$isp_name,'member_interfaces'=>$members_ports,'bgp_communities'=>$bgp_communities);

    if(!empty($vr_core_intf))
    {
        $delete_isp_json['vr_core_interface'] = $vr_core_intf;
    }

    return $delete_isp_json;
}

function modify_isp_config($isp_data)
{
    $name=$isp_data['isp_name'];
    $local_ip = $isp_data['local_ip'];
    $neighbour_ip = $isp_data['neighbour_ip'];
    $local_subnet = $isp_data['local_ip_subnet'];
    $neighbour_subnet = $isp_data['neighbour_ip_subnet'];
    $local_as = $isp_data['local_as'];
    $peer_as = $isp_data['peer_as'];
    $isp_interface_name = $isp_data['service_interface'];
    $isp_vlan = $isp_data['service_interface_vlan'];
    $isp_unit = $isp_data['service_interface_unit'];
    $isp_id = $isp_data['isp_id'];
    $bgp_id = $isp_data['bgp_id'];
    $lag_interface = $isp_data['lag_interface'];
    $md5_passphrase = $isp_data['md5_passphrase'];
    $restart_timer = $isp_data['restart_timer'];
    $stale_routes_time = $isp_data['stale_routes_time'];
     $isp_type = strtoupper($isp_data['isp_type']);

    $isp_db_config = get_isp_details_with_bgp($isp_id);
    $bgp_config = process_bgp_array($isp_db_config);
    $name = $isp_data['isp_name'];
    $interface_name = $isp_db_config['service_interface'];
    $interface_unit = $isp_db_config['service_interface_unit'];

    $delete_local_as = array();
    $delete_peer_as = array();
    $delete_neighbour_ip = array();
    $delete_local_ip = array();
    $delete_local_ip_subnet = array();
    $delete_bgp_groups = array();
    $delete_interface = array();
    $add_local_as = array();
    $add_peer_as = array();
    $add_neighbour_ip = array();
    $add_local_ip = array();
    $add_local_subnet = array();
    
      if(!is_array($isp_interface_name))
    {
        $isp_interface_name = array($isp_interface_name);
        $isp_vlan = array($isp_vlan);
        $local_ip = array($local_ip);
        $neighbour_ip = array($neighbour_ip);
        $local_subnet = array($local_subnet);
        $neighbour_subnet = array($neighbour_subnet);
        $local_as = array($local_as);
        $peer_as = array($peer_as);
        $isp_unit = array($isp_unit);
        $bgp_id = array($bgp_id);
        $lag_interface = array($lag_interface);
        $md5_passphrase = array($md5_passphrase);
        $restart_timer = array($restart_timer);
        $stale_routes_time = array($stale_routes_time);
    }
    
    $intf_array = array();
    $bgp_group = array();
    $passive_interfaces = array();
    $add_interface = array();
    $add_members_ports = array();
    $del_members_ports = array();
    $del_md5 = array();
    $add_md5 = array();
    $del_restart_timer = array();
    $add_restart_timer = array();
    $del_timeout = array();
    $add_timeout = array();
    $add_bgp_community = array();
    $del_bgp_community = array();
    $add_vlan = array();
    $del_vlan = array();
    $del_member = array();
    $bgp_member_ports = array();
    for($i=0;$i<count($isp_interface_name);$i++)
    {
        $bgp_key = $bgp_id[$i];
        $local_asn = $local_as[$i];
        $peer_asn = $peer_as[$i];
        $intf_local_ip = $local_ip[$i];
        $nbh_ip = $neighbour_ip[$i];
        $local_ip_sub = $local_subnet[$i];
        $interface_name = $isp_interface_name[$i];
        $vlan_id = $isp_vlan[$i];
        $unit = $isp_unit[$i];
        $member_ports = $lag_interface[$i][0];
        $md5 = $md5_passphrase[$i];
        $timer = $restart_timer[$i];
        $timeout = $stale_routes_time[$i];

	$pop_id = $isp_data['pop_id'];
	$pop_details = get_sinlge_pop_detail($pop_id);
	$pop_name = $pop_details['pop_name'];
	$isp_name = $isp_data['isp_name'];


        if(array_key_exists($bgp_key,$bgp_config))
        {
            $isp_db_config = $bgp_config[$bgp_key];
            if($isp_db_config['local_as'] != $local_asn)
            {
                $delete_local_as[] = array('local_as'=>$isp_db_config['local_as'],'bgp_group'=>$isp_db_config['bgp_group']);
                $add_local_as[] = array('local_as'=>$local_asn,'bgp_group'=>$isp_db_config['bgp_group']);
            }
            if($isp_db_config['peer_as'] != $peer_asn)
            {
                $delete_peer_as[] = array('peer_as'=>$isp_db_config['peer_as'],'bgp_group'=>$isp_db_config['bgp_group']);
                $add_peer_as[] = array('peer_as'=>$peer_asn,'bgp_group'=>$isp_db_config['bgp_group']);
            }
            
            if(($isp_interface_name[$i] != $isp_db_config['service_interface']) || ($unit != $isp_db_config['service_interface_unit']))
            {
                if($isp_interface_name[$i] != $isp_db_config['service_interface'])
                {
                    $bgp_members_ports = get_interface_member_ports($pop_id,$isp_db_config['service_interface']);
                    foreach($bgp_members_ports as $port)
                    {
                        $del_member[$port['ae_interface_name']][]=$port['member_interface_name'];
                    }
                  
                    foreach($member_ports as $ports)
                    {
                        $array = array();
                        $array['name'] = $ports;
                        $array['ae_interface'] = $isp_interface_name[$i];
                        $add_members_ports[] = $array;
                        add_config_to_db($pop_id, NULL,$ports,NULL,NULL);
                    }

                }
                $delete_interface[] = array('interface_name'=>$isp_db_config['service_interface'],'interface_unit'=>$isp_db_config['service_interface_unit'],'isp_name'=>$isp_name,'pop_name'=>$pop_name);
                if($vlan_id)
                    $add_interface[] = array('interface_name'=>$isp_interface_name[$i],'interface_unit'=>$unit,'vlan_id'=>$vlan_id,'local_ip'=>$intf_local_ip,'local_ip_subnet'=>$local_ip_sub,'isp_name'=>$isp_name,'pop_name'=>$pop_name);
                else
                    $add_interface[] = array('interface_name'=>$isp_interface_name[$i],'interface_unit'=>$unit,'local_ip'=>$intf_local_ip,'local_ip_subnet'=>$local_ip_sub,'isp_name'=>$isp_name,'pop_name'=>$pop_name);
                    
            }else
            {
                if(($isp_db_config['local_ip'] != $intf_local_ip) || ($isp_db_config['subnet_mask'] != $local_ip_sub))
                {
                    $delete_local_ip[] = array('local_ip'=>$isp_db_config['local_ip'],'local_ip_subnet'=>$isp_db_config['subnet_mask'],'interface_name'=>$isp_db_config['service_interface'],'interface_unit'=>$isp_db_config['service_interface_unit'],'bgp_group'=>$isp_db_config['bgp_group']);
                    if($vlan_id)
                        $add_local_ip[] = array('local_ip'=>$intf_local_ip,'local_ip_subnet'=>$local_ip_sub,'interface_name'=>$interface_name,'interface_unit'=>$unit,'vlan_id'=>$vlan_id,'bgp_group'=>$isp_db_config['bgp_group']);
                    else
                        $add_local_ip[] = array('local_ip'=>$intf_local_ip,'local_ip_subnet'=>$local_ip_sub,'interface_name'=>$interface_name,'interface_unit'=>$unit,'bgp_group'=>$isp_db_config['bgp_group']);
                }
                //else if($isp_db_config['service_interface_vlan'] != $vlan_id  )
                else
                {
                    if($isp_db_config['service_interface_vlan'] != $vlan_id  )
                    {
                        if(isset($isp_db_config['service_interface_vlan']))
                            $del_vlan[] = array('interface_name'=>$interface_name,'interface_unit'=>$unit,'vlan'=>$isp_db_config['service_interface_vlan']);
                        if($vlan_id)
                            $add_vlan[] = array('interface_name'=>$interface_name,'interface_unit'=>$unit,'vlan'=>$vlan_id);
                        else
                            $add_vlan[] = array('interface_name'=>$interface_name,'interface_unit'=>$unit);
                    }
                }
                
            }

            if($isp_interface_name[$i] == $isp_db_config['service_interface'])
            {
                $tmp_ae_members = array();
/*                $bgp_mem_count = count($bgp_members_ports);
                $mem_count = count($member_ports);
                for($j= 0;$j<$bgp_mem_count; $j++)
                {
                    $is_intf_same = 0;
                    for($k =0 ;$k<$mem_count ;$k++)
                    {
                        if($bgp_members_ports[$j]['member_interface_name']==$member_ports[$k])
                        {
                            unset($bgp_members_ports[$j]);
                            unset($member_ports[$k]);
                        }
                    }
                }
*/

                foreach($member_ports as $ports)
                {
                    $array = array();
                    $is_member_present = dbFetchRow("select count(*) as cnt from nxg_pop_ae_members where pop_id = ? and ae_interface_name =? and member_interface_name =? ",array($pop_id,$isp_interface_name[$i],$ports));

                    if($is_member_present['cnt'] <= 0)
                    {

                        $array['name'] = $ports;
                        $array['ae_interface'] = $isp_interface_name[$i];
                        $add_members_ports[] = $array;
                    }
                    $tmp_ae_members [] = $ports;
//                    add_config_to_db($pop_id, NULL,$ports,NULL,NULL);
                }

                if(count($tmp_ae_members) > 0 )
                {
                    if(count($tmp_ae_members) == 1)
                    {
                        $tmp_ae_members[0] = "'".$tmp_ae_members[0]."'";
                    }
                    $bgp_members_ports = dbFetchRows("select ae_interface_name,member_interface_name from nxg_pop_ae_members where member_interface_name not in (?) and pop_id = ? and ae_interface_name = ?", array($tmp_ae_members,$pop_id, $isp_db_config['service_interface']));
                }
                else
                {
                    $bgp_members_ports = dbFetchRows("select ae_interface_name,member_interface_name from nxg_pop_ae_members where pop_id = ? and ae_interface_name = ?", array($pop_id, $isp_db_config['service_interface']));

                }
               foreach($bgp_members_ports as $port)
                {
                    $del_member[$port['ae_interface_name']][]=$port['member_interface_name'];
                }

            }

            if(trim($isp_db_config['neighbor_ip']) != trim($nbh_ip))
            {
                $delete_neighbour_ip[] = array('neighbour_ip'=>$isp_db_config['neighbor_ip'],'bgp_group'=>$isp_db_config['bgp_group']);
                $array['neighbour_ip'] =$nbh_ip;
                $array['bgp_group']=$isp_db_config['bgp_group'];
                if($isp_db_config['md5_passphrase'] != $md5)
                {
                    $array['md5'] = $md5;
                }
                else
                {
                    $array['md5'] = $sp_db_config['md5_passphrase'];
                }
                if($isp_db_config['restart_timer'] != $timer)
                {
                    $array['restart_timer']=$timer;
                }
                else
                {
                    $array['restart_timer']=$isp_db_config['restart_timer'];
                }
                if($isp_db_config['bgp_stale_routes_time'] != $timeout)
                {
                    $array['timeout'] = $timeout;
                }
                else
                {
                    $array['timeout'] = $isp_db_config['bgp_stale_routes_time'];
                }
                $add_neighbour_ip[] = $array;

            }
            else
            {
                if($isp_db_config['md5_passphrase'] != $md5)
                {
                    $del_md5[] = array('md5'=>$isp_db_config['md5_passphrase'],'bgp_group'=>$isp_db_config['bgp_group'],'neighbour_ip'=>$isp_db_config['neighbor_ip']);
                    $add_md5[] = array('md5'=>$md5,'bgp_group'=>$isp_db_config['bgp_group'],'neighbour_ip'=>$isp_db_config['neighbor_ip']);
                }
                if($isp_db_config['restart_timer'] != $timer)
                {
                    $del_restart_timer[] = array('restart_timer'=>$isp_db_config['restart_timer'],'bgp_group'=>$isp_db_config['bgp_group'],'neighbour_ip'=>$isp_db_config['neighbor_ip']);
                    $add_restart_timer[] = array('restart_timer'=>$timer,'bgp_group'=>$isp_db_config['bgp_group'],'neighbour_ip'=>$isp_db_config['neighbor_ip']);
                }
                if($isp_db_config['bgp_stale_routes_time'] != $timeout)
                {
                    $del_timeout[] = array('timeout'=>$isp_db_config['bgp_stale_routes_time'],'bgp_group'=>$isp_db_config['bgp_group'],'neighbour_ip'=>$isp_db_config['neighbor_ip']);
                    $add_timeout[] = array('timeout'=>$timeout,'bgp_group'=>$isp_db_config['bgp_group'],'neighbour_ip'=>$isp_db_config['neighbor_ip']);
                }
            }
            foreach($member_ports as $ports)
            {
                $bgp_member_ports[] = "'".$ports."'";
            }
            unset($bgp_config[$bgp_key]);
        }
        else
        {
            $new_bgp_group = get_bgp_group_db($isp_id);
            $new_bgp_group = $new_bgp_group +1 ;
            $bgp_group[] = array('neighbour_ip'=>$nbh_ip,'local_as'=>$local_asn,'bgp_group'=>$new_bgp_group,'local_ip'=>$intf_local_ip,'peer_as'=>$peer_asn,'md5'=>$md5,'restart_timer'=>$timer,'timeout'=>$timeout);
            if($vlan_id) 
               $add_interface[] = array('interface_name'=>$isp_interface_name[$i],'interface_unit'=>$unit,'vlan_id'=>$vlan_id,'local_ip'=>$intf_local_ip,'local_ip_subnet'=>$local_ip_sub,'pop_name'=>$pop_name,'isp_name'=>$isp_name);
            else
               $add_interface[] = array('interface_name'=>$isp_interface_name[$i],'interface_unit'=>$unit,'local_ip'=>$intf_local_ip,'local_ip_subnet'=>$local_ip_sub,'pop_name'=>$pop_name,'isp_name'=>$isp_name);
                
            foreach($member_ports as $ports)
            {
                $array = array();
                $array['name'] = $ports;
                $array['ae_interface'] = $isp_interface_name[$i];
                $add_members_ports[] = $array;

                add_config_to_db($pop_id, NULL,$ports,NULL,NULL);
            }

        }

    }         
    foreach ($bgp_config as $config)
    {
        $delete_bgp_groups[] = array('name'=>$config['bgp_group']);
        $delete_interface[] = array('interface_name'=>$config['service_interface'],'interface_unit'=>$config['service_interface_unit']);
/*        $bgp_members_ports = get_interface_member_ports($isp_id,$isp_db_config['service_interface']);
        foreach($bgp_members_ports as $port)
        {
            $del_member[]=$port['member_interface_name'];
        }
*/
    }
   
/*    if(count($bgp_member_ports) > 0)
    {
        $del_mem_ports = dbFetchRows("select member_interface_name from nxg_pop_ae_members where member_interface_name not in (?)",array($bgp_member_ports,$isp_id));
        if(count($del_mem_ports)> 0)
        {
            foreach($del_mem_ports as $port)
            {
                $del_member[] = $port['member_interface_name'];
            }
        }
    }
*/
 //   else
/*    {
        $del_mem_ports = dbFetchRows("select member_interface_name from nxg_pop_ae_members where isp_id = ? ",array($isp_id));
        if(count($del_mem_ports)> 0)
        {
            foreach($del_mem_ports as $port)
            {
                $del_member[] = $port['member_interface_name'];

            }
        }
    }
*/

    foreach($del_member as $key=>$interface)
    {
        $uniq_del_members = array_unique($interface);  
        $array = array();
        $array['ae_name'] = $key;
        foreach($uniq_del_members as $mport)
        { 
            $array['member_ports'][]['name']=$mport;
        }
            $del_members_ports[] = $array;
    } 

    $bgp_community_names = $isp_data['bgp_community_names'][0];

    $db_bgp_communities = get_isp_bgp_communities($isp_id);

    $mod_bgp_community = process_bgp_community($db_bgp_communities);

    foreach($bgp_community_names as $bgp_config)
    {

        $tmp = explode('~~~',$bgp_config);
        $id = $tmp[2];
        if(array_key_exists($id, $mod_bgp_community))
        {
            unset($mod_bgp_community[$id]);
            continue;
        }
        else
        {
            $array = array();
            $array['name'] = trim($tmp[0]);
            $array['member'] = trim($tmp[1]);
            $add_bgp_community[] = $array;
        }
    }
    foreach($mod_bgp_community as $del_community=>$value)
    {
       $del_bgp_community[]['name'] = $value['community_name'];
    }
        
    $db_isp = get_isp_details($isp_id);
 
    $config = array('name'=>$name,'delete_local_as'=>$delete_local_as,'delete_peer_as'=>$delete_peer_as,'delete_neighbour_ip'=>$delete_neighbour_ip,'delete_local_ip'=>$delete_local_ip,'delete_neighbour_ip'=>$delete_neighbour_ip,'delete_bgp_groups'=>$delete_bgp_groups,'delete_interface'=>$delete_interface,'add_local_as'=>$add_local_as,'add_peer_as'=>$add_peer_as,'add_neighbour_ip'=>$add_neighbour_ip,'add_local_ip'=>$add_local_ip,'add_neighbour_ip'=>$add_neighbour_ip,'bgp_group'=>$bgp_group,'add_interface'=>$add_interface,'del_md5'=>$del_md5,'add_md5'=>$add_md5,'del_restart_timer'=>$del_restart_timer,'add_restart_timer'=>$add_restart_timer,'del_timeout'=>$del_timeout,'add_timeout'=>$add_timeout,'add_members_ports'=>$add_members_ports,'del_members_ports'=>$del_members_ports,'add_bgp_community'=>$add_bgp_community,'del_bgp_community'=>$del_bgp_community,'add_vlan'=>$add_vlan,'del_vlan'=>$del_vlan,'del_isp_type'=>$db_isp['isp_type'],'add_isp_type'=>$isp_type);
   
     return $config;
    
}

function offline_isp_config($isp_data)
{

    $name = $isp_data['isp_name'];
    $config['name'] = $name;
    return $config;
}
function process_bgp_array($isp_db_config)
{
    $array = array();
    foreach($isp_db_config as $config)
    {
        $bgp_id = $config['bgp_group'];
        $array[$bgp_id] = $config;
    }
    return $array;
}
function process_bgp_community($bgp_config)
{
    $array = array();
    foreach($bgp_config  as $config)
    {
        $array[$config['community_id']] = $config;
    }
    return $array;
}
?>

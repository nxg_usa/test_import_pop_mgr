<?php
include_once("/opt/observium/html/nexusguard/api/common/common.inc.php");
include_once("/opt/observium/includes/defaults.inc.php");
include_once("/opt/observium/config.php");
include_once("/opt/observium/includes/definitions.inc.php");
include_once("/opt/observium/nexusguard/common/functions.inc.php");
include_once '/opt/observium/nexusguard/resource/ip_mgmt_config.php';

function get_add_filter_config($add_filter_data)
{

            $pop_id = $add_filter_data['pop_id'];
            $pop_name =dbFetchRow('select pop_name from nxg_pop_details where id='.$pop_id.'');

            $config_json = array();
            $services=$add_filter_data['service'];
            $service= explode('-',$services);

            if($add_filter_data['filter_type'] == "customer")
            {

                if($service[0]=="ICMP")
                {
                    $config_json['filter_name']=$add_filter_data['filter_name'];
                    $config_json['filter_type']=$add_filter_data['filter_type'];
                    $config_json['pop_id']=$add_filter_data['pop_id'];
                    $config_json['pop_name']=$pop_name['pop_name'];
                    $config_json['customer']=$add_filter_data['customer'];
                    $config_json['network_prefix_ip']=$add_filter_data['network_prefix_ip'];
                    $config_json['network_prefix_subnet_ip']=$add_filter_data['network_prefix_subnet_ip'];
                    if (!empty($add_filter_data['protocol']))
                     {
                    $config_json['protocol']=$add_filter_data['protocol'];
                     }
                    if (!empty($add_filter_data['port_no']))
                    {
                    $config_json['port_no']=$add_filter_data['port_no'];
                    }
                    $config_json['service']=$service[0];
                }else{

                    $config_json['filter_name']=$add_filter_data['filter_name'];
                    $config_json['filter_type']=$add_filter_data['filter_type'];
                    $config_json['pop_id']=$add_filter_data['pop_id'];
                    $config_json['pop_name']=$pop_name['pop_name'];
                    $config_json['customer']=$add_filter_data['customer'];
                    $config_json['network_prefix_ip']=$add_filter_data['network_prefix_ip'];
                    $config_json['network_prefix_subnet_ip']=$add_filter_data['network_prefix_subnet_ip'];
                    if (!empty($add_filter_data['protocol']))
                     {
                    $config_json['protocol']=$add_filter_data['protocol'];
                     }
                    if (!empty($add_filter_data['port_no']))
                    {
                    $config_json['port_no']=$add_filter_data['port_no'];
                    }
                    $config_json['service']=$service[0];
                }

            }else
            {


                if($service[0]=="ICMP"){
                    $config_json['filter_name']=$add_filter_data['filter_name'];
                    $config_json['filter_type']=$add_filter_data['filter_type'];
                    $config_json['pop_name']=$pop_name['pop_name'];
                    $config_json['network_prefix_ip']=$add_filter_data['network_prefix_ip'];
                    $config_json['network_prefix_subnet_ip']=$add_filter_data['network_prefix_subnet_ip'];
                    if (!empty($add_filter_data['protocol']))
                     {
                    $config_json['protocol']=$add_filter_data['protocol'];
                     }
                    if (!empty($add_filter_data['port_no']))
                    {
                    $config_json['port_no']=$add_filter_data['port_no'];
                    }
                    $config_json['service']=$service[0];
                }else{
                    $config_json['filter_name']=$add_filter_data['filter_name'];
                    $config_json['filter_type']=$add_filter_data['filter_type'];
                    $config_json['pop_name']=$pop_name['pop_name'];
                    $config_json['network_prefix_ip']=$add_filter_data['network_prefix_ip'];
                    $config_json['network_prefix_subnet_ip']=$add_filter_data['network_prefix_subnet_ip'];
                    if (!empty($add_filter_data['protocol']))
                     {
                    $config_json['protocol']=$add_filter_data['protocol'];
                     }
                    if (!empty($add_filter_data['port_no']))
                    {
                    $config_json['port_no']=$add_filter_data['port_no'];
                    }
                    $config_json['service']=$service[0];
                }
            }
            $config =array('filter'=>$config_json);
    return $config;
}
?>


<?php

include_once("/opt/observium/html/nexusguard/api/common/common.inc.php");
include_once("/opt/observium/includes/defaults.inc.php");
include_once("/opt/observium/config.php");
include_once("/opt/observium/includes/definitions.inc.php");
include_once("/opt/observium/nexusguard/common/functions.inc.php");
include_once '/opt/observium/nexusguard/resource/ip_mgmt_config.php';
include_once('/opt/observium/nexusguard/db/db_add_diversion.php');



function get_add_diversion_config($add_diversion_data,$pop_id ){


$add_diversion_array_for_local = array();
$add_diversion_array_for_global = array();
$previous_diversion_all_config_for_local = array();
$previous_diversion_all_config_for_global = array();
$add_diversion_all_config = array();
$previous_diversion_all_config_for_local= array();
$previous_diversion_all_config_for_global=array();
//$diversion_type = strtolower($add_diversion_data['diversion_type']);

$custom_diversion_type=(explode ('~~~',$add_diversion_data['diversion_type']));
if($custom_diversion_type[1] != "")
{
$diversion_type = $custom_diversion_type[1];
}
else
{
$diversion_type = $add_diversion_data['diversion_type'];
}
        $add_diversion_array_for_global['diversion_name'] = $add_diversion_data['diversion_name'];
        $add_diversion_array_for_global['pop_id'] = $pop_id;
        $add_diversion_array_for_global['network_prefix_ip'] = $add_diversion_data['network_prefix_ip'];
        $add_diversion_array_for_global['network_prefix_subnet_ip'] = $add_diversion_data['network_prefix_subnet_ip'];
        $add_diversion_array_for_global['source_ip'] = $add_diversion_data['source_ip'];
        $add_diversion_array_for_global['source_subnet'] = $add_diversion_data['source_subnet'];
        $add_diversion_array_for_global['source_port'] = $add_diversion_data['source_port'];
        if (!empty($add_diversion_data['protocol']))
        {
        $add_diversion_array_for_global['protocol'] = $add_diversion_data['protocol'];
        }
        $add_diversion_array_for_global['port_no'] = $add_diversion_data['port_no'];

        if($custom_diversion_type[0] == "custom")
        {
            $add_diversion_config_from_db = dbFetchRow('select max(target) as target from  nxg_pop_interface_ip_mgmt left join nxg_router_interface_mapping on nxg_pop_interface_ip_mgmt.pop_id=nxg_router_interface_mapping.pop_details_id and  nxg_pop_interface_ip_mgmt.interface_unit=nxg_router_interface_mapping.vlan_id and nxg_pop_interface_ip_mgmt.interface_name=nxg_router_interface_mapping.device_interface_name where nxg_router_interface_mapping.scrubber_type="custom" and nxg_router_interface_mapping.pop_details_id='.$pop_id.' and nxg_router_interface_mapping.scrubber_custom_name="'.$diversion_type.'"');
            
        }
        else
        {
            $add_diversion_config_from_db = dbFetchRow('select max(target) as target from  nxg_pop_interface_ip_mgmt left join nxg_router_interface_mapping on nxg_pop_interface_ip_mgmt.pop_id=nxg_router_interface_mapping.pop_details_id and  nxg_pop_interface_ip_mgmt.interface_unit=nxg_router_interface_mapping.vlan_id and nxg_pop_interface_ip_mgmt.interface_name=nxg_router_interface_mapping.device_interface_name where nxg_router_interface_mapping.pop_details_id='.$pop_id.' and nxg_router_interface_mapping.scrubber_type="'.$diversion_type.'"');
        }

       $exabgp_details_global=dbFetchRow('select local_speaker_md5_key_string,global_speaker_md5_key_string,local_speaker_ip,global_speaker_ip,local_server_ip,global_server_ip,local_asn_global,peer_asn_global,local_asn,peer_asn from nxg_exabgp_details where pop_details_id='.$pop_id.'');

       $add_diversion_array_for_global['md5_keystring'] = $exabgp_details_global['global_speaker_md5_key_string'];
       $global_speaker_ip=(explode ('/',$exabgp_details_global['global_speaker_ip']));
       $add_diversion_array_for_global['global_speaker_ip']= $global_speaker_ip[0];
       $add_diversion_array_for_global['global_server_ip']= $exabgp_details_global['global_server_ip'];
       $add_diversion_array_for_global['local_asn_global']= $exabgp_details_global['local_asn_global'];
       $add_diversion_array_for_global['peer_asn_global']= $exabgp_details_global['peer_asn_global'];
       $add_diversion_array_for_global['target_value'] = $add_diversion_config_from_db['target'];
       $add_diversion_array_for_global['target'] = "100:".$add_diversion_config_from_db['target'];
       $add_diversion_all_config_for_global[]=$add_diversion_array_for_global; 







        $previous_global_config_from_database_pop_id = dbFetchRows("select distinct id from nxg_pop_details");
        foreach ($previous_global_config_from_database_pop_id as $content )
        {
            $previous_all_config_from_database = dbFetchRows('select * from nxg_diversion_details where pop='.$content['id'].'');
            $exabgp_details_per_pop=dbFetchRow('select local_speaker_md5_key_string,global_speaker_md5_key_string,local_speaker_ip,global_speaker_ip,local_server_ip,global_server_ip,local_asn_global,peer_asn_global,local_asn,peer_asn from nxg_exabgp_details where pop_details_id='.$content['id'].'');
            foreach ($previous_all_config_from_database as $previous_config_from_database)
            {
                $previous_diversion_array_for_global['diversion_name'] = $previous_config_from_database['name'];
                $previous_diversion_array_for_global['pop_id'] = $content['id'];
                $previous_diversion_array_for_global['network_prefix_ip'] = $previous_config_from_database['network_ip'];
                $previous_diversion_array_for_global['network_prefix_subnet_ip'] = $previous_config_from_database['network_prefix'];
                $previous_diversion_array_for_global['source_ip'] = $previous_config_from_database['source_ip'];
                $previous_diversion_array_for_global['source_subnet'] = $previous_config_from_database['source_subnet'];
                $previous_diversion_array_for_global['source_port'] = $previous_config_from_database['source_port'];
                $previous_diversion_array_for_global['protocol'] = $previous_config_from_database['protocol'];
                $previous_diversion_array_for_global['port_no'] = $previous_config_from_database['port'];
                $global_speaker_ip_prev=(explode ('/',$exabgp_details_per_pop['global_speaker_ip']));
                $previous_diversion_array_for_global['global_speaker_ip']=$global_speaker_ip_prev[0];
                $previous_diversion_array_for_global['global_server_ip']= $exabgp_details_per_pop['global_server_ip'];
                $previous_diversion_array_for_global['local_asn_global']= $exabgp_details_per_pop['local_asn_global'];
                $previous_diversion_array_for_global['peer_asn_global']= $exabgp_details_per_pop['peer_asn_global'];
                $previous_diversion_array_for_global['target'] = "100:".$previous_config_from_database['target'];
                $previous_diversion_array_for_global['md5_keystring'] = $exabgp_details_per_pop['global_speaker_md5_key_string'];
                $previous_diversion_all_config_for_global[]=$previous_diversion_array_for_global;


            }
        }

   



$result=array();
$result['global_server']['gui']=$add_diversion_all_config_for_global;
$result['global_server']['prev']=$previous_diversion_all_config_for_global;            
##################################### FOR LOCAL #########################################

            $add_diversion_array_for_local_single_pop['diversion_name'] = $add_diversion_data ['diversion_name'];
            $add_diversion_array_for_local_single_pop['pop_id'] = $add_diversion_data ['pop_id'];
            $add_diversion_array_for_local_single_pop['network_prefix_ip'] = $add_diversion_data['network_prefix_ip'];
            $add_diversion_array_for_local_single_pop['network_prefix_subnet_ip'] = $add_diversion_data['network_prefix_subnet_ip'];
            $add_diversion_array_for_local_single_pop['source_ip'] = $add_diversion_data['source_ip'];
            $add_diversion_array_for_local_single_pop['source_subnet'] = $add_diversion_data['source_subnet'];
            $add_diversion_array_for_local_single_pop['source_port'] = $add_diversion_data['source_port'];
            if (!empty($add_diversion_data['protocol']))
            {
            $add_diversion_array_for_local_single_pop['protocol'] = $add_diversion_data['protocol'];
            }
            $add_diversion_array_for_local_single_pop['port_no'] = $add_diversion_data['port_no'];



         if($custom_diversion_type[0] == "custom")
         {
             $add_diversion_config_from_db_for_single_pop = dbFetchRow('select max(target) as target from  nxg_pop_interface_ip_mgmt left join nxg_router_interface_mapping on nxg_pop_interface_ip_mgmt.pop_id=nxg_router_interface_mapping.pop_details_id and  nxg_pop_interface_ip_mgmt.interface_unit=nxg_router_interface_mapping.vlan_id and nxg_pop_interface_ip_mgmt.interface_name=nxg_router_interface_mapping.device_interface_name where nxg_router_interface_mapping.scrubber_type="custom" and nxg_router_interface_mapping.pop_details_id='.$add_diversion_data ['pop_id'].' and nxg_router_interface_mapping.scrubber_custom_name="'.$diversion_type.'"');
     
        }
        else
        {
             $add_diversion_config_from_db_for_single_pop = dbFetchRow('select max(target) as target from  nxg_pop_interface_ip_mgmt left join nxg_router_interface_mapping on nxg_pop_interface_ip_mgmt.pop_id=nxg_router_interface_mapping.pop_details_id and  nxg_pop_interface_ip_mgmt.interface_unit=nxg_router_interface_mapping.vlan_id and nxg_pop_interface_ip_mgmt.interface_name=nxg_router_interface_mapping.device_interface_name where nxg_router_interface_mapping.pop_details_id='.$add_diversion_data ['pop_id'].' and nxg_router_interface_mapping.scrubber_type="'.$diversion_type.'"');
        }

        $exabgp_details_local=dbFetchRow('select local_speaker_md5_key_string,global_speaker_md5_key_string,local_speaker_ip,global_speaker_ip,local_server_ip,global_server_ip,local_asn_global,peer_asn_global,local_asn,peer_asn from nxg_exabgp_details where pop_details_id='.$pop_id.'');

       $add_diversion_array_for_local_single_pop['md5_keystring'] = $exabgp_details_local['local_speaker_md5_key_string'];
       $local_speaker_ip_gui=(explode ('/',$exabgp_details_local['local_speaker_ip']));
       $add_diversion_array_for_local_single_pop['local_speaker_ip']= $local_speaker_ip_gui[0];
       $add_diversion_array_for_local_single_pop['local_server_ip']= $exabgp_details_local['local_server_ip'];
       $add_diversion_array_for_local_single_pop['local_asn']= $exabgp_details_local['local_asn'];
       $add_diversion_array_for_local_single_pop['peer_asn']= $exabgp_details_local['peer_asn'];
       $add_diversion_array_for_local_single_pop['target_value']= $add_diversion_config_from_db_for_single_pop['target'];
       $add_diversion_array_for_local_single_pop['target'] = "100:".$add_diversion_config_from_db_for_single_pop['target'];
       $add_diversion_all_config_for_local_single_pop[]=$add_diversion_array_for_local_single_pop;






         $previous_all_config_from_database = dbFetchRows('select * from nxg_diversion_details where pop='.$pop_id.'');
         $exabgp_details_per_pop=dbFetchRow('select local_speaker_md5_key_string,global_speaker_md5_key_string,local_speaker_ip,global_speaker_ip,local_server_ip,global_server_ip,local_asn_global,peer_asn_global,local_asn,peer_asn from nxg_exabgp_details where pop_details_id='.$pop_id.'');
            foreach ($previous_all_config_from_database as $previous_config_from_database)
            {
                $previous_diversion_array_for_local['diversion_name'] = $previous_config_from_database ['name'];
                $previous_diversion_array_for_local['pop_id'] = $previous_config_from_database['pop'];
                $previous_diversion_array_for_local['network_prefix_ip'] = $previous_config_from_database['network_ip'];
                $previous_diversion_array_for_local['network_prefix_subnet_ip'] = $previous_config_from_database['network_prefix'];
                $previous_diversion_array_for_local['source_ip'] = $previous_config_from_database['source_ip'];
                $previous_diversion_array_for_local['source_subnet'] = $previous_config_from_database['source_subnet'];
                $previous_diversion_array_for_local['source_port'] = $previous_config_from_database['source_port'];
                $previous_diversion_array_for_local['protocol'] = $previous_config_from_database['protocol'];
                $previous_diversion_array_for_local['port_no'] = $previous_config_from_database['port'];
                $local_speacker_ip_local=(explode ('/',$exabgp_details_per_pop['local_speaker_ip']));
                $previous_diversion_array_for_local['local_speaker_ip']=$local_speacker_ip_local[0];
                $previous_diversion_array_for_local['local_server_ip']= $exabgp_details_per_pop['local_server_ip'];
                $previous_diversion_array_for_local['local_asn']= $exabgp_details_per_pop['local_asn'];
                $previous_diversion_array_for_local['peer_asn']= $exabgp_details_per_pop['peer_asn'];
                $previous_diversion_array_for_local['target'] = "100:".$previous_config_from_database['target'];
                $previous_diversion_array_for_local['md5_keystring'] = $exabgp_details_per_pop['local_speaker_md5_key_string'];
                $previous_diversion_all_config_for_local[]=$previous_diversion_array_for_local;

            }


$result['local_server']['gui']=$add_diversion_all_config_for_local_single_pop;
$result['local_server']['prev']=$previous_diversion_all_config_for_local;

return $result;


}
 ?>

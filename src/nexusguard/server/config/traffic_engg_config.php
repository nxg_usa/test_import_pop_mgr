<?php
include_once("/opt/observium/includes/defaults.inc.php");
include_once("/opt/observium/config.php");
include_once("/opt/observium/includes/definitions.inc.php");
include_once("/opt/observium/nexusguard/common/functions.inc.php");
include_once '/opt/observium/nexusguard/db/db_isp_functions.php';
include_once '/opt/observium/nexusguard/db/db_pop_functions.php';
include_once '/opt/observium/nexusguard/resource/ip_mgmt_config.php';

$traffic_log_type = "config";

function get_traffic_engg_config($cust_traffic_engg_data)
{
    global $traffic_log_type;
    log_trace($traffic_log_type, __FILE__, __FUNCTION__, "Begin");

    $config['add_traffic_data'] = array();
    $config['del_traffic_data'] = array();
    $config['add_community_data'] = array();
    $config['del_community_data'] = array();
    $config['add_prefix_data'] = array();
    $config['del_prefix_data'] = array();
    $config['del_isp_data'] = array();
    $config['add_traffic_data'] = array();
    $config['del_isp_name'] = array();
    $config['customer_name'] = $cust_traffic_engg_data['customer_name'];
    $all_gui_isp = array();

    $customer_name = $cust_traffic_engg_data['customer_name'];
    $traffic_engg_data = $cust_traffic_engg_data['inbound_traffic_data'];
    $cust_id = $cust_traffic_engg_data['cust_id'];

    if(isset($traffic_engg_data['inbound_pop']))
        $traffic_engg_data = array($traffic_engg_data);

    $db_inbound_rule_data = dbFetchRows("select nxg_customer_inbound_rule.*,nxg_pop_details.pop_name as pop_name ,group_concat(nxg_customer_rule_prefix.prefix) as prefix_list, group_concat(nxg_customer_rule_isp.isp_id) as isp_list,group_concat(nxg_customer_rule_community.community_id) as community_list from nxg_customer_inbound_rule left join nxg_customer_rule_prefix on nxg_customer_inbound_rule.rule_id = nxg_customer_rule_prefix.rule_id left join nxg_customer_rule_isp on nxg_customer_rule_isp.rule_id = nxg_customer_inbound_rule.rule_id left join nxg_customer_rule_community on nxg_customer_rule_community.rule_id = nxg_customer_inbound_rule.rule_id left join nxg_pop_details on nxg_customer_inbound_rule.pop_id = nxg_pop_details.id where cust_id= ? group by nxg_customer_inbound_rule.rule_id;",array($cust_id));

    $db_inbound_rule = array();
    
    foreach($db_inbound_rule_data as $row )
    {
        $db_inbound_rule[$row['rule_id']] = $row;
    }
    for($i = 0 ;$i<count($traffic_engg_data);$i++)
    {
        $inbound_pop = $traffic_engg_data[$i]['inbound_pop'];
        $inbound_prefix = $traffic_engg_data[$i]['inbound_prefix'];
        $inbound_isp = $traffic_engg_data[$i]['inbound_isp'];
        $inbound_community = $traffic_engg_data[$i]['inbound_community'];
        $inbound_asn = $traffic_engg_data[$i]['inbound_asn'];
        $inbound_rule_id = $traffic_engg_data[$i]['rule_id'];
        $inbound_action = $traffic_engg_data[$i]['inbound_action'];

        if($inbound_action != "delete" )
            $config['add_traffic_data'][]=gen_traffic_config($inbound_isp,$inbound_community,$inbound_prefix,$inbound_asn,$customer_name); 
    }

    $del_config = array();
    foreach($db_inbound_rule as $inbound_rule_id)
    {
        $del_isp_array = array();
        $del_community_name_array = array();
        $del_prefix_details_array = array();

        $db_isp_array = array_unique(explode(",",$inbound_rule_id['isp_list']));
        $db_comuntiy_array = array_unique(explode(",",$inbound_rule_id['community_list']));
        $db_prefix_array = array_unique(explode(",",$inbound_rule_id['prefix_list']));
        
        foreach($db_isp_array as $isp)
        {
            $del_config[$isp]['isp_id'] = $isp;
            foreach($db_comuntiy_array as $community)
            {
                foreach($db_prefix_array as $prefix)
                {
                    $del_config[$isp]['community_list'][$community]['prefix_list'][] = $prefix;
                }

            }
           /* foreach($db_prefix_array as $prefix)
            {
                $del_config[$isp]['prefix_list'][] = $prefix;
            }*/
            $asn_level = $inbound_rule_id['asn_level'];
            if($asn_level != "none" )
            {
            foreach($db_prefix_array as $prefix)
            {
                $del_config[$isp]['asn_list'][$asn_level]['prefix_list'][] = $prefix;
            }
                }
        }
//        $config['del_traffic_data'][]= gen_traffic_config($db_isp_array,$db_comuntiy_array,$db_prefix_array,$inbound_rule_id['asn_level'],$customer_name) ;

    }

    $config['del_traffic_data'] = gen_del_traffic($del_config,$customer_name);
    $config['customer_name'] = $customer_name;
    return $config;  
}

function gen_del_traffic($del_config,$customer_name)
{
    $del_array = array();
    $community_name_array= array();
    $prefix_details_array = array();
    $asn_exp_list = array();


    foreach($del_config as $key=>$config)
    {

        $array = array();
        $del_community=array();
        $del_asn=array();
        $isp_details = get_isp_details_with_bgp($config['isp_id']);
        $isp_name = $isp_details[0]['name'];
        $array['name']=$isp_name;
        foreach($isp_details as $if_details)
        {
            $array['interface_configuration'][] = array('interface_name'=>$if_details['service_interface'],'unit'=>$if_details['service_interface_unit']);
        }
        $community_list =  $config['community_list'];
        foreach($community_list as $community=>$com_prefix)
        {
            if(!empty($community))
            {
                $prefix_details_array = array();
                $community_details = get_bgp_community($community);
                $community_name = $community_details['community_name'];
                $community_name_array[]['name'] = $community_name;
                $prefix_list = array_unique($com_prefix['prefix_list']);
                foreach($prefix_list as $prefix)
                {
                    $prefix_details_array[]['name'] = $prefix;
                }

                $del_community[] = array('community_name'=>$community_name, 'prefix_list'=>$prefix_details_array);
            }
        }
        foreach($config['asn_list'] as $asn=>$asn_prefix)
        {
            $prefix_details_array = array();
            $prefix_list = array_unique($asn_prefix['prefix_list']);
            foreach($prefix_list as $prefix)
            {
                $prefix_details_array[]['name'] = $prefix;
            }
            $del_asn[] = array('asn_level'=>$asn, 'prefix_list'=>$prefix_details_array);

        }
        $del_array[] = array('isp_details'=>$array,'del_community'=>$del_community,'del_asn'=>$del_asn);
    }
    return $del_array;

}

function gen_traffic_config($inbound_isp,$inbound_community,$inbound_prefix,$asn_level,$customer_name)
{
    $isp_array = array();
    $community_name_array= array();
    $prefix_details_array = array();
    if(!is_array($inbound_isp))
    {
        $inbound_isp = array($inbound_isp);
    }
    foreach($inbound_isp as $isp)
    {
        $isp_details = get_isp_details_with_bgp($isp);
        $isp_name = $isp_details[0]['name'];
        $array = array();
        $array['name']=$isp_name;
        foreach($isp_details as $if_details)
        {
            $array['interface_configuration'][] = array('interface_name'=>$if_details['service_interface'],'unit'=>$if_details['service_interface_unit']);
        }
        $isp_array[] = $array;
    }
    foreach($inbound_community as $community)
    {
        if(!empty($community))
        {
            $community_details = get_bgp_community($community);
            $community_name = $community_details['community_name'];
            $community_name_array[]['name'] = $community_name;
        }
    }
    foreach($inbound_prefix as $prefix)
    {
        //            $prefix_details = get_prefix_details($prefix);
        //$refix_array[]['name'] = $prefix_details['name']
        $prefix_details_array[]['name'] = $prefix;
    }
    
    return array('inboud_isp'=>$isp_array,'community_names'=>$community_name_array,'prefix_list'=>$prefix_details_array,'asn_expansion'=>$asn_level);
}
function get_del_traffic_data($isp_array,$community_array,$prefix_array,$asn_level)
{
}
function get_del_isp_array($all_gui_isp,$cust_id,$pop_id)
{
    $db_del_isp_list = array();
    $del_isp_list = array();
    if(count($all_gui_isp) > 0)
    {
        $db_del_isp_list = dbFetchRows("select  distinct isp_id from nxg_customer_inbound_rule left join nxg_customer_rule_isp on nxg_customer_inbound_rule.rule_id = nxg_customer_rule_isp.rule_id where isp_id not in(?) and cust_id = ? group by isp_id",array($all_gui_isp,$cust_id));
    }
    else
    {
        $db_del_isp_list = dbFetchRows("select distinct isp_id from nxg_customer_inbound_rule left join nxg_customer_rule_isp on nxg_customer_inbound_rule.rule_id = nxg_customer_rule_isp.rule_id where pop_id= ? and cust_id = ? group by isp_id ",array($pop_id,$cust_id));
    }

    foreach($db_del_isp_list as $isp)
    {
        $isp_details = get_isp_details_with_bgp($isp);
        $isp_name = $isp_details[0]['name'];
        $array = array();
        $array['name']=$isp_name;
        foreach($isp_details as $if_details)
        {
            $array['interface_configuration'][] = array('interface_name'=>$if_details['service_interface'],'unit'=>$if_details['service_interface_unit']);
        }
        $del_isp_list[] = $array;
    }
    return $del_isp_list;
}

function get_outbound_engg_config($data)
{
    global $traffic_log_type;
    log_trace($traffic_log_type, __FILE__, __FUNCTION__, "Begin");
    $type = $data['outbound_type'];
    $cust_id = $data['cust_id'];
    $pop_id = $data['pop_id'];
    $config = array();

    $db_out_te = dbFetchRow("select distinct outbound_te from nxg_customer_conn where customer_conn='tunnel' and cust_id = ? and pop_id =? ",array($cust_id,$pop_id));
    if($db_out_te['outbound_te'] != $type)
    {
        if($type != 0)
        {

            $out_bound_vr_ip = dbFetchRow("select ip_address  from nxg_pop_interface_ip_mgmt where type='pop' and isp_id is NULL and conn_id is NULL and pop_id = ? and routing_instance = ? ",array($pop_id,'VR-E-TYPE-'.$type));


            $config['next_hop'] = $out_bound_vr_ip['ip_address'];
        }
        if($db_out_te['outbound_te'] != 0)
        {
            $out_bound_vr_ip = dbFetchRow("select ip_address  from nxg_pop_interface_ip_mgmt where type='pop' and isp_id is NULL and conn_id is NULL and pop_id = ? and routing_instance = ? ",array($pop_id,'VR-E-TYPE-'.$db_out_te['outbound_te']));


            $config['del_next_hop'] = $out_bound_vr_ip['ip_address'];

        }

        $public_ips = array();
        $cust_public_ips = dbFetchRows("select distinct endpoint_ip_remote from nxg_customer_conn where customer_conn='tunnel' and cust_id = ? and pop_id=?", array($cust_id,$pop_id));
        foreach($cust_public_ips as $ip)
        {
            $public_ips[]['ip'] = $ip['endpoint_ip_remote'];
        }
        $config['cust_public_ip']= $public_ips;
    }
    log_trace($traffic_log_type, __FILE__, __FUNCTION__, "End");
    return $config;

}
?>

<?php

include_once("/opt/observium/html/nexusguard/api/common/common.inc.php");
include_once("/opt/observium/includes/defaults.inc.php");
include_once("/opt/observium/config.php");
include_once("/opt/observium/includes/definitions.inc.php");
include_once("/opt/observium/nexusguard/common/functions.inc.php");
include_once '/opt/observium/nexusguard/resource/ip_mgmt_config.php';
include_once('/opt/observium/nexusguard/db/db_add_diversion.php');



function delete_diversion_config($diversion_id,$diversion_type,$pop_id){




$previous_diversion_all_config_for_local = array();
$previous_diversion_all_config_for_global = array();



$previous_all_config_from_database = dbFetchRows('select * from nxg_diversion_details where id!='.$diversion_id.'');

$exabgp_details_per_pop=dbFetchRow('select local_speaker_md5_key_string,global_speaker_md5_key_string,local_speaker_ip,global_speaker_ip,local_server_ip,global_server_ip,local_asn_global,peer_asn_global,local_asn,peer_asn from nxg_exabgp_details where pop_details_id='.$pop_id.'');

foreach($previous_all_config_from_database as $previous_config_from_database)
{
    

        $previous_diversion_array_for_global['diversion_name'] = $previous_config_from_database['name'];
        $previous_diversion_array_for_global['pop_id'] = $previous_config_from_database['pop'];
        $previous_diversion_array_for_global['network_prefix_ip'] = $previous_config_from_database['network_ip'];
        $previous_diversion_array_for_global['network_prefix_subnet_ip'] = $previous_config_from_database['network_prefix'];
        $previous_diversion_array_for_global['source_ip'] = $previous_config_from_database['source_ip'];
        $previous_diversion_array_for_global['source_subnet'] = $previous_config_from_database['source_subnet'];
        $previous_diversion_array_for_global['source_port'] = $previous_config_from_database['source_port'];
        $previous_diversion_array_for_global['protocol'] = $previous_config_from_database['protocol'];
        $previous_diversion_array_for_global['port_no'] = $previous_config_from_database['port'];
	$prev_global_speaker=(explode ('/',$exabgp_details_per_pop['global_speaker_ip']));
        $previous_diversion_array_for_global['global_speaker_ip']= $prev_global_speaker[0];
        $previous_diversion_array_for_global['global_server_ip']= $exabgp_details_per_pop['global_server_ip'];
        $previous_diversion_array_for_global['local_asn_global']= $exabgp_details_per_pop['local_asn_global'];
        $previous_diversion_array_for_global['peer_asn_global']= $exabgp_details_per_pop['peer_asn_global'];
        $previous_diversion_array_for_global['target'] = "100:".$previous_config_from_database['target'];
        $previous_diversion_array_for_global['md5_keystring'] = $exabgp_details_per_pop['global_speaker_md5_key_string'];
        $previous_diversion_all_config_for_global[]=$previous_diversion_array_for_global;

}


$previous_single_config_from_database = dbFetchRows('select * from nxg_diversion_details where pop='.$pop_id.' and id!='.$diversion_id.'');


if ($previous_single_config_from_database != NULL )
{
foreach($previous_single_config_from_database as $previous_single_config_from_database)
{
        $previous_diversion_array_for_local['diversion_name'] = $previous_single_config_from_database['name'];
        $previous_diversion_array_for_local['pop_id'] = $previous_single_config_from_database['pop'];
        $previous_diversion_array_for_local['network_prefix_ip'] = $previous_single_config_from_database['network_ip'];
        $previous_diversion_array_for_local['network_prefix_subnet_ip'] = $previous_single_config_from_database['network_prefix'];
        $previous_diversion_array_for_local['source_ip'] = $previous_single_config_from_database['source_ip'];
        $previous_diversion_array_for_local['source_subnet'] = $previous_single_config_from_database['source_subnet'];
        $previous_diversion_array_for_local['source_port'] = $previous_single_config_from_database['source_port'];
        $previous_diversion_array_for_local['protocol'] = $previous_single_config_from_database['protocol'];
        $previous_diversion_array_for_local['port_no'] = $previous_single_config_from_database['port'];
    	$prev_local_speaker_ip=(explode ('/',$exabgp_details_per_pop['local_speaker_ip']));
        $previous_diversion_array_for_local['local_speaker_ip']= $prev_local_speaker_ip[0];
        $previous_diversion_array_for_local['local_server_ip']= $exabgp_details_per_pop['local_server_ip'];
        $previous_diversion_array_for_local['local_asn']= $exabgp_details_per_pop['local_asn'];
        $previous_diversion_array_for_local['peer_asn']= $exabgp_details_per_pop['peer_asn'];
        $previous_diversion_array_for_local['target'] = "100:".$previous_single_config_from_database['target'];
        $previous_diversion_array_for_local['md5_keystring'] = $exabgp_details_per_pop['local_speaker_md5_key_string'];
        $previous_diversion_all_config_for_local[]=$previous_diversion_array_for_local;
}
}

$result=array();
$result['local_server']['prev']=$previous_diversion_all_config_for_local;
$result['global_server']['prev']=$previous_diversion_all_config_for_global;
return $result;
}

 ?>

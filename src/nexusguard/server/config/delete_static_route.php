<?php
include_once('/opt/observium/html/nexusguard/views/includes/common_includes.php');
include_once("/opt/observium/config.php");
include_once("/opt/observium/includes/definitions.inc.php");
include_once("/opt/observium/nexusguard/common/functions.inc.php");
include_once '/opt/observium/nexusguard/db/db_isp_functions.php';
include_once '/opt/observium/nexusguard/db/db_pop_functions.php';
include_once '/opt/observium/nexusguard/resource/ip_mgmt_config.php';


function generate_delete_static_config($diversion_id,$pop_id)
{

$route_name = dbFetchRow("select route_name from nxg_static_route where id = ?",array($diversion_id));
$all_isp_names = dbFetchRows("select name from nxg_isp_details where pop_details_id= ?",array($pop_id));


$manual_diversion_final_config_for_isp=array();

foreach($all_isp_names as $parameter)
{
$manual_diversion_array['isp_name'] = $parameter['name'];
$manual_diversion_final_config_for_isp[]=$manual_diversion_array;
}

$delete_route_name = array();
$delete_route_name['route_name'] =$route_name;
$delete_route_name['isp_names'] = $manual_diversion_final_config_for_isp;

return $delete_route_name;

}
?>


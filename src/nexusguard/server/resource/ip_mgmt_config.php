<?php
include_once '/opt/observium/nexusguard/db/db_ip_mgmt_functions.php';
include_once '/opt/observium/nexusguard/db/db_pop_functions.php';
include_once("/opt/observium/nexusguard/var/popmgr_var.php");

$logtype="config";
function increment_ip_by_one($ip_address)
{
    $long = ip2long($ip_address);
    $long += 1;
    $ip_address = long2ip($long);
    return $ip_address;
}


function get_ip_address_by_subnet($pop_id_id,$interface,$subnet,$type,$subtype,$pop_name)
{
    global $logtype;
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    log_debug($logtype, __FILE__, __FUNCTION__, "Get unit for pop_id=".$pop_id_id ." and interface=".$interface);

    $ips = array();

    $ip_address = NULL;
    if($pop_id_id==NULL)
    {
        $ip_address = get_base_ip_address($pop_name,$type,$subtype);
    }
    else
    {
        $db_ip_ip = get_ip_from_db($pop_id_id,$interface,$type,$subtype);
        if(empty($db_ip_ip))
        {
            $pop_details =  get_sinlge_pop_detail($pop_id_id);
            $ip_address = get_base_ip_address($pop_details["pop_name"],$type,$subtype);
        }
        else
        {
            $ip_address = increment_ip_by_one($db_ip_ip);
            log_debug($logtype, __FILE__, __FUNCTION__, "Got database ip =".$db_ip_ip );
        }
    }

    if($subnet=="31")
    {    
        if((ip2long($ip_address) % 2) == 0)
        {
            $ips[] = $ip_address;
            $ips[] = increment_ip_by_one($ip_address);
        }
        else
        {
            $ip_address = increment_ip_by_one($ip_address);
            $ips[] = $ip_address;
            $ips[] = increment_ip_by_one($ip_address);
        }
    }

    if($subnet=="32")
    {
        $ips[] = $ip_address;
    }
    log_debug($logtype, __FILE__, __FUNCTION__, "Returning  ips =".json_encode($ips) );
    log_trace($logtype, __FILE__, __FUNCTION__, "End");
    return $ips;
}


function get_unit_by_type($pop_id,$interface,$type)
{
    global $logtype;
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    log_debug($logtype, __FILE__, __FUNCTION__, "Get unit for pop_id=".$pop_id ." and interface=".$interface);

    if($pop_id == NULL)
    {
        $unit = get_starting_unit($type);
    }
    else
    {
        $db_unit = get_unit_for_interface($pop_id,$interface,$type);
        if(empty($db_unit))
        {
            $unit = get_starting_unit($type);   
        }else
        {
            $unit = $db_unit+ 1;
        }

        log_debug($logtype, __FILE__, __FUNCTION__, "Got database unit =".$db_unit ." returning unit =".$unit." interface=".$interface);
    }
    log_debug($logtype, __FILE__, __FUNCTION__, "Returning unit =".$unit);

    log_trace($logtype, __FILE__, __FUNCTION__, "End");
    return $unit;
}

function get_starting_unit($type)
{
    global $logtype;
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");
   
    global $var_ip_pool;

    log_debug($logtype, __FILE__, __FUNCTION__, "Got type=".$type);
    
    $unit = $var_ip_pool[$type]['start_unit'];

    log_debug($logtype, __FILE__, __FUNCTION__, "Got unit=".$unit);
    log_trace($logtype, __FILE__, __FUNCTION__, "End");
    return $unit;

}

function get_interface_name($type,$subtype)
{
    global $logtype;
     global $var_ip_pool;
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    $ifname = $var_ip_pool[$type][$subtype]["name"];

    log_debug($logtype, __FILE__, __FUNCTION__, "Got ifname=".$ifname);
    log_trace($logtype, __FILE__, __FUNCTION__, "End");
    return $ifname;


}

function get_starting_ip_address($pop_id, $interface)
{

    global $logtype;
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");
    
    $db_ip = get_starting_ip_for_interface($interface);
    if(empty($db_ip))
    {
        $db_ip = '10.2.1.1';
    }
    
    log_trace($logtype, __FILE__, __FUNCTION__, "End");
    return $db_ip;
}

function get_next_available_gige_if($pop_id)
{
    global $logtype;
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    log_debug($logtype, __FILE__, __FUNCTION__, "PoP Id="+$pop_id);

    $max_if = get_maxed_used_if($pop_id, "ge-");
    if($max_if == null)
        return "ge-0/0/0";
    

    log_debug($logtype, __FILE__, __FUNCTION__, "PoP Id="+$pop_id);


    log_trace($logtype, __FILE__, __FUNCTION__, "End");
}


function get_next_available_gr_if($pop_id)
{
    global $logtype;
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    log_debug($logtype, __FILE__, __FUNCTION__, "PoP Id="+$pop_id);

    $max_if = get_maxed_used_if($pop_id, "gr-");
    if($max_if == null)
        return "gr-0/0/0";

    log_debug($logtype, __FILE__, __FUNCTION__, "PoP Id="+$pop_id);


    log_trace($logtype, __FILE__, __FUNCTION__, "End");
}


function increment_if($if)
{
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    $ifarray= explode ("/",$if);
    $port = $ifarray[2];
    $port = $port +1; // TODO handle roll over
    
    $newif =  $ifarray[0] ."/".  $ifarray[1] ."/".  $port;

    log_debug($logtype, __FILE__, __FUNCTION__, "input if =".$if." new if =".$newif);

    log_trace($logtype, __FILE__, __FUNCTION__, "End");

}

function get_base_ip_address($pop_name,$type,$interface)
{
    global $logtype;
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    global $var_ip_pool;
    global $var_ip_pop_prefix;

    $ip_address = "";  

 
    log_debug($logtype, __FILE__, __FUNCTION__, "Got type=".$type." , interface=".$interface);

    if(isset($var_ip_pool[$pop_name][$type][$interface]["start_ip"]))
        $ip_address = $var_ip_pool[$pop_name][$type][$interface]["start_ip"];
    else
        {
            $ip_prfix = $var_ip_pop_prefix[$pop_name];
            if(!isset($ip_prfix))
                $ip_prfix = $var_ip_pop_prefix["default"];

            $ip_address = $var_ip_pool[$type][$interface]["start_ip"];
            $ip_address = str_replace("x.x",$ip_prfix,$ip_address);

        }

    log_debug($logtype, __FILE__, __FUNCTION__, "Got starting ip address=".$ip_address);
    log_trace($logtype, __FILE__, __FUNCTION__, "End");
    return $ip_address;
}

function get_next_ip_addresses($ip_address,$subnet)
{
    global $logtype;
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin=".$ip_address);
    
    $ips = array();
    
    $ip_address = increment_ip_by_one($ip_address);
    if($subnet=="31")
    {
        if(ip2long(($ip_address % 2)) == 0)
        {
            $ips[] = $ip_address;
            $ips[] = increment_ip_by_one($ip_address);
        }
        else
        {
            $ip_address = increment_ip_by_one($ip_address);
            $ips[] = $ip_address;
            $ips[] = increment_ip_by_one($ip_address);
        }
    }

    if($subnet=="32")
    {
        $ips['first_ip_address'] = $ip_address;
    }
    log_debug($logtype, __FILE__, __FUNCTION__, "Returning  ips =".json_encode($ips) );

    log_trace($logtype, __FILE__, __FUNCTION__, "End");
    return $ips;
}
?>

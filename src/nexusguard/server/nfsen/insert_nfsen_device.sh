#/bin/bash

source /opt/observium/nexusguard/ansible/popmgr_env.sh

device_hostname=$1
device_id=$2
device_ip=$3
port=$4



#device_name="device_$device_id" # better to generate the nfsen device_name in a single function in php, so it can be accessed from any php function
device_name="$device_hostname"

prev_device=`grep -i $device_name $NFSEN_CONF_DIR | awk -F '=>' '{print $1}' | tr -d "'"`
prev_port=`grep $device_name $NFSEN_CONF_DIR | awk -F '=>' '{print $3}' | awk -F ',' '{print $1}' | tr -d "'"` 

prev_port_space=`echo $prev_port | sed -e "s/ //g"`

prev_ip=`grep $device_name $NFSEN_CONF_DIR | awk -F '=>' '{print $4}' | awk -F ',' '{print $1}'| tr -d "'"`

prev_ip_space=`echo $prev_ip | sed -e "s/ //g"`

if [ "$device_name" = $prev_device ]
then
    string="'$device_name' => { 'port' => '$port','IP' => '$device_ip' ,'col' => '#CC0099', 'type' => 'netflow' },"
    sed -i "s/.*$device_name.*//g" $NFSEN_CONF_DIR
    cd /usr/local/nfsen/bin
    echo -e "yes"  | sudo ./nfsen reconfig
    sudo ./nfsen start
    sed -i "/%source/a$string" $NFSEN_CONF_DIR
    cd /usr/local/nfsen/bin
    echo -e "yes"  | sudo ./nfsen reconfig
    sudo ./nfsen start
    exit 0

fi


if [ "$device_name" != "$prev_device" -a "$device_ip" != "$prev_ip_space" -a "$port" != "$prev_port_space" ]
then
    
    string="'$device_name' => { 'port' => '$port','IP' => '$device_ip' ,'col' => '#CC0099', 'type' => 'netflow' },"
    sudo sed -i "/%source/a$string" $NFSEN_CONF_DIR
    insert_string=`grep -i $device_name $NFSEN_CONF_DIR | awk -F '=>' '{print $1}' | tr -d "'"`
    if [ x$insert_string = "x" ]
    then
            exit 1
    fi
fi

cd /usr/local/nfsen/bin
echo -e "yes"  | sudo ./nfsen reconfig
sudo ./nfsen start


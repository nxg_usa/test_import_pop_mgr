<?php

include_once("/opt/observium/includes/defaults.inc.php");
include_once("/opt/observium/config.php");
include_once("/opt/observium/includes/definitions.inc.php");
include_once("/opt/observium/nexusguard/logger/logger.php");

/*
 * duplicated from db_pop_functions.php, so we don't have to (circularly) include all the db_pop_functions.php includes . . .
 */
function get_device_by_name($device_name)
{
    $logtype="DB";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    $device_details = dbFetchRow("select * from devices where UPPER(hostname) = UPPER(?) ",array($device_name));

    log_trace($logtype, __FILE__, __FUNCTION__, "End");

    return $device_details;
}

function generate_nfsen_devicename($device_hostname, $device_id=NULL) {

  if(!is_null($device_id)) {
    $nfsen_devicename = 'device_' . "$device_id";
    return $nfsen_devicename;
  }

  $deviceDetails = get_device_by_name($device_hostname);
  if(!is_null($deviceDetails)) {
    $deviceId = $deviceDetails['device_id'];
    $nfsen_devicename = 'device_' . "$deviceId";
    return $nfsen_devicename;
  }
  else {
    return NULL;
  }
}

/*
 * main section (for testing)
 */

/*
echo 'ansible-mx5, ' . generate_nfsen_devicename('blah', 26) . ' ';
echo 'ansible-mx5, ' . generate_nfsen_devicename('ansible-mx5') . ' ';
/* */

?>

#!/bin/bash

source /opt/observium/nexusguard/ansible/popmgr_env.sh

NAME=$1
IP=$2

sudo cp $HOSTS_FILE_LOCATION /tmp/

if [ "$NAME" != "" -a "$IP" != "" ];
then
    if grep -w "$IP"  $HOSTS_FILE_LOCATION
    then
        sed -i "s/$IP.*//g" $HOSTS_FILE_LOCATION
    fi
     echo "$IP $NAME" >> $HOSTS_FILE_LOCATION
else
    
    exit 1

fi



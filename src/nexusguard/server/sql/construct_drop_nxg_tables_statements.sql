 SELECT CONCAT('DROP TABLE `',t.table_schema,'`.`',t.table_name,'`;') AS stmt
   FROM information_schema.tables t
  WHERE t.table_schema = 'observium'
    AND t.table_name LIKE 'nxg\_%' ESCAPE '\\'
  ORDER BY t.table_name;

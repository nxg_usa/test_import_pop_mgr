-- MySQL dump 10.13  Distrib 5.5.44, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: observium
-- ------------------------------------------------------
-- Server version	5.5.44-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `nxg_app_service_prefix_list`
--

DROP TABLE IF EXISTS `nxg_app_service_prefix_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nxg_app_service_prefix_list` (
  `pop_id` int(11) DEFAULT NULL,
  `prefix` varchar(256) DEFAULT NULL,
  KEY `pop_id` (`pop_id`),
  CONSTRAINT `nxg_app_service_prefix_list_ibfk_1` FOREIGN KEY (`pop_id`) REFERENCES `nxg_pop_details` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nxg_auditlog`
--

DROP TABLE IF EXISTS `nxg_auditlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nxg_auditlog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pop_id` int(11) DEFAULT NULL,
  `audit_time` datetime DEFAULT NULL,
  `user_name` varchar(50) DEFAULT NULL,
  `module_name` varchar(20) DEFAULT NULL,
  `commit_comment` varchar(256) DEFAULT NULL,
  `commit_log` text,
  `commit_status` varchar(20) DEFAULT NULL,
  `guid` char(32) DEFAULT NULL,
  `syslog_commit` varchar(256) DEFAULT NULL,
  `commit_match` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pop_id` (`pop_id`),
  CONSTRAINT `nxg_auditlog_ibfk_1` FOREIGN KEY (`pop_id`) REFERENCES `nxg_pop_details` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nxg_auth_details`
--

DROP TABLE IF EXISTS `nxg_auth_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nxg_auth_details` (
  `id` int(11) NOT NULL DEFAULT '0',
  `username` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nxg_bgp_details`
--

DROP TABLE IF EXISTS `nxg_bgp_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nxg_bgp_details` (
  `bgp_id` int(11) NOT NULL DEFAULT '0',
  `local_ip` varchar(32) DEFAULT NULL,
  `subnet_mask` int(11) DEFAULT NULL,
  `local_as` int(11) DEFAULT NULL,
  `neighbor_ip` varchar(32) DEFAULT NULL,
  `subnet_mask_neighbor` int(11) DEFAULT NULL,
  `peer_as` int(11) DEFAULT NULL,
  `isp_id` int(11) DEFAULT NULL,
  `service_interface` varchar(64) DEFAULT NULL,
  `service_interface_vlan` int(11) DEFAULT NULL,
  `service_interface_unit` int(11) DEFAULT NULL,
  `md5_passphrase` varchar(128) DEFAULT NULL,
  `restart_timer` int(11) DEFAULT NULL,
  `sub_interface_name` varchar(64) DEFAULT NULL,
  `bgp_group` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bgp_stale_routes_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_IspBgp` (`isp_id`),
  CONSTRAINT `fk_IspBgp` FOREIGN KEY (`isp_id`) REFERENCES `nxg_isp_details` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nxg_customer`
--

DROP TABLE IF EXISTS `nxg_customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nxg_customer` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `location` varchar(256) DEFAULT NULL,
  `email` varchar(256) DEFAULT NULL,
  `phone` varchar(256) DEFAULT NULL,
  `ossid` varchar(256) DEFAULT NULL,
  `description` varchar(1024) DEFAULT NULL,
  `notes` text,
  `offline` int(11) DEFAULT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nxg_customer_app_service`
--

DROP TABLE IF EXISTS `nxg_customer_app_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nxg_customer_app_service` (
  `destination_net` varchar(256) DEFAULT NULL,
  `destination_net_mask` varchar(256) DEFAULT NULL,
  `service_name` varchar(256) DEFAULT NULL,
  `service_protocol` varchar(64) DEFAULT NULL,
  `port` int(11) NOT NULL,
  `cust_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nxg_customer_blocked_service`
--

DROP TABLE IF EXISTS `nxg_customer_blocked_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nxg_customer_blocked_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_name` varchar(256) DEFAULT NULL,
  `service_protocol` varchar(64) DEFAULT NULL,
  `port` int(11) NOT NULL,
  `cust_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nxg_customer_conn`
--

DROP TABLE IF EXISTS `nxg_customer_conn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nxg_customer_conn` (
  `conn_id` int(11) NOT NULL AUTO_INCREMENT,
  `cust_id` int(11) DEFAULT NULL,
  `conn_loc` varchar(256) DEFAULT NULL,
  `customer_conn` varchar(256) DEFAULT NULL,
  `conn_if` varchar(256) DEFAULT NULL,
  `endpoint_ip_local` varchar(32) DEFAULT NULL,
  `endpoint_ip_remote` varchar(32) DEFAULT NULL,
  `inside_ip_local` varchar(32) DEFAULT NULL,
  `inside_ip_remote` varchar(32) DEFAULT NULL,
  `asn_local` int(11) DEFAULT NULL,
  `asn_remote` int(11) DEFAULT NULL,
  `conn_if_vlan` int(11) DEFAULT NULL,
  `conn_if_unit` int(11) DEFAULT NULL,
  `pop_id` int(11) DEFAULT NULL,
  `outbound_te` int(11) DEFAULT '0',
  PRIMARY KEY (`conn_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nxg_customer_inbound_rule`
--

DROP TABLE IF EXISTS `nxg_customer_inbound_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nxg_customer_inbound_rule` (
  `rule_id` int(11) NOT NULL AUTO_INCREMENT,
  `cust_id` int(11) DEFAULT NULL,
  `pop_id` int(11) DEFAULT NULL,
  `asn_level` varchar(8) DEFAULT NULL,
  `prefix_object_id` int(11) DEFAULT NULL,
  `isp_object_id` int(11) DEFAULT NULL,
  `community_object_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`rule_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nxg_customer_inbound_traffic`
--

DROP TABLE IF EXISTS `nxg_customer_inbound_traffic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nxg_customer_inbound_traffic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pop_id` int(11) DEFAULT NULL,
  `cust_id` int(11) DEFAULT NULL,
  `prefix_id` int(11) DEFAULT NULL,
  `isp_id` int(11) DEFAULT NULL,
  `isp_asn_level` int(11) DEFAULT NULL,
  `community_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nxg_customer_net_service`
--

DROP TABLE IF EXISTS `nxg_customer_net_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nxg_customer_net_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `net_srv_prefix` varchar(256) DEFAULT NULL,
  `cust_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nxg_customer_rule_community`
--

DROP TABLE IF EXISTS `nxg_customer_rule_community`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nxg_customer_rule_community` (
  `rule_id` int(11) DEFAULT NULL,
  `community_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nxg_customer_rule_isp`
--

DROP TABLE IF EXISTS `nxg_customer_rule_isp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nxg_customer_rule_isp` (
  `rule_id` int(11) DEFAULT NULL,
  `isp_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nxg_customer_rule_prefix`
--

DROP TABLE IF EXISTS `nxg_customer_rule_prefix`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nxg_customer_rule_prefix` (
  `rule_id` int(11) DEFAULT NULL,
  `prefix` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nxg_customers`
--

DROP TABLE IF EXISTS `nxg_customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nxg_customers` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `location` varchar(256) DEFAULT NULL,
  `email` varchar(256) DEFAULT NULL,
  `phone` varchar(256) DEFAULT NULL,
  `ossid` int(11) NOT NULL,
  `services` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nxg_diversion_details`
--

DROP TABLE IF EXISTS `nxg_diversion_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nxg_diversion_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `add_date` datetime DEFAULT NULL,
  `name` varchar(256) DEFAULT NULL,
  `diversion_type` varchar(64) DEFAULT NULL,
  `pop` int(11) DEFAULT NULL,
  `network_ip` varchar(32) DEFAULT NULL,
  `network_prefix` varchar(32) DEFAULT NULL,
  `protocol` varchar(32) DEFAULT NULL,
  `port` int(11) DEFAULT NULL,
  `comment` varchar(256) DEFAULT NULL,
  `admin_name` varchar(64) DEFAULT NULL,
  `source_ip` varchar(32) DEFAULT NULL,
  `source_subnet` varchar(32) DEFAULT NULL,
  `source_port` int(11) DEFAULT NULL,
  `md5_keystring` varchar(256) DEFAULT NULL,
  `target` varchar(30) DEFAULT NULL,
  `custom_diversion_name` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nxg_exabgp_details`
--

DROP TABLE IF EXISTS `nxg_exabgp_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nxg_exabgp_details` (
  `pop_details_id` int(11) DEFAULT NULL,
  `local_speaker_interface` varchar(32) DEFAULT NULL,
  `local_vlan` int(11) DEFAULT NULL,
  `local_speaker_ip` varchar(32) DEFAULT NULL,
  `local_asn` int(11) DEFAULT NULL,
  `peer_asn` int(11) DEFAULT NULL,
  `restart_time_local` int(11) DEFAULT NULL,
  `stale_routes_time_local` int(11) DEFAULT NULL,
  `global_speaker_interface` varchar(32) DEFAULT NULL,
  `global_vlan` int(11) DEFAULT NULL,
  `global_speaker_ip` varchar(32) DEFAULT NULL,
  `local_asn_global` int(11) DEFAULT NULL,
  `peer_asn_global` int(11) DEFAULT NULL,
  `restart_time_global` int(11) DEFAULT NULL,
  `stale_routes_time_global` int(11) DEFAULT NULL,
  `local_server_ip` varchar(32) DEFAULT NULL,
  `global_server_ip` varchar(32) DEFAULT NULL,
  `local_speaker_md5_key_string` varchar(32) DEFAULT NULL,
  `global_speaker_md5_key_string` varchar(32) DEFAULT NULL,
  `bgp_group_name_local` varchar(32) DEFAULT NULL,
  `bgp_group_name_global` varchar(32) DEFAULT NULL,
  KEY `pop_details_id` (`pop_details_id`),
  CONSTRAINT `nxg_exabgp_details_ibfk_1` FOREIGN KEY (`pop_details_id`) REFERENCES `nxg_pop_details` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nxg_isp_ae_members`
--

DROP TABLE IF EXISTS `nxg_isp_ae_members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nxg_isp_ae_members` (
  `isp_id` int(11) DEFAULT NULL,
  `ae_interface_name` varchar(64) DEFAULT NULL,
  `member_interface_name` varchar(64) DEFAULT NULL,
  `bgp_group` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nxg_isp_bgp_community`
--

DROP TABLE IF EXISTS `nxg_isp_bgp_community`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nxg_isp_bgp_community` (
  `community_name` varchar(128) DEFAULT NULL,
  `community_member` varchar(128) DEFAULT NULL,
  `isp_id` int(11) DEFAULT NULL,
  `community_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`community_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nxg_isp_cost_details`
--

DROP TABLE IF EXISTS `nxg_isp_cost_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nxg_isp_cost_details` (
  `isp_id` int(11) DEFAULT NULL,
  `committed_information_rate` bigint(11) DEFAULT NULL,
  `committed_information_cost` decimal(13,4) DEFAULT NULL,
  `data_input` int(11) DEFAULT NULL,
  `data_output` int(11) DEFAULT NULL,
  `total_data_transfered_cost` decimal(13,4) DEFAULT NULL,
  `fixed_cost` decimal(13,4) DEFAULT NULL,
  KEY `fk_IspCost` (`isp_id`),
  CONSTRAINT `fk_IspCost` FOREIGN KEY (`isp_id`) REFERENCES `nxg_isp_details` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nxg_isp_details`
--

DROP TABLE IF EXISTS `nxg_isp_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nxg_isp_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `description` longtext,
  `pop_details_id` int(11) DEFAULT NULL,
  `offnet_interface` varchar(64) DEFAULT NULL,
  `offnet_interface_vlan` varchar(64) DEFAULT NULL,
  `billing_cycle` int(11) DEFAULT NULL,
  `billing_start_date` int(11) DEFAULT NULL,
  `billing_model` varchar(32) DEFAULT NULL,
  `sub_interface_name` varchar(64) DEFAULT NULL,
  `offnet_interface_unit` int(11) DEFAULT NULL,
  `offline` int(11) DEFAULT NULL,
  `notes` text,
  `billing_domain` varchar(64) DEFAULT NULL,
  `isp_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_PopIsp` (`pop_details_id`),
  CONSTRAINT `fk_PopIsp` FOREIGN KEY (`pop_details_id`) REFERENCES `nxg_pop_details` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nxg_pl_agg_prefix_list`
--

DROP TABLE IF EXISTS `nxg_pl_agg_prefix_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nxg_pl_agg_prefix_list` (
  `pop_id` int(11) DEFAULT NULL,
  `prefix` varchar(512) DEFAULT NULL,
  KEY `pop_id` (`pop_id`),
  CONSTRAINT `nxg_pl_agg_prefix_list_ibfk_1` FOREIGN KEY (`pop_id`) REFERENCES `nxg_pop_details` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nxg_pop_ae_members`
--

DROP TABLE IF EXISTS `nxg_pop_ae_members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nxg_pop_ae_members` (
  `pop_id` int(11) DEFAULT NULL,
  `ae_interface_name` varchar(64) DEFAULT NULL,
  `member_interface_name` varchar(64) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `last_update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `pop_id` (`pop_id`),
  CONSTRAINT `nxg_pop_ae_members_ibfk_1` FOREIGN KEY (`pop_id`) REFERENCES `nxg_pop_details` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nxg_pop_details`
--

DROP TABLE IF EXISTS `nxg_pop_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nxg_pop_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pop_edge_router_device_id` int(11) DEFAULT NULL,
  `pop_edge_router_name` varchar(256) DEFAULT NULL,
  `pop_edge_router_username` varchar(256) DEFAULT NULL,
  `pop_edge_router_password` varchar(256) DEFAULT NULL,
  `pop_name` varchar(256) DEFAULT NULL,
  `oss_device_id` int(11) DEFAULT NULL,
  `pop_notes` varchar(512) DEFAULT NULL,
  `pop_description` varchar(256) DEFAULT NULL,
  `username` varchar(256) DEFAULT NULL,
  `password` varchar(256) DEFAULT NULL,
  `device_enrolment_status` int(11) DEFAULT '0',
  `loopback_ip` varchar(32) DEFAULT NULL,
  `netflow_server_ip` varchar(32) DEFAULT NULL,
  `netflow_source_address` varchar(32) DEFAULT NULL,
  `netflow_port` int(11) DEFAULT NULL,
  `syslog_host_ip` varchar(32) DEFAULT NULL,
  `flow_mgr_ip` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nxg_pop_interface_ip_mgmt`
--

DROP TABLE IF EXISTS `nxg_pop_interface_ip_mgmt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nxg_pop_interface_ip_mgmt` (
  `pop_id` int(11) DEFAULT NULL,
  `ip_address` varchar(64) DEFAULT NULL,
  `interface_unit` int(11) DEFAULT NULL,
  `target` int(11) DEFAULT NULL,
  `route_distinguisher` varchar(64) DEFAULT NULL,
  `interface_name` varchar(64) DEFAULT NULL,
  `diversion_name` varchar(64) DEFAULT NULL,
  `isp_id` int(56) DEFAULT NULL,
  `routing_instance` varchar(64) DEFAULT NULL,
  `conn_id` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(64) DEFAULT NULL,
  `subtype` varchar(64) DEFAULT NULL,
  `last_update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `pop_id` (`pop_id`),
  CONSTRAINT `nxg_pop_interface_ip_mgmt_ibfk_1` FOREIGN KEY (`pop_id`) REFERENCES `nxg_pop_details` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nxg_pop_interface_ip_mgmt_lock`
--

DROP TABLE IF EXISTS `nxg_pop_interface_ip_mgmt_lock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nxg_pop_interface_ip_mgmt_lock` (
  `state` varchar(100) DEFAULT NULL,
  `last_update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;


--
-- Table structure for table `nxg_pop_mgmt_config`
--

DROP TABLE IF EXISTS `nxg_pop_mgmt_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nxg_pop_mgmt_config` (
  `pop_id` int(11) DEFAULT NULL,
  `mgmt_interface` varchar(64) DEFAULT NULL,
  `mgmt_ip` varchar(64) DEFAULT NULL,
  `mgmt_ip_subnet` int(11) DEFAULT NULL,
  `mgmt_gateway` varchar(64) DEFAULT NULL,
  KEY `fk_PopMgmt` (`pop_id`),
  CONSTRAINT `fk_PopMgmt` FOREIGN KEY (`pop_id`) REFERENCES `nxg_pop_details` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nxg_router_interface_mapping`
--

DROP TABLE IF EXISTS `nxg_router_interface_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nxg_router_interface_mapping` (
  `pop_details_id` int(11) DEFAULT NULL,
  `device_interface_name` varchar(32) DEFAULT NULL,
  `scrubber_type` varchar(32) DEFAULT NULL,
  `scrubber_custom_name` varchar(32) DEFAULT NULL,
  `vlan_name` varchar(32) DEFAULT NULL,
  `vlan_id` int(11) DEFAULT NULL,
  `sub_interface_name` varchar(32) DEFAULT NULL,
  `insertion_interface` varchar(32) DEFAULT NULL,
  `insertion_vlan` int(11) DEFAULT NULL,
  `row_id` int(10) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`row_id`),
  KEY `pop_details_id` (`pop_details_id`),
  CONSTRAINT `fk_PopInterfaceMapping` FOREIGN KEY (`pop_details_id`) REFERENCES `nxg_pop_details` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nxg_starting_unit_ip_address`
--

DROP TABLE IF EXISTS `nxg_starting_unit_ip_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nxg_starting_unit_ip_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `interface_name` varchar(64) DEFAULT NULL,
  `interface_unit` int(11) DEFAULT NULL,
  `ip_address` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nxg_static_route`
--

DROP TABLE IF EXISTS `nxg_static_route`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nxg_static_route` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `add_date` datetime DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL,
  `pop_name` varchar(64) DEFAULT NULL,
  `route_name` varchar(255) DEFAULT NULL,
  `network_prefix` varchar(32) DEFAULT NULL,
  `network_prefix_subnet` varchar(32) DEFAULT NULL,
  `next_hop` varchar(32) DEFAULT NULL,
  `comment` varchar(256) DEFAULT NULL,
  `pop_id` int(11) DEFAULT NULL,
  `diversion_type` varchar(64) DEFAULT NULL,
  `protocol` varchar(32) DEFAULT NULL,
  `admin_name` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nxg_traffic_filter`
--

DROP TABLE IF EXISTS `nxg_traffic_filter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nxg_traffic_filter` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `filter_name` varchar(32) NOT NULL,
  `filter_type` varchar(32) NOT NULL,
  `customer` varchar(50) DEFAULT NULL,
  `destination_prefix` varchar(32) DEFAULT NULL,
  `destination_subnet` varchar(255) NOT NULL,
  `protocol` varchar(32) DEFAULT NULL,
  `port` int(10) DEFAULT NULL,
  `comment` varchar(256) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `user` varchar(32) DEFAULT NULL,
  `pop_id` int(11) DEFAULT NULL,
  `services` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-10-20  8:48:31

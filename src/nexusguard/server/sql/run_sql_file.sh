#!/bin/sh

# $1 is the mysql login name (e.g., observium)
# $2 is the mysql database name (e.g., observium)
# $3 is the file containing sql statements to be executed (e.g., nxg_db_schema.sql)


mysql --verbose -u $1 -p $2 < $3

<?php
/***
This php is used for application logging to file. Logging is done with help of different functions used for logging severity of each message.
We have used standard log4php for logging.
log4php is configured using xml file "LoggerConfig.xml" present in config directory og logger 
Logger configuration includes Log filename for logging, log level, format etc.

*/

// Include log4php file for configuration
include('log4php/Logger.php');

//Configuration file used logger
Logger::configure('/opt/observium/nexusguard/logger/config/LoggerConfig.xml');


function format_message($logType, $log_severity, $filename, $function_name, $message)
{
    $request_id=$_SERVER['UNIQUE_ID'];
    $msg=" - ".$logType." - ".$log_severity." - ".$filename." - ".$function_name." - ".$message. " - [ ".$request_id ." ]";
    return $msg; 
}

/*
Following function will log TRACE(low level) logs and accepts input in following format
1)Log type
2)File Name
3)Function Name
4)Message
*/
function log_trace($logType, $filename, $function_name, $message)
{
    $log_severity="TRACE";
    $logger = Logger::getLogger("nxg");
    $msg=format_message($logType, $log_severity, $filename, $function_name, $message);
    $logger->trace($msg);
}

/*
Following function will log Info logs and accepts input in following format
1)Log type
2)File Name
3)Function Name
4)Message
*/
function log_info($logType, $filename, $function_name, $message)
{
    $log_severity="INFO";
    $logger = Logger::getLogger("nxg");
    $msg=format_message($logType, $log_severity, $filename, $function_name, $message);
    $logger->info($msg);
}

/*
Following function will log DEBUG logs and accepts input in following format
1)Log type
2)File Name
3)Function Name
4)Message
*/
function log_debug($logType, $filename, $function_name, $message)
{
    $log_severity="DEBUG";
    $logger = Logger::getLogger("nxg");
    $msg=format_message($logType, $log_severity, $filename, $function_name, $message);
    $logger->debug($msg);
}

/*
Following function will log WARN logs and accepts input in following format
1)Log type
2)File Name
3)Function Name
4)Message
*/
function log_warn($logType, $filename, $function_name, $message)
{
    $log_severity="WARN";
    $logger = Logger::getLogger("nxg");
    $msg=format_message($logType, $log_severity, $filename, $function_name, $message);
    $logger->warn($msg);
}

/*
Following function will log CRITICAL logs and accepts input in following format
1)Log type
2)File Name
3)Function Name
4)Message
*/
function log_critical($logType, $filename, $function_name, $message)
{
    $log_severity="CRITICAL";
    $logger = Logger::getLogger("nxg");
    $msg=format_message($logType, $log_severity, $filename, $function_name, $message);
    $logger->fatal($msg);
}

/*
Following function will log ERROR logs and accepts input in following format
1)Log type
2)File Name
3)Function Name
4)Message
*/
function log_error($logType, $filename, $function_name, $message)
{
    $log_severity="ERROR";
    $logger = Logger::getLogger("nxg");
    $msg=format_message($logType, $log_severity, $filename, $function_name, $message);
    $logger->error($msg);
}
?>

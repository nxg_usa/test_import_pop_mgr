#!/bin/bash

echo "############################################" >/tmp/show_config.log

host=$1
username=$2
password=$3
action=$4
jsonfile=$5
#ANSIBLE_DIRECTORY="/opt/observium/nexusguard/ansible"
ANSIBLE_DIRECTORY="/tmp/"

#----------------- Remove log and configuration and inventory file
#rm $ANSIBLE_DIRECTORY/POPtemplate.conf
#rm $ANSIBLE_DIRECTORY/commit_check.log
#rm $ANSIBLE_DIRECTORY/changes.log
#rm $ANSIBLE_DIRECTORY/hosts
#rm $ANSIBLE_DIRECTORY/POPtemplate.json
#-----------------


#echo $jsonfile > $ANSIBLE_DIRECTORY/POPtemplate.json

#----------------- Create host file
echo "$host ansible_connection=local ansible_ssh_host=$host ansible_ssh_port=830 ansible_ssh_user=$username ansible_ssh_pass=$password " > $ANSIBLE_DIRECTORY/hosts
#-----------------



#------------------ execute ansible according to action
if [ $action = "commit_check" ]
then
        ansible-playbook -i $ANSIBLE_DIRECTORY/hosts /opt/observium/nexusguard/ansible/POPtemplate.yml  -vvvv --check  --extra-vars "@${ANSIBLE_DIRECTORY}/POPtemplate1.json"

elif [ $action = "commit" ]
then
        ansible-playbook -i $ANSIBLE_DIRECTORY/hosts $ANSIBLE_DIRECTORY/POPtemplate.yml  -vvvv --extra-vars "@POPtemplate.json" 
fi
#------------------


cat /opt/observium/nexusguard/ansible/POPtemplate.conf

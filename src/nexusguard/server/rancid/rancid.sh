#!/bin/bash
RANCID_CONFIG=" /usr/local/rancid"
RANCID_CONF_FILE="/usr/local/rancid/etc/rancid.conf"
echo "#############################3 in script rancid"
RANCID_GRP=`grep -w 'LIST_OF_GROUPS='  $RANCID_CONF_FILE | grep -v '#' | cut -f2 -d= | tr -d '"'`
vendor="juniper"
method="ssh"
rancid_grp="networking"
host=$1
username=$2
password=$3
log_file=$4
autoenable_pwd=$5
var_dir_check="/usr/local/rancid/var"
ROUTER_DB_DIR="/usr/local/rancid/var/$rancid_grp"
grep -w "$host" $RANCID_CONFIG/.cloginrc
status=$?
if [ $status != 0 ];then

    echo -e "\n add method ${host} {${method}}" >>$RANCID_CONFIG/.cloginrc
    echo "add user $host {$username}" >> $RANCID_CONFIG/.cloginrc
    if [ "$autoenable_pwd" == "" ];then
        echo "add password $host {$password}" >> $RANCID_CONFIG/.cloginrc
        echo  "add autoenable $host 1" >> $RANCID_CONFIG/.cloginrc
    
    else
        echo "add password $host {$password} {$autoenable_pwd}" >> $RANCID_CONFIG/.cloginrc

    fi



fi
                    
if [ ! -d $ROUTER_DB_DIR ];then
    echo "/usr/local/rancid/var directory not present running /usr/local/rancid/bin/rancid-cvs ... "
    su -c "/usr/local/rancid/bin/rancid-cvs" rancid 
fi
grep -w "$host" $ROUTER_DB_DIR/router.db
status=$?
if [ $status != 0 ];then
     echo "${host};${vendor};up" >> $ROUTER_DB_DIR/router.db
fi

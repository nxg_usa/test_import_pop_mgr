<?php
//include '/opt/observium/includes/dbFacile.php';
include_once("/opt/observium/includes/defaults.inc.php");
include_once("/opt/observium/config.php");
include_once("/opt/observium/includes/definitions.inc.php");

include($config['install_dir'] . "/includes/functions.inc.php");
include($config['html_dir'] . "/includes/functions.inc.php");



function add_pop_template($data)
{
    $edge_router_ip=$data['edge_mgmt_ip'];
    $mgmt_ip=$data['pop_mgr_ip'];
    $nxgapp_anycast_ip=$data['nxgapp_anycast_ip'];
    $index=dbInsert(array('edge_router_management_ip'=>$edge_router_ip, 'pop_manager_ip'=>$mgmt_ip,'nxg_anycast_ip'=>$nxgapp_anycast_ip),'pop_template');
    $interfaces=$data['intf_to_processor'];
    $i=0;
    $vlan_names=$data['vlan_name'];
    $vlan_numbers=$data['vlan_number'];
    $types=$data['type'];
    $customs_names=$data['custom_name'];
    foreach($interfaces as $interface)
    {
        $vlan_name=$vlan_names[$i];
        $vlan_number=$vlan_numbers[$i];
        $type=$types[$i];
        $custom_name="";
        if($type=="custom")
        {
            $custom_name=$customs_names[$i];
        }
        dbInsert(array('pop_template_id'=>$index, 'device_interface_name'=>$interface, 'scrubber_type'=>$type, 'scrubber_custom_name'=>$custom_name, 'vlan_name'=>$vlan_name, 'vlan_number'=>$vlan_number), 'nxg_router_interface_mapping');
        $i++;
    }
    
}

?>

<?php

	
//include '/opt/observium/includes/dbFacile.php';
include_once("/opt/observium/includes/defaults.inc.php");
include_once("/opt/observium/config.php");
include_once("/opt/observium/includes/definitions.inc.php");
include_once("/opt/observium/nexusguard/logger/logger.php");
include_once("/opt/observium/nexusguard/config/isp_config.php");

//include("/opt/observium/includes/functions.inc.php");
include_once("/opt/observium/html/includes/functions.inc.php");



function add_an_isp($data)
{
    $logtype="DB";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    $name = $data['isp_name'];
    $pop_id = $data['pop_id'];
    $desc = $data['isp_description'];
    $service_interface = $data['service_interface'];
    $service_interface_vlan = $data['service_interface_vlan'];
    $billing_model = $data['billing_model'];
    $billing_cycle = $data['billing_cycle'];
    $starting_date = $data['starting_date'];
    $billing_domain = $data['billing_domain'];
    $notes = $data['notes'];
    $isp_type = strtoupper($data['isp_type']);

    $index = dbInsert(array('name'=>$name,'description'=>$desc,'pop_details_id'=>$pop_id,'billing_cycle'=>$billing_cycle,'billing_start_date'=>$starting_date,'billing_model'=>$billing_model,'billing_domain'=>$billing_domain,'notes'=>$notes,'isp_type'=>$isp_type),'nxg_isp_details');
    $local_ip_list = $data['local_ip'];
    $local_ip_subnet_list = $data['local_ip_subnet'];
    $local_as_list = $data['local_as'];

    $neighbour_ip_list = $data['neighbour_ip'];
    $neighbour_ip_subnet_list = $data['neighbour_ip_subnet'];
    $neighbour_as_list = $data['peer_as'];

    $service_interface_unit = $data['service_interface_unit'];
    $md5_passphrase = $data['md5_passphrase'];
    $restart_timer = $data['restart_timer'];
    $stale_routes_time = $data['stale_routes_time'];
    $lag_interface = $data['lag_interface'];

    if(!is_array($local_ip_list))
    {
        $local_ip_list = array($local_ip_list);
        $local_ip_subnet_list = array($local_ip_subnet_list);
        $local_as_list = array($local_as_list);
        $neighbour_ip_list = array($neighbour_ip_list);
        $neighbour_ip_subnet_list = array($neighbour_ip_subnet_list);
        $neighbour_as_list = array($neighbour_as_list);
        $service_interface = array($service_interface);
        $service_interface_vlan = array($service_interface_vlan);
        $service_interface_unit = array($service_interface_unit);
        $md5_passphrase = array($md5_passphrase);
        $restart_timer = array($restart_timer);
        $stale_routes_time = array($stale_routes_time);
        $lag_interface = array($lag_interface);
    }

    $i=0;
    foreach($local_ip_list as $local_ip)
    {
        $local_ip_subnet =  $local_ip_subnet_list[$i];
        $local_as = $local_as_list[$i];
        $neighbour_ip = $neighbour_ip_list[$i];
        $neighbour_ip_subnet = $neighbour_ip_subnet_list[$i];
        $neighbour_as = $neighbour_as_list[$i];
        $isp_interface = $service_interface[$i];
        $isp_interface_unit = $service_interface_unit[$i];
        $isp_interface_vlan = $service_interface_vlan[$i];
        $bgp_md5_passphrase = $md5_passphrase[$i];
        $bgp_restart_timer = $restart_timer[$i];
        $timeout = $stale_routes_time[$i];
        $member_ports = $lag_interface[$i][0];

        dbInsert(array('local_ip'=>$local_ip,'subnet_mask'=>$local_ip_subnet,'local_as'=>$local_as,'neighbor_ip'=>$neighbour_ip,'subnet_mask_neighbor'=>$neighbour_ip_subnet,'peer_as'=>$neighbour_as,'isp_id'=>$index,'service_interface'=>$isp_interface,'service_interface_vlan'=>$isp_interface_vlan,'service_interface_unit'=>$isp_interface_unit,'md5_passphrase'=>$bgp_md5_passphrase,'restart_timer'=>$bgp_restart_timer,'sub_interface_name'=>$isp_interface.".".$isp_interface_unit,'bgp_group'=>$i,'bgp_stale_routes_time'=>$timeout),'nxg_bgp_details');


        foreach($member_ports as $ports)
        {
            if(!is_array($ports))
            {
                $ports = array($ports);
            }
            foreach($ports as $ae)
            {
                //dbInsert(array('ae_interface_name'=>$isp_interface,'member_interface_name'=>$ae,'pop_id'=>$pop_id),'nxg_pop_ae_members');
                add_ae_member_port($pop_id, $isp_interface,$ae);
            }
        }

        $i++;
    }
    if($billing_model == "committed_information_rate")
    {
        $committed_information_rate_list = $data['slab_rate'];
        $committed_information_cost_list = $data['slab_cost'];

        if(!is_array($committed_information_rate_list))
        {
            $committed_information_rate_list = array($committed_information_rate_list);
            $committed_information_cost_list = array($committed_information_cost_list);
        }

        $i=0;
        foreach($committed_information_rate_list as  $committed_information_rate)
        {
            $committed_information_cost = $committed_information_cost_list[$i];
            dbInsert(array('committed_information_rate'=>$committed_information_rate,'committed_information_cost'=>$committed_information_cost,'isp_id'=>$index),'nxg_isp_cost_details');
            $i++;
        }
    }
    else if($billing_model === "data_transfer")
    {
        $data_input = $data['input_data'];
        $data_output = $data['output_data'];
        $data_transfer_cost = $data['data_transfer_cost'];
        dbInsert(array('isp_id'=>$index,'data_input'=>$data_input,'data_output'=>$data_output,'total_data_transfered_cost'=>$data_transfer_cost),'nxg_isp_cost_details');
    }
    else if($billing_model === "fixed")
    {
        $fixed_cost = $data['fixed_cost'];
        dbInsert(array('isp_id'=>$index,'fixed_cost'=>$fixed_cost),'nxg_isp_cost_details');

    }

    $bgp_community_names = $data['bgp_community_names'][0];

    $i = 0;
    foreach($bgp_community_names as $community)
    {
        $array = array();
        $tmp = explode("~~~",$community);
        $community_name = trim($tmp[0]);
        $community_member = trim($tmp[1]);
        dbInsert(array('community_name'=>$community_name,'community_member'=>$community_member,'isp_id'=>$index),'nxg_isp_bgp_community');
        $i++;
    }

    log_trace($logtype, __FILE__, __FUNCTION__, "End");

    return $index;    
}
/*function add_config_to_db($pop_id, $unit,$interface,$ip,$routing_instance)
{
    $logtype="DB";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    $index = dbInsert(array('pop_id'=>$pop_id,'interface_unit'=>$unit,'interface_name'=>$interface,'ip_address'=>$ip,'routing_instance'=>$routing_instance),'nxg_pop_interface_ip_mgmt');

    log_trace($logtype, __FILE__, __FUNCTION__, "End");

    return $index;
}*/
   
function is_ispname_used($pop_id, $isp_name)
{

    $logtype="DB";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    $result = dbFetchRows("select * from nxg_isp_details where pop_details_id= ? and name = ?",array($pop_id,$isp_name));

    log_trace($logtype, __FILE__, __FUNCTION__, "End");

    return $result;
}    

function get_isp_details($isp_id)
{

    $logtype="DB";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    $result = dbFetchRow("select * from nxg_isp_details where id = ? ",array($isp_id));

    log_trace($logtype, __FILE__, __FUNCTION__, "End");

    return $result;
}

function get_isp_generated_config($isp_id)
{
    $logtype="DB";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    $result = dbFetchRows("select * from nxg_pop_interface_ip_mgmt where isp_id = ? and subtype='ifl'", array($isp_id));

    log_trace($logtype, __FILE__, __FUNCTION__, "End");

    return $result;
}
function delete_isp_db($isp_id)
{
    $logtype="DB";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    dbDelete('nxg_bgp_details', '`isp_id` = ?', array($isp_id));
    dbDelete('nxg_pop_interface_ip_mgmt', '`isp_id` = ?', array($isp_id));
    dbDelete('nxg_isp_cost_details', '`isp_id` = ?', array($isp_id));
//    dbDelete('nxg_pop_ae_members', '`isp_id` = ?', array($isp_id));
    dbDelete('nxg_isp_bgp_community', '`isp_id` = ?', array($isp_id));
    dbDelete('nxg_isp_details', '`id` = ?', array($isp_id));

    log_trace($logtype, __FILE__, __FUNCTION__, "End");
}

/*function update_config($isp_config,$isp_id,$pop_id)
{
    $logtype="DB";

    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");
    $interfaces = $isp_config['interfaces'];

    foreach($interfaces as $interface)
    {
        $name = $interface['interface_name'];
        foreach( $interface['interface_configuration'] as $config)
        {
            $unit = $config['unit'];
            dbUpdate(array('isp_id'=>$isp_id),'nxg_pop_interface_ip_mgmt','interface_name = ? and interface_unit = ? and pop_id =? ', array($name, $unit,$pop_id));
        }
    }

    log_trace($logtype, __FILE__, __FUNCTION__, "End");
}
*/
function get_isp_by_pop($pop_id)
{
    $logtype="DB";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    $result = dbFetchRows("select * from nxg_isp_details where pop_details_id = ?",array($pop_id));
    
    log_trace($logtype, __FILE__, __FUNCTION__, "End");
    
    return $result;
}

function get_isp_details_with_bgp($isp_id)
{
    $logtype="DB";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    $result = dbFetchRows("select * from nxg_isp_details left join nxg_bgp_details on nxg_isp_details.id = nxg_bgp_details.isp_id where nxg_isp_details.id = ?",array($isp_id));

    log_trace($logtype, __FILE__, __FUNCTION__, "End");
    
    return $result;
}
function get_isp_details_with_cost($isp_id)
{
    $logtype="DB";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    $result = dbFetchRows("select * from nxg_isp_details left join nxg_isp_cost_details on nxg_isp_details.id = nxg_isp_cost_details.isp_id where nxg_isp_details.id = ?",array($isp_id));

    log_trace($logtype, __FILE__, __FUNCTION__, "End");

    return $result;
}

function modify_an_isp($data)
{

    $index = $data['isp_id'];
    $pop_id = $data['pop_id'];

    $billing_model = $data['billing_model'];
    $billing_cycle = $data['billing_cycle'];
    $starting_date = $data['starting_date'];
    $billing_domain = $data['billing_domain'];
    $notes = $data['notes'];
    $description = $data['isp_description'];
    
    $service_interface = $data['service_interface'];
    $service_interface_vlan = $data['service_interface_vlan'];

    $local_ip_list = $data['local_ip'];
    $local_ip_subnet_list = $data['local_ip_subnet'];
    $local_as_list = $data['local_as'];

    $neighbour_ip_list = $data['neighbour_ip'];
    $neighbour_ip_subnet_list = $data['neighbour_ip_subnet'];
    $neighbour_as_list = $data['peer_as'];

    $service_interface_unit = $data['service_interface_unit'];
    $md5_passphrase = $data['md5_passphrase'];
    $restart_timer = $data['restart_timer'];
    $bgp_group = $data['bgp_id'];
    $stale_routes_time = $data['stale_routes_time'];
    $lag_interface = $data['lag_interface'];
    $isp_type = strtoupper($data['isp_type']);

//    delete_bgp_and_cost_info($index);
//    delete_ae_interfaces($index);

    dbUpdate(array('billing_model'=>$billing_model,'billing_cycle'=>$billing_cycle,'billing_start_date'=>$starting_date,'billing_domain'=>$billing_domain,'notes'=>$notes,'description'=>$description,'isp_type'=>$isp_type),'nxg_isp_details',"id= ? ",array($index));  

    if(!is_array($local_ip_list))
    {
        $local_ip_list = array($local_ip_list);
        $local_ip_subnet_list = array($local_ip_subnet_list);
        $local_as_list = array($local_as_list);
        $neighbour_ip_list = array($neighbour_ip_list);
        $neighbour_ip_subnet_list = array($neighbour_ip_subnet_list);
        $neighbour_as_list = array($neighbour_as_list);
        $service_interface = array($service_interface);
        $service_interface_vlan = array($service_interface_vlan);
        $service_interface_unit = array($service_interface_unit);
        $md5_passphrase = array($md5_passphrase);
        $restart_timer = array($restart_timer);
        $bgp_group = array($bgp_group);
        $stale_routes_time = array($stale_routes_time);
        $lag_interface = array($lag_interface);

    }

    $isp_db_config = get_isp_details_with_bgp($index);
    $bgp_config = process_bgp_array($isp_db_config);

    $i=0;
    foreach($local_ip_list as $local_ip)
    {
        $bgp_key = $bgp_group[$i];
        $local_ip_subnet =  $local_ip_subnet_list[$i];
        $local_as = $local_as_list[$i];
        $neighbour_ip = $neighbour_ip_list[$i];
        $neighbour_ip_subnet = $neighbour_ip_subnet_list[$i];
        $neighbour_as = $neighbour_as_list[$i];
        $isp_interface = $service_interface[$i];
        $isp_interface_unit = $service_interface_unit[$i];
        $isp_interface_vlan = $service_interface_vlan[$i];
        $bgp_md5_passphrase = $md5_passphrase[$i];
        $bgp_restart_timer = $restart_timer[$i];
        $timeout = $stale_routes_time[$i];
        $member_ports = $lag_interface[$i][0];


        if(array_key_exists($bgp_key,$bgp_config))
        {
            dbUpdate(array('local_ip'=>$local_ip,'subnet_mask'=>$local_ip_subnet,'local_as'=>$local_as,'neighbor_ip'=>$neighbour_ip,'subnet_mask_neighbor'=>$neighbour_ip_subnet,'peer_as'=>$neighbour_as,'service_interface'=>$isp_interface,'service_interface_vlan'=>$isp_interface_vlan,'service_interface_unit'=>$isp_interface_unit,'md5_passphrase'=>$bgp_md5_passphrase,'restart_timer'=>$bgp_restart_timer,'sub_interface_name'=>$isp_interface.".".$isp_interface_unit,'bgp_stale_routes_time'=>$timeout),'nxg_bgp_details','bgp_group=? and isp_id = ?',array($bgp_key,$index));

            //dbDelete('nxg_pop_ae_members','isp_id =? and ae_interface_name =? and row_id=? and type="isp"',array($index,$isp_interface,$bgp_key));
            foreach($member_ports  as $port)
            {
                //dbInsert(array('ae_interface_name'=>$isp_interface,'member_interface_name'=>$port,'isp_id'=>$index,'row_id'=>$bgp_key,'pop_id'=>$pop_id,'type'=>'isp'),'nxg_pop_ae_members');
                add_ae_member_port($pop_id, $isp_interface,$port);
            }
            unset($bgp_config[$bgp_key]);
        }
        else
        {
            $new_bgp_group = get_bgp_group_db($index);
            $new_bgp_group = $new_bgp_group +1 ;
            dbInsert(array('local_ip'=>$local_ip,'subnet_mask'=>$local_ip_subnet,'local_as'=>$local_as,'neighbor_ip'=>$neighbour_ip,'subnet_mask_neighbor'=>$neighbour_ip_subnet,'peer_as'=>$neighbour_as,'isp_id'=>$index,'service_interface'=>$isp_interface,'service_interface_vlan'=>$isp_interface_vlan,'service_interface_unit'=>$isp_interface_unit,'md5_passphrase'=>$bgp_md5_passphrase,'restart_timer'=>$bgp_restart_timer,'sub_interface_name'=>$isp_interface.".".$isp_interface_unit,'bgp_group'=>$new_bgp_group,'bgp_stale_routes_time'=>$timeout),'nxg_bgp_details');
        }
        $i++;
    
        foreach($member_ports  as $port)
        {
             //dbInsert(array('ae_interface_name'=>$isp_interface,'member_interface_name'=>$port,'isp_id'=>$index,'row_id'=>$new_bgp_group,'pop_id'=>$pop_id,'type'=>'isp'),'nxg_pop_ae_members');
                add_ae_member_port($pop_id, $isp_interface,$port);
        }
    }
    dbDelete('nxg_isp_cost_details','isp_id =?',array($index));
    if($billing_model == "committed_information_rate")
    {
        $committed_information_rate_list = $data['slab_rate'];
        $committed_information_cost_list = $data['slab_cost'];

        if(!is_array($committed_information_rate_list))
        {
            $committed_information_rate_list = array($committed_information_rate_list);
            $committed_information_cost_list = array($committed_information_cost_list);
        }

        $i=0;
        foreach($committed_information_rate_list as  $committed_information_rate)
        {
            $committed_information_cost = $committed_information_cost_list[$i];
            dbInsert(array('committed_information_rate'=>$committed_information_rate,'committed_information_cost'=>$committed_information_cost,'isp_id'=>$index),'nxg_isp_cost_details');
            $i++;
        }
    }
    else if($billing_model === "data_transfer")
    {
        $data_input = $data['input_data'];
        $data_output = $data['output_data'];
        $data_transfer_cost = $data['data_transfer_cost'];
        dbInsert(array('isp_id'=>$index,'data_input'=>$data_input,'data_output'=>$data_output,'total_data_transfered_cost'=>$data_transfer_cost),'nxg_isp_cost_details');
    }
    else if($billing_model === "fixed")
    {
        $fixed_cost = $data['fixed_cost'];
        dbInsert(array('isp_id'=>$index,'fixed_cost'=>$fixed_cost),'nxg_isp_cost_details');

    }

    $bgp_community_names = $data['bgp_community_names'][0];

    $db_bgp_communities = get_isp_bgp_communities($index);

    $mod_bgp_community = process_bgp_community($db_bgp_communities);

    $i = 0;
    foreach($bgp_community_names as $community)
    {
        $array = array();
        $tmp = explode("~~~",$community);
        $community_name = trim($tmp[0]);
        $community_member = trim($tmp[1]);
        $id = $tmp[2];
        if(array_key_exists($id, $mod_bgp_community))
        {
            dbUpdate(array('community_name'=>$community_name,'community_member'=>$community_member),'nxg_isp_bgp_community','community_id = ?',array($id));
            unset($mod_bgp_community[$id]);
        }
        else
        {
            dbInsert(array('community_name'=>$community_name,'community_member'=>$community_member,'isp_id'=>$index),'nxg_isp_bgp_community');   
        }
        $i++;
    }

    foreach($mod_bgp_community as $community=>$value)
    {
          log_debug($logtype, __FILE__, __FUNCTION__, "Deleting communiyt id = ".$community);
          dbDelete('nxg_isp_bgp_community','community_id = ?',array($community));
    }

    foreach($bgp_config as $key=>$value)
    {
    //    dbDelete('nxg_pop_ae_members','isp_id =? and ae_interface_name =? and row_id = ? and type="isp"',array($index,$value['service_interface'],$key));
        dbDelete('nxg_bgp_details','isp_id =? and bgp_group = ?',array($index,$key));
    } 
}
function delete_bgp_and_cost_info($isp_id)
{
    $logtype="DB";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    dbDelete('nxg_bgp_details', '`isp_id` = ?', array($isp_id));
    dbDelete('nxg_isp_cost_details', '`isp_id` = ?', array($isp_id));

    log_trace($logtype, __FILE__, __FUNCTION__, "End");
}

function toggle_isp_status($isp_id,$status)
{
    dbUpdate(array('offline'=>$status),'nxg_isp_details','id= ?',array($isp_id));
}
function get_bgp_group_db($isp_id)
{
    $result = dbFetchRow("select max(bgp_group) as bgp_group from nxg_bgp_details where isp_id = ?" ,array($isp_id));
    return $result ['bgp_group'];
}
function get_interface_member_ports($pop_id,$interface,$bgp_grp)
{

    if($bgp_grp == "")
    {
         $result = dbFetchRows("select ae_interface_name,member_interface_name from nxg_pop_ae_members where pop_id = ? and ae_interface_name  =?" ,array($pop_id,$interface));
    }
    else
    {
        $result = dbFetchRows("select ae_interface_name,member_interface_name from nxg_pop_ae_members where pop_id = ? and ae_interface_name  =? " ,array($pop_id,$interface));

    }

    return $result;
}
function get_isp_bgp_communities($isp_id)
{
    $result = dbFetchRows("select * from nxg_isp_bgp_community where isp_id = ?" ,array($isp_id));
    return $result;
}

function delete_ae_interfaces($isp_id)
{
//    dbDelete('nxg_pop_ae_members','`isp_id` = ?', array($isp_id));
}

function is_isp_interface_used($pop_id, $interface_name,$isp_id,$unit)
{
    $logtype="DB";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");


//    $interface_details = dbFetchRows("select * from nxg_pop_details left join nxg_isp_details on nxg_isp_details.pop_details_id = nxg_pop_details.id left join nxg_pop_interface_ip_mgmt on nxg_pop_interface_ip_mgmt.pop_id = nxg_pop_details.id left join nxg_exabgp_details on nxg_exabgp_details.pop_details_id = nxg_pop_details.id left join nxg_bgp_details on nxg_bgp_details.isp_id = nxg_isp_details.id where (nxg_bgp_details.service_interface= ? or nxg_pop_interface_ip_mgmt.interface_name = ? or nxg_exabgp_details.local_speaker_interface = ? or nxg_exabgp_details.global_speaker_interface = ?) and nxg_pop_details.id = ? and nxg_isp_details.id != ? and nxg_bgp_details.service_interface_unit != ?",array($interface_name,$interface_name,$interface_name,$interface_name,$pop_id,$isp_id,$unit));
    $interface_details = dbFetchRows("select service_interface,nxg_isp_details.id,name,pop_details_id from nxg_bgp_details left join nxg_isp_details on nxg_isp_details.id = nxg_bgp_details.isp_id where nxg_isp_details.id !=? and service_interface_unit =? and service_interface='?'",array($isp_id,$unit,$interface_name));

    log_trace($logtype, __FILE__, __FUNCTION__, "End");

    return $interface_details;
}
function get_bgp_community($community_id)
{
    $logtype="DB";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");
    $isp_bgp_community = dbFetchRow("select * from nxg_isp_bgp_community where community_id =?",array($community_id));
    log_trace($logtype, __FILE__, __FUNCTION__, "End");
    return $isp_bgp_community;
}

function get_active_cust_conn($isp_id)
{

    $result = dbFetchRow("select count(*) as cnt from nxg_customer_rule_isp where isp_id = ? ",array($isp_id));

    return $result['cnt'];
}
?>

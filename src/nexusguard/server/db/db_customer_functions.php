<?php



function add_customer($form_data)
{
        $db_data = array();
        $db_data["ossid"] = $form_data["oss_id"];
        $db_data["name"] = $form_data["customer_name"];
        $db_data["email"] = $form_data["customer_email"];
        $db_data["phone"] = $form_data["customer_phone"];
        $db_data["description"] = $form_data["description"];
        $db_data["notes"] = $form_data["notes"];
        
        $index = dbInsert($db_data,"nxg_customer");

        return $index;
}


function add_update_customer_conn($cust_id,$form_data)
{
        $db_data = array();
        $db_data["cust_id"] = $cust_id;

        $connection =  $form_data["connection"];
        $db_data["conn_loc"] = $connection["conn_loc"];
        $db_data["customer_conn"] = $connection["customer_conn"];
        $db_data["conn_if"] = $connection["conn_if"];
        $db_data["conn_if_unit"] = $connection["conn_if_unit"];
        $db_data["conn_if_vlan"] = $connection["conn_if_vlan"];
        $db_data["endpoint_ip_local"] = $connection["endpoint_ip_local"];
        $db_data["endpoint_ip_remote"] = $connection["endpoint_ip_remote"];
        $db_data["inside_ip_local"] = $connection["inside_ip_local"];
        $db_data["inside_ip_remote"] = $connection["inside_ip_remote"];
        $db_data["asn_local"] = $connection["asn_local"];
        $db_data["asn_remote"] = $connection["asn_remote"];
        $db_data["pop_id"] = $connection["pop_id"];

        $conn_id = $connection["conn_id"];
        if(isset($conn_id) && !empty($conn_id))
             dbUpdate($db_data,"nxg_customer_conn",'cust_id =? and conn_id=?',array($cust_id,$conn_id));
        else
            $index = dbInsert($db_data,"nxg_customer_conn");

        return $index;
}


function add_nxg_net_data($cust_id,$net_srv_prefix)
{
    $db_data = array();
    $db_data["net_srv_prefix"] = $net_srv_prefix;
    $db_data["cust_id"] = $cust_id;

    $index = dbInsert($db_data,"nxg_customer_net_service");

    return $index;
}


function add_inbound_traffic($cust_id,$pop_id,$prefix,$asn_level)
{
    $traffic_log_type = 'DB';
//    log_trace($traffic_log_type, __FILE__, __FUNCTION__, "Begin");

    $rule_id = dbInsert(array('cust_id'=>$cust_id,'pop_id'=>$pop_id,'asn_level'=>$asn_level),'nxg_customer_inbound_rule');
    dbInsert(array('rule_id'=>$rule_id,'prefix'=>$prefix),'nxg_customer_rule_prefix');

    $all_isp = get_all_isps($pop_id);

    foreach($all_isp as $isp)
    {
        dbInsert(array('rule_id'=>$rule_id,'isp_id'=>$isp['id']),'nxg_customer_rule_isp');
    }

//    log_trace($traffic_log_type, __FILE__, __FUNCTION__, "End");

}



function is_present_in_db($select_query)
{
    $result = dbFetchRows($select_query);
    return count($result);
}
function get_db_cust_details($cust_id)
{
    $result = dbFetchRow("select * from nxg_customer where customer_id = ?",array($cust_id));
    return $result;
}

function get_db_cust_conn_details($conn_id)
{
    $result = dbFetchRow("select * from nxg_customer_conn where conn_id = ?",array($cust_id));
    return $result;
}
function get_db_conn_if_config($pop_id,$conn_id)
{
    $result = dbFetchRows("select * from nxg_pop_interface_ip_mgmt where pop_id=? and conn_id =? ",array($pop_id,$conn_id));
    return $result;
}

function getCustomerDetails()
{
    $customer_details = dbFetchRows("select * from nxg_customer");
    return $customer_details;
}

function delete_cust_conn($conn_id)
{
 dbDelete('nxg_customer_conn', '`conn_id` = ?', array($conn_id));

}

function delete_customer($cust_id)
{
    dbDelete('nxg_customer_net_service', '`cust_id` = ?', array($cust_id));
    dbDelete('nxg_customer', '`customer_id` = ?', array($cust_id));
}

function toggle_cust_status($cust_id,$status)
{

    dbUpdate(array('offline'=>$status),'nxg_customer','customer_id =?',array($cust_id));
}
?>

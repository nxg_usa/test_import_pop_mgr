<?php 
include_once("/opt/observium/includes/defaults.inc.php");
include_once("/opt/observium/config.php");
include_once("/opt/observium/includes/definitions.inc.php");


function add_static_route($data,$admin_name)
{
$protocol=$data['protocol'];
$diversion_type=$data['diversion_type'];
$pop_id=$data['pop_id'];
$route_name=$data['route_name'];
$network_prefix_ip=$data['network_prefix_ip'];
$subnet_ip=$data['network_prefix_subnet_ip'];
$next_hop=$data['next_hop'];
$comment=$data['comment'];
$date=date('Y-m-d H:i:s');
$pop_name=dbFetchRows('select pop_name from nxg_pop_details where id='.$pop_id);

$index = dbInsert(array('protocol'=>$protocol,'diversion_type'=>$diversion_type , 'pop_name'=>$pop_name[0]['pop_name'],'add_date'=>$date,'route_name'=>$route_name,'pop_id'=>$pop_id,'network_prefix'=>$network_prefix_ip,'network_prefix_subnet'=>$subnet_ip,'next_hop'=>$next_hop,'comment'=>$comment,'admin_name'=>$admin_name),'nxg_static_route');
}

function getPop(){
$dbdata=dbFetchRows('select id,pop_name from nxg_pop_details');
return $dbdata;
}

?>

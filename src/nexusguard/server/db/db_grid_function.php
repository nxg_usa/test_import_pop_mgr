<?php
include "/opt/observium/nexusguard/validator/functions.php";
include "/opt/observium/nexusguard/logger/logger.php";
include_once("/opt/observium/includes/defaults.inc.php");
include_once("/opt/observium/config.php");
include_once("/opt/observium/includes/definitions.inc.php");

/* this function will return all pop details required to show grid details
*/
function get_pop_grid_details($vars)
{
    $logtype="DB";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");
   

    $array = array();

    // Short events? (no pagination, small out)
    $array['short'] = (isset($vars['short']) && $vars['short']);
    // With pagination? (display page numbers in header)
    $array['pagination'] = (isset($vars['pagination']) && $vars['pagination']);
    pagination($vars, 0, TRUE); // Get default pagesize/pageno
    $array['pageno']   = $vars['pageno'];
    $array['pagesize'] = $vars['pagesize'];
    $start    = $array['pagesize'] * $array['pageno'] - $array['pagesize'];
    $pagesize = $array['pagesize'];

    $param = array();

    $where = get_pop_filter($vars);
    $query = ' from nxg_pop_details left join nxg_customer_conn on nxg_customer_conn.pop_id = nxg_pop_details.id left join nxg_isp_details on nxg_pop_details.id = nxg_isp_details.pop_details_id left join nxg_router_interface_mapping on nxg_pop_details.id = nxg_router_interface_mapping.pop_details_id';
    $query .= $where ;
    $query .= ' group by nxg_pop_details.id ';
    $query_count = 'SELECT COUNT(*) '.$query;

    $query = 'select nxg_pop_details.*,nxg_customer_conn.* ,count(distinct(nxg_customer_conn.cust_id)) as customer_count  '.$query;
//    $query .= ' ORDER BY `nxg_pop_details.id` DESC ';
    $query .= "LIMIT $start,$pagesize";
    // Query events
    $array['entries'] = dbFetchRows($query, $param);

    // Query events count
    if ($array['pagination'] && !$array['short'])
    {
        $array['count'] = dbFetchCell($query_count, $param);
        $array['pagination_html'] = pagination($vars, $array['count']);
    } else {
        $array['count'] = count($array['entries']);
    }
 
    log_debug($logtype, __FILE__, __FUNCTION__, "Got ".count($array['entries'])." pop record");

    log_trace($logtype, __FILE__, __FUNCTION__, "End");

    return $array;        

}

function get_pop_and_isp_details($vars)
{

    $logtype="DB";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");
    
    $where = get_isp_filter($vars);   

//    $grid_records = dbFetchRows("select nxg_isp_details.id as isp_id ,nxg_pop_details.pop_name ,ports.*,`ports-state`.ifInOctets_rate as in_rate ,`ports-state`.ifOutOctets_rate as out_rate ,`ports-state`.ifInUcastPkts_rate ,`ports-state`.ifOutUcastPkts_rate, nxg_isp_details.* from nxg_pop_details left join devices on devices.device_id=nxg_pop_details.pop_edge_router_device_id left join nxg_isp_details on nxg_pop_details.id=nxg_isp_details.pop_details_id left join nxg_bgp_details on nxg_bgp_details.isp_id = nxg_isp_details.id left join ports on ports.ifName=nxg_isp_details.sub_interface_name and devices.device_id = ports.device_id left join `ports-state` on ports.port_id =`ports-state`.port_id $where");
    $grid_records = dbFetchRows("select nxg_isp_details.description, nxg_isp_details.id, nxg_isp_details.name,nxg_isp_details.id as isp_id,ports.port_id,nxg_bgp_details.sub_interface_name,`ports-state`.ifInOctets_rate as in_rate ,`ports-state`.ifOutOctets_rate as out_rate ,`ports-state`.ifInUcastPkts_rate ,`ports-state`.ifOutUcastPkts_rate from nxg_isp_details left join nxg_bgp_details on nxg_isp_details.id = nxg_bgp_details.isp_id left join nxg_pop_details on nxg_pop_details.id = nxg_isp_details.pop_details_id left join devices on nxg_pop_details.pop_edge_router_device_id = devices.device_id left join ports on ports.ifName = nxg_bgp_details.sub_interface_name and devices.device_id= ports.device_id left join  `ports-state` on ports.port_id =`ports-state`.port_id $where");

    log_debug($logtype, __FILE__, __FUNCTION__, "Got ".count($grid_records)." pop record");

    log_trace($logtype, __FILE__, __FUNCTION__, "End");

    return $grid_records;        
}


function get_pop_and_processors_details($vars)
{

    $logtype="DB";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");
   
    $where = get_scrubber_filter($vars);

    $grid_records = dbFetchRows("select nxg_pop_details.pop_name ,ports.ifName ,ports.port_id,nxg_router_interface_mapping.* from nxg_pop_details left outer join devices on devices.device_id=nxg_pop_details.pop_edge_router_device_id left join nxg_router_interface_mapping on nxg_router_interface_mapping.pop_details_id = nxg_pop_details.id left join ports on ports.ifName = nxg_router_interface_mapping.sub_interface_name and ports.device_id = nxg_pop_details.pop_edge_router_device_id $where");

    log_debug($logtype, __FILE__, __FUNCTION__, "Got ".count($grid_records)." pop record");

    log_trace($logtype, __FILE__, __FUNCTION__, "End");

    return $grid_records;        
}

function get_pop_filter($vars)
{
    $where = ' WHERE 1 ';
    foreach ($vars as $var => $value)
    {
        if ($value != '')
        {
            switch ($var)
            {
                case 'pop_id':
                
                    $where .= generate_query_values($value, 'nxg_pop_details.id');
                    break;
                case 'isp_name':
                    $where .= generate_query_values($value, 'nxg_isp_details.name');
                    break;
                case 'scrubber_type':
                    $where .= generate_query_values($value, 'nxg_router_interface_mapping.scrubber_type') ;
                    break;
                case 'checklog':
                    $where .= generate_query_values($value, 'commit_log', '%LIKE%') ;
                    break ;
                case 'timestamp_from':
                    $where .= ' AND `audit_time` >= ?';
                    $param[] = $value;
                    break;
                case 'timestamp_to':
                    $where .= ' AND `audit_time` <= ?';
                    $param[] = $value;
                    break;
            }
        }
    }
    return $where;

}
function get_isp_filter($vars)
{
    $where = ' WHERE 1 ';
    foreach ($vars as $var => $value)
    {
        if ($value != '')
        {
            switch ($var)
            {
                case 'pop_id':

                    $where .= generate_query_values($value, 'nxg_pop_details.id');
                    break;
                case 'isp_name':
                    $where .= generate_query_values($value, 'nxg_isp_details.name');
                    break;
                case 'intf_name':
                    $where .= generate_query_values($value, 'nxg_bgp_details.sub_interface_name', '%LIKE%') ;
                    break ;
            }
        }
    }
    return $where;

}
function get_scrubber_filter($vars)
{
    $where = ' WHERE 1 ';
    foreach ($vars as $var => $value)
    {
        if ($value != '')
        {
            switch ($var)
            {
                case 'pop_id':

                    $where .= generate_query_values($value, 'nxg_pop_details.id');
                    break;
                case 'scrubber_type':
                    $where .= generate_query_values($value, 'nxg_router_interface_mapping.scrubber_type') ;
                    break;
                case 'intf_name':
                    $where .= generate_query_values($value, 'nxg_router_interface_mapping.sub_interface_name') ;
                    break ;
            }
        }
    }
    return $where;

}


<?php
//include '/opt/observium/includes/dbFacile.php';
include_once("/opt/observium/includes/defaults.inc.php");
include_once("/opt/observium/config.php");
include_once("/opt/observium/includes/definitions.inc.php");
include_once("/opt/observium/nexusguard/logger/logger.php");
include_once("/opt/observium/nexusguard/config/isp_config.php");

//include("/opt/observium/includes/functions.inc.php");
include_once("/opt/observium/html/includes/functions.inc.php");

$logtype = "DB";

function add_traffic_engg_data($cust_traffic_engg_data)
{
    global $logtype;
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    $traffic_engg_data = $cust_traffic_engg_data['inbound_traffic_data'];

    if(isset($traffic_engg_data['inbound_pop']))
        $traffic_engg_data = array($traffic_engg_data);

    $cust_id = $cust_traffic_engg_data['cust_id'];
    
     $db_inbound_rule_data = dbFetchRows("select nxg_customer_inbound_rule.*,nxg_pop_details.pop_name as pop_name ,group_concat(nxg_customer_rule_prefix.prefix) as prefix_list, group_concat(nxg_customer_rule_isp.isp_id) as isp_list,group_concat(nxg_customer_rule_community.community_id) as community_list from nxg_customer_inbound_rule left join nxg_customer_rule_prefix on nxg_customer_inbound_rule.rule_id = nxg_customer_rule_prefix.rule_id left join nxg_customer_rule_isp on nxg_customer_rule_isp.rule_id = nxg_customer_inbound_rule.rule_id left join nxg_customer_rule_community on nxg_customer_rule_community.rule_id = nxg_customer_inbound_rule.rule_id left join nxg_pop_details on nxg_customer_inbound_rule.pop_id = nxg_pop_details.id where cust_id= ? group by nxg_customer_inbound_rule.rule_id;",array($cust_id));

    $db_inbound_rule = array();

    foreach($db_inbound_rule_data as $row )
    {
        $db_inbound_rule[$row['rule_id']] = $row;
    }
    
    for($i = 0 ;$i<count($traffic_engg_data);$i++)
    {
        $isp_array = array();
        $community_name_array= array();
        $prefix_details_array = array();
        $inbound_pop = $traffic_engg_data[$i]['inbound_pop'];
        $inbound_prefix = $traffic_engg_data[$i]['inbound_prefix'];
        $inbound_isp = $traffic_engg_data[$i]['inbound_isp'];
        $inbound_community = $traffic_engg_data[$i]['inbound_community'];
        $inbound_asn = $traffic_engg_data[$i]['inbound_asn'];
        $inbound_rule_id = $traffic_engg_data[$i]['rule_id'];
        $inbound_action = $traffic_engg_data[$i]['inbound_action'];   
        if(array_key_exists($inbound_rule_id,$db_inbound_rule))
        {

            log_debug($logtype, __FILE__, __FUNCTION__, "Processing rule id ".$inbound_rule_id,", and action =".$inbound_action);
            if($inbound_action == "delete" )
            {
                log_trace($logtype, __FILE__, __FUNCTION__, "Deleting rule id ".$inbound_rule_id);
                 dbDelete('nxg_customer_rule_prefix','rule_id =?',array($inbound_rule_id));
                dbDelete('nxg_customer_rule_community','rule_id =?',array($inbound_rule_id));
                dbDelete('nxg_customer_rule_isp','rule_id =?',array($inbound_rule_id));
                 dbDelete('nxg_customer_inbound_rule','rule_id =?',array($inbound_rule_id));
                continue;
            }
            dbUpdate(array('pop_id'=>$inbound_pop,'asn_level'=>$inbound_asn),'nxg_customer_inbound_rule','rule_id = ?',array($inbound_rule_id));
            if(!is_array($inbound_isp))
            {
                $inbound_isp = array($inbound_isp);
            }
            dbDelete('nxg_customer_rule_isp','rule_id =?',array($inbound_rule_id));
            foreach($inbound_isp as $isp)
            {
                $isp_details = get_isp_details($isp);
                $isp_name = $isp_details['name'];
                dbInsert(array('rule_id'=>$inbound_rule_id,'isp_id'=>$isp),'nxg_customer_rule_isp');
            }
            dbDelete('nxg_customer_rule_community','rule_id =?',array($inbound_rule_id));
            foreach($inbound_community as $community)
            {
                dbInsert(array('rule_id'=>$inbound_rule_id,'community_id'=>$community),'nxg_customer_rule_community');
            }
            dbDelete('nxg_customer_rule_prefix','rule_id =?',array($inbound_rule_id));
            foreach($inbound_prefix as $prefix)
            {
                //            $prefix_details = get_prefix_details($prefix);
                //$refix_array[]['name'] = $prefix_details['name']
                $prefix_details_array[]['name'] = "CPL-C-".$cust_traffic_engg_data['customer_name']."-".$prefix;
                dbInsert(array('rule_id'=>$inbound_rule_id,'prefix'=>$prefix),'nxg_customer_rule_prefix');
            }


        }
        else
        { 
            $rule_id = dbInsert(array('cust_id'=>$cust_id,'pop_id'=>$inbound_pop,'asn_level'=>$inbound_asn),'nxg_customer_inbound_rule');

            if(!is_array($inbound_isp))
            {
                $inbound_isp = array($inbound_isp);
            }
            foreach($inbound_isp as $isp)
            {
                $isp_details = get_isp_details($isp);
                $isp_name = $isp_details['name'];
                dbInsert(array('rule_id'=>$rule_id,'isp_id'=>$isp),'nxg_customer_rule_isp');
            }
            foreach($inbound_community as $community)
            {
                $community_details = get_bgp_community($community);
                $community_name = $community_details['community_name'];
                $community_name_array[]['name'] = $community_name;
                dbInsert(array('rule_id'=>$rule_id,'community_id'=>$community),'nxg_customer_rule_community');
            }
            foreach($inbound_prefix as $prefix)
            {
                //            $prefix_details = get_prefix_details($prefix);
                //$refix_array[]['name'] = $prefix_details['name']
                $prefix_details_array[]['name'] = "CPL-C-".$cust_traffic_engg_data['customer_name']."-".$prefix;
                dbInsert(array('rule_id'=>$rule_id,'prefix'=>$prefix),'nxg_customer_rule_prefix');
            }
        }
    }
 

}
function add_outbound_traffic_engg_data($data)
{
    $traffic_log_type = 'DB';
    log_trace($traffic_log_type, __FILE__, __FUNCTION__, "Begin");
    $type = $data['outbound_type'];
    $cust_id = $data['cust_id'];
    $pop_id = $data['pop_id'];
    $config = array();

    dbUpdate(array('outbound_te'=>$type),'nxg_customer_conn','customer_conn="tunnel" and cust_id = ? and pop_id = ? ',array($cust_id,$pop_id));
    log_trace($traffic_log_type, __FILE__, __FUNCTION__, "End");

}
?>

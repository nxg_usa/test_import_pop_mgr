<?php
include_once("/opt/observium/includes/defaults.inc.php");
include_once("/opt/observium/config.php");
include_once("/opt/observium/includes/definitions.inc.php");
include_once("/opt/observium/nexusguard/logger/logger.php");
include_once("/opt/observium/nexusguard/var/popmgr_var.php");
include_once "/opt/observium/nexusguard/nfsen/utils.inc.php";

$add_device_script="/opt/observium/add_device.php";
$logtype="DB";

function enroll_new_device($data)
{
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");
    global $config;
    global $var_device_user;
    global $var_device_password;
    global $var_popmgr_rancid_user;
    $edge_router_flag=1;
    $srx_flag=1;
    $edge_router_name=$data['new_name'];
    $edge_router_ip=$data['mgmt_ip'];
    $srx_name=$data['new_name_srx'];	
    $srx_ip=$data['new_ip_srx'];
    $ip=$data['new_ip'];
    $auth_level=$data['auth_level'];
    $auth_name=$data['user_name'];
    $auth_pwd=$data['auth_password'];
    $auth_algo=$data['auth_algo'];
    $port=$data['port'];
    $transport=$data['transport'];
    $snmp_ver="v3";
    $username=$data['edge_router_username'];
    $password=$data['edge_router_password'];
    $cryptopass=$data['crypto_password'];
    $cryptoalgo=$data['crypto_algo'];	
    $device_hostname=$edge_router_name;
    $device_ip=$data['netflow_source_address'];
    $netflow_port=$data['netflow_port'];
    $flow_mgr_ip=$data['flow_mgr_ip'];
    $timeout=$data['timeout'];
    $retries=$data['tries'];
    $device_id = 0;

    $snmp_v3 = array (
        'authlevel'  => $auth_level,
        'authname'   => $auth_name,
        'authpass'   => $auth_pwd,
        'authalgo'   => $auth_algo,
        'cryptopass' => $cryptopass,
        'cryptoalgo' => $cryptoalgo,
        'port'=> $port,
        'timeout'=>$timeout,
        'retries'=>$retries,

      );
    array_unshift($config['snmp']['v3'], $snmp_v3);
    add_device_to_hosts($device_hostname,$edge_router_ip);
    if(!empty($edge_router_name))
    {
    	$device_id=add_device($edge_router_name,$snmp_ver,$port,$transport);
    	if($device_id!=0)
	    {
		    $edge_router_flag=0;
	    }
        else
        {
            $edge_router_flag=1;

        }
	
    }
    if($edge_router_flag == 0)
    {
        configure_nfsen($device_hostname,$device_id,$flow_mgr_ip,$netflow_port);
        configure_rancid($device_hostname,$var_popmgr_rancid_user,$var_device_password);
    }
    return $edge_router_flag;
    log_trace($logtype, __FILE__, __FUNCTION__, "End");
}

function add_pop_template($data)
{

    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");
    
    global $var_device_user;
    global $var_device_password;
    $pop_name=$data['pop_name'];
    $auth_level=$data['auth_level'];
    $auth_name=$data['user_name'];
    $auth_pwd=$data['auth_password'];
    $auth_algo=$data['auth_algo'];
    $port=$data['port'];
    $transport=$data['transport'];
    $pop_desc=$data['pop_description'];
    $pop_edge_router_id=$data['edge-router'];
    $pop_srx_router_id=$data['srx'];
    $pop_edge_router_username=$data['user_name'];
    $pop_edge_router_password=$data['auth_password'];
    $vpc_terminal_ip_local=$data['vpc_local_ip'];
    $vpc_terminal_ip_remote=$data['vpc_remote_ip'];
    $ins_intf=$data['ins_intf'];
    $ins_vlan=$data['ins_vlan'];
    $pop_username=$data['username'];    
    $pop_password=$data['password'];
    $interfaces=$data['intf_to_processor'];
    $i=0;
    $vlan_names=$data['vlan_name'];
    $vlan_numbers=$data['vlan'];
    $types=$data['type'];
    $customs_names=$data['traffic_name'];
    $exabgp_interface_name=$data['exabgp_interface'];
    $exabgp_vlan=$data['vlan_intf'];
    $exabgp_ip=$data['exa_ip'];
    $server_ip=$data['server_peer'];
    $exa_local_as=$data['exa_local'];
    $exa_peer_as=$data['exa_peer'];
    $isp_name=$data['general_name'];
    $isp_desc=$data['general_description'];
    $offnet_interface=$data['nb_interface'];
    $offnet_interface_vlan=$data['nb_vlan_interface'];
    $isp_local_ip=$data['local_ip'];
    $isp_local_ip_subnet=$data['local_ip_subnet'];
    $isp_neighbour_ip=$data['neighbour_ip'];
    $isp_neighbour_ip_subnet=$data['neighbour_ip_subnet'];
    $isp_local_as=$data['local_as'];
    $isp_peer_as=$data['peer_as'];
    $mgmt_interface = $data['mgmt_interface'];
    $mgmt_ip = $data['mgmt_ip'];
    $mgmt_ip_subnet = $data['mgmt_ip_subnet'];
    $mgmt_gateway = $data['mgmt_gateway'];
    $scope = $data['scope'];
    $device_enrolment_status=0;
    $pop_id=$data['pop_id']; 
    $loopback_ip=$data['loopback_ip'];
    $anycast_ip=$data['anycast_ip'];
    $anycast_ip_subnet=$data['anycast_ip_subnet'];
    $ossid_device_id=$data['oss_dev_id'];
    $notes=$data['pop_notes'];
    //exaBGP details

    $global_server_ip = $data['global_server_ip'];
    $global_server_ssh_key = $data['global_server_ssh_key'];
    $global_server_username = $data['global_server_username'];
    $global_speaker_interface = $data['global_speaker_interface'];
    $global_speaker_md5 = $data['global_speaker_md5']; 
    $global_speaker_ip= $data['global_speaker_ip'];
    $global_vlan = $data['global_vlan']; 
    $local_asn = $data['local_asn'];   
    $local_asn_global = $data['local_asn_global'];    
    $local_interface = $data['local_interface']; 
    $local_server_ip = $data['local_server_ip'];
    $local_server_ssh_key = $data['local_server_ssh_key'];    
    $local_server_username = $data['local_server_username'];  
    $local_speaker_ip = $data['local_speaker_ip'];    
    $local_speaker_ip_local = $data['local_speaker_ip_local'];  
    $local_speaker_md5 = $data['local_speaker_md5'];  
    $local_vlan = $data['local_vlan']; 
    $restart_timer_global = $data['restart_timer_global'];   
    $restart_timer_local = $data['restart_timer_local'];
    $stale_routes_time_local = $data['stale_routes_time_local'];
    $stale_routes_time_global = $data['stale_routes_time_global'];
    $bgp_group_name_local = "ExaBGP-FlowSpec-Local";
    $bgp_group_name_global = "ExaBGP-FlowSpec-Remote";
    $prefix_list =$data['prefix_list'];
    $pl_agg_prefix_list=$data['pl_agg_prefix_list'];
    //Netflow &syslog detaails

    $netflow_server_ip=$data['netflow_server_ip'];
    $netflow_source_address=$data['netflow_source_address'];
    $netflow_port=$data['netflow_port'];
    $syslog_host_ip=$data['syslog_host_ip'];
    $flow_mgr_ip=$data['flow_mgr_ip'];
    $edge_router_name="";

    $lag_interface_div = $data['lag_interface'];
    $lag_interface_ins = $data['lag_interface_ins'];


     $lag_interface_ins=$data['lag_interface_ins'];
     $lag_interface=$data['lag_interface'];


    $array= array();
    log_trace($logtype, __FILE__, __FUNCTION__, "scope=".$scope);
    if(is_string($interfaces))
    {
        $tmp=(explode(" ",$interfaces));
        $interfaces=$tmp;
        $tmp=(explode(" ",$vlan_numbers));
        $vlan_numbers=$tmp;
        $tmp=(explode(" ",$types));
        $types=$tmp;
        $tmp=(explode(" ",$customs_names));
        $customs_names=$tmp;
        $tmp=(explode(" ",$ins_intf));
        $ins_intf=$tmp;
        $tmp=(explode(" ",$ins_vlan));
        $ins_vlan=$tmp;
        $lag_interface = array($lag_interface);
        $lag_interface_ins = array($lag_interface_ins);
    }

    $pop_edge_router_name=$data['new_name'];
    $pop_edge_router_username=$data['user_name'];
    $pop_edge_router_password=$data['auth_password'];        
    if(!empty($pop_edge_router_name))
    {
        $edge_router_id=dbFetchRows("select device_id from devices where UPPER(hostname)=UPPER('$pop_edge_router_name');");
        $pop_edge_router_id=$edge_router_id[0]['device_id'];
        if(!empty($pop_edge_router_id))
        {
            $device_enrolment_status=1;
        }
    }
    elseif(!empty($pop_edge_router_id))
    {
        $device_enrolment_status=1;
    }

    if($data['srx_type']=="new_router_srx")
    {
        $pop_srx_router_name=$data['new_name_srx'];
        if(!empty($pop_srx_router_name))
        {
            $srx_router_device_id=dbFetchRows("select device_id from devices where UPPER(hostname)=UPPER('$pop_srx_router_name');");
            $pop_srx_router_id=$srx_router_device_id[0]['device_id'];
        }
    }
    
    if($scope=="new")
    {
        
        $status="insert";
        $index=dbInsert(array('pop_name'=>$pop_name,'pop_edge_router_device_id'=>$pop_edge_router_id,'pop_description'=>$pop_desc,'loopback_ip'=>$loopback_ip,'device_enrolment_status'=>$device_enrolment_status,'oss_device_id'=>$ossid_device_id,'pop_notes'=>$notes,'pop_edge_router_name'=>$pop_edge_router_name,'username'=>$var_device_user,'password'=>$var_device_password,'pop_edge_router_username'=>$pop_edge_router_username,'pop_edge_router_password'=>$pop_edge_router_password,'netflow_server_ip'=>$netflow_server_ip,'netflow_source_address'=>$netflow_source_address,'netflow_port'=>$netflow_port,'syslog_host_ip'=>$syslog_host_ip,'flow_mgr_ip'=>$flow_mgr_ip),'nxg_pop_details');
        $array['pop_id']=$index;
        $service_prefix_list  = explode("\n",$prefix_list);
        for($i = 0 ; $i < count($service_prefix_list) ;$i++)
        {
            dbInsert(array('pop_id'=>$index,'prefix'=>$service_prefix_list[$i]),'nxg_app_service_prefix_list');    
        }
        $agg_prefix_list = explode("\n",$pl_agg_prefix_list);
        for($i = 0 ; $i < count($agg_prefix_list) ;$i++)
        {
            dbInsert(array('pop_id'=>$index,'prefix'=>$agg_prefix_list[$i]),'nxg_pl_agg_prefix_list');
        }
  
        dbInsert(array('mgmt_interface'=>$mgmt_interface,'mgmt_ip'=>$mgmt_ip,'mgmt_ip_subnet'=>$mgmt_ip_subnet,'mgmt_gateway'=>$mgmt_gateway,'pop_id'=>$index),'nxg_pop_mgmt_config');
        $i=0;
        foreach($interfaces as $interface)
        {
            $vlan_name=$vlan_names[$i];
            $vlan_number=$vlan_numbers[$i];
            $insertion_intf=$ins_intf[$i];
            $insertion_vlan=$ins_vlan[$i];  
            $type=$types[$i];
            $custom_name="";
            if($type=="custom")
            {
                $custom_name=$customs_names[$i];
            }
           $row_cnt_pop_details= dbInsert(array('pop_details_id'=>$index, 'device_interface_name'=>$interface, 'scrubber_type'=>$type, 'scrubber_custom_name'=>$custom_name,'insertion_interface'=>$insertion_intf,'insertion_vlan'=>$insertion_vlan,'sub_interface_name'=>$interface.".".$vlan_number,'vlan_id'=>$vlan_number), 'nxg_router_interface_mapping');
            $member_ports = $lag_interface[$i][0];
             for($j=0;$j<count($member_ports);$j++)
             {
                //dbInsert(array('pop_id'=>$index,'ae_interface_name'=>$interface,'member_interface_name'=>$member_ports[$j]),'nxg_pop_ae_members');
                if(!empty($member_ports[$j]))
                    add_ae_member_port($index, $interface,$member_ports[$j]);

             }

             $member_ports = $lag_interface_ins[$i][0];
             for($j=0;$j<count($member_ports);$j++)
             {

                // dbInsert(array('pop_id'=>$index,'ae_interface_name'=>$insertion_intf,'member_interface_name'=>$member_ports[$j],'type'=>"ins"),'nxg_pop_ae_members');
                if(!empty($member_ports[$j]))
                    add_ae_member_port($index, $insertion_intf,$member_ports[$j]);
             }

            $i++;
        }   

        dbInsert(array('pop_details_id'=>$index,'local_speaker_interface'=>$local_interface,'local_vlan'=>$local_vlan,'local_speaker_ip'=>$local_speaker_ip,'local_asn'=>$local_asn,'peer_asn'=>$local_asn,'restart_time_local'=>$restart_timer_local,'global_speaker_interface'=>$global_speaker_interface,'global_vlan'=>$global_vlan,'global_speaker_ip'=>$global_speaker_ip,'local_asn_global'=>$local_asn_global,'peer_asn_global'=>$local_asn_global,'restart_time_global'=>$restart_timer_global,'local_server_ip'=>$local_server_ip,'global_server_ip'=>$global_server_ip,'local_speaker_md5_key_string'=>$local_speaker_md5,'global_speaker_md5_key_string'=>$global_speaker_md5,'stale_routes_time_local'=>$stale_routes_time_local,'stale_routes_time_global'=>$stale_routes_time_global,'bgp_group_name_local'=>$bgp_group_name_local,'bgp_group_name_global'=>$bgp_group_name_global), 'nxg_exabgp_details');

        $array['status']='insert';

    }
    elseif($scope=="preenrolled_edit")
    {
        $status="update";
        $array['status']='update';
        $array['pop_id']=$pop_id;
        if($data['edge_router_type']=="new_router")
        {
            $pop_edge_router_name=$data['new_name'];
            if(!empty($pop_edge_router_name))
            {
                $edge_router_id=dbFetchRows("select device_id from devices where UPPER(hostname)=UPPER('$pop_edge_router_name');");
                $pop_edge_router_id=$edge_router_id[0]['device_id'];
                if(!empty($pop_edge_router_id))
                {
                    $device_enrolment_status=1;
                }
            }
        }
        elseif(!empty($pop_edge_router_id))
        {
            $device_enrolment_status=1;
        }
        dbUpdate(array('pop_name'=>$pop_name,'pop_description'=>$pop_desc,'pop_edge_router_device_id'=>$pop_edge_router_id,'username'=>$var_device_user,'password'=>$var_device_password,'loopback_ip'=>$loopback_ip,'device_enrolment_status'=>$device_enrolment_status,'oss_device_id'=>$ossid_device_id,'pop_notes'=>$notes,'pop_edge_router_name'=>$pop_edge_router_name,'pop_edge_router_username'=>$pop_edge_router_username,'pop_edge_router_password'=>$pop_edge_router_password,'netflow_server_ip'=>$netflow_server_ip,'netflow_source_address'=>$netflow_source_address,'netflow_port'=>$netflow_port,'syslog_host_ip'=>$syslog_host_ip,'flow_mgr_ip'=>$flow_mgr_ip),'nxg_pop_details','`id`=?',array($pop_id));


            dbDelete('nxg_app_service_prefix_list','pop_id=?',array($pop_id));
            $service_prefix_list  = explode("\n",$prefix_list);
            for($i = 0 ; $i < count($service_prefix_list) ;$i++)
            {
                dbInsert(array('pop_id'=>$pop_id,'prefix'=>$service_prefix_list[$i]),'nxg_app_service_prefix_list');
            }


             dbDelete('nxg_pl_agg_prefix_list','pop_id=?',array($pop_id));
            $agg_prefix_list  = explode("\n",$pl_agg_prefix_list);
            for($i = 0 ; $i < count($agg_prefix_list) ;$i++)
            {
                dbInsert(array('pop_id'=>$pop_id,'prefix'=>$agg_prefix_list[$i]),'nxg_pl_agg_prefix_list');
            }
             //dbDelete('nxg_pop_ae_members','pop_id=?',array($pop_id));
        

        dbUpdate(array('mgmt_interface'=>$mgmt_interface,'mgmt_ip'=>$mgmt_ip,'mgmt_ip_subnet'=>$mgmt_ip_subnet,'mgmt_gateway'=>$mgmt_gateway),'nxg_pop_mgmt_config','`pop_id`=?',array($pop_id));
        dbDelete('nxg_router_interface_mapping', "`pop_details_id` =  ?", array($pop_id));
        $i=0;
        foreach($interfaces as $interface)
        {
            $vlan_name=$vlan_names[$i];
            $vlan_number=$vlan_numbers[$i];
            $insertion_intf=$ins_intf[$i];
            $insertion_vlan=$ins_vlan[$i];
            $type=$types[$i];
            $custom_name="";
            if($type=="custom")
            {
                $custom_name=$customs_names[$i];
            }
           $row_cnt_pop_details= dbInsert(array('pop_details_id'=>$pop_id, 'device_interface_name'=>$interface, 'scrubber_type'=>$type, 'scrubber_custom_name'=>$custom_name,'insertion_interface'=>$insertion_intf,'insertion_vlan'=>$insertion_vlan,'sub_interface_name'=>$interface.".".$vlan_number,'vlan_id'=>$vlan_number), 'nxg_router_interface_mapping');

         $member_ports = $lag_interface[$i][0];
         for($j=0;$j<count($member_ports);$j++)
             {
          //      dbInsert(array('pop_id'=>$pop_id,'ae_interface_name'=>$interface,'member_interface_name'=>$member_ports[$j],'row_id'=>$row_cnt_pop_details,'type'=>"div"),'nxg_pop_ae_members');
                if(!empty($member_ports[$j]))
                    add_ae_member_port($pop_id, $interface,$member_ports[$j]);

             }

             $member_ports = $lag_interface_ins[$i][0];
             for($j=0;$j<count($member_ports);$j++)
             {

//                 dbInsert(array('pop_id'=>$pop_id,'ae_interface_name'=>$insertion_intf,'member_interface_name'=>$member_ports[$j],'row_id'=>$row_cnt_pop_details,'type'=>"ins"),'nxg_pop_ae_members');
                if(!empty($member_ports[$j]))
                    add_ae_member_port($pop_id, $insertion_intf,$member_ports[$j]);
             }
            $i++;
        }
        dbUpdate(array('local_speaker_interface'=>$local_interface,'local_vlan'=>$local_vlan,'local_speaker_ip'=>$local_speaker_ip,'local_asn'=>$local_asn,'peer_asn'=>$local_asn,'restart_time_local'=>$restart_timer_local,'global_speaker_interface'=>$global_speaker_interface,'global_vlan'=>$global_vlan,'global_speaker_ip'=>$global_speaker_ip,'local_asn_global'=>$local_asn_global,'peer_asn_global'=>$local_asn_global,'restart_time_global'=>$restart_timer_global,'local_server_ip'=>$local_server_ip,'global_server_ip'=>$global_server_ip,'local_speaker_md5_key_string'=>$local_speaker_md5,'global_speaker_md5_key_string'=>$global_speaker_md5,'stale_routes_time_local'=>$stale_routes_time_local,'stale_routes_time_global'=>$stale_routes_time_global), 'nxg_exabgp_details','`pop_details_id` = ?',array($pop_id));

    }
    else
    {
        $status="update";
        $array['status']='update';
        //		echo"<br> in postenroll update********************$device_enrolment_status %%%%%%%%%%%%%%$pop_id<br>";
        dbUpdate(array('pop_name'=>$pop_name,'pop_description'=>$pop_desc,'pop_edge_router_device_id'=>$pop_edge_router_id,'username'=>$pop_username,'loopback_ip'=>$loopback_ip,'device_enrolment_status'=>$device_enrolment_status,'oss_device_id'=>$ossid_device_id,'pop_notes'=>$notes,'pop_edge_router_name'=>$pop_edge_router_name,'pop_edge_router_username'=>$pop_edge_router_username,'pop_edge_router_password'=>$pop_edge_router_password),'nxg_pop_details','`id`=?',array($pop_id));


        dbUpdate(array('mgmt_interface'=>$mgmt_interface,'mgmt_ip'=>$mgmt_ip,'mgmt_ip_subnet'=>$mgmt_ip_subnet,'mgmt_gateway'=>$mgmt_gateway),'nxg_pop_mgmt_config','`pop_id`=?',array($pop_id));

        dbDelete('nxg_router_interface_mapping', "`pop_details_id` =  ?", array($pop_id));
        $i=0;
        foreach($interfaces as $interface)
        {
            $vlan_name=$vlan_names[$i];
            $vlan_number=$vlan_numbers[$i];
            $insertion_intf=$ins_intf[$i];
            $insertion_vlan=$ins_vlan[$i];
            $type=$types[$i];
            $custom_name="";
            if($type=="custom")
            {
                $custom_name=$customs_names[$i];
            }
            dbInsert(array('pop_details_id'=>$pop_id, 'device_interface_name'=>$interface, 'scrubber_type'=>$type, 'scrubber_custom_name'=>$custom_name,'insertion_interface'=>$insertion_intf,'insertion_vlan'=>$insertion_vlan,'sub_interface_name'=>$interface.".".$vlan_number,'vlan_id'=>$vlan_number), 'nxg_router_interface_mapping');
            $i++;
        }

        dbUpdate(array('local_speaker_interface'=>$local_interface,'local_vlan'=>$local_vlan,'local_speaker_ip'=>$local_speaker_ip,'local_asn'=>$local_asn,'peer_asn'=>$local_asn,'restart_time_local'=>$restart_timer_local,'global_speaker_interface'=>$global_speaker_interface,'global_vlan'=>$global_vlan,'global_speaker_ip'=>$global_speaker_ip,'local_asn_global'=>$local_asn_global,'peer_asn_global'=>$local_asn_global,'restart_time_global'=>$restart_timer_global,'local_server_ip'=>$local_server_ip,'global_server_ip'=>$global_server_ip,'local_speaker_md5_key_string'=>$local_speaker_md5,'global_speaker_md5_key_string'=>$global_speaker_md5,'stale_routes_time_local'=>$stale_routes_time_local,'stale_routes_time_global'=>$stale_routes_time_global), 'nxg_exabgp_details','`pop_details_id` = ?',array($pop_id));


    }

    log_trace($logtype, __FILE__, __FUNCTION__, "End");
    return $array;
}

function decomission_pop($pop_id)
{
    $deleted_rows=dbDelete('nxg_pop_details', "`id` =  ?", array($pop_id));
    if(!$deleted_rows)
    {
        return 1;
    }
    else
    {
        return 0;
    }

}
function get_isp_name($pop_id)
{
	    $logtype="DB";
	    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");
	    $isp_name= dbFetchRow("select name from nxg_isp_details where pop_details_id = ?",array($pop_id));	
	    return $isp_name['name'];
	    log_trace($logtype, __FILE__, __FUNCTION__, "End");

}

function add_pop_interface_config($pop_id,$interface_name,$unit,$ip_address, $community, $route_dis, $div_name,$type,$subtype)
{
    $logtype="DB";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");
    
    $id = dbInsert(array('pop_id'=>$pop_id, 'ip_address'=>$ip_address, 'interface_unit'=>$unit, 'target'=>$community, 'route_distinguisher'=>$route_dis,'interface_name'=>$interface_name, 'diversion_name'=>$div_name,'routing_instance'=>$div_name,'type'=>"pop",'subtype'=>$subtype),'nxg_pop_interface_ip_mgmt');

    log_trace($logtype, __FILE__, __FUNCTION__, "End");
    return $id;
}

function get_all_pops()
{
    $logtype="DB";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");
    $pop_details = dbFetchRows("select id, pop_name from nxg_pop_details");
    log_trace($logtype, __FILE__, __FUNCTION__, "End");
    return $pop_details;
}
function get_sinlge_pop_detail($pop_id)
{
    $logtype="DB";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");
    $pop_details = dbFetchRow("select * from nxg_pop_details where id = ?",array($pop_id));
    log_trace($logtype, __FILE__, __FUNCTION__, "End");
    return $pop_details;
}
function get_all_pop_data($pop_id)
{
	log_trace($logtype, __FILE__, __FUNCTION__, "Begin");
	$pop_data=dbFetchRow("select nxg_pop_details.*,nxg_isp_details.*,nxg_exabgp_details.*,nxg_bgp_details.*,nxg_pop_mgmt_config.* from nxg_pop_details left join nxg_isp_details on nxg_isp_details.pop_details_id=nxg_pop_details.id left join nxg_pop_mgmt_config on nxg_pop_mgmt_config.pop_id=nxg_pop_details.id left join nxg_exabgp_details on nxg_exabgp_details.pop_details_id=nxg_pop_details.id left join nxg_bgp_details on nxg_bgp_details.isp_id=nxg_isp_details.id where nxg_pop_details.id=?",array($pop_id));
	log_trace($logtype, __FILE__, __FUNCTION__, "End");
	return $pop_data;

}


function is_interface_used($pop_id, $interface_name,$unit,$vlan)
{
    $logtype="DB";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    $interface_details = dbFetchRows("select * from nxg_pop_details left join nxg_isp_details on nxg_isp_details.pop_details_id = nxg_pop_details.id left join nxg_pop_interface_ip_mgmt on nxg_pop_interface_ip_mgmt.pop_id = nxg_pop_details.id left join nxg_exabgp_details on nxg_exabgp_details.pop_details_id = nxg_pop_details.id left join nxg_bgp_details on nxg_bgp_details.isp_id = nxg_isp_details.id where (nxg_bgp_details.service_interface= ? or nxg_pop_interface_ip_mgmt.interface_name = ? or nxg_exabgp_details.local_speaker_interface = ? or nxg_exabgp_details.global_speaker_interface = ?) and nxg_pop_details.id = ? and nxg_bgp_details.service_interface_unit = ?  and nxg_bgp_details.service_interface_vlan = ?",array($interface_name,$interface_name,$interface_name,$interface_name,$pop_id,$unit,$vlan));

    log_trace($logtype, __FILE__, __FUNCTION__, "End");

    return $interface_details;
}
function get_pop_details_by_name($pop_name)
{
    $logtype="DB";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    $pop_details = dbFetchRow("select * from nxg_pop_details where UPPER(pop_name) = UPPER(?) ",array($pop_name));

    log_trace($logtype, __FILE__, __FUNCTION__, "End");

    return $pop_details;
}
/*function get_device_by_name($device_name)
{
    $logtype="DB";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    $device_details = dbFetchRow("select * from devices where UPPER(hostname) = UPPER(?) ",array($device_name));

    log_trace($logtype, __FILE__, __FUNCTION__, "End");

    return $device_details;
}
*/
function get_pop_ports_info($pop_id)
{
    $logtype="DB";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    //$device_details = dbFetchRows("select ifName from ports left join devices on devices.device_id=ports.device_id left join nxg_pop_details on nxg_pop_details.pop_edge_router_device_id=devices.device_id where nxg_pop_details.id = ? and ifType='ethernetCsmacd' or ifType='ieee8023adLag' ",array($pop_id));
    $device_details = dbFetchRows("select ifName from ports left join devices on devices.device_id=ports.device_id left join nxg_pop_details on nxg_pop_details.pop_edge_router_device_id=devices.device_id where nxg_pop_details.id = ?  and (ifType='ethernetCsmacd' or ifType='ieee8023adLag') and ( ifName like '%ae%');",array($pop_id));

    log_trace($logtype, __FILE__, __FUNCTION__, "End");

    return $device_details;
}
function get_pop_ge_ports_info($pop_id)
{
    $logtype="DB";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    //$device_details = dbFetchRows("select ifName from ports left join devices on devices.device_id=ports.device_id left join nxg_pop_details on nxg_pop_details.pop_edge_router_device_id=devices.device_id where nxg_pop_details.id = ? and ifType='ethernetCsmacd' or ifType='ieee8023adLag' ",array($pop_id));
    $device_details = dbFetchRows("select ifName from ports left join devices on devices.device_id=ports.device_id left join nxg_pop_details on nxg_pop_details.pop_edge_router_device_id=devices.device_id where nxg_pop_details.id = ?  and (ifType='ethernetCsmacd' or ifType='ieee8023adLag') and ( ifName like '%ge%' or ifName like '%xe%');",array($pop_id));

    log_trace($logtype, __FILE__, __FUNCTION__, "End");
    return $device_details;

}
function get_pop_interface_data($pop_id)
{
        log_trace($logtype, __FILE__, __FUNCTION__, "Begin");
        $pop_data=dbFetchRows("select * from nxg_router_interface_mapping where pop_details_id=?",array($pop_id));
        log_trace($logtype, __FILE__, __FUNCTION__, "End");
        return $pop_data;

}

function db_delete_interface($pop_id,$interface,$unit)
{

         dbDelete('nxg_router_interface_mapping', "`pop_details_id` =  ? and `device_interface_name` = ? and vlan_id` = ?" , array($pop_id,$interface,$unit));
        dbDelete('nxg_pop_interface_ip_mgmt', "`pop_id` =  ? and `interface_name` = ? and `interface_unit` = ?" , array($pop_id,$interface,$unit));

}



function db_insert_interface($pop_id,$interface,$type,$custom_name=NULL,$insertion_intf,$insertion_vlan,$vlan_number)
{
                                dbInsert(array('pop_details_id'=>$pop_id, 'device_interface_name'=>$interface, 'scrubber_type'=>$type, 'scrubber_custom_name'=>$custom_name,'insertion_interface'=>$insertion_intf,'insertion_vlan'=>$insertion_vlan,'sub_interface_name'=>$interface.".".$vlan_number,'vlan_id'=>$vlan_number), 'nxg_router_interface_mapping');
                //              dbInsert(array('pop_id'=>$pop_id, 'ip_address'=>$ip_address, 'interface_unit'=>$unit, 'target'=>$community, 'route_distinguisher'=>$route_dis,'interface_name'=>$interface_name, 'diversion_name'=>$div_name),'nxg_pop_interface_ip_mgmt');


}


function  update_exabgp_config($pop_data)
{

    $pop_id=$pop_data['pop_id'];
    $global_server_ip = $pop_data['global_server_ip'];
    $global_server_username = $pop_data['global_server_username'];
    $global_speaker_interface = $pop_data['global_speaker_interface'];
    $global_speaker_md5 = $pop_data['global_speaker_md5'];
    $global_speaker_ip= $pop_data['global_speaker_ip'];
    $global_vlan = $pop_data['global_vlan'];
    $local_asn = $pop_data['local_asn'];
    $peer_asn = $local_asn;
    $local_asn_global = $pop_data['local_asn_global'];
    $peer_asn_global = $local_asn_global;
    $local_speaker_interface = $pop_data['local_interface'];
    $local_server_ip = $pop_data['local_server_ip'];
    $local_server_username = $pop_data['local_server_username'];
    $local_speaker_ip = $pop_data['local_speaker_ip'];
    $local_speaker_ip_local = $pop_data['local_speaker_ip_local'];
    $local_speaker_md5 = $pop_data['local_server_md5'];
    $local_vlan = $pop_data['local_vlan'];
    $restart_timer_global = $pop_data['restart_timer_global'];
    $restart_timer_local = $pop_data['restart_timer_local'];
    $stale_routes_time_local =  $pop_data['stale_routes_time_local'];
    $stale_routes_time_global =  $pop_data['stale_routes_time_global'];
    $edge_router_username = $pop_data['username'];
    $local_speaker_md5_key_string = $pop_data['local_server_md5'];
    $global_speaker_md5_key_string = $pop_data['global_speaker_md5'];

    $update_status = dbUpdate(array('local_speaker_interface'=>$local_speaker_interface,'local_vlan'=>$local_vlan,'local_speaker_ip'=>$local_speaker_ip,'restart_time_local'=>$restart_timer_local,'stale_routes_time_local'=>$stale_routes_time_local,'global_speaker_interface'=>$global_speaker_interface,'global_vlan'=>$global_vlan,'global_speaker_ip'=>$global_speaker_ip,'local_asn_global'=>$local_asn_global,'peer_asn_global'=>$peer_asn_global,'restart_time_global'=>$restart_timer_global,'stale_routes_time_global'=>$stale_routes_time_global,'local_server_ip'=>$local_server_ip,'global_server_ip'=>$global_server_ip,'local_speaker_md5_key_string'=>$local_speaker_md5_key_string,'global_speaker_md5_key_string'=>$global_speaker_md5_key_string,'local_asn'=>$local_asn,'peer_asn'=>$peer_asn),'nxg_exabgp_details','`pop_details_id`=?',array($pop_id));

    return $update_status;

}
function update_interfaces_db($pop_data)
{
    $pop_id=$pop_data['pop_id'];
    $intf_to_processors=$pop_data['intf_to_processor'];
    $intf_to_processors_vlan = $pop_data['vlan'];
    $scrubber_type= $pop_data['type'];
    $custom_name= $pop_data['traffic_name'];
    $gui_row_id= $pop_data['gui_row_id'];
    $insertion_intf=$pop_data['ins_intf'];
    $insertion_vlan=$pop_data['ins_vlan'];
     $lag_interface = $pop_data['lag_interface'];
    $lag_interface_ins = $pop_data['lag_interface_ins'];

    $pop_interface_data_db=get_pop_interface_data($pop_id);
    $db_intf_keys=generate_db_keys($pop_interface_data_db);
    if(is_string($intf_to_processors))
    {
        $tmp=(explode(" ",$intf_to_processors));
        $intf_to_processors=$tmp;
        $tmp=(explode(" ",$insertion_intf));
        $insertion_intf=$tmp;
        $tmp=(explode(" ",$intf_to_processors_vlan));
        $intf_to_processors_vlan=$tmp;
        $tmp=(explode(" ",$scrubber_type));
        $scrubber_type=$tmp;
        $tmp=(explode(" ",$custom_name));
        $custom_name=$tmp;
        $tmp=(explode(" ",$insertion_vlan));
        $insertion_vlan=$tmp;
        $tmp=(explode(" ",$gui_row_id));
        $gui_row_id=$tmp;

    }
    if(!is_array($lag_interface))
    {
        $lag_interface = array($lag_interface);
    }
    if(!is_array($lag_interface_ins))
    {
        $lag_interface_ins = array($lag_interface_ins);
    }
    for($i=0;$i<count($intf_to_processors);$i++)
    {
        $intf=$intf_to_processors[$i];
        $unit=$intf_to_processors_vlan[$i];
        $key=$intf.".".$unit;
        $scrub_type=$scrubber_type[$i];
        $scrubber_custom_name=$custom_name[$i];
        $ins_intf=$insertion_intf[$i];
        $ins_vlan=$insertion_vlan[$i];
        $row_id = $gui_row_id[$i];


        log_trace($logtype, __FILE__, __FUNCTION__, "Processing processor info =".$intf);
            log_trace($logtype, __FILE__, __FUNCTION__, "Processing row id ".$row_id);
        if(array_key_exists($row_id,$db_intf_keys))
        {
            dbUpdate(array('device_interface_name'=>$intf,'scrubber_type'=>$scrub_type,'scrubber_custom_name'=>$scrubber_custom_name,'vlan_id'=>$unit,'sub_interface_name'=>$intf.".".$unit,'insertion_interface' => $ins_intf,'insertion_vlan' => $ins_vlan),'nxg_router_interface_mapping','`row_id`=?',array($row_id));
            unset($db_intf_keys[$row_id]);
        //    dbDelete('nxg_pop_ae_members', "`pop_id` =  ? and row_id = ? ", array($pop_id,$row_id));

            $member_ports_div = $lag_interface[$i][0];
            $member_ports_ins = $lag_interface_ins[$i][0];

            if(!is_array($member_ports_div))
            {
                $member_ports_div = array($member_ports_div);
            }
            if(!is_array($member_ports_ins))
            {
                $member_ports_ins = array($member_ports_ins);
            }
            for($x=0;$x<count($member_ports_div);$x++)
            {
                if(!empty($member_ports_div[$x]))
                {
               //     dbInsert(array('pop_id'=>$pop_id,'ae_interface_name'=>$intf,'member_interface_name'=>$member_ports_div[$x],'row_id'=>$row_id,'type'=>"div"),'nxg_pop_ae_members');
                     add_ae_member_port($pop_id, $intf,$member_ports_div[$x]);
                }
            }
            for($y=0;$y<count($member_ports_ins);$y++)
            {
                if(!empty($member_ports_ins[$y]))
                {
                    //dbInsert(array('pop_id'=>$pop_id,'ae_interface_name'=>$ins_intf,'member_interface_name'=>$member_ports_ins[$y],'row_id'=>$row_id,'type'=>"ins"),'nxg_pop_ae_members');
                     add_ae_member_port($pop_id, $ins_intf,$member_ports_ins[$y]);
                }
            }

        }
        else
        {
            $index=dbInsert(array('pop_details_id'=>$pop_id,'device_interface_name'=>$intf,'scrubber_type'=>$scrub_type,'scrubber_custom_name'=>$scrubber_custom_name,'vlan_id'=>$unit,'sub_interface_name'=>$intf.".".$unit,'insertion_interface' => $ins_intf,'insertion_vlan' => $ins_vlan),'nxg_router_interface_mapping');
             $member_ports_div = $lag_interface[$i][0];
            $member_ports_ins = $lag_interface_ins[$i][0];
            if(!is_array($member_ports_div))
            {
                $member_ports_div = array($member_ports_div);
            }
            if(!is_array($member_ports_ins))
            {
                $member_ports_ins = array($member_ports_ins);
            }
            for($x=0;$x<count($member_ports_div);$x++)
            {
                if(!empty($member_ports_div[$x]))
                {
                //    dbInsert(array('pop_id'=>$pop_id,'ae_interface_name'=>$intf,'member_interface_name'=>$member_ports_div[$x],'row_id'=>$index,'type'=>"div"),'nxg_pop_ae_members');
                    add_ae_member_port($pop_id, $intf,$member_ports_div[$x]);
                }
            }
            for($y=0;$y<count($member_ports_ins);$y++)
            {
                if(!empty($member_ports_ins[$y]))
                {
                 //   dbInsert(array('pop_id'=>$pop_id,'ae_interface_name'=>$ins_intf,'member_interface_name'=>$member_ports_ins[$y],'row_id'=>$index,'type'=>"ins"),'nxg_pop_ae_members');
                    add_ae_member_port($pop_id, $ins_intf,$member_ports_ins[$y]);
                }
            }


        }

    }
    foreach($db_intf_keys as $key)
    {
        $row_id= $key['row_id'];
        //dbDelete('nxg_pop_ae_members', "`pop_id` =  ? and row_id = ? ", array($pop_id,$row_id));
        dbDelete('nxg_router_interface_mapping', "`pop_details_id` =  ? and row_id = ? ", array($pop_id,$row_id));
        dbDelete('nxg_pop_interface_ip_mgmt',"pop_id =? and interface_name = ? and interface_unit =? ",array($pop_id,$key['device_interface_name'],$key['vlan_id']));
        dbDelete('nxg_pop_interface_ip_mgmt',"pop_id =? and interface_name = ? and interface_unit =? ",array($pop_id,$key['insertion_interface'],$key['insertion_vlan']));
    }
    
    update_pop_details($pop_data);
}



function generate_db_keys($pop_interface_data_db)
{
    $db_intf_keys=array();
    foreach($pop_interface_data_db as $intf_data)
    {
        $row_id=$intf_data['row_id'];
        $db_intf_keys[$row_id] = $intf_data;
    }

    return $db_intf_keys;
}

function update_pop_details($pop_data)
{
    $pop_id=$pop_data['pop_id'];
    $pop_name = $pop_data['pop_name'];
    $oss_dev_id = $pop_data['oss_dev_id'];
    $pop_description = $pop_data['pop_description'];
    $pop_notes = $pop_data['pop_notes'];
    $loopback_ip = $pop_data['loopback_ip'];
    $prefix_list = $pop_data['prefix_list'];
    $pl_agg_prefix_list = $pop_data['pl_agg_prefix_list'];

    dbUpdate(array('pop_name'=>$pop_name,'oss_device_id'=>$oss_dev_id,'pop_description'=>$pop_description,'pop_notes'=>$pop_notes,'loopback_ip'=>$loopback_ip
),'nxg_pop_details','id=?',array($pop_id));

    $prefix_list_array = explode("\n",$prefix_list);
   
    dbDelete('nxg_app_service_prefix_list','pop_id=?',array($pop_id));
 
    foreach($prefix_list_array as $prefix)
    {
        dbInsert(array('prefix'=>$prefix,'pop_id'=>$pop_id),'nxg_app_service_prefix_list');
    }

     $agg_prefix_list_array = explode("\n",$pl_agg_prefix_list);

    dbDelete('nxg_pl_agg_prefix_list','pop_id=?',array($pop_id));

    foreach($agg_prefix_list_array as $prefix)
    {
        dbInsert(array('prefix'=>$prefix,'pop_id'=>$pop_id),'nxg_pl_agg_prefix_list');
    }

}
function update_netflow_syslog_db($pop_data)
{
    $pop_id=$pop_data['pop_id'];
    $netflow_server_ip=$pop_data['netflow_server_ip'];
    $netflow_source_address=$pop_data['netflow_source_address'];
    $netflow_port=$pop_data['netflow_port'];
    $syslog_host_ip=$pop_data['syslog_host_ip']; 

    dbUpdate(array('netflow_server_ip'=>$netflow_server_ip,'netflow_source_address'=>$netflow_source_address,'netflow_port'=>$netflow_port,'syslog_host_ip'=>$syslog_host_ip),'nxg_pop_details','id=?',array($pop_id));

}

function get_all_isps($pop_id)
{
     $isp_list=dbFetchRows("select id,name from nxg_isp_details where pop_details_id = ?",array($pop_id));
     return $isp_list;

}
function configure_nfsen($device_hostname,$device_id,$flow_mgr_ip,$netflow_port)
{
    $nfsen_devicename = generate_nfsen_devicename($device_hostname, $device_id);
    if(is_null($nfsen_devicename)) {
    	return 1;
    }

    $nfsen_config_script="/opt/observium/nexusguard/nfsen/insert_nfsen_device.sh";
    $output = exec("sudo bash -x /opt/observium/nexusguard/nfsen/insert_nfsen_device.sh $nfsen_devicename $device_id $flow_mgr_ip $netflow_port",$output, $return);
    if($return != 1)
    {
        return 0;
    }
    else
    {
        return 1;
    }

}

function configure_rancid($device_hostname,$var_popmgr_rancid_user,$var_device_password)
{

    $tmp=strtolower($device_hostname);
    $device_hostname=$tmp;
    $rancid_config_script="/opt/observium/nexusguard/rancid/rancid.sh";
    $output = exec("sudo bash -x /opt/observium/nexusguard/rancid/rancid.sh $device_hostname $var_popmgr_rancid_user '$var_device_password'",$output, $return);
    if($return != 1)
    {
        return 0;
    }
    else
    {
        return 1;
    }


}
function get_all_enrolled_pops()
{
    $logtype="DB";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");
    $pop_details = dbFetchRows("select id, pop_name from nxg_pop_details where device_enrolment_status=1");
    log_trace($logtype, __FILE__, __FUNCTION__, "End");
    return $pop_details;
}
function get_all_ae_members($pop_id,$interface)
{
    $ae_member_ports=dbFetchRows("select * from nxg_pop_ae_members where pop_id=? and ae_interface_name = ?",array($pop_id,$interface));
    return $ae_member_ports;
}

function update_nfsen_config($pop_id,$hostname,$device_id,$flow_mgr_ip,$netflow_port)
{

    $nfsen_details=dbFetchRows("select flow_mgr_ip,netflow_port from nxg_pop_details where id =?;",array($pop_id));
    $netflow_port_db=$nfsen_details[0]['netflow_port'];
    $flow_mgr_ip_db=$nfsen_details[0]['flow_mgr_ip'];
    if(($netflow_port_db != $flow_mgr_ip) || ($flow_mgr_ip_db != $flow_mgr_ip))
    {
        configure_nfsen($hostname,$device_id,$flow_mgr_ip,$netflow_port);    
        dbUpdate(array('flow_mgr_ip'=>$flow_mgr_ip),'nxg_pop_details','`id`=?',array($pop_id));

    }
}
function add_device_to_hosts($device_hostname,$edge_router_ip)
{
    $output = exec("sudo bash -x /opt/observium/nexusguard/add_host/host_replace_script.sh '$device_hostname' '$edge_router_ip'",$output, $return);
}
?>

<?php
include_once("/opt/observium/includes/defaults.inc.php");
include_once("/opt/observium/config.php");
include_once("/opt/observium/includes/definitions.inc.php");
include_once("/opt/observium/nexusguard/logger/logger.php");

function get_unit_for_interface($pop_id,$interface,$type,$subtype)
{
    $logtype="DB";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");
    
    log_debug($logtype, __FILE__, __FUNCTION__, "Get unit for pop_id=".$pop_id ." and interface=".$interface. " and type=".$type );

    $result = dbFetchRow("select max(interface_unit) as unit from nxg_pop_interface_ip_mgmt where pop_id = ? and  type= ? and interface_name=?", array($pop_id,$type,$interface));
    $unit = $result['unit'];

    log_trace($logtype, __FILE__, __FUNCTION__, "End");
    return $unit;
}

function get_ip_from_db($pop_id_id=NULL,$interface,$type,$subtype)
{

    $logtype="DB";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    log_debug($logtype, __FILE__, __FUNCTION__, "Get unit for pop_id=".$pop_id_id." type=".$type." subtype=".$subtype);

    $result = dbFetchRow("select inet_ntoa(max(inet_aton(ip_address))) as ip from nxg_pop_interface_ip_mgmt where pop_id = ? and type = ? and subtype= ?", array($pop_id_id,$type,$subtype));
    $ip = $result['ip'];

    log_trace($logtype, __FILE__, __FUNCTION__, "End");

    return $ip;
}
function add_config_to_db($pop_id, $unit,$interface,$ip,$routing_instance,$type,$subtype)
{
    $logtype="DB";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    $ip_new = explode("/",$ip);
    $ip=$ip_new[0];

    $index = dbInsert(array('pop_id'=>$pop_id,'interface_unit'=>$unit,'interface_name'=>$interface,'ip_address'=>$ip,'routing_instance'=>$routing_instance,'type'=>$type,'subtype'=>$subtype),'nxg_pop_interface_ip_mgmt');

    log_trace($logtype, __FILE__, __FUNCTION__, "End");

    return $index;
}
function get_starting_unit_from_db($interface)
{

    $logtype="DB";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    $result = dbFetchRow("select interface_unit from nxg_starting_unit_ip_address where interface_name = ?", array($interface));
    $unit = $result['interface_unit'];

    log_trace($logtype, __FILE__, __FUNCTION__, "End");

    return $unit;
    
}
function get_starting_ip_for_interface($interface)
{

    $logtype="DB";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    $result = dbFetchRow("select ip_address from nxg_starting_unit_ip_address where interface_name = ?", array($interface));
    $ip = $result['ip_address'];

    log_trace($logtype, __FILE__, __FUNCTION__, "End");

    return $ip;
    
}
function update_config($isp_config,$isp_id,$pop_id)
{
    $logtype="DB";

    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");
    $interfaces = $isp_config['interfaces'];

    foreach($interfaces as $interface)
    {
        $name = $interface['interface_name'];
        foreach( $interface['interface_configuration'] as $config)
        {
            $unit = $config['unit'];
            dbUpdate(array('isp_id'=>$isp_id),'nxg_pop_interface_ip_mgmt','interface_name = ? and interface_unit = ? and pop_id =? ', array($name, $unit,$pop_id));
        }
    }

    log_trace($logtype, __FILE__, __FUNCTION__, "End");
}



function update_cust_config($pop_id,$conn_id,$cust_config)
{
    $logtype="DB";

    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");
    $interfaces = $cust_config['interfaces'];

    foreach($interfaces as $interface)
    {
        $name = $interface['interface_name'];
        foreach( $interface['interface_configuration'] as $config)
        {
            $unit = $config['unit'];
            dbUpdate(array('conn_id'=>$conn_id),'nxg_pop_interface_ip_mgmt','interface_name = ? and interface_unit = ? and pop_id =? ', array($name, $unit,$pop_id));
        }
    }

    log_trace($logtype, __FILE__, __FUNCTION__, "End");
}

function add_customer_if_ip_mgmt_config($pop_id,$conn_id,$cust_config)
{
    $logtype="DB";

    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");
    $interfaces = $cust_config['interfaces'];

    foreach($interfaces as $interface)
    {
        $interface_name = $interface['interface_name'];

        foreach( $interface['interface_configuration'] as $config)
        {
            $unit = $config['unit'];
            $routing_instance = $config['vrname'];
            $subtype = $config['iftype'];
            $ip = $config['ipv4_address'];
            $ip_new = explode("/",$ip);
            $ip=$ip_new[0];


            $db_data["pop_id"] = $pop_id;
            $db_data["interface_unit"] = $unit;
            $db_data["interface_name"] =  $interface_name;
            $db_data["ip_address"] =  $ip;
            $db_data["routing_instance"] = $routing_instance;
            $db_data["subtype"] = $subtype;
            $db_data["type"] = "customer";
            $db_data["conn_id"] = $conn_id;
            
            dbInsert($db_data,'nxg_pop_interface_ip_mgmt');
        }
    }

    log_trace($logtype, __FILE__, __FUNCTION__, "End");

}

function get_maxed_used_if($pop_id, $if_prefix)
{
    $logtype="DB";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    log_debug($logtype, __FILE__, __FUNCTION__, "Get max used if for pop_id=".$pop_id ." and if prefix=".$if_prefix);

    $result = dbFetchRow("select max(interface_name) as if from nxg_pop_interface_ip_mgmt where pop_id = ? and  interface_name like '?%'", array($pop_id,$if_prefix));
    $maxif = $result['if'];
    
    log_debug($logtype, __FILE__, __FUNCTION__, "Get max used if for pop_id=".$pop_id ." and if prefix=".$if_prefix." max if="+$maxif);

    log_trace($logtype, __FILE__, __FUNCTION__, "End");
    return $maxif;
}

function delete_pop_interface_ip_mgmt($conn_id)
{
    $logtype="DB";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    dbDelete('nxg_pop_interface_ip_mgmt', '`conn_id` = ?', array($conn_id));

    log_trace($logtype, __FILE__, __FUNCTION__, "End");
}

function add_isp_if_ip_mgmt_config($pop_id,$isp_id,$isp_config)
{
    $logtype="DB";

    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");
   
    $interfaces = $isp_config['interfaces'];

    foreach($interfaces as $interface)
    {
        $interface_name = $interface['interface_name'];
        foreach( $interface['interface_configuration'] as $config)
        {
            $unit = $config['unit'];
            $routing_instance = $config['vrname'];
            $subtype = $config['iftype'];
            $ip = $config['ipv4_address'];
            $ip_new = explode("/",$ip);
            $ip=$ip_new[0];

            $db_data = array();
            $db_data["pop_id"] = $pop_id;
            $db_data["interface_unit"] = $unit;
            $db_data["interface_name"] =  $interface_name;
            $db_data["ip_address"] =  $ip;
            $db_data["routing_instance"] = $routing_instance;
            $db_data["subtype"] = $subtype;
            $db_data["type"] = "isp";
            $db_data["isp_id"] = $isp_id;
            dbInsert($db_data,'nxg_pop_interface_ip_mgmt');
        }

    }
    log_trace($logtype, __FILE__, __FUNCTION__, "End");
}

function add_pop_config($pop_id, $pop_config)
{
    $logtype="DB";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");
    
    $interfaces = $pop_config['add_interfaces'];
    
    foreach($interfaces as $interface)
    {
        $interface_name = $interface['interface_name'];
        foreach( $interface['interface_configuration'] as $config)
        {
            $unit = $config['unit'];
            $routing_instance = $config['vr_div_name'];
            $subtype = $config['subtype'];
            $target = $config['target'];
            $route_dist = $config['route_distinguisher'];
            $ip = $config['ipv4_address'];
            $ip_new = explode("/",$ip);
            $ip=$ip_new[0];

            $db_data = array();
            $db_data["pop_id"] = $pop_id;
            $db_data["ip_address"] =  $ip;
            $db_data["interface_unit"] = $unit;
            $db_data["target"] = $target;
            $db_data["route_distinguisher"] = $route_dist;
            $db_data["interface_name"] =  $interface_name;
            $db_data["routing_instance"] = $routing_instance;
            $db_data["diversion_name"] = $routing_instance;
            $db_data["subtype"] = $subtype;
            $db_data["type"] = "pop";
            dbInsert($db_data,'nxg_pop_interface_ip_mgmt');
        }

    }


    log_trace($logtype, __FILE__, __FUNCTION__, "End");

}
function add_ae_member_port($pop_id, $ae_interface,$member_port)
{

    $logtype="DB";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    $is_member_present = dbFetchRow("select count(*) as cnt from nxg_pop_ae_members where pop_id = ? and ae_interface_name =? and member_interface_name =? ",array($pop_id,$ae_interface,$member_port));

    if($is_member_present['cnt'] <= 0)
    {
        dbInsert(array('ae_interface_name'=>$ae_interface,'member_interface_name'=>$member_port,'pop_id'=>$pop_id),'nxg_pop_ae_members');
    }

    log_trace($logtype, __FILE__, __FUNCTION__, "End");
}

function delete_ae_member_port($pop_id, $isp_config)
{

    $logtype="DB";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    $del_isp_config = $isp_config['del_members_ports'];
    foreach($del_isp_config as $config)
    {
        $ae = $config['ae_name'];
        foreach($config['member_ports'] as $ports)
        {
            $mport = $ports['name']; 
            dbDelete('nxg_pop_ae_members','member_interface_name = ? and pop_id = ? and ae_interface_name = ?',array($mport,$pop_id,$ae));
        }
    }

    log_trace($logtype, __FILE__, __FUNCTION__, "End");
}

function delete_pop_ae_member_port($pop_id, $pop_config)
{

    $logtype="DB";
    log_trace($logtype, __FILE__, __FUNCTION__, "Begin");

    $del_isp_config = $pop_config['del_member_port_config'];
    foreach($del_isp_config as $mconfig)
    {
        foreach($mconfig['member_ports'] as $config )
        {
            $mport = $config['name']; 
            dbDelete('nxg_pop_ae_members','member_interface_name = ? and pop_id = ?',array($mport,$pop_id));
        }
    }

    log_trace($logtype, __FILE__, __FUNCTION__, "End");
}
?>

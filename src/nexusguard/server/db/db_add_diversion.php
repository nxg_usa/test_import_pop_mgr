<?php
include_once("/opt/observium/includes/defaults.inc.php");
include_once("/opt/observium/config.php");
include_once("/opt/observium/includes/definitions.inc.php");

//include("/opt/observium/includes/functions.inc.php");
include_once("/opt/observium/html/includes/functions.inc.php");

function db_insert_exabgp_diversion_for_all_pop($add_diversion_all_config,$admin_name,$target,$custom_diversion_type,$custom_diversion_name)
{
    
        $add_diversion_array['pop_id'] = $add_diversion_all_config['pop_id'];
        $add_diversion_array['diversion_name'] = $add_diversion_all_config['diversion_name'];
        $add_diversion_array['diversion_type'] = $add_diversion_all_config['diversion_type'];
        $add_diversion_array['network_prefix_ip'] = $add_diversion_all_config['network_prefix_ip'];
        $add_diversion_array['network_prefix_subnet_ip'] = $add_diversion_all_config['network_prefix_subnet_ip'];
        $add_diversion_array['port_no'] = $add_diversion_all_config['port_no'];
        $add_diversion_array['source_ip'] = $add_diversion_all_config['source_ip'];
        $add_diversion_array['source_subnet'] = $add_diversion_all_config['source_subnet'];
        $add_diversion_array['source_port'] = $add_diversion_all_config['source_port'];
        $add_diversion_array['protocol'] = $add_diversion_all_config['protocol'];
        $add_diversion_array['comment'] = $add_diversion_all_config['comment'];
        $add_diversion_array['md5_keystring'] = $add_diversion_all_config['md5_keystring'];
        $date=date('Y-m-d H:i:s');
        var_dump($add_diversion_array['pop_id']);


        dbInsert(array('pop'=>$add_diversion_array['pop_id'],'name'=>$add_diversion_array['diversion_name'],'diversion_type'=>$add_diversion_array['diversion_type'],'network_ip'=>$add_diversion_array['network_prefix_ip'],'network_prefix'=>$add_diversion_array['network_prefix_subnet_ip'],'source_ip'=>$add_diversion_array['source_ip'],'source_subnet'=>$add_diversion_array['source_subnet'],'protocol'=>$add_diversion_array['protocol'],'source_port'=>$add_diversion_array['source_port'],'port'=>$add_diversion_array['source_port'],'comment'=>$add_diversion_array['comment'],'md5_keystring'=>$add_diversion_array['md5_keystring'],'admin_name'=>$admin_name,'date'=>$date), 'nxg_diversion_details');

}

function db_insert_exabgp_diversion($add_diversion_data,$admin_name,$target,$custom_diversion_type,$custom_diversion_name)
{
    
        $pop_id=$add_diversion_data['pop_id'];
        $diversion_name = $add_diversion_data['diversion_name'];
        if ($custom_diversion_type == "custom")
        {
        $diversion_type = $custom_diversion_type;
        }
        else
        {
        $diversion_type = $add_diversion_data['diversion_type'];
        }
        $destination_ip = $add_diversion_data['network_prefix_ip'];
        $destination_prefix = $add_diversion_data['network_prefix_subnet_ip'];
        $source_ip = $add_diversion_data['source_ip'];
        $source_subnet = $add_diversion_data['source_subnet'];
        $source_port = $add_diversion_data['source_port'];
        $protocol = $add_diversion_data['protocol'];
        $port_no = $add_diversion_data['port_no'];
        $comment = $add_diversion_data['comment'];
        $md5_keystring = $add_diversion_data['md5_keystring'];
        $date=date('Y-m-d H:i:s');


        dbInsert(array('pop'=>$pop_id,'name'=>$diversion_name,'diversion_type'=>$diversion_type,'network_ip'=>$destination_ip,'network_prefix'=>$destination_prefix,'source_ip'=>$source_ip,'source_subnet'=>$source_subnet,'protocol'=>$protocol,'port'=>$port_no,'comment'=>$comment,'md5_keystring'=>$md5_keystring,'source_port'=>$source_port,'admin_name'=>$admin_name,'add_date'=>$date,'target'=>$target,'custom_diversion_name'=>$custom_diversion_name), 'nxg_diversion_details');

}


?>

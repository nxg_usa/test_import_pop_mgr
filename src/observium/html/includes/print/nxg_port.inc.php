<?php

/**
 * Observium
 *
 *   This file is part of Observium.
 *
 * @package    observium
 * @subpackage webui
 * @copyright  (C) 2006-2013 Adam Armstrong, (C) 2013-2015 Observium Limited
 *
 */

function nxg_print_port_row($port,$cnt, $vars = array())
{
  global $config, $cache;
  $device = device_by_id_cache($port['device_id']);

  humanize_port($port);

  if(!isset($vars['view'])) { $vars['view'] = "basic"; }

  // Populate $port_adsl if the port has ADSL-MIB data
  if (!isset($cache['ports_option']['ports_adsl']) || in_array($port['port_id'], $cache['ports_option']['ports_adsl']))
  {
    $port_adsl = dbFetchRow("SELECT * FROM `ports_adsl` WHERE `port_id` = ?", array($port['port_id']));
  }

  // Populate $port['tags'] with various tags to identify port statuses and features
  // Port Errors
  if ($port['ifInErrors_delta'] > 0 || $port['ifOutErrors_delta'] > 0)
  {
    $port['tags'] .= generate_port_link($port, '<span class="label label-important">Errors</span>', 'port_errors');
  }

  // Port Deleted
  if ($port['deleted'] == '1')
  {
    $port['tags'] .= '<a href="'.generate_url(array('page' => 'deleted-ports')).'"><span class="label label-important">Deleted</span></a>';
  }

  // Port CBQoS
  if (isset($cache['ports_option']['ports_cbqos']))
  {
    if (in_array($port['port_id'], $cache['ports_option']['ports_cbqos']))
    {
      $port['tags'] .= '<a href="' . generate_port_url($port, array('view' => 'cbqos')) . '"><span class="label label-info">CBQoS</span></a>';
    }
  }
  else if (dbFetchCell("SELECT COUNT(*) FROM `ports_cbqos` WHERE `port_id` = ?", array($port['port_id'])))
  {
    $port['tags'] .= '<a href="' . generate_port_url($port, array('view' => 'cbqos')) . '"><span class="label label-info">CBQoS</span></a>';
  }

  // Port MAC Accounting
  if (isset($cache['ports_option']['mac_accounting']))
  {
    if (in_array($port['port_id'], $cache['ports_option']['mac_accounting']))
    {
      $port['tags'] .= '<a href="' . generate_port_url($port, array('view' => 'macaccounting')) . '"><span class="label label-info">MAC</span></a>';
    }
  }
  else if (dbFetchCell("SELECT COUNT(*) FROM `mac_accounting` WHERE `port_id` = ?", array($port['port_id'])))
  {
    $port['tags'] .= '<a href="' . generate_port_url($port, array('view' => 'macaccounting')) . '"><span class="label label-info">MAC</span></a>';
  }

  // Populated formatted versions of port rates.
  $port['bps_in']  = formatRates($port['ifInOctets_rate'] * 8);
  $port['bps_out'] = formatRates($port['ifOutOctets_rate'] * 8);

  $port['pps_in']  = format_si($port['ifInUcastPkts_rate'])."pps";
  $port['pps_out'] = format_si($port['ifOutUcastPkts_rate'])."pps";


  if($vars['view'] == "basic" || $vars['view'] == "graphs")  // Print basic view table row
  {
    $table_cols = '8';
    echo('<tr class="' . $port['row_class'] . '">
            <td class="state-marker"></td>
            <td width="1px"></td><td><!--<input type="checkbox" id="row'.$cnt.'"/>--></td>');

    if($vars['page'] != "device") // Print device name link if we're not inside the device page hierarchy.
    {
      $table_cols++; // Increment table columns by one to make sure graph line draws correctly

      echo("    <td width='200px'><span class=entity>" . generate_device_link($device, short_hostname($device['hostname'], "20")) . "</span><br />
                <span class=em>" . escape_html(truncate($port['location'], 32, "")) . "</span></td>");
    }

    echo("    <td><span class=entity>" . generate_port_link($port, rewrite_ifname($port['port_label'])) . " " . $port['tags'] . "</span><br />
                <span class=em>" . escape_html(truncate($port['ifAlias'], 50, '')) . "</span></td>" .

      '<td style="width: 110px;"> <i class="icon-circle-arrow-down" style="' . $port['bps_in_style'] . '"></i>  <span class="small" style="' . $port['bps_in_style'] . '">' . formatRates($port['in_rate']) . '</span><br />' .
      '<i class="icon-circle-arrow-up" style="' . $port['bps_out_style'] . '"></i> <span class="small" style="' . $port['bps_out_style'] . '">' . formatRates($port['out_rate']) . '</span><br /></td>' .

      '<td style="width: 90px;"> <i class="icon-circle-arrow-down" style="' . $port['bps_in_style'] . '"></i>  <span class="small" style="' . $port['bps_in_style'] . '">' . $port['ifInOctets_perc'] . '%</span><br />' .
      '<i class="icon-circle-arrow-up" style="' . $port['bps_out_style'] . '"></i> <span class="small" style="' . $port['bps_out_style'] . '">' . $port['ifOutOctets_perc'] . '%</span><br /></td>' .

      '<td style="width: 110px;"><i class="icon-circle-arrow-down" style="' . $port['pps_in_style'] . '"></i>  <span class="small" style="' . $port['pps_in_style'] . '">' . format_bi($port['ifInUcastPkts_rate']) . 'pps</span><br />' .
      '<i class="icon-circle-arrow-up" style="' . $port['pps_out_style'] . '"></i> <span class="small" style="' . $port['pps_out_style'] . '">' . format_bi($port['ifOutUcastPkts_rate']) . 'pps</span></td>' .

      '<td style="width: 110px;"><small>' . $port['human_speed'] . "<br />" . $port['ifMtu'] . "</small></td>
            <td ><small>" . $port['human_type'] . "<br />" . $port['human_mac'] . "</small></td>
          </tr>\n");
  }
  else if($vars['view'] == "details" || $vars['view'] == "detail") // Print detailed view table row
  {
    $table_cols = '9';

    echo('<tr class="' . $port['row_class'] . '"');
    if($vars['tab'] != "port") { echo (' onclick="location.href=\'' . generate_port_url($port) . '\'" style="cursor: pointer;"'); }
    echo('>');
    echo('         <td class="state-marker"></td>
         <td style="width: 1px;"></td>');

    if($vars['page'] != "device") // Print device name link if we're not inside the device page hierarchy.
    {
      $table_cols++; // Increment table columns by one to make sure graph line draws correctly

      echo('    <td width="200"><span class=entity>' . generate_device_link($device, short_hostname($device['hostname'], "20")) . '</span><br />
                <span class=em>' . escape_html(truncate($port['location'], 32, "")) . '</span></td>');
    }

    echo('
         <td style="min-width: 250px;">');

    echo("        <span class='entity-title'>
              " . generate_port_link($port) . " ".$port['tags']."
           </span><br /><span class=small>".htmlentities($port['ifAlias'])."</span>");

    if ($port['ifAlias']) { echo("<br />"); }

    unset ($break);

    if (!isset($cache['ports_option']['ipv4_addresses']) || in_array($port['port_id'], $cache['ports_option']['ipv4_addresses']))
    {
      foreach (dbFetchRows("SELECT * FROM `ipv4_addresses` WHERE `port_id` = ?", array($port['port_id'])) as $ip)
      {
        echo($break ."<a class=small href=\"javascript:popUp('/netcmd.php?cmd=whois&amp;query=".$ip['ipv4_address']."')\">".$ip['ipv4_address']."/".$ip['ipv4_prefixlen']."</a>");
        $break = "<br />";
      }
    }
    if (!isset($cache['ports_option']['ipv6_addresses']) || in_array($port['port_id'], $cache['ports_option']['ipv6_addresses']))
    {
      foreach (dbFetchRows("SELECT * FROM `ipv6_addresses` WHERE `port_id` = ?", array($port['port_id'])) as $ip6)
      {
        echo($break ."<a class=small href=\"javascript:popUp('/netcmd.php?cmd=whois&amp;query=".$ip6['ipv6_address']."')\">".Net_IPv6::compress($ip6['ipv6_address'])."/".$ip6['ipv6_prefixlen']."</a>");
        $break = "<br />";
      }
    }

    //echo("</span>");

    echo('</td>');

    // Print port graph thumbnails
    echo('<td style="width: 147px;">');
    $port['graph_type'] = "port_bits";
    echo(generate_port_link($port, "<img src='graph.php?type=port_bits&amp;id=".$port['port_id']."&amp;from=".$config['time']['day']."&amp;to=".$config['time']['now']."&amp;width=100&amp;height=20&amp;legend=no' alt=\"\" />"));
    $port['graph_type'] = "port_upkts";
    echo(generate_port_link($port, "<img src='graph.php?type=port_upkts&amp;id=".$port['port_id']."&amp;from=".$config['time']['day']."&amp;to=".$config['time']['now']."&amp;width=100&amp;height=20&amp;legend=no' alt=\"\" />"));
    $port['graph_type'] = "port_errors";
    echo(generate_port_link($port, "<img src='graph.php?type=port_errors&amp;id=".$port['port_id']."&amp;from=".$config['time']['day']."&amp;to=".$config['time']['now']."&amp;width=100&amp;height=20&amp;legend=no' alt=\"\" />"));
    echo('</td>');

    echo('<td style="width: 100px; white-space: nowrap;">');

    if ($port['ifOperStatus'] == "up" || $port['ifOperStatus'] == "monitoring")
    {
      // Colours generated by humanize_port
      echo '<i class="icon-circle-arrow-down" style="',$port['bps_in_style'], '"></i> <span class="small" style="',$port['bps_in_style'], '">' , formatRates($port['in_rate']) , '</span><br />',
      '<i class="icon-circle-arrow-up"   style="',$port['bps_out_style'],'"></i> <span class="small" style="',$port['bps_out_style'],'">' , formatRates($port['out_rate']), '</span><br />',
      '<i class="icon-circle-arrow-down" style="',$port['pps_in_style'], '"></i> <span class="small" style="',$port['pps_in_style'], '">' , format_bi($port['ifInUcastPkts_rate']), 'pps</span><br />',
      '<i class="icon-circle-arrow-up"   style="',$port['pps_out_style'],'"></i> <span class="small" style="',$port['pps_out_style'],'">' , format_bi($port['ifOutUcastPkts_rate']),'pps</span>';
    }

    echo('</td><td style="width: 110px;">');
    if ($port['ifType'] && $port['ifType'] != "") { echo("<span class=small>" . $port['human_type'] . "</span>"); } else { echo("-"); }
    echo ('<br />');
    if ($port['ifSpeed']) { echo("<span class=small>".humanspeed($port['ifSpeed'])."</span>"); }
    if ($port['ifDuplex'] && $port['ifDuplex'] != "unknown") { echo('<span class=small> (' . str_replace("Duplex", "", $port['ifDuplex']) . ')</span>'); }
    echo("<br />");
    if ($port['ifMtu'] && $port['ifMtu'] != "") { echo("<span class=small>MTU " . $port['ifMtu'] . "</span>"); } else { echo("<span class=small>Unknown MTU</span>"); }
    // if ($ifHardType && $ifHardType != "") { echo("<span class=small>" . $ifHardType . "</span>"); } else { echo("-"); }

    //echo ('<br />');

    // Set VLAN data if the port has ifTrunk populated
    if ($port['ifTrunk'])
    {
      if ($port['ifVlan'])
      {
        // Native VLAN
        if (!isset($cache['ports_vlan']))
        {
          $native_state = dbFetchCell('SELECT `state` FROM `ports_vlans` WHERE `device_id` = ? AND `port_id` = ?',    array($device['device_id'], $port['port_id']));
          $native_name  = dbFetchCell('SELECT `vlan_name` FROM vlans     WHERE `device_id` = ? AND `vlan_vlan` = ?;', array($device['device_id'], $port['ifVlan']));
        } else {
          $native_state = $cache['ports_vlan'][$port['port_id']][$port['ifVlan']]['state'];
          $native_name  = $cache['ports_vlan'][$port['port_id']][$port['ifVlan']]['vlan_name'];
        }
        switch ($native_state)
        {
          case 'blocking':   $class = 'text-error';   break;
          case 'forwarding': $class = 'text-success'; break;
          default:           $class = 'muted';
        }
        if (empty($native_name)) {$native_name = 'VLAN'.str_pad($port['ifVlan'], 4, '0', STR_PAD_LEFT); }
        $native_tooltip = 'NATIVE: <strong class='.$class.'>'.$port['ifVlan'].' ['.$native_name.']</strong><br />';
      }

      if (!isset($cache['ports_vlan']))
      {
        $vlans = dbFetchRows('SELECT * FROM `ports_vlans` AS PV
                         LEFT JOIN vlans AS V ON PV.`vlan` = V.`vlan_vlan` AND PV.`device_id` = V.`device_id`
                         WHERE PV.`port_id` = ? AND PV.`device_id` = ? ORDER BY PV.`vlan`;', array($port['port_id'], $device['device_id']));
      } else {
        $vlans = $cache['ports_vlan'][$port['port_id']];
      }
      $vlans_count = count($vlans);
      $rel = ($vlans_count || $native_tooltip) ? 'tooltip' : ''; // Hide tooltip for empty
      echo('<p class="small"><a class="label label-info" data-rel="'.$rel.'" data-tooltip="<div class=\'small\' style=\'max-width: 320px; text-align: justify;\'>'.$native_tooltip);
      if ($vlans_count)
      {
        echo('ALLOWED: ');
        $vlans_aggr = array();
        foreach ($vlans as $vlan)
        {
          if ($vlans_count > 20)
          {
            // Aggregate VLANs
            $vlans_aggr[] = $vlan['vlan'];
          } else {
            // List VLANs
            switch ($vlan['state'])
            {
              case 'blocking':   $class = 'text-error'; break;
              case 'forwarding': $class = 'text-success';  break;
              default:           $class = 'muted';
            }
            if (empty($vlan['vlan_name'])) { 'VLAN'.str_pad($vlan['vlan'], 4, '0', STR_PAD_LEFT); }
            echo("<strong class=".$class.">".$vlan['vlan'] ." [".$vlan['vlan_name']."]</strong><br />");
          }
        }
        if ($vlans_count > 20)
        {
          // End aggregate VLANs
          echo('<strong>'.range_to_list($vlans_aggr, ', ').'</strong>');
        }
      }
      echo('</div>">'.$port['ifTrunk'].'</a></p>');
    }
    else if ($port['ifVlan'])
    {
      if (!isset($cache['ports_vlan']))
      {
        $native_state = dbFetchCell('SELECT `state` FROM `ports_vlans` WHERE `device_id` = ? AND `port_id` = ?',    array($device['device_id'], $port['port_id']));
        $native_name  = dbFetchCell('SELECT `vlan_name` FROM vlans     WHERE `device_id` = ? AND `vlan_vlan` = ?;', array($device['device_id'], $port['ifVlan']));
      } else {
        $native_state = $cache['ports_vlan'][$port['port_id']][$port['ifVlan']]['state'];
        $native_name  = $cache['ports_vlan'][$port['port_id']][$port['ifVlan']]['vlan_name'];
      }
      switch ($vlan_state)
      {
        case 'blocking':   $class = 'label-error';   break;
        case 'forwarding': $class = 'label-success'; break;
        default:           $class = '';
      }
      $rel = ($native_name) ? 'tooltip' : ''; // Hide tooltip for empty
      echo('<br /><span data-rel="'.$rel.'" class="label '.$class.'"  data-tooltip="<strong class=\'small '.$class.'\'>'.$port['ifVlan'].' ['.$native_name.']</strong>">VLAN ' . $port['ifVlan'] . '</span>');
    }
    else if ($port['ifVrf']) // Print the VRF name if the port is assigned to a VRF
    {
      $vrf_name = dbFetchCell("SELECT `vrf_name` FROM `vrfs` WHERE `vrf_id` = ?", array($port['ifVrf']));
      echo('<br /><span class="small badge badge-success" data-rel="tooltip" data-tooltip="VRF">'.$vrf_name.'</span>');
    }

    echo ('</td>');

    // If the port is ADSL, print ADSL port data.
    if ($port_adsl['adslLineCoding'])
    {
      echo('<td style="width: 200px;"><span class="small">');
      echo('<span class="label">'.$port_adsl['adslLineCoding'].'</span> <span class="label">' . rewrite_adslLineType($port_adsl['adslLineType']).'</span>');
      echo("<br />");
      echo('SYN <i class="icon-circle-arrow-down green"></i> '.formatRates($port_adsl['adslAtucChanCurrTxRate']) . ' <i class="icon-circle-arrow-up blue"></i> '. formatRates($port_adsl['adslAturChanCurrTxRate']));
      echo("<br />");
      //echo("Max:".formatRates($port_adsl['adslAtucCurrAttainableRate']) . "/". formatRates($port_adsl['adslAturCurrAttainableRate']));
      //echo("<br />");
      echo('ATN <i class="icon-circle-arrow-down green"></i> '.$port_adsl['adslAtucCurrAtn'] . 'dBm <i class="icon-circle-arrow-up blue"></i> '. $port_adsl['adslAturCurrAtn'] . 'dBm');
      echo('<br />');
      echo('SNR <i class="icon-circle-arrow-down green"></i> '.$port_adsl['adslAtucCurrSnrMgn'] . 'dB <i class="icon-circle-arrow-up blue"></i> '. $port_adsl['adslAturCurrSnrMgn']. 'dB');
      echo('</span>');
    } else {
      // Otherwise print normal port data
      echo("<td style='width: 150px;'>");
      if ($port['ifPhysAddress'] && $port['ifPhysAddress'] != "") { echo("<span class=small>" . $port['human_mac'] . "</span>"); } else { echo("-"); }
      echo("<br />");
    }

    echo('</td>');
    echo('<td style="min-width: 275px" class=small>');


    if (strpos($port['port_label'], "oopback") === FALSE && !$graph_type)
    {
      unset($br);

      // Populate links array for ports with direct links
      if (!isset($cache['ports_option']['neighbours']) || in_array($port['port_id'], $cache['ports_option']['neighbours']))
      {
        foreach (dbFetchRows('SELECT * FROM `neighbours` WHERE `port_id` = ?', array($port['port_id'])) as $neighbour)
        {
          // print_r($link);
          if($neighbour['remote_port_id']) {
            $int_links[$neighbour['remote_port_id']] = $neighbour['remote_port_id'];
            $int_links_phys[$neighbour['remote_port_id']] = 1;
          } else {
            $int_links_unknown[] = $neighbour;
          }
        }
      } else {  }

      // Populate links array for devices which share an IPv4 subnet
      if (!isset($cache['ports_option']['ipv4_addresses']) || in_array($port['port_id'], $cache['ports_option']['ipv4_addresses']))
      {
        foreach (dbFetchColumn('SELECT DISTINCT(`ipv4_network_id`) FROM `ipv4_addresses`
                                 LEFT JOIN `ipv4_networks` USING(`ipv4_network_id`)
                                 WHERE `port_id` = ? AND `ipv4_network` NOT IN (?);', array($port['port_id'], $config['ignore_common_subnet'])) as $network_id)
        {
          $sql = 'SELECT N.*, P.`port_id`, P.`device_id` FROM `ipv4_addresses` AS A, `ipv4_networks` AS N, `ports` AS P
                   WHERE A.`port_id` = P.`port_id` AND P.`device_id` != ?
                   AND A.`ipv4_network_id` = ? AND N.`ipv4_network_id` = A.`ipv4_network_id`
                   AND P.`ifAdminStatus` = "up"';

          $params = array($device['device_id'], $network_id);

          foreach (dbFetchRows($sql, $params) as $new)
          {
            if ($cache['devices']['id'][$new['device_id']]['disabled'] && !$config['web_show_disabled']) { continue; }
            $int_links[$new['port_id']] = $new['port_id'];
            $int_links_v4[$new['port_id']][] = $new['ipv4_network'];
          }
        }
      }

      // Populate links array for devices which share an IPv6 subnet
      if (!isset($cache['ports_option']['ipv6_addresses']) || in_array($port['port_id'], $cache['ports_option']['ipv6_addresses']))
      {
        foreach (dbFetchColumn("SELECT DISTINCT(`ipv6_network_id`) FROM `ipv6_addresses`
                                 LEFT JOIN `ipv6_networks` USING(`ipv6_network_id`)
                                 WHERE `port_id` = ? AND `ipv6_network` NOT IN (?);", array($port['port_id'], $config['ignore_common_subnet'])) as $network_id)
        {
          $sql = "SELECT P.`port_id`, P.`device_id` FROM `ipv6_addresses` AS A, `ipv6_networks` AS N, `ports` AS P
                   WHERE A.`port_id` = P.`port_id` AND P.device_id != ?
                   AND A.`ipv6_network_id` = ? AND N.`ipv6_network_id` = A.`ipv6_network_id`
                   AND P.`ifAdminStatus` = 'up' AND A.`ipv6_origin` != 'linklayer' AND A.`ipv6_origin` != 'wellknown'";

          $params = array($device['device_id'], $network_id);

          foreach (dbFetchRows($sql, $params) as $new)
          {
            if ($cache['devices']['id'][$new['device_id']]['disabled'] && !$config['web_show_disabled']) { continue; }
            $int_links[$new['port_id']] = $new['port_id'];
            $int_links_v6[$new['port_id']][] = $new['port_id'];
          }
        }
      }

      // Output contents of links array
      foreach ($int_links as $int_link)
      {
        $link_if  = get_port_by_id_cache($int_link);
        $link_dev = device_by_id_cache($link_if['device_id']);
        echo($br);

        if ($int_links_phys[$int_link]) { echo('<a alt="Directly connected" class="oicon-connect"></a> '); }
        else { echo('<a alt="Same subnet" class="oicon-network-hub"></a> '); }

        echo("<b>" . generate_port_link($link_if, $link_if['port_label_short']) . " on " . generate_device_link($link_dev, short_hostname($link_dev['hostname'])) . "</b>");

        ## FIXME -- do something fancy here.

        if ($int_links_v6[$int_link]) { echo ' ', overlib_link('', '<span class="label label-success">IPv6</span>', implode("<br />", $int_links_v6[$int_link]), NULL); }
        if ($int_links_v4[$int_link]) { echo ' ', overlib_link('', '<span class="label label-info">IPv4</span>', implode("<br />", $int_links_v4[$int_link]), NULL); }
        $br = "<br />";
      }

      // Output content of unknown links array (where ports don't exist in our database, or they weren't matched)

      foreach ($int_links_unknown as $int_link)
      {
        // FIXME -- Expose platform and version here.
        echo('<a alt="Directly connected" class="oicon-plug-connect"></a> ');
        echo('<b><i>'.short_ifname($int_link['remote_port']).'</i></b> on ');

        echo('<b><i><a data-rel="tooltip" data-tooltip="<div class=\'small\' style=\'max-width: 500px;\'><b>'.$int_link['remote_platform'].'</b><br />'.$int_link['remote_version'].'</div>" data-hasqtip="4" aria-describedby="qtip-4">'.$int_link['remote_hostname'].'</a></i></b>');
        echo('<br />');
      }


    }

    if (!isset($cache['ports_option']['pseudowires']) || in_array($port['port_id'], $cache['ports_option']['pseudowires']))
    {
      foreach (dbFetchRows("SELECT * FROM `pseudowires` WHERE `port_id` = ?", array($port['port_id'])) as $pseudowire)
      {
        //`port_id`,`peer_device_id`,`peer_ldp_id`,`pwID`,`pwIndex`
        #    $pw_peer_dev = dbFetchRow("SELECT * FROM `devices` WHERE `device_id` = ?", array($pseudowire['peer_device_id']));
        $pw_peer_int = dbFetchRow("SELECT * FROM `ports` AS I, `pseudowires` AS P WHERE I.`device_id` = ? AND P.`pwID` = ? AND P.`port_id` = I.`port_id`", array($pseudowire['peer_device_id'], $pseudowire['pwID']));

        #    $pw_peer_int = get_port_by_id_cache($pseudowire['peer_device_id']);
        $pw_peer_dev = device_by_id_cache($pseudowire['peer_device_id']);

        if (is_array($pw_peer_int))
        {
          humanize_port($pw_peer_int);
          echo($br.'<i class="oicon-arrow-switch"></i> <strong>' . generate_port_link($pw_peer_int, $pw_peer_int['port_label_short']) .' on '. generate_device_link($pw_peer_dev, short_hostname($pw_peer_dev['hostname'])) . '</strong>');
        } else {
          echo($br.'<i class="oicon-arrow-switch"></i> <strong> VC ' . $pseudowire['pwID'] .' on '. $pseudowire['peer_addr'] . '</strong>');
        }
        echo ' <span class="label">'.$pseudowire['pwPsnType'].'</span>';
        echo ' <span class="label">'.$pseudowire['pwType'].'</span>';
        $br = "<br />";
      }
    }

    if (!isset($cache['ports_option']['ports_pagp']) || in_array($port['ifIndex'], $cache['ports_option']['ports_pagp']))
    {
      foreach (dbFetchRows("SELECT * FROM `ports` WHERE `pagpGroupIfIndex` = ? AND `device_id` = ?", array($port['ifIndex'], $device['device_id'])) as $member)
      {
        humanize_port($member);
        $pagp[$device['device_id']][$port['ifIndex']][$member['ifIndex']] = TRUE;
        echo($br.'<i class="oicon-arrow-join"></i> <strong>' . generate_port_link($member) . ' [PAgP]</strong>');
        $br = "<br />";
      }
    }

    if ($port['pagpGroupIfIndex'] && $port['pagpGroupIfIndex'] != $port['ifIndex'])
    {
      $pagp[$device['device_id']][$port['pagpGroupIfIndex']][$port['ifIndex']] = TRUE;
      $parent = dbFetchRow("SELECT * FROM `ports` WHERE `ifIndex` = ? and `device_id` = ?", array($port['pagpGroupIfIndex'], $device['device_id']));
      humanize_port($parent);
      echo($br.'<i class="oicon-arrow-split"></i> <strong>' . generate_port_link($parent) . ' [PAgP]</strong>');
      $br = "<br />";
    }

    if (!isset($cache['ports_option']['ports_stack_low']) || in_array($port['ifIndex'], $cache['ports_option']['ports_stack_low']))
    {
      foreach (dbFetchRows("SELECT * FROM `ports_stack` WHERE `port_id_low` = ? and `device_id` = ?", array($port['ifIndex'], $device['device_id'])) as $higher_if)
      {
        if ($higher_if['port_id_high'])
        {
          if ($pagp[$device['device_id']][$higher_if['port_id_high']][$port['ifIndex']]) { continue; } // Skip if same PAgP port
          $this_port = get_port_by_index_cache($device['device_id'], $higher_if['port_id_high']);
          if (is_array($this_port))
          {
            echo($br.'<i class="oicon-arrow-split"></i> <strong>' . generate_port_link($this_port) . '</strong>');
            $br = "<br />";
          }
        }
      }
    }

    if (!isset($cache['ports_option']['ports_stack_high']) || in_array($port['ifIndex'], $cache['ports_option']['ports_stack_high']))
    {
      foreach (dbFetchRows("SELECT * FROM `ports_stack` WHERE `port_id_high` = ? and `device_id` = ?", array($port['ifIndex'], $device['device_id'])) as $lower_if)
      {
        if ($lower_if['port_id_low'])
        {
          if ($pagp[$device['device_id']][$port['ifIndex']][$lower_if['port_id_low']]) { continue; } // Skip if same PAgP ports
          $this_port = get_port_by_index_cache($device['device_id'], $lower_if['port_id_low']);
          if (is_array($this_port))
          {
            echo($br.'<i class="oicon-arrow-join"></i> <strong>' . generate_port_link($this_port) . "</strong>");
            $br = "<br />";
          }
        }
      }
    }

    unset($int_links, $int_links_v6, $int_links_v4, $int_links_phys, $br);

    echo("</td></tr>");
  } // End Detailed View

  // If we're showing graphs, generate the graph and print the img tags

  if ($vars['graph'] == "etherlike")
  {
    $graph_file = get_port_rrdfilename($port, "dot3", TRUE);
  } else {
    $graph_file = get_port_rrdfilename($port, NULL, TRUE);
  }

  if ($vars['graph'] && is_file($graph_file))
  {

    echo('<tr><td colspan="'.$table_cols.'">');

    $graph_array['to']     = $config['time']['now'];
    $graph_array['id']     = $port['port_id'];
    $graph_array['type']   = 'port_'.$vars['graph'];

    print_graph_row($graph_array);

    echo('</td></tr>');

  }
}

// EOF

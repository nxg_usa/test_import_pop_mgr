<?php
if (!isset($vars['view']) ) { $vars['view'] = "view_topo"; }
//$navbar = array('brand' => "Pop Template", 'class' => "navbar-narrow");
//$navbar['options']['pop_template_grid']['text'] = 'Pop Template Grid';
//$navbar['options']['add_pop_template_jquery']['text'] = 'Pop template Grid Jquery';
//$navbar['options']['add_pop_template']['text'] = 'Add Pop Template';
//$navbar['options']['pop_isp_grid']['text'] = 'Pop & ISP Grid';
//$navbar['options']['customer_grid']['text'] = 'Customer Grid';
//$navbar['options']['add_customer']['text'] = 'Add a Customer';
//$navbar['options']['add_filter']['text'] = 'Add a Filter';
//$navbar['options']['traffic_filter']['text'] = 'Traffic Filter';
//$navbar['options']['auditlog_grid']['text'] = 'Audit Log';
//$navbar['options']['add_isp']['text'] = 'Add ISP';
//$navbar['options']['traffic_diversion']['text'] = 'Traffic Diversion';
$link_array = array('page'    => 'pop_mgr',
                    );

foreach ($navbar['options'] as $option => $array)
{
  if ($vars['view'] == $option) { $navbar['options'][$option]['class'] .= " active"; }
  $navbar['options'][$option]['url'] = generate_url($link_array,array('view' => $option));
}
//print_navbar($navbar);
unset($navbar);

if($vars['view'] == "ports_popup")
{
    include("nexusguard/views/pop/ports_popup.php"); //Addnew pop template
}

if($vars['view'] == "add_pop_template")
{
    include("nexusguard/views/pop/add_pop_template.php"); //Addnew pop template
}
else if($vars['view'] =="modify_pop_template")
{
    include("nexusguard/views/pop/modify_pop_template.php"); //Modify pop template
}

else if($vars['view'] =="pop_isp_grid")
{
        include("nexusguard/views/pop/pop_isp_grid.php"); //Show pop & isp grid
}
else if($vars['view'] =="add_pop_template_jquery")
{
        include("nexusguard/views/pop/pop_template_grid_jquery.php"); //Show pop & isp grid
}
else if($vars['view'] =="add_neighbor")
{

        include("nexusguard/views/isp/add_isp.php"); //Show pop & isp grid
}
else if($vars['view'] =="add_isp_bak")
{

        include("nexusguard/views/isp/add_isp_bak.php"); //Show pop & isp grid
}
else if($vars['view'] =="add_customer")
{
        include("nexusguard/views/customer/add_customer.php"); //Show pop & isp grid
}
else if($vars['view'] =="traffic_engg")
{
        include("nexusguard/views/customer/traffic_engg.php"); //Show pop & isp grid
}
else if($vars['view'] =="modify_customer")
{

        include("nexusguard/views/customer/modify_customer.php"); //Show pop & isp grid
}
else if($vars['view'] =="customer_grid")
{

        include("nexusguard/views/customer/customer_grid.php"); //Show pop & isp grid
}

else if($vars['view'] == "auditlog_grid")
{
    include("nexusguard/views/auditlog/auditlog_grid.php"); //Show pop audit log grid
}

else if($vars['view'] == "config_watch")
{
    include("nexusguard/views/config_watch/config_watch.php"); //Show config differences
}

else if($vars['view'] == "modify_customer"){
}
else if($vars['view'] == "modify_customer"){

include("nexusguard/views/customer/edit_customer.php"); //Show pop edit customer grid
}
else if($vars['view'] == "modify_neighbor"){

include("nexusguard/views/isp/modify_isp.php"); //Show pop edit isp grid
}
else if($vars['view'] == "add_filter"){

include("nexusguard/views/filter/add_filter.php"); //Show pop add filter

}else if($vars['view'] == "modify_filter"){

    include("nexusguard/views/filter/modify_filter.php"); //Show pop add filter

}else if($vars['view'] == "traffic_filter"){

include("nexusguard/views/filter/traffic_filter.php"); //Show pop traffic filter

}else if($vars['view'] == "modify_pop_template"){

include("nexusguard/views/pop/modify_pop_template.php");

}else if( $vars['view'] == "add_diversion"){

include("nexusguard/views/diversion/add_diversion.php"); //Show add diversion

}if( $vars['view'] == "traffic_diversion"){

include("nexusguard/views/diversion/traffic_diversion.php"); //Show add diversion

}else if( $vars['view'] == "modify_diversion"){

include("nexusguard/views/diversion/delete_exabgp_diversion.php"); //Show add diversion
}
else if( $vars['view'] == "add_static_route"){

include("nexusguard/views/diversion/add_static_route.php"); //Show add diversion
}
else if( $vars['view'] == "delete_route"){

include("nexusguard/views/diversion/delete_route.php"); //Show add diversion
}
else if( $vars['view'] == "delete_manual_diversion"){

include("nexusguard/views/diversion/delete_manual_diversion.php"); //Show delete static diversion form
}

else if( $vars['view'] == "dashboard"){

include_once("nexusguard/views/dashboard/dashboard_conf.php"); // get URLs for AJAX queries
include("nexusguard/views/dashboard/dashboard.php"); //Show monitoring dashboard

}else if( $vars['view'] == "bwcosts"){

include_once("nexusguard/views/dashboard/bwcosts_conf.php"); // get URLs for AJAX queries
include("nexusguard/views/dashboard/bwcosts.php"); //Show bwcosts
//}else if( $vars['view'] == "bwcostsanalysis"){
//
//include_once("nexusguard/views/dashboard/bwcosts_conf.php"); // get URLs for AJAX queries
//include("nexusguard/views/dashboard/bwcostsanalysis.php"); //Analyze bwcosts
}
else if( $vars['view'] == "delete_isp"){

include("nexusguard/views/isp/delete_isp.php"); //Show add diversion
}
else if( $vars['view'] == "edit_isp"){

include("nexusguard/views/isp/modify_isp_details.php"); //Show add diversion
}
else if( $vars['view'] == "delete_traffic_filter"){

include("nexusguard/views/filter/delete_traffic_filter.php");

}else if( $vars['view'] == "customer_grid_filter"){

include("nexusguard/views/customer/customer_grid_filter.php");

}else if( $vars['view'] == "ip_management"){

include("nexusguard/views/config_watch/ip_management.php");

}



?>

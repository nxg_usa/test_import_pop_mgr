#!/bin/bash

source /opt/observium/nexusguard/ansible/popmgr_env.sh

host=$1
username=$2
password=$3
action=$4
jsonfile=$5
conf_file=$6
commit_check_log=$7
FILES_DIRECTORY="/opt/observium/nexusguard/ansible/script"


#---------------- Remove log and configuration and inventory file
rm $FILES_DIRECTORY/mod_pop_install_hosts
#-----------------


#----------------- Create host file
echo "$host ansible_connection=local ansible_ssh_host=$host" > $MODIFY_POP_HOST_FILE
#-----------------



#------------------ execute ansible according to action
if [ $action = "commit_check" ]
then
       sudo ansible-playbook -i $MODIFY_POP_HOST_FILE $FILES_DIRECTORY/modify_pop.yml  --check -vvvv --extra-vars "@$jsonfile" --extra-vars '{ "commit_comment": "'$conf_file'" }'
        status=$?
        if [ $status != 0 ]
        then
            exit $status
        fi

elif [ $action = "commit" ]
then
    sudo ansible-playbook -i $MODIFY_POP_HOST_FILE $FILES_DIRECTORY/modify_pop.yml  --extra-vars "@$jsonfile" --extra-vars '{ "commit_comment": "'$conf_file'" }'
        status=$?
        if [ $status != 0 ]
        then
            exit $status
        fi
fi
#------------------


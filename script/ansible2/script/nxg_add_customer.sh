#!/bin/bash

source /opt/observium/nexusguard/ansible/popmgr_env.sh

host=$1
username=$2
password=$3
action=$4
jsonfile=$5
conf_file=$6
commit_check_log=$7



#----------------- Create host file
echo "$host ansible_connection=local ansible_ssh_host=$host" > $CUSTOMER_HOST_FILE
#-----------------



#------------------ execute ansible according to action
if [ $action = "commit_check" ]
then
       sudo ansible-playbook -i $CUSTOMER_HOST_FILE $FILES_DIRECTORY/add_customer.yml  --check --extra-vars "@$jsonfile"  --extra-vars '{ "commit_comment": "'$conf_file'" }'
        status=$?
        if [ $status != 0 ]
        then
            exit $status
        fi

elif [ $action = "commit" ]
then
       sudo ansible-playbook -i $CUSTOMER_HOST_FILE $FILES_DIRECTORY/add_customer.yml  --extra-vars "@$jsonfile" --extra-vars '{ "commit_comment": "'$conf_file'" }'
        status=$?
        if [ $status != 0 ]
        then
            exit $status
        fi
fi  
#------------------


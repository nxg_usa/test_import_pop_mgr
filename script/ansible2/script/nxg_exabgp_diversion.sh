#!/bin/bash

#source env file
source /opt/observium/nexusguard/ansible/popmgr_env.sh
pop_name=${1}
local_server_ip=${2}
local_server_username=${3}
global_server_ip=${4}
global_server_username=${5}
action=${6}
jsonfile=${7}
latest_local_conf_file=${8}
latest_global_conf_file=${9}
commit_check_log=${10}


#----------------- Create host file
echo "[remote]" > $LOCAL_EXABGP_HOST_FILE
echo "$local_server_ip ansible_connection=local ansible_ssh_host=$local_server_ip ansible_ssh_port=22 ansible_ssh_user=$local_server_username ansible_ssh_pass=$SSH_KEY_DIR " >> $LOCAL_EXABGP_HOST_FILE
echo "[remote]" > $GLOBAL_EXABGP_HOST_FILE
echo "$global_server_ip ansible_connection=local ansible_ssh_host=$global_server_ip ansible_ssh_port=22 ansible_ssh_user=$global_server_username ansible_ssh_pass=$SSH_KEY_DIR " >> $GLOBAL_EXABGP_HOST_FILE
#-----------------

if [ $action = "commit_check" ]
then

#------------------ execute ansible according to action
sudo ansible-playbook -i $LOCAL_EXABGP_HOST_FILE $FILES_DIRECTORY/exabgp_diversion.yml  --check  --extra-vars "@$jsonfile"     
 
 if [ ! -f $OUTPUT_DIRECTORY/$pop_name"_ADD_NXG_LATEST_LOCAL_CONF.cfg" ]
 then
        echo "" > $OUTPUT_DIRECTORY/$pop_name"_ADD_NXG_LATEST_LOCAL_CONF.cfg"
 fi

 if [ ! -f $OUTPUT_DIRECTORY/$pop_name"_ADD_NXG_LATEST_GLOBAL_CONF.cfg" ]
 then
        echo "" > $OUTPUT_DIRECTORY/$pop_name"_ADD_NXG_LATEST_GLOBAL_CONF.cfg"
 fi

echo "-----------------------  local server commit diff for $pop_name ----------------------"
diff -u $OUTPUT_DIRECTORY/$pop_name"_ADD_NXG_LATEST_LOCAL_CONF.cfg" $latest_local_conf_file
local_diff=$?
echo "-----------------------  global server commit diff for $pop_name ----------------------"
diff -u $OUTPUT_DIRECTORY/$pop_name"_ADD_NXG_LATEST_GLOBAL_CONF.cfg" $latest_global_conf_file 
global_diff=$?


#-----------------------------------------------------------------------------



else
echo -e "###################################  LOCAL EXABGP SERVER ##################################\n\n\n"

    last_local_conf_file_to_copy_to_script=`ls -lrt $OUTPUT_DIRECTORY/local_exabgp_diversion* | awk -F ' ' '{print $9}' |  tail -n 1 `
    last_global_conf_file_to_copy_to_script=`ls -lrt $OUTPUT_DIRECTORY/global_exabgp_diversion* | awk -F ' ' '{print $9}' | tail -n 1 `



    echo 'env exabgp.tcp.bind='$local_server_ip' \'  > $OUTPUT_DIRECTORY/local_nxg-exabgp.env
    echo 'exabgp.log.enable=true \'  >> $OUTPUT_DIRECTORY/local_nxg-exabgp.env
    echo 'exabgp.log.destination=/var/log/exbgp.log \' >> $OUTPUT_DIRECTORY/local_nxg-exabgp.env
    echo 'exabgp.log.packets=true \ ' >> $OUTPUT_DIRECTORY/local_nxg-exabgp.env
    echo 'exabgp.daemon.user=wheel \ ' >> $OUTPUT_DIRECTORY/local_nxg-exabgp.env
    echo 'exabgp.daemon.daemonize=true \ ' >> $OUTPUT_DIRECTORY/local_nxg-exabgp.env
    echo 'exabgp.daemon.pid=/var/run/exabpg.pid' >> $OUTPUT_DIRECTORY/local_nxg-exabgp.env


    echo '#!/bin/bash' > $OUTPUT_DIRECTORY/local_startExaBGP
    echo 'env exabgp.tcp.bind='$local_server_ip' \' >> $OUTPUT_DIRECTORY/local_startExaBGP
    echo 'exabgp.log.enable=true \' >>  $OUTPUT_DIRECTORY/local_startExaBGP
    echo 'exabgp.log.destination=/var/log/exbgp.log \' >> $OUTPUT_DIRECTORY/local_startExaBGP
    echo 'exabgp.log.packets=true \' >>  $OUTPUT_DIRECTORY/local_startExaBGP
    echo 'exabgp.log.message=true \' >> $OUTPUT_DIRECTORY/local_startExaBGP
    echo 'exabgp.log.network=true \' >> $OUTPUT_DIRECTORY/local_startExaBGP
    echo 'exabgp.log.parser=true \' >> $OUTPUT_DIRECTORY/local_startExaBGP
    echo 'exabgp.log.processes=true \' >> $OUTPUT_DIRECTORY/local_startExaBGP
    echo 'exabgp.log.reactor=true \' >> $OUTPUT_DIRECTORY/local_startExaBGP
    echo 'exabgp.log.rib=true \' >> $OUTPUT_DIRECTORY/local_startExaBGP
    echo 'exabgp.daemon.user=root \' >> $OUTPUT_DIRECTORY/local_startExaBGP
    echo 'exabgp.daemon.daemonize=true \' >> $OUTPUT_DIRECTORY/local_startExaBGP
    echo 'exabgp.daemon.pid=/var/run/exabpg.pid  \' >> $OUTPUT_DIRECTORY/local_startExaBGP
    echo 'exabgp '$REMOTE_EXABGP_DIR'/nxg_exabgp_configuration.cfg' >> $OUTPUT_DIRECTORY/local_startExaBGP




    echo "[remote]" > $LOCAL_EXABGP_HOST_FILE_FOR_ANSIBLE_REMOTE
    echo "$local_server_ip ansible_ssh_user=$local_server_username " >> $LOCAL_EXABGP_HOST_FILE_FOR_ANSIBLE_REMOTE

    sudo scp -o 'StrictHostKeyChecking no' -o ConnectTimeout=2 -i $SSH_KEY_DIR $OUTPUT_DIRECTORY/local_startExaBGP  $local_server_username@$local_server_ip:$REMOTE_EXABGP_DIR/startExaBGP
    scp_nxg_exabgp_local_script=$?
    
    sudo scp -o 'StrictHostKeyChecking no' -o ConnectTimeout=2 -i $SSH_KEY_DIR $OUTPUT_DIRECTORY/local_nxg-exabgp.env  $local_server_username@$local_server_ip:$REMOTE_EXABGP_DIR/nxg-exabgp.env
    scp_nxg_exabgp_local_env_file=$?

    sudo ansible -T 2 remote -i $LOCAL_EXABGP_HOST_FILE_FOR_ANSIBLE_REMOTE -m raw -a "sudo cp $REMOTE_EXABGP_DIR/nxg_exabgp_configuration.cfg $REMOTE_EXABGP_DIR/nxg_exabgp_configuration.cfg.backup" -vvvv -c paramiko --private-key=$SSH_KEY_DIR >> /dev/null 2>&1

    sudo scp -o 'StrictHostKeyChecking no' -o ConnectTimeout=2 -i $SSH_KEY_DIR $last_local_conf_file_to_copy_to_script  $local_server_username@$local_server_ip:$REMOTE_EXABGP_DIR/nxg_exabgp_configuration.cfg
    scp_nxg_exabgp_local_conf_file=$?

    if [ $scp_nxg_exabgp_local_conf_file = 0 -a $scp_nxg_exabgp_local_script = 0 -a $scp_nxg_exabgp_local_env_file = 0 ]
    then    
        sudo ansible -T 2 remote -i $LOCAL_EXABGP_HOST_FILE_FOR_ANSIBLE_REMOTE -m raw -a "ps -ef | grep exabgp | grep -v grep" -vvvv -c paramiko --private-key=$SSH_KEY_DIR  > /tmp/ansible_exabgp_command_log_local
        pid_of_local_exabgp=`cat /tmp/ansible_exabgp_command_log_local | grep '/usr/local/bin/exabgp' | awk -F ' ' '{print $2}'`
        if [ x"$pid_of_local_exabgp" = x ]
        then
            sudo ansible -T 2 remote -i $LOCAL_EXABGP_HOST_FILE_FOR_ANSIBLE_REMOTE -m raw -a "sudo exabgp --fi > /usr/local/etc/exabgp/exabgp.env" -c paramiko --private-key=$SSH_KEY_DIR >> /dev/null 2>&1
            sudo ansible -T 2 remote -i $LOCAL_EXABGP_HOST_FILE_FOR_ANSIBLE_REMOTE -m raw -a "sh -x $REMOTE_EXABGP_DIR/startExaBGP"  -c paramiko --private-key=$SSH_KEY_DIR >> /dev/null 2>&1
            sudo ansible -T 2 remote -i $LOCAL_EXABGP_HOST_FILE_FOR_ANSIBLE_REMOTE -m raw -a "ps -ef | grep exabgp | grep -v grep"  -c paramiko --private-key=$SSH_KEY_DIR  > /tmp/check_process_start_local
            pid_of_local_exabgp_check_start=`cat /tmp/check_process_start_local | grep '/usr/local/bin/exabgp' | awk -F ' ' '{print $2}'` 
             if [ x"$pid_of_local_exabgp_check_start" = x ]
             then
                    echo -e "-------------------- DEAMON START FAIL ,  RELOADING LAST SAVED CONFIGURATION AND RESTARTING SERVER----------------\n\n\n"
                    sudo ansible -T 2 remote -i $LOCAL_EXABGP_HOST_FILE_FOR_ANSIBLE_REMOTE -m raw -a "sudo cp $REMOTE_EXABGP_DIR/nxg_exabgp_configuration.cfg.backup $REMOTE_EXABGP_DIR/nxg_exabgp_configuration.cfg" -c paramiko --private-key=$SSH_KEY_DIR  >> /dev/null 2>&1
        
                    sudo ansible -T 2 remote -i $LOCAL_EXABGP_HOST_FILE_FOR_ANSIBLE_REMOTE -m raw -a "sh -x $REMOTE_EXABGP_DIR/startExaBGP"  -c paramiko --private-key=$SSH_KEY_DIR >> /dev/null 2>&1
                    sudo ansible -T 2 remote -i $LOCAL_EXABGP_HOST_FILE_FOR_ANSIBLE_REMOTE -m raw -a "ps -ef | grep exabgp | grep -v grep" -c paramiko --private-key=$SSH_KEY_DIR  > /tmp/check_process_start_again_local
                    pid_of_local_exabgp_check_start_again=`cat /tmp/check_process_start_again_local | grep '/usr/local/bin/exabgp' | awk -F ' ' '{print $2}'`             
                    id_of_local_exabgp_check_start_again=0
                    if [ x"$pid_of_local_exabgp_check_start_again" = x ]
                    then
                            id_of_local_exabgp_check_start_again=1
                            echo -e "---------------- RELOADING LAST SAVED CONFIGURATION AND RESTARTING SERVER ALSO FAILED --------------\n\n\n"
                    fi
            else
                        id_of_local_exabgp_check_start_again=0
                        echo -e "------------ DIVERSION ADDED SUCCESSFULLY --------------"
            fi        

             

        else
                sudo ansible -T 2 remote -i $LOCAL_EXABGP_HOST_FILE_FOR_ANSIBLE_REMOTE -m raw -a "sudo kill -9 $pid_of_local_exabgp" -vvvv -c paramiko --private-key=$SSH_KEY_DIR >> /dev/null 2>&1
                sudo ansible -T 2 remote -i $LOCAL_EXABGP_HOST_FILE_FOR_ANSIBLE_REMOTE -m raw -a "sh -x $REMOTE_EXABGP_DIR/startExaBGP" -vvvv -c paramiko --private-key=$SSH_KEY_DIR >> /dev/null 2>&1
                sudo ansible -T 2 remote -i $LOCAL_EXABGP_HOST_FILE_FOR_ANSIBLE_REMOTE -m raw -a "ps -ef | grep exabgp | grep -v grep" -vvvv -c paramiko --private-key=$SSH_KEY_DIR  > /tmp/check_process_start_local
            pid_of_local_exabgp_check_start=`cat /tmp/check_process_start_local | grep '/usr/local/bin/exabgp' | awk -F ' ' '{print $2}'`
             if [ x"$pid_of_local_exabgp_check_start" = x ]
             then
                    echo -e "--------------------DEAMON START FAIL ,  RELOADING LAST SAVED CONFIGURATION AND RESTARTING SERVER----------------\n\n\n"
                    sudo ansible -T 2 remote -i $LOCAL_EXABGP_HOST_FILE_FOR_ANSIBLE_REMOTE -m raw -a "sudo cp $REMOTE_EXABGP_DIR/nxg_exabgp_configuration.cfg.backup $REMOTE_EXABGP_DIR/nxg_exabgp_configuration.cfg" -vvvv -c paramiko --private-key=$SSH_KEY_DIR >> /dev/null 2>&1

                    sudo ansible -T 2 remote -i $LOCAL_EXABGP_HOST_FILE_FOR_ANSIBLE_REMOTE -m raw -a "sh -x $REMOTE_EXABGP_DIR/startExaBGP" -vvvv -c paramiko --private-key=$SSH_KEY_DIR >> /dev/null 2>&1
                    sudo ansible -T 2 remote -i $LOCAL_EXABGP_HOST_FILE_FOR_ANSIBLE_REMOTE -m raw -a "ps -ef | grep exabgp | grep -v grep" -vvvv -c paramiko --private-key=$SSH_KEY_DIR  > /tmp/check_process_start_again_local
                    pid_of_local_exabgp_check_start_again=`cat /tmp/check_process_start_again_local | grep '/usr/local/bin/exabgp' | awk -F ' ' '{print $2}'`
                    id_of_local_exabgp_check_start_again=0 
                    if [ x"$pid_of_local_exabgp_check_start_again" = x ]
                    then
                            id_of_local_exabgp_check_start_again=1
                            echo -e "---------------- RELOADING LAST SAVED CONFIGURATION AND RESTARTING SERVER ALSO FAILED --------------\n\n\n"
                    fi
             else
                        id_of_local_exabgp_check_start_again=0
                        echo -e "------------ DIVERSION ADDED SUCCESSFULLY --------------\n\n\n"
            fi


             
        fi


        copy_files_local=0


    else
        echo "----------------------- Could not copy files on the local exabgp server for $pop_name---------------------------"
        copy_files_local=1 
    fi


echo -e "#############################  GLOBAL EXABGP SERVER ##################################\n\n\n"

        echo 'env exabgp.tcp.bind='$global_server_ip' \'  > $OUTPUT_DIRECTORY/global_nxg-exabgp.env
    echo 'exabgp.log.enable=true \'  >> $OUTPUT_DIRECTORY/global_nxg-exabgp.env
    echo 'exabgp.log.destination=/var/log/exbgp.log \' >> $OUTPUT_DIRECTORY/global_nxg-exabgp.env
    echo 'exabgp.log.packets=true \ ' >> $OUTPUT_DIRECTORY/global_nxg-exabgp.env
    echo 'exabgp.daemon.user=wheel \ ' >> $OUTPUT_DIRECTORY/global_nxg-exabgp.env
    echo 'exabgp.daemon.daemonize=true \ ' >> $OUTPUT_DIRECTORY/global_nxg-exabgp.env
    echo 'exabgp.daemon.pid=/var/run/exabpg.pid' >> $OUTPUT_DIRECTORY/global_nxg-exabgp.env

    echo '#!/bin/bash' > $OUTPUT_DIRECTORY/global_startExaBGP
    echo 'env exabgp.tcp.bind='$global_server_ip' \' >> $OUTPUT_DIRECTORY/global_startExaBGP
    echo 'exabgp.log.enable=true \' >>  $OUTPUT_DIRECTORY/global_startExaBGP
    echo 'exabgp.log.destination=/var/log/exbgp.log \' >> $OUTPUT_DIRECTORY/global_startExaBGP
    echo 'exabgp.log.packets=true \' >>  $OUTPUT_DIRECTORY/global_startExaBGP
    echo 'exabgp.log.message=true \' >> $OUTPUT_DIRECTORY/global_startExaBGP
    echo 'exabgp.log.network=true \' >> $OUTPUT_DIRECTORY/global_startExaBGP
    echo 'exabgp.log.parser=true \' >> $OUTPUT_DIRECTORY/global_startExaBGP
    echo 'exabgp.log.processes=true \' >> $OUTPUT_DIRECTORY/global_startExaBGP
    echo 'exabgp.log.reactor=true \' >> $OUTPUT_DIRECTORY/global_startExaBGP
    echo 'exabgp.log.rib=true \' >> $OUTPUT_DIRECTORY/global_startExaBGP
    echo 'exabgp.daemon.user=root \' >> $OUTPUT_DIRECTORY/global_startExaBGP
    echo 'exabgp.daemon.daemonize=true \' >> $OUTPUT_DIRECTORY/global_startExaBGP
    echo 'exabgp.daemon.pid=/var/run/exabpg.pid  \' >> $OUTPUT_DIRECTORY/global_startExaBGP
    echo 'exabgp '$REMOTE_EXABGP_DIR'/nxg_exabgp_configuration.cfg' >> $OUTPUT_DIRECTORY/global_startExaBGP



    echo "[remote]" > $GLOBAL_EXABGP_HOST_FILE_FOR_ANSIBLE_REMOTE
    echo "$global_server_ip ansible_ssh_user=$global_server_username " >> $GLOBAL_EXABGP_HOST_FILE_FOR_ANSIBLE_REMOTE


    sudo scp -o 'StrictHostKeyChecking no' -o ConnectTimeout=2 -i $SSH_KEY_DIR $OUTPUT_DIRECTORY/global_startExaBGP  $global_server_username@$global_server_ip:$REMOTE_EXABGP_DIR/startExaBGP
    scp_nxg_exabgp_global_script=$?


    sudo scp -o 'StrictHostKeyChecking no' -o ConnectTimeout=2 -i $SSH_KEY_DIR $OUTPUT_DIRECTORY/global_nxg-exabgp.env  $global_server_username@$global_server_ip:$REMOTE_EXABGP_DIR/nxg-exabgp.env
    scp_nxg_exabgp_global=$?


    sudo scp -o 'StrictHostKeyChecking no' -o ConnectTimeout=2 -i $SSH_KEY_DIR $last_global_conf_file_to_copy_to_script  $local_server_username@$global_server_ip:$REMOTE_EXABGP_DIR/nxg_exabgp_configuration.cfg
    scp_nxg_global_conf=$?

    sudo ansible -T 2 remote -i $GLOBAL_EXABGP_HOST_FILE_FOR_ANSIBLE_REMOTE -m raw -a "sudo cp $REMOTE_EXABGP_DIR/nxg_exabgp_configuration.cfg $REMOTE_EXABGP_DIR/nxg_exabgp_configuration.cfg.backup" -vvvv -c paramiko --private-key=$SSH_KEY_DIR >> /dev/null 2>&1

    if [ $scp_nxg_exabgp_global_script = 0 -a $scp_nxg_exabgp_global = 0 -a $scp_nxg_global_conf = 0 ]
    then
        sudo ansible -T 2 remote -i $GLOBAL_EXABGP_HOST_FILE_FOR_ANSIBLE_REMOTE -m raw -a "ps -ef | grep exabgp | grep -v grep" -vvvv -c paramiko --private-key=$SSH_KEY_DIR  > /tmp/ansible_exabgp_command_log_global
        pid_of_global_exabgp=`cat /tmp/ansible_exabgp_command_log_global | grep '/usr/local/bin/exabgp' | awk -F ' ' '{print $2}'`
        if [ x"$pid_of_global_exabgp" = x ]
        then
            sudo ansible -T 2 remote -i $GLOBAL_EXABGP_HOST_FILE_FOR_ANSIBLE_REMOTE -m raw -a "sudo exabgp --fi > /usr/local/etc/exabgp/exabgp.env" -vvvv -c paramiko --private-key=$SSH_KEY_DIR >> /dev/null 2>&1
            sudo ansible -T 2 remote -i $GLOBAL_EXABGP_HOST_FILE_FOR_ANSIBLE_REMOTE -m raw -a "sh -x $REMOTE_EXABGP_DIR/startExaBGP" -vvvv -c paramiko --private-key=$SSH_KEY_DIR >> /dev/null 2>&1
            sudo ansible -T 2 remote -i $GLOBAL_EXABGP_HOST_FILE_FOR_ANSIBLE_REMOTE -m raw -a "ps -ef | grep exabgp | grep -v grep" -vvvv -c paramiko --private-key=$SSH_KEY_DIR  > /tmp/check_process_start_global
            pid_of_global_exabgp_check_start=`cat /tmp/check_process_start_global | grep '/usr/local/bin/exabgp' | awk -F ' ' '{print $2}'`
            if [ x"$pid_of_global_exabgp_check_start" = x ]
            then
                echo -e "--------------------DEAMON START FAIL ,  RELOADING LAST SAVED CONFIGURATION AND RESTARTING SERVER----------------\n\n\n"
                 sudo ansible -T 2 remote -i $GLOBAL_EXABGP_HOST_FILE_FOR_ANSIBLE_REMOTE -m raw -a "sudo cp $REMOTE_EXABGP_DIR/nxg_exabgp_configuration.cfg.backup $REMOTE_EXABGP_DIR/nxg_exabgp_configuration.cfg" -vvvv -c paramiko --private-key=$SSH_KEY_DIR >> /dev/null 2>&1
                 sudo ansible -T 2 remote -i $GLOBAL_EXABGP_HOST_FILE_FOR_ANSIBLE_REMOTE -m raw -a "ps -ef | grep exabgp | grep -v grep" -vvvv -c paramiko --private-key=$SSH_KEY_DIR  > /tmp/check_process_start_global_again
            pid_of_global_exabgp_check_start_again=`cat /tmp/check_process_start_global_again | grep '/usr/local/bin/exabgp' | awk -F ' ' '{print $2}'`
            id_of_global_exabgp_check_start_again=0
                if [ x"$pid_of_global_exabgp_check_start_again" = x ]
                then
                        id_of_global_exabgp_check_start_again=1
                        echo -e "---------------- RELOADING LAST SAVED CONFIGURATION AND RESTARTING SERVER ALSO FAILED --------------\n\n\n"     
                fi 
            else
                        id_of_global_exabgp_check_start_again=0
                        echo -e "------------ DIVERSION ADDED SUCCESSFULLY --------------\n\n\n"
            fi
            
        else
            sudo ansible -T 2 remote -i $GLOBAL_EXABGP_HOST_FILE_FOR_ANSIBLE_REMOTE -m raw -a "sudo kill -9 $pid_of_global_exabgp" -vvvv -c paramiko --private-key=$SSH_KEY_DIR >> /dev/null 2>&1
            sudo ansible -T 2 remote -i $GLOBAL_EXABGP_HOST_FILE_FOR_ANSIBLE_REMOTE -m raw -a "sh -x $REMOTE_EXABGP_DIR/startExaBGP" -vvvv -c paramiko --private-key=$SSH_KEY_DIR >> /dev/null 2>&1
            sudo ansible -T 2 remote -i $GLOBAL_EXABGP_HOST_FILE_FOR_ANSIBLE_REMOTE -m raw -a "ps -ef | grep exabgp | grep -v grep" -vvvv -c paramiko --private-key=$SSH_KEY_DIR  > /tmp/check_process_start_global
            pid_of_global_exabgp_check_start=`cat /tmp/check_process_start_global | grep '/usr/local/bin/exabgp' | awk -F ' ' '{print $2}'`
        
            if [ x"$pid_of_global_exabgp_check_start" = x ]
            then
                echo -e "--------------------DEAMON START FAIL ,  RELOADING LAST SAVED CONFIGURATION AND RESTARTING SERVER----------------\n\n\n"
                 sudo ansible -T 2 remote -i $GLOBAL_EXABGP_HOST_FILE_FOR_ANSIBLE_REMOTE -m raw -a "sudo cp $REMOTE_EXABGP_DIR/nxg_exabgp_configuration.cfg.backup $REMOTE_EXABGP_DIR/nxg_exabgp_configuration.cfg" -vvvv -c paramiko --private-key=$SSH_KEY_DIR >> /dev/null 2>&1
                 sudo ansible -T 2 remote -i $GLOBAL_EXABGP_HOST_FILE_FOR_ANSIBLE_REMOTE -m raw -a "ps -ef | grep exabgp | grep -v grep" -vvvv -c paramiko --private-key=$SSH_KEY_DIR  > /tmp/check_process_start_global_again
            pid_of_global_exabgp_check_start_again=`cat /tmp/check_process_start_global_again | grep '/usr/local/bin/exabgp' | awk -F ' ' '{print $2}'`
                id_of_global_exabgp_check_start_again=0
                if [ x"$pid_of_global_exabgp_check_start_again" = x ]
                then
                        id_of_global_exabgp_check_start_again=1
                        echo -e "---------------- RESTORING THE CONFIG AND RESTARTING DEAMON ALSO FAILED --------------\n\n\n"
                fi
            else
                        id_of_global_exabgp_check_start_again=0
                        echo -e "-------------- DIVERSION ADDED SUCCESSFULLY --------------\n\n\n"
            fi
        fi
        copy_files_global=0
    else
        echo "-------------------------------- Could not copy files to global server for $pop_name -----------------------"
        copy_files_global=1


fi







if [ $copy_files_local != 1 -o $copy_files_global != 1 ]
then
     if [ $id_of_local_exabgp_check_start_again = 1 -a $id_of_global_exabgp_check_start_again = 1 ]
     then
                exit 1
     else
                last_local_conf_file_to_copy=`ls -lrt $OUTPUT_DIRECTORY/local_exabgp_diversion* | awk -F ' ' '{print $9}' |  tail -n 1 `
                last_global_conf_file_to_copy=`ls -lrt $OUTPUT_DIRECTORY/global_exabgp_diversion* | awk -F ' ' '{print $9}' | tail -n 1 `
                sudo cp $last_local_conf_file_to_copy $OUTPUT_DIRECTORY/$pop_name"_ADD_NXG_LATEST_LOCAL_CONF.cfg"
                sudo cp $last_global_conf_file_to_copy $OUTPUT_DIRECTORY/$pop_name"_ADD_NXG_LATEST_GLOBAL_CONF.cfg"
                exit 0
      
    fi
else
                exit 1
fi





fi

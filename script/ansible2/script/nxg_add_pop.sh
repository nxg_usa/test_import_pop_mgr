#!/bin/bash

source /opt/observium/nexusguard/ansible/popmgr_env.sh

host=$1
username=$2
password=$3
action=$4
pop_jsonfile=$5
isp_json=$6




#----------------- Create host file
echo "$host ansible_connection=local ansible_ssh_host=$host " > $ADD_POP_HOST_FILE
#-----------------



#------------------ execute ansible according to action
if [ $action = "commit_check" ]
then
       sudo ansible-playbook -i $ADD_POP_HOST_FILE $FILES_DIRECTORY/POPtemplate.yml   --check  --extra-vars "@$pop_jsonfile"  

fi
#------------------





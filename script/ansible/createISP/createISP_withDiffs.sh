#!/bin/bash

rm -rf logs/*

ansible-playbook -i hosts createISP.yml --check --diff --extra-vars "@vars/createISP.json" -vvvv

if [ -e 'logs/createISP_diffs' ]; then
	clear
	echo "The proposed configuration differs from the current configuration.  Please review the diffs below:"
	cat logs/createISP_diffs
	read -r -p "Would you like to proceed with the commit? [y/N] " response
	case $response in
    		[yY][eE][sS]|[yY]) 
			ansible-playbook -i hosts createISP.yml --diff --extra-vars "@vars/createISP.json" -vvvv
        		;;
    		*)
        		echo "Exiting and will not proceed with commit"
			exit 0
        		;;
	esac
else
	clear
	echo "No diffs exist and commit will proceed"
	ansible-playbook -i hosts createISP.yml --diff --extra-vars "@vars/createISP.json" -vvvv
fi

exit 0

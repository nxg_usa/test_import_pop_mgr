#!/bin/bash

host=$1
username=$2
password=$3
action=$4
ANSIBLE_DIRECTORY="/root/ansible/"

#----------------- Remove log and configuration and inventory file 
rm $ANSIBLE_DIRECTORY/POPtemplate.conf
rm $ANSIBLE_DIRECTORY/commit_check.log
rm $ANSIBLE_DIRECTORY/changes.log
rm $ANSIBLE_DIRECTORY/hosts
#-----------------


#----------------- Create host file 
echo "$host ansible_connection=local ansible_ssh_host=$host ansible_ssh_port=830 ansible_ssh_user=$username ansible_ssh_pass=$password " > $ANSIBLE_DIRECTORY/hosts 
#-----------------



#------------------ execute ansible according to action
if [ $action = "commit_check" ]
then
	ansible-playbook -i $ANSIBLE_DIRECTORY/hosts $ANSIBLE_DIRECTORY/POPtemplate.yml  -vvvv --check  --extra-vars "@POPtemplate.json"
		
		if [ $? -ne 0 ]; then
			echo “An error occurred during run.  Please review the following log messages:”
        		cat $ANSIBLE_DIRECTORY/commit_check.log
		else
			exit 0
		fi

elif [ $action = "commit" ]
then
	ansible-playbook -i $ANSIBLE_DIRECTORY/hosts $ANSIBLE_DIRECTORY/POPtemplate.yml  -vvvv --extra-vars "@POPtemplate.json"

		if [ $? -ne 0 ]; then
			echo “An error occurred during run.  Please review the following log messages:”
        		cat $ANSIBLE_DIRECTORY/changes.log
		else
			exit 0
		fi

fi
#------------------


#!/bin/bash
echo -n "Enter password for git URL  > "
read -s git_pwd
git_url="https://sandeepgotkhindikar:$git_pwd@bitbucket.org/serro-dev/pop_mgr.git"
git_branch="master"
base_dir="/home/ubuntu/nxg_builds"
clone_dir="$base_dir/pop_mgr"
observium_dir="/opt/observium"
backup_dir="$base_dir/observium_backup"
date_backup_file=`date +"%b_%d_%Y_%H_%M"`
nxg_server_dir="/opt/observium/"
nxg_html_dir="/opt/observium/html"
env_var_dir="/opt/observium/nexusguard/var"
git_nfsen_dir="$clone_dir/src/nfsen"
nfsen_dir="/var/www/nfsen"
git_pop_mgr_file="$clone_dir/src/observium/html/pages/pop_mgr.inc.php"
git_navbar_file="$clone_dir/src/observium/html/includes/navbar.inc.php"
navbar_file_dir="/opt/observium/html/includes"
pop_mgr_file_dir="/opt/observium/html/pages"
build_version=`cat ${env_var_dir}/build.version`
build_version="$build_version"


if [ -d $clone_dir ];then
        rm -rf $clone_dir
fi
if [ ! -d $backup_dir ];then
        mkdir $backup_dir
fi
echo "---- Fetching sourcecode from git"
git clone -b ${git_branch} ${git_url}
if [ $? -ne 0 ];then
    echo "Git clone failed"
    exit 1
else
     echo "Git clone succeded"
fi
echo "---- Taking backup for existing Observium codebase"
cd /opt
sudo tar -cvzf $backup_dir/observium_backup_${date_backup_file}.tar.gz observium --exclude=observium/rrd --exclude=observium/mibs --exclude=observium/.svn
if [ $? -ne 0 ];then
     echo "---- Failed to create backup for Observium,exiting ....."
     exit 1
else
    echo "---- Created backup for existing Observium"
    sudo rm -rf $nxg_server_dir/nexusguard
    sudo rm -rf $nxg_html_dir/nexusguard
    sudo cp -rf $clone_dir/src/nexusguard/server $nxg_server_dir/nexusguard
    sudo cp -rf $clone_dir/src/nexusguard/html  $nxg_html_dir/nexusguard
    sudo cp -rf $clone_dir/src/observium/html $observium_dir/
    sudo rm -rf $nxg_server_dir/nexusguard/ansible/
    sudo cp -rf $clone_dir/script/ansible2/    $nxg_server_dir/nexusguard/ansible
    sudo cp -rf $clone_dir/script/ansible2/popmgr_env.sh.default $nxg_server_dir/nexusguard/ansible/popmgr_env.sh
    sudo mkdir  $nxg_server_dir/nexusguard/ansible/output
    sudo chown -R www-data:www-data $nxg_server_dir/nexusguard/ansible/
    sudo chmod 755 $nxg_server_dir/nexusguard/ansible/script/*
    sudo chmod +x $nxg_html_dir/nexusguard/views/config_watch/job_config_watcher.php
    sudo chmod +x $nxg_html_dir/nexusguard/views/auditlog/syslog_cronjob.php
    sudo chmod 755 $nxg_server_dir/nexusguard/nfsen/insert_nfsen_device.sh
    sudo chmod 755 $nxg_server_dir/nexusguard/rancid/rancid.sh
    sudo chmod 755 $nxg_server_dir/nexusguard/phpipam/phpipam.sh
    cd $env_var_dir
    sudo mv popmgr_var.php.default popmgr_var.php
    sudo cp -rf $git_nfsen_dir/* $nfsen_dir
    sudo cp -rf $git_pop_mgr_file $pop_mgr_file_dir
    sudo cp -rf $git_navbar_file $navbar_file_dir
    cd $clone_dir 
    echo "Tagging the build with new version i.e $build_version"
    tag="${build_version}"
    git tag -af $tag -m 'version $version'
    git push origin -f $tag
fi
